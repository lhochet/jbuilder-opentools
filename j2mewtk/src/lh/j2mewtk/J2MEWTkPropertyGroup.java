package lh.j2mewtk;

import java.io.*;
import java.util.*;

import com.borland.jbuilder.node.*;
import com.borland.jbuilder.paths.*;
import com.borland.primetime.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.runtime.*;
import lh.j2mewtk.run.*;
import com.borland.primetime.vfs.*;
import java.util.logging.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:12 $
 * 0.5, 7/07/02
 * 0.4, 7/05/02
 * 0.3, 28/11/01
 * 0.2, 18/09/01
 * 0.1.1, 22/07/01
 * 0.1, 14/05/01
 */

public class J2MEWTkPropertyGroup implements PropertyGroup
{
  static Logger log = Logger.getLogger(J2MEWTkPropertyGroup.class.getName());

  public static final String J2MEWTK_CAT = "lh.j2mewtk";
  public static final String WTK_VERSION = "wtkversion";
  public static final String INVALID_VERSION = "-1";

  public static final String JB_RUNTIME_CAT = "runtime";

  public static final int V_UNKNOWN = 0;
//  public static final int V1_0_1 = 1;
//  public static final int V1_0_2EA = 2;

//  public static final int V1_0_3 = 3;
//  public static final int V1_0_3L = 4;
//  public static final int V1_0_4 = 5;
//  public static final int V1_0_4L = 6;
//  public static final int V2_0B1 = 7;

  // global properties
  public static final GlobalProperty WIZARD_MIDLET_VERSION = new GlobalProperty(J2MEWTK_CAT, "wizard_midlet_version", "0.1");
//  public static final GlobalIntegerProperty WTK_VERSION = new GlobalIntegerProperty(J2MEWTK_CAT, "wtk_version", 0);
  public static final GlobalProperty NEW_PROJECTS_JDK_NAME = new GlobalProperty(J2MEWTK_CAT, "new_prj_jdk_name", "J2ME CLDC-MIDP");
//  public static final GlobalProperty KVEM_HOME = new GlobalProperty(J2MEWTK_CAT, "kvem_home");
//  public static final GlobalProperty MIDPAPI_ZIP_SUBPATH = new GlobalProperty(J2MEWTK_CAT, "midpapi_zip_subpath", "\\lib\\midpapi.zip");
  public static final GlobalProperty KENV_ZIP_SUBPATH = new GlobalProperty(J2MEWTK_CAT, "kenv_zip_subpath", "/wtklib/kenv.zip");
  public static final GlobalProperty KTOOLS_ZIP_SUBPATH = new GlobalProperty(J2MEWTK_CAT, "ktools_zip_subpath", "/wtklib/ktools.zip");
  public static final GlobalProperty CUSTOMJMF_JAR_SUBPATH = new GlobalProperty(J2MEWTK_CAT, "customjmf_jar_subpath", "/wtklib/customjmf.jar");
  public static final GlobalProperty DEVICES_DIR_SUBPATH = new GlobalProperty(J2MEWTK_CAT, "devices_dir_subpath", "/wtklib/devices");
  public static final GlobalProperty PREVERIFY_SUBPATH = new GlobalProperty(J2MEWTK_CAT, "preverify_subpath", "/bin/preverify");
  public static final GlobalProperty EMULATOR_MAIN_CLASS = new GlobalProperty(J2MEWTK_CAT, "emulator_main_class", "com.sun.kvem.environment.EmulatorWrapper");
//  public static final GlobalProperty EMULATOR_MAIN_CLASS = new GlobalProperty(J2MEWTK_CAT, "emulator_main_class", "com.sun.kvem.midp.Main");

  // project node properties
  public static final MapProperty JDK_NAME = new MapProperty(JB_RUNTIME_CAT, "jdk_name");
  public static final MapProperty DEVICE_NAME = new MapProperty(JB_RUNTIME_CAT, "device_name");
  public static final MapProperty HTTP_PORT = new MapProperty(JB_RUNTIME_CAT, "http_port");
  public static final MapProperty HTTP_HOST = new MapProperty(JB_RUNTIME_CAT, "http_host");
  public static final MapProperty TRACE_GC = new MapProperty(JB_RUNTIME_CAT, "trace_gc");
//  public static final NodeProperty TRACE_CALLS = new NodeProperty(J2MEWTK_CAT, "trace_calls");
  public static final MapProperty TRACE_CLASSES = new MapProperty(JB_RUNTIME_CAT, "trace_classes");
//  public static final NodeProperty TRACE_EXCEPTIONS = new NodeProperty(J2MEWTK_CAT, "trace_exceptions");
  public static final MapProperty TRACE_ALL = new MapProperty(JB_RUNTIME_CAT, "trace_all");

  public static final NodeProperty JAD_FILE = new NodeProperty(J2MEWTK_CAT, "jad_file");
  public static final NodeProperty JAR_FILE = new NodeProperty(J2MEWTK_CAT, "jar_file");
  public static final NodeProperty MANIFEST_FILE = new NodeProperty(J2MEWTK_CAT, "manifest_file");
  public static final NodeProperty PREVERIFIED_DIR = new NodeProperty(J2MEWTK_CAT, "preverified_dir");
  public static final NodeProperty RES_DIR = new NodeProperty(J2MEWTK_CAT, "res_dir");

  // JDK Props
  public static final MapProperty JDK_WTK_VERSION = new MapProperty(J2MEWTK_CAT, WTK_VERSION);
  public static final MapProperty OPTIONALS_VERSION = new MapProperty(J2MEWTK_CAT, "optionals");
  // keep MobileSet props
  public static final MapProperty PROFILE_VERSION = new MapProperty("micro", "micro_profile");
  public static final MapProperty CONFIGURATION_VERSION = new MapProperty("micro", "micro_configuration");


  // cls dir (from std prg)

  public J2MEWTkPropertyGroup()
  {
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH J2MEWTkPropertyGroup 0.6");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    PropertyManager.registerPropertyGroup(new J2MEWTkPropertyGroup());
  }

  public void initializeProperties()
  {
//    System.out.println("init props");
    char newChar = File.separatorChar;
    if (KENV_ZIP_SUBPATH.getValue().indexOf(newChar) < 0)
    {
      char oldChar = newChar == '/' ? '\\' : '/';
//      MIDPAPI_ZIP_SUBPATH.setValue(MIDPAPI_ZIP_SUBPATH.getValue().replace(oldChar, newChar));
      KENV_ZIP_SUBPATH.setValue(KENV_ZIP_SUBPATH.getValue().replace(oldChar, newChar));
      KTOOLS_ZIP_SUBPATH.setValue(KTOOLS_ZIP_SUBPATH.getValue().replace(oldChar, newChar));
      DEVICES_DIR_SUBPATH.setValue(DEVICES_DIR_SUBPATH.getValue().replace(oldChar, newChar));
      PREVERIFY_SUBPATH.setValue(PREVERIFY_SUBPATH.getValue().replace(oldChar, newChar));
    }
  }

  public PropertyPageFactory getPageFactory(Object parm1)
  {
    final Object topic = parm1;

    //check if we should show the project properties
    if (topic instanceof RunConfiguration.SubTopic)
    {
      RunConfiguration.SubTopic subtopic = (RunConfiguration.SubTopic)parm1;
      final Map map = subtopic.getConfiguration().getMap();
      String runnerStr = RunConfiguration.RUNNABLE_TYPE.getValue(map);
      if ((runnerStr != null) && runnerStr.equals(J2MEWTkEmulatorRunner.class.getName()))
      {
        return new PropertyPageFactory(WTKRes.getString("propgrp.emulator_prefs"))
        {
          public PropertyPage createPropertyPage()
          {
            return new EmulatorPreferencesPropertyPage(map);
          }
        };
      }
    }

    //check if we should show the project properties
    if (topic instanceof JBProject)
    {
      String jadpath = JAD_FILE.getValue((JBProject)topic);
      if (jadpath != null)
      {
        return new PropertyPageFactory(WTKRes.getString("propgrp.j2me"))
        {
          public PropertyPage createPropertyPage()
          {
            return new J2MEWTkProjectPropertyPage((JBProject)topic);
          }
        };
      }
    }
    else if (topic == null) // or the j2me wtk configuration properties
    {
      // disabled, not currently used
//      return new PropertyPageFactory(WTKRes.getString("propgrp.j2me_toolkit"))
//      {
//        public PropertyPage createPropertyPage()
//        {
//          return new J2MEWTkToolkitPropertyPage();
//        }
//      };
    }
    return null; // else not our business
  }

  public static String getJarExePath()
  {
    JBProject prj = (JBProject)(Browser.getActiveBrowser().getDefaultProject());
    JDKPathSet jdk = prj.getPaths().getJDKPathSet();
    String sjdk = jdk.getHomePath().getFullName();
    String jarpathexe = sjdk + "/bin/jar";
    if (com.borland.primetime.PrimeTime.isVerbose()) log.finest("jar path: "+jarpathexe);
//    if (!(new File(jarpath)).exists()) return null;
    return jarpathexe;
  }

  public static boolean isMobile(JDKPathSet jdk)
  {
    Url url = jdk.getHomePath();
    String home = url.getFile();
    File wtklib = new File(home, "wtklib");
    return wtklib.exists();
  }
}
