package lh.j2mewtk;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.*;

import com.borland.jbuilder.paths.*;
import com.borland.jbuilder.runtime.*;
import com.borland.primetime.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.vfs.*;
import java.util.logging.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:13 $
 * @created 1/05/04
 */

public class WTKJDKType implements JDKType
{
//  private static Logger log = Logger.getLogger(WTKJDKType.class.getName());
  private static boolean finished = false;
//  private static long start = 0;
//  private static long end = 0;
//  private static void start()
//  {
//    start = System.currentTimeMillis();
//  }
//  private static void end()
//  {
//    end = System.currentTimeMillis();
//    log.finest("" + (end - start) + " ms");
//  }


  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH WTKJDKType 0.1");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    JDKTypeManager.registerJDKType(new WTKJDKType());
  }

  public WTKJDKType()
  {
  }

  public PropertyPageFactory getPageFactory(final Object parm1)
  {
    if (parm1 instanceof JDKPathSet)
    {
      return new PropertyPageFactory(WTKRes.getString("wtktype.WTK"))
      {
        public PropertyPage createPropertyPage()
        {
          return new WTKJDKPropertyPage((JDKPathSet)parm1);
        }
      };
    }
    return null;
  }

  public void fixupPathSet(JDKPathSet pathSet)
  {
//    log.finest("fixup");
//      pathSet.setProperty("lhtst", "tst", "tst");
//    if (wtkversion != null)
//    {
//      pathSet.setProperty("lhtst", "wtkversion", wtkversion);
//    }
  }

  public int getKind()
  {
    return JDKType.MICRO_EDITION;
  }

  public int getMatchLevel(JDKPathSet pathSet)
  {
    Map map = pathSet.getMap();
    String configversion = J2MEWTkPropertyGroup.CONFIGURATION_VERSION.getValue(map);
//    log.finest("ml configversion="+configversion);
    String profileversion = J2MEWTkPropertyGroup.PROFILE_VERSION.getValue(map);
//    log.finest("ml profileversion="+profileversion);

    if ((profileversion != null) && (configversion != null))
    {
      return JDKType.BASIC_MATCH;
    }

    String wtkversion = J2MEWTkPropertyGroup.JDK_WTK_VERSION.getValue(map);
//    String wtkversion = pathSet.getProperty(J2MEWTkPropertyGroup.J2MEWTK_CAT, J2MEWTkPropertyGroup.WTK_VERSION);
//    log.finest("ml wtkversion="+wtkversion);
    if ((wtkversion == null) || (wtkversion.equals(J2MEWTkPropertyGroup.INVALID_VERSION)))
    {
      return addVersions(pathSet) ? JDKType.BASIC_MATCH : JDKType.NO_MATCH;
//      return addVersions(pathSet) ? JDKType.MATCH : JDKType.NO_MATCH;
    }
    else
    {
//      return JDKType.BASIC_MATCH;
      return JDKType.MATCH;
    }

//    log.finest("ml2 wtkversion="+wtkversion);
//
//    if (wtkversion == null)
//    {
//      wtkversion = J2MEWTkPropertyGroup.INVALID_VERSION;
//    }
//
//    J2MEWTkPropertyGroup.JDK_WTK_VERSION.setValue(map, wtkversion);
//
//    if (!wtkversion.equals(J2MEWTkPropertyGroup.INVALID_VERSION))
//    {
//      return JDKType.BASIC_MATCH;
//    }
//    return JDKType.NO_MATCH;
  }


  // helpers

//  private static String wtkversion = null;
  public static boolean addVersions(final JDKPathSet pathSet) //)
  {
    final Url home = pathSet.getHomePath();
    final String homes = home.getFile();
    final File kenv = new File(homes, J2MEWTkPropertyGroup.KENV_ZIP_SUBPATH.getValue());
    final File ktools = new File(homes, J2MEWTkPropertyGroup.KTOOLS_ZIP_SUBPATH.getValue());
//    log.finest("kenv=" + kenv);
//    log.finest("ktools=" + ktools);

    if (!kenv.exists())return false;
    if (!ktools.exists())return false;
    boolean ret = true; //false;
    finished = true;

//    log.finest("trying to run cmd line");
    finished = false;
//    start();
    File java = JavaProcess.getJavaLauncher(JDKPathSet.getDefaultJDK().getHomePath().getFileObject());
    RunJavaProcess process = new RunJavaProcess();
    process.addJavaProcessListener(new JavaProcessListener()
    {
      public void javaProcessPaused(JavaProcess javaProcess)
      {
      }

      public void javaProcessResumed(JavaProcess javaProcess)
      {
      }

      public void javaProcessSent(JavaProcess javaProcess, String string, boolean _boolean)
      {
//        log.finest("s = " + string);
        Map map = pathSet.getMap();
        String vrs;
        StringTokenizer tk;
        if (string.startsWith("J2ME Wireless Toolkit"))
        {
//          Map map = pathSet.getMap();
//          String vrs;
          tk = new StringTokenizer(string, "\n");
          vrs = tk.nextToken();
          vrs = vrs.substring("J2ME Wireless Toolkit ".length());
//          log.finest("s = " + vrs);
          J2MEWTkPropertyGroup.JDK_WTK_VERSION.setValue(map, vrs);
          if (tk.hasMoreTokens())
          {
            vrs = tk.nextToken();
//            log.finest("s2 = " + vrs);
            if (vrs.startsWith("Profile: "))
            {
              vrs = vrs.substring("Profile: ".length());
            }
//            log.finest("s3 = " + vrs);
            J2MEWTkPropertyGroup.PROFILE_VERSION.setValue(map, vrs);
            if (tk.hasMoreTokens())
            {
              vrs = tk.nextToken();
//              log.finest("s4 = " + vrs);
              if (vrs.startsWith("Configuration: "))
              {
                vrs = vrs.substring("Configuration: ".length());
              }
//              log.finest("s5 = " + vrs);
              J2MEWTkPropertyGroup.CONFIGURATION_VERSION.setValue(map, vrs);
              if (tk.hasMoreTokens())
              {
                vrs = tk.nextToken();
//                log.finest("s6 = " + vrs);
                if (vrs.startsWith("Optional: "))
                {
                  vrs = vrs.substring("Optional: ".length());
                }
//                log.finest("s7 = " + vrs);
                J2MEWTkPropertyGroup.OPTIONALS_VERSION.setValue(map, vrs);
              }
            }
          }
          // profile
          else if (string.startsWith("Profile: "))
          {
//            Map map = pathSet.getMap();
//            String vrs;
            tk = new StringTokenizer(string, "\n");
            vrs = tk.nextToken();
//            log.finest("s2 = " + vrs);
            if (vrs.startsWith("Profile: "))
            {
              vrs = vrs.substring("Profile: ".length());
            }
//            log.finest("s3 = " + vrs);
            J2MEWTkPropertyGroup.PROFILE_VERSION.setValue(map, vrs);
            if (tk.hasMoreTokens())
            {
              vrs = tk.nextToken();
//              log.finest("s4 = " + vrs);
              if (vrs.startsWith("Configuration: "))
              {
                vrs = vrs.substring("Configuration: ".length());
              }
//              log.finest("s5 = " + vrs);
              J2MEWTkPropertyGroup.CONFIGURATION_VERSION.setValue(map, vrs);
              if (tk.hasMoreTokens())
              {
                vrs = tk.nextToken();
//                log.finest("s6 = " + vrs);
                if (vrs.startsWith("Optional: "))
                {
                  vrs = vrs.substring("Optional: ".length());
                }
//                log.finest("s7 = " + vrs);
                J2MEWTkPropertyGroup.OPTIONALS_VERSION.setValue(map, vrs);
              }
            }
          }
          // config
          else if (string.startsWith("Configuration: "))
          {
//            Map map = pathSet.getMap();
//            String vrs;
            tk = new StringTokenizer(string, "\n");
            vrs = tk.nextToken();
//            log.finest("s4 = " + vrs);
            if (vrs.startsWith("Configuration: "))
            {
              vrs = vrs.substring("Configuration: ".length());
            }
//            log.finest("s5 = " + vrs);
            J2MEWTkPropertyGroup.CONFIGURATION_VERSION.setValue(map, vrs);
            if (tk.hasMoreTokens())
            {
              vrs = tk.nextToken();
//              log.finest("s6 = " + vrs);
              if (vrs.startsWith("Optional: "))
              {
                vrs = vrs.substring("Optional: ".length());
              }
//              log.finest("s7 = " + vrs);
              J2MEWTkPropertyGroup.OPTIONALS_VERSION.setValue(map, vrs);
            }
          }
          // optional
          else if (string.startsWith("Optional: "))
          {
//            Map map = pathSet.getMap();
//            String vrs;
            tk = new StringTokenizer(string, "\n");
            vrs = tk.nextToken();
//            log.finest("s6 = " + vrs);
            if (vrs.startsWith("Optional: "))
            {
              vrs = vrs.substring("Optional: ".length());
            }
//            log.finest("s7 = " + vrs);
            J2MEWTkPropertyGroup.OPTIONALS_VERSION.setValue(map, vrs);
          }

          finished = true;
        }
//        end();
      }

      public void javaProcessStarted(JavaProcess javaProcess)
      {
      }

      public void javaProcessStopped(JavaProcess javaProcess, int _int)
      {
        finished = true;
      }
    });
    com.borland.primetime.util.OSEnvironment os = new com.borland.primetime.util.OSEnvironment();
    process.start(JDKPathSet.getDefaultJDK().getHomePath().getFileObject(),
                  "com.sun.kvem.environment.EmulatorWrapper",
                  kenv + ";" + ktools,
                  "-Dkvem.home=" + homes,
                  "-version 0", os.getVariables(), home.getFileObject());

    while (!finished)
    {
      try
      {
        Thread.currentThread().sleep(100);
      }
      catch (InterruptedException ex)
      {
      }
    }
//    end();
    return ret;
  }
}


//    try
//    {
//      start();
//      ByteArrayOutputStream baos = new ByteArrayOutputStream();
//      PrintStream new_out = new PrintStream(baos);
//
//      URLClassLoader ucl = new URLClassLoader(new URL[]
//                                              {
//                                              kenv.toURL(),
//                                              ktools.toURL()
//      });
//
//      System.setProperty("kvem.home", homes);
//      System.setProperty("java.library.path", homes + "/bin/");
//
//
//      Class wtk = null;
//
//      try
//      {
//        wtk = ucl.loadClass("com.sun.kvem.environment.EmulatorWrapper");
//      }
//      catch (ClassNotFoundException ex1)
//      {
//        // ignore
//      }
//
//      // abort if no wrapper is found
//      if (wtk == null) return null;
//
//      System.setOut(new_out);
//
//      String[] args = new String[5];
/// /      Method m = wtk.getMethod("main", new Class[] { args.getClass() });
/// /      m.invoke(null, new Object[] { new String[] { "-version", "0" } });
//
//      Object emulator = wtk.newInstance(); // wtk1.4 fails here
//      System.setOut(new_out);
//
//      Method m = wtk.getMethod("main", new Class[] { args.getClass() });
//      m.invoke(null, new Object[] { new String[] { "-version", "0" } });
/// /      Method m = wtk.getMethod("run", new Class[]
/// /                               {args.getClass()});
/// /      m.invoke(emulator, new Object[]
/// /               {new String[]
/// /               {"-version"}
/// /      });
//      String output = baos.toString();
//      StringTokenizer tk = new StringTokenizer(output, "\n");
//      wtkversion = tk.nextToken();
//      System.setOut(old_out);
//      end();
//      return wtkversion.substring("J2ME Wireless Toolkit ".length());
//    }
//    catch (Exception ex)
//    {
//      end();
//      ex.printStackTrace();


//    }
//    catch (/*ExceptionInInitializer*/Error ex)
//    {
//      // wtk 1.4 throws an error
/// /      ex.printStackTrace();
//      // try to run java process
//      RunJavaProcess process = new RunJavaProcess();
//      System.setOut(old_out);
//      File java = JavaProcess.getJavaLauncher(JDKPathSet.getDefaultJDK().getHomePath().getFileObject());
/// /      process.setCommandLine(java + "-Dkvem.home="+home.getFileObject().toString() + " -cp \"" + kenv.toString() + "; " + ktools.toString() + " \" com.sun.kvem.environment.EmulatorWrapper -version 0");
/// /      System.out.println("cmd = " + process.getCommandLine());
//      //-Dkvem.home=c:\WTK\WTK104 -cp c:\WTK\WTK104/wtklib/kenv.zip;c:\WTK\WTK104/wtklib/ktools.zip com.sun.kvem.environment.EmulatorWrapper
//      process.addJavaProcessListener(new JavaProcessListener()
//      {
//        public void javaProcessPaused(JavaProcess javaProcess) {}
//        public void javaProcessResumed(JavaProcess javaProcess) {}
//        public void javaProcessSent(JavaProcess javaProcess, String string, boolean _boolean)
//        {
//          log.finest("s = " + string);
//          StringTokenizer tk = new StringTokenizer(string, "\n");
//          wtkversion = tk.nextToken();
//          wtkversion = wtkversion.substring("J2ME Wireless Toolkit ".length());
//          log.finest("s = " + wtkversion);
//          end();
//        }
//        public void javaProcessStarted(JavaProcess javaProcess) {}
//        public void javaProcessStopped(JavaProcess javaProcess, int _int) {}
//      });
//          com.borland.primetime.util.OSEnvironment os = new com.borland.primetime.util.OSEnvironment();
//      process.start(process.startJavaProcess(JDKPathSet.getDefaultJDK().getHomePath().getFileObject(), "com.sun.kvem.environment.EmulatorWrapper",
//                    kenv + ";" + ktools,
//                    "-Dkvem.home="+homes,
//                    "-version 0", os.getVariables(), home.getFileObject()));
//    }
//    finally
//    {
//      System.setOut(old_out);

