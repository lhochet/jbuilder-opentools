package lh.j2mewtk.midletwiz;

import java.awt.*;
import javax.swing.*;
import com.borland.primetime.wizard.*;
import com.borland.jbuilder.wizard.jbx.*;
import java.awt.event.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;
import com.borland.jbuilder.node.*;
import java.util.ResourceBundle;
import com.borland.jbuilder.ide.*;
import lh.j2mewtk.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:15 $
 * 0.2, 3/03/02
 * 0.1, 4/06/01
 */

public class J2MEWTkMidletAttributesWizardPage extends BasicWizardPage
{
  J2MEWtkMidletWizardClassPage classPage = null;
  //
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JTextField tfName = new JTextField();
  JTextField tfIcon = new JTextField();
  JButton btnIcon = new JButton();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  public J2MEWTkMidletAttributesWizardPage()
  {
    try
    {
      jbInit();
      this.setLargeIcon(null);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public J2MEWTkMidletAttributesWizardPage(J2MEWtkMidletWizardClassPage classPage)
  {
    this();
    this.classPage = classPage;
  }

  private void jbInit() throws Exception
  {
    this.setInstructions(WTKRes.getString("mltattrwiz.fillin"));
    this.setPageTitle(WTKRes.getString("mltattrwiz.midlet_attributes"));
    this.setLargeIcon(null);
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    jLabel1.setText(WTKRes.getString("mtlattrwiz.name"));
    jLabel2.setText(WTKRes.getString("mtlattrwiz.icon"));
    btnIcon.setText("...");
    btnIcon.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnIcon_actionPerformed(e);
      }
    });
    this.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 8, 0, 0), 5, 4));
    jPanel1.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 8, 189, 0), 14, 4));
    jPanel1.add(tfName, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10, 9, 0, 11), 167, 0));
    jPanel1.add(tfIcon, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 9, 189, 0), 140, 0));
    jPanel1.add(btnIcon, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 0, 189, 11), -19, -6));
  }
  public void activated(WizardHost host)
  {
    super.activated(host);
    String className = classPage.getClassName();
                tfName.setText(className);
//    StringBuffer buf = new StringBuffer(className);
//    buf.setCharAt(0, Character.toUpperCase(buf.charAt(0)));
//    tfName.setText(buf.toString());
  }

  void btnIcon_actionPerformed(ActionEvent e)
  {
    try
    {
      UrlChooser.setLastDir(getProjectDir());
      Url url = UrlChooser.promptForUrl(this, new Url(tfIcon.getText()));
      if (url != null) tfIcon.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForUrl(this, null);
      if (url != null) tfIcon.setText(url.getFullName());
    }
  }

  private Url getProjectDir()
  {
    JBProject prj = (JBProject)getWizardHost().getBrowser().getActiveProject();
    Url projectDir = prj.getUrl();
    projectDir = projectDir.getParent();
    return projectDir;
  }

  public String getName()
  {
    return tfName.getText();
  }

  public String getIconPath()
  {
    return tfIcon.getText();
  }

}
