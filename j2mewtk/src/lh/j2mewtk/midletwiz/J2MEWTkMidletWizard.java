package lh.j2mewtk.midletwiz;

import com.borland.primetime.*;
import com.borland.primetime.wizard.*;
import com.borland.jbuilder.wizard.jbx.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.help.*;
import com.borland.jbuilder.node.*;
import java.io.*;
import java.util.*;
import com.borland.primetime.util.*;
import lh.j2mewtk.*;
import com.borland.primetime.ide.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:15 $
 * 0.2, 3/03/02
 * 0.1, 4/06/01
 */

public class J2MEWTkMidletWizard extends ClassWizard
{
  public static final WizardAction J2MEWTK_MIDLET_WIZARD = new WizardAction (
    WTKRes.getString("mltwiz.midlet"), WTKRes.getString("mltwiz.single_char_3").charAt(0), WTKRes.getString("mltwiz.create_new_midlet"),
    JbxWizards.WIZARD_NewClass.getSmallIcon(),
    JbxWizards.WIZARD_NewClass.getLargeIcon(),
    true, WTKRes.getString("mltwiz.j2me_wtk"))
  {
    protected Wizard createWizard() {
      return new J2MEWTkMidletWizard();
    }
    /**
     * Returns the enabled state of the <code>Action</code>.
     *
     * @return true if this <code>Action</code> is enabled
     * @todo Implement this javax.swing.Action method
     */
    public boolean isEnabled()
    {
      Browser b = Browser.getActiveBrowser();
      if (b == null) return false;
      JBProject prj = (JBProject)b.getActiveProject();
      return J2MEWTkPropertyGroup.JAD_FILE.getValue(prj) != null;
//      return J2MEWTkPropertyGroup.isMobile(prj.getPaths().getJDKPathSet());
    }
  };

  J2MEWTkMidletAttributesWizardPage attributes = null;


  public J2MEWTkMidletWizard()
  {
    super(0);
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH J2MEWTkMidletWizard 0.5.1");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    WizardManager.registerWizardAction(J2MEWTK_MIDLET_WIZARD);
  }

  public String[] getSuperclassList()
  {
    return new String[] {"javax.microedition.midlet.MIDlet"};
  }
  public WizardPage invokeWizard(WizardHost host)
  {
    WizardPage wp = super.invokeWizard(host);
                initializeOptions();
                page = new J2MEWtkMidletWizardClassPage(this, options);
                addWizardPage(page);
                removeWizardPage(wp);
    host.setFinishEnabled(false);
    attributes = new J2MEWTkMidletAttributesWizardPage((J2MEWtkMidletWizardClassPage)page);
    addWizardPage(attributes);
    setWizardTitle(WTKRes.getString("mltwiz.create_wiz"));
    return page;
  }
//  public void generateBean(JBProject project, String packageName, String className, String superClass)
  public void generateBean(JBProject project, String packageName, String className, String superClass) throws VetoException
  {
    super.generateBean(project, packageName, className, superClass);
    updateJAD(project, packageName, className);
  }

  private void updateJAD(JBProject project, String packageName, String className)
  {
    String jadPath = null;
    try
    {
      jadPath = J2MEWTkPropertyGroup.JAD_FILE.getValue(project);
      FileNode jad = project.getNode(new Url(new File(jadPath)));

      Vector lines = new Vector();
      Vector midletsLines = new Vector();
      int nxtInd = 1;
      Buffer buffer = jad.getBuffer();
      BufferedReader reader = new BufferedReader(new StringReader(new String(buffer.getContent())));
      String ln = reader.readLine();
      while(ln != null)
      {
        if (ln.startsWith("MIDlet-"))
        {
          String sind = ln.substring(7, ln.indexOf(':')); // 7 = len(MIDlet-)
          try
          {
            int ind = Integer.parseInt(sind);
            if (ind >= nxtInd) nxtInd = ind + 1;
            midletsLines.add(ln);
          }
          catch (Exception ex)
          {
            lines.add(ln);
          }
        }
        else
        {
          lines.add(ln);
        };
        ln = reader.readLine();
      };

      String name = attributes.getName();
      if ((name == null) || (name.equals("")))
      {
//        String clsname = page.getClassNameEdit().getText();
//        StringBuffer buf = new StringBuffer(clsname);
//        buf.setCharAt(0, Character.toUpperCase(buf.charAt(0)));
//        name = buf.toString();
      }
      String icon = attributes.getIconPath();
      String fullClassName = packageName + "." + className;
      String line = "MIDlet-" + nxtInd + ": " + name + ", " + icon + ", " + fullClassName;
      midletsLines.add(line);


      StringWriter sw = new StringWriter();
      PrintWriter writer = new PrintWriter(sw);
      Iterator iter = midletsLines.iterator();
      while (iter.hasNext())
      {
        Object item = iter.next();
        writer.println(item);
      };
      iter = lines.iterator();
      while (iter.hasNext())
      {
        Object item = iter.next();
        writer.println(item);
      }
      writer.flush();
      buffer.setContent(sw.getBuffer().toString().getBytes());
      buffer.updateContent();
    }
    catch (Exception ex)
    {
//      System.err.println("jadPath : " + jadPath);
      ex.printStackTrace();
    }
  }
  public void help(WizardPage page, WizardHost host)
  {
    if (page == attributes)
    {
      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("mltwiz.MidletAttributesWiza")), host.getDialogParent());
      return;
    }
    super.help(page,  host);
  }
}
