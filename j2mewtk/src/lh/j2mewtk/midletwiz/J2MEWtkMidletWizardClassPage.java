package lh.j2mewtk.midletwiz;

import com.borland.jbuilder.ui.*;
import com.borland.jbuilder.wizard.common.*;
import com.borland.jbuilder.wizard.jbx.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:16 $
 * @created 3/03/02
 */

public class J2MEWtkMidletWizardClassPage extends BasicClassWizardPage
{

  public J2MEWtkMidletWizardClassPage(BeanGenerator beanGenerator, BeanOptions beanOptions)
  {
    super(beanGenerator, beanOptions);
  }

  public String getClassName()
  {
    return basicClassPane.getClassName();
  }

}
