package lh.j2mewtk;

import com.borland.jbuilder.paths.*;
import com.borland.primetime.help.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

public class WTKJDKPropertyPage extends JDKPropertyPage
{
  private JDKPathSet pathSet;
  private JLabel jLabel1 = new JLabel();
  private JTextField tfWTKVersion = new JTextField();
  private JLabel jLabel2 = new JLabel();
  private JTextField tfConfig = new JTextField();
  private JLabel jLabel3 = new JLabel();
  private JTextField tfProfile = new JTextField();
  private JPanel jPanel1 = new JPanel();
  private TitledBorder titledBorder1;
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JList lstOptional = new JList();

  public WTKJDKPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public WTKJDKPropertyPage(JDKPathSet pathSet)
  {
    this();
    this.pathSet = pathSet;
  }
  public void readProperties()
  {
    Map map = pathSet.getMap();
    tfWTKVersion.setText(J2MEWTkPropertyGroup.JDK_WTK_VERSION.getValue(map));
    tfProfile.setText(J2MEWTkPropertyGroup.PROFILE_VERSION.getValue(map));
    tfConfig.setText(J2MEWTkPropertyGroup.CONFIGURATION_VERSION.getValue(map));
    String optionals = J2MEWTkPropertyGroup.OPTIONALS_VERSION.getValue(map);
    DefaultListModel v = new DefaultListModel();
    StringTokenizer tk = new StringTokenizer(optionals, ",");
    while (tk.hasMoreTokens())
    {
      v.addElement(tk.nextToken());
    }
    lstOptional.setModel(v);
  }
  public void writeProperties()
  {
    Map map = pathSet.getMap();
    J2MEWTkPropertyGroup.PROFILE_VERSION.setValue(map, tfProfile.getText());
    J2MEWTkPropertyGroup.CONFIGURATION_VERSION.setValue(map,tfConfig.getText());
  }
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.WTKJDKPropertyPage"));
  }
  private void jbInit() throws Exception
  {
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("wtkproppg.Optionals_"));
    jLabel1.setText(WTKRes.getString("wtkproppg.WTK_Version_"));
    this.setLayout(gridBagLayout1);
    tfWTKVersion.setEditable(false);
    tfWTKVersion.setText("");
    tfWTKVersion.setColumns(5);
    jLabel2.setText(WTKRes.getString("wtkproppg.Configuration_"));
    jLabel3.setText(WTKRes.getString("wtkproppg.Profile_"));
    jPanel1.setBorder(titledBorder1);
    jPanel1.setLayout(borderLayout1);
    tfConfig.setText("");
    tfProfile.setText("");
    lstOptional.setBackground(UIManager.getColor("TextField.inactiveBackground"));
    lstOptional.setEnabled(true);
    jScrollPane1.setEnabled(true);
    this.add(jLabel1,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    this.add(tfWTKVersion,   new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    this.add(jLabel2,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    this.add(tfConfig,   new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    this.add(jLabel3,   new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    this.add(tfProfile,   new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    this.add(jPanel1,   new GridBagConstraints(0, 3, 2, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    jPanel1.add(jScrollPane1,  BorderLayout.CENTER);
    jScrollPane1.getViewport().add(lstOptional, null);
  }

}
