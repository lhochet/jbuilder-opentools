package lh.j2mewtk;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.borland.jbuilder.node.*;
import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:12 $
 * @created 16/05/01
 */

public class J2MEWTkProjectPropertyPage extends PropertyPage
{
  JBProject project = null;

  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JButton jarFileBrowseButton = new JButton();
  JLabel jadFileLabel = new JLabel();
  JButton jadFileBrowseButton = new JButton();
  JButton resDirBrowseButton = new JButton();
  JTextField resDirTextField = new JTextField();
  JLabel manifestFileLabel = new JLabel();
  JTextField jadFileTextField = new JTextField();
  JButton manifestFileBrowseButton = new JButton();
  JTextField manifestFileTextField = new JTextField();
  JLabel resDirLabel = new JLabel();
  JTextField jarFileTextField = new JTextField();
  JLabel preverDirLabel = new JLabel();
  JTextField preverDirTextField = new JTextField();
  JLabel jarFileLabel = new JLabel();
  JButton preverDirBrowseButton = new JButton();
  GridBagLayout gridBagLayout1 = new GridBagLayout();


  public J2MEWTkProjectPropertyPage(JBProject prj)
  {
    this();
    this.project = prj;
  }
  public void writeProperties()
  {
    J2MEWTkPropertyGroup.PREVERIFIED_DIR.setValue(project, preverDirTextField.getText());
    J2MEWTkPropertyGroup.RES_DIR.setValue(project, resDirTextField.getText());
    J2MEWTkPropertyGroup.JAD_FILE.setValue(project, jadFileTextField.getText());
    J2MEWTkPropertyGroup.JAR_FILE.setValue(project, jarFileTextField.getText());
    J2MEWTkPropertyGroup.MANIFEST_FILE.setValue(project, manifestFileTextField.getText());
  }
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.ProjectPropertyPage"));
  }
  public void readProperties()
  {
    String tmp;
    tmp = J2MEWTkPropertyGroup.PREVERIFIED_DIR.getValue(project);
    if (tmp != null) preverDirTextField.setText(tmp);
    tmp = J2MEWTkPropertyGroup.RES_DIR.getValue(project);
    if (tmp != null) resDirTextField.setText(tmp);
    tmp = J2MEWTkPropertyGroup.JAD_FILE.getValue(project);
    if (tmp != null) jadFileTextField.setText(tmp);
    tmp = J2MEWTkPropertyGroup.JAR_FILE.getValue(project);
    if (tmp != null) jarFileTextField.setText(tmp);
    tmp = J2MEWTkPropertyGroup.MANIFEST_FILE.getValue(project);
    if (tmp != null) manifestFileTextField.setText(tmp);
  }

  public J2MEWTkProjectPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    jarFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jarFileBrowseButton_actionPerformed(e);
      }
    });
    jarFileBrowseButton.setText("...");
    jadFileLabel.setText(WTKRes.getString("prjprops.jad_file"));
    jadFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jadFileBrowseButton_actionPerformed(e);
      }
    });
    jadFileBrowseButton.setText("...");
    resDirBrowseButton.setText("...");
    resDirBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resDirBrowseButton_actionPerformed(e);
      }
    });
    manifestFileLabel.setText(WTKRes.getString("prjprops.manifest_file"));
    manifestFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        manifestFileBrowseButton_actionPerformed(e);
      }
    });
    manifestFileBrowseButton.setText("...");
    resDirLabel.setText(WTKRes.getString("prjprops.resources_directory"));
    preverDirLabel.setText(WTKRes.getString("prjprops.preverified_directory"));
    jarFileLabel.setText(WTKRes.getString("prjprops.jar_file"));
    preverDirBrowseButton.setText("...");
    preverDirBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        preverDirBrowseButton_actionPerformed(e);
      }
    });
    this.add(jPanel1, BorderLayout.NORTH);
    jPanel1.add(manifestFileTextField, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(preverDirLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(8, 7, 0, 0), 5, 4));
    jPanel1.add(preverDirTextField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(8, 0, 0, 0), 201, 0));
    jPanel1.add(preverDirBrowseButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(8, 7, 0, 11), -18, -6));
    jPanel1.add(resDirBrowseButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
    jPanel1.add(resDirTextField, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(resDirLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 0), 5, 4));
    jPanel1.add(jadFileLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 63), 15, 4));
    jPanel1.add(jadFileTextField, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(jadFileBrowseButton, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
    jPanel1.add(jarFileBrowseButton, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
    jPanel1.add(jarFileTextField, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(jarFileLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 65), 13, 4));
    jPanel1.add(manifestFileLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 45), 7, 4));
    jPanel1.add(manifestFileBrowseButton, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
  }
  void preverDirBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      Url url = UrlChooser.promptForDir(this, new Url(preverDirTextField.getText()), WTKRes.getString("prjprops.choose_preverified"));
      if (url != null) preverDirTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForDir(this, null, WTKRes.getString("prjprops.choose_preverified"));
      if (url != null) preverDirTextField.setText(url.getFullName());
    }
  }
  void resDirBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      Url url = UrlChooser.promptForDir(this, new Url(resDirTextField.getText()), WTKRes.getString("prjprops.choose_resources"));
      if (url != null) resDirTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForDir(this, null, WTKRes.getString("prjprops.choose_resources"));
      if (url != null) resDirTextField.setText(url.getFullName());
    }
  }
  void jadFileBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      Url url = UrlChooser.promptForUrl(this, new Url(jadFileTextField.getText()));
      if (url != null) jadFileTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForUrl(this, null);
      if (url != null) jadFileTextField.setText(url.getFullName());
    }
  }
  void jarFileBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
    Url url = UrlChooser.promptForUrl(this, new Url(jarFileTextField.getText()));
    if (url != null) jarFileTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForUrl(this, null);
      if (url != null) jarFileTextField.setText(url.getFullName());
    }
  }
  void manifestFileBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      Url url = UrlChooser.promptForUrl(this, new Url(manifestFileTextField.getText()));
      if (url != null) manifestFileTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForUrl(this, null);
      if (url != null) manifestFileTextField.setText(url.getFullName());
    }
  }
}
