package lh.j2mewtk.prjwiz;

import java.awt.*;
import javax.swing.*;
import com.borland.primetime.wizard.*;
import com.borland.jbuilder.node.ui.*;
import com.borland.primetime.node.*;
import java.util.ResourceBundle;
import com.borland.jbuilder.node.*;
import java.util.*;
import lh.j2mewtk.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:16 $
 * @created 3/06/01
 */

public class J2MEWTkMainAttributesWizardPage extends BasicWizardPage
{
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JLabel jLabel3 = new JLabel();
  JLabel jLabel4 = new JLabel();
  JTextField tfName = new JTextField();
  JTextField tfVersion = new JTextField();
  JTextField tfDescription = new JTextField();
  JTextField tfVendor = new JTextField();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JLabel jLabel5 = new JLabel();
  JLabel jLabel6 = new JLabel();
  JTextField tfProfile = new JTextField();
  JTextField tfConfiguration = new JTextField();
  JPanel jPanel2 = new JPanel();

  public J2MEWTkMainAttributesWizardPage()
  {
    try
    {
      jbInit();
      this.setLargeIcon(null);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    this.setInstructions(WTKRes.getString("prjwizmainattr.enter_the_required"));
    this.setPageTitle(WTKRes.getString("prjwizmainattr.Midlet_Suite"));
    this.setLargeIcon(null);
    this.setPageStyle(0);
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    jLabel1.setText(WTKRes.getString("prjwizmainattr.Name_"));
    jLabel2.setText(WTKRes.getString("prjwizmainattr.Version_"));
    jLabel3.setText(WTKRes.getString("prjwizmainattr.Description_"));
    jLabel4.setText(WTKRes.getString("prjwizmainattr.Vendor_"));
    jLabel5.setRequestFocusEnabled(true);
    jLabel5.setText(WTKRes.getString("prjwizmainattr.MicroEdition-Profile"));
    jLabel6.setText(WTKRes.getString("prjwizmainattr.MicroEdition-Configuration"));
    tfProfile.setText("MIDP-1.0");
    tfConfiguration.setText("CLDC-1.0");
    this.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(tfName,     new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jLabel1,     new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(tfVersion,        new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 60, 0));
    jPanel1.add(tfDescription,     new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jLabel4,      new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jLabel3,     new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jLabel2,     new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(tfVendor,      new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jLabel5,      new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jLabel6,    new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(tfProfile,   new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(tfConfiguration,   new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jPanel2,    new GridBagConstraints(0, 6, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
  }
  public void activated(WizardHost host)
  {
    super.activated(host);
    host.setFinishEnabled(true);

    JBProject prj = (JBProject)host.getBrowser().getActiveProject();
    String prjname = prj.getDisplayName();
    prjname = prjname.substring(0, prjname.indexOf("."));
    String vendor = GeneralProjectPropertyPage.getJavaDocHeaderValue(prj, GeneralProjectPropertyPage.INDEX_JAVADOC_COMPANY);
//    String vendor = getWizardHost().getBrowser().getActiveProject().getProperty("Company");
    if ((vendor == null) || (vendor.equals("")))
    {
      vendor = GeneralProjectPropertyPage.getJavaDocHeaderValue(prj, GeneralProjectPropertyPage.INDEX_JAVADOC_AUTHOR);
//      vendor = getWizardHost().getBrowser().getActiveProject().getProperty("Author[0]");
      if ((vendor == null) || (vendor.equals("")))
      {
        vendor = WTKRes.getString("prjwizmainattr.Unknown");
      }
    }

    String txt = null;
    txt = tfName.getText();
    if ((txt == null) || (txt.equals(""))) tfName.setText(prjname);
    txt = tfVersion.getText();
    if ((txt == null) || (txt.equals(""))) tfVersion.setText(J2MEWTkPropertyGroup.WIZARD_MIDLET_VERSION.getValue());
    txt = tfVendor.getText();
    if ((txt == null) || (txt.equals(""))) tfVendor.setText(vendor);

    Map map = prj.getPaths().getJDKPathSet().getMap();
    String config = J2MEWTkPropertyGroup.CONFIGURATION_VERSION.getValue(map);
    if (config != null)
    {
      tfConfiguration.setText(config);
    }
    String profile = J2MEWTkPropertyGroup.PROFILE_VERSION.getValue(map);
    if (config != null)
    {
      tfProfile.setText(profile);
    }
  }
  public void checkPage() throws com.borland.primetime.util.VetoException
  {
    super.checkPage();
    String txt = null;
    txt = tfName.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Required_Fiel_Name"));
    txt = tfVersion.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Required_Fiel_Version"));
    txt = tfVendor.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Required_Fiel_Vendor"));
  }
  public String getMidletName()
  {
    return tfName.getText();
  }
  public String getVersion()
  {
    return tfVersion.getText();
  }
  public String getDescription()
  {
    return tfDescription.getText();
  }
  public String getVendor()
  {
    return tfVendor.getText();
  }
  public String getProfile()
  {
    return tfProfile.getText();
  }
  public String getConfiguration()
  {
    return tfConfiguration.getText();
  }
}
