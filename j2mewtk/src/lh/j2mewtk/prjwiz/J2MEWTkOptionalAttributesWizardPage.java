package lh.j2mewtk.prjwiz;

import com.borland.primetime.wizard.*;
import java.awt.*;
import javax.swing.*;
import java.util.ResourceBundle;
import lh.j2mewtk.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:16 $
 * @created 3/06/01
 */

public class J2MEWTkOptionalAttributesWizardPage extends BasicWizardPage
{
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JLabel jLabel3 = new JLabel();
  JLabel jLabel4 = new JLabel();
  JLabel jLabel5 = new JLabel();
  JTextField tfIcon = new JTextField();
  JTextField tfInfoURL = new JTextField();
  JTextField tfDataSize = new JTextField();
  JTextField tfInstallNotifyURL = new JTextField();
  JTextField tfDeleteConfirm = new JTextField();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  public J2MEWTkOptionalAttributesWizardPage()
  {
    try
    {
      jbInit();
      this.setLargeIcon(null);
   }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    this.setInstructions(WTKRes.getString("prjwizoptattr.Optionaly_enter_the"));
    this.setPageTitle(WTKRes.getString("prjwizoptattr.Other_Attributes"));
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    jLabel1.setText(WTKRes.getString("prjwizoptattr.Icon_"));
    jLabel2.setText(WTKRes.getString("prjwizoptattr.Info_URL_"));
    jLabel3.setText(WTKRes.getString("prjwizoptattr.Data_size_"));
    jLabel4.setText(WTKRes.getString("prjwizoptattr.Install_notifiy_URL_"));
    jLabel5.setText(WTKRes.getString("prjwizoptattr.Delete_confirm_"));
    this.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 66), 14, 4));
    jPanel1.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 10, 0, 55), 2, 4));
    jPanel1.add(tfIcon, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 0, 13), 161, 0));
    jPanel1.add(tfInfoURL, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 13), 161, 0));
    jPanel1.add(tfDataSize, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 13), 161, 0));
    jPanel1.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 10, 0, 51), 1, 4));
    jPanel1.add(jLabel4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 10, 0, 0), 7, 4));
    jPanel1.add(tfInstallNotifyURL, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 13), 161, 0));
    jPanel1.add(jLabel5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 10, 99, 20), 1, 4));
    jPanel1.add(tfDeleteConfirm, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 99, 13), 161, 0));
  }
  public String getIcon()
  {
    return tfIcon.getText();
  }
  public String getInfoURL()
  {
    return tfInfoURL.getText();
  }
  public String getDataSize()
  {
    return tfDataSize.getText();
  }
  public String getInstallNotifyURL()
  {
    return tfInstallNotifyURL.getText();
  }
  public String getDeleteConfirm()
  {
    return tfDeleteConfirm.getText();
  }
}
