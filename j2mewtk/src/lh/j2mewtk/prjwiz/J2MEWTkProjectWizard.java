package lh.j2mewtk.prjwiz;

import java.io.*;
import java.util.*;

import com.borland.jbuilder.node.*;
//import com.borland.jbuilder.wizard.project.*;
import com.borland.jbuilder.wizard.newproject.*;
import com.borland.primetime.*;
import com.borland.primetime.help.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.wizard.*;

import lh.j2mewtk.*;
import com.borland.primetime.ide.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:16 $
 * @created 26/05/01
 */

public class J2MEWTkProjectWizard extends BasicWizard
{
  public static final WizardAction J2MEWTK_PROJECT_WIZARD = new WizardAction (
    WTKRes.getString("prjwiz.Project"), WTKRes.getString("prjwiz.single_char_4").charAt(0), WTKRes.getString("prjwiz.Create_a_new_J2ME"),
    ProjectWizard.WIZARD_NewJBProject.getSmallIcon(),
    ProjectWizard.WIZARD_NewJBProject.getLargeIcon(),
    true, WTKRes.getString("prjwiz.J2ME_Wireless_Toolkit"))
  {
    protected Wizard createWizard() {
      return new J2MEWTkProjectWizard();
    }

    /**
     * Returns the enabled state of the <code>Action</code>.
     *
     * @return true if this <code>Action</code> is enabled
     */
    public boolean isEnabled()
    {
//      if (true) return true;
      Browser b = Browser.getActiveBrowser();
      if (b == null) return false;
      JBProject prj = (JBProject)b.getActiveProject();
      return J2MEWTkPropertyGroup.isMobile(prj.getPaths().getJDKPathSet()) &&
        J2MEWTkPropertyGroup.JAD_FILE.getValue(prj) == null;
    }
  };

  private J2MEWTkProjectWizardPage postProject = new J2MEWTkProjectWizardPage();
  private J2MEWTkMainAttributesWizardPage infoAttributes = new J2MEWTkMainAttributesWizardPage();
  private J2MEWTkOptionalAttributesWizardPage optionalAttributes = new J2MEWTkOptionalAttributesWizardPage();


  public J2MEWTkProjectWizard()
  {
    super();
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH J2MEWTkProjectWizard 0.4");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    WizardManager.registerWizardAction(J2MEWTK_PROJECT_WIZARD);
  }

  public WizardPage invokeWizard(WizardHost host)
  {
//    addWizardPage(newJBProject);
    addWizardPage(postProject);
    addWizardPage(infoAttributes);
    addWizardPage(optionalAttributes);
    setWizardTitle(WTKRes.getString("prjwiz.J2ME_Wireless_Toolkit1"));
    return postProject;
//    return newJBProject;
  }
  public WizardPage next(WizardPage page, WizardHost host) throws com.borland.primetime.util.VetoException
  {
//    if (page == newJBProject)
//    {
//      if (newJBProject.isCreate())
//      {
//        if (created)
//        {
//          throw new com.borland.primetime.util.VetoException(res.getString("Project_already"));
//        }
//        else
//        {
//          Project preprj = host.getBrowser().getActiveProject();
//          ProjectWizard.WIZARD_NewJBProject.invokeWizard(host.getBrowser());
//
//          Project prj = host.getBrowser().getActiveProject();
//
//          if (preprj == prj)
//          {
//            throw new com.borland.primetime.util.VetoException(res.getString("Create_a_new_project2"));
//          }
//          created = true;
//          newJBProject.setEnableCreate(false);
//        }
//        return postProject;
//      }
//    }
    if (page == postProject)
    {
      host.setFinishEnabled(true);
      return infoAttributes;
    }
    if (page == infoAttributes)
    {
      return optionalAttributes;
    }
    return super.next(page, host);
  }

  public WizardPage finish(WizardPage page, WizardHost host) throws com.borland.primetime.util.VetoException
  {
    WizardPage pg = super.finish(page,  host);
    if (pg == null)
    {
      JBProject project = (JBProject)host.getBrowser().getActiveProject();
      String preverDirPath = postProject.getPreverDirPath();
      String resDirPath = postProject.getResDirPath();
      String jadPath = postProject.getJadPath();
      String jarPath = postProject.getJarPath();
      String manifestPath = postProject.getManifestPath();

//      try
//      {
        // add files
        FileNode jad = project.getNode(new Url(new File(jadPath)));
        jad.setParent(project);

        FileNode jar = project.getNode(new Url(new File(jarPath)));
        jar.setParent(project);
        try
        {
          jar.getUrl().getFileObject().createNewFile();
        }
        catch (IOException ex)
        {
          // just ignore
        }


        FileNode manifest = project.getNode(new Url(new File(manifestPath)));
        manifest.setParent(project);

        // create dirs
        File dir = new File(resDirPath);
        dir.mkdirs();
        dir = new File(preverDirPath);
        dir.mkdirs();

        // set jdk
        // chg, does not override the current jdk when adding wtk support to the current project
//        project.getPaths().setJDKPathSet(project.getPaths().getJDK(J2MEWTkPropertyGroup.NEW_PROJECTS_JDK_NAME.getValue()));

        // set props
        J2MEWTkPropertyGroup.PREVERIFIED_DIR.setValue(project, preverDirPath);
        J2MEWTkPropertyGroup.RES_DIR.setValue(project, resDirPath);
        J2MEWTkPropertyGroup.JAD_FILE.setValue(project, jadPath);
        J2MEWTkPropertyGroup.JAR_FILE.setValue(project, jarPath);
        J2MEWTkPropertyGroup.MANIFEST_FILE.setValue(project, manifestPath);

        // init files
        initJAD(jad, jar);
        initManifest(manifest, jar);
//      }
//      catch (Exception ex)
//      {
//        ex.printStackTrace();
//      }

    }
    return pg;
  }

  private void initJAD(FileNode jad, FileNode jar)
  {
    try
    {
      Buffer buffer = jad.getBuffer();
      StringWriter sw = new StringWriter();
      PrintWriter writer = new PrintWriter(sw);
      String desc = infoAttributes.getDescription();
      if (!desc.equals("")) writer.println("MIDlet-Description: " + desc);
      writer.println("MIDlet-Jar-Size: 0");
      writer.println("MIDlet-Jar-URL: " + jar.getDisplayName());
      writer.println("MIDlet-Name: " + infoAttributes.getMidletName());
      writer.println("MIDlet-Vendor: " + infoAttributes.getVendor());
      writer.println("MIDlet-Version: " + infoAttributes.getVersion());
      // Optionals
      String txt = null;
      txt = optionalAttributes.getIcon();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Icon: " + txt);
      txt = optionalAttributes.getInfoURL();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Info-URL: " + txt);
      txt = optionalAttributes.getDataSize();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Data-Size: " + txt);
      txt = optionalAttributes.getInstallNotifyURL();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Install-Notify: " + txt);
      txt = optionalAttributes.getDeleteConfirm();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Delete-Confirm: " + txt);

      writer.flush();
      buffer.setContent(sw.getBuffer().toString().getBytes());
    }
    catch (IOException ioex) {ioex.printStackTrace();}
  }

  private void initManifest(FileNode manifest, FileNode jar)
  {
    try
    {
      String jarname = jar.getDisplayName();
      Buffer buffer = manifest.getBuffer();
      StringWriter sw = new StringWriter();
      PrintWriter writer = new PrintWriter(sw);
      String desc = infoAttributes.getDescription();
      if (!desc.equals("")) writer.println("MIDlet-Description: " + desc);
      writer.println("MIDlet-Jar-URL: " + jarname);
      writer.println("MIDlet-Name: " + infoAttributes.getMidletName());
      writer.println("MIDlet-Vendor: " + infoAttributes.getVendor());
      writer.println("MIDlet-Version: " + infoAttributes.getVersion());
      writer.println("MicroEdition-Configuration: " + infoAttributes.getConfiguration());
      writer.println("MicroEdition-Profile: " + infoAttributes.getProfile());
      // Optionals
      String txt = null;
      txt = optionalAttributes.getIcon();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Icon: " + txt);
      txt = optionalAttributes.getInfoURL();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Info-URL: " + txt);
      txt = optionalAttributes.getDataSize();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Data-Size: " + txt);
      txt = optionalAttributes.getInstallNotifyURL();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Install-Notify: " + txt);
      txt = optionalAttributes.getDeleteConfirm();
      if ( !( (txt == null) || (txt.equals("")) ) ) writer.println("MIDlet-Delete-Confirm: " + txt);

      writer.flush();
      buffer.setContent(sw.getBuffer().toString().getBytes());
    }
    catch (IOException ioex) {ioex.printStackTrace();}
  }
  public void help(WizardPage page, WizardHost host)
  {
//    if (page == newJBProject)
//    {
//      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), res2.getString("NewJBProject_html")), host.getDialogParent());
//      return;
//    }
    if (page == postProject)
    {
      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.PostProject_html")), host.getDialogParent());
      return;
    }
    if (page == infoAttributes)
    {
      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.InfoAttributes_html")), host.getDialogParent());
      return;
    }
    if (page == optionalAttributes)
    {
      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.OptionalAttributes")), host.getDialogParent());
      return;
    }
    super.help(page,  host);
  }

}
