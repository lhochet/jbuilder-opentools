package lh.j2mewtk.prjwiz;

import com.borland.primetime.wizard.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;
import com.borland.jbuilder.node.*;
import java.util.ResourceBundle;
import lh.j2mewtk.*;


/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:16 $
 * @created 27/05/01
 */

public class J2MEWTkProjectWizardPage extends BasicWizardPage
{
  Url projectDir = null;

  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JButton jarFileBrowseButton = new JButton();
  JLabel jadFileLabel = new JLabel();
  JButton jadFileBrowseButton = new JButton();
  JTextField resDirTextField = new JTextField();
  JButton resDirBrowseButton = new JButton();
  JLabel manifestFileLabel = new JLabel();
  JTextField jadFileTextField = new JTextField();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JButton manifestFileBrowseButton = new JButton();
  JLabel resDirLabel = new JLabel();
  JTextField manifestFileTextField = new JTextField();
  JTextField jarFileTextField = new JTextField();
  JLabel preverDirLabel = new JLabel();
  JLabel jarFileLabel = new JLabel();
  JTextField preverDirTextField = new JTextField();
  JButton preverDirBrowseButton = new JButton();

  public J2MEWTkProjectWizardPage()
  {
    try
    {
      jbInit();
      this.setLargeIcon(null);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    jPanel1.setLayout(gridBagLayout1);
    this.setInstructions(WTKRes.getString("prjwizpg.If_needed_adjust_the"));
    this.setPageTitle(WTKRes.getString("prjwizpg.J2ME_Wireless_Toolkit2"));
    this.setLayout(borderLayout1);
    jarFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jarFileBrowseButton_actionPerformed(e);
      }
    });
    jarFileBrowseButton.setText("...");
    jadFileLabel.setText(WTKRes.getString("prjwizpg.JAD_file_"));
    jadFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jadFileBrowseButton_actionPerformed(e);
      }
    });
    jadFileBrowseButton.setText("...");
    resDirBrowseButton.setText("...");
    resDirBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resDirBrowseButton_actionPerformed(e);
      }
    });
    manifestFileLabel.setText(WTKRes.getString("prjwizpg.Manifest_file_"));
    manifestFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        manifestFileBrowseButton_actionPerformed(e);
      }
    });
    manifestFileBrowseButton.setText("...");
    resDirLabel.setText(WTKRes.getString("prjwizpg.Resources_directory_"));
    preverDirLabel.setText(WTKRes.getString("prjwizpg.Preverified_directory"));
    jarFileLabel.setText(WTKRes.getString("prjwizpg.JAR_file_"));
    preverDirBrowseButton.setText("...");
    preverDirBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        preverDirBrowseButton_actionPerformed(e);
      }
    });
    this.add(jPanel1, BorderLayout.NORTH);
    jPanel1.add(manifestFileTextField, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(preverDirLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(8, 7, 0, 0), 5, 4));
    jPanel1.add(preverDirTextField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(8, 0, 0, 0), 201, 0));
    jPanel1.add(preverDirBrowseButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(8, 7, 0, 11), -18, -6));
    jPanel1.add(resDirBrowseButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
    jPanel1.add(resDirTextField, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(resDirLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 0), 5, 4));
    jPanel1.add(jadFileLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 63), 15, 4));
    jPanel1.add(jadFileTextField, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(jadFileBrowseButton, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
    jPanel1.add(jarFileBrowseButton, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
    jPanel1.add(jarFileTextField, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 0, 0, 0), 201, 0));
    jPanel1.add(jarFileLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 65), 13, 4));
    jPanel1.add(manifestFileLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(9, 7, 0, 45), 7, 4));
    jPanel1.add(manifestFileBrowseButton, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(9, 7, 0, 11), -18, -6));
  }
  void jarFileBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      UrlChooser.setLastDir(getProjectDir());
      Url url = UrlChooser.promptForUrl(this, new Url(jarFileTextField.getText()));
      if (url != null) jarFileTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForUrl(this, null);
      if (url != null) jarFileTextField.setText(url.getFullName());
    }
  }
  void jadFileBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      UrlChooser.setLastDir(getProjectDir());
      Url url = UrlChooser.promptForUrl(this, new Url(jadFileTextField.getText()));
      if (url != null) jadFileTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForUrl(this, null);
      if (url != null) jadFileTextField.setText(url.getFullName());
    }
  }
  void resDirBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      UrlChooser.setLastDir(getProjectDir());
      Url url = UrlChooser.promptForDir(this, new Url(resDirTextField.getText()), WTKRes.getString("prjwizpg.Choose_resources"));
      if (url != null) resDirTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForDir(this, null, WTKRes.getString("prjwizpg.Choose_resources"));
      if (url != null) resDirTextField.setText(url.getFullName());
    }
  }
  void manifestFileBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      UrlChooser.setLastDir(getProjectDir());
      Url url = UrlChooser.promptForUrl(this, new Url(manifestFileTextField.getText()));
      if (url != null) manifestFileTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForUrl(this, null);
      if (url != null) manifestFileTextField.setText(url.getFullName());
    }
  }
  void preverDirBrowseButton_actionPerformed(ActionEvent e)
  {
    try
    {
      UrlChooser.setLastDir(getProjectDir());
      Url url = UrlChooser.promptForDir(this, new Url(preverDirTextField.getText()), WTKRes.getString("prjwizpg.Choose_preverfied"));
      if (url != null) preverDirTextField.setText(url.getFullName());
    }
    catch (InvalidUrlException iuex)
    {
      Url url = UrlChooser.promptForDir(this, null, WTKRes.getString("prjwizpg.Choose_preverfied"));
      if (url != null) preverDirTextField.setText(url.getFullName());
    }
  }

  private Url getProjectDir()
  {
    if (projectDir != null) return projectDir;
    JBProject prj = (JBProject)getWizardHost().getBrowser().getActiveProject();
    projectDir = prj.getUrl();
    projectDir = projectDir.getParent();
    return projectDir;
  }

  public void activated(WizardHost host)
  {
    super.activated(host);
    String prjdirtxt = getProjectDir().getFullName() + "/";
    String prjname = getWizardHost().getBrowser().getActiveProject().getDisplayName();
    prjname = prjname.substring(0, prjname.indexOf("."));

    String txt = null;
    txt = resDirTextField.getText();
    if ((txt == null) || (txt.equals(""))) resDirTextField.setText(prjdirtxt + "res");

    txt = preverDirTextField.getText();
    if ((txt == null) || (txt.equals(""))) preverDirTextField.setText(prjdirtxt + "prever");

    txt = jadFileTextField.getText();
    if ((txt == null) || (txt.equals(""))) jadFileTextField.setText(prjdirtxt + prjname + ".jad");

    txt = jarFileTextField.getText();
    if ((txt == null) || (txt.equals(""))) jarFileTextField.setText(prjdirtxt + prjname + ".jar");

    txt = manifestFileTextField.getText();
    if ((txt == null) || (txt.equals(""))) manifestFileTextField.setText(prjdirtxt + prjname + ".mf");
  }

  public void checkPage() throws com.borland.primetime.util.VetoException
  {
    super.checkPage();
    String txt = null;
    txt = resDirTextField.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Empty_field_"));
    txt = preverDirTextField.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Empty_field_"));
    txt = jadFileTextField.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Empty_field_"));
    txt = jarFileTextField.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Empty_field_"));
    txt = manifestFileTextField.getText();
    if ((txt == null) || (txt.equals(""))) throw new com.borland.primetime.util.VetoException(WTKRes.getString("Empty_field_"));
  }

  public String getResDirPath()
  {
    return resDirTextField.getText();
  }

  public String getPreverDirPath()
  {
    return preverDirTextField.getText();
  }

  public String getJadPath()
  {
    return jadFileTextField.getText();
  }

  public String getJarPath()
  {
    return jarFileTextField.getText();
  }

  public String getManifestPath()
  {
    return manifestFileTextField.getText();
  }

}
