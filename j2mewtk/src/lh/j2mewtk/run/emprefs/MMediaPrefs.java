package lh.j2mewtk.run.emprefs;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import com.borland.primetime.properties.*;
import lh.j2mewtk.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:18 $
 * @created 10/07/04
 */

public class MMediaPrefs extends EmulatorPreferencesPage
{
  private final static String TRUE = "true";
  private final static String FALSE = " false";

  private static final MapBooleanProperty MM_WAV = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mm_wav");
  private static final MapBooleanProperty MM_MIDI = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mm_midi");
  private static final MapBooleanProperty MM_VIDEO = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mm_video");
  private static final MapBooleanProperty MM_AUDIO_MIXING = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mm_audio_mixing");
  private static final MapBooleanProperty MM_AUDIO_CAPTURE = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mm_audio_capture");
  private static final MapBooleanProperty MM_AUDIO_RECORD = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mm_audio_record");
  private static final MapBooleanProperty MM_MIDI_TONES = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mm_midi_tones");

  private final String WTK_WAV_MM = "mm.format.wav";
  private final String WTK_MIDI_MM = "mm.format.midi";
  private final String WTK_VIDEO_MM = "mm.format.video";
  private final String WTK_AUDIO_MIXING_MM = "mm.control.mixing";
  private final String WTK_AUDIO_CAPTURE_MM = "mm.control.capture";
  private final String WTK_AUDIO_RECORD_MM = "mm.control.record";
  private final String WTK_MIDI_TONES_MM = "mm.control.midi";

  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private TitledBorder titledBorder1;
  private GridLayout gridLayout1 = new GridLayout();
  private JCheckBox cbxWAV = new JCheckBox();
  private JCheckBox cbxMIDI = new JCheckBox();
  private JCheckBox cbxVideo = new JCheckBox();
  private TitledBorder titledBorder2;
  private GridLayout gridLayout2 = new GridLayout();
  private JCheckBox cbxAudioMixing = new JCheckBox();
  private JCheckBox cbxAudioRecord = new JCheckBox();
  private JCheckBox cbxAudioCapture = new JCheckBox();
  private JCheckBox cbxMIDITones = new JCheckBox();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JPanel jPanel3 = new JPanel();
  public MMediaPrefs()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public boolean isSet(Map map)
  {
    return MM_WAV.getBoolean(map)
    || MM_MIDI.getBoolean(map)
    || MM_VIDEO.getBoolean(map)
    || MM_AUDIO_MIXING.getBoolean(map)
    || MM_AUDIO_CAPTURE.getBoolean(map)
    || MM_AUDIO_RECORD.getBoolean(map)
    || MM_MIDI_TONES.getBoolean(map);
  }
  public void updateProperties(Map map, Properties props)
  {
    props.put(WTK_WAV_MM, MM_WAV.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_MIDI_MM, MM_MIDI.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_VIDEO_MM, MM_VIDEO.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_AUDIO_MIXING_MM, MM_AUDIO_MIXING.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_AUDIO_CAPTURE_MM, MM_AUDIO_CAPTURE.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_AUDIO_RECORD_MM, MM_AUDIO_RECORD.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_MIDI_TONES_MM, MM_MIDI_TONES.getBoolean(map) ? TRUE : FALSE);
  }
  public void writeProperties(Map map)
  {
    MM_WAV.setBoolean(map, cbxWAV.isSelected());
    MM_MIDI.setBoolean(map, cbxMIDI.isSelected());
    MM_VIDEO.setBoolean(map, cbxVideo.isSelected());
    MM_AUDIO_MIXING.setBoolean(map, cbxAudioMixing.isSelected());
    MM_AUDIO_CAPTURE.setBoolean(map, cbxAudioCapture.isSelected());
    MM_AUDIO_RECORD.setBoolean(map, cbxAudioRecord.isSelected());
    MM_MIDI_TONES.setBoolean(map, cbxMIDITones.isSelected());
  }
  public void readProperties(Map map)
  {
    cbxWAV.setSelected(MM_WAV.getBoolean(map));
    cbxMIDI.setSelected(MM_MIDI.getBoolean(map));
    cbxVideo.setSelected(MM_VIDEO.getBoolean(map));
    cbxAudioMixing.setSelected(MM_AUDIO_MIXING.getBoolean(map));
    cbxAudioCapture.setSelected(MM_AUDIO_CAPTURE.getBoolean(map));
    cbxAudioRecord.setSelected(MM_AUDIO_RECORD.getBoolean(map));
    cbxMIDITones.setSelected(MM_MIDI_TONES.getBoolean(map));
  }

  public String getName()
  {
    return WTKRes.getString("emprefs.mmedia.name");
  }
  private void jbInit() throws Exception
  {
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.mmedia.supported_format"));
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.mmedia.supported_optional_features"));
    this.setLayout(gridBagLayout1);
    jPanel1.setBorder(titledBorder1);
    jPanel1.setLayout(gridLayout1);
    jPanel2.setBorder(titledBorder2);
    jPanel2.setLayout(gridLayout2);
    cbxWAV.setText(WTKRes.getString("emprefs.mmedia.wav"));
    cbxMIDI.setText(WTKRes.getString("emprefs.mmedia.midi"));
    cbxVideo.setText(WTKRes.getString("emprefs.mmedia.video"));
    gridLayout2.setColumns(2);
    gridLayout2.setRows(2);
    cbxAudioMixing.setText(WTKRes.getString("emprefs.mmedia.audio_mixing"));
    cbxAudioRecord.setText(WTKRes.getString("emprefs.mmedia.audio_record"));
    cbxAudioCapture.setText(WTKRes.getString("emprefs.mmedia.audio_capture"));
    cbxMIDITones.setText(WTKRes.getString("emprefs.mmedia.midi_tones"));
    this.add(jPanel1,   new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 0, 0));
    jPanel1.add(cbxWAV, null);
    jPanel1.add(cbxMIDI, null);
    jPanel1.add(cbxVideo, null);
    this.add(jPanel2,   new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 0, 0));
    jPanel2.add(cbxAudioMixing, null);
    jPanel2.add(cbxAudioCapture, null);
    jPanel2.add(cbxAudioRecord, null);
    jPanel2.add(cbxMIDITones, null);
    this.add(jPanel3,   new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
  }

}
