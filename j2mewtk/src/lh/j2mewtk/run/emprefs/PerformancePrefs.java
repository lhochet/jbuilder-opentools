package lh.j2mewtk.run.emprefs;

import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.borland.primetime.properties.*;

import lh.j2mewtk.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:19 $
 * @created 10/07/04
 */

public class PerformancePrefs extends EmulatorPreferencesPage
{
  private final static String TRUE = "true";
  private final static String FALSE = " false";
  private final static String DEFAULT = "default";
  private final static String IMMEDIATE = "immediate";
  private final static String PERIODIC = "periodic";

  private static final MapBooleanProperty PERF_ENABLE_NET = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "perf_enable_net");
  private static final MapProperty PERF_NET_SPEED = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "perf_net_speed");
  private static final MapBooleanProperty PERF_ENABLE_VM = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "perf_enable_vm");
  private static final MapProperty PERF_VM_SPEED = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "perf_vm_speed");
  private static final MapProperty PERF_GRAPHICS_LATENCY = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "perf_graphics_latency");
  private static final MapProperty PERF_REFRESH_MODE = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "perf_refresh_mode");
  private static final MapProperty PERF_REFRESH_RATE = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "perf_refresh_rate");

  private final String WTK_ENABLE_NET_PERF = "netspeed.enableSpeedEmulation";
  private final String WTK_NET_SPEED_PERF = "netspeed.bitpersecond";
  private final String WTK_ENABLE_VM_PERF = "vmspeed.enableEmulation";
  private final String WTK_VM_SPEED_PERF = "vmspeed.bytecodespermilli";
  private final String WTK_GRAPHICS_LATENCY_PERF = "screen.graphicsLatency";
  private final String WTK_REFRESH_MODE_PERF = "screen.refresh.mode";
  private final String WTK_REFRESH_RATE_PERF = "screen.refresh.rate";

  private JPanel jPanel1 = new JPanel();
  private TitledBorder titledBorder1;
  private JCheckBox cbxEnableNetwork = new JCheckBox();
  private JComboBox cbNetworkBits = new JComboBox();
  private JLabel jLabel1 = new JLabel();
  private JPanel jPanel2 = new JPanel();
  private TitledBorder titledBorder2;
  private JCheckBox cbxEnableVMSpeed = new JCheckBox();
  private JSlider jsVMSpeed = new JSlider();
  private JLabel jLabel2 = new JLabel();
  private JPanel jPanel3 = new JPanel();
  private TitledBorder titledBorder3;
  private JSlider jsGraphicsLatency = new JSlider();
  private JLabel jLabel3 = new JLabel();
  private JLabel jLabel4 = new JLabel();
  private JLabel lblPeriod = new JLabel();
  private JLabel lblFramesSec = new JLabel();
  private JSlider jsRefreshPeriod = new JSlider();
  private JLabel jLabel7 = new JLabel();
  private JRadioButton rDefault = new JRadioButton();
  private JRadioButton rImmediate = new JRadioButton();
  private JRadioButton rPeriodic = new JRadioButton();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private GridBagLayout gridBagLayout4 = new GridBagLayout();
  private JPanel jPanel4 = new JPanel();
  private ButtonGroup bgDisplayRefresh = new ButtonGroup();
  public PerformancePrefs()
  {
    try
    {
      preJBInit();
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * preJBInit
   */
  private void preJBInit()
  {
    cbNetworkBits.addItem("1200");
    cbNetworkBits.addItem("2400");
    cbNetworkBits.addItem("9600");
    cbNetworkBits.addItem("14400");
    cbNetworkBits.addItem("19200");
    cbNetworkBits.addItem("28800");
    cbNetworkBits.addItem("33600");
    cbNetworkBits.addItem("56000");
    cbNetworkBits.addItem("112000");
  }

  public boolean isSet(Map map)
  {
    if (PERF_ENABLE_NET.getBoolean(map)) return true;
    if (PERF_NET_SPEED.getValue(map) != null) return true;
    if (PERF_ENABLE_VM.getBoolean(map)) return true;
    if (PERF_VM_SPEED.getValue(map) != null) return true;
    if (PERF_GRAPHICS_LATENCY.getValue(map) != null) return true;
    String val = PERF_REFRESH_MODE.getValue(map);
    if ((val != null) && (!val.equals(DEFAULT))) return true;
    if (PERF_REFRESH_RATE.getValue(map) != null) return true;
    return false;
  }
  public void updateProperties(Map map, Properties props)
  {
    String val;

    if (PERF_ENABLE_NET.getBoolean(map)) props.setProperty(WTK_ENABLE_NET_PERF, PERF_ENABLE_NET.getBoolean(map) ? TRUE : FALSE);
    val = PERF_NET_SPEED.getValue(map);
    if (val != null) props.setProperty(WTK_NET_SPEED_PERF, val);

    if (PERF_ENABLE_VM.getBoolean(map)) props.setProperty(WTK_ENABLE_VM_PERF, PERF_ENABLE_VM.getBoolean(map) ? TRUE : FALSE);
    val = PERF_VM_SPEED.getValue(map);
    if (val != null) props.setProperty(WTK_VM_SPEED_PERF, val);

    val = PERF_GRAPHICS_LATENCY.getValue(map);
    if (val != null) props.setProperty(WTK_GRAPHICS_LATENCY_PERF, val);
    val = PERF_REFRESH_MODE.getValue(map);
    if ((val != null) && (!val.equals(DEFAULT))) props.setProperty(WTK_REFRESH_MODE_PERF, val);
    val = PERF_REFRESH_RATE.getValue(map);
    if (val != null) props.setProperty(WTK_REFRESH_RATE_PERF, val);
  }
  public void writeProperties(Map map)
  {
    PERF_ENABLE_NET.setBoolean(map, cbxEnableNetwork.isSelected());
    PERF_NET_SPEED.setValue(map, (String)cbNetworkBits.getSelectedItem());
    PERF_ENABLE_VM.setBoolean(map, cbxEnableVMSpeed.isSelected());
    PERF_VM_SPEED.setValue(map, Integer.toString(jsVMSpeed.getValue()));
    PERF_GRAPHICS_LATENCY.setValue(map, Integer.toString(jsGraphicsLatency.getValue()));
    String val = DEFAULT;
    if (rImmediate.isSelected())
    {
      val = IMMEDIATE;
    }
    else if (rPeriodic.isSelected())
    {
      val = PERIODIC;
    }
    PERF_REFRESH_MODE.setValue(map, val);
    PERF_REFRESH_RATE.setValue(map, Integer.toString(jsRefreshPeriod.getValue()));
  }
  public void readProperties(Map map)
  {
    String val;
    int ival;
    cbxEnableNetwork.setSelected(PERF_ENABLE_NET.getBoolean(map));
    cbNetworkBits.setSelectedItem(PERF_NET_SPEED.getValue(map));
    cbNetworkBits.setEnabled(cbxEnableNetwork.isSelected());

    cbxEnableVMSpeed.setSelected(PERF_ENABLE_VM.getBoolean(map));
    val = PERF_VM_SPEED.getValue(map);
    try
    {
      ival = Integer.parseInt(val);
    }
    catch (NumberFormatException ex)
    {
      ival = 0;
    }
    jsVMSpeed.setValue(ival);
    jsVMSpeed.setEnabled(cbxEnableVMSpeed.isSelected());

    val = PERF_GRAPHICS_LATENCY.getValue(map);
    try
    {
      ival = Integer.parseInt(val);
    }
    catch (NumberFormatException ex)
    {
      ival = 0;
    }
    jsGraphicsLatency.setValue(ival);

    val = PERF_REFRESH_MODE.getValue(map);
    if (val == null)
    {
      rDefault.setSelected(true);
      enablePeriod(false);
    }
    else if (val.equals(DEFAULT))
    {
      rDefault.setSelected(true);
      enablePeriod(false);
    }
    else if (val.equals(IMMEDIATE))
    {
      rImmediate.setSelected(true);
      enablePeriod(false);
    }
    else if (val.equals(PERIODIC))
    {
      rPeriodic.setSelected(true);
      enablePeriod(true);
    }
    val = PERF_REFRESH_RATE.getValue(map);
    try
    {
      ival = Integer.parseInt(val);
    }
    catch (NumberFormatException ex)
    {
      ival = 30;
    }
    jsRefreshPeriod.setValue(ival);
  }

  public String getName()
  {
    return WTKRes.getString("emprefs.perf.name");
  }
  private void jbInit() throws Exception
  {
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.perf.network_title"));
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.perf.vm_speed_title"));
    titledBorder3 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.perf.graphics_title"));
    this.setLayout(gridBagLayout4);
    jPanel1.setBorder(titledBorder1);
    jPanel1.setLayout(gridBagLayout1);
    cbxEnableNetwork.setText(WTKRes.getString("emprefs.perf.enable_network"));
    cbxEnableNetwork.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cbxEnableNetwork_actionPerformed(e);
      }
    });
    jLabel1.setText(WTKRes.getString("emprefs.perf.bits_sec"));
    jPanel2.setBorder(titledBorder2);
    jPanel2.setLayout(gridBagLayout2);
    cbxEnableVMSpeed.setText(WTKRes.getString("emprefs.perf.enable_vm"));
    cbxEnableVMSpeed.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cbxEnableVMSpeed_actionPerformed(e);
      }
    });
    jsVMSpeed.setLabelTable(null);
    jsVMSpeed.setMajorTickSpacing(450);
    jsVMSpeed.setMaximum(1000);
    jsVMSpeed.setMinimum(100);
    jsVMSpeed.setMinorTickSpacing(100);
    jsVMSpeed.setPaintLabels(true);
    jsVMSpeed.setPaintTicks(true);
    jsVMSpeed.setPaintTrack(true);
    jLabel2.setText(WTKRes.getString("emprefs.perf.bytecode_ms"));
    jPanel3.setBorder(titledBorder3);
    jPanel3.setLayout(gridBagLayout3);
    jsGraphicsLatency.setMajorTickSpacing(10);
    jsGraphicsLatency.setMaximum(50);
    jsGraphicsLatency.setMinorTickSpacing(5);
    jsGraphicsLatency.setPaintLabels(true);
    jsGraphicsLatency.setPaintTicks(true);
    jLabel3.setText(WTKRes.getString("emprefs.perf.latency"));
    jLabel4.setText(WTKRes.getString("emprefs.perf.ms"));
    lblPeriod.setText(WTKRes.getString("emprefs.perf.period"));
    lblFramesSec.setText(WTKRes.getString("emprefs.perf.frames_sec"));
    jsRefreshPeriod.setPaintTicks(true);
    jsRefreshPeriod.setPaintLabels(true);
    jsRefreshPeriod.setMinorTickSpacing(5);
    jsRefreshPeriod.setMaximum(30);
    jsRefreshPeriod.setMajorTickSpacing(10);
    jLabel7.setText(WTKRes.getString("emprefs.perf.display_refresh"));
    rDefault.setBorder(null);
    rDefault.setText(WTKRes.getString("emprefs.perf.double_buffer"));
    rDefault.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rDefault_actionPerformed(e);
      }
    });
    rImmediate.setBorder(null);
    rImmediate.setBorderPainted(false);
    rImmediate.setText(WTKRes.getString("emprefs.perf.immediate"));
    rImmediate.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rImmediate_actionPerformed(e);
      }
    });
    rPeriodic.setBorder(null);
    rPeriodic.setText(WTKRes.getString("emprefs.perf.periodic"));
    rPeriodic.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rPeriodic_actionPerformed(e);
      }
    });
    cbNetworkBits.setEditable(true);
    this.add(jPanel1,   new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 0, 0));
    jPanel1.add(cbxEnableNetwork,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    jPanel1.add(cbNetworkBits,   new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 0), 59, 0));
    jPanel1.add(jLabel1,   new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    this.add(jPanel2,    new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 0, 0));
    jPanel2.add(cbxEnableVMSpeed,    new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 5, 0));
    jPanel2.add(jsVMSpeed,    new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 0), 0, 0));
    jPanel2.add(jLabel2,     new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(7, 5, 5, 5), 0, 0));
    this.add(jPanel3,    new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
    jPanel3.add(jLabel3,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel3.add(jsGraphicsLatency,     new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    jPanel3.add(jLabel4,     new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel3.add(jLabel7,      new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(17, 5, 0, 0), 0, 0));
    jPanel3.add(rImmediate,     new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(15, 5, 0, 0), 0, 0));
    jPanel3.add(rPeriodic,       new GridBagConstraints(3, 1, 2, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(15, 5, 0, 0), 0, 0));
    jPanel3.add(lblPeriod,    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
    jPanel3.add(lblFramesSec,    new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    jPanel3.add(rDefault,     new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(15, 5, 0, 0), 0, 0));
    jPanel3.add(jsRefreshPeriod,    new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 0), 0, 0));
    this.add(jPanel4,   new GridBagConstraints(0, 3, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    bgDisplayRefresh.add(rPeriodic);
    bgDisplayRefresh.add(rImmediate);
    bgDisplayRefresh.add(rDefault);
  }

  void rDefault_actionPerformed(ActionEvent e)
  {
    enablePeriod(false);
  }

  void rImmediate_actionPerformed(ActionEvent e)
  {
    enablePeriod(false);
  }

  void rPeriodic_actionPerformed(ActionEvent e)
  {
    enablePeriod(true);
  }

  private void enablePeriod(boolean enable)
  {
    lblPeriod.setEnabled(enable);
    jsRefreshPeriod.setEnabled(enable);
    lblFramesSec.setEnabled(enable);
  }

  void cbxEnableNetwork_actionPerformed(ActionEvent e)
  {
    cbNetworkBits.setEnabled(cbxEnableNetwork.isSelected());
  }

  void cbxEnableVMSpeed_actionPerformed(ActionEvent e)
  {
    jsVMSpeed.setEnabled(cbxEnableVMSpeed.isSelected());
  }
}
