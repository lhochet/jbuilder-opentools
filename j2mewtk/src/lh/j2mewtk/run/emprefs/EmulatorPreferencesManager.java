package lh.j2mewtk.run.emprefs;

import java.util.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:18 $
 * @created 26/06/04
 */

public class EmulatorPreferencesManager
{
  private static EmulatorPreferencesManager instance = null;
  public static EmulatorPreferencesManager getInstance()
  {
    if (instance == null) instance = new EmulatorPreferencesManager();
    return instance;
  }

  Vector pages = new Vector();

  private EmulatorPreferencesManager()
  {
  }

  public Vector getPages()
  {
    if (pages.isEmpty())
    {
      pages.add(new HttpPrefs());
      pages.add(new MonitorPrefs());
      pages.add(new PerformancePrefs());
      pages.add(new StoragePrefs());
      pages.add(new MMediaPrefs());
      pages.add(new WMAPrefs());
    }
    return pages;
  }

}
