package lh.j2mewtk.run.emprefs;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import com.borland.primetime.properties.*;
import lh.j2mewtk.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:19 $
 * @created 10/07/04
 */

public class StoragePrefs extends EmulatorPreferencesPage
{
  private static final MapProperty STO_ROOT = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "storage_root");
  private static final MapProperty STO_SZ = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "storage_size");

  private final String WTK_ROOT_STO = "storage.root";
  private final String WTK_SZ_STO = "storage.size";

  private JLabel jLabel1 = new JLabel();
  private JTextField tfRootDirectory = new JTextField();
  private JLabel jLabel2 = new JLabel();
  private JTextField tfStorageSize = new JTextField();
  private JLabel jLabel3 = new JLabel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JPanel jPanel1 = new JPanel();
  public StoragePrefs()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public boolean isSet(Map map)
  {
    String val = STO_ROOT.getValue(map);
    if ((val != null) && (!val.equals(""))) return true;
    val = STO_SZ.getValue(map);
    if ((val != null) && (!val.equals(""))) return true;
    return false;
  }
  public void updateProperties(Map map, Properties props)
  {
    String val = STO_ROOT.getValue(map);
    if ((val != null) && (!val.equals(""))) props.setProperty(WTK_ROOT_STO, val);
    val = STO_SZ.getValue(map);
    if ((val != null) && (!val.equals(""))) props.setProperty(WTK_SZ_STO, val);
  }
  public void writeProperties(Map map)
  {
    STO_ROOT.setValue(map, tfRootDirectory.getText());
    STO_SZ.setValue(map, tfStorageSize.getText());
  }
  public void readProperties(Map map)
  {
    String val = STO_ROOT.getValue(map);
    if (val != null) tfRootDirectory.setText(val);
    val = STO_SZ.getValue(map);
    if (val != null) tfStorageSize.setText(val);
  }

  public String getName()
  {
    return WTKRes.getString("emprefs.sto.name");
  }
  private void jbInit() throws Exception
  {
    jLabel1.setText(WTKRes.getString("emprefs.sto.root"));
    this.setLayout(gridBagLayout1);
    jLabel2.setText(WTKRes.getString("emprefs.sto.sz"));
    tfStorageSize.setText("");
    tfStorageSize.setColumns(5);
    jLabel3.setText(WTKRes.getString("emprefs.sto.kd"));
    tfRootDirectory.setText("");
    this.add(jLabel1,    new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    this.add(tfRootDirectory,    new GridBagConstraints(2, 0, 2, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(8, 5, 0, 10), 0, 0));
    this.add(jLabel2,       new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(12, 10, 0, 0), 0, 0));
    this.add(tfStorageSize,        new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 5, 0, 0), 0, 0));
    this.add(jLabel3,     new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(12, 5, 0, 0), 0, 0));
    this.add(jPanel1,   new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
  }

}
