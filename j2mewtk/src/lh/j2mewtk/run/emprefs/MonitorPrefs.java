package lh.j2mewtk.run.emprefs;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import com.borland.primetime.properties.*;
import lh.j2mewtk.*;
import javax.swing.border.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:19 $
 * @created 10/07/04
 */

public class MonitorPrefs extends EmulatorPreferencesPage
{
  private final static String TRUE = "true";
  private final static String FALSE = " false";

  private static final MapBooleanProperty MON_MEMORY = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_memory");
  private static final MapBooleanProperty MON_COMM = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_comm");
  private static final MapBooleanProperty MON_DATAGRAM = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_datagram");
  private static final MapBooleanProperty MON_HTTP = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_http");
  private static final MapBooleanProperty MON_HTTPS = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_https");
  private static final MapBooleanProperty MON_SOCKET = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_socket");
  private static final MapBooleanProperty MON_SSL = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_ssl");
  private static final MapBooleanProperty MON_PROFILING = new MapBooleanProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "mon_profiling");

  private final String WTK_MEMORY_MON = "kvem.memory.monitor.enable";
  private final String WTK_COMM_MON = "kvem.netmon.comm.enable";
  private final String WTK_DATAGRAM_MON = "kvem.netmon.datagram.enable";
  private final String WTK_HTTP_MON = "kvem.netmon.http.enable";
  private final String WTK_HTTPS_MON = "kvem.netmon.https.enable";
  private final String WTK_SOCKET_MON = "kvem.netmon.socket.enable";
  private final String WTK_SSL_MON = "kvem.netmon.ssl.enable";
  private final String WTK_PROFILER = "kvem.profiler.enable";

  private JPanel jPanel1 = new JPanel();
  private JCheckBox cbxMemory = new JCheckBox();
  private JCheckBox cbxComm = new JCheckBox();
  private JCheckBox cbxDatagram = new JCheckBox();
  private JCheckBox cbxHTTP = new JCheckBox();
  private JCheckBox cbxHTTPS = new JCheckBox();
  private JCheckBox cbxSocket = new JCheckBox();
  private JCheckBox cbxSSL = new JCheckBox();
  private JCheckBox cbxProfiler = new JCheckBox();
  private GridLayout gridLayout1 = new GridLayout();
  private JPanel jPanel2 = new JPanel();
  private Border border1;
  private TitledBorder titledBorder1;
  private TitledBorder titledBorder2;
  private FlowLayout flowLayout1 = new FlowLayout();
  private FlowLayout flowLayout2 = new FlowLayout();
  private JPanel jPanel3 = new JPanel();
  private Border border2;
  private TitledBorder titledBorder3;
  private TitledBorder titledBorder4;
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JPanel jPanel4 = new JPanel();
  public MonitorPrefs()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public boolean isSet(Map map)
  {
    return MON_MEMORY.getBoolean(map)
    || MON_COMM.getBoolean(map)
    || MON_DATAGRAM.getBoolean(map)
    || MON_HTTP.getBoolean(map)
    || MON_HTTPS.getBoolean(map)
    || MON_SOCKET.getBoolean(map)
    || MON_SSL.getBoolean(map)
    || MON_PROFILING.getBoolean(map);
  }
  public void updateProperties(Map map, Properties props)
  {
    props.put(WTK_MEMORY_MON, MON_MEMORY.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_COMM_MON, MON_COMM.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_DATAGRAM_MON, MON_DATAGRAM.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_HTTP_MON, MON_HTTP.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_HTTPS_MON, MON_HTTPS.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_SOCKET_MON, MON_SOCKET.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_SSL_MON, MON_SSL.getBoolean(map) ? TRUE : FALSE);
    props.put(WTK_PROFILER, MON_PROFILING.getBoolean(map) ? TRUE : FALSE);
  }
  public void writeProperties(Map map)
  {
    MON_MEMORY.setBoolean(map, cbxMemory.isSelected());
    MON_COMM.setBoolean(map, cbxComm.isSelected());
    MON_DATAGRAM.setBoolean(map, cbxDatagram.isSelected());
    MON_HTTP.setBoolean(map, cbxHTTP.isSelected());
    MON_HTTPS.setBoolean(map, cbxHTTPS.isSelected());
    MON_SOCKET.setBoolean(map, cbxSocket.isSelected());
    MON_SSL.setBoolean(map, cbxSSL.isSelected());
    MON_PROFILING.setBoolean(map, cbxProfiler.isSelected());
  }
  public void readProperties(Map map)
  {
    cbxMemory.setSelected(MON_MEMORY.getBoolean(map));
    cbxComm.setSelected(MON_COMM.getBoolean(map));
    cbxDatagram.setSelected(MON_DATAGRAM.getBoolean(map));
    cbxHTTP.setSelected(MON_HTTP.getBoolean(map));
    cbxHTTPS.setSelected(MON_HTTPS.getBoolean(map));
    cbxSocket.setSelected(MON_SOCKET.getBoolean(map));
    cbxSSL.setSelected(MON_SSL.getBoolean(map));
    cbxProfiler.setSelected(MON_PROFILING.getBoolean(map));
  }

  public String getName()
  {
    return WTKRes.getString("emprefs.mon.name");
  }
  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(SystemColor.controlText,1);
    titledBorder1 = new TitledBorder(border1,WTKRes.getString("emprefs.mon.memory"));
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.mon.memory"));
    border2 = BorderFactory.createLineBorder(new Color(153, 153, 153),1);
    titledBorder3 = new TitledBorder(border2,WTKRes.getString("emprefs.mon.profiler"));
    titledBorder4 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.mon.network"));
    cbxMemory.setBorder(null);
    cbxMemory.setText(WTKRes.getString("emprefs.mon.mem_mon"));
    jPanel1.setLayout(gridLayout1);
    cbxComm.setText(WTKRes.getString("emprefs.mon.com_mon"));
    cbxDatagram.setText(WTKRes.getString("emprefs.mon.dat_mon"));
    cbxHTTP.setText(WTKRes.getString("emprefs.mon.http_mon"));
    cbxHTTPS.setText(WTKRes.getString("emprefs.mon.https_mon"));
    cbxSocket.setText(WTKRes.getString("emprefs.mon.sock_mon"));
    cbxSSL.setText(WTKRes.getString("emprefs.mon.ssl_mon"));
    cbxProfiler.setBorder(null);
    cbxProfiler.setText(WTKRes.getString("emprefs.mon.profiling"));
    gridLayout1.setColumns(1);
    gridLayout1.setRows(0);
    gridLayout1.setVgap(5);
    this.setLayout(gridBagLayout1);
    jPanel1.setBorder(titledBorder4);
    jPanel2.setBorder(titledBorder2);
    jPanel2.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    flowLayout1.setHgap(5);
    flowLayout1.setVgap(0);
    flowLayout2.setVgap(0);
    flowLayout2.setHgap(5);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    jPanel3.setLayout(flowLayout2);
    jPanel3.setBorder(titledBorder3);
    jPanel1.add(cbxComm, null);
    jPanel1.add(cbxDatagram, null);
    jPanel1.add(cbxHTTP, null);
    jPanel1.add(cbxHTTPS, null);
    jPanel1.add(cbxSocket, null);
    jPanel1.add(cbxSSL, null);
    this.add(jPanel3,     new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 10, 5, 10), 0, 0));
    jPanel3.add(cbxProfiler, null);
    this.add(jPanel4,  new GridBagConstraints(0, 3, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    this.add(jPanel2,     new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 5, 10), 0, 0));
    jPanel2.add(cbxMemory, null);
    this.add(jPanel1,    new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 10, 5, 10), 0, 0));
  }

}
