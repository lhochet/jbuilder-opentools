package lh.j2mewtk.run.emprefs;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import com.borland.primetime.properties.*;
import lh.j2mewtk.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:19 $
 * @created 10/07/04
 */

public class WMAPrefs extends EmulatorPreferencesPage
{
  private static final MapProperty WMA_NEXT_PHONE = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "wma_nextnumber");
  private static final MapProperty WMA_FIRST_PHONE = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "wma_firstnumber");
  private static final MapProperty WMA_SMSC_PHONE = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "wma_smscnumber");
  private static final MapProperty WMA_FRAGMENT_LOSS = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "wma_fragmentloss");
  private static final MapProperty WMA_DELIVERY_DELAY = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "wma_deliverydelay");

  private final String WTK_NEXT_PHONE_WMA = "wma.client.phonenumber";
  private final String WTK_FIRST_PHONE_WMA = "wma.server.firstAssignedPhoneNumber";
  private final String WTK_SMSC_PHONE_WMA = "wma.smsc.phoneNumber";
  private final String WTK_FRAGMENT_LOSS_WMA = "wma.server.percentFragmentLoss";
  private final String WTK_DELIVERY_DELAY_WMA = "wma.server.deliveryDelayMS";

  private JLabel jLabel1 = new JLabel();
  private JTextField tfFirstNumber = new JTextField();
  private JLabel jLabel2 = new JLabel();
  private JTextField tfNextNumber = new JTextField();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private TitledBorder titledBorder1;
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JPanel jPanel3 = new JPanel();
  private TitledBorder titledBorder2;
  private JLabel jLabel3 = new JLabel();
  private JTextField tfDelay = new JTextField();
  private JPanel jPanel4 = new JPanel();
  private JLabel jLabel4 = new JLabel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private JPanel jPanel5 = new JPanel();
  private GridLayout gridLayout1 = new GridLayout();
  private JLabel jLabel5 = new JLabel();
  private JSlider jsRandomLoss = new JSlider();
  private JLabel jLabel6 = new JLabel();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private JPanel jPanel6 = new JPanel();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private JLabel jLabel7 = new JLabel();
  private JTextField tfSMSCNumber = new JTextField();
  public WMAPrefs()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public boolean isSet(Map map)
  {
    if (WMA_NEXT_PHONE.getValue(map) != null) return true;
    if (WMA_FIRST_PHONE.getValue(map) != null) return true;
    if (WMA_SMSC_PHONE.getValue(map) != null) return true;
    if (WMA_FRAGMENT_LOSS.getValue(map) != null) return true;
    if (WMA_DELIVERY_DELAY.getValue(map) != null) return true;
    return false;
  }
  public void updateProperties(Map map, Properties props)
  {
    String val = WMA_NEXT_PHONE.getValue(map);
    if ((val != null) && (!val.equals(""))) props.setProperty(WTK_NEXT_PHONE_WMA, val);
    val = WMA_FIRST_PHONE.getValue(map);
    if ((val != null) && (!val.equals(""))) props.setProperty(WTK_FIRST_PHONE_WMA, val);
    val = WMA_SMSC_PHONE.getValue(map);
    if ((val != null) && (!val.equals(""))) props.setProperty(WTK_SMSC_PHONE_WMA, val);
    val = WMA_FRAGMENT_LOSS.getValue(map);
    if ((val != null) && (!val.equals(""))) props.setProperty(WTK_FRAGMENT_LOSS_WMA, val);
    val = WMA_DELIVERY_DELAY.getValue(map);
    if ((val != null) && (!val.equals(""))) props.setProperty(WTK_DELIVERY_DELAY_WMA, val);
  }
  public void writeProperties(Map map)
  {
    String val = tfNextNumber.getText();
    if (!val.equals("")) WMA_NEXT_PHONE.setValue(map, val);
    val = tfFirstNumber.getText();
    if (!val.equals("")) WMA_FIRST_PHONE.setValue(map, val);
    val = tfSMSCNumber.getText();
    if (!val.equals("")) WMA_SMSC_PHONE.setValue(map, val);
    val = tfDelay.getText();
    if (!val.equals("")) WMA_DELIVERY_DELAY.setValue(map, val);
    WMA_FRAGMENT_LOSS.setValue(map, "" + jsRandomLoss.getValue());
  }
  public void readProperties(Map map)
  {
    tfNextNumber.setText(WMA_NEXT_PHONE.getValue(map));
    tfFirstNumber.setText(WMA_FIRST_PHONE.getValue(map));
    tfSMSCNumber.setText(WMA_SMSC_PHONE.getValue(map));
    tfDelay.setText(WMA_DELIVERY_DELAY.getValue(map));
    int ival = 0;
    try
    {
      ival = Integer.parseInt(WMA_FRAGMENT_LOSS.getValue(map));
    }
    catch (Exception ex)
    {
    }
    jsRandomLoss.setValue(ival);
  }

  public String getName()
  {
    return WTKRes.getString("emprefs.wma.name");
  }
  private void jbInit() throws Exception
  {
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.wma.numbers_title"));
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.wma.msg_delivery_title"));
    jLabel1.setText(WTKRes.getString("emprefs.wma.first_number"));
    this.setLayout(gridBagLayout3);
    jLabel2.setText(WTKRes.getString("emprefs.wma.next_number"));
    jPanel1.setBorder(titledBorder1);
    jPanel1.setLayout(gridBagLayout1);
    jPanel3.setBorder(titledBorder2);
    jPanel3.setLayout(gridLayout1);
    jLabel3.setText(WTKRes.getString("emprefs.wma.fragment_delivery_delay"));
    tfDelay.setText("");
    tfDelay.setColumns(5);
    jPanel4.setLayout(flowLayout1);
    jLabel4.setText(WTKRes.getString("emprefs.wma.ms"));
    flowLayout1.setAlignment(FlowLayout.LEFT);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(0);
    jLabel5.setText(WTKRes.getString("emprefs.wma.random_loss"));
    jPanel5.setLayout(gridBagLayout2);
    jLabel6.setText(WTKRes.getString("emprefs.wma.percent"));
    jsRandomLoss.setMajorTickSpacing(10);
    jsRandomLoss.setMinorTickSpacing(5);
    jsRandomLoss.setPaintLabels(true);
    jsRandomLoss.setPaintTicks(true);
    jPanel6.setOpaque(false);
    tfFirstNumber.setText("");
    tfFirstNumber.setColumns(15);
    tfNextNumber.setText("");
    tfNextNumber.setColumns(15);
    jLabel7.setText(WTKRes.getString("emprefs.wma.smsc_number"));
    tfSMSCNumber.setText("");
    this.add(jPanel1,   new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 0, 0));
    jPanel1.add(tfFirstNumber,        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jLabel1,          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jLabel2,         new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(tfNextNumber,        new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jPanel2,      new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(tfSMSCNumber,     new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jLabel7,    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    this.add(jPanel3,   new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 10, 0, 10), 0, 0));
    jPanel3.add(jPanel4, null);
    jPanel4.add(jLabel3, null);
    jPanel4.add(tfDelay, null);
    jPanel4.add(jLabel4, null);
    jPanel3.add(jPanel5, null);
    jPanel5.add(jLabel5,    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    jPanel5.add(jsRandomLoss,   new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
    jPanel5.add(jLabel6,    new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    this.add(jPanel6,   new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
  }

}
