package lh.j2mewtk.run.emprefs;

import java.util.*;
import javax.swing.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:18 $
 * @created 26/06/04
 */

public abstract class EmulatorPreferencesPage extends JPanel
{
  public abstract String getName();
  public abstract void readProperties(Map map);
  public abstract void writeProperties(Map map);
  public abstract boolean isSet(Map map);
  public abstract void updateProperties(Map map, Properties props);
}
