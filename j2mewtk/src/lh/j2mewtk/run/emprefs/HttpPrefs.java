package lh.j2mewtk.run.emprefs;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import com.borland.primetime.properties.*;

import lh.j2mewtk.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:18 $
 * @created 10/07/04
 */

public class HttpPrefs extends EmulatorPreferencesPage
{
  private static final MapProperty HTTP_VERSION = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "http_version");
  private static final MapProperty HTTP_HOST = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "http_host2");
  private static final MapProperty HTTP_PORT = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "http_port2");
  private static final MapProperty HTTPS_HOST = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "https_host");
  private static final MapProperty HTTPS_PORT = new MapProperty(J2MEWTkPropertyGroup.JB_RUNTIME_CAT, "https_port");

  private final String WTK_HTTP_VERSION = "http.version";
  private final String WTK_HTTP_HOST = "http.proxyHost";
  private final String WTK_HTTP_PORT = "http.proxyPort";
  private final String WTK_HTTPS_HOST = "https.proxyHost";
  private final String WTK_HTTPS_PORT = "https.proxyPort";

  private JPanel jPanel1 = new JPanel();
  private JRadioButton rHttp10 = new JRadioButton();
  private JRadioButton rHttp11 = new JRadioButton();
  private GridLayout gridLayout1 = new GridLayout();
  private ButtonGroup httpVersionGroup = new ButtonGroup();
  private TitledBorder titledBorder1;
  private JPanel jPanel2 = new JPanel();
  private JLabel jLabel1 = new JLabel();
  private JLabel jLabel2 = new JLabel();
  private JTextField tfHTTPHost = new JTextField();
  private JTextField tfHTTPPort = new JTextField();
  private TitledBorder titledBorder2;
  private JLabel jLabel3 = new JLabel();
  private JTextField tfHTTPSPort = new JTextField();
  private JLabel jLabel4 = new JLabel();
  private JTextField tfHTTPSHost = new JTextField();
  private JPanel jPanel3 = new JPanel();
  private Border border1;
  private TitledBorder titledBorder3;
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private JPanel jPanel4 = new JPanel();
  private JPanel jPanel5 = new JPanel();
  private JPanel jPanel6 = new JPanel();

  public HttpPrefs()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.http.http_version"));
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),1),WTKRes.getString("emprefs.http.http_proxy"));
    border1 = BorderFactory.createLineBorder(new Color(153, 153, 153),1);
    titledBorder3 = new TitledBorder(border1, WTKRes.getString("emprefs.http.https_proxy"));
    this.setLayout(gridBagLayout1);
    jPanel1.setBorder(titledBorder1);
    jPanel1.setLayout(gridLayout1);
    rHttp10.setText("1.0");
    rHttp11.setText("1.1");
    gridLayout1.setColumns(1);
    gridLayout1.setHgap(5);
    gridLayout1.setRows(2);
    gridLayout1.setVgap(5);
    jPanel2.setBorder(titledBorder2);
    jPanel2.setLayout(gridBagLayout2);
    jLabel1.setText(WTKRes.getString("emprefs.http.http_host"));
    jLabel2.setText(WTKRes.getString("emprefs.http.http_port"));
    jLabel3.setText(WTKRes.getString("emprefs.http.https_port"));
    jLabel4.setText(WTKRes.getString("emprefs.http.https_host"));
    jPanel3.setLayout(gridBagLayout3);
    jPanel3.setBorder(titledBorder3);
    tfHTTPHost.setText("");
    tfHTTPPort.setText("");
    tfHTTPSHost.setText("");
    this.add(jPanel1,       new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 10, 0, 10), 0, 0));
    jPanel1.add(rHttp10, null);
    jPanel1.add(rHttp11, null);
    httpVersionGroup.add(rHttp10);
    httpVersionGroup.add(rHttp11);
    this.add(jPanel2,     new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 0, 0));
    jPanel2.add(jLabel1,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(tfHTTPHost,   new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel2.add(tfHTTPPort,   new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel2.add(jLabel2,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(jPanel5,  new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    this.add(jPanel3,     new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 10, 0, 10), 0, 0));
    jPanel3.add(jLabel4,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel3.add(tfHTTPSHost,   new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel3.add(tfHTTPSPort,   new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel3.add(jLabel3,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel3.add(jPanel6,  new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    this.add(jPanel4,   new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
  }

  public void readProperties(Map map)
  {
//    String vrs = (String)map.get(HTTP_VERSION);
    String vrs = HTTP_VERSION.getValue(map);
    if (vrs != null)
    {
      if (vrs.equals("1.0"))
      {
        rHttp10.setSelected(true);
      }
      else if (vrs.equals("1.1"))
      {
        rHttp11.setSelected(true);
      }
    }

    tfHTTPHost.setText((String)map.get(HTTP_HOST));
    tfHTTPPort.setText((String)map.get(HTTP_PORT));
    tfHTTPSHost.setText((String)map.get(HTTPS_HOST));
    tfHTTPSPort.setText((String)map.get(HTTPS_PORT));
  }

  public void writeProperties(Map map)
  {
    if (rHttp10.isSelected())
    {
      HTTP_VERSION.setValue(map, "1.0");
    }
    else
    {
      HTTP_VERSION.setValue(map, "1.1");
    }
    map.put(HTTP_HOST, tfHTTPHost.getText());
    J2MEWTkPropertyGroup.HTTP_HOST.setValue(map, tfHTTPHost.getText());
    map.put(HTTP_PORT, tfHTTPPort.getText());
    map.put(HTTPS_HOST, tfHTTPSHost.getText());
    map.put(HTTPS_PORT, tfHTTPSPort.getText());
  }

  /**
   * getName
   *
   * @return String
   */
  public String getName()
  {
    return WTKRes.getString("emprefs.http.name");
  }

  /**
   * isSet
   *
   * @param map Map
   * @return boolean
   */
  public boolean isSet(Map map)
  {
    boolean ret = false;

    String vrs = (String)map.get(HTTP_VERSION);
    if (vrs != null)
    {
      if (vrs.equals("1.0"))
      {
        ret = true;
      }
      else if (vrs.equals("1.1"))
      {
        ret = true;
      }
    }

    if (map.get(HTTP_HOST) != null) ret = true;
    if (map.get(HTTP_PORT) != null) ret = true;
    if (map.get(HTTPS_HOST) != null) ret = true;
    if (map.get(HTTPS_PORT) != null) ret = true;
    return ret;
  }

  /**
   * updateProperties
   *
   * @param map Map
   * @param props Properties
   */
  public void updateProperties(Map map, Properties props)
  {
    String s;
    s = (String)map.get(HTTP_VERSION);
    if (s != null)
    {
      props.put(WTK_HTTP_VERSION, s);
    }

    s = (String)map.get(HTTP_HOST);
    if (s != null)
    {
      props.put(WTK_HTTP_HOST, s);
    }

    s = (String)map.get(HTTP_PORT);
    if (s != null)
    {
      props.put(WTK_HTTP_PORT, s);
    }

    s = (String)map.get(HTTPS_HOST);
    if (s != null)
    {
      props.put(WTK_HTTPS_HOST, s);
    }

    s = (String)map.get(HTTPS_PORT);
    if (s != null)
    {
      props.put(WTK_HTTPS_PORT, s);
    }
  }
}
