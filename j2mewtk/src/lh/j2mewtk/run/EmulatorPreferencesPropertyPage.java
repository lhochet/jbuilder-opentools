package lh.j2mewtk.run;

import java.util.*;

import java.awt.*;
import javax.swing.*;

import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;

import lh.j2mewtk.run.emprefs.*;
import lh.j2mewtk.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2004</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:17 $
 */
public class EmulatorPreferencesPropertyPage extends PropertyPage
{
  private Map map = null;
  private Vector pages = null;

  private JTabbedPane jTabbedPane1 = new JTabbedPane();
  private BorderLayout borderLayout1 = new BorderLayout();

  public EmulatorPreferencesPropertyPage()
  {
    super();
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * postJBInit
   */
  private void postJBInit()
  {
    Iterator iter = pages.iterator();
    while (iter.hasNext())
    {
      EmulatorPreferencesPage pg = (EmulatorPreferencesPage)iter.next();
      jTabbedPane1.add(pg,   pg.getName());
    }
  }

  public EmulatorPreferencesPropertyPage(Map map)
  {
    this();
    this.map = map;
    pages = EmulatorPreferencesManager.getInstance().getPages();
    postJBInit();
  }

  /**
   * readProperties
   */
  public void readProperties()
  {
    if (map != null)
    {
      Iterator iter = pages.iterator();
      while (iter.hasNext())
      {
        EmulatorPreferencesPage pg = (EmulatorPreferencesPage)iter.next();
        pg.readProperties(map);
      }
    }
  }

  /**
   * writeProperties
   */
  public void writeProperties()
  {
    if (map != null)
    {
      Iterator iter = pages.iterator();
      while (iter.hasNext())
      {
        EmulatorPreferencesPage pg = (EmulatorPreferencesPage)iter.next();
        pg.writeProperties(map);
      }
    }
  }

  /**
   * getHelpTopic
   *
   * @return HelpTopic
   * @todo Implement this com.borland.primetime.properties.PropertyPage method
   */
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.EmulatorPreferencesPropertyPage"));
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    this.add(jTabbedPane1, java.awt.BorderLayout.CENTER);
  }
}
