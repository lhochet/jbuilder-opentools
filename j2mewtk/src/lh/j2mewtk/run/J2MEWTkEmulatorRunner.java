package lh.j2mewtk.run;

import java.io.*;
import java.util.*;

import com.borland.jbuilder.node.*;
import com.borland.jbuilder.paths.*;
import com.borland.jbuilder.runtime.*;
import com.borland.primetime.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.runtime.*;
import lh.j2mewtk.run.emprefs.*;

import lh.j2mewtk.*;
import com.borland.primetime.vfs.*;
import com.borland.jbuilder.debugger.*;
import com.sun.jdi.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:18 $
 * 0.3, 28/11/01
 * 0.2, 19/09/01
 * 0.1.1, 24/07/01
 * 0.1, 12/05/01
 */

public class J2MEWTkEmulatorRunner extends JavaRunner
{
  private static final String TRUE = "true";
  private static final String FALSE = "false";

  // Toolkit Home
  private String tkHome = null;
  // device name
  private String deviceName = null;
  // http proxy host
  private String httphost = null;
  // http proxy port
  private String httpport = null;
  // trace gc
  private String tracegc = null;
  // trace calls
//  private String tracecalls = null;
  // trace classes
  private String traceclasses = null;
  // trace exceptions
//  private String traceexceptions = null;
  // trace all
  private String traceall = null;
  // jad path
  private String jadpath = null;


  public J2MEWTkEmulatorRunner()
  {

  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH J2MEWTkEmulatorRunner 0.7");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    J2MEWTkEmulatorRunner me = new J2MEWTkEmulatorRunner();
    RuntimeManager.registerRunner(me);
  }

  public boolean isValid(Browser browser, Project project, Map map, boolean debug)
  {
    // Toolkit Home
    JDKPathSet wtk = getRunJDK(map);
    if (wtk == null) return false;

    File tkHomeDir = wtk.getHomePath().getFileObject();
    if (!tkHomeDir.exists()) return false;
    tkHome = tkHomeDir.getAbsolutePath();
//    if (tkHome == null) return false;

//    if (project == null) return false;

    // jad path
    jadpath = J2MEWTkPropertyGroup.JAD_FILE.getValue(project);
    if (jadpath == null) return false;

    // device name
    deviceName = J2MEWTkPropertyGroup.DEVICE_NAME.getValue(map);
    if (deviceName == null) return false;
    // http proxy host
    httphost = J2MEWTkPropertyGroup.HTTP_HOST.getValue(map);
//    if (httphost == null) return false;
    // http proxy port
    httpport = J2MEWTkPropertyGroup.HTTP_PORT.getValue(map);
//    if (httport == null) return false;
    // trace gc
    tracegc = J2MEWTkPropertyGroup.TRACE_GC.getValue(map);
    if (tracegc != null)
    {
      if (!tracegc.equals(TRUE))
       if (!tracegc.equals(FALSE))
         return false;
    }
//    // trace calls
//    tracecalls = J2MEWTkPropertyGroup.TRACE_CALLS.getValue(project);
//    if (tracecalls != null)
//    {
//      if (!tracecalls.equals(TRUE))
//       if (!tracecalls.equals(FALSE))
//         return false;
//    }
    // trace classes
    traceclasses = J2MEWTkPropertyGroup.TRACE_CLASSES.getValue(map);
    if (traceclasses != null)
    {
      if (!traceclasses.equals(TRUE))
       if (!traceclasses.equals(FALSE))
         return false;
    }
    // trace exceptions
//    traceexceptions = J2MEWTkPropertyGroup.TRACE_EXCEPTIONS.getValue(project);
//    if (traceexceptions != null)
//    {
//      if (!traceexceptions.equals(TRUE))
//       if (!traceexceptions.equals(FALSE))
//         return false;
//    }
    // trace calls
    traceall = J2MEWTkPropertyGroup.TRACE_ALL.getValue(map);
    if (traceall != null)
    {
      if (!traceall.equals(TRUE))
       if (!traceall.equals(FALSE))
         return false;
    }
    return true;
  }

  public PropertyPageFactory getPageFactory(Project project, Map map)
  {
    jadpath = J2MEWTkPropertyGroup.JAD_FILE.getValue(project);
    if (jadpath == null) return null;
    return new J2MEWTkEmulatorPropertyPageFactory(project, map);
  }

  public void run(Browser browser, Project project, Map map, boolean debug)
  {
    super.run(browser, (JBProject)browser.getDefaultProject(),  map,  debug);

//    try
//    {
//      JBProject prj = (JBProject)project;
//      JDKPathSet sav = prj.getPaths().getJDKPathSet();
//      prj.getPaths().setJDKPathSet(((JBProject)browser.getDefaultProject()).getPaths().getJDKPathSet());
//      prj.getPaths().addProjectLibrary(sav);
//      super.run(browser, /*(JBProject)browser.getDefaultProject()*/prj,  map,  debug);
//      prj.getPaths().setJDKPathSet(sav);
//
//      DebugInfoManager dbgman = prj.getDebugInfoManager();
//      System.out.println("dbgman.isDebugging()="+dbgman.isDebugging());
//      if (dbgman.isDebugging())
//      {
//        BreakpointTreeModel btm = dbgman.getBreakpointTreeModel();
//        dbgman.restoreBreakpoints(prj);
//
//        DebugJavaProcessTracker djpt = dbgman.getCurrentDebugSession();
//        VirtualMachine vm = djpt.virtualMachine();
//        dbgman.connectAllBreakpoints(vm);
//
//        btm.connectAllBreakpoints(vm);
//
//        djpt.forceUpdateViews();
//      }
//
//    }
//    catch (Exception ex)
//    {
//      ex.printStackTrace();
//    }
//    // uses the default project jdk (should be jb default jdk) for the java command
  }

  public String getMainClassName(JBProject project, Map map)
  {
    return J2MEWTkPropertyGroup.EMULATOR_MAIN_CLASS.getValue();
  }
  public File getWorkingDirectory(JBProject parm1, Map parm2)
  {
    return new File(tkHome);
  }
  public String getClassPath(JBProject prj, Map map)
  {
    String path = tkHome + J2MEWTkPropertyGroup.KENV_ZIP_SUBPATH.getValue() + ";" + tkHome + J2MEWTkPropertyGroup.KTOOLS_ZIP_SUBPATH.getValue();
//    String wtkversion = getWtkVersion(map);
//    if (wtkversion.startsWith("2."))
    File f = new File(tkHome + J2MEWTkPropertyGroup.CUSTOMJMF_JAR_SUBPATH.getValue());
    if (f.exists())
    {
      path += ";" + tkHome + J2MEWTkPropertyGroup.CUSTOMJMF_JAR_SUBPATH.getValue();
    }
    return path;
  }

  public String getParameters(JBProject prj, Map map)
  {
    String parameters = "";
    if (deviceName != null) parameters = "-Xdevice:" + deviceName;
    parameters += " -Xdescriptor:\"" + jadpath + "\"";

    String httpproxy = "";
    if (httphost != null)
    {
      if (!httphost.equals(""))
      {
        httpproxy = httphost;
      }
    }
    if (httpport != null)
    {
      if (!httpport.equals(""))
      {
        httpproxy += ":" + httpport;
      }
    }
    if (httpproxy.length() > 0) parameters += " -Dcom.sun.midp.io.http.proxy=" + httpproxy;

    String verbose = "";
    boolean isall = false;
    if (traceall != null)
    {
      if (traceall.equals(TRUE))
      {
        verbose += "all";
        isall = true;
      }
    }

    if (!isall)
    {
      if (tracegc != null)
      {
        if (tracegc.equals(TRUE))
        {
          if (verbose.length() > 0)
            verbose += ",";
          verbose += "gc";
        }
      }
      if (traceclasses != null)
      {
        if (traceclasses.equals(TRUE))
        {
          if (verbose.length() > 0)
            verbose += ",";
          verbose += "class";
        }
      }
    }
    if (verbose.length() > 0) parameters += " -Xverbose:" + verbose;

    // check for overriden preferences
    boolean hasProps = false;
    Properties props = new Properties();
    Vector pages = EmulatorPreferencesManager.getInstance().getPages();
    Iterator iter = pages.iterator();
    while (iter.hasNext())
    {
      EmulatorPreferencesPage pg = (EmulatorPreferencesPage)iter.next();
      if (pg.isSet(map))
      {
        hasProps = true;
        pg.updateProperties(map, props);
      }
    }

    if (hasProps)
    {
      try
      {
        File f = File.createTempFile("wtkrunner", ".properties");
        f.deleteOnExit();
        props.store(new FileOutputStream(f), null);
        parameters += " -Xprefs:" + f;
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
      }
    }


    parameters += " 0"; // ?? unknown, sun's addition
    return parameters;
  }


  public String getVMParameters(JBProject parm1, Map parm2)
  {
    String vmParameters = "";
    if (tkHome != null) vmParameters = "-Dkvem.home=\"" + tkHome +"\"";

    return vmParameters;
  }

  public String getDebugClassName(JBProject parm1, Map parm2)
  {
    return WTKRes.getString("wtkrun.emrunner.MIDP_Emulator");
  }

//  private String getWtkVersion(Map map)
//  {
//    String wtkversion = J2MEWTkPropertyGroup.INVALID_VERSION;
//
//    JDKPathSet wtk = getRunJDK(map);
//    if (wtk == null) return wtkversion;
//
//    Map map2 = wtk.getMap();
//    wtkversion = J2MEWTkPropertyGroup.JDK_WTK_VERSION.getValue(map2);
//    if (wtkversion == null)
//    {
//      wtkversion = WTKJDKType.getWtkVersion(wtk);
//      if (wtkversion == null)
//      {
//        wtkversion = J2MEWTkPropertyGroup.INVALID_VERSION;
//      }
//      J2MEWTkPropertyGroup.JDK_WTK_VERSION.setValue(map2, wtkversion);
//    }
//    return wtkversion;
//  }

  private JDKPathSet getRunJDK(Map map)
  {
    String key = J2MEWTkPropertyGroup.JDK_NAME.getValue(map);
    if (key == null) return null;

    return PathSetManager.getJDK(key);
  }
}
