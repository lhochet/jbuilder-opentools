package lh.j2mewtk.run;

import com.borland.primetime.properties.*;
import com.borland.primetime.help.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import com.borland.primetime.node.*;
import com.borland.jbuilder.node.*;
import java.util.*;
import java.awt.event.*;
import java.io.*;
import com.borland.primetime.ide.*;
import com.borland.jbuilder.paths.*;
import com.borland.primetime.vfs.*;
import lh.j2mewtk.*;


/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:17 $
 * 0.2, 22/07/01
 * @created 12/05/01
 */

public class J2MEWTkEmulatorPropertyPage extends PropertyPage
{
  private static final String TRUE = "true";
  private static final String FALSE = "false";

  JBProject project = null;
  Map map = null;

  Border border1;
  TitledBorder titledBorder1;
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JCheckBox callsCheckBox = new JCheckBox();
  JTextField portTextField = new JTextField();
  JPanel tracePanel = new JPanel();
  JComboBox deviceCombo = new JComboBox();
  JCheckBox gcCheckBox = new JCheckBox();
  JPanel httpPanel = new JPanel();
  JTextField hostTextField = new JTextField();
  JLabel portLabel = new JLabel();
  JCheckBox classesCheckBox = new JCheckBox();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JLabel hostLabel = new JLabel();
  JLabel deviceLabel = new JLabel();
  JCheckBox exceptionCheckBox = new JCheckBox();
  GridBagLayout gridBagLayout3 = new GridBagLayout();
  GridBagLayout gridBagLayout2 = new GridBagLayout();
  JPanel jPanel2 = new JPanel();
  JCheckBox allCheckBox = new JCheckBox();
  JLabel jdkLabel = new JLabel();
  JComboBox jdkCombo = new JComboBox();

  public J2MEWTkEmulatorPropertyPage(Project project, Map map)
  {
    this();
    this.project = (JBProject)project;
    this.map = map;
  }
  public J2MEWTkEmulatorPropertyPage()
  {
    try
    {
      preJBInit();
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public void writeProperties()
  {
    if (project != null)
    {
      // jdk name
      J2MEWTkPropertyGroup.JDK_NAME.setValue(map, (String)jdkCombo.getSelectedItem());
//      J2MEWTkPropertyGroup.JDK_NAME.setValue(project, (String)jdkCombo.getSelectedItem());
      // device name
      J2MEWTkPropertyGroup.DEVICE_NAME.setValue(map, (String)deviceCombo.getSelectedItem());
//      J2MEWTkPropertyGroup.DEVICE_NAME.setValue(project, (String)deviceCombo.getSelectedItem());
      // http proxy host
      J2MEWTkPropertyGroup.HTTP_HOST.setValue(map, hostTextField.getText());
//      J2MEWTkPropertyGroup.HTTP_HOST.setValue(project, hostTextField.getText());
      // http proxy port
      J2MEWTkPropertyGroup.HTTP_PORT.setValue(map, portTextField.getText());
//      J2MEWTkPropertyGroup.HTTP_PORT.setValue(project, portTextField.getText());
      // trace gc
      J2MEWTkPropertyGroup.TRACE_GC.setValue(map, gcCheckBox.isSelected() ? TRUE : FALSE);
//      J2MEWTkPropertyGroup.TRACE_GC.setValue(project, gcCheckBox.isSelected() ? TRUE : FALSE);
      // trace calls
//      J2MEWTkPropertyGroup.TRACE_CALLS.setValue(project, callsCheckBox.isSelected() ? TRUE : FALSE);
      // trace classes
      J2MEWTkPropertyGroup.TRACE_CLASSES.setValue(map, classesCheckBox.isSelected() ? TRUE : FALSE);
//      J2MEWTkPropertyGroup.TRACE_CLASSES.setValue(project, classesCheckBox.isSelected() ? TRUE : FALSE);
      // trace exceptions
//      J2MEWTkPropertyGroup.TRACE_EXCEPTIONS.setValue(project, exceptionCheckBox.isSelected() ? TRUE : FALSE);
      // all
      J2MEWTkPropertyGroup.TRACE_ALL.setValue(map, allCheckBox.isSelected() ? TRUE : FALSE);
    }
  }
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.EmulatorPropertyPage_html"));
  }
  public void readProperties()
  {
    if (project != null)
    {
      // jdk name
//      String jdkname = J2MEWTkPropertyGroup.JDK_NAME.getValue(project);
      String jdkname = J2MEWTkPropertyGroup.JDK_NAME.getValue(map);
      if (jdkname != null)
      {
        jdkCombo.setSelectedItem(jdkname);
      }
      // device name
//      String deviceName = J2MEWTkPropertyGroup.DEVICE_NAME.getValue(project);
      String deviceName = J2MEWTkPropertyGroup.DEVICE_NAME.getValue(map);
      if (deviceName != null)
      {
        deviceCombo.setSelectedItem(deviceName);
      }
      // http proxy host
//      String httphost = J2MEWTkPropertyGroup.HTTP_HOST.getValue(project);
      String httphost = J2MEWTkPropertyGroup.HTTP_HOST.getValue(map);
      if (httphost != null)
      {
        hostTextField.setText(httphost);
      }
      // http proxy port
      String httpport = J2MEWTkPropertyGroup.HTTP_PORT.getValue(map);
      if (httpport != null)
      {
        portTextField.setText(httpport);
      }
      // trace gc
      String tracegc = J2MEWTkPropertyGroup.TRACE_GC.getValue(map);
      gcCheckBox.setSelected(false);
      if (tracegc != null)
      {
        if (tracegc.equals(TRUE)) gcCheckBox.setSelected(true);
      }
      // trace calls
//      String tracecalls = J2MEWTkPropertyGroup.TRACE_CALLS.getValue(project);
//      callsCheckBox.setSelected(false);
//      if (tracecalls != null)
//      {
//        if (tracecalls.equals(TRUE)) callsCheckBox.setSelected(true);
//      }
      // trace classes
//      String traceclasses = J2MEWTkPropertyGroup.TRACE_CLASSES.getValue(project);
      String traceclasses = J2MEWTkPropertyGroup.TRACE_CLASSES.getValue(map);
      classesCheckBox.setSelected(false);
      if (traceclasses != null)
      {
        if (traceclasses.equals(TRUE)) classesCheckBox.setSelected(true);
      }
      // trace exceptions
//      String traceexceptions = J2MEWTkPropertyGroup.TRACE_EXCEPTIONS.getValue(project);
//      exceptionCheckBox.setSelected(false);
//      if (traceexceptions != null)
//      {
//        if (traceexceptions.equals(TRUE)) exceptionCheckBox.setSelected(true);
//      }
      // trace all
      String traceall = J2MEWTkPropertyGroup.TRACE_ALL.getValue(map);
      allCheckBox.setSelected(false);
      if (traceall != null)
      {
        if (traceall.equals(TRUE)) allCheckBox.setSelected(true);
      }
    }
  }

  private void preJBInit()
  {
//    if (J2MEWTkPropertyGroup.WTK_VERSION.getInteger() >= J2MEWTkPropertyGroup.V1_0_4)
//    {
      callsCheckBox.setVisible(false);
      callsCheckBox.setSelected(false);
      exceptionCheckBox.setVisible(false);
      exceptionCheckBox.setSelected(false);
//    }

      // jdks
      jdkCombo.removeAllItems();
      String selected = null;
      String curjdk = ((JBProject)Browser.getActiveBrowser().getActiveProject()).getPaths().getJDKPathSet().getName();
      Iterator iter = PathSetManager.getJDKs().iterator();
      while (iter.hasNext())
      {
        JDKPathSet jdk = (JDKPathSet)iter.next();
        String name = jdk.getName();
        if (name.equals(curjdk))
          selected = name;
        if (J2MEWTkPropertyGroup.isMobile(jdk))
        {
          jdkCombo.addItem(name);
        }
      }
      if (selected == null)
      {
//        selected = J2MEWTkPropertyGroup.NEW_PROJECTS_JDK_NAME.getValue();
        // if no jdk is selected tries to set to the first jdk in the list
        selected = (String)jdkCombo.getItemAt(0);
      }
      jdkCombo.setSelectedItem(selected);

      if (selected == null) return;

      JDKPathSet jdk = PathSetManager.getJDK(selected);
      String homepath = jdk.getHomePath().getFile();
      File devices = new File(homepath + J2MEWTkPropertyGroup.DEVICES_DIR_SUBPATH.getValue()); // "D:\\J2mewtk\\wtklib\\devices\\");
      String[] list = devices.list();
      if (list == null)
      {
        Browser.getActiveBrowser().getStatusView().setText(WTKRes.getString("wtkrun.emproppg.NoDeviceFound"), Color.RED);
        return;
      }
      Arrays.sort(list);
      for (int i = 0; i < list.length; i++)
      {
        deviceCombo.addItem(list[i]);
      }
  }
  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(Color.gray,1);
    titledBorder1 = new TitledBorder(border1,WTKRes.getString("wtkrun.emproppg.Trace"));
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout3);
    callsCheckBox.setEnabled(false);
    callsCheckBox.setText(WTKRes.getString("wtkrun.emproppg.Methods_calls"));
    tracePanel.setBorder(titledBorder1);
    tracePanel.setLayout(gridBagLayout2);
    gcCheckBox.setText(WTKRes.getString("wtkrun.emproppg.Garbage_Collection"));
    gcCheckBox.addActionListener(new J2MEWTkEmulatorPropertyPage_gcCheckBox_actionAdapter(this));
    httpPanel.setBorder(new TitledBorder(BorderFactory.createLineBorder(Color.gray,1),WTKRes.getString("wtkrun.HTTP_Proxy")));
    httpPanel.setLayout(gridBagLayout1);
    portLabel.setText(WTKRes.getString("wtkrun.emproppg.Port_"));
    classesCheckBox.setText(WTKRes.getString("wtkrun.emproppg.Classes"));
    classesCheckBox.addActionListener(new J2MEWTkEmulatorPropertyPage_classesCheckBox_actionAdapter(this));
    hostLabel.setText(WTKRes.getString("wtkrun.emproppg.Host_"));
    deviceLabel.setToolTipText("");
    deviceLabel.setText(WTKRes.getString("wtkrun.emproppg.Device_"));
    exceptionCheckBox.setEnabled(false);
    exceptionCheckBox.setText(WTKRes.getString("wtkrun.emproppg.Exceptions"));
    allCheckBox.setText(WTKRes.getString("wtkrun.emproppg.TraceALL"));
    allCheckBox.addActionListener(new J2MEWTkEmulatorPropertyPage_allCheckBox_actionAdapter(this));
    jdkLabel.setText(WTKRes.getString("wtkrun.emproppg.ConfigJDK_"));
    jdkLabel.setToolTipText("");
    jdkCombo.addActionListener(new J2MEWTkEmulatorPropertyPage_jdkCombo_actionAdapter(this));
    this.add(jPanel1,  BorderLayout.CENTER);
    jPanel1.add(deviceCombo,      new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(tracePanel,        new GridBagConstraints(0, 3, 3, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
    tracePanel.add(gcCheckBox,      new GridBagConstraints(0, 0, 1, 1, 0.5, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    tracePanel.add(callsCheckBox,        new GridBagConstraints(0, 1, 1, 1, 0.5, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    tracePanel.add(exceptionCheckBox,       new GridBagConstraints(1, 1, 1, 1, 0.5, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
    tracePanel.add(classesCheckBox,          new GridBagConstraints(1, 0, 1, 1, 0.5, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
    tracePanel.add(allCheckBox,    new GridBagConstraints(0, 2, 1, 1, 0.5, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    jPanel1.add(httpPanel,      new GridBagConstraints(0, 2, 3, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
    httpPanel.add(hostLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    httpPanel.add(portLabel,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    httpPanel.add(hostTextField,  new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    httpPanel.add(portTextField,  new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    jPanel1.add(deviceLabel,     new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jPanel2,     new GridBagConstraints(0, 4, 3, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(jdkLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jdkCombo,   new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
  }

  public boolean isPageValid()
  {
    if (deviceCombo.getSelectedItem() == null) return false;
    return true;
  }

  void allCheckBox_actionPerformed(ActionEvent e)
  {
    if (allCheckBox.isSelected())
    {
      gcCheckBox.setSelected(true);
      classesCheckBox.setSelected(true);
    }
  }

  void gcCheckBox_actionPerformed(ActionEvent e)
  {
    if (!gcCheckBox.isSelected())
    {
      allCheckBox.setSelected(false);
    }
  }

  void classesCheckBox_actionPerformed(ActionEvent e)
  {
    if (!classesCheckBox.isSelected())
    {
      allCheckBox.setSelected(false);
    }
  }

  void jdkCombo_actionPerformed(ActionEvent e)
  {
    deviceCombo.setSelectedIndex(-1);
    deviceCombo.removeAllItems();

    JDKPathSet jdk = PathSetManager.getJDK((String)jdkCombo.getSelectedItem());
    String homepath = jdk.getHomePath().getFile();
    File devices = new File(homepath + J2MEWTkPropertyGroup.DEVICES_DIR_SUBPATH.getValue()); // "D:\\J2mewtk\\wtklib\\devices\\");
    String[] list = devices.list();
    if (list == null)
    {
      Browser.getActiveBrowser().getStatusView().setText(WTKRes.getString("wtkrun.emproppg.NoDeviceFound"), Color.RED);
      return;
    }
    Arrays.sort(list);
    for (int i = 0; i < list.length; i++)
    {
      deviceCombo.addItem(list[i]);
    }

  }


}

// adapters...
//

class J2MEWTkEmulatorPropertyPage_allCheckBox_actionAdapter implements java.awt.event.ActionListener
{
  J2MEWTkEmulatorPropertyPage adaptee;

  J2MEWTkEmulatorPropertyPage_allCheckBox_actionAdapter(J2MEWTkEmulatorPropertyPage adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.allCheckBox_actionPerformed(e);
  }
}

class J2MEWTkEmulatorPropertyPage_gcCheckBox_actionAdapter implements java.awt.event.ActionListener
{
  J2MEWTkEmulatorPropertyPage adaptee;

  J2MEWTkEmulatorPropertyPage_gcCheckBox_actionAdapter(J2MEWTkEmulatorPropertyPage adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.gcCheckBox_actionPerformed(e);
  }
}

class J2MEWTkEmulatorPropertyPage_classesCheckBox_actionAdapter implements java.awt.event.ActionListener
{
  J2MEWTkEmulatorPropertyPage adaptee;

  J2MEWTkEmulatorPropertyPage_classesCheckBox_actionAdapter(J2MEWTkEmulatorPropertyPage adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.classesCheckBox_actionPerformed(e);
  }
}

class J2MEWTkEmulatorPropertyPage_jdkCombo_actionAdapter implements java.awt.event.ActionListener
{
  J2MEWTkEmulatorPropertyPage adaptee;

  J2MEWTkEmulatorPropertyPage_jdkCombo_actionAdapter(J2MEWTkEmulatorPropertyPage adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.jdkCombo_actionPerformed(e);
  }
}
