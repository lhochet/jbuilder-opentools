package lh.j2mewtk.run;

import com.borland.primetime.properties.*;
import com.borland.primetime.node.*;
import java.util.*;
import com.borland.jbuilder.paths.*;
import com.borland.primetime.vfs.*;
import java.io.*;
import com.borland.jbuilder.node.*;

import lh.j2mewtk.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:17 $
 * @created 12/05/01
 */

public class J2MEWTkEmulatorPropertyPageFactory extends PropertyPageFactory
{
  Project project = null;
  Map map = null;


  public J2MEWTkEmulatorPropertyPageFactory(Project project, Map map)
  {
    this();
    this.project = project;
    this.map = map;
  }
  public J2MEWTkEmulatorPropertyPageFactory()
  {
    super(WTKRes.getString("wtkrun.emppgf.MIDP_Emulator"));
  }
  public PropertyPage createPropertyPage()
  {
    if (J2MEWTkPropertyGroup.isMobile(((JBProject)project).getPaths().getJDKPathSet()))
    {
      return new J2MEWTkEmulatorPropertyPage(project, map);
    }
    else
    {
      return null;
  }

}








}
