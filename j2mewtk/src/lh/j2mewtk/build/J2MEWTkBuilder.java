package lh.j2mewtk.build;

import java.util.logging.*;

import com.borland.jbuilder.build.*;
import com.borland.primetime.*;
import com.borland.primetime.build.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;

import lh.j2mewtk.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:14 $
 * 0.4, 20/11/02
 * 0.3, 26/02/02
 * 0.2, 28/11/01
 * 0.1.1, 24/07/01
 * 0.1, 10/05/01
 */
public class J2MEWTkBuilder extends Builder
{
  private static Logger log = Logger.getLogger(J2MEWTkBuildTask.class.getName());

  private boolean isAdded = false;

  public J2MEWTkBuilder()
  {
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH J2MEWTkBuilder 0.6");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION +
        "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." +
        minorVersion);
    }

    J2MEWTkBuilder me = new J2MEWTkBuilder();

    BuilderManager.registerBuilder(me);
  }

  public void beginUpdateBuildProcess(BuildProcess process)
  {
    super.beginUpdateBuildProcess(process);
    log.finest("J2MEWTkBuilder.beginUpdateBuildProcess()");
    isAdded = false;
  }
  public void updateBuildProcess(BuildProcess process, Node node)
  {
    super.updateBuildProcess(process, node);

    if (isAdded)
    {
      return;
    }
    if (!node.getLongDisplayName().endsWith(".jad"))
    {
      return;
    }
    isAdded = true;

    // clean
    CleanBuilder.getCleanBuildTask(process).addDirectory(new Url("file", J2MEWTkPropertyGroup.PREVERIFIED_DIR.getValue(process.getProject())), null);

    // preverify
//    J2MEWTkPreverifyBuildTask preverifytask = (J2MEWTkPreverifyBuildTask)
      process.createTask(J2MEWTkPreverifyBuildTask.class, "lh.j2mewtkot.preverify");

    // preverify
//    J2MEWTkBuildTask packagetask = (J2MEWTkBuildTask)
      process.createTask(J2MEWTkBuildTask.class, "lh.j2mewtkot.package");

    // listener
    J2MEWTkBuilderBuildListener listener = new J2MEWTkBuilderBuildListener();
    process.addBuildListener(listener);
    listener.setPreverifyTask((J2MEWTkPreverifyBuildTask)process.getFirstBuildTask("lh.j2mewtkot.preverify",J2MEWTkPreverifyBuildTask.class));
    listener.setPackageTask((J2MEWTkBuildTask)process.getFirstBuildTask("lh.j2mewtkot.package",J2MEWTkBuildTask.class));
  }
  public void endUpdateBuildProcess(BuildProcess process)
  {
    super.endUpdateBuildProcess(process);
    process.addDependency("lh.j2mewtkot.preverify", Phase.POST_COMPILE_PHASE);
    process.addDependency("lh.j2mewtkot.package", Phase.PACKAGE_PHASE);

  }

}
