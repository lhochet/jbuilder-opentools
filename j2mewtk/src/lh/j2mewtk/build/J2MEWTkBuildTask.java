package lh.j2mewtk.build;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import com.borland.jbuilder.node.*;
import com.borland.primetime.build.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.vfs.*;

import lh.j2mewtk.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:14 $
 * 0.5, 1/09/02
 * 0.4, 1/08/02
 * 0.3, 26/02/02
 * 0.2, 28/11/01
 * 0.1.1, 24/05/01
 * 0.1, 10/05/01
 */

public class J2MEWTkBuildTask extends BuildTask
{
  private static Logger log = Logger.getLogger(J2MEWTkBuildTask.class.getName());

  public static Object TASK_KEY = new Object();

  private boolean enabled = false;

  public J2MEWTkBuildTask()
  {
  }

  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  public boolean build(BuildProcess process)
  {
    log.fine("J2MEWTkBuildTask.build()");
    if (!enabled)
    {
      log.fine("build of J2MEWTkBuildTask canceled: not enabled, no output previously generated");
      return true;
    }

    try
    {
      JBProject prj = (JBProject)process.getProject();
      Browser browser = Browser.getActiveBrowser();
//      String tkHome = ((JBProject)Browser.getActiveBrowser().getActiveProject()).getPaths().getJDKPathSet().getHomePath().getFile();

      String preverdir = J2MEWTkPropertyGroup.PREVERIFIED_DIR.getValue(prj);
      String resdir = J2MEWTkPropertyGroup.RES_DIR.getValue(prj);
      String manifest = J2MEWTkPropertyGroup.MANIFEST_FILE.getValue(prj);
      String jarpath = J2MEWTkPropertyGroup.JAR_FILE.getValue(prj);
      String jadpath = J2MEWTkPropertyGroup.JAD_FILE.getValue(prj);

      String cmd = "";

      // jar
      log.fine("Jaring");
      process.fireBuildStatus(WTKRes.getString("wtkbuildtask.jaring"), false);
      if (File.separatorChar == '\\')
      {
        cmd = "\"" + J2MEWTkPropertyGroup.getJarExePath() + "\" cmf \"" + manifest + "\" \"" + jarpath + "\" -C \"" + preverdir + "\" . -C \"" + resdir + "\" .";
      }
      else
      {
        cmd = J2MEWTkPropertyGroup.getJarExePath() + " cmf " + manifest + " " + jarpath + " -C " + preverdir + " . -C " + resdir + " .";
      }
      lh.cmdline.Runner runner = new lh.cmdline.Runner(browser, WTKRes.getString("wtkbuildtask.jar"), cmd, true, true, true, false, false);
      runner.run();
      if (runner.getRetCode() == 0)
      {
        runner.removeTab();
        process.fireBuildOutputEvent(new BuildOutputEvent(this, new Url("file", jarpath), true));
      }

      // update jar size
      log.fine("Updating jad");
      process.fireBuildStatus(WTKRes.getString("wtkbuildtask.updating_jad"), false);
      File jarfile = new File(jarpath);
      File jadfile = new File(jadpath);
      long jadlen = jadfile.length();
      int jadintlen = jadlen < Integer.MAX_VALUE ? (int)jadlen : Integer.MAX_VALUE;
      BufferedReader jadreader = new BufferedReader(new FileReader(jadfile), jadintlen);
      jadintlen = jadintlen + 10 < Integer.MAX_VALUE ? jadintlen + 10 : Integer.MAX_VALUE;
      StringWriter stringwriter = new StringWriter(jadintlen);
      PrintWriter writer = new PrintWriter(stringwriter, true);
      String line;
      while ((line = jadreader.readLine()) != null)
      {
        if (line.startsWith("MIDlet-Jar-Size: "))
        {
          line = "MIDlet-Jar-Size: " + jarfile.length();
        }
        writer.println(line);
      }
      jadreader.close();
      FileWriter jadwriter = new FileWriter(jadfile);
      writer.flush();
      jadwriter.write(stringwriter.toString());
      jadwriter.flush();
      jadwriter.close();

      return true;

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }

    return false;
  }

}
