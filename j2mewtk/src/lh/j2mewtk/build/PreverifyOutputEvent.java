package lh.j2mewtk.build;

import com.borland.primetime.build.BuildOutputEvent;
import com.borland.primetime.vfs.Url;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2002</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 14/11/02
 */

public class PreverifyOutputEvent extends BuildOutputEvent
{

  public PreverifyOutputEvent(Object task, Url url, boolean created)
  {
    super(task, url, created);
  }
}