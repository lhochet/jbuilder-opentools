package lh.j2mewtk.build;

import com.borland.primetime.build.*;
import java.util.logging.*;
import java.util.*;
import java.io.*;
import com.borland.jbuilder.node.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.vfs.*;
import com.borland.jbuilder.paths.*;

import lh.j2mewtk.*;


/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:14 $
 * 0.1, 14/11/02
 */

public class J2MEWTkPreverifyBuildTask extends BuildTask
{
  private static Logger log = Logger.getLogger(J2MEWTkPreverifyBuildTask.class.getName());

  private boolean enabled = false;

  public J2MEWTkPreverifyBuildTask()
  {
  }
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  public boolean build(BuildProcess process)
  {
    log.fine("J2MEWTkPreverifyBuildTask.build()");
    if (!enabled)
    {
      log.finest("build of J2MEWTkPreverifyBuildTask canceled: not enabled, no output previously generated");
      return true;
    }

    try
    {
      JBProject prj = (JBProject)process.getProject();
      Browser browser = Browser.getActiveBrowser();
      String tkHome = ((JBProject)Browser.getActiveBrowser().getActiveProject()).getPaths().getJDKPathSet().getHomePath().getFile();

      String preverpath = J2MEWTkPropertyGroup.PREVERIFY_SUBPATH.getValue();
//      String midpapi = J2MEWTkPropertyGroup.MIDPAPI_ZIP_SUBPATH.getValue();
      String preverdir = J2MEWTkPropertyGroup.PREVERIFIED_DIR.getValue(prj);
      String clsdir = prj.getPaths().getOutPath().getFullName();

      boolean iswindows = File.separatorChar == '\\';

      StringBuffer cpbuf = new StringBuffer();
      String syssep = File.pathSeparator;
      Url[] urls = prj.getPaths().getJDKPathSet().getFullClassPath();
      if (iswindows)
      {
        for (int i = 0; i < urls.length; ++i)
        {
          if (i > 0)
          {
            cpbuf.append(syssep);
          }
          cpbuf.append("\"");
          cpbuf.append(urls[i].getFileObject());
          cpbuf.append("\"");
        }
      }
      else
      {
        for (int i = 0; i < urls.length; ++i)
        {
          if (i > 0) cpbuf.append(syssep);
          cpbuf.append(urls[i].getFileObject());
        }
      }
      String classpath = cpbuf.toString();

      // preverify
      log.fine("Preverifying");
      process.fireBuildStatus(WTKRes.getString("preverifybuildtask.preverifying"), false);
      String cmd = "";
      File dir = new File(preverdir);
      dir.mkdirs();

      if (iswindows)
      {
          cmd = "\"" + tkHome + preverpath + "\" -classpath " + classpath + " -d \"" + preverdir + "\" \"" + clsdir + "\"";
      }
      else
      {
        cmd = tkHome + preverpath + " -classpath " + classpath + " -d " + preverdir + " " + clsdir + "";
      }
      lh.cmdline.Runner runner = new lh.cmdline.Runner(browser, WTKRes.getString("preverifybuildtask.preverify"), cmd, true, true, true, false, false);
      runner.run();
      if (runner.getRetCode() == 0)
      {
        runner.removeTab();
        process.fireBuildOutputEvent(new PreverifyOutputEvent(this, new Url("file", preverdir), true));
        return true;
      }

//      return true;

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }

    return false;
  }

}
