package lh.j2mewtk.build;

import com.borland.primetime.build.*;
import com.borland.primetime.vfs.*;
import java.util.logging.Logger;
import com.borland.primetime.node.*;
import lh.j2mewtk.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:14 $
 * 0.3, 1/09/02
 * 0.2, 1/08/02
 * 0.1, 25/02/02
 */
public class J2MEWTkBuilderBuildListener extends BuildListener
{
  private static Logger log = Logger.getLogger(J2MEWTkBuilderBuildListener.class.getName());

  private J2MEWTkBuildTask packagetask = null;
  private J2MEWTkPreverifyBuildTask preverifytask = null;

  public J2MEWTkBuilderBuildListener()
  {
  }
  public void setPreverifyTask(J2MEWTkPreverifyBuildTask preverifytask)
  {
    this.preverifytask = preverifytask;
  }
  public void setPackageTask(J2MEWTkBuildTask packagetask)
  {
    this.packagetask = packagetask;
  }
  public void buildOutputEvent(BuildProcess process, BuildOutputEvent event)
  {
    log.fine("buildOutputEvent: process = " + process + ", output created(" + event.isCreated() + ") = " + event.getOutputUrl() + ", event = " + event);
    if (event instanceof ClassOutputEvent)
    {
      log.fine("enabling j2mewtk task");
      if (preverifytask != null) preverifytask.setEnabled(true);
//      process.addDependency("lh.j2mewtkot", Phase.POST_COMPILE_PHASE);
    }
    else if (event instanceof PreverifyOutputEvent)
    {
      log.fine("enabling j2mewtk task");
      if (packagetask != null) packagetask.setEnabled(true);
    }

//    super.buildOutputEvent(process, event);
  }

}
