package lh.j2mewtk;

import java.util.*;

/**
 *
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2001-2004</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:12 $
 * @deprecated
 */
public class Res_fr extends java.util.ListResourceBundle
{
  static final Object[][] contents = new String[][]{
        { "Preverifying_", "Pr�v�fication..." },
        { "_d", "\" -d " },
        { "_classpath", "\" -classpath \u0000" },
        { "Preverify", "Pr�v�rification" },
        { "Jaring_", "Cr�ation jar..." },
        { "Jar", "Jar" },
        { "Updating_JAD_", "Mise � jour JAD..." },
        { "Trace", "Trace" },
        { "Methods_calls", "Methods calls" },
        { "Garbage_Collection", "Garbage Collection" },
        { "TraceALL", "All" },
        { "HTTP_Proxy", "HTTP Proxy" },
        { "Port_", "Port :" },
        { "Classes", "Classes" },
        { "Host_", "H�te :" },
        { "Device_", "Appareil :" },
        { "Exceptions", "Exceptions" },
        { "MIDP_Emulator", "Emulateur MIDP" },
        { "Enter_the_Required", "Entrez les attributs n�cessaires pour ce groupe de midlets" },
        { "Midlet_Suite", "Attributs du Groupe de Midlets" },
        { "Name_", "Nom :" },
        { "Version_", "Version :" },
        { "Description_", "Description :" },
        { "Vendor_", "Vendeur :" },
        { "Unknown", "Inconnu" },
        { "Required_Fiel_Name", "Champ requis : Nom" },
        { "Required_Fiel_Version", "Champ requis : Version" },
        { "Required_Fiel_Vendor", "Champ requis : Vendeur" },
        { "Fill_in_the", "Compl�tez les attributs pour cette midlet" },
        { "Midlet_Attributes", "Attribut de cette Midlet" },
        { "Icon_", "Icone :" },
        { "Create_a_new_midlet", "Cr�e une nouvelle midlet" },
        { "single_char_3", "m" },
        { "Midlet", "Midlet" },
        { "J2ME_Wireless_Toolkit", "J2ME Wireless Toolkit" },
        { "Midlet_Class_Wizard", "Assistant classe Midlet" },
        { "MidletAttributesWiza", "MidletAttributesWizardPage_FR.html" },
        { "Select_whether_a_new", "Choisissez si un nouveau projet doit �tre cr�� ou si vous voulez utilis� le projet en cour " },
        { "should_be_used_", "doit �tre utilis�." },
        { "Create_a_new_project", "Cr�er un nouveau projet ou ajouter au projet en cours " },
        { "Create_a_new_project1", "Cr�er un nouveau projet" },
        { "Use_this_project", "Utilis� le projet en cour" },
        { "Optionaly_enter_the", "Entrez les valeurs des autres attributs standard (Optionel)" },
        { "Other_Attributes", "Autres Attributs" },
        { "Info_URL_", "URL pour infos :" },
        { "Data_size_", "Taille des donn�es :" },
        { "Install_notifiy_URL_", "URL notifi�e lors de l'installation :" },
        { "Delete_confirm_", "Confirmer la suppression :" },
        { "ProjectPropertyPage", "ProjectPropertyPage_FR.html" },
        { "JAD_file_", "Fichier JAD :" },
        { "Manifest_file_", "Fichier Manifest :" },
        { "Resources_directory_", "R�pertoire des resources :" },
        { "Preverified_directory", "R�pertoire des pr�v�rifi�s :" },
        { "JAR_file_", "Fichier JAR :" },
        { "Choose_preverfied", "S�lectionnez le r�pertoire pour les classes pr�v�rifi�es..." },
        { "Choose_resources", "S�lectionnez le r�pertoire pour les fichiers de resources..." },
        { "Create_a_new_J2ME", "Cr�e un nouveau projet J2ME Wireless Toolkit" },
        { "single_char_4", "p" },
        { "Project", "Projet" },
        { "J2ME_Wireless_Toolkit1", "Assistant de projets J2ME Wireless Toolkit" },
        { "Project_already", "Le projet existe d�j�, l'utiliser ?" },
        { "Create_a_new_project2", "Cr�ez un nouveau projet ou utilisez un projet existant." },
        { "NewJBProject_html", "NewJBProject_FR.html" },
        { "PostProject_html", "PostProject_FR.html" },
        { "EmulatorPropertyPage_html", "EmulatorPropertyPage_FR.html" },
        { "InfoAttributes_html", "InfoAttributes_FR.html" },
        { "OptionalAttributes", "OptionalAttributes_FR.html" },
        { "If_needed_adjust_the", "Si n�cessaire ajustez les chemins vers ces �l�ments de projet J2ME" },
        { "J2ME_Wireless_Toolkit2", "Sp�cifique au J2ME Wireless Toolkit" },
        { "Empty_field_", "Champ vide." },
        { "J2ME", "J2ME" },
        { "J2ME_Toolkit", "J2ME Toolkit" },
        { "ToolkitPropertyPage", "ToolkitPropertyPage_FR.html" },
        { "Preverifier_sub_path_", "Sous chemind du pr�v�rificateur :" },
        { "Toolkit_home_", "Base du Toolkit :" },
        { "Kenv_zip_sub_path_", "Sous chemin de kenv.zip :" },
        { "MIDPapi_zip_sub_path_", "Sous chemin de midpapi.zip:" },
        { "Emulator_main_class_", "Classe principale de l'�mulateur :" },
        { "J2ME_WTk_JDK_", "JDK pour les nouveaux projets J2ME WTk :" },
        { "Devices_sub_path_", "Sous chemin des appareils :" },
        { "J2ME_WTk_Version_", "Version du J2ME WTk :" },
        { "KTools_zip_sub_path_", "Sous chemin de ktools.zip :" },
        { "Choose_J2ME_Wireless", "S�lectionner le chemin de base du J2ME Wireless Toolkit..." },
        { "NoDeviceFound", "Aucun appareil n'a �t� trouv� dans le r�pertoire wtklib/devices de votre J2ME Wireless Toolkit." },
        { "ConfigJDK_", "JDK J2ME :" },
        { "MicroEdition-Profile:", "MicroEdition-Profile :" },
        { "MicroEdition-Configuration:", "MicroEdition-Configuration :" },
        { "NA", "N/A" }};
  public Object[][] getContents()
  {
    return contents;
  }
}
