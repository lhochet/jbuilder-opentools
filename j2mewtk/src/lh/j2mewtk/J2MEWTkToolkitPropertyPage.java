package lh.j2mewtk;

import java.util.*;

import java.awt.*;
import javax.swing.*;

import com.borland.jbuilder.node.*;
import com.borland.jbuilder.paths.*;
import com.borland.primetime.help.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.properties.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2001-2004
 * Company:
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-10-09 17:19:12 $
 * 0.4, 28/11/01
 * 0.3, 18/09/01
 * 0.1 16/05/01
 */

public class J2MEWTkToolkitPropertyPage extends PropertyPage
{
//  static private final int NB_DEPRECATED = 2;

  BorderLayout borderLayout1 = new BorderLayout();
  JPanel jPanel1 = new JPanel();
  JLabel JDKLabel = new JLabel();
  JComboBox cbJDK = new JComboBox();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  public J2MEWTkToolkitPropertyPage()
  {
    try
    {
      preJBInit();
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public void writeProperties()
  {
    // jdk
    String jdk = (String)cbJDK.getSelectedItem();
//    System.out.println("jdk = " + jdk);
    J2MEWTkPropertyGroup.NEW_PROJECTS_JDK_NAME.setValue(jdk);
  }
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(new ZipHelpBook("j2mewtkdoc.jar"), WTKRes.getString("help.ToolkitPropertyPage"));
  }
  public void readProperties()
  {
  }
  private void preJBInit()
  {
    // jdks
    cbJDK.removeAllItems();
    String selected = null;
    String curjdk = J2MEWTkPropertyGroup.NEW_PROJECTS_JDK_NAME.getValue();
    Iterator iter =
      ((JBProject)Browser.getActiveBrowser().getActiveProject()).getPaths().
      getJDKs().iterator();
    while (iter.hasNext())
    {
      JDKPathSet jdk = (JDKPathSet)iter.next();
      String name = jdk.getName();
      if (name.equals(curjdk))
        selected = name;
      if (J2MEWTkPropertyGroup.isMobile(jdk))
      {
        cbJDK.addItem(name);
      }
    }
    cbJDK.setSelectedItem(selected);
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    JDKLabel.setText(WTKRes.getString("wtkproppg.j2me_wtk_jdk"));
    this.add(jPanel1,  BorderLayout.NORTH);
    jPanel1.add(JDKLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    jPanel1.add(cbJDK,    new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
  }

}
