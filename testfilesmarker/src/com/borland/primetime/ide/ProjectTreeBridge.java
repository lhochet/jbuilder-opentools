package com.borland.primetime.ide;

import com.borland.primetime.node.*;

public class ProjectTreeBridge {
  public static Node getTreeNode(Object value) {
    Node node = null;
    if (value instanceof ProjectTree.ProjectTreeNode) {
      node = ((ProjectTree.ProjectTreeNode)value).getNode();
    }
    return node;
  }
}
