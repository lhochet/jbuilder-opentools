package net.java.dev.jbuilder.opentool.testfilesmarker;

import javax.swing.*;

import com.borland.primetime.ide.*;
import java.awt.Component;
import com.borland.primetime.node.Project;
import javax.swing.tree.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ContainerListener;
import java.awt.event.ContainerEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import com.borland.jbuilder.node.JBProject;
import com.borland.primetime.vfs.Url;
import com.borland.jbuilder.paths.*;

public class TestFileMarkerOpenTool {
  public static void initOpenTool(byte majorVersion, byte minorVersion) {
    Browser.addStaticBrowserListener(new BrowserAdapter() {
      public void browserProjectActivated(Browser browser, Project project) {
        if (project instanceof JBProject &&
            sourceAndTestAreSame((JBProject)project)) {
          return;
        }
        final ProjectView projectView = browser.getProjectView();
        ArrayList list = new ArrayList();
        findProjectTree(browser.getRootPane(), list);
        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
          ProjectTree projectTree = (ProjectTree) iterator.next();
          TreeCellRenderer existingCellRenderer = projectTree.getCellRenderer();
          if (! (existingCellRenderer instanceof
                 TestFileMarkerTreeRenderer)) {
            projectTree.setCellRenderer(new TestFileMarkerTreeRenderer(
                existingCellRenderer));
          }
        }
      }

      private boolean sourceAndTestAreSame(JBProject project) {
        ProjectPathSet path = project.getPaths();
        Url source = path.getDefaultSourcePath();
        Url test = path.getTestPath();
        return source.equals(test);
      }

    });
  }

  private static void findProjectTree(JComponent component, List list) {
    for (int i = 0; i < component.getComponentCount(); i++) {
      Component child = component.getComponent(i);
      if (child instanceof ProjectTree) {
        list.add(child);
        return;
      }
      else if (child instanceof JComponent) {
        findProjectTree((JComponent) child, list);
      }
    }
  }

}
