package net.java.dev.jbuilder.opentool.testfilesmarker;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import com.borland.primetime.node.UrlNode;
import com.borland.jbuilder.node.JBProject;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.node.Project;
import com.borland.primetime.ide.ProjectTree;
import com.borland.primetime.node.Node;
import com.borland.primetime.node.FileNode;
import com.borland.primetime.ide.*;

public class TestFileMarkerTreeRenderer
    implements TreeCellRenderer {
  private TreeCellRenderer delegate;

  public TestFileMarkerTreeRenderer(TreeCellRenderer delegate) {
    this.delegate = delegate;
  }

  public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                boolean selected,
                                                boolean expanded, boolean leaf,
                                                int row, boolean hasFocus) {
    Component component = delegate.getTreeCellRendererComponent(tree, value,
        selected, expanded, leaf, row, hasFocus);
    Node treeNode = ProjectTreeBridge.getTreeNode(value);
    if (treeNode instanceof FileNode) {
      FileNode node = (FileNode) treeNode;
      JBProject project = (JBProject) node.getProject();
      if (project != null && project.getPaths().getSourcePath().length > 1) {
        Url testPath = project.getPaths().getTestPath();
        if (testPath.isChild(node.getUrl())) {
          component.setForeground(Color.BLUE);
        }
      }
    } else if (treeNode instanceof Project) {
      Project project = (Project) treeNode;
      if (project.getUrl().getName().equalsIgnoreCase(".project")) {
        ((JLabel)component).setText(project.getUrl().getParent().getName());
      }
    }
    return component;
  }
}
