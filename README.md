# JBuilder OpenTools #

This projet was a federative project for opensourced JBuilder OpenTool projects for versions of JBuilder between 4.0 and 2006.
JBuilder 2007 and after were based on Eclipse with which these OpenTools are not compatible. 

Name | Description | Author
---- | ----------- | ------
Command Line |Allows to type a command line from inside JBuilder and see its console output in a similar way to JBuilder Java console | lhochet
Community | Provides menu anchors and delayed load up for community opentools | lhochet
Find Files | Like "Find Classes", except that it works on all files. | wolfdancer
J2ME Wireless Toolkit Opentool | Set of opentools that attempts to integrate Sun's J2ME Wireless Toolkit within JBuilder. | lhochet
JBuilder Jabber Client Opentool | Adds a Jabber client within JBuilder. | lhochet
JDK1.5 Compiler extension | Adds a new compiler option for compiling projects using JDK 1.5 (aka JDK 5) language extensions to JBuilder 9 and X. | lhochet
Line Mover | Line related editor actions | wolfdancer
NewToolbar | Add New Class and New Interface buttons to the file toolbar. | lhochet
OpenTools Manager | Check, download new or updated opentools for JBuilder | lhochet
Package Version Incrementer | Increment packages versions in manifest files of archives nodes | lhochet
Project Loaders | Loads project files of other IDEs as JBuilder project | wolfdancer
Test Files Marker | Colors in BLUE in the project view the files in the test directories. | wolfdancer
XmlFormatter | Adds a format XML option to the context menu of the editor for an XML based document for versions of JBuilder below JBuilder 2005. | lhochet
BuildListener | Log the build info provided by the BuildListener class | lhochet
JBLogging | Adds a panel that shows opentools java.util logs | lhochet
OutErrToLog | Convert System.out, System.err messages to java.util log messages (not an opentool, but used with JBLogging | lhochet