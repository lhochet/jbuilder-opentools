package lh.util.jblogging;

import java.util.Date;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-22 21:09:43 $
 * @created 4/08/02
 */
public abstract class JBLogMessage
{
  protected JBLogMessage()
  {
  }

  public abstract Date getTimeStamp();
  public abstract String getMessage();
  public abstract String getLocalisedMessage();
  public abstract boolean isError();
  public abstract boolean isWarning();
  public abstract boolean isConfig();
  public abstract boolean isInfo();
  public abstract boolean isDebug();
  public abstract boolean isTrace();
  public abstract String getLevelName();
  public abstract String getLevelLocalisedName();
  public abstract int getLevel();
  public abstract String getLoggerName();
  public abstract String getClassName();
  public abstract String getMethodName();
  public abstract Throwable getThrown();
}
