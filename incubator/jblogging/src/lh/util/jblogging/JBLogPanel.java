package lh.util.jblogging;

import java.awt.*;
import javax.swing.*;

import org.jdesktop.swing.*;
import javax.swing.table.*;
import java.util.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-22 21:09:43 $
 * @created 21/01/05
 */
public class JBLogPanel extends JPanel
{
  private static class LogTableModel extends AbstractTableModel
  {
    Vector msgs = new Vector();

    public void addMessage(JBLogMessage msg)
    {
      msgs.add(msg);
      int row = msgs.indexOf(msg);
      fireTableRowsInserted(row, row);
    }

    public int getColumnCount()
    {
      return 6;
    }

    public String getColumnName(int columnIndex)
    {
      switch (columnIndex)
      {
        case 0: return "Time";
        case 1: return "Message";
        case 2: return "Level";
        case 3: return "Class";
        case 4: return "Method";
        case 5: return "Thrown";
      }
      return null;
    }

    public int getRowCount()
    {
      return msgs.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex)
    {
      JBLogMessage msg = (JBLogMessage)msgs.get(rowIndex);
      switch (columnIndex)
      {
        case 0: return msg.getTimeStamp();
        case 1: return msg.getMessage();
        case 2: return msg.getLevelName();
        case 3: return msg.getClassName();
        case 4: return msg.getMethodName();
        case 5: return msg.getThrown();
      }
      return null;
    }

  }

  public JBLogPanel()
  {
    try
    {
      jbInit();
      jXtblLogs.setModel(model);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    this.add(jScrollPane1, java.awt.BorderLayout.CENTER);
    jScrollPane1.getViewport().add(jXtblLogs);
  }

  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JXTable jXtblLogs = new JXTable();
  private LogTableModel model = new LogTableModel();


  public void addMessage(JBLogMessage msg)
  {
    model.addMessage(msg);
  }


}
