package lh.util.jblogging;

import com.borland.primetime.ide.*;
import com.borland.primetime.ide.view.*;
import com.borland.primetime.ide.workspace.*;
import lh.util.jblogging.view.JBLogViewType;
import lh.util.jblogging.view.JBLogViewFactory;
import com.borland.primetime.*;
import com.borland.jbuilder.*;
import com.borland.primetime.util.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-22 21:09:43 $
 * @created 21/01/05
 */
public class JBLogging
{
  private static class JBLoggingViewStateAction extends BrowserStateAction
  {
    View view = null;

    public JBLoggingViewStateAction()
    {
      super("JB Logs");
    }
    public void setState(Browser browser, boolean state)
    {
      Workspace workspace = WorkspaceManager.getWorkspace(browser);
      if (view == null)
      {
        View[] views = workspace.getViews(JBLogViewType.ID);
        if (views.length == 1)
        {
          view = views[0];
        }
        else
        {
          ViewManager.registerViewFactory(new JBLogViewFactory());
          view = ViewManager.createView(browser, JBLogViewType.ID);
        }
      }

      if (state)
      {
        workspace.openView(view);
        workspace.setViewActive(view);
      }
      else
      {
        workspace.removeView(view);
        view = null;
      }
    }

    public boolean getState(Browser browser)
    {
      Workspace workspace = WorkspaceManager.getWorkspace(browser);
      if (view == null)
      {
        View[] views = workspace.getViews(JBLogViewType.ID);
        if (views.length == 1)
        {
          view = views[0];
        }
      }

      if (view != null)
      {
        return workspace.isViewVisible(view);
      }
      return false;
    }
  }

  private static BrowserStateAction STATE_SunClockView = new JBLoggingViewStateAction();

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("LH JB Logging vrs " + getOpenToolVersion());
      System.out.println("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Excuted with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    // setup action
    JBuilderMenu.GROUP_ViewIdePanes.add(STATE_SunClockView);

    // setup view factory that will create views at the default location, same place as message view
    ViewManager.registerViewFactory(new JBLogViewFactory());
  }

  public static Version getOpenToolVersion()
  {
    return new Version(getVersionFromPackage(JBLogging.class));
  }

  private static String getVersionFromPackage(Class clazz)
  {
    Package pkg = clazz.getPackage();
    return pkg.getImplementationVersion();
  }
}
