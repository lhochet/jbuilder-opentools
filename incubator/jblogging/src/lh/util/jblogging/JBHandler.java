package lh.util.jblogging;

import java.util.logging.SimpleFormatter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import lh.util.jblogging.view.*;
import java.util.logging.Handler;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-22 21:09:42 $
 * @created 21/01/05
 */
public class JBHandler extends Handler
{

    public JBHandler()
    {
    }
    public void flush()
    {
    }
    public void publish(LogRecord record)
    {
      JBLogViewFactory.addMessage(new  JBJDKLogMessage(record));
    }
    public void close() throws java.lang.SecurityException
    {
    }

}
