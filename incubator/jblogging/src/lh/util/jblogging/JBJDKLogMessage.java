package lh.util.jblogging;

import java.util.logging.SimpleFormatter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.Date;
import java.util.logging.Level;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-22 21:09:43 $
 * @created 4/08/02
 */
  public class JBJDKLogMessage extends JBLogMessage
  {
    private LogRecord record;
    private static final Formatter formatter = new SimpleFormatter();

    public JBJDKLogMessage(LogRecord record)
    {
      this.record = record;
    }

    public Date getTimeStamp()
    {
      return new Date(record.getMillis());
    }

    public String getMessage()
    {
      String msg = record.getMessage();
//    if (msg.equals("THROW"))
//    {
//      Throwable ex = record.getThrown();
//      msg = ex.toString();// ex.getMessage();
//    }
      return msg;
    }

    public String getLocalisedMessage()
    {
      String msg = formatter.formatMessage(record);
//    if (msg.equals("THROW"))
//    {
//      Throwable ex = record.getThrown();
//      msg = ex.toString();// ex.getLocalizedMessage();
//    }
      return msg;
    }

    public boolean isError()
    {
      return record.getLevel().equals(Level.SEVERE);
    }
    public boolean isWarning()
    {
      return record.getLevel().equals(Level.WARNING);
    }
    public boolean isConfig()
    {
      return record.getLevel().equals(Level.CONFIG);
    }
    public boolean isInfo()
    {
      return record.getLevel().equals(Level.INFO);
    }
    public boolean isDebug()
    {
      return record.getLevel().equals(Level.FINE);
    }
    public boolean isTrace()
    {
      return record.getLevel().equals(Level.FINER) || record.getLevel().equals(Level.FINEST);
    }
    public String getLevelName()
    {
      return record.getLevel().getName();
    }
    public String getLevelLocalisedName()
    {
      return record.getLevel().getLocalizedName();
    }
    public int getLevel()
    {
      return record.getLevel().intValue();
    }
    public String getLoggerName()
    {
      return record.getLoggerName();
    }
    public String getClassName()
    {
      return record.getSourceClassName();
    }
    public String getMethodName()
    {
      return record.getSourceMethodName();
    }
    public Throwable getThrown()
    {
      return record.getThrown();
    }


}
