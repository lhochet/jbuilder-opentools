package lh.util.jblogging.view;

import com.borland.primetime.ide.view.ViewType;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-22 21:09:44 $
 * @created 21/01/05
 */
public class JBLogViewType extends ViewType
{
  public static final String ID = "jblogs-view";
  private static final String NAME = "JB Logs";

  public JBLogViewType()
  {
    super(null, ID, NAME, null, false);
  }
}
