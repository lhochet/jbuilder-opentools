package lh.util.jblogging.view;

import com.borland.primetime.ide.DefaultViewFactory;
import javax.swing.JComponent;
import com.borland.primetime.ide.Browser;
import javax.swing.*;
import lh.util.jblogging.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-22 21:09:44 $
 * @created 21/01/05
 */
public class JBLogViewFactory extends DefaultViewFactory
{
  private static final JBLogViewType type = new JBLogViewType();
  private static JBLogPanel panel = new JBLogPanel();

  public static void addMessage(JBLogMessage msg)
  {
    panel.addMessage(msg);
  }

  public JBLogViewFactory()
  {
    super(type);
  }
  protected JComponent createComponent(Browser browser)
  {
    return panel;
  }
}
