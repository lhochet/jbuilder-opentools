package lh.util.buildlogging;

import com.borland.primetime.*;
import com.borland.primetime.build.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.ide.*;
import java.util.*;
import com.borland.jbuilder.build.*;
import java.util.logging.*;

/**
 * Title:        J2ME Wireless ToolKit OT
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author Ludovic HOCHET
 * @version 0.1, 9/09/02
 */
public class DummyBuilder extends Builder
{
  private static Logger log = Logger.getLogger(DummyBuilder.class.getName());

  private static LoggingBuildListener buildlistener = new LoggingBuildListener();

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH DummyBuilder 0.1");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION +
        "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." +
        minorVersion);
    }

    DummyBuilder me = new DummyBuilder();
    BuilderManager.registerBuilder(me);
  }


  public void endUpdateBuildProcess(BuildProcess process)
  {
    /**@todo: Override this com.borland.primetime.build.Builder method*/
    super.endUpdateBuildProcess(process);
  }

  public BuildAction[] getMappableTargets(Node node)
  {
    /**@todo: Override this com.borland.primetime.build.Builder method*/
    return super.getMappableTargets(node);
  }

  public void beginUpdateBuildProcess(BuildProcess process)
  {
    super.beginUpdateBuildProcess(process);
    if (PrimeTime.isVerbose())
    {
      process.addBuildListener(buildlistener);
    }

  }

  public void updateBuildProcess(BuildProcess process, Node node)
  {
    /**@todo: Override this com.borland.primetime.build.Builder method*/
    super.updateBuildProcess(process, node);

    if (PrimeTime.isVerbose())
    {
      log.info("DummyBuilder.updateBuildProcess called for " + node.
        getLongDisplayName());
    }

  }
}
