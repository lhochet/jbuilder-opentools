package lh.util.buildlogging;

import com.borland.primetime.build.*;
import com.borland.primetime.vfs.*;
import java.util.logging.Logger;
import com.borland.primetime.node.*;

/**
 * <p>Title: J2ME Wireless ToolKit OT</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2000</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 9/09/02
 */
public class LoggingBuildListener extends BuildListener
{
  private static Logger log = Logger.getLogger(LoggingBuildListener.class.getName());

  public LoggingBuildListener()
  {
  }

  public void buildMessage(BuildProcess process, String file, String message)
  {
    log.info("buildMessage: process = " + process + ", file = " + file + ", message = " + message);
    super.buildMessage(process, file, message);
  }

  public void buildStatus(BuildProcess process, String info, boolean error)
  {
    log.info("buildStatus: process = " + process + ", info = " + info + ", error = " + error);
    super.buildStatus(process, info, error);
  }

  public void buildFinish(BuildProcess process)
  {
    log.info("buildFinish: process = " + process);
    super.buildFinish(process);
  }

  public void buildProblem(BuildProcess process, Url url, boolean error, String
      message, int line, int col, String helptopic)
  {
    log.severe("buildProblem: process = " + process + ", url = " + url + ", error = " +
        error + ", message = " + message + ", line = " + line + ", col = " + col +
        ", helptopic = " + helptopic);
    super.buildProblem(process, url, error, message, line, col, helptopic);
  }

  public void buildStart(BuildProcess process)
  {
    log.info("buildStart: process = " + process);
    super.buildStart(process);
  }

  public void buildInformation(BuildProcess process, ExternalTask task, Url url,
      String info, int line, int col, boolean error)
  {
    log.info("buildInformation: process = " + process + ", info = " + info + ", error = " +
        error);
    super.buildInformation(process, task, url, info, line, col, error);
  }
  public void buildOutputEvent(BuildProcess process, BuildOutputEvent event)
  {
    log.info("buildOutputEvent: process = " + process + ", output created(" + event.isCreated() + ") = " + event.getOutputUrl() + ", event = " + event);
    super.buildOutputEvent(process, event);
  }
  public void taskFinished(BuildProcess process, Object task)
  {
    log.info("taskFinished: process = " + process + ", task = " + task);
    super.taskFinished(process, task);
  }
  public void targetStarted(BuildProcess process, String target)
  {
    log.info("targetStarted: process = " + process + ", target = " + target);
    super.targetStarted(process, target);
  }
  public void taskStarted(BuildProcess process, Object task)
  {
    log.info("taskStarted: process = " + process + ", task = " + task);
    super.taskStarted(process, task);
  }
  public void targetFinished(BuildProcess process, String target)
  {
    log.info("targetFinished: process = " + process + ", target = " + target);
    super.targetFinished(process, target);
  }
  public void buildStatus(BuildProcess process, String parm2, int parm3)
  {
    log.info("buildStatus: process = " + process + ", parm2 = " + parm2 + ", parm3 = " + parm3);
    super.buildStatus(process, parm2, parm3);
  }
  public void projectGroupBuildFinish(ProjectGroup group)
  {
    log.info("projectGroupBuildFinish: group = " + group);
    super.projectGroupBuildFinish(group);
  }
  public void projectGroupBuildStart(ProjectGroup group)
  {
    log.info("projectGroupBuildStart: group = " + group);
    super.projectGroupBuildStart(group);
  }

}
