package lh.util.outerrtolog;

import java.util.logging.*;
import com.borland.primetime.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 17/08/02
 */

public class OutErrToLog
{
  private static Logger log = Logger.getLogger(OutErrToLog.class.getName());

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    log.info("LH OutErrToLog vrs 0.1");
    log.info("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
    log.info("  Excuted with JB OTAPI " + majorVersion + "." + minorVersion);
    try
    {
      System.setErr(new ErrInterceptor(System.err));
      System.setOut(new OutInterceptor(System.out));
    }
    catch (Exception ex)
    {
      log.log(Level.FINE, "Could not set up the interceptor print streams", ex);
    }

  }
}