package lh.util.outerrtolog;

import java.io.*;
import java.util.logging.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 17/08/02
 */

public class ErrInterceptor extends PrintStream
{
  private static Logger log = Logger.getLogger(ErrInterceptor.class.getName());

// PrintStream out = null;
  public ErrInterceptor(PrintStream out)
  {
    super(out);
//    this.out = out;
  }

  public void flush()
  {
//    log.info("tst");
    /**@todo Override this java.io.PrintStream method*/
    super.flush();
  }
  public void write(int b)
  {
//    log.info("" + (char)b);
    /**@todo Override this java.io.PrintStream method*/
    super.write(b);
  }
  public void write(byte[] parm1, int parm2, int parm3)
  {
    /**@todo Override this java.io.PrintStream method*/
    if (!(new String(parm1, parm2, parm3)).equals(System.getProperty("line.separator")))
    {
      log.warning(new String(parm1, parm2, parm3));
//      super.flush();
    }
//    super.write(parm1, parm2, parm3);
//    super.flush();
//    log.info("tst aft");
  }
}