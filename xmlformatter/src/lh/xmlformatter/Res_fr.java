package lh.xmlformatter;

import java.util.*;

/**
 * <p>XmlFormatter OpenTool, French ResourceBundle</p>
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-13 20:33:02 $
 * @created 21/12/03
 */
public class Res_fr extends java.util.ListResourceBundle
{
  static final private Object[][] contents = new String[][]{
  { "XML_Formatting", "Formatage XML" },
  { "propertypage_helpfile", "/doc/xmlformattingpropertypage_fr.html" },
  { "Indentation_", "Indentation :" },
  { "Options_", "Options :" },
  { "Text_on_own_line", "Texte sur sa propre ligne" },
  { "Linear_XML", "XML lin�aire" },
  { "Use_Spaces", "Utiliser des espaces" },
  { "Use_Tabs", "Utiliser des tabulations" },
  { "Format_the_currently", "Formater le document XML en cours" },
  { "Format_XML_accelerator", "F" },
  { "Format_XML_", "Formater XML..." }};
  public Object[][] getContents()
  {
    return contents;
  }
}
