package lh.xmlformatter;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.borland.primetime.help.*;
import com.borland.primetime.node.*;
import com.borland.primetime.properties.*;
import java.util.ResourceBundle;

/**
 * <p>XML Formatter Property Page</p>
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-13 20:33:03 $
 * @created 25/03/03
 */

public class XMLFormatterPropertyPage extends PropertyPage
{
  static ResourceBundle res = ResourceBundle.getBundle("lh.xmlformatter.Res");
  private Project prj = null;
  private ButtonGroup buttonGroup1 = new ButtonGroup();
  private JPanel jPanel1 = new JPanel();
  private JSpinner jSpinner1 = new JSpinner();
  private JRadioButton rUseTabs = new JRadioButton();
  private JRadioButton rUseSpaces = new JRadioButton();
  private TitledBorder titledBorder1;
  private JPanel jPanel2 = new JPanel();
  private JCheckBox tfLinearXML = new JCheckBox();
  private JCheckBox tfTextOnOwnLine = new JCheckBox();
  private TitledBorder titledBorder2;
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private JPanel jPanel3 = new JPanel();
  private JPanel jPanel4 = new JPanel();

  /**
   * Default constructror for the Designer
   * <p>shouldn't be used for anything else
   */
  public XMLFormatterPropertyPage()
  {
    this((Project)null);
  }

  /**
   * Constructor
   * @param prj Project for which the property page is created
   */
  public XMLFormatterPropertyPage(Project prj)
  {
    this.prj = prj;
    try
    {
      preJBInit();
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * Update the project properties with the values from the page
   */
  public void writeProperties()
  {
    if (prj != null)
    {
      XMLFormatterPropertyGroup.USE_TABS.setBoolean(prj, rUseTabs.isSelected());
      // use_space is equivalent to use_tab = false
      XMLFormatterPropertyGroup.NB_SPACES.setValue(prj, jSpinner1.getValue().toString());
      XMLFormatterPropertyGroup.TEXT_ON_OWN_LINE.setBoolean(prj, tfTextOnOwnLine.isSelected());
      XMLFormatterPropertyGroup.LINEAR_XML.setBoolean(prj, tfLinearXML.isSelected());
    }
  }

  /**
   * Set up the property page with the values of the project properties
   */
  public void readProperties()
  {
    if (prj != null)
    {
      if (XMLFormatterPropertyGroup.USE_TABS.getBoolean(prj))
      {
        rUseTabs.setSelected(true);
      }
      else
      {
        rUseSpaces.setSelected(true);
      }
      jSpinner1.setValue(new Integer(XMLFormatterPropertyGroup.NB_SPACES.getValue(prj)));

      tfTextOnOwnLine.setSelected(false);
      tfLinearXML.setSelected(false);
      if (XMLFormatterPropertyGroup.TEXT_ON_OWN_LINE.getBoolean(prj))
      {
        tfTextOnOwnLine.setSelected(true);
      }
      if (XMLFormatterPropertyGroup.LINEAR_XML.getBoolean(prj))
      {
        tfLinearXML.setSelected(true);
      }
    }
  }

  /**
   * Return the HelpTopic for this property page
   * @return HelpTopic Topic to display
   */
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource(res.getString("propertypage_helpfile")).toString());
  }

  /**
   * JB Designer UI creation method
   * @throws Exception
   */
  private void jbInit() throws Exception
  {
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), res.getString("Indentation_"));
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), res.getString("Options_"));
    tfTextOnOwnLine.setText(res.getString("Text_on_own_line"));
    tfTextOnOwnLine.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfTextOnOwnLine_actionPerformed(e);
      }
    });
    tfLinearXML.setText(res.getString("Linear_XML"));
    tfLinearXML.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfLinearXML_actionPerformed(e);
      }
    });
    jPanel2.setBorder(titledBorder2);
    jPanel2.setLayout(gridBagLayout2);
    rUseSpaces.setText(res.getString("Use_Spaces"));
    rUseTabs.setText(res.getString("Use_Tabs"));
    this.setLayout(gridBagLayout3);
    jPanel1.setBorder(titledBorder1);
    jPanel1.setLayout(gridBagLayout1);
    jPanel1.add(rUseSpaces, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
      , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(rUseTabs, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                 new Insets(5, 5, 0, 5), 24, 0));
    jPanel1.add(jSpinner1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 5), 20, 0));
    jPanel1.add(jPanel4, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0
                                                , GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                                                new Insets(0, 0, 0, 0), 0, 0));
    this.add(jPanel2, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                             new Insets(5, 5, 5, 5), 0, 0));
    jPanel2.add(tfTextOnOwnLine, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
      , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    jPanel2.add(tfLinearXML, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
      , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    jPanel2.add(jPanel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0
                                                , GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                                                new Insets(0, 0, 0, 0), 0, 0));
    this.add(jPanel1, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                             new Insets(5, 5, 0, 5), 0, 0));
    buttonGroup1.add(rUseSpaces);
    buttonGroup1.add(rUseTabs);
  }

  /**
   * Additional UI init occuring before jbInit();
   */
  private void preJBInit()
  {
    jSpinner1.setModel(new SpinnerNumberModel(2, 1, 100, 1));
  }

  /**
   * Deselect Linear XML check box if the text on own line is selected
   * @param e ActionEvent, ignored
   */
  private void tfTextOnOwnLine_actionPerformed(ActionEvent e)
  {
    if (tfTextOnOwnLine.isSelected())
    {
      tfLinearXML.setSelected(false);
    }
  }

  /**
   * Deselect text on own line check box if the Linear XML is selected
   * @param e ActionEvent, ignored
   */
  private void tfLinearXML_actionPerformed(ActionEvent e)
  {
    if (tfLinearXML.isSelected())
    {
      tfTextOnOwnLine.setSelected(false);
    }
  }
}
