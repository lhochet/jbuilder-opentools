package lh.xmlformatter;

import java.io.*;
import java.util.logging.*;
import javax.xml.parsers.*;

import javax.swing.*;

import com.borland.jbuilder.node.*;

import com.borland.primetime.*;
import com.borland.primetime.editor.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.vfs.*;

import lh.util.xmlwriter.*;
import java.util.ResourceBundle;

/**
 * <p>XmlFormatter OpenTool, set up the Format XML action</p>
 * <p>Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-13 20:33:03 $
 * @created 27/09/02
 */

public class XmlFormatter implements EditorContextActionProvider
{
  static ResourceBundle res = ResourceBundle.getBundle("lh.xmlformatter.Res");
  private static Logger log = Logger.getLogger(XmlFormatter.class.getName());

  /**
   * Instance of the XML Formater action
   */
  private static FormatAction FORMAT_XML_ACTION = new FormatAction();

  /**
   * Format/Pretty print an XML file
   */
  static private class FormatAction extends BrowserAction
  {
    /**
     * Node to pretty print
     */
    private XMLFileNode node = null;

    public FormatAction()
    {
      super(res.getString("Format_XML_"), res.getString("Format_XML_accelerator").charAt(0), res.getString("Format_the_currently"),
            BrowserIcons.ICON_FORMATCODE);
    }

    /**
     * Called before displaying the popup menu, enable the action if the current
     * node is an XMLFileNode, also update the value of the node field
     * @param browser Browser
     */
    public void update(Browser browser)
    {
      enabled = false;
      node = null;
      Node activeNode = Browser.getActiveBrowser().getActiveNode();
      if (activeNode instanceof XMLFileNode)
      {
        enabled = true;
        node = (XMLFileNode)activeNode;
      }
    }

    /**
     * Called when the action is chosen
     * @param browser Browser from which the action is performed
     */
    public void actionPerformed(Browser browser)
    {
      try
      {
        // only update the node here in the unlikely case that it wouldn't have been in update
        if (node == null)
        {
          Node activeNode = /*Browser.getActiveBrowser()*/browser.getActiveNode();
          if (!(activeNode instanceof XMLFileNode))
          {
            return;
          }
          node = (XMLFileNode)activeNode;
        }

        Buffer buffer = node.getBuffer(); // input and final output buffer
        StringWriter sw = new StringWriter(); // temp output buffer
        org.w3c.dom.Document doc = null; // DOM document that hold the 'format less' xml document

        // create the factory for the DOM parser, disable as much validation as possible
        // document only need to be well formed
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
//        factory.setAttribute("http://xml.org/sax/features/validation",
//                             new Boolean(false));
        factory.setAttribute("http://apache.org/xml/features/validation/dynamic",
                             new Boolean(false));
        factory.setAttribute("http://apache.org/xml/features/nonvalidating/load-dtd-grammar",
                             new Boolean(false));
        factory.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd",
                             new Boolean(false));
        factory.setAttribute("http://apache.org/xml/features/validation/schema",
                             new Boolean(false));
        factory.setAttribute("http://apache.org/xml/features/validation/schema-full-checking",
                             new Boolean(false));
        DocumentBuilder builder = factory.newDocumentBuilder();

        // parse the input document, "removing" the existing format
        doc = builder.parse(buffer.getInputStream());
        if (doc == null) return; //abort

        // create the pretty printer and set its properties
        DOM2Sax d2s = DOM2Sax.createDOM2Writer(sw);
        d2s.setIdentationUseTabs(XMLFormatterPropertyGroup.USE_TABS.getBoolean(Browser.getActiveBrowser().getActiveProject()));
        d2s.setIdentationSize(Integer.parseInt(XMLFormatterPropertyGroup.NB_SPACES.getValue(Browser.getActiveBrowser().getActiveProject())));
        d2s.setTextOnOwnLine(XMLFormatterPropertyGroup.TEXT_ON_OWN_LINE.getBoolean(Browser.getActiveBrowser().getActiveProject()));
        d2s.setLinearXML(XMLFormatterPropertyGroup.LINEAR_XML.getBoolean(Browser.getActiveBrowser().getActiveProject()));
        // do it
        d2s.fire(doc);

        // retrieve the pretty formated document and update the node buffer, then let the buffer listener know about it
        byte[] bytes = sw.toString().getBytes();
        buffer.setContent(bytes);
        buffer.fireBufferStateChanged(Buffer.STATE_READONLY, Buffer.STATE_MODIFIED);
      }
      catch (Exception ex)
      {
        log.throwing("XmlFormatter.FormatAction", "actionPerformed", ex);
      }
    }
  }

  /**
   * Initialise the opentool
   * @param majorVersion Major version number of the 'instance' of JBuilder running this OT (eg. 4)
   * @param minorVersion Minor version number of the 'instance' of JBuilder running this OT (eg. 6 for JB9)
   */
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    // could be usefull for diagnostic
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH XmlFormatter 0.3");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    try
    {
      EditorManager.registerContextActionProvider(new XmlFormatter());
      PropertyManager.registerPropertyGroup(new XMLFormatterPropertyGroup());

    }
    catch (Exception ex)
    {
      log.throwing("XmlFormatter", "initOpenTool", ex);
    }

  }

  /**
   * Return FORMAT_XML_ACTION if the active node in the active browser is an XMLFileNode
   * @param ep EditorPane for which the action should be generated, useless at it does not provide any access to the node...
   * @return FORMAT_XML_ACTION if the active node in the active browser is an XMLFileNode or null else to indicate that no action is coming from us.
   */
  public Action getContextAction(EditorPane ep)
  {
    if (Browser.getActiveBrowser().getActiveNode() instanceof XMLFileNode)
    {
      return FORMAT_XML_ACTION;
    }
    return null;
  }

  /**
   * Return the relative position in the context menu 'main' group, the higher the priority, the higher in the menu the action will be
   * @return int, currently returns 99
   */
  public int getPriority()
  {
    return 99;
  }

}
