package lh.xmlformatter;

import com.borland.primetime.properties.PropertyGroup;
import com.borland.primetime.properties.PropertyPageFactory;
import com.borland.primetime.properties.*;
import com.borland.primetime.node.*;
import java.util.ResourceBundle;

/**
 * <p>XML Formatter Property Group</p>
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-13 20:33:03 $
 * @created 25/03/03
 */

public class XMLFormatterPropertyGroup implements PropertyGroup
{
  /**
   * name of the category for the properties
   */
  static ResourceBundle res = ResourceBundle.getBundle("lh.xmlformatter.Res");
  private static final String XMLFORMATTER_CAT = "lh.xmlformatter";

  /**
   * True if tab should be used for indentation,
   * <br>False if spaces are used,
   * <br>Default is false.
   */
  static NodeBooleanProperty USE_TABS = new NodeBooleanProperty(XMLFORMATTER_CAT, "usetabs", false);

  /**
   * Number of spaces per indentation when using spaces to indent
   * <br>Default is 2.
   */
  static NodeProperty NB_SPACES = new NodeProperty(XMLFORMATTER_CAT, "nbpaces", "2");

  /**
   * True if the text data should be on their own lines,
   * <br>False otherwise,
   * <br>Default is false.
   */
  static NodeBooleanProperty TEXT_ON_OWN_LINE = new NodeBooleanProperty(XMLFORMATTER_CAT, "textonownline", false);

  /**
   * True if the XML produced should be a single line without indentation,
   * <br>False if multilines indented XML is to be produced,
   * <br>Default is false.
   */
  static NodeBooleanProperty LINEAR_XML = new NodeBooleanProperty(XMLFORMATTER_CAT, "linearxml", false);

  /**
   * Initialise the properties (does nothing in this case)
   *
   * @see com.borland.primetime.properties.PropertyGroup#initializeProperties()
   */
  public void initializeProperties()
  {
  }

  /**
   * Create the XML formatter property page factory for the given topic if it is an intance of a project, else return null.
   * @see com.borland.primetime.properties.getPageFactory#getPageFactory(Object)
   * @param topic Object Topic for which the property manager is asking for a property page
   * @return PropertyPageFactory return the property page factory for XML formatter or null if the topic is not a project
   */
  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic instanceof Project)
    {
      final Project prj = (Project)topic;
      return new PropertyPageFactory(res.getString("XML_Formatting"))
      {
        public PropertyPage createPropertyPage()
        {
          return new XMLFormatterPropertyPage(prj);
        }
      };
    }
    return null; // else not our business
  }

}
