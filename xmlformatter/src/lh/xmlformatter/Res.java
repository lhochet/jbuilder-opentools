package lh.xmlformatter;

import java.util.*;

/**
 * <p>XmlFormatter OpenTool, English ResourceBundle</p>
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-06-13 20:33:02 $
 * @created 21/12/03
 */
public class Res extends java.util.ListResourceBundle
{
  static final private Object[][] contents = new String[][]{
        { "XML_Formatting", "XML Formatting" },
        { "propertypage_helpfile", "/doc/xmlformattingpropertypage.html" },
        { "Indentation_", "Indentation:" },
        { "Options_", "Options:" },
        { "Text_on_own_line", "Text on own line" },
        { "Linear_XML", "Linear XML" },
        { "Use_Spaces", "Use Spaces" },
        { "Use_Tabs", "Use Tabs" },
        { "Format_the_currently", "Format the currently opened XML document" },
        { "Format_XML_accelerator", "F" },
        { "Format_XML_", "Format XML..." }};
  public Object[][] getContents()
  {
    return contents;
  }
}
