package lh.cmdline;

import com.borland.primetime.properties.*;
import com.borland.primetime.node.*;
import java.util.*;

/**
 * <p>Title: Command line open tool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 27/04/02
 */

public class CommandLinePropertyGroup implements PropertyGroup
{
  public class CommandLinePropertyPageFactory extends PropertyPageFactory
  {
    FileNode node = null;
    public CommandLinePropertyPageFactory(FileNode node)
    {
      super("Execute");
      this.node = node;
    }
    public PropertyPage createPropertyPage()
    {
      try
      {
        return new ExecutablePropertyPage(node);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      return null;
    }
  }

  public CommandLinePropertyGroup()
  {
  }
  public void initializeProperties()
  {
    // do nothing
  }
  public PropertyPageFactory getPageFactory(Object topic)
  {
//    System.out.println("topic = " + topic);
//    System.out.println("topic class = " + topic.getClass().getName());
    if ((topic instanceof ExeFileNode) || (topic instanceof BatFileNode))
    {
      return new CommandLinePropertyPageFactory((FileNode)topic);
    }
    else if ((topic instanceof FileNode) && (((FileNode)topic).getUrl().getFileExtension().toUpperCase().equals("EXE")))
    {
      return new CommandLinePropertyPageFactory((FileNode)topic);
    }
    return null; // else not our business
  }
}