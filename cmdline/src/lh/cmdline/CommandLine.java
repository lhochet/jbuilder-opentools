
/**
 * Title:        LH JBuilder UI Extension<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @author       Ludovic HOCHET
 * @version      $Revision: 1.1 $ $Date: 2005-11-06 14:15:50 $
 * 0.6, 6/10/02
 * 0.5.1, 12/07/02
 * 0.4.5, 3/12/01
 * 0.4, 17/10/01
 * 0.3, 30/06/01
 */
package lh.cmdline;

import javax.swing.Action;
import com.borland.primetime.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.actions.*;
import com.borland.jbuilder.JBuilderMenu;
import com.borland.jbuilder.node.JBProject;
import com.borland.primetime.runtime.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.ide.about.*;


public class CommandLine
{
  static boolean isJB4OrLess = false;

  public static final ActionGroup GROUP_LHJBExt =
    new ActionGroup("LHJBExtGroup", 'L', "LH JBuilder extensions group...");

  public static void initOpenTool(byte major, byte minor)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH CommandLine 0.8.1");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + major + "." + minor);
    }

    CreditInfoPageFactory.registerUserInformation(new CreditInformation("LH CommandLine 0.8.1"));

    if (major == 4)
    {
      if (minor < 2)
      {
        isJB4OrLess = true;
      }
    }

    // Add the CommandLine action to lh jb ext group
    //GROUP_LHJBExt.add(CommandLineDialogPanel.ACTION_CommandLine);
    GROUP_LHJBExt.add(ExecuteNodeAction.EXECUTE_NODE_ACTION);

    // Add lh jb ext groupp to the main Tools menu
    JBuilderMenu.GROUP_Tools.add(GROUP_LHJBExt);

    // Add lh jb ext group to the main toolbar
    Browser.addToolBarGroup(GROUP_LHJBExt);

    // Register our actions with the Project View's context menu
    ProjectView.registerContextActionProvider(new ContextActionProvider() {
      public Action getContextAction(Browser browser, Node[] nodes) {
        // Only return our actions if the selected nodes are all
        // JBProject nodes, otherwise return null.
        for (int i=0; i<nodes.length; i++) {
          if (!(nodes[i] instanceof JBProject))
            return null;
        }
        return GROUP_LHJBExt;
      }
    });

    RuntimeManager.registerRunner(new ExecutableRunner());

    PropertyManager.registerPropertyGroup(new CommandLinePropertyGroup());

  }

}
