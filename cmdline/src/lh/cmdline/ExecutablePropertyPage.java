package lh.cmdline;

import com.borland.primetime.properties.*;
import com.borland.primetime.help.HelpTopic;
import com.borland.primetime.node.*;
import java.util.*;
import com.borland.primetime.help.*;
import java.awt.*;


/**
 * <p>Title: Command line open tool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:51 $
 * 0.1, 26/04/02
 */

public class ExecutablePropertyPage extends PropertyPage
{
  static final String CATEGORY = "lh.cmdline";

  Node node = null;
  Map props = null;
  NodeProperty cmdprop = null;
  NodeProperty paramsprop = null;
  NodeProperty showretvalprop = null;
  NodeProperty showstdoutprop = null;
  NodeProperty showstderrprop = null;
  NodeProperty allowinputprop = null;
  NodeProperty echoinputprop = null;
  NodeProperty reusetabprop = null;
  NodeProperty workingdirprop = null;

  private BorderLayout borderLayout1 = new BorderLayout();
  private CommandLineDialogPanel jPanel1 = new CommandLineDialogPanel();

  ExecutablePropertyPage()
  {
    // for the designer
    this(null, null);
  }
  ExecutablePropertyPage(FileNode node)
  {
    // for the designer
    this(node, new HashMap());
    String cmd = cmdprop.getValue(node);
    if ((cmd == null) || (cmd.equals("")))
    {
      cmd = node.getUrl().getFileObject().getAbsolutePath();
      cmdprop.setValue(node, cmd);
    }
    jPanel1.disableCommand();
  }
  ExecutablePropertyPage(Node node, Map props)
  {
    this.node = node;
    this.props = props;

    cmdprop = new NodeProperty(CATEGORY, ExecutableRunner.COMMAND);
    paramsprop = new NodeProperty(CATEGORY, ExecutableRunner.PARAMETERS);
    showretvalprop = new NodeProperty(CATEGORY, ExecutableRunner.SHOWRETVAL, Boolean.TRUE.toString());
    showstdoutprop = new NodeProperty(CATEGORY, ExecutableRunner.SHOWSTDOUT, Boolean.TRUE.toString());
    showstderrprop = new NodeProperty(CATEGORY, ExecutableRunner.SHOWSTDERR, Boolean.TRUE.toString());
    allowinputprop = new NodeProperty(CATEGORY, ExecutableRunner.ALLOWINPUT, Boolean.FALSE.toString());
    echoinputprop = new NodeProperty(CATEGORY, ExecutableRunner.ECHOINPUT, Boolean.FALSE.toString());
    reusetabprop = new NodeProperty(CATEGORY, ExecutableRunner.ECHOINPUT, Boolean.FALSE.toString());
    workingdirprop = new NodeProperty(CATEGORY, ExecutableRunner.WORKING_DIRECTORY);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public void writeProperties()
  {
    String cmd = jPanel1.getTfCommand();
    String params = jPanel1.getTfParameters();
    boolean showretval = jPanel1.isShowRetVal();
    boolean showstdout = jPanel1.isStdOut();
    boolean showstderr = jPanel1.isStdErr();
    boolean allowinput = jPanel1.isStdIn();
    boolean echoinput = jPanel1.isEchoInput();
    boolean reusetab = jPanel1.isReuseTab();
    String workingdir = jPanel1.getTfWorkingDir();
    if (!jPanel1.useSpecificDirectory())
    {
      workingdir = null;
    }

    props.put(ExecutableRunner.COMMAND, cmd);
    props.put(ExecutableRunner.PARAMETERS, params);
    props.put(ExecutableRunner.SHOWRETVAL, new Boolean(showretval));
    props.put(ExecutableRunner.SHOWSTDOUT, new Boolean(showstdout));
    props.put(ExecutableRunner.SHOWSTDERR, new Boolean(showstderr));
    props.put(ExecutableRunner.ALLOWINPUT, new Boolean(allowinput));
    props.put(ExecutableRunner.ECHOINPUT, new Boolean(echoinput));
    props.put(ExecutableRunner.REUSETAB, new Boolean(reusetab));
    props.put(ExecutableRunner.WORKING_DIRECTORY, workingdir);

    cmdprop.setValue(node, cmd);
    paramsprop.setValue(node, params);
    showretvalprop.setValue(node, "" + showretval);
    showstdoutprop.setValue(node, "" + showstdout);
    showstderrprop.setValue(node, "" + showstderr);
    allowinputprop.setValue(node, "" + allowinput);
    echoinputprop.setValue(node, "" + echoinput);
    reusetabprop.setValue(node, "" + reusetab);
    workingdirprop.setValue(node, workingdir);
  }
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("doc/cmdlinedialog.html").toString());
  }
  public void readProperties()
  {
    jPanel1.setTfCommand(cmdprop.getValue(node));
    jPanel1.setTfParameters(paramsprop.getValue(node));
    jPanel1.setShowRetVal(showretvalprop.getValue(node).equalsIgnoreCase("true"));
    jPanel1.setStdOut(showstdoutprop.getValue(node).equalsIgnoreCase("true"));
    jPanel1.setStdErr(showstderrprop.getValue(node).equalsIgnoreCase("true"));
    jPanel1.setStdIn(allowinputprop.getValue(node).equalsIgnoreCase("true"));
    jPanel1.setEchoInput(echoinputprop.getValue(node).equalsIgnoreCase("true"));
    jPanel1.setReuseTab(reusetabprop.getValue(node).equalsIgnoreCase("true"));
    jPanel1.setTfWorkingDir(workingdirprop.getValue(node));

  }
  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    this.add(jPanel1,  BorderLayout.NORTH);
    this.setPreferredSize(new Dimension(480, 300));
  }

//  /**
//   * isPageValid
//   *
//   * @return boolean
//   * @todo Implement this com.borland.primetime.properties.PropertyPage method
//   */
//  public boolean isPageValid()
//  {
//    writeProperties();
//    return true;
//  }
}
