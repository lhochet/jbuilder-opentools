package lh.cmdline;

import java.awt.*;
import javax.swing.*;
import com.borland.jbuilder.ide.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import com.borland.primetime.ide.*;
import java.lang.reflect.*;
import java.util.*;
import com.borland.primetime.ide.macros.*;

/**
 * <p>Title: Command line open tool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:52 $
 * @created 18/09/02
 */

public class MacrosPanel extends JPanel implements ListSelectionListener
{
  private String[] macrosnames = null; //MacroTable.getMacros(MacroTable.GUI_MACRO);
  private String[] macrosdesc = null; //MacroTable.getMacroDescriptions(MacroTable.GUI_MACRO);
  private MacroTable[] mts = null;

  private JLabel jLabel1 = new JLabel();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JList lstMacros = new JList();
  private JLabel jLabel2 = new JLabel();
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JTextArea taCurVal = new JTextArea();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JCheckBox cbxValue = new JCheckBox();
  public MacrosPanel()
  {
    Browser b = Browser.getActiveBrowser();
    mts = MacroTableFactory.createMacroTables(b, b.getActiveProject(), MacroTable.GUI_MACRO);
    int sz = 0;
    String[][] macronamesarrays = new String[mts.length][];
    String[][] macrodescarrays = new String[mts.length][];
    for (int i = mts.length - 1; i >= 0; i--)
    {
      MacroTable mt = mts[i];
      macronamesarrays[i] = mt.getMacros(MacroTable.GUI_MACRO);
      macrodescarrays[i] = mt.getMacroDescriptions(MacroTable.GUI_MACRO);
      sz += macronamesarrays[i].length;
    }
    macrosnames = new String[sz];
    macrosdesc = new String[sz];
    sz = 0;
    for (int i = mts.length - 1; i >= 0; i--)
    {
      System.arraycopy(macronamesarrays[i], 0, macrosnames, sz, macronamesarrays[i].length);
      System.arraycopy(macrodescarrays[i], 0, macrosdesc, sz, macrodescarrays[i].length);
      sz += macronamesarrays[i].length;
    }
    try
    {
      jbInit();
      postJBInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    jLabel1.setText("Macros:");
    this.setLayout(gridBagLayout1);
    jLabel2.setToolTipText("");
    jLabel2.setText("Current value:");
    taCurVal.setEnabled(false);
    taCurVal.setEditable(false);
    taCurVal.setText("");
    taCurVal.setLineWrap(true);
    lstMacros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    cbxValue.setText("Insert value instead of macro");
    this.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                             new Insets(5, 10, 0, 0), 0, 0));
    this.add(jScrollPane1, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                                                  new Insets(5, 10, 0, 10), 0, 0));
    this.add(jScrollPane2, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0
                                                  , GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                                  new Insets(5, 10, 0, 10), 0, 83));
    this.add(jLabel2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                             new Insets(5, 10, 0, 0), 0, 0));
    this.add(cbxValue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
                                              , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                              new Insets(5, 10, 5, 10), 0, 0));
    jScrollPane2.getViewport().add(taCurVal, null);
    jScrollPane1.getViewport().add(lstMacros, null);
  }

  private void postJBInit()
  {
//    String[] macros = mt.getMacros(MacroTable.GUI_MACRO);
//    String[] macrosdesc = MacroTable.getMacroDescriptions(MacroTable.BUILD_MACRO);
//    for (int i = 0; i < macros.length; ++i)
//    {
//      macros[i] += " - " + macrosdesc[i];
//    }


    String[] macros = new String[macrosnames.length];
    for (int i = 0; i < macros.length; ++i)
    {
      macros[i] = macrosnames[i] + " - " + macrosdesc[i];
    }
    lstMacros.setListData(macros);
    lstMacros.addListSelectionListener(this);
    lstMacros.setSelectedIndex(0);
  }

  public void valueChanged(ListSelectionEvent e)
  {
    try
    {
//      String value = (String)mt.getMethod(macrosnames[lstMacros.getSelectedIndex()]).invoke(mt, null);
      String value = expand(macrosnames[lstMacros.getSelectedIndex()]);
      if (value == null)
      {
        taCurVal.setText("");
        taCurVal.setCaretPosition(0);
        return;
      }
      StringTokenizer tk = new StringTokenizer(value, ";");
      value = "";
      while (tk.hasMoreTokens())
      {
        value += tk.nextToken();
        if (tk.hasMoreTokens())
        {
          value += ";\n";
        }
      }
      taCurVal.setText(value);
      taCurVal.setCaretPosition(0);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public String getMacro()
  {
    return macrosnames[lstMacros.getSelectedIndex()];
  }

  public String getValue()
  {
    try
    {
      return expand(macrosnames[lstMacros.getSelectedIndex()]);
//      Browser b = Browser.getActiveBrowser();
//      MacroTable mt = new MacroTable(b.getActiveProject(), b, MacroTable.GUI_MACRO);
//      return (String)mt.getMethod(macrosnames[lstMacros.getSelectedIndex()]).invoke(mt, null);
    }
    catch (Exception ex)
    {
      return null;
    }
  }

  public boolean isInsertValue()
  {
    return cbxValue.isSelected();
  }

  public String expand(String name)
  {
    for (int i = 0; i < mts.length; i++)
    {
      try
      {
        Macro macro = mts[i].findMacro(name);
        if (macro != null)
        {
          return mts[i].expand(macro, null);
        }
      }
      catch (Exception ex)
      {
//      return null;
      }
    }
    return null;
  }

}
