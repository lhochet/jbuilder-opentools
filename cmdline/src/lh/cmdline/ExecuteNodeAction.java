
/**
 * Title:        Command line open tool<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:51 $
 * 0.4, 2/11/01
 * 0.3, 30/06/01
 */
package lh.cmdline;

import com.borland.primetime.help.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import com.borland.primetime.node.*;
import com.borland.primetime.properties.*;
import java.awt.*;
import java.io.*;

public class ExecuteNodeAction extends BrowserAction
{
  public static final ExecuteNodeAction EXECUTE_NODE_ACTION = new ExecuteNodeAction();


  private FileNode node = null;

  public ExecuteNodeAction()
  {
    super("Run command line...", 'R', "Allow to run an external program from JBuilder", BrowserIcons.ICON_CONSOLE);
  }

  public ExecuteNodeAction(FileNode node)
  {
    super("Execute...", 'E', "Execute the selected file...", BrowserIcons.ICON_RUN);
    this.node = node;
  }

  public void actionPerformed(Browser browser)
  {
    if (node == null)
    {
      CommandLineDialogPanel panel = new CommandLineDialogPanel();
      panel.setPreferredSize(new Dimension(480, 250));

//      String helptopiclocation = getClass().getResource("doc/cmdlinedialog.html").toString();
//      System.out.println("helptopiclocation = " + helptopiclocation);
//      String helptopicstr = helptopiclocation.substring(helptopiclocation.indexOf('!') + 1);
//      System.out.println("helptopic = " + helptopicstr);
//      String helpbookpath = helptopiclocation.substring(0, helptopiclocation.indexOf('!'));
//      System.out.println("helpbookpath = " + helpbookpath);
//      ZipHelpBook helpbook = new ZipHelpBook(helpbookpath);
//      ZipHelpBook helpbook = PrimetimeHelp.createBook("C:\\JBuilder7\\lib\\ext\\cmdline.jar");
      HelpTopic helptopic = PrimetimeHelp.createTopic(null, getClass().getResource("doc/cmdlinedialog.html").toString());
      if (DefaultDialog.showModalDialog(browser, "Run command", panel, helptopic));// /*null*/ /**/new ZipHelpTopic(helpbook, helptopic)/**/))
      {
        String cmd = panel.getTfCommand();
        if (cmd == null) return;
        if (cmd.equals("")) return;
        String prms = panel.getTfParameters();
        if (prms != null)
        {
          if (!prms.equals(""))
          {
            cmd += " " + prms;
          }
        }

        Runner runner = null;
        if (panel.useSpecificDirectory())
        {
          runner = new Runner(browser, cmd, panel.isShowRetVal(),
                              panel.isStdOut(), panel.isStdErr(),
                              panel.isStdIn(), panel.isEchoInput(),
                              new File(panel.getTfWorkingDir()), panel.isReuseTab());
        }
        else
        {
          runner = new Runner(browser, cmd, panel.isShowRetVal(),
                              panel.isStdOut(), panel.isStdErr(),
                              panel.isStdIn(), panel.isEchoInput(),
                              panel.isReuseTab());
        }

        Thread tmp = new Thread(runner);
        tmp.start();
      }
    }
    else
    {
      NodeProperty cmdprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.COMMAND);
      NodeProperty paramsprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.PARAMETERS);
      NodeProperty showretvalprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.SHOWRETVAL, Boolean.TRUE.toString());
      NodeProperty showstdoutprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.SHOWSTDOUT, Boolean.TRUE.toString());
      NodeProperty showstderrprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.SHOWSTDERR, Boolean.TRUE.toString());
      NodeProperty allowinputprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.ALLOWINPUT, Boolean.FALSE.toString());
      NodeProperty echoinputprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.ECHOINPUT, Boolean.FALSE.toString());
      NodeProperty workingdirprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.WORKING_DIRECTORY);

      String cmd = cmdprop.getValue(node);
      if ((cmd == null) || (cmd.equals("")))
      {
        cmd = node.getUrl().getFileObject().getAbsolutePath();
        cmdprop.setValue(node, cmd);
      }
      if (cmd.indexOf(' ') > 0) cmd = "\"" + cmd + "\"";
      String params = paramsprop.getValue(node);
      if (params != null)
      {
        if (!params.equals(""))
        {
          cmd += " " + params;
        }
      }
      boolean showretval = showretvalprop.getValue(node).equalsIgnoreCase("true");
      boolean showstdout = showstdoutprop.getValue(node).equalsIgnoreCase("true");
      boolean showstderr = showstderrprop.getValue(node).equalsIgnoreCase("true");
      boolean allowinput = allowinputprop.getValue(node).equalsIgnoreCase("true");
      boolean echoinput = echoinputprop.getValue(node).equalsIgnoreCase("true");
      String workingdir = workingdirprop.getValue(node);

      Runner runner = null;

      if ((workingdir != null) && (!workingdir.equals("")))
      {
        runner = new Runner(browser, cmd, showretval, showstdout, showstderr, allowinput, echoinput, new File(workingdir), true);
      }
      else
      {
        runner = new Runner(browser, cmd, showretval, showstdout, showstderr, allowinput, echoinput);
      }

      Thread tmp = new Thread(runner);
      tmp.start();
    }
  }
}
