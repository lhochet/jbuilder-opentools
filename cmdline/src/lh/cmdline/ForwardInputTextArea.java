
/**
 * Title:        Command line open tool<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:52 $
 * 0.4, 15/10/01
 * 0.3, 30/06/01
 */
package lh.cmdline;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.plaf.TextUI;

public class ForwardInputTextArea extends JTextPane implements KeyListener
{
  private final int MAXLINELENGTH = 10000;

  class PopupListener extends MouseAdapter
  {
    public void mousePressed(MouseEvent e)
    {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e)
    {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e)
    {
      if (e.isPopupTrigger())
      {
        popup.show(e.getComponent(), e.getX(), e.getY());
      }
    }
  }

  Runner runner = null;
  boolean forward = false;
  boolean echoInput = false;
  SimpleAttributeSet outTxtAttr = null;
  SimpleAttributeSet errTxtAttr = null;
  Document doc = null;
  JPopupMenu popup = null;
  JCheckBoxMenuItem wrapmenuitem = null;

  boolean wrap = false;
  private ForwardInputTextArea instance = null;

  private ForwardInputTextAreaEditorKit kit = null;

  public ForwardInputTextArea(Runner runner)
  {
    kit = new ForwardInputTextAreaEditorKit();
    setEditorKit(kit);

    this.runner = runner;
    instance = this;
    addKeyListener(this);
    outTxtAttr = new SimpleAttributeSet();
    outTxtAttr.addAttribute(StyleConstants.Foreground, Color.black);
    errTxtAttr = new SimpleAttributeSet();
    errTxtAttr.addAttribute(StyleConstants.Foreground, Color.red);
    doc = getDocument();

    popup = new JPopupMenu();

//clear
    JMenuItem menuitem = new JMenuItem("Clear All");
    menuitem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
//        System.out.println("clear");
        instance.setText("");
      }
    });
    popup.add(menuitem);
//copy
    menuitem = new JMenuItem("Copy All");
    menuitem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
//        System.out.println("copy");
        instance.selectAll();
        instance.copy();
      }
    });
    popup.add(menuitem);
// wrap
    wrapmenuitem = new JCheckBoxMenuItem("Word Wrap");
    wrapmenuitem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        System.out.println("wrap changed");
        wrap = !wrap;
        wrapmenuitem.setState(wrap);
        kit.setWrap(wrap);
        instance.invalidate();
        instance.updateUI();
      }
    });
    popup.add(wrapmenuitem);

    MouseListener popupListener = new PopupListener();
    addMouseListener(popupListener);
  }

  public void setForward(boolean forward)
  {
    this.forward = forward;
  }

  public void setEchoInput(boolean echoInput)
  {
    this.echoInput = echoInput;
  }

  public void setRunner(Runner runner)
  {
    this.runner = runner;
  }

  public void keyPressed(KeyEvent ev)
  {
//    System.out.println("keyPressed");
//    System.out.println("ev char = @" + ev.getKeyCode() + "@, char = @" + ev.getKeyChar() + "@");
//    System.out.println("ev char = @" + ev.getKeyCode() + "@");
//    if (forward && (runner != null)) runner.sendKey(ev.getKeyCode());

  }

  public void keyReleased(KeyEvent ev)
  {
//    System.out.println("keyReleased");
//    System.out.println("ev char = @" + ev.getKeyCode() + "@, char = @" + ev.getKeyChar() + "@");
    //nothing
  }

  public void keyTyped(KeyEvent ev)
  {
//    System.out.println("keyTyped");
    System.out.println("ev char = @" + ev.getKeyCode() + "@, char = @" + ev.getKeyChar() + "@");
    if (Character.isISOControl(ev.getKeyChar()) /*&& (ev.getKeyChar() != '\n')*/)
    {
      byte ctrlcode = /*Character.getNumericValue(ev.getKeyChar()); //*/ (byte)ev.getKeyChar(); // - 'A';
      System.out.println("ctrl code = " + ctrlcode);
      if (forward && (runner != null)) runner.sendControlKey(ctrlcode);
    }
    else
    {
      if (forward && (runner != null)) runner.sendKey(ev.getKeyChar());
    }

    if (!echoInput) ev.setKeyChar((char)0);
//    ev.setKeyCode(0);
  }

  synchronized public void appendOut(String txt)
  {
//    synchronized (doc)
//    {
      try
      {
//      if (true) {doc.insertString(doc.getLength(), txt, outTxtAttr);paintImmediately(getBounds());return;}
//      int len = doc.getLength();
        int left = txt.length();
        int inserted = 0;
//        int off = 0;
//        while (left > 0)
//        {
          int len2 = doc.getLength();
//        System.out.println("left = " + left);
//        System.out.println("inserted = " + inserted);
//        System.out.println("off = " + off);
          inserted = left;// - MAXLINELENGTH > 0 ? MAXLINELENGTH : left;
//          left -= inserted;
//        System.out.println("<<<<<<<<<<" + txt.substring(off, off + inserted) + ">>>>>>>");
          //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          doc.insertString(len2, txt, outTxtAttr);
//          doc.insertString(len2, txt.substring(off, off + inserted), outTxtAttr);
//          doc.insertString(len2, txt.substring(off, off + inserted)+"\n", outTxtAttr);
//          off += inserted;
//        updateUI();
          paintImmediately(getBounds());
          setCaretPosition(len2 + inserted);
//        }
//      scrollRectToVisible(new Rectangle(getLocation()));
//      updateUI();
      }
      catch (BadLocationException blex)
      {
        blex.printStackTrace();
      }
//    }
//    append(txt);
  }

  synchronized public void appendErr(String txt)
  {
//    synchronized (doc)
//    {
      try
      {
        int left = txt.length();
        int inserted = 0;
//        int off = 0;
//        while (left > 0)
//        {
          int len2 = doc.getLength();
          if (len2 > 0)
          {
            String s = doc.getText(len2, 1);
            char c = s.charAt(0);
//            if (!(c == '\n' || c == '\r')) doc.insertString(len2, "\n", errTxtAttr);
//            len2++;
          }
          inserted = left;// - MAXLINELENGTH > 0 ? MAXLINELENGTH : left;
//          left -= inserted;
          doc.insertString(len2, txt, errTxtAttr);
//          doc.insertString(len2, txt.substring(off, off + inserted), errTxtAttr);
//          doc.insertString(len2, txt.substring(off, off + inserted)+"\n", errTxtAttr);
//          off += inserted;
          paintImmediately(getBounds());
          setCaretPosition(len2 + inserted);
//        }
      }
      catch (BadLocationException blex)
      {
        blex.printStackTrace();
      }
//    }
  }

/** /
  public Dimension getPreferredSize()
  {
    Dimension ret = super.getPreferredSize();
    if (true) return ret;
    if (!wrap)
    {
      if (getParent() instanceof JViewport)
      {
          JViewport port = (JViewport)getParent();
          TextUI ui = getUI();
          int w = port.getWidth();
          Dimension min = ui.getMinimumSize(this);
          Dimension max = ui.getMaximumSize(this);
          Dimension cur = ui.getPreferredSize(this);

          System.out.println("cur width = " + cur.width);
          System.out.println("port width = " + w);
          System.out.println("max width = " + max.width);
          System.out.println("min width = " + min.width);

//          if ((w >= min.width) && (w <= max.width))
          {
            // && cur.width < maxwidth
            if (cur.width > w) ret.width = cur.width;
          }
//          int maxwidth = 150000;
//          System.out.println("cur width = " + cur.width);
//          System.out.println("port width = " + w);
//          System.out.println("max width = " + max.width);

//          ret.width = MAXLINELENGTH; // 15000; //cur.width;
      }
    }
    return ret;
  }
/**/

  public boolean getScrollableTracksViewportWidth()
  {
//    if (true) return (wrap) ? true : false; //super.getScrollableTracksViewportWidth();
    if (true) return super.getScrollableTracksViewportWidth();
    if (getParent() instanceof JViewport)
    {
        JViewport port = (JViewport)getParent();
        TextUI ui = getUI();
        int w = port.getWidth();
        Dimension min = ui.getMinimumSize(this);
        Dimension max = ui.getMaximumSize(this);
        Dimension cur = ui.getPreferredSize(this);

//        int maxwidth = 150000;
//        System.out.println("cur width = " + cur.width);
//        System.out.println("port width = " + w);
//        System.out.println("max width = " + max.width);

        if ((w >= min.width) && (w <= max.width))
        {
          // && cur.width < maxwidth
          if (!wrap && cur.width > w) return false;
          return true;
        }
    }
    return false;
  }

  /**
   * Returns the preferred size of the viewport for a view component.
   *
   * @return The preferredSize of a JViewport whose view is this Scrollable.
   * @todo Implement this javax.swing.Scrollable method
   */
/*
  public Dimension getPreferredScrollableViewportSize()
  {
    Dimension ret =  super.getPreferredScrollableViewportSize();
    if (true) return ret;
    if (!wrap)
    {
      if (getParent() instanceof JViewport)
      {
          JViewport port = (JViewport)getParent();
          TextUI ui = getUI();
          int w = port.getWidth();
          Dimension min = ui.getMinimumSize(this);
          Dimension max = ui.getMaximumSize(this);
          Dimension cur = ui.getPreferredSize(this);

          System.out.println("2cur width = " + cur.width);
          System.out.println("2port width = " + w);
          System.out.println("2max width = " + max.width);
          System.out.println("2min width = " + min.width);

//          if ((w >= min.width) && (w <= max.width))
          {
            // && cur.width < maxwidth
            if (cur.width > w) ret.width = cur.width;
          }
//          int maxwidth = 150000;
//          System.out.println("cur width = " + cur.width);
//          System.out.println("port width = " + w);
//          System.out.println("max width = " + max.width);

//          ret.width = MAXLINELENGTH; // 15000; //cur.width;
      }
    }
    return ret;
  }
*/
}
