package lh.cmdline;

import com.borland.primetime.properties.PropertyPageFactory;
import com.borland.primetime.node.Project;
import java.util.Map;
import com.borland.primetime.ide.Browser;
import com.borland.primetime.properties.*;
import java.io.*;

/**
 * <p>Title: Command line open tool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 26/04/02
 */

public class ExecutableRunner implements com.borland.primetime.runtime.Runner
{
  final static String COMMAND = "command";
  final static String PARAMETERS = "parameters";
  final static String SHOWRETVAL = "showretval";
  final static String SHOWSTDOUT = "showstdout";
  final static String SHOWSTDERR = "showstderr";
  final static String ALLOWINPUT = "allowinput";
  final static String ECHOINPUT = "echoinput";
  final static String REUSETAB = "reusetab";
  final static String WORKING_DIRECTORY = "workingdir";

  private static Project sprj = null;
  private static Map sprops = null;

  public PropertyPageFactory getPageFactory(Project prj, Map props)
  {
    sprj = prj;
    sprops = props;
    return new PropertyPageFactory("Executable")
    {
      public PropertyPage createPropertyPage()
      {
        ExecutablePropertyPage epp = new ExecutablePropertyPage(sprj, sprops);
        sprj = null;
        sprops = null;
        return epp;
      }
    };
  }
  public boolean isValid(Browser browser, Project prj, Map props, boolean debug)
  {
    NodeProperty cmdprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.COMMAND);
    NodeProperty paramsprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.PARAMETERS);
    NodeProperty showretvalprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.SHOWRETVAL, Boolean.TRUE.toString());
    NodeProperty showstdoutprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.SHOWSTDOUT, Boolean.TRUE.toString());
    NodeProperty showstderrprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.SHOWSTDERR, Boolean.TRUE.toString());
    NodeProperty allowinputprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.ALLOWINPUT, Boolean.FALSE.toString());
    NodeProperty echoinputprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.ECHOINPUT, Boolean.FALSE.toString());
    NodeProperty reusetabprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.REUSETAB, Boolean.FALSE.toString());
    NodeProperty workingdirprop = new NodeProperty(ExecutablePropertyPage.CATEGORY, ExecutableRunner.WORKING_DIRECTORY);

    String cmd = cmdprop.getValue(prj);
    if ((cmd == null) || (cmd.equals(""))) return false;
    String params = paramsprop.getValue(prj);
    boolean showretval = showretvalprop.getValue(prj).equalsIgnoreCase("true");
    boolean showstdout = showstdoutprop.getValue(prj).equalsIgnoreCase("true");
    boolean showstderr = showstderrprop.getValue(prj).equalsIgnoreCase("true");
    boolean allowinput = allowinputprop.getValue(prj).equalsIgnoreCase("true");
    boolean echoinput = echoinputprop.getValue(prj).equalsIgnoreCase("true");
    boolean reusetab = reusetabprop.getValue(prj).equalsIgnoreCase("true");
    String workingdir = workingdirprop.getValue(prj);

    props.put(ExecutableRunner.COMMAND, cmd);
    props.put(ExecutableRunner.PARAMETERS, params);
    props.put(ExecutableRunner.SHOWRETVAL, new Boolean(showretval));
    props.put(ExecutableRunner.SHOWSTDOUT, new Boolean(showstdout));
    props.put(ExecutableRunner.SHOWSTDERR, new Boolean(showstderr));
    props.put(ExecutableRunner.ALLOWINPUT, new Boolean(allowinput));
    props.put(ExecutableRunner.ECHOINPUT, new Boolean(echoinput));
    props.put(ExecutableRunner.REUSETAB, new Boolean(reusetab));
    props.put(ExecutableRunner.WORKING_DIRECTORY, workingdir);

    return true;
  }

  private boolean getBool(Map props, String what)
  {
    Object o = props.get(what);
    if (o == null) return false;
    return ((Boolean)o).booleanValue();
  }
  public void run(Browser browser, Project prj, Map props, boolean debug)
  {
    String cmd = (String)props.get(COMMAND);
    if ((cmd == null) || (cmd.equals(""))) return;
    String params = (String)props.get(PARAMETERS);
    if ((params != null) && (!params.equals("")))
    {
      cmd += " " + params;
    }

    boolean showretval = getBool(props, SHOWRETVAL);
    boolean showstdout = getBool(props, SHOWSTDOUT);
    boolean showstderr = getBool(props, SHOWSTDERR);
    boolean allowinput = getBool(props, ALLOWINPUT);
    boolean echoinput = getBool(props, ECHOINPUT);
    boolean reusetab = getBool(props, REUSETAB);

    Runner runner = null;
    String workingdir = (String)props.get(WORKING_DIRECTORY);
    if ((workingdir != null) && (!workingdir.equals("")))
    {
      runner = new Runner(browser, cmd, showretval, showstdout, showstderr, allowinput, echoinput, new File(workingdir), reusetab);
    }
    else
    {
      runner = new Runner(browser, cmd, showretval, showstdout, showstderr, allowinput, echoinput, reusetab);
    }

    Thread tmp = new Thread(runner);
    tmp.start();
  }
}
