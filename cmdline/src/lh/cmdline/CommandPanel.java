
/**
 * Title:        Command line open tool<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @version 0.3, 30/06/01
 */
package lh.cmdline;

import javax.swing.*;
import java.awt.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.actions.*;
import java.awt.event.*;
import javax.swing.border.*;


public class CommandPanel extends JPanel
{


  private Runner runner = null;

  private JScrollPane jScrollPane1 = new JScrollPane();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  private ActionButton btnRun = null;
  private ActionButton btnStop = null;
  private FlowLayout flowLayout1 = new FlowLayout();
  private ForwardInputTextArea ta = null;
  private Border border1;

  public CommandPanel(Runner runner)
  {
    this.runner = runner;
    ta = new ForwardInputTextArea(runner);

    try
    {
      preJBInit();
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  public ForwardInputTextArea getTextArea()
  {
    return ta;
  }

  public ActionButton getStopButton()
  {
    return btnStop;
  }

  public ActionButton getRunButton()
  {
    return btnRun;
  }

  private void preJBInit()
  {
    btnRun = new ActionButton(this, new BrowserAction("Run", 'R', "Run the current program.", BrowserIcons.ICON_PROGRAMRUN)
                                         {
                                           public void actionPerformed(Browser browser)
                                           {
                                             btnStop.setEnabled(true);
                                             btnRun.setEnabled(false);
                                             run();
                                           }
                                         }
                                );
    btnStop = new ActionButton(this, new BrowserAction("Stop", 'S', "Stop the current program.", BrowserIcons.ICON_PROGRAMSTOP)
                                         {
                                           public void actionPerformed(Browser browser)
                                           {
                                             btnRun.setEnabled(true);
                                             btnStop.setEnabled(false);
                                             stop();
                                           }
                                         }
                                );
  }

  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.white,Color.white,new Color(142, 142, 142),new Color(99, 99, 99));
    this.setLayout(borderLayout1);
    jPanel1.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    this.add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(ta, null);
    this.add(jPanel1, BorderLayout.SOUTH);
    jPanel1.add(btnStop, null);
    jPanel1.add(btnRun, null);
  }

  void stop()
  {
    btnStop.setEnabled(false);
    btnRun.setEnabled(true);
    runner.exit();
  }

  void run()
  {
    btnStop.setEnabled(true);
    btnRun.setEnabled(false);
    Thread tmp = new Thread(runner);
    tmp.start();
  }

}