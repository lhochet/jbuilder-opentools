
/**
 * Title:        Command line open tool<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @author Ludovic HOCHET
 * @version 0.3, 30/06/01
 */
package lh.cmdline;

import javax.swing.*;
import com.borland.primetime.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;

public class ExeFileNode extends FileNode
{

  public static final Icon ICON = BrowserIcons.ICON_FILEBINARY;

  public ExeFileNode(Project project, Node parent, Url url) throws DuplicateNodeException
  {
    super(project, parent, url);
  }

  public Icon getDisplayIcon()
  {
    return ICON;
  }

  public static void initOpenTool(byte major, byte minor)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH ExeFileNode 0.3");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + major + "." + minor);
    }

    try
    {
      Icon ICON = BrowserIcons.ICON_FILEBINARY;

      registerFileNodeClass("exe", "Executable Files", ExeFileNode.class, ICON);

      ProjectView.registerContextActionProvider
      (
        new ContextActionProvider()
        {
          public Action getContextAction(Browser browser, Node[] nodes)
          {
            if (browser.getActiveProject() == null) return null;
            if (nodes.length > 1) return null;
            if (nodes[0] instanceof ExeFileNode) return new ExecuteNodeAction((ExeFileNode)nodes[0]);
            if (nodes[0] instanceof FileNode)
            {
              if (((FileNode)nodes[0]).getUrl().getFileExtension().toUpperCase().equals("EXE")) return new ExecuteNodeAction((FileNode)nodes[0]);
            }
            return null;
          }
        }
      );
    }
    catch (Exception ex)
    {
      System.out.println("ExeFileNode.initOpenTool: exception occured");
      ex.printStackTrace();
    }
  }

}