package lh.cmdline;

import javax.swing.Icon;
import com.borland.primetime.ide.*;
import java.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:50 $
 * 0.1, 19/04/03
 */

public class CmdMessageCategory extends MessageCategory
{
  private static Map cats = new HashMap();

  private CommandPanel cpanel = null;
  private Runner runner = null;

  public static CmdMessageCategory getCat(String cmd)
  {
    return (CmdMessageCategory)cats.get(cmd);
  }

  public static void putCat(String cmd, CmdMessageCategory cat)
  {
    cats.put(cmd, cat);
  }

//  public CmdMessageCategory(String p0, String p1, Icon p2, String p3)
//  {
//    super(p0, p1, p2, p3);
//  }
//
//  public CmdMessageCategory(String p0, String p1, Icon p2)
//  {
//    super(p0, p1, p2);
//  }

  public CmdMessageCategory(String title, String tooltip, CommandPanel cpanel, Runner runner)
  {
    super(title, tooltip);
    this.cpanel = cpanel;
    this.runner = runner;
  }
  public void categoryClosing() throws com.borland.primetime.util.VetoException
  {
    super.categoryClosing();
    cats.remove(getToolTip());
  }
  public CommandPanel getCPanel()
  {
    return cpanel;
  }
  public Runner getRunner()
  {
    return runner;
  }
  public void setRunner(Runner runner)
  {
    this.runner = runner;
  }

//  public CmdMessageCategory(String p0, Icon p1)
//  {
//    super(p0, p1);
//  }
//
//  public CmdMessageCategory(String p0)
//  {
//    super(p0);
//  }
}