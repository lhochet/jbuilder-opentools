package lh.cmdline;

import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;

/**
 * Title:        Command line open tool<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:50 $
 * 0.4, 27/10/01
 * 0.3, 1/07/01
 */

public class CommandLineDialogPanel extends JPanel
{
  private Border border1;
  private TitledBorder titledBorder1;
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JCheckBox cbxReuseTab = new JCheckBox();
  private JButton btnMacro = new JButton();
  private JCheckBox cbxStdErr = new JCheckBox();
  private JCheckBox cbxEchoInput = new JCheckBox();
  private JLabel jLabel1 = new JLabel();
  private JCheckBox cbxStdOut = new JCheckBox();
  private JButton btnBrowse = new JButton();
  private JCheckBox cbxShowRetVal = new JCheckBox();
  private JCheckBox cbxStdIn = new JCheckBox();
  private JLabel jLabel2 = new JLabel();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private JTextField tfCommand = new JTextField();
  private JTextField tfParameters = new JTextField();
  private JPanel jPanel2 = new JPanel();
  private JPanel jPanel3 = new JPanel();
  private Border border2;
  private TitledBorder titledBorder2;
  private JRadioButton rbDefault = new JRadioButton();
  private JRadioButton rbSpecified = new JRadioButton();
  private JTextField tfWorkingDir = new JTextField();
  private JButton btnBrowseDir = new JButton();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private ButtonGroup bgWorkingDir = new ButtonGroup();

  public CommandLineDialogPanel()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(Color.gray,1);
    titledBorder1 = new TitledBorder(border1,"Options");
    border2 = BorderFactory.createLineBorder(Color.gray,1);
    titledBorder2 = new TitledBorder(border2,"Working Directory");
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    cbxReuseTab.setText("Reuse tab with the same command");
    cbxReuseTab.setSelected(true);
    btnMacro.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnMacro_actionPerformed(e);
      }
    });
    btnMacro.setText("Add {Macro}");
    btnMacro.setMargin(new Insets(2, 2, 2, 2));
    cbxStdErr.setText("Show stderr messages");
    cbxStdErr.setSelected(true);
    cbxEchoInput.setText("Echo input");
    cbxEchoInput.setSelected(true);
    cbxEchoInput.setEnabled(false);
    jLabel1.setText("Command:");
    cbxStdOut.setText("Show stdout messages");
    cbxStdOut.setSelected(true);
    btnBrowse.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        btnBrowse_actionPerformed(e);
      }
    });
    btnBrowse.setText("...");
    cbxShowRetVal.setText("Show returned value");
    cbxShowRetVal.setSelected(true);
    cbxStdIn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cbxStdIn_actionPerformed(e);
      }
    });
    cbxStdIn.setText("Allow input");
    jLabel2.setText("Parameters:");
    jPanel2.setLayout(gridBagLayout2);
    jPanel2.setBorder(titledBorder1);
    jPanel3.setBorder(titledBorder2);
    jPanel3.setLayout(gridBagLayout3);
    rbDefault.setSelected(true);
    rbDefault.setText("Default");
    rbDefault.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rbDefault_actionPerformed(e);
      }
    });
    rbSpecified.setText("Specified:");
    rbSpecified.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rbSpecified_actionPerformed(e);
      }
    });
    btnBrowseDir.setEnabled(false);
    btnBrowseDir.setText("...");
    btnBrowseDir.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBrowseDir_actionPerformed(e);
      }
    });
    tfWorkingDir.setEnabled(false);
    tfWorkingDir.setText("");
    this.add(jPanel1,  BorderLayout.CENTER);
    jPanel2.add(cbxShowRetVal, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    jPanel2.add(cbxStdErr, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    jPanel2.add(cbxStdOut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    jPanel2.add(cbxStdIn, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    jPanel2.add(cbxEchoInput, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 2, 0), 0, 0));
    jPanel2.add(cbxReuseTab, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    jPanel1.add(jPanel3,    new GridBagConstraints(0, 3, 3, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jLabel2,              new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(tfCommand,               new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(tfParameters,               new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(btnMacro,          new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
    jPanel1.add(jLabel1,         new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(btnBrowse,          new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
    jPanel1.add(jPanel2,        new GridBagConstraints(0, 4, 3, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    jPanel3.add(rbDefault,    new GridBagConstraints(0, 0, 3, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel3.add(rbSpecified,       new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 1, 0));
    jPanel3.add(tfWorkingDir,      new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    jPanel3.add(btnBrowseDir,          new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 6, 5), 0, -3));
    bgWorkingDir.add(rbDefault);
    bgWorkingDir.add(rbSpecified);
  }

  void btnBrowse_actionPerformed(ActionEvent e)
  {
    UrlChooser chooser = new UrlChooser(this, Browser.getActiveBrowser().getActiveProject().getUrl());
    chooser.setTitle("Choose file to execute...");
    chooser.show();
    Url url = chooser.getUrl();

//    Url url = UrlChooser.promptForUrl(this, Browser.getActiveBrowser().getActiveProject().getUrl());
    if (url != null) tfCommand.setText(url.getFullName());
  }

  void cbxStdIn_actionPerformed(ActionEvent e)
  {
    cbxEchoInput.setEnabled(cbxStdIn.isSelected());
  }

  void btnMacro_actionPerformed(ActionEvent e)
  {
    MacrosPanel panel = new MacrosPanel();
    if (DefaultDialog.showModalDialog(this, "Select Macro", panel, null))
    {
      String res = tfParameters.getText();
      res += " ";
      if (panel.isInsertValue())
      {
        res += panel.getValue();
      }
      else
      {
        res += "(" + panel.getMacro() + ")";
      }
      tfParameters.setText(res);
    } //if
  }

  void btnBrowseDir_actionPerformed(ActionEvent e)
  {
//    UrlChooser chooser = new UrlChooser(this, Browser.getActiveBrowser().getActiveProject().getUrl());
//    chooser.setTitle("Choose Working Directory...");
//    chooser.show();
//    Url url = chooser.getUrl();
    String cdir = tfWorkingDir.getText();
    if ((cdir == null) || (cdir.length() == 0))
    {
      cdir = System.getProperty("user.dir");
    }
    Url url = new Url(new File(cdir));
    url = UrlChooser.promptForDir(this, url, "Choose working directory...");
    if (url != null) tfWorkingDir.setText(url.getFullName());
  }
  public boolean isEchoInput()
  {
    return cbxEchoInput.isSelected();
  }
  public boolean isReuseTab()
  {
    return cbxReuseTab.isSelected();
  }
  public boolean isShowRetVal()
  {
    return cbxShowRetVal.isSelected();
  }
  public boolean isStdErr()
  {
    return cbxStdErr.isSelected();
  }
  public boolean isStdIn()
  {
    return cbxStdIn.isSelected();
  }
  public boolean isStdOut()
  {
    return cbxStdOut.isSelected();
  }
  public boolean useSpecificDirectory()
  {
    return rbSpecified.isSelected();
  }
  public String getTfCommand()
  {
    return tfCommand.getText();
  }
  public String getTfParameters()
  {
    return tfParameters.getText();
  }
  public String getTfWorkingDir()
  {
    return tfWorkingDir.getText();
  }

  public void disableCommand()
  {
    tfCommand.setEnabled(false);
    btnBrowse.setVisible(false);
  }

  void rbDefault_actionPerformed(ActionEvent e)
  {
    tfWorkingDir.setEnabled(false);
    btnBrowseDir.setEnabled(false);
  }

  void rbSpecified_actionPerformed(ActionEvent e)
  {
    tfWorkingDir.setEnabled(true);
    btnBrowseDir.setEnabled(true);
  }
  public void setEchoInput(boolean echoInput)
  {
    cbxEchoInput.setSelected(echoInput);
  }
  public void setReuseTab(boolean reuseTab)
  {
    cbxReuseTab.setSelected(reuseTab);
  }
  public void setShowRetVal(boolean showRetVal)
  {
    cbxShowRetVal.setSelected(showRetVal);
  }
  public void setStdErr(boolean stdErr)
  {
    cbxStdErr.setSelected(stdErr);
  }
  public void setStdIn(boolean stdIn)
  {
    cbxStdIn.setSelected(stdIn);
  }
  public void setStdOut(boolean stdOut)
  {
    cbxStdOut.setSelected(stdOut);
  }
  public void setTfCommand(String tfCommand)
  {
    this.tfCommand.setText(tfCommand);
  }
  public void setTfParameters(String tfParameters)
  {
    this.tfParameters.setText(tfParameters);
  }
  public void setTfWorkingDir(String tfWorkingDir)
  {
    if ((tfWorkingDir != null) && (tfWorkingDir.length() > 0))
    {
      rbSpecified.setSelected(true);
      this.tfWorkingDir.setEnabled(true);
      btnBrowseDir.setEnabled(true);
    }
    this.tfWorkingDir.setText(tfWorkingDir);
  }

}
