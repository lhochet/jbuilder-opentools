
/**
 * Title:        Command line open tool<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @author Ludovic HOCHET
 * @version 0.3, 30/06/01
 */
package lh.cmdline;

import java.awt.*;
import java.io.*;
import javax.swing.*;
import javax.swing.text.*;

public class OutputListener extends Thread
{
  /**/BufferedReader stream = null;
  BufferedReader errstream = null;
  ForwardInputTextArea ta = null;
  boolean running = true;

  public OutputListener(/**/BufferedReader stream, BufferedReader errstream, ForwardInputTextArea ta)
  {
    this.stream = stream;
    this.errstream = errstream;
    this.ta = ta;
  }

  public void run()
  {
    try
    {
      ta.setAutoscrolls(true);
      while (running)
      {
//        System.out.println("&& running");
        if (stream.ready())
        {
//          System.out.println("&& out ready");
          StringBuffer sb = new StringBuffer("");
          while (stream.ready())
          {
            char c = (char)stream.read();
            sb.append(c);
            if (c == '\n')
            {
              break;
            }
          }
//          if (nbl == 0) sb.append('\n');

          String tmp = sb.toString();
//          String tmp = stream.readLine();
//          if (com.borland.primetime.PrimeTime.isVerbose()) System.out.println("o*" + tmp + "*");
          if ((tmp != null) && (tmp.length() > 0)) ta.appendOut(tmp); // +"\n");
          ta.repaint();
        }
/*
        System.out.println("&& errr");
        if (errstream.ready())
        {
          System.out.println("&& err ready");
          StringBuffer sb = new StringBuffer("");
          while (errstream.ready())
          {
//            System.out.printl("buf = " + sb);
            sb.append((char)errstream.read());
          }

          String tmp = sb.toString();
//          String tmp = errstream.readLine();
          if (com.borland.primetime.PrimeTime.isVerbose()) System.out.println("e*" + tmp + "*");
          if ((tmp != null) && (tmp.length() > 0)) ta.appendErr(tmp);
//          if ((tmp != null) && (tmp.length() > 0)) ta.appendErr(tmp +"\n");
          ta.repaint();
        }
*/
        ta.setCaretPosition(ta.getText().length());
//        ta.updateUI();

        this.yield();
        try
        {
          // slow down thread only if there isn't already something to display
//          if (!(stream.ready() || errstream.ready())) Thread.sleep(50);
          if (!stream.ready()) Thread.sleep(50);
        }
        catch (InterruptedException sleepex)
        {
        }


/*
        while (stream.available() > 0)
        {
          int c = stream.read();
          if (c > 0)
          {
//            synchronized (ta)
//            {
              ta.append("" + (char)c);
//            }
          }
          //this.yield();
        }

        while (errstream.available() > 0)
        {
          int c = errstream.read();
          if (c > 0)
          {
//            synchronized (ta)
//            {
              ta.append("" + (char)c);
//            }
          }
          //this.yield();
        }
*/
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void exit()
  {
    running = false;
  }

  public boolean available()
  {
    try
    {
      return stream.ready() || errstream.ready();
    }
    catch (Exception ex)
    {
      return false;
    }

  }

}
