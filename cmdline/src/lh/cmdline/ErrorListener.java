package lh.cmdline;

import java.io.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:51 $
 */

public class ErrorListener extends Thread
{
  BufferedReader errstream = null;
  ForwardInputTextArea ta = null;
  boolean running = true;

  public ErrorListener(BufferedReader errstream, ForwardInputTextArea ta)
  {
    this.errstream = errstream;
    this.ta = ta;
  }

  public void run()
  {
    try
    {
      ta.setAutoscrolls(true);
      while (running)
      {
        try
        {
          // slow down thread only if there isn't already something to display
          if (!errstream.ready()) Thread.sleep(100);
        }
        catch (InterruptedException sleepex)
        {
        }
//        System.out.println("&& running ERR");
        if (errstream.ready())
        {
//          System.out.println("&& err ready");
          StringBuffer sb = new StringBuffer("");
          while (errstream.ready())
          {
//            System.out.printl("buf = " + sb);
//            sb.append((char)errstream.read());
            char c = (char)errstream.read();
            sb.append(c);
            if (c == '\n')
            {
              break;
            }

          }

          String tmp = sb.toString();
//          String tmp = errstream.readLine();
          if (com.borland.primetime.PrimeTime.isVerbose()) System.out.println("e*" + tmp + "*");
          if ((tmp != null) && (tmp.length() > 0)) ta.appendErr(tmp);
//          if ((tmp != null) && (tmp.length() > 0)) ta.appendErr(tmp +"\n");
          ta.repaint();
        }

        ta.setCaretPosition(ta.getText().length());
//        ta.updateUI();

        this.yield();
//        try
//        {
//          // slow down thread only if there isn't already something to display
//          if (!errstream.ready()) Thread.sleep(50);
//        }
//        catch (InterruptedException sleepex)
//        {
//        }


/*
        while (stream.available() > 0)
        {
          int c = stream.read();
          if (c > 0)
          {
//            synchronized (ta)
//            {
              ta.append("" + (char)c);
//            }
          }
          //this.yield();
        }

        while (errstream.available() > 0)
        {
          int c = errstream.read();
          if (c > 0)
          {
//            synchronized (ta)
//            {
              ta.append("" + (char)c);
//            }
          }
          //this.yield();
        }
*/
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void exit()
  {
    running = false;
  }

  public boolean available()
  {
    try
    {
      return errstream.ready();
    }
    catch (Exception ex)
    {
      return false;
    }

  }

}
