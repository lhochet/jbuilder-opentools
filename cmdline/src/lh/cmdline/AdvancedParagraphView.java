package lh.cmdline;

import javax.swing.text.*;
import java.awt.*;
import javax.swing.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:49 $
 */

public class AdvancedParagraphView extends ParagraphView
{

  public AdvancedParagraphView(Element elem)
  {
    super(elem);

    strategy = new AdvancedFlowStrategy();
  }

  protected View createRow()
  {
    Element elem = getElement();
    return new AdvancedRow(elem);
  }

  public View createPublicRow()
  {
    return createRow();
  }

  protected static int getSpaceCount(String content)
  {
    int result = 0;
    int index = content.indexOf(' ');
    while (index >= 0)
    {
      result++;
      index = content.indexOf(' ', index + 1);
    }
    return result;
  }

  protected static int[] getSpaceIndexes(String content, int shift)
  {
    int cnt = getSpaceCount(content);
    int[] result = new int[cnt];
    int counter = 0;
    int index = content.indexOf(' ');
    while (index >= 0)
    {
      result[counter] = index + shift;
      counter++;
      index = content.indexOf(' ', index + 1);
    }
    return result;
  }

  static class AdvancedFlowStrategy extends FlowStrategy
  {
    public void layout(FlowView fv)
    {
//      super.layout(fv);
      /**/
      int p0 = fv.getStartOffset();
      int p1 = fv.getEndOffset();

      // we want to preserve all views from the logicalView from being
      // removed
      View lv = getLogicalView(fv);
      int n = lv.getViewCount();
      for (int i = 0; i < n; i++)
      {
        View v = lv.getView(i);
        v.setParent(lv);
      }
      fv.removeAll();
      for (int rowIndex = 0; p0 < p1; rowIndex++)
      {
        View row = ((AdvancedParagraphView) fv).createPublicRow();
        fv.append(row);

        // layout the row to the current span.  If nothing fits,
        // force something.
        int next = layoutRow(fv, rowIndex, p0);
        if (row.getViewCount() == 0)
        {
          row.append(createView(fv, p0, Integer.MAX_VALUE, rowIndex));
          next = row.getEndOffset();
        }
        if (next <= p0)
        {
//            p0++;
//              throw new StateInvariantError("infinite loop in formatting");
          break;
        }
        else
        {
          p0 = next;
        }
      }
      /**/

      AttributeSet attr = fv.getElement().getAttributes();
      float lineSpacing = StyleConstants.getLineSpacing(attr);
      boolean justifiedAlignment = (StyleConstants.getAlignment(attr) ==
                                    StyleConstants.ALIGN_JUSTIFIED);
      if (!(justifiedAlignment || (lineSpacing > 1)))
      {
        return;
      }

      int cnt = fv.getViewCount();
      for (int i = 0; i < cnt - 1; i++)
      {
        AdvancedRow row = (AdvancedRow) fv.getView(i);
        if (lineSpacing > 1)
        {
          float height = row.getMinimumSpan(View.Y_AXIS);
          float addition = (height * lineSpacing) - height;
          if (addition > 0)
          {
            row.setInsets(row.getTopInset(), row.getLeftInset(),
                          (short) addition, row.getRightInset());
          }
        }

        if (justifiedAlignment)
        {
          restructureRow(row, i);
          row.setRowNumber(i + 1);
        }
      }
    }

    protected int layoutRow(FlowView fv, int rowIndex, int pos)
    {
      View row = fv.getView(rowIndex);
      int x = fv.getFlowStart(rowIndex);
      int spanLeft = fv.getFlowSpan(rowIndex);
      int end = fv.getEndOffset();
      TabExpander te = (fv instanceof TabExpander) ? (TabExpander) fv : null;

      // Indentation.
      int preX = x;
      int availableSpan = spanLeft;
//      preX = x;

      final int flowAxis = fv.getFlowAxis();
      boolean forcedBreak = false;
      while (pos < end && spanLeft > 0)
      {
        View v = createView(fv, pos, spanLeft, rowIndex);
        if (v == null)
        {
          break;
        }

        int chunkSpan;
        if ((flowAxis == X_AXIS) && (v instanceof TabableView))
        {
          chunkSpan = (int) ((TabableView) v).getTabbedSpan(x, te);
        }
        else
        {
          chunkSpan = (int) v.getPreferredSpan(flowAxis);
        }

        // If a forced break is necessary, break
        if (v.getBreakWeight(flowAxis, pos, spanLeft) >= ForcedBreakWeight)
        {
          int n = row.getViewCount();
          if (n > 0)
          {
            /* If this is a forced break and it's not the only view
             * the view should be replaced with a call to breakView.
             * If it's it only view, it should be used directly.  In
             * either case no more children should be added beyond this
             * view.
             */
            v = v.breakView(flowAxis, pos, x, spanLeft);
            if (v != null)
            {
              if ((flowAxis == X_AXIS) && (v instanceof TabableView))
              {
                chunkSpan = (int) ((TabableView) v).getTabbedSpan(x, te);
              }
              else
              {
                chunkSpan = (int) v.getPreferredSpan(flowAxis);
              }
            }
            else
            {
              chunkSpan = 0;
            }
          }
          forcedBreak = true;
        }

        spanLeft -= chunkSpan;
        x += chunkSpan;
        if (v != null)
        {
          row.append(v);
          pos = v.getEndOffset();
        }
        if (forcedBreak)
        {
          break;
        }

      }
      if (spanLeft < 0)
      {
        // This row is too long and needs to be adjusted.
        adjustRow(fv, rowIndex, availableSpan, preX);
      }
      else if (row.getViewCount() == 0)
      {
        // Impossible spec... put in whatever is left.
        View v = createView(fv, pos, Integer.MAX_VALUE, rowIndex);
        row.append(v);
      }
      return row.getEndOffset();
    }

    protected void adjustRow(FlowView fv, int rowIndex, int desiredSpan, int x)
    {
      final int flowAxis = fv.getFlowAxis();
      View r = fv.getView(rowIndex);
      int n = r.getViewCount();
      int span = 0;
      int bestWeight = BadBreakWeight;
      int bestSpan = 0;
      int bestIndex = -1;
      int bestOffset = 0;
      View v;
      for (int i = 0; i < n; i++)
      {
        v = r.getView(i);
        int spanLeft = desiredSpan - span;

        int w = v.getBreakWeight(flowAxis, x + span, spanLeft);
        if ((w >= bestWeight) && (w > BadBreakWeight))
        {
          bestWeight = w;
          bestIndex = i;
          bestSpan = span;
          if (w >= ForcedBreakWeight)
          {
            // it's a forced break, so there is
            // no point in searching further.
            break;
          }
        }
        span += v.getPreferredSpan(flowAxis);
      }
      if (bestIndex < 0)
      {
        // there is nothing that can be broken, leave
        // it in it's current state.
        return;
      }

      // Break the best candidate view, and patch up the row.
      int spanLeft = desiredSpan - bestSpan;
      v = r.getView(bestIndex);
      v = v.breakView(flowAxis, v.getStartOffset(), x + bestSpan, spanLeft);
      View[] va = new View[1];
      va[0] = v;
      View lv = getLogicalView(fv);
      for (int i = bestIndex; i < n; i++)
      {
        View tmpView = r.getView(i);
        if (contains(lv, tmpView))
        {
          tmpView.setParent(lv);
        }
        else if (tmpView.getViewCount() > 0)
        {
          recursiveReparent(tmpView, lv);
        }
      }
      r.replace(bestIndex, n - bestIndex, va);
    }

    private boolean contains(View logicalView, View v)
    {
      int n = logicalView.getViewCount();
      for (int i = 0; i < n; i++)
      {
        if (logicalView.getView(i) == v)
        {
          return true;
        }
      }
      return false;
    }

    private void recursiveReparent(View v, View logicalView)
    {
      int n = v.getViewCount();
      for (int i = 0; i < n; i++)
      {
        View tmpView = v.getView(i);
        if (contains(logicalView, tmpView))
        {
          tmpView.setParent(logicalView);
        }
        else
        {
          recursiveReparent(tmpView, logicalView);
        }
      }
    }

    protected void restructureRow(View row, int rowNum)
    {
      int rowStartOffset = row.getStartOffset();
      int rowEndOffset = row.getEndOffset();
      String rowContent = "";
      try
      {
        rowContent = row.getDocument().getText(rowStartOffset,
                                               rowEndOffset - rowStartOffset);
        if (rowNum == 0)
        {
          int index = 0;
          while (rowContent.charAt(0) == ' ')
          {
            rowContent = rowContent.substring(1);
            if (rowContent.length() == 0)
            {
              break;
            }
          }
        }
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
      int rowSpaceCount = getSpaceCount(rowContent);
      if (rowSpaceCount < 1)
      {
        return;
      }
      int[] rowSpaceIndexes = getSpaceIndexes(rowContent, row.getStartOffset());
      int currentSpaceIndex = 0;

      for (int i = 0; i < row.getViewCount(); i++)
      {
        View child = row.getView(i);
        if ((child.getStartOffset() < rowSpaceIndexes[currentSpaceIndex]) &&
            (child.getEndOffset() > rowSpaceIndexes[currentSpaceIndex]))
        {
//split view
          View first = child.createFragment(child.getStartOffset(),
                                            rowSpaceIndexes[currentSpaceIndex]);
          View second = child.createFragment(rowSpaceIndexes[currentSpaceIndex],
                                             child.getEndOffset());
          View[] repl = new View[2];
          repl[0] = first;
          repl[1] = second;

          row.replace(i, 1, repl);
          currentSpaceIndex++;
          if (currentSpaceIndex >= rowSpaceIndexes.length)
          {
            break;
          }
        }
      }
      int childCnt = row.getViewCount();
    }

  }

  class AdvancedRow extends BoxView
  {

    private int rowNumber = 0;

    AdvancedRow(Element elem)
    {
      super(elem, View.X_AXIS);
    }

    protected void loadChildren(ViewFactory f)
    {
    }

    public AttributeSet getAttributes()
    {
      View p = getParent();
      return (p != null) ? p.getAttributes() : null;
    }

    public float getAlignment(int axis)
    {
      if (axis == View.X_AXIS)
      {
        AttributeSet attr = getAttributes();
        int justification = StyleConstants.getAlignment(attr);
        switch (justification)
        {
          case StyleConstants.ALIGN_LEFT:
          case StyleConstants.ALIGN_JUSTIFIED:
            return 0;
          case StyleConstants.ALIGN_RIGHT:
            return 1;
          case StyleConstants.ALIGN_CENTER:
            return 0.5f;
        }
      }
      return super.getAlignment(axis);
    }

    public Shape modelToView(int pos, Shape a, Position.Bias b) throws
        BadLocationException
    {
      Rectangle r = a.getBounds();
      View v = getViewAtPosition(pos, r);
      if ((v != null) && (!v.getElement().isLeaf()))
      {
// Don't adjust the height if the view represents a branch.
        return super.modelToView(pos, a, b);
      }
      r = a.getBounds();
      int height = r.height;
      int y = r.y;
      Shape loc = super.modelToView(pos, a, b);
      r = loc.getBounds();
      r.height = height;
      r.y = y;
      return r;
    }

    public int getStartOffset()
    {
      int offs = Integer.MAX_VALUE;
      int n = getViewCount();
      for (int i = 0; i < n; i++)
      {
        View v = getView(i);
        offs = Math.min(offs, v.getStartOffset());
      }
      return offs;
    }

    public int getEndOffset()
    {
      int offs = 0;
      int n = getViewCount();
      for (int i = 0; i < n; i++)
      {
        View v = getView(i);
        offs = Math.max(offs, v.getEndOffset());
      }
      return offs;
    }

    protected void layoutMinorAxis(int targetSpan, int axis, int[] offsets,
                                   int[] spans)
    {
      baselineLayout(targetSpan, axis, offsets, spans);
    }

    protected SizeRequirements calculateMinorAxisRequirements(int axis,
        SizeRequirements r)
    {
      return baselineRequirements(axis, r);
    }

    protected int getViewIndexAtPosition(int pos)
    {
// This is expensive, but are views are not necessarily layed
// out in model order.
      if (pos < getStartOffset() || pos >= getEndOffset())
      {
        return -1;
      }
      for (int counter = getViewCount() - 1; counter >= 0; counter--)
      {
        View v = getView(counter);
        if (pos >= v.getStartOffset() &&
            pos < v.getEndOffset())
        {
          return counter;
        }
      }
      return -1;
    }

    public short getTopInset()
    {
      return super.getTopInset();
    }

    public short getLeftInset()
    {
      return super.getLeftInset();
    }

    public short getRightInset()
    {
      return super.getRightInset();
    }

    public void setInsets(short topInset, short leftInset, short bottomInset,
                          short rightInset)
    {
      super.setInsets(topInset, leftInset, bottomInset, rightInset);
    }

    protected void layoutMajorAxis(int targetSpan, int axis, int[] offsets,
                                   int[] spans)
    {
      super.layoutMajorAxis(targetSpan, axis, offsets, spans);
      AttributeSet attr = getAttributes();
      if ((StyleConstants.getAlignment(attr) != StyleConstants.ALIGN_JUSTIFIED) &&
          (axis != View.X_AXIS))
      {
        return;
      }
      int cnt = offsets.length;

      int span = 0;
      for (int i = 0; i < cnt; i++)
      {
        span += spans[i];
      }
      if (getRowNumber() == 0)
      {
        return;
      }
      int startOffset = getStartOffset();
      int len = getEndOffset() - startOffset;
      String context = "";
      try
      {
        context = getElement().getDocument().getText(startOffset, len);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
      int spaceCount = getSpaceCount(context) - 1;

      int pixelsToAdd = targetSpan - span;

      if (this.getRowNumber() == 1)
      {
        int firstLineIndent = (int) StyleConstants.getFirstLineIndent(
            getAttributes());
        pixelsToAdd -= firstLineIndent;
      }

      int[] spaces = getSpaces(pixelsToAdd, spaceCount);
      int j = 0;
      int shift = 0;
      for (int i = 1; i < cnt; i++)
      {
        LabelView v = (LabelView) getView(i);
        offsets[i] += shift;
        if ((isContainSpace(v)) && (i != cnt - 1))
        {
          offsets[i] += spaces[j];
          spans[i - 1] += spaces[j];
          shift += spaces[j];
          j++;
        }
      }
    }

    protected int[] getSpaces(int space, int cnt)
    {
      int[] result = new int[cnt];
      if (cnt == 0)
      {
        return result;
      }
      int base = space / cnt;
      int rst = space % cnt;

      for (int i = 0; i < cnt; i++)
      {
        result[i] = base;
        if (rst > 0)
        {
          result[i]++;
          rst--;
        }
      }

      return result;
    }

    public float getMinimumSpan(int axis)
    {
      if (axis == View.X_AXIS)
      {
        AttributeSet attr = getAttributes();
        if (StyleConstants.getAlignment(attr) != StyleConstants.ALIGN_JUSTIFIED)
        {
          return super.getMinimumSpan(axis);
        }
        else
        {
          return this.getParent().getMinimumSpan(axis);
        }
      }
      else
      {
        return super.getMinimumSpan(axis);
      }
    }

    public float getMaximumSpan(int axis)
    {
      if (axis == View.X_AXIS)
      {
        AttributeSet attr = getAttributes();
        if (StyleConstants.getAlignment(attr) != StyleConstants.ALIGN_JUSTIFIED)
        {
          return super.getMaximumSpan(axis);
        }
        else
        {
          return this.getParent().getMaximumSpan(axis);
        }
      }
      else
      {
        return super.getMaximumSpan(axis);
      }
    }

    public float getPreferredSpan(int axis)
    {
      if (axis == View.X_AXIS)
      {
        AttributeSet attr = getAttributes();
        if (StyleConstants.getAlignment(attr) != StyleConstants.ALIGN_JUSTIFIED)
        {
          return super.getPreferredSpan(axis);
        }
        else
        {
          return this.getParent().getPreferredSpan(axis);
        }
      }
      else
      {
        return super.getPreferredSpan(axis);
      }
    }

    public void setRowNumber(int value)
    {
      rowNumber = value;
    }

    public int getRowNumber()
    {
      return rowNumber;
    }
  }

  public int getFlowSpan(int index)
  {
    int span = super.getFlowSpan(index);
    if (index == 0)
    {
      int firstLineIdent = (int) StyleConstants.getFirstLineIndent(this.
          getAttributes());
      span -= firstLineIdent;
    }
    return span;
  }

  protected void layoutMinorAxis(int targetSpan, int axis, int[] offsets,
                                 int[] spans)
  {
    super.layoutMinorAxis(targetSpan, axis, offsets, spans);
    int firstLineIdent = (int) StyleConstants.getFirstLineIndent(this.
        getAttributes());
    offsets[0] += firstLineIdent;
  }

  protected static boolean isContainSpace(View v)
  {
    int startOffset = v.getStartOffset();
    int len = v.getEndOffset() - startOffset;
    try
    {
      String text = v.getDocument().getText(startOffset, len);
      if (text.indexOf(' ') >= 0)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    catch (Exception ex)
    {
      return false;
    }

  }
}
