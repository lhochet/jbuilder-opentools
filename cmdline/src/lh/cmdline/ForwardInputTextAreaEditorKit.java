package lh.cmdline;

import javax.swing.text.StyledEditorKit;
import javax.swing.text.ViewFactory;
import javax.swing.text.*;
import java.util.logging.*;
import java.awt.*;
import javax.swing.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:52 $
 * @created 10/01/04
 */

public class ForwardInputTextAreaEditorKit extends StyledEditorKit
{
  static private Logger log = Logger.getLogger(ForwardInputTextAreaEditorKit.class.getName());

  private static final StyledViewFactory defaultFactory = new StyledViewFactory();

  public ForwardInputTextAreaEditorKit()
  {
    super();
  }

  public void setWrap(boolean wrap)
  {
    defaultFactory.setWrap(wrap);
  }

  /**
   * Fetches a factory that is suitable for producing views of any models that
   * are produced by this kit.
   *
   * @return the factory
   * @todo Implement this javax.swing.text.EditorKit method
   */
  public ViewFactory getViewFactory()
  {
//    log.finest("in getViewFactory");
//    System.out.println("in getViewFactory");
    return defaultFactory;
  }

  // ---- default ViewFactory implementation ---------------------

  static class StyledViewFactory implements ViewFactory
  {
    private boolean wrap = false;
    public void setWrap(boolean wrap_)
    {
      wrap = wrap_;
    }

    public View create(Element elem)
    {
      String kind = elem.getName();
//      log.finest("in StyledViewFactory.create(" + kind + ")");
//      System.out.println("in StyledViewFactory.create(" + kind + ")");
      if (kind != null)
      {
        if (kind.equals(AbstractDocument.ContentElementName))
        {
          return new LabelView(elem); // org: LabelView(elem);
        }
        else if (kind.equals(AbstractDocument.ParagraphElementName))
        {
          if (wrap)
          {
            if (true) return new AdvancedParagraphView(elem);

            if (elem.getEndOffset() - elem.getStartOffset() > 10000)
            {
//              return new WrappedPlainView(elem, true);
//              return new LabelView(elem); // org: LabelView(elem);
            }
            else
            {
//            return new BoxView(elem, View.Y_AXIS);
//            return new WrappedPlainView(elem, false);
//            return new WrappedLineParagraphView(elem); // org: ParagraphView(elem);
              return new ParagraphView(elem); // org: ParagraphView(elem);
            }
          }
          else
          {
            return new MonoLineParagraphView(elem); //, View.X_AXIS);
//        return new BoxView(elem, View.X_AXIS);
          }

        }
        else if (kind.equals(AbstractDocument.SectionElementName))
        {
//          return new TopBoxView(elem); //, View.X_AXIS);
          return new BoxView(elem, View.Y_AXIS);
        }
//        else if (kind.equals(StyleConstants.ComponentElementName))
//        {
//          return new ComponentView(elem);
//        }
//        else if (kind.equals(StyleConstants.IconElementName))
//        {
//          return new IconView(elem);
//        }
      }

      // default to text display
      return new LabelView(elem);
    }

  }

  static class MonoLineParagraphView extends BoxView
  {
    MonoLineParagraphView(Element elem)
    {
      super(elem, BoxView.X_AXIS);
    }

    public float getAlignment(int axis)
    {
      switch (axis)
     {
       case Y_AXIS:
         return 1.0f;
       case X_AXIS:
         return 0.0f;
       default:
         throw new IllegalArgumentException("Invalid axis: " + axis);
     }
//     if (true) return 0.0f;
//      switch (axis)
//      {
//        case Y_AXIS:
//          float a = 0.5f;
//          if (getViewCount() != 0)
//          {
//            int paragraphSpan = (int) getPreferredSpan(View.Y_AXIS);
//            View v = getView(0);
//            int rowSpan = (int) v.getPreferredSpan(View.Y_AXIS);
//            a = (paragraphSpan != 0) ? ((float) (rowSpan / 2)) / paragraphSpan : 0;
//          }
//          return a;
//        case X_AXIS:
//          return 0.0f;
//        default:
//          throw new IllegalArgumentException("Invalid axis: " + axis);
//      }
    }

    protected void layoutMinorAxis(int targetSpan, int axis, int[] offsets, int[] spans)
    {
      super.layoutMinorAxis(targetSpan, axis, offsets, spans);
    }

    protected SizeRequirements calculateMinorAxisRequirements(int axis, SizeRequirements r)
    {
//      return super.calculateMinorAxisRequirements(axis, r);
      // calculate tiled request
      float min = 0;
      float pref = 0;
      float max = 0;

      int n = getViewCount();
      for (int i = 0; i < n; i++) {
          View v = getView(i);
          min += v.getMinimumSpan(axis);
          pref += v.getPreferredSpan(axis);
          max += v.getMaximumSpan(axis);
      }

      if (r == null) {
          r = new SizeRequirements();
      }
      r.alignment = 0.5f;
      r.minimum = (int) min;
      r.preferred = (int) pref;
      r.maximum = (int) max;
      return r;
    }
  }

  static class WrappedLineParagraphView extends ParagraphView
  {
    WrappedLineParagraphView(Element elem)
    {
      super(elem); //, BoxView.Y_AXIS);
    }
//    public float getAlignment(int axis)
//    {
//      switch (axis)
//      {
//        case Y_AXIS:
//          return 1.0f;
//        case X_AXIS:
//          return 0.0f;
//        default:
//          throw new IllegalArgumentException("Invalid axis: " + axis);
//      }
//    }
/*
    public float getAlignment(int axis) {
        switch (axis) {
        case Y_AXIS:
            float a = 0.5f;
            if (getViewCount() != 0) {
                int paragraphSpan = (int) getPreferredSpan(View.Y_AXIS);
                View v = getView(0);
                int rowSpan = (int) v.getPreferredSpan(View.Y_AXIS);
                a = (paragraphSpan != 0) ? ((float)(rowSpan / 2)) / paragraphSpan : 0;
            }
            return a;
        case X_AXIS:
            return 0.0f;
        default:
            throw new IllegalArgumentException("Invalid axis: " + axis);
        }
    }
*/
  }


}
