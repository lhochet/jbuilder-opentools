
package lh.cmdline;

import javax.swing.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.actions.*;
import java.io.*;
import com.borland.jbuilder.ide.*;
import java.util.*;
import com.borland.primetime.ide.macros.*;

/**
 * Title:        Command line open tool<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * Company:      <p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-11-06 14:15:52 $
 * 0.4, 15/10/01
 * 0.3, 30/06/01
 */
public class Runner implements Runnable //extends Thread
{

  CmdMessageCategory msgcat = null;
  CommandPanel cpanel = null;
  ForwardInputTextArea ta = null;
  String cmd = null;
  boolean wait = false;
  boolean showout = false;
  boolean showerr = false;
  boolean handlein = false;
  boolean echoInput = true;
  File workingdir = null;
  String[] osenv = null;
  boolean reuseTab = true;

  Process process = null;

  ActionButton btnRun = null;
  ActionButton btnStop = null;
  Browser browser = null;
  int retCode;
  OutputStreamWriter writer = null;

  /**
   * Create a new Command line runner
   * @param browser JBuilder browser in which this is executing
   * @param cmd Command to be executed
   * @param wait If true wait for the command to return before itself returning
   * @param showout If true shows the standard output of the command
   * @param showerr If true shows the standard errors output of the command
   * @param handlein If true attempt to forward inputs to the command
   * @deprecated
   */
  public Runner(Browser browser, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein)
  {
    this(browser, "Command", cmd, wait, showout, showerr, handlein, true);
  }
  /**
   * Create a new Command line runner
   * @param browser JBuilder browser in which this is executing
   * @param cat MessageView category [ie what is going to be the text on the tab]
   * @param cmd Command to be executed
   * @param wait If true wait for the command to return before itself returning
   * @param showout If true shows the standard output of the command
   * @param showerr If true shows the standard errors output of the command
   * @param handlein If true attempt to forward inputs to the command
   * @deprecated
   */
  public Runner(Browser browser, String cat, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein)
  {
    this(browser, cat, cmd, wait, showout, showerr, handlein, true);
  }

  public Runner(Browser browser, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput)
  {
    this(browser, "Command", cmd, wait, showout, showerr, handlein, echoInput);
  }
  public Runner(Browser browser, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput, boolean reuseTab)
  {
    this(browser, "Command", cmd, wait, showout, showerr, handlein, echoInput, reuseTab);
  }
  public Runner(Browser browser, String cat, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput)
  {
    this(browser, cat, cmd, wait, showout, showerr, handlein, echoInput, null, true);
  }
  public Runner(Browser browser, String cat, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput, boolean reuseTab)
  {
    this(browser, cat, cmd, wait, showout, showerr, handlein, echoInput, null, reuseTab);
  }
  /**
   * Create a new Runner
   * @param browser JBuilder browser in which this is executing
   * @param cat MessageView category [ie what is going to be the text on the tab]
   * @param cmd Command to be executed
   * @param wait If true wait for the command to return before itself returning
   * @param showout If true shows the standard output of the command
   * @param showerr If true shows the standard errors output of the command
   * @param handlein If true attempt to forward inputs to the command
   * @param echoInput If true echo what ever has been typed
   * @param workingdir Directory in which the command should be executed
   * @deprecated
   */
  public Runner(Browser browser, String cat, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput, File workingdir)
  {
    this(browser, cat, cmd, wait, showout, showerr, handlein, echoInput, workingdir, null, true);
  }
  public Runner(Browser browser, String cat, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput, File workingdir, boolean reusetab)
  {
    this(browser, cat, cmd, wait, showout, showerr, handlein, echoInput, workingdir, null, reusetab);
  }
  public Runner(Browser browser, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput, File workingdir, boolean reusetab)
  {
    this(browser, "Command", cmd, wait, showout, showerr, handlein, echoInput, workingdir, null, reusetab);
  }
  /**
   * Create a new Runner
   * @param browser JBuilder browser in which this is executing
   * @param cat MessageView category [ie what is going to be the text on the tab]
   * @param cmd Command to be executed
   * @param wait If true wait for the command to return before itself returning
   * @param showout If true shows the standard output of the command
   * @param showerr If true shows the standard errors output of the command
   * @param handlein If true attempt to forward inputs to the command
   * @param echoInput If true echo what ever has been typed
   * @param workingdir Directory in which the command should be executed
   * @param osenv Operating System environment
   * @param reuseTab If true reuse the tab if one exist for the gived command
   */
  public Runner(Browser browser, String cat, String cmd, boolean wait, boolean showout, boolean showerr,
                boolean handlein, boolean echoInput, File workingdir, String[] osenv, boolean reuseTab)
  {
    this.browser = browser;
    this.cmd = cmd;
    this.wait = wait;
    this.showout = showout;
    this.showerr = showerr;
    this.handlein = handlein;
    this.echoInput = echoInput;
    this.workingdir = workingdir;
    this.osenv = osenv;
    this.reuseTab = reuseTab;

    boolean doReuseExistingTab = false;

    if (reuseTab)
    {
      msgcat = CmdMessageCategory.getCat(cmd);
      if (msgcat != null)
      {
        Runner r = msgcat.getRunner();
        if ((r == null) || (r.isFinished()))
        {
            doReuseExistingTab = true;
        }
      }
    }

    if (doReuseExistingTab)
    {
      msgcat.setRunner(this);
      cpanel = msgcat.getCPanel();
      ta = cpanel.getTextArea();
      ta.setForward(handlein);
      ta.setEchoInput(echoInput);
      btnStop = cpanel.getStopButton();
      btnRun = cpanel.getRunButton();
      browser.getMessageView().showTab(msgcat);
    }
    else
    {
      cpanel = new CommandPanel(this);
      msgcat = new CmdMessageCategory(cat, cmd, cpanel, this);
      CmdMessageCategory.putCat(cmd , msgcat);
      ta = cpanel.getTextArea();
      if  (handlein) ta.setForward(true);
      if  (echoInput) ta.setEchoInput(true);
      btnStop = cpanel.getStopButton();
      btnRun = cpanel.getRunButton();
      browser.getMessageView().addCustomTab(msgcat, cpanel);
    }
//      else
//      {
//        msgcat = new MessageCategory(cat, cmd); //, BrowserIcons.ICON_RUN);
//        cats.put(cmd, msgcat);
//      }
//    }
//    else
//    {
//      msgcat = new MessageCategory(cat, cmd); //, BrowserIcons.ICON_RUN);
//      cats.put(cmd, msgcat);
//    }
//      browser.getMessageView().showTab(msgcat);
  }

//  public Runner(Runner runner)
//  {
//    this.msgcat = runner.msgcat;
//    this.cpanel = runner.cpanel;
//    this.ta = runner.ta;
//    this.cmd = runner.cmd;
//    this.wait = runner.wait;
//    this.showout = runner.showout;
//    this.showerr = runner.showerr;
//    this.handlein = runner.handlein;
//    this.workingdir = runner.workingdir;
//    this.btnRun = runner.btnRun;
//    this.btnStop = runner.btnStop;
//    this.browser = runner.browser;
//  }

  public void run()
  {
    msgcat.setIcon(BrowserIcons.ICON_RUN);
    OutputListener outlist = null;
    ErrorListener errlist = null;

    try
    {
      MessageView mv = browser.getMessageView();
      mv.showTab(msgcat);
      mv.updateUI();
//      System.err.println("cmd = @" + cmd + "@");
      btnRun.setEnabled(false);
      btnStop.setEnabled(true);
//      ta.setEditable(true);
      ta.setEditable(false);

      if (osenv == null)
      {
        com.borland.primetime.util.OSEnvironment os = new com.borland.primetime.util.OSEnvironment();
        osenv = os.getVariables();
      }

      String lcmd = expandCmdLine(cmd);
      ta.setText("Running: " + lcmd + "\n");
      if (workingdir == null)
      {
        process = Runtime.getRuntime().exec(lcmd, osenv); // IOEx
      }
      else
      {
//        String[] envp = new String[0];
        if (!workingdir.exists())
        {
          throw new IOException("Working directory '" + workingdir +"' does not exist.");
        }
//        if (osenv == null)
//        {
//          com.borland.primetime.util.OSEnvironment os = new com.borland.primetime.util.OSEnvironment();
//          osenv = os.getVariables();
//        }
        process = Runtime.getRuntime().exec(lcmd, osenv, workingdir); // IOEx
      }
//      System.err.println("cmd started");

      /**/BufferedReader outreader = null;
      BufferedReader errreader = null;
      if (showout)
      {
        outreader = /**/new BufferedReader(/**/new InputStreamReader(process.getInputStream())/**/, 1024)/**/;
      }
      else
      {
        outreader = new BufferedReader(new StringReader(""));
      }

      if (showerr)
      {
        errreader = new BufferedReader(new InputStreamReader(process.getErrorStream()), 1024);
      }
      else
      {
        errreader = new BufferedReader(new StringReader(""));
      }

      if (handlein)
      {
        ta.setEditable(true);
        writer = new OutputStreamWriter(process.getOutputStream(),"UTF-8");
      }

      if (showout || showerr)
      {
        outlist = new OutputListener(outreader, errreader, ta);
        outlist.start();
        errlist = new ErrorListener(errreader, ta);
        errlist.start();
      }
      //process.waitFor(); // InterruptedEx
//      while (outlist.available()) Thread.currentThread().yield();
//      if (errlist != null) errlist.exit();
      if (wait)
      {

        boolean finished = false;
        while (!finished)
        {
          try
          {
            retCode = process.exitValue();
            process = null;
            finished = true;
            ta.appendOut("\nReturn value = " + retCode + "\n");
          }
          catch (IllegalThreadStateException ex)
          {
            // expected exception
            Thread.currentThread().yield();
            try
            {
              Thread.sleep(1000);
            }
            catch (InterruptedException ex2)
            {
            }

          }//catch
        }//while

      } // if

      if (outlist != null) outlist.exit();
      if (errlist != null) errlist.exit();

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      ta.appendErr("\nException occured: " + ex.getLocalizedMessage());
    }
    finally
    {
      ta.setEditable(false);
      btnRun.setEnabled(true);
      btnStop.setEnabled(false);
      ta.setCaretPosition(ta.getText().length());

      if (outlist != null) outlist.exit();
      if (errlist != null) errlist.exit();

      process = null;
      msgcat.setIcon(null); //BrowserIcons.ICON_BLANK); //BrowserIcons.ICON_NULL);
    }
  }

//        static int i = 0;
//  public void sendKey(int key)
  public void sendKey(char key)
  {
    try
    {
      if ((process != null) && (writer != null))
      {
//        OutputStreamWriter writer = new OutputStreamWriter(process.getOutputStream());
//System.out.println("key = " + key);
//System.out.println("encoding = " + writer.getEncoding());
//				if (key == '\n')
//				{
          writer.write(key);
//				}
//				else
//				{
//					writer.write((key >>> 0) & 0xFF);
//					writer.write((key >>> 8) & 0xFF);
//				}

        writer.flush();

//        i++;
//        if (i > 10)
//        {
//          writer.write(3);
//          writer.flush();
//        }

//        DataOutputStream dos = new DataOutputStream(process.getOutputStream());
//        dos.writeChar(key);
//        dos.flush();

//        process.getOutputStream().write(key);
//        process.getOutputStream().flush();
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }


  public void sendControlKey(byte key)
  {
    try
    {
      if ((process != null) && (writer != null))
      {
        process.getOutputStream().write(key);
        process.getOutputStream().flush();
//        DataOutputStream dos = new DataOutputStream(process.getOutputStream());
//        dos.writeByte(key);
//        dos.flush();
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void exit()
  {
    if (process != null) process.destroy();
    ta.setEditable(false);
    btnRun.setEnabled(true);
    btnStop.setEnabled(false);

  }

  public int getRetCode()
  {
    return retCode;
  }

  public void removeTab()
  {
    try
    {
      if (CommandLine.isJB4OrLess)
      {
        browser.getMessageView().removeTab(msgcat);
      }
      else
      {
        browser.getMessageView().removeTab(msgcat, false);
      }
    }
    catch (Exception e)
    {
      if (com.borland.primetime.PrimeTime.isVerbose()) e.printStackTrace();
    }

  }

  private String expandCmdLine(String cmd)
  {
    Browser b = Browser.getActiveBrowser();
    MacroExpander me = new MacroExpander(cmd, b.getActiveProject(), b, MacroTable.GUI_MACRO);
    String[] elts = me.toArray();
    String ret = "";
    int nb = 0;
    for (int i = 0; i < elts.length; ++i)
    {
      if (elts[i] != null)
      {
        if (elts[i].equals("<params>"))
        {
          elts[i] = JOptionPane.showInputDialog(b, "Parameter ($Prompt) " + ++nb);
        }
        ret += elts[i];
        if (i < elts.length)
        {
          ret += " ";
        }
      }
    }

    return ret;
  }

  public boolean isFinished()
  {
    if (process != null)
    {
      try
      {
        retCode = process.exitValue();
        process = null;
      }
      catch (IllegalThreadStateException ex)
      {
        // this should mean that the process is still running
        return false;
      } //catch

    }
    return true;
  }
}
