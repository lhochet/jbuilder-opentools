/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbuilder.opentools.community.proxy;

import com.borland.primetime.properties.*;
import com.borland.primetime.*;
import java.util.logging.*;

/**
 * PropertyGroup to hold the proxy settings
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ \$Date: 2005-03-27 13:40:38 $
 * @created 2/03/05
 */
public class ProxyPropertyGroup implements PropertyGroup
{
  private static Logger log = Logger.getLogger(ProxyPropertyGroup.class.getName());

  /** Category for the Proxy properties **/
  private static final String PROXY_CAT = "net.java.dev.jbuilder.opentools.community.proxy";

  /** true if the proxy setting overriding is enabled */
  public static final GlobalBooleanProperty ENABLED = new GlobalBooleanProperty(PROXY_CAT, "enabled", false);
  /** proxy host*/
  public static final GlobalProperty HOST = new GlobalProperty(PROXY_CAT, "host");
  /** proxy port (defaults to: 80)*/
  public static final GlobalIntegerProperty PORT = new GlobalIntegerProperty(PROXY_CAT, "port", 80);
  /** proxy user (if empty then the authentification is not used)*/
  public static final GlobalProperty USER = new GlobalProperty(PROXY_CAT, "user");
  /** proxy password */
  public static final GlobalProperty PWD = new GlobalProperty(PROXY_CAT, "password");

  /**
   * Registers this property group with the property manager
   * @param majorVersion major version number of the PrimeTime version used
   * @param minorVersion minor version number of the PrimeTime version used
   */
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("LH Proxy PropertyGroup vrs 0.1");
      System.out.println("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." +
                         PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    PropertyManager.registerPropertyGroup(new ProxyPropertyGroup());
  }

  /**
   * Return the Proxy property page factory if the topic is community
   *
   * @param topic property topic to show
   * @return PropertyPageFactory to show a proxy property page
   */
  public PropertyPageFactory getPageFactory(Object topic)
  {
    // null: shows in the jbuilder preferences page
    if (topic == null) //topic == net.java.dev.jbuilder.opentools.community.CommunityPropertyGroup.COMMUNITY_TOPIC)
    {
      return new PropertyPageFactory("Proxy")
      {
        public PropertyPage createPropertyPage()
        {
          return new ProxyPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

  /**
   * Set the system proxy settings based on the properties
   */
  public void initializeProperties()
  {
    updateSystemProxyInfo();
  }

  /**
   * Does set the system setting based on the properties. Set the host and port
   * if the proxy setting are enabled, and set the proxy authentification
   * username and password if the user is set in the properties
   * (also called when the property page is commited)
   */
  static final void updateSystemProxyInfo()
  {
    if (ENABLED.getBoolean())
    {
      log.info("Community Proxy enabled.");
      String proxyHost = HOST.getValue();
      String proxyPort = PORT.getValue();
      String proxyUser = USER.getValue();
      String proxyPassword = PWD.getValue();
      log.info("proxy: " + proxyHost + ":" + proxyPort);

      System.setProperty("http.proxyHost", proxyHost);
      System.setProperty("http.proxyPort", proxyPort);

      if ((proxyUser != null) && (!proxyUser.equals("")))
      {
        log.info("proxy login: " + proxyUser + ":" + proxyPassword);
        System.setProperty("http.proxyUser", proxyUser);
        System.setProperty("http.proxyPassword", proxyPassword);
      }
    }
    else
    {
      log.info("Community Proxy NOT enabled.");
    }
  }


}
