/*
   Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbuilder.opentools.community;

import java.io.*;
import java.util.*;

/**
 * Utilitary class to extract a version out of a build.num
 * located in the same package as a given class.
 * <br/>(Based on Joi Ellis code, see:
 * <a href="http://www.visi.com/~gyles19/jbuilder/BuildNumberOT.html">
 * http://www.visi.com/~gyles19/jbuilder/BuildNumberOT.html</a>)
 *
 * @author Ludovic HOCHET
 * @version 0.1, 19/08/03
 */

public class VersionUtil
{
  /**
   * Extract a version out of a build.num located in the same package as a given class.
   * @param clazz Class indicating the package in which the build.num is located
   * @return The version extracted from build.num as a String, or <b>null</b> if any error occured
   */
  public static String getVersionString(Class clazz)
  {
    try
    {
      Properties props = new Properties();
      InputStream in = null;
      in = clazz.getResourceAsStream("build.num");
      props.load(in);
      in.close();
      return props.getProperty("version");
    }
    catch (Exception ex)
    {
      return null;
    }
  }

  /**
   * Extract a version out of the package of a given class.
   * @param clazz Class indicating the package from which to get the version
   * @return The version extracted as a String, or <b>null</b> if not known
   */
  public static String getVersionFromPackage(Class clazz)
  {
    Package pkg = clazz.getPackage();
    return pkg.getImplementationVersion();
  }


}
