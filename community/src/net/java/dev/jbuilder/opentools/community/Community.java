/*
   Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbuilder.opentools.community;

import java.util.*;

import javax.swing.*;

import com.borland.jbuilder.*;
import com.borland.primetime.*;
import com.borland.primetime.actions.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.personality.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.util.*;

/**
 * The Community OpenTool<br/>
 * Adds the Community menus and toolbar, hosts the static community agent handling methods.</br>
 * This OpenTool is registered in the LH_Community category.
 * @author Ludovic HOCHET
 * @version $Revision: 1.4 $ \$Date: 2005-03-27 13:41:22 $
 * @created 15/04/02
 */

public class Community
{

  /**
   * Tools|Community Options... menu entry. Brings up the Community properties dialog
   */
  public static class CommunityOptionsAction extends BrowserAction
  {
    /** Constructor **/
    public CommunityOptionsAction()
    {
      super("Community Options...", 'C', "Community Options...");
    }

    /**
     * Brings up the Community properties dialog. Property factories should add property pages when receiving the CommunityPropertyGroup.COMMUNITY_TOPIC
     * @param browser Browser used as the parent for the property dialog
     */
    public void actionPerformed(Browser browser)
    {
      PropertyManager.showPropertyDialog(browser, "Community properties...", CommunityPropertyGroup.COMMUNITY_TOPIC, null);
    }

    /**
     * Overriden so that the action is only when the Community personality is enabled
     * @return Personality[] array containing only the Community personality
     */
    public Personality[] getPersonalities()
    {
      return CommunityPersonality.COMMUNITY_PERSONALITIES;
    }

  }

  /**
   * Subclass of ActionGroup to be shown only when the Community personality is enabled
   */
  public static class CommunityActionGroup extends ActionGroup
  {
    CommunityActionGroup(String name, char mnemonic, String desc)
    {
      super(name, mnemonic, desc);
    }
    CommunityActionGroup(String name, char mnemonic, String desc, Icon icon)
    {
      super(name, mnemonic, desc, icon);
    }

    /**
     * Overriden so that the action is only when the Community personality is enabled
     * @return Personality[] array containing only the Community personality
     */
    public Personality[] getPersonalities()
    {
      return CommunityPersonality.COMMUNITY_PERSONALITIES;
    }

  }

  /**
   * 'Body' of the autoexec thread
   */
  private static class AutoExec implements Runnable
  {
    /**
     * Calls run on each agent in turn if the agent isAutoexec() returns true
     */
    public void run()
    {
      Iterator iter = agents.iterator();
      while (iter.hasNext())
      {
        Agent agent = (Agent)iter.next();
        if (agent.isAutoexec()) agent.run();
      }
    }
  }

  /** ActionGroup used by OpenTools to add themselves to the Tools|Community submenu **/
  public static final ActionGroup GROUP_Community = new CommunityActionGroup("Community", 'C', "Borland Community actions group...", BrowserIcons.ICON_HELPBORLANDONLINE);
  /** ActionGroup used by OpenTools to add themselves to the Community toolbar **/
  public static final ActionGroup GROUP_CommunityToolbar = new CommunityActionGroup("Community", 'C', "Borland Community actions group...");

  /** Instance of the CommunityOptionsAction **/
  public static final CommunityOptionsAction OPTIONS_ACTION = new CommunityOptionsAction();

  /** Convenience runtime version of the community OpenTool **/
  public static final Version communityOTVersion = new Version(0, 3);
  /** Convenience post initOpenTool instance of the version of PrimeTime used **/
  public static Version jbOTAPI;

  /** Private list of community agents, used for the autocheck **/
  private static ArrayList agents = new ArrayList();

  /**
   * Initialise the Community OpenTool.<br/>
   * Adds the Community menu and toolbar, as well as the entry for the Community options property dialog
   * @param majorVersion major version number of the PrimeTime version used
   * @param minorVersion minor version number of the PrimeTime version used
   */
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    jbOTAPI = new Version(majorVersion, minorVersion);
    if (PrimeTime.isVerbose())
    {
      System.out.println("LH Community vrs " + VersionUtil.getVersionString(Community.class));
      System.out.println("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    // Add properties menu entry

    JBuilderMenu.GROUP_ToolsOptions.add(OPTIONS_ACTION);

    // Add the community group to the main Tools menu
    GROUP_Community.setPopup(true);
    JBuilderMenu.GROUP_ToolsStandard.add(/*2,*/ GROUP_Community);

    // Add community group to the main toolbar
    Browser.addToolBarGroup(GROUP_CommunityToolbar);
    // Force the community toolbar to be visible (even if empty)
    Browser.getActiveBrowser().getToolBarPane().setToolBarVisible(GROUP_CommunityToolbar, true);
  }

  /**
   * Return the version of the Community OpenTool
   * @return Version of the Community OpenTool
   */
  public static Version getOpenToolVersion()
  {
    return new Version(VersionUtil.getVersionFromPackage(Community.class));
  }

  /**
   * Register a Community Agent, this must be done during the initialisation of
   * the LH_Community OpenTools category
   * @param agent Community Agent to add
   */
  public static void registerAgent(Agent agent)
  {
    agents.add(agent);
  }

  /**
   * Run the startup autoexecutions methods in a separate thread.<br/>
   * This method can be disabled by providing a system property lh.community.noautoexec
   * and by disabling the Community Personality for a non default project.
   * @see Community.AutoExec
   */
  static void autoexec()
  {
    if (System.getProperty("net.java.dev.jbuilder.opentools.community.noautoexec") != null) return;

    // Can be used by an end user to disable the autoexec while working on a particular project
    if (!PersonalityManager.inCurrentPersonality(CommunityPersonality.COMMUNITY_PERSONALITIES, PersonalityManager.getCurrentPersonalityContext())) return;

    Thread thread = new Thread(new AutoExec());
    thread.start();
  }

}
