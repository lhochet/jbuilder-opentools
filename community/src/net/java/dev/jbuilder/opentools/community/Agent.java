/*
   Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbuilder.opentools.community;

/**
 * Extended by OpenTools requiring to execute some code when JB starts up. It is
 * executed once the LH_Community category has been loaded.
 * @author Ludovic HOCHET
 * @version 0.1, 22/04/02
 */

public interface Agent extends Runnable
{
  /**
   * Determines if the run method should be executed
   * @return <b>true</b> if the run method should be executed, <b>false</b> otherwise
   */
  public boolean isAutoexec();
}
