/*
   Copyright: Copyright (c) 2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbuilder.opentools.community;

import com.borland.primetime.*;
import com.borland.primetime.personality.*;

/**
 * Personality used as a root personality for Community related OpenTools<br/>
 * The CommunityPersonality does not depends on any other personality (PrimeTime in particular).<br/>
 * This OpenTool is registered in the Core category.<br/><br/>
 * <b>Note</b>: Disabling the CommunityPersonality in the default project will prevent the loading of the OpenTools
 * in the LH_Community category.<br/>
 * <b>Note 2</b>: Disabling the CommunityPersonality in the current project will prevent the autoexec method of the Community OpenTool to run
 *
 * @author Ludovic HOCHET
 * @version 0.1, 14/12/04
 * @since Community 0.3
 */

public class CommunityPersonality extends DefaultPersonality
{
  /** Static instance of the Community Personality **/
  public static final Personality COMMUNITY_PERSONALITY = new CommunityPersonality();
  /** Convenience wrapper of the static instance for the methods returning a Personality[] **/
  public static final Personality[] COMMUNITY_PERSONALITIES = new Personality[] { COMMUNITY_PERSONALITY };

  /**
   * Register the static instance of the Community personality with the personalty manager
   * @param majorVersion major version number of the PrimeTime version used
   * @param minorVersion minor version number of the PrimeTime version used
   */
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("LH CommunityPersonality vrs 0.1");
      System.out.println("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." +
                         PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    PersonalityManager.registerPersonality(COMMUNITY_PERSONALITY);
  }

  /**
   * Overrides the DefaultPersonality constructor to set the name of this personality
   */
  public CommunityPersonality()
  {
    super("Community");
  }

}
