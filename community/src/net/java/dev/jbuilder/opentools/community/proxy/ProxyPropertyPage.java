/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbuilder.opentools.community.proxy;

import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;
import javax.swing.JCheckBox;
import java.awt.Rectangle;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.*;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Proxy property page
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ \$Date: 2005-03-27 13:40:48 $
 * @created 2/03/05
 */
public class ProxyPropertyPage extends PropertyPage
{
  public ProxyPropertyPage()
  {
    try
    {
      jbInit();
      cbxEnabled_actionPerformed(null);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * Return the help topic for the proxy property page
   *
   * @return HelpTopic instance for the proxy property page
   */
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("doc/proxypropertypage.html").toString());
  }

  /**
   * Initialise the page from the proxy properties
   */
  public void readProperties()
  {
    cbxEnabled.setSelected(ProxyPropertyGroup.ENABLED.getBoolean());
    tfHost.setText(ProxyPropertyGroup.HOST.getValue());
    tfPort.setText(ProxyPropertyGroup.PORT.getValue());
    tfUser.setText(ProxyPropertyGroup.USER.getValue());
    tfPassord.setText(ProxyPropertyGroup.PWD.getValue());
  }

  /**
   * update the proxy properties from the page, update the system proxy settings as well
   */
  public void writeProperties()
  {
    ProxyPropertyGroup.ENABLED.setBoolean(cbxEnabled.isSelected());
    ProxyPropertyGroup.HOST.setValue(tfHost.getText());
    ProxyPropertyGroup.PORT.setValue(tfPort.getText());
    ProxyPropertyGroup.USER.setValue(tfUser.getText());
    ProxyPropertyGroup.PWD.setValue(tfPassord.getText());
    ProxyPropertyGroup.updateSystemProxyInfo();
  }

  /**
   * jbInit(), set up the page components
   * @throws Exception
   */
  private void jbInit() throws Exception
  {
    cbxEnabled.setText("Connect via a proxy");
    cbxEnabled.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cbxEnabled_actionPerformed(e);
      }
    });
    this.setLayout(gridBagLayout1);
    jLabel1.setText("Host:");
    tfHost.setText("host");
    tfHost.setColumns(40);
    jLabel2.setText("Port:");
    tfPort.setText("80");
    tfPort.setColumns(4);
    jLabel3.setText("User:");
    tfUser.setColumns(30);
    jLabel4.setText("Password:");
    tfPassord.setColumns(20);
    jLabel5.setText("(Readable)");
    this.add(cbxEnabled, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(5, 5, 0, 0), 0, 0));
    this.add(jLabel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                             new Insets(5, 5, 0, 0), 0, 0));
    this.add(tfHost, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0
                                            , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                            new Insets(5, 5, 0, 5), 0, 0));
    this.add(jLabel2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                             new Insets(5, 5, 0, 0), 0, 0));
    this.add(tfPort, new GridBagConstraints(1, 2, 1, 1, 0.3, 0.0
                                            , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                            new Insets(5, 5, 0, 5), 40, 0));
    this.add(jLabel3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                             new Insets(5, 5, 0, 0), 0, 0));
    this.add(tfUser, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
                                            , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                            new Insets(5, 5, 0, 5), 0, 0));
    this.add(jLabel4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                             new Insets(5, 5, 0, 0), 0, 0));
    this.add(tfPassord, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0
                                               , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                               new Insets(5, 5, 0, 5), 0, 0));
    this.add(jLabel5, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                             new Insets(5, 5, 0, 5), 0, 0));
    this.add(jPanel1, new GridBagConstraints(0, 5, 1, 1, 0.0, 1.0
                                             , GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                                             new Insets(0, 0, 0, 0), 0, 0));
  }

  /**
   * Enable/disable the proxy text fields depending on the state of the cbxEnabled checkbox
   * @param e ActionEvent
   */
  public void cbxEnabled_actionPerformed(ActionEvent e)
  {
    if (cbxEnabled.isSelected())
    {
      tfHost.setEnabled(true);
      tfPort.setEnabled(true);
      tfUser.setEnabled(true);
      tfPassord.setEnabled(true);
    }
    else
    {
      tfHost.setEnabled(false);
      tfPort.setEnabled(false);
      tfUser.setEnabled(false);
      tfPassord.setEnabled(false);
    }
  }

  private JCheckBox cbxEnabled = new JCheckBox();
  private JLabel jLabel1 = new JLabel();
  private JTextField tfHost = new JTextField();
  private JLabel jLabel2 = new JLabel();
  private JTextField tfPort = new JTextField();
  private JLabel jLabel3 = new JLabel();
  private JTextField tfUser = new JTextField();
  private JLabel jLabel4 = new JLabel();
  private JTextField tfPassord = new JTextField();
  private JLabel jLabel5 = new JLabel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JPanel jPanel1 = new JPanel();
}
