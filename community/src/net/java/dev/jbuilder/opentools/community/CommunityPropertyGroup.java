/*
   Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbuilder.opentools.community;

import com.borland.primetime.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.personality.*;
import com.borland.primetime.properties.*;

/**
 * PropertyGroup used to get notified of when the Browser is shown, and to trigger
 * the initialisation of the LH_Community OpenTool category.<br/>
 * This OpenTool is registered in the UI category.
 * @author Ludovic HOCHET
 * @version 0.3, 14/12/04
 * 0.2.2, 10/10/04<br>
 * 0.2.1, 19/08/03<br>
 * 0.2, 19/04/03<br>
 * @created 0.1, 15/04/02
 */

public class CommunityPropertyGroup implements PropertyGroup
{
  /** Topic passed for creating the Community properties dialog **/
  public static final Object COMMUNITY_TOPIC = new Object();

  /** Category for the Community properties **/
  private static final String COMMUNITY_CAT = "net.java.dev.jbuilder.opentools.community";
  /**
   * Delay to wait before loading the LH_Community OpenTool category once notified of a shown browser,
   * this can currently only be changed by editing the ~/.primetime?/user.properties file
   **/
  private static final GlobalIntegerProperty DELAY = new GlobalIntegerProperty(COMMUNITY_CAT, "delay", 2500);

  /**
   * Registers this property group with the property manager
   * @param majorVersion major version number of the PrimeTime version used
   * @param minorVersion minor version number of the PrimeTime version used
   */
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("LH Community PropertyGroup vrs 0.3");
      System.out.println("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." +
                         PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    PropertyManager.registerPropertyGroup(new CommunityPropertyGroup());
  }

  /**
   * Register a static browser listener to get notified of when a browser is shown. Once so,
   * initialise the LH_Community OpenTool category and autoexec the agents registered during
   * the initialisation.<br/>
   * Note: This method is disabled if the Community personality is unchecked in the default project.
   */
  public void initializeProperties()
  {
    // comment this to remove the disabling of the community based opentools when the community personality is unchecked in the default project
    if (!PersonalityManager.inCurrentPersonality(CommunityPersonality.COMMUNITY_PERSONALITIES, PersonalityManager.getCurrentPersonalityContext())) return;

    com.borland.primetime.ide.Browser.addStaticBrowserListener(new com.borland.primetime.ide.BrowserAdapter()
    {
      private void startCommunity()
      {
        com.borland.primetime.ide.Browser.removeStaticBrowserListener(this);

        Thread t = new Thread()
        {
          public void run()
          {
            try
            {
              Thread.sleep(DELAY.getInteger());
              PrimeTime.initializeOpenTools("LH_Community");
              Community.autoexec();
            }
            catch (InterruptedException ex)
            {
              ex.printStackTrace();
            }
          }
        };
        t.start();
      }

      public void browserOpened(Browser browser)
      {
        // this no longer works in JB11...
      }

      public void browserProjectActivated(Browser browser, Project project)
      {
        startCommunity();
      }
    }); // addStatic
  }

  /**
   * Currently unused method required by the PropertyGroup interface. Always return <b>null</b>.
   * @param topic Property topic to display
   * @return PropertyPageFactory for the given topic, or <b>null</b> if this group is not concerned by the topic
   */
  public PropertyPageFactory getPageFactory(Object topic)
  {
//    if (topic == COMMUNITY_TOPIC)
//    {
//      return new PropertyPageFactory("Community")
//      {
//        public PropertyPage createPropertyPage()
//        {
//          return new PropertyPage();
//        }
//      };
//    }
    return null; // else not our business
  }

}
