package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:06 $
 * @created 6/08/03
 */

public class PresenceHandler extends FinalDelegatedHandler
{
  private Presence presence;
  private static final int NONE = -1;
  private static final int SHOW = 0;
  private static final int STATUS = 1;
  private static final int PRIORITY = 2;
  private static final int ERROR = 3;
  private int elt = NONE;

  public PresenceHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (qName.equals("presence"))
    {
      presence = new Presence();
      parent.setStanza(presence);
      presence.setId(attributes.getValue("id"));
      presence.setFrom(attributes.getValue("from"));
      presence.setTo(attributes.getValue("to"));
      presence.setRawType(attributes.getValue("type"));

      String type = attributes.getValue("type");
      if (type == null)
      {
        presence.setType(Presence.AVAILABLE);
      }
      else if (type.equals(""))
      {
        presence.setType(Presence.AVAILABLE);
      }
      else if (type.equals("available"))
      {
        presence.setType(Presence.AVAILABLE);
      }
      else if (type.equals("unavailable"))
      {
        presence.setType(Presence.UNAVAILABLE);
      }
      else if (type.equals("subscribe"))
      {
        presence.setType(Presence.SUBSCRIBE);
      }
      else if (type.equals("subscribed"))
      {
        presence.setType(Presence.SUBSCRIBED);
      }
      else if (type.equals("unsubscribe"))
      {
        presence.setType(Presence.UNSUBSCRIBE);
      }
      else if (type.equals("unsubscribed"))
      {
        presence.setType(Presence.UNSUBSCRIBED);
      }
      else if (type.equals("probe"))
      {
        presence.setType(Presence.PROBE);
      }
      else if (type.equals("error"))
      {
        presence.setType(Presence.ERROR);
      }

    }
    else if (qName.equals("show"))
    {
      elt = SHOW;
    }
    else if (qName.equals("status"))
    {
      elt = STATUS;
    }
    else if (qName.equals("priority"))
    {
      elt = PRIORITY;
    }
    else if (qName.equals("error"))
    {
      elt = ERROR;
      try
      {
        presence.setErrorcode(Integer.parseInt(attributes.getValue("code")));
      }
      catch (NumberFormatException ex)
      {
        presence.setErrorcode(-1);
      }
    }
    else
    {
      elt = NONE;
    }
  }

  public void characters(char[] ch, int start, int length) throws SAXException
  {
    String txt = new String(ch, start, length);
    txt = txt.trim();

    switch (elt)
    {
      case SHOW:
        presence.setShow(txt);
        break;
      case STATUS:
        presence.setStatus(txt);
        break;
      case PRIORITY:
        try
        {
          presence.setPriority(Integer.parseInt(txt));
        }
        catch (NumberFormatException ex)
        {
          presence.setPriority(-1);
        }
        break;
      case ERROR:
        presence.setErrormessage(txt);
        break;
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {

    if (qName.equals("presence"))
    {
      parent.release();
    }
  }

}