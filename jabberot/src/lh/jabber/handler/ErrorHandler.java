package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.*;
import java.util.logging.*;


/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:43 $
 * @created 6/08/03
 */

public class ErrorHandler extends FinalDelegatedHandler
{
  private Logger log = Logger.getLogger(ErrorHandler.class.getName());

  private JabberError error = null;

  public ErrorHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (qName.equals("stream:error"))
    {
      Stanza stanza = parent.getStanza();
      if (stanza == null)
      {
        error = new JabberError();
        error.setRawType(attributes.getValue("type"));
        error.setFrom(attributes.getValue("from"));
        error.setTo(attributes.getValue("to"));
        error.setId(attributes.getValue("id"));
      }
      else
      {
        error = new JabberError(stanza);
      }
      parent.setStanza(error);

      try
      {
        error.setCode(Integer.parseInt(attributes.getValue("code")));
      }
      catch (NumberFormatException nfe)
      {
        nfe.printStackTrace();
      }
//      error.setXmppType(attributes.getValue("type"));
    }
    else if (qName.equals("iq"))
    {
    }
    else
    {
//      log.warning("unhandled jabber error element: " + qName.toString());
//      error.setXmppName(localName);
    }
  }

  public void characters(char[] ch, int start, int length) throws SAXException
  {
    if ((length > 0) && (error != null)) error.setMsg(new String(ch, start, length));
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (qName.equals("stream:error"))
    {
      log.warning("JabberError: msg id = " + error.getPacketid() + ", code = " + error.getCode() + ", msg = " + error.getMsg());
      parent.release();
    }
  }
}
