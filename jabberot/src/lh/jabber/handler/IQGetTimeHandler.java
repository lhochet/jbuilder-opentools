package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:50 $
 * @created 6/08/03
 */

public class IQGetTimeHandler extends FinalDelegatedHandler
{
  public IQGetTimeHandler()
  {
  }
  public void endElement(String uri, String localName, String qName) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
      GetTimeStanza stanza = new GetTimeStanza(parent.getStanza());
      parent.setStanza(stanza);
      parent.release();
    }
  }

}