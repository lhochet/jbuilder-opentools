package lh.jabber.handler;

import lh.jabber.RosterItem;
import lh.jabber.stanza.RosterStanza;
import org.xml.sax.SAXException;
import java.util.Vector;
import org.xml.sax.Attributes;
import lh.jabber.JabberId;

/**
 * <p>Title: Jabber OpenTool</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:03 $
 */
public class IQSetRoster extends FinalDelegatedHandler
{
  private RosterItem item = new RosterItem();

  public IQSetRoster()
  {
  }
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
//      items = new Vector();
    }
    else if (localName.equals("item"))
    {
      //<item jid='lhochet@localhost' name='lhochet' subscription='none' ask='subscribe'/>
      item.setJid(new JabberId(attributes.getValue("jid")));
      item.setName(attributes.getValue("name"));
      item.setGroup(attributes.getValue("group"));
      item.setSubscription(attributes.getValue("subscription"));
      item.setAsk(attributes.getValue("ask"));
//      items.addElement(item);
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (qName.equals("query"))
    {
      RosterStanza stanza = new RosterStanza(parent.getStanza());
      stanza.setUpdate(true);
      stanza.setItem(item);

      parent.setStanza(stanza);

      parent.release();
    }
  }
}
