package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.BuzzStanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:37 $
 * @created 24/08/03
 */

public class BuzzMessageHandler extends FinalDelegatedHandler
{
  public BuzzMessageHandler()
  {
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("x"))
    {
      BuzzStanza stanza = new BuzzStanza(parent.getStanza());
      parent.setStanza(stanza);
      parent.release();
    }
  }
}