package lh.jabber.handler;

import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:41 $
 * @created 6/08/03
 */

public interface DelegatingHandler
{
  public DelegatedHandler getDelegated();
  public void setupDelegated(String key);
  public void release();
  public void setStanza(Stanza stanza);
  public Stanza getStanza();
}