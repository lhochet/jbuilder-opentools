package lh.jabber.handler;

import org.xml.sax.SAXException;
import lh.jabber.JabberService;
import org.xml.sax.Attributes;
import lh.jabber.ResultListener;
import java.util.HashMap;
import lh.jabber.stanza.JabberError;
import java.util.logging.Logger;

/**
 * <p>Title: Jabber OpenTool</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:45 $
 */
public class IQErrorHandler extends DelegatedDelegatingHandlerImpl
{
  private Logger log = Logger.getLogger(IQErrorHandler.class.getName());

  private JabberService service = null;

  private JabberError error = null;
  private String msg;

  public IQErrorHandler(JabberService service)
  {
    this.service = service;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    log.finest("start : " + qName);
    if (getDelegated() == null)
    {
        if (qName.equals("iq"))
        {
          error = new JabberError(getStanza());
          parent.setStanza(error);
          error.setPacketid(attributes.getValue("id"));
//          if (key != null)
//          {
//            {
//              listener.notifyResult();
//
//              setStanza(null);
//              parent.release();
//            }
//          }
//          return;
        }
        else  if (qName.equals("error"))
        {
          error.setCode(Integer.parseInt(attributes.getValue("code")));
        }
    }
    else
    {
      super.startElement(uri, localName, qName, attributes);
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    log.finest("end : " + qName);
//    if (getDelegated() == null)
//    {
//    if (qName.equals("query"))
//    {
//      super.endElement(uri, localName, qName);
//      parent.release();
//    }
    /*else*/ if (qName.equals("error"))
    {
      error.setMsg(msg);
      super.endElement(uri, localName, qName);
      parent.release();
    }
    else
    {
      super.endElement(uri, localName, qName);
    }
//    }
//    else
//    {
//      super.endElement(uri, localName, qName);
//    }
  }


  public void characters(char[] ch, int start, int length) throws SAXException
  {
    if (getDelegated() == null)
    {
      msg = new String(ch, start, length);
      log.finest("chars : " + msg);
    }
    else
    {
      super.characters(ch, start, length);
    }
  }

}
