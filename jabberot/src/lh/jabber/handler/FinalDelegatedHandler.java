package lh.jabber.handler;

import org.xml.sax.helpers.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:44 $
 * @created 6/08/03
 */

public class FinalDelegatedHandler extends DefaultHandler implements DelegatedHandler
{
  protected DelegatingHandler parent = null;

  public FinalDelegatedHandler()
  {
  }

  public void setParent(DelegatingHandler parent)
  {
    this.parent = parent;
  }
}