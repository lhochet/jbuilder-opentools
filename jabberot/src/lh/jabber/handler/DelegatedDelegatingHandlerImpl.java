package lh.jabber.handler;

import lh.jabber.stanza.*;


/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:39 $
 * @created 6/08/03
 */

public class DelegatedDelegatingHandlerImpl extends DelegatingHandlerImpl implements DelegatedHandler
{
  DelegatingHandler parent = null;

  public DelegatedDelegatingHandlerImpl()
  {
  }

  public void setParent(DelegatingHandler parent)
  {
    this.parent = parent;
  }
  public void setStanza(Stanza stanza)
  {
    parent.setStanza(stanza);
  }
  public Stanza getStanza()
  {
    return parent.getStanza();
  }

}