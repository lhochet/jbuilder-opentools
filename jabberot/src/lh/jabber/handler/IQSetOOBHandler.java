package lh.jabber.handler;

import lh.jabber.stanza.IQSetOOBStanza;
import org.xml.sax.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:01 $
 * @created 15/11/03
 */

public class IQSetOOBHandler extends FinalDelegatedHandler
{
  private IQSetOOBStanza stanza = null;

  private static final int NONE = -1;
  private static final int URL = 0;
  private static final int DESC = 1;
  private int step = NONE;

  public IQSetOOBHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
      stanza = new IQSetOOBStanza(parent.getStanza());
      step = NONE;
    }
    else if (localName.equals("url"))
    {
      step = URL;
    }
    else if (localName.equals("desc"))
    {
      step = DESC;
    }
    else
    {
      step = NONE;
    }
  }
  public void characters(char[] ch, int start, int length) throws SAXException
  {
    String str = new String(ch, start, length);
    str = str.trim();
    switch (step)
    {
      case URL:
        stanza.setUrl(str);
        break;
      case DESC:
        stanza.setDesc(str);
        break;
    }
  }
  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("query"))
    {
      parent.setStanza(stanza);
      parent.release();
    }
  }
}
