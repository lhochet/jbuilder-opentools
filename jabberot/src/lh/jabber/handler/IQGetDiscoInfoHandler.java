package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.IQGetDiscoInfoStanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:46 $
 * @created 24/08/03
 */

public class IQGetDiscoInfoHandler extends FinalDelegatedHandler
{
  private IQGetDiscoInfoStanza stanza = null;

  public IQGetDiscoInfoHandler()
  {
  }
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
      stanza = new IQGetDiscoInfoStanza(parent.getStanza());
      stanza.setNode(attributes.getValue("node"));
    }
  }
  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("query"))
    {
      parent.setStanza(stanza);
      parent.release();
    }
  }
}