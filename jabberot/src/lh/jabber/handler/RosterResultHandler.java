package lh.jabber.handler;

import org.xml.sax.*;
import java.util.*;
import lh.jabber.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:07 $
 * @created 6/08/03
 */

public class RosterResultHandler extends FinalDelegatedHandler
{
  private Vector items = null;

  public RosterResultHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
      items = new Vector();
    }
    else if (localName.equals("item"))
    {
      //<item jid='lhochet@localhost' name='lhochet' subscription='none' ask='subscribe'/>
      RosterItem item = new RosterItem();
      item.setJid(new JabberId(attributes.getValue("jid")));
      item.setName(attributes.getValue("name"));
      item.setGroup(attributes.getValue("group"));
      item.setSubscription(attributes.getValue("subscription"));
      item.setAsk(attributes.getValue("ask"));
      items.addElement(item);
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (qName.equals("query"))
    {
      RosterStanza stanza = new RosterStanza(parent.getStanza());
      stanza.setItems(items);
      parent.setStanza(stanza);

      parent.release();
    }
  }
}
