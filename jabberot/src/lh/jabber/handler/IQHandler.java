package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:52 $
 * @created 6/08/03
 */

public class IQHandler extends DelegatedDelegatingHandlerImpl
{
  public IQHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if /*((getDelegated() == null) && */(qName.equals("iq"))//)
    {
        setupDelegated(attributes.getValue("type"));
        Stanza stanza = new Stanza();
        setStanza(stanza);

          stanza.setRawType(attributes.getValue("type"));
          stanza.setFrom(attributes.getValue("from"));
          stanza.setTo(attributes.getValue("to"));
          stanza.setId(attributes.getValue("id"));

        super.startElement(uri, localName, qName, attributes);
    }
    else
    {
      super.startElement(uri, localName, qName, attributes);
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (qName.equals("iq"))
    {
      parent.release();
    }
    else
    {
      super.endElement(uri, localName, qName);
    }
  }
}
