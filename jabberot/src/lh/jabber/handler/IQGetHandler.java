package lh.jabber.handler;

import org.xml.sax.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:48 $
 * @created 6/08/03
 */

public class IQGetHandler extends DelegatedDelegatingHandlerImpl
{
  public IQGetHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (getDelegated() == null)
    {
        if (qName.equals("iq"))
        {
          return;
        }
        else  if (qName.equals("query"))
        {
          setupDelegated(attributes.getValue("xmlns"));
          super.startElement(uri, localName, qName, attributes);
        }
    }
    else
    {
      super.startElement(uri, localName, qName, attributes);
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
      if (qName.equals("query"))
      {
        super.endElement(uri, localName, qName);
       parent.release();
      }
      else
      {
        super.endElement(uri, localName, qName);
      }
  }
}