package lh.jabber.handler;


import org.xml.sax.*;
import java.util.*;
import lh.jabber.stanza.RegisterStanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:55 $
 * @created 20/08/03
 */

public class IQResultRegisterHandler extends FinalDelegatedHandler
{
  final int NONE = 0;
  final int INSTRUCTIONS = 1;
  private int step = NONE;

  private Vector fields = null;
  private StringBuffer buf = new StringBuffer();

  private String instructions = null;
  public IQResultRegisterHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    step = NONE;
    if (localName.equals("query"))
    {
      fields = new Vector();
    }
    else if (localName.equals("instructions"))
    {
      step = INSTRUCTIONS;
      buf.delete(0, buf.length());
    }
    else
    {
      fields.add(localName);
    }
  }
  public void characters(char[] ch, int start, int length) throws SAXException
  {
    String str = new String(ch, start, length);
    str = str.trim();
    switch (step)
    {
      case INSTRUCTIONS:
        buf.append(str);
        buf.append("\n");
        break;
    }
  }
  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("query"))
    {
      RegisterStanza stanza = new RegisterStanza(parent.getStanza());
      stanza.setInstructions(instructions);
      stanza.setFields(fields);
      parent.setStanza(stanza);
      parent.release();
    }
    else if (localName.equals("instructions"))
    {
      instructions = buf.toString();
    }
  }
}