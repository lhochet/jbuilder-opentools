package lh.jabber.handler;

import java.util.*;

import java.awt.*;

import org.xml.sax.*;
import lh.jabber.stanza.*;
import lh.jabber.whiteboard.*;
import lh.jabber.whiteboard.Rectangle;
import lh.jabber.whiteboard.Shape;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:08 $
 * @created 30/10/03
 */

public class WhiteBoardHandler extends FinalDelegatedHandler
{
  private WhiteBoardStanza stanza = null;

  public WhiteBoardHandler()
  {
  }
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("x"))
    {
      stanza = new WhiteBoardStanza(parent.getStanza());
      parent.setStanza(stanza);
    }
    else if (localName.equals("path"))
    {
      stanza.addShape(createPath(attributes.getValue("p")));
    }
    else if (localName.equals("rect"))
    {
      stanza.addShape(createRect(attributes.getValue("start_x"), attributes.getValue("start_y"), attributes.getValue("end_x"), attributes.getValue("end_y")));
    }
    else if (localName.equals("line"))
    {
      stanza.addShape(createLine(attributes.getValue("start_x"), attributes.getValue("start_y"), attributes.getValue("end_x"), attributes.getValue("end_y")));
    }
    else if (localName.equals("clear"))
    {
      stanza.addShape(createClear());
    }
  }

  /**
   * createClear
   *
   * @return Shape
   */
  private Shape createClear()
  {
    return new Clear();
  }

  /**
   * createLine
   *
   * @param start_x String
   * @param start_y String
   * @param end_x String
   * @param end_y String
   * @return Shape
   */
  private Shape createLine(String start_x, String start_y, String end_x, String end_y)
  {
    Shape ret = new Line();
    ret.start(new Point(Integer.parseInt(start_x), Integer.parseInt(start_y)));
    ret.end(new Point(Integer.parseInt(end_x), Integer.parseInt(end_y)));
    return ret;
  }

  /**
   * createRect
   *
   * @param start_x String
   * @param start_y String
   * @param end_x String
   * @param end_y String
   * @return Shape
   */
  private Shape createRect(String start_x, String start_y, String end_x, String end_y)
  {
    Shape ret = new Rectangle();
    ret.start(new Point(Integer.parseInt(start_x), Integer.parseInt(start_y)));
    ret.end(new Point(Integer.parseInt(end_x), Integer.parseInt(end_y)));
    return ret;
  }

  /**
   * createPath
   *
   * @param path String
   * @return Shape
   */
  private Shape createPath(String path)
  {
    Shape ret = new Path();
    boolean first = true;

    StringTokenizer stk = new StringTokenizer(path, " ");
    while (stk.hasMoreTokens())
    {
      int x = Integer.parseInt(stk.nextToken());
      int y = Integer.parseInt(stk.nextToken());
      if (first)
      {
        ret.start(new Point(x, y));
        first = false;
      }
      else
      {
        ret.intermediate(new Point(x, y));
      }
    }
    return ret;
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("x"))
    {
      parent.release();
    }
  }
}
