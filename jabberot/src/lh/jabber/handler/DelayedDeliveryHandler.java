package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:38 $
 * @created 30/07/04
 */
public class DelayedDeliveryHandler extends FinalDelegatedHandler
{
  public DelayedDeliveryHandler()
  {
    super();
  }
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("x"))
    {
      String stamp = attributes.getValue("stamp");
      Stanza stanza = parent.getStanza();
      if (stanza instanceof Message)
      {
        Message message = (Message)stanza;
        message.setTime(stamp);
      }
//      stanza = new WhiteBoardStanza(parent.getStanza());
//      parent.setStanza(stanza);
    }
//    else if (localName.equals("path"))
//    {
//      stanza.addShape(createPath(attributes.getValue("p")));
//    }
//    else if (localName.equals("rect"))
//    {
//      stanza.addShape(createRect(attributes.getValue("start_x"), attributes.getValue("start_y"), attributes.getValue("end_x"), attributes.getValue("end_y")));
//    }
//    else if (localName.equals("line"))
//    {
//      stanza.addShape(createLine(attributes.getValue("start_x"), attributes.getValue("start_y"), attributes.getValue("end_x"), attributes.getValue("end_y")));
//    }
//    else if (localName.equals("clear"))
//    {
//      stanza.addShape(createClear());
//    }
  }

//  public void characters(char[] ch, int start, int length) throws SAXException
//  {
//      String txt = new String(ch, start, length);
//          txt = txt.trim();
//          msg.setThread(txt);
//  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("x"))
    {
      parent.release();
    }
  }
}
