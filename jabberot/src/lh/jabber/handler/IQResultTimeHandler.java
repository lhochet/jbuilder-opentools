package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:56 $
 * @created 6/08/03
 */

public class IQResultTimeHandler extends FinalDelegatedHandler
{
  private ClientTime clienttime = null;

  private static final int NONE = -1;
  private static final int UTC = 0;
  private static final int TZ = 1;
  private static final int DISPLAY = 2;
  private int step = NONE;

  public IQResultTimeHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
      clienttime = new ClientTime();
      step = NONE;
    }
    else if (localName.equals("utc"))
    {
      step = UTC;
    }
    else if (localName.equals("tz"))
    {
      step = TZ;
    }
    else if (localName.equals("display"))
    {
      step = DISPLAY;
    }
    else
    {
      step = NONE;
    }
  }
  public void characters(char[] ch, int start, int length) throws SAXException
  {
    String str = new String(ch, start, length);
    str = str.trim();
    switch (step)
    {
      case UTC:
        clienttime.utc = str;
        break;
      case TZ:
        clienttime.tz = str;
        break;
      case DISPLAY:
        clienttime.display = str;
        break;
    }
  }
  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("query"))
    {
      ClientTimeStanza stanza = new ClientTimeStanza(parent.getStanza());
      stanza.setClientTime(clienttime);
      parent.setStanza(stanza);
      parent.release();
    }
  }
}