package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:07 $
 * @created 6/08/03
 */

public class StreamHandler extends FinalDelegatedHandler
{
  private JabberService service = null;

  public StreamHandler(JabberService service)
  {
    this.service = service;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
//    if (getDelegated() == null)
//    {
//      if (qName.equals("stream:stream"))
//      {
    service.setSessionId(attributes.getValue("id"));
    service.setStatus(JabberService.CONNECTED);
    service.docontinue();
        parent.release();
//      }
//      else
//      {
//      System.err.println("error");
//        setupDelegated(qName);
//        super.startElement(uri, localName, qName, attributes);
//      }
//    }
//    else
//    {
//      super.startElement(uri, localName, qName, attributes);
//    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    service.setStatus(JabberService.DISCONNECTED);
    parent.release();
  }
}