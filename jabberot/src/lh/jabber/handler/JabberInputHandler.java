package lh.jabber.handler;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.*;
import java.util.logging.*;
import lh.jabber.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:05 $
 * @created 6/08/03
 */

public class JabberInputHandler extends DelegatingHandlerImpl
{
  static Logger log = Logger.getLogger(JabberInputHandler.class.getName());
  static
  {
    log.setLevel(Level.FINEST);
  }

  private JabberService service = null;
  private InputLogger debughandler;

  public JabberInputHandler(JabberService service)
  {
    this.service = service;
    debughandler = new InputLogger(); //new SaxWriter(new PrintWriter(System.out));
  }

  public void release()
  {
    super.release();
    debughandler.flush();
    Stanza stanza = getStanza();
    if (stanza != null)
     {
       service.receivedStanza(stanza);
       setStanza(null);
     }
  }

  public void characters(char[] ch, int start, int length) throws SAXException
  {
    try
    {
      String txt = new String(ch, start, length);

      debughandler.characters(ch, start, length);
      super.characters(ch, start, length);
    }
    catch (SAXException ex)
    {
      log.finest("JabberInputHandler.characters(" + new String(ch, start, length) + ")");
      log.throwing("JabberInputHandler", "characters", ex);
      debughandler.flush();
    }
  }

  public void endDocument() throws SAXException
  {
    debughandler.endDocument();
    debughandler.flush();
    super.endDocument();
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    try
    {
//      log.finest("JabberInputHandler.endElement(" + uri + ", " + localName + ", " + qName + ")");
      debughandler.endElement(uri, localName, qName);
      if (getDelegated() == null)
      {
        setupDelegated(qName);
      }
      super.endElement(uri, localName, qName);
      if (getDelegated() == null)
      {
        debughandler.flush();
      }
    }
    catch (SAXException ex)
    {
      log.finest("JabberInputHandler.endElement(" + uri + ", " + localName + ", " + qName + ")");
      log.throwing("JabberInputHandler", "endElement", ex);
      debughandler.flush();
    }
}

  public void endPrefixMapping(String prefix) throws SAXException
  {
    super.endPrefixMapping(prefix);
  }

  public void ignorableWhiteSpace(char[] ch, int start, int length) throws SAXException
  {
    debughandler.ignorableWhitespace(ch, start, length);
  }

  public void processingInstruction(String target, String data) throws SAXException
  {
    debughandler.processingInstruction(target, data);
  }

  public void startDocument() throws SAXException
  {
    debughandler.startDocument();
    debughandler.flush();
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
  {
    try
    {
      debughandler.startElement(uri, localName, qName, attributes);
      if (getDelegated() == null)
      {
        setupDelegated(qName);
      }
      super.startElement(uri, localName, qName, attributes);
    }
    catch (SAXException ex)
    {
      log.finest("JabberInputHandler.startElement(" + uri + ", " + localName + ", " + qName + ")");
      log.throwing("JabberInputHandler", "startElement", ex);
      debughandler.flush();
    }
  }

  public void startPrefixMapping(String prefix, String uri) throws SAXException
  {
    super.startPrefixMapping(prefix, uri);
  }

  public void error(SAXParseException e) throws SAXException
  {
    System.err.println("SAX error:" + e);
    e.printStackTrace();
    log.throwing("JabberInputHandler", "error", e);
    throw e;
  }

  public void fatalError(SAXParseException e) throws SAXException
  {
    System.err.println("SAX fatal error :" + e);
    e.printStackTrace();
    log.throwing("JabberInputHandler", "fatalError", e);
    throw e;
  }

  public void warning(SAXParseException e) throws SAXException
  {
    System.err.println("SAX warning :" + e);
    e.printStackTrace();
    log.throwing("JabberInputHandler", "warning", e);
  }
}

