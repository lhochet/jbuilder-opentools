package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:58 $
 * @created 6/08/03
 */

public class IQResultVersionHandler extends FinalDelegatedHandler
{
  private ClientVersion clientversion = null;

  private static final int NONE = -1;
  private static final int NAME = 0;
  private static final int VERSION = 1;
  private static final int OS = 2;
  private int step = NONE;

  public IQResultVersionHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
      clientversion = new ClientVersion();
      step = NONE;
    }
    else if (localName.equals("name"))
    {
      step = NAME;
    }
    else if (localName.equals("version"))
    {
      step = VERSION;
    }
    else if (localName.equals("os"))
    {
      step = OS;
    }
    else
    {
      step = NONE;
    }
  }
  public void characters(char[] ch, int start, int length) throws SAXException
  {
    String str = new String(ch, start, length);
    str = str.trim();
    switch (step)
    {
      case NAME:
        clientversion.name = str;
        break;
      case VERSION:
        clientversion.version = str;
        break;
      case OS:
        clientversion.os = str;
        break;
    }
  }
  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("query"))
    {
      ClientVersionStanza stanza = new ClientVersionStanza(parent.getStanza());
      stanza.setClientVersion(clientversion);
      parent.setStanza(stanza);
      parent.release();
    }
  }
}