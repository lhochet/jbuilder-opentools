package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.LastActivityStanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:54 $
 * @created 7/08/03
 */

public class IQResultLastActivityHandler extends FinalDelegatedHandler
{

  private int seconds = 0;

  public IQResultLastActivityHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (localName.equals("query"))
    {
      try
      {
        seconds = Integer.parseInt(attributes.getValue("seconds"));
      }
      catch (NumberFormatException ex)
      {
        // ignore
      }
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (localName.equals("query"))
    {
      LastActivityStanza stanza = new LastActivityStanza(parent.getStanza());
      stanza.setSeconds(seconds);
      parent.setStanza(stanza);
      parent.release();
    }
  }
}