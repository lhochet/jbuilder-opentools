package lh.jabber.handler;

import org.xml.sax.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:06 $
 * @created 6/08/03
 */

public class MessageHandler extends DelegatedDelegatingHandlerImpl
{

  private Message msg;
  private static final int NONE = -1;
  private static final int THREAD = 0;
  private static final int SUBJECT = 1;
  private static final int BODY = 2;
  private static final int X = 3;
  private int elt = NONE;

  private StringBuffer buf = new StringBuffer();

  public MessageHandler()
  {
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.
      SAXException
  {
    if (getDelegated() == null)
    {
      if (qName.equals("message"))
      {
        msg = new Message();
//        msg.setType(Message.CHAT);
        parent.setStanza(msg);

        msg.setRawType(attributes.getValue("type"));
        msg.setType(Message.toType(attributes.getValue("type")));
        msg.setFrom(attributes.getValue("from"));
        msg.setTo(attributes.getValue("to"));
        msg.setId(attributes.getValue("id"));
      }
      else if (qName.equals("thread"))
      {
        elt = THREAD;
      }
      else if (qName.equals("subject"))
      {
        elt = SUBJECT;
      }
      else if (qName.equals("body"))
      {
        elt = BODY;
        buf.delete(0, buf.length());
      }
      else if (qName.equals("x"))
      {
        setupDelegated(attributes.getValue("xmlns"));
        super.startElement(uri, localName, qName, attributes);
      }
      else
      {
        elt = NONE;
      }
    }
    else
    {
      super.startElement(uri, localName, qName, attributes);
    }
  }

  public void characters(char[] ch, int start, int length) throws SAXException
  {
    if (getDelegated() == null)
    {
      String txt = new String(ch, start, length);

      switch (elt)
      {
        case THREAD:
          txt = txt.trim();
          msg.setThread(txt);
          break;
        case SUBJECT:
          txt = txt.trim();
          msg.setSubject(txt);
          break;
        case BODY:
          buf.append(txt);
//          buf.append("\n");
          break;
      }
    }
    else
    {
      super.characters(ch, start, length);
    }
  }


  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (getDelegated() == null)
    {

      elt = NONE;

      if (qName.equals("message"))
      {
        parent.release();
      }
      else if (qName.equals("body"))
      {
        msg.setBody(buf.toString());
      }
    }
    else
    {
      super.endElement(uri, localName, qName);
    }
  }


}

