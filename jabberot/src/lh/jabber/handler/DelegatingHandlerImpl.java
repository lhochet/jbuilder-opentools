package lh.jabber.handler;

import org.xml.sax.helpers.*;
import java.util.logging.*;
import java.util.*;
import org.xml.sax.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:42 $
 * @created 6/08/03
 */

public class DelegatingHandlerImpl extends DefaultHandler implements DelegatingHandler
{
//  Logger log = Logger.getLogger("lh.jabber");

  private HashMap handlers = new HashMap();
  private DelegatedHandler delegated;
  private Stanza stanza = null;

  public DelegatingHandlerImpl()
  {
  }

  public DelegatedHandler getDelegated()
  {
    return delegated;
  }

  protected void setDelegated(DelegatedHandler handler)
  {
    delegated = handler;
  }

  public void release()
  {
    delegated = null;
  }

  public void setStanza(Stanza stanza)
  {
    this.stanza = stanza;
  }
  public Stanza getStanza()
  {
    return stanza;
  }


  public void addHandler(String key, DelegatedHandler handler)
  {
    handlers.put(key, handler);
  }

  public void setupDelegated(String key)
  {
    if (key == null) return;
    delegated = (DelegatedHandler)handlers.get(key);
    if (delegated != null) delegated.setParent(this);
  }

  public void setupNonNullDelegated(String key)
  {
    DelegatedHandler tdelegated = (DelegatedHandler)handlers.get(key);
    if (tdelegated != null)
    {
      delegated = tdelegated;
      delegated.setParent(this);
    }
  }


  public void characters(char[] ch, int start, int length) throws SAXException
  {
    if (delegated != null) delegated.characters(ch, start, length);
  }

  public void endDocument() throws SAXException
  {
    if (delegated != null) delegated.endDocument();
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (delegated != null) delegated.endElement(uri, localName, qName);
  }

  public void endPrefixMapping(String prefix) throws SAXException
  {
    if (delegated != null) delegated.endPrefixMapping(prefix);
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
  {
    if (delegated != null) delegated.startElement(uri, localName, qName, attributes);
  }

  public void startPrefixMapping(String prefix, String uri) throws SAXException
  {
    if (delegated != null) delegated.startPrefixMapping(prefix, uri);
  }


}
