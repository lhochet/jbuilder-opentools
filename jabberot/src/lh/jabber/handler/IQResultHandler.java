package lh.jabber.handler;

import org.xml.sax.*;
import java.util.*;
import lh.jabber.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:53 $
 * @created 6/08/03
 */

public class IQResultHandler extends DelegatedDelegatingHandlerImpl
{
  private JabberService service = null;

  private HashMap resultlisteners = new HashMap();

  public IQResultHandler(JabberService service)
  {
    this.service = service;
  }

  public void addListener(String key, ResultListener listener)
  {
    resultlisteners.put(key, listener);
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) throws org.xml.sax.SAXException
  {
    if (getDelegated() == null)
    {
        if (qName.equals("iq"))
        {
          Stanza stanza = getStanza();
          if (stanza == null)
          {
            stanza = new IQResultStanza();
          }
          else
          {
            stanza = new IQResultStanza(stanza);
          }
          setStanza(stanza);

//          String key = attributes.getValue("id");
//          if (key != null)
//          {
//            ResultListener listener = (ResultListener) resultlisteners.get(key);
//            if (listener != null)
//            {
//              listener.notifyResult();
//
//              setStanza(null);
//              parent.release();
//            }
//          }
          return;
        }
        else  if (qName.equals("query"))
        {
          setupDelegated(attributes.getValue("xmlns"));
          super.startElement(uri, localName, qName, attributes);
        }
    }
    else
    {
      super.startElement(uri, localName, qName, attributes);
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException
  {
//    if (getDelegated() == null)
//    {
    if (qName.equals("query"))
    {
      super.endElement(uri, localName, qName);
      parent.release();
    }
    else
    {
      super.endElement(uri, localName, qName);
    }
//    }
//    else
//    {
//      super.endElement(uri, localName, qName);
//    }
  }
}
