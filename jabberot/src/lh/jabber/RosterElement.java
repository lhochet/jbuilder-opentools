package lh.jabber;

import lh.jabber.stanza.Presence;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:32 $
 * @created 1/01/04
 */

public class RosterElement
{
  public RosterElement()
  {
  }

  public Presence getPresence()
  {
    return presence;
  }

  protected Presence presence = null;
  protected JabberId jid;
  protected ClientTime clientTime = null;
  protected ClientVersion clientVersion = null;
  protected String name;
  protected Presence sentPresence = null;
  public void setJid(JabberId jid)
  {
    this.jid = jid;
  }

  public void setPresence(Presence presence)
  {
    this.presence = presence;
  }

  public JabberId getJid()
  {
    return jid;
  }

  public void setClientTime(ClientTime t)
  {
    clientTime = t;
  }

  public ClientTime getClientTime()
  {
    return clientTime;
  }

  public void setClientVersion(ClientVersion v)
  {
    clientVersion = v;
  }

  public ClientVersion getClientVersion()
  {
    return clientVersion;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setSentPresence(Presence sentPresence)
  {
    this.sentPresence = sentPresence;
  }

  public Presence getSentPresence()
  {
    return sentPresence;
  }

}
