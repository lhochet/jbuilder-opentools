package lh.jabber;

import java.io.*;
import java.net.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;

import lh.jabber.handler.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:25 $
 * @created 10/01/03
 */

public class JabberService //extends Thread
{
  Logger log = Logger.getLogger(JabberService.class.getName());

  private String clientname;
  private String clientversion;
  private String clientos;

  private JabberId jid;
  private String password;
  private Presence presence;
  private String sessionid;

  private int counter = 0;
  private Presence tmpPresence;

  private RosterListener rosterlistener;

  private Vector messageslisteners = new Vector();
  private Vector newmessageslisteners = new Vector();
  private boolean dispatchingMessage = false;

  private Vector subscriptionListeners = new Vector();
  private Vector availabilityListeners = new Vector();
  private Vector serviceSentPresenceListeners = new Vector();

  private Vector clientsInfoListeners = new Vector();
  private RegisterListener registerlistener;

  private HashMap onetimeerrorlisteners = new HashMap();
  private HashMap resultlisteners = new HashMap();

  private ArrayList errorlisteners = new ArrayList();

  public static final int UNKNOWN = -1;
  public static final int DISCONNECTED = 0;
  public static final int CONNECTED = 1;
  public static final int AUTHENTIFICATED = 2;
  public static final int READY = 3;

  private int status = DISCONNECTED;
  private Vector statusListeners = new Vector();

  private lh.jabber.handler.StreamHandler streamhandler;
  private IQHandler iqhandler;
  private IQResultHandler resulthandler;
  private RosterResultHandler rosterresulthandler;
  private IQGetHandler iqgethandler;
  private IQGetVersionHandler iqgetversionhandler;
  private IQGetTimeHandler iqgettimehandler;
  private IQResultTimeHandler iqtimeresulthandler = null;
  private IQResultVersionHandler iqversionresulthandler = null;

  private MessageHandler messagehandler;

  private PresenceHandler presencehandler;

//  private Hashtable waitingResults;

  private String server = "localhost";
  private int port = 5222;
  private Socket socket;
  private Reader reader;
  private Writer writer;
  private InputProcess inputprocess;
  private OutputSender sender;

  private JabberInputHandler inputhandler;

  private IQResultLastActivityHandler iqlastactivityresulthandler = null;
  private IQGetLastActivityHandler iqgetlastactivityhandler = null;
  private IQResultRegisterHandler iqregisterresulthandler = null;
  private IQSetHandler iqsethandler;
  private IQSetOOBHandler iqsetoobhandler;
  private OOBListener ooblistener = null;

  public JabberService()
  {
//    super("JabberService");
    presence = new Presence();
    presence.setType(Presence.UNAVAILABLE);
    tmpPresence = new Presence();
    tmpPresence.setType(Presence.UNAVAILABLE);
  }
  public JabberService(String server, int port)
  {
    this();
    this.server = server;
    this.port = port;
  }

  public void setClientName(String name)
  {
    this.clientname = name;
  }
  public void setClientVersion(String version)
  {
    this.clientversion = version;
  }
  public void setClientOs(String os)
  {
    this.clientos = os;
  }

  public void setJabberId(JabberId jid)
  {
    this.jid = jid;
  }
  public JabberId getJabberId()
  {
    return jid;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public void setStatus(int status)
  {
    this.status = status;
    fireStatusUpdate(status);
  }
  public int getStatus()
  {
    return status;
  }
  public void addStatusListener(ServiceStatusListener l)
  {
    statusListeners.add(l);
  }
  private void fireStatusUpdate(int lstatus)
  {
    Iterator iter = ((Vector)statusListeners.clone()).iterator();
    while (iter.hasNext())
    {
      ((ServiceStatusListener) iter.next()).serviceStatusUpdate(lstatus);
    }
  }

  public Presence getPresence()
  {
    return presence;
  }

  public void setServer(String server) //throws JabberException
  {
//    if (socket != null) throw new JabberException("Already connected");
    this.server = server;
  }
  public String getServer()
  {
    return server;
  }

  public void setPort(int port) //throws JabberException
  {
//    if (socket != null) throw new JabberException("Already connected");
    this.port = port;
  }
  public int getPort()
  {
    return port;
  }

  public void setSessionId(String sessionid)
  {
    this.sessionid = sessionid;
  }
  public String getSessionId()
  {
    return sessionid;
  }

//  public void run()
//  {
//    try
//    {
//      connect();
//      while (status != DISCONNECTED)
//      {
//        try
//        {
//          Thread.sleep(2000);
//        }
//        catch (InterruptedException ex)
//        {
//          ex.printStackTrace();
//        }
//
//      }
//    }
//    catch (Exception ex)
//    {
//      ex.printStackTrace();
//    }
//
//  }

  private void initHandlers()
  {
    //setup inputhandler
    // Input Handler
    inputhandler = new JabberInputHandler(this);
    // Stream
    streamhandler = new lh.jabber.handler.StreamHandler(this);
    inputhandler.addHandler("stream:stream", streamhandler);

    // Error
    inputhandler.addHandler("stream:error", new lh.jabber.handler.ErrorHandler());

    // IQ
    iqhandler = new IQHandler();
    inputhandler.addHandler("iq", iqhandler);
    // IQ Error
    iqhandler.addHandler("error", new lh.jabber.handler.IQErrorHandler(this));

    // IQ Result
    resulthandler = new IQResultHandler(this);
    iqhandler.addHandler("result", resulthandler);
    // Roster result
    rosterresulthandler = new RosterResultHandler();
    resulthandler.addHandler("jabber:iq:roster", rosterresulthandler);
    // Version result
    iqversionresulthandler = new IQResultVersionHandler();
    resulthandler.addHandler("jabber:iq:version", iqversionresulthandler);
    // time result
    iqtimeresulthandler = new IQResultTimeHandler();
    resulthandler.addHandler("jabber:iq:time", iqtimeresulthandler);
    // last activity result
    iqlastactivityresulthandler = new IQResultLastActivityHandler();
    resulthandler.addHandler("jabber:iq:last", iqlastactivityresulthandler);
    // register result
    iqregisterresulthandler = new IQResultRegisterHandler();
    resulthandler.addHandler("jabber:iq:register", iqregisterresulthandler);


    // IQ Get
    iqgethandler = new IQGetHandler();
    iqhandler.addHandler("get", iqgethandler);
    // Get Version
    iqgetversionhandler = new IQGetVersionHandler();
    iqgethandler.addHandler("jabber:iq:version", iqgetversionhandler);
    // Get Time
    iqgettimehandler = new IQGetTimeHandler();
    iqgethandler.addHandler("jabber:iq:time", iqgettimehandler);
    // Get Last activity
    iqgetlastactivityhandler = new IQGetLastActivityHandler();
    iqgethandler.addHandler("jabber:iq:last", iqgetlastactivityhandler);
    // Disco Items
    iqgethandler.addHandler("http://jabber.org/protocol/disco#items", new IQGetDiscoItemsHandler());
    // Disco Infos
    iqgethandler.addHandler("http://jabber.org/protocol/disco#info", new IQGetDiscoInfoHandler());


    // IQ Set
    iqsethandler = new IQSetHandler();
    iqhandler.addHandler("set", iqsethandler);
    // OOB
    iqsetoobhandler = new IQSetOOBHandler();
    iqsethandler.addHandler("jabber:iq:oob", iqsetoobhandler);
    // Set Roster Item
    iqsethandler.addHandler("jabber:iq:roster", new IQSetRoster());


    // Message
    messagehandler = new MessageHandler();
    inputhandler.addHandler("message", messagehandler);
    // Buzz
    messagehandler.addHandler("lh:jabber:buzz", new BuzzMessageHandler());
    // Whiteboard
    messagehandler.addHandler("lh:jabber:whiteboard", new WhiteBoardHandler());
    // Delayed delivery
    messagehandler.addHandler("jabber:x:delay", new DelayedDeliveryHandler());


    // Presence
    presencehandler = new PresenceHandler();
    inputhandler.addHandler("presence", presencehandler);

  }

  public synchronized void docontinue()
  {
    notify();
  }

  public synchronized void connect() throws IOException, JabberException
  {
    connect(true);
  }
  public synchronized void connect(boolean authentificate) throws IOException, JabberException
  {
    if (socket != null) throw new JabberException("Already connected");
    socket = new Socket(server, port);
    writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
    reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

    if (inputhandler == null) initHandlers();

    inputprocess = new InputProcess(this, reader, inputhandler);
    inputprocess.start();

    sender = new OutputSender(writer);

    sender.write("<stream:stream to='" + server + "' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams'>");
//    sender.write("<stream:stream to='" + server + "' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>");
    sender.flush();

    try
    {
      wait();
    }
    catch (InterruptedException ex1)
    {
      ex1.printStackTrace();
    }
/*
    int attempts = 0;
    while ((status != CONNECTED) && (attempts < 20))
    {
      ++attempts;
      try
      {
        Thread.sleep(100);
      }
      catch (InterruptedException ex)
      {
        ex.printStackTrace();
      }
    }
 */
    if ((status == CONNECTED) && authentificate)
    {
      authentificate();
    }


  }

  public void disconnect()
  {
    if (socket == null) return;
    try
    {
      if (!socket.isClosed())
      {
        sendPresence(Presence.UNAVAILABLE, null, "Disconnected", null, 0);
        sendBye();
        socket.close();
      }
    }
    catch (IOException ex)
    {
      // ignore quietly
    }

    setStatus(DISCONNECTED);

    socket = null;
  }

  public void authentificate() throws IOException, JabberException
  {
    String id = "auth_" + counter++;

    ResultListener rl = new ResultListener()
    {
      public void notifyResult()
      {
        try
        {
          authentificateDigest();
        }
        catch (JabberException ex)
        {
          ex.printStackTrace();
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
        }
      }
    };
    /*resulthandler.*/addResultListener(id, rl);

//      addResultHandler(id,authHandler);
      sender.write("<iq type='get' id='");
      sender.write(id);
      sender.write("'><query xmlns='jabber:iq:auth'><username>");
      sender.write(jid.getUser());
      sender.write("</username></query></iq>");
      sender.flush();

//      int attempts = 0;
//      while ((status != AUTHENTIFICATED) && (attempts < 20))
//      {
//        ++attempts;
//        try
//        {
//          Thread.sleep(100);
//        }
//        catch (InterruptedException ex)
//        {
//          ex.printStackTrace();
//        }
//      }
  }

  private String toHexString(byte[] bytes)
  {
    StringBuffer buf = new StringBuffer(bytes.length * 2);
    for (int i = 0; i < bytes.length; ++i)
    {
      int hex = bytes[i];
      if (hex < 0)
      {
        hex += 256;
      }
      if (hex >= 16)
      {
        buf.append(Integer.toHexString(hex));
      }
      else
      {
        buf.append('0');
        buf.append(Integer.toHexString(hex));
      }
    }
    return buf.toString().toLowerCase();
  }

  private void authentificateDigest() throws IOException, JabberException
  {
    String id = "digest_auth_" + counter++;

    ResultListener rl = new ResultListener()
    {
      public void notifyResult()
      {
        setStatus(READY);
        sendPresence(Presence.AVAILABLE, null, "Available", null, 1);
        try
        {
          initRoster();
        }
        catch (IOException ex)
        {
        }
        catch (JabberException ex)
        {
        }
      }
    };
    /*resulthandler.*/addResultListener(id, rl);

//    ErrorListener el = new ErrorListener()
//    {
//      public void receivedError(JabberError error)
//      {
//        disconnect();
//        Message msg = new Message();
//        msg.setType(Message.ERROR);
//        msg.setSubject("Error: " + error.getCode() + ", " + error.getMsg());
//        msg.setBody("Error: " + error.getCode() + ", in packet " + error.getPacketid() + ", " + error.getMsg());
//        Iterator iter = ((Vector)messageslisteners).iterator();
//        while (iter.hasNext())
//        {
//          ((MessageListener) iter.next()).received(msg);
//        }
//      }
//
//    };
//    addOneTimeErrorListener(id,el);

    // get digest
    String digest = null;

    try
    {
//      MessageDigest md = MessageDigest.getInstance("SHA");
//      md.update(sessionid.getBytes());
//      digest = toHexString(md.digest(password.getBytes()));
      MessageDigest md = MessageDigest.getInstance("SHA");
      String tohash  = sessionid + password;
//      System.out.println("tohash="+tohash);
      md.update(tohash.getBytes());
      digest = toHexString(md.digest());
    }
    catch (NoSuchAlgorithmException ex)
    {
      ex.printStackTrace();
    }

    sender.write("<iq type='set' id='");
    sender.write(id);
    sender.write("'><query xmlns='jabber:iq:auth'><username>");
    sender.write(jid.getUser());
    sender.write("</username><resource>");
    sender.write(jid.getResource());
    sender.write("</resource><digest>");
    sender.write(digest);
    sender.write("</digest></query></iq>");
    sender.flush();



//    int attempts = 0;
//    while ((status != AUTHENTIFICATED) && (attempts < 20))
//    {
//      System.err.println("attempt " + attempts);
//      ++attempts;
//      try
//      {
//        Thread.sleep(100);
//      }
//      catch (InterruptedException ex)
//      {
//        ex.printStackTrace();
//      }
//    }

//    if (status == AUTHENTIFICATED)
//    {
//      setStatus(READY);
//    }
  }

//  private void authentificatePlain() throws IOException, JabberException
//  {
//    String id = "plain_auth_" + counter++;
//
//    ResultListener rl = new ResultListener()
//    {
//      public void notifyResult()
//      {
//        sendPresence(Presence.AVAILABLE, null, "Available", null, 1);
//        try
//        {
//          initRoster();
//        }
//        catch (IOException ex)
//        {
//        }
//        catch (JabberException ex)
//        {
//        }
//      }
//    };
//    /*resulthandler.*/addResultListener(id, rl);
//
//      sender.write("<iq type='set' id='");
//      sender.write(id);
//      sender.write("'><query xmlns='jabber:iq:auth'><username>");
//      sender.write(jid.getUser());
//      sender.write("</username><resource>");
//      sender.write(jid.getResource());
//      sender.write("</resource><password>");
//      sender.write(password);
//      sender.write("</password></query></iq>");
//      sender.flush();
//
//      int attempts = 0;
//      while ((status != AUTHENTIFICATED) && (attempts < 20))
//      {
//        ++attempts;
//        try
//        {
//          Thread.sleep(100);
//        }
//        catch (InterruptedException ex)
//        {
//          ex.printStackTrace();
//        }
//      }
//  }

  private void initRoster() throws IOException, JabberException
  {
    String id = "roster_" + counter++;

//    ResultListener rl = new ResultListener()
//    {
//      public void notifyResult()
//      {
//      }
//    };
//    resulthandler.addListener(id, rl);

// <iq id="jcl_9" type="get"><query xmlns="jabber:iq:roster"/></iq>
      sender.write("<iq type='get' id='");
      sender.write(id);
      sender.write("'><query xmlns='jabber:iq:roster' /></iq>");
      sender.flush();

//      int attempts = 0;
//      while ((status != AUTHENTIFICATED) && (attempts < 20))
//      {
//        ++attempts;
//        try
//        {
//          Thread.sleep(100);
//        }
//        catch (InterruptedException ex)
//        {
//          ex.printStackTrace();
//        }
//      }
  }

  void setRosterItems(Vector items)
  {
    if (rosterlistener != null) rosterlistener.setItems(items);
  }
  void updateRosterItem(RosterItem item)
  {
    if (rosterlistener != null) rosterlistener.updateItem(item);
  }
  public void addRosterListener(RosterListener l)
  {
    rosterlistener = l;
  }

  public void sendPresence(int type, String to, String pstatus, String show, int priority)
  {
    try
    {
      if (status != DISCONNECTED)
      {
        sender.write("<presence ");
        switch (type)
        {
          case Presence.SUBSCRIBE:
            sender.write("type='subscribe' ");
            break;
          case Presence.SUBSCRIBED:
            sender.write("type='subscribed' ");
            break;
          case Presence.UNSUBSCRIBE:
            sender.write("type='unsubscribe' ");
            break;
          case Presence.UNSUBSCRIBED:
            sender.write("type='unsubscribed' ");
            break;
          case Presence.UNAVAILABLE:
            sender.write("type='unavailable' ");
            break;
          case Presence.AVAILABLE:
            // leave, don't add type
            break;
          case Presence.PROBE:
            sender.write("type='probe' ");
            break;
          case Presence.ERROR:
            sender.write("type='error' ");
            break;
          case Presence.INVISIBLE:
            sender.write("type='invisible' ");
            break;
          case Presence.VISIBLE:
            sender.write("type='visible' ");
            break;
        }
        if (to != null)
        {
          sender.write("to='");
          sender.write(to);
          sender.write("' ");
        }
        sender.write(">");
        if (pstatus != null)
        {
          sender.write("<status>");
          sender.write(pstatus);
          sender.write("</status>");
        }
        if (show != null)
        {
          sender.write("<show>");
          sender.write(show);
          sender.write("</show>");
        }
        if (priority > -1)
        {
          sender.write("<priority>");
          sender.write(Integer.toString(priority));
          sender.write("</priority>");
        }
        sender.write("</presence>");
        sender.flush();
      }

      //service presence update
      if (to == null)
      {
        presence.setType(type);
        presence.setTo(to);
        presence.setStatus(pstatus);
        presence.setShow(show);
        presence.setPriority(priority);
      }
      // update temp presence
      tmpPresence.setType(type);
      tmpPresence.setTo(to);
      tmpPresence.setStatus(pstatus);
      tmpPresence.setShow(show);
      tmpPresence.setPriority(priority);
      //fire presence update
      Iterator iter = serviceSentPresenceListeners.iterator();
      while (iter.hasNext())
      {
        ((PresenceUpdateListener)iter.next()).receivedUpdateEvent(tmpPresence);
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void sendMessage(int type, String to, String subject, String message)
  {
    try
    {
      String id = "msg_" + counter++;
      sender.write("<message ");
      switch (type)
      {
        case Message.CHAT:
          sender.write("type='chat' ");
          break;
        case Message.GROUPCHAT:
          sender.write("type='groupchat' ");
          break;
        case Message.HEADLINE:
          sender.write("type='headline' ");
          break;
        case Message.ERROR: // no handled, treat as single
          sender.write("type='error' ");
          break;
        case Message.NORMAL:
        default:
          //add nothing
      }
      if (to != null)
      {
        sender.write("to='");
        sender.write(to);
        sender.write("' ");
      }
      sender.write(" id='");
      sender.write(id);
      sender.write("'>");

      sender.write("<thread>");
      sender.write("thread_");
      sender.write(jid.toString());
      sender.write("_");
      sender.write(to);
      sender.write("</thread>");

      if (subject != null)
      {
        sender.write("<subject>");
        sender.write(subject);
        sender.write("</subject>");
      }
      sender.write("<body>");
      message = lh.util.XmlUtil.escapeXml(message);
      sender.write(message);
      sender.write("</body>");

      sender.write("</message>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }


  public void addMessageListener(MessageListener l)
  {
    if (dispatchingMessage)
    {
//      log.fine("add msg list newmsglis");
      newmessageslisteners.add(l);
    }
    else
    {
//      log.fine("add msg list norm");
      messageslisteners.add(l);
    }
  }

  public void receivedMessage(Message msg)
  {
    dispatchingMessage = true;
    Iterator iter = messageslisteners.iterator();
    while (iter.hasNext())
    {
//      log.fine("calling list");
      ((MessageListener) iter.next()).received(msg);
    }
    messageslisteners.addAll(newmessageslisteners);
    newmessageslisteners.clear();
    dispatchingMessage = false;
  }

  public void addSubscriptionListener(PresenceSubscriptionListener l)
  {
    subscriptionListeners.addElement(l);
  }
  public void addAvailibityListener(PresenceUpdateListener l)
  {
    availabilityListeners.addElement(l);
  }
  public void addServiceSentPresenceListener(PresenceUpdateListener l)
  {
    serviceSentPresenceListeners.addElement(l);
  }
  public void receivedPresence(Presence presence)
  {
    switch (presence.getType())
    {
      case Presence.SUBSCRIBE:
      case Presence.SUBSCRIBED:
      case Presence.UNSUBSCRIBE:
      case Presence.UNSUBSCRIBED:
        // subscription
        Iterator iter = subscriptionListeners.iterator();
        while (iter.hasNext())
        {
          ((PresenceSubscriptionListener)iter.next()).receivedSubscriptionEvent(presence);
        }
        break;
      case Presence.UNAVAILABLE:
      case Presence.AVAILABLE:
        // presence update
        Iterator iter2 = availabilityListeners.iterator();
        while (iter2.hasNext())
        {
          ((PresenceUpdateListener)iter2.next()).receivedUpdateEvent(presence);
        }
        break;
      case Presence.PROBE:
        // probe should return our presence
        /** @todo implement presence.probe response */
        break;
      case Presence.ERROR:
        // ?
        /** @todo implement presence.error response */
        break;
    }
  }

  public void requestVersion(JabberId jid)
  {
    String to;
    if (jid == null)
    {
      to = server;
    }
    else
    {
      to = jid.getFullJID();
    }

    String id = "version_" + counter++;

//    ResultListener rl = new ResultListener()
//    {
//      public void notifyResult()
//      {
//        /** @todo add something to show it to the UI */
//      }
//    };
//    resulthandler.addListener(id, rl);

    try
    {
      sender.write("<iq type='get' id='");
      sender.write(id);
      sender.write("' to='");
      sender.write(to);
      sender.write("'><query xmlns='jabber:iq:version' /></iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
  public void sendVersionInfo(String id, String to)
  {

    try
    {
      log.info("Version sent to: " + to);

      sender.write("<iq type='result' id='");
      if (id == null) id = "version_" + counter++;
      sender.write(id);
      sender.write("' to='");
      sender.write(to);
      sender.write("'><query xmlns='jabber:iq:version'>");
      sender.write("<name>");
      if (clientname == null) clientname = "LH Java Jabber Client";
      sender.write(clientname);
//      sender.write(" ");
//      sender.write(clientversion);
      sender.write("</name>");
      if (clientversion != null)
      {
        sender.write("<version>");
        sender.write(clientversion);
        sender.write("</version>");
      }
      if (clientos != null)
      {
        sender.write("<os>");
        sender.write(clientos);
        sender.write("</os>");
      }
      sender.write("</query>");
      sender.write("</iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void requestTime(JabberId jid)
  {
    String to;
    if (jid == null)
    {
      to = server;
    }
    else
    {
      to = jid.getFullJID();
    }

    String id = "time_" + counter++;

//    ResultListener rl = new ResultListener()
//    {
//      public void notifyResult()
//      {
//        /** @todo add something to show it to the UI */
//      }
//    };
//    resulthandler.addListener(id, rl);

    try
    {
      sender.write("<iq type='get' id='");
      sender.write(id);
      sender.write("' to='");
      sender.write(to);
      sender.write("'><query xmlns='jabber:iq:time' /></iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
  public void sendTimeInfo(String id, String to)
  {

    try
    {
      log.info("Time requested by: " + to);

//      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
      Date now = new Date();
      sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
      String time = sdf.format(now);

      String tz = TimeZone.getDefault().getDisplayName();

      sender.write("<iq type='result' id='");
      if (id == null) id = "time_" + counter++;
      sender.write(id);
      sender.write("' to='");
      sender.write(to);
      sender.write("'><query xmlns='jabber:iq:time'>");
      sender.write("<utc>");
      sender.write(time);
      sender.write("</utc>");
//      if (tz != null)
//      {
        sender.write("<tz>");
        sender.write(tz);
        sender.write("</tz>");
//      }
      sender.write("<display>");
      sender.write(now.toString());
      sender.write("</display>");
      sender.write("</query>");
      sender.write("</iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void setClientVersion(String jid, ClientVersion cv)
  {
    Iterator iter = clientsInfoListeners.iterator();
    while (iter.hasNext()) {
      ClientsInfoListener item = (ClientsInfoListener)iter.next();
      item.receivedVersion(jid, cv);
    }
  }

  public void setClientTime(String jid, ClientTime ct)
  {
    Iterator iter = clientsInfoListeners.iterator();
    while (iter.hasNext()) {
      ClientsInfoListener item = (ClientsInfoListener)iter.next();
      item.receivedTime(jid, ct);
    }
  }

  public void addClientsInfoListener(ClientsInfoListener client)
  {
    clientsInfoListeners.addElement(client);
  }

  public void receivedError(JabberError error)
  {
    dispatchingMessage = true;

    String id = error.getId();
    if (id != null)
    {
      ErrorListener listener = (ErrorListener) onetimeerrorlisteners.get(id);
      if (listener != null)
      {
        onetimeerrorlisteners.remove(id);
        listener.receivedError(error);
        docontinue();
        return;
      }
    }
//    else
//    {
      fireError(error);
      disconnect();
//        presence.setType(Presence.UNAVAILABLE);
//        presence.setTo(null);
//        presence.setStatus("Disconnected following an error");
////        presence.setShow(show);
//        presence.setPriority(999);
//        //fire presence update
//        Iterator iter = serviceSentPresenceListeners.iterator();
//        while (iter.hasNext())
//        {
//          ((PresenceUpdateListener) iter.next()).receivedUpdateEvent(presence);
//        }

//    }

//    Message msg = new Message();
//    msg.setType(Message.ERROR);
//    msg.setSubject("Error: " + error.getCode() + ", " + error.getMsg());
//    msg.setBody("Error: " + error.getCode() + ", in packet " + error.getPacketid() + ", " + error.getMsg());
//    Iterator iter = messageslisteners.iterator();
//    while (iter.hasNext())
//    {
////      log.fine("calling list");
//      ((MessageListener) iter.next()).received(msg);
//    }
//    messageslisteners.addAll(newmessageslisteners);
//    newmessageslisteners.clear();
    dispatchingMessage = false;


  }

  public void receivedStanza(Stanza stanza)
{
  log.finest("stanza received: " + stanza);
  if (stanza instanceof ClientTimeStanza)
  {
    setClientTime(stanza.getFrom(),((ClientTimeStanza)stanza).getClientTime());
  }
  else if (stanza instanceof ClientVersionStanza)
  {
    setClientVersion(stanza.getFrom(),((ClientVersionStanza)stanza).getVersion());
  }
  else if (stanza instanceof GetTimeStanza)
  {
    sendTimeInfo(stanza.getId(), stanza.getFrom());
  }
  else if (stanza instanceof GetVersionStanza)
  {
    sendVersionInfo(stanza.getId(), stanza.getFrom());
  }
  else if (stanza instanceof JabberError)
  {
    receivedError((JabberError)stanza);
  }
  else if (stanza instanceof Message)
  {
    receivedMessage((Message)stanza);
  }
  else if (stanza instanceof Presence)
  {
    receivedPresence((Presence)stanza);
  }
  else if (stanza instanceof RosterStanza)
  {
    RosterStanza rs = (RosterStanza)stanza;
    if (rs.isSet())
    {
      Vector v = rs.getItems();
      if (v == null)
      {
        v = new Vector();
        RosterItem item = rs.getItem();
        if (item != null)
        {
          v.add(item);
        }
      }
      setRosterItems(v);
    }
    else
    {
      updateRosterItem(rs.getItem());
    }
  }
  else if (stanza instanceof LastActivityStanza)
  {
    receivedLastActivity(((LastActivityStanza)stanza));
  }
  else if (stanza instanceof GetLastActivityStanza)
  {
    sendLastActivity((GetLastActivityStanza)stanza);
  }
  else if (stanza instanceof IQGetDiscoItemsStanza)
  {
    receivedDiscoItems((IQGetDiscoItemsStanza)stanza);
  }
  else if (stanza instanceof IQGetDiscoInfoStanza)
  {
    receivedDiscoInfo((IQGetDiscoInfoStanza)stanza);
  }
  else if (stanza instanceof IQSetOOBStanza)
  {
    receivedOOB((IQSetOOBStanza)stanza);
  }
  else if (stanza instanceof IQResultStanza)
  {
    receivedIQResult((IQResultStanza)stanza);
  }
  else if (stanza instanceof RegisterStanza)
  {
    registerlistener.receivedStanza(stanza);
  }

}

  private void receivedIQResult(IQResultStanza stanza)
  {
    String key = stanza.getId();
    if (key != null)
    {
      ResultListener listener = (ResultListener) resultlisteners.get(key);
      if (listener != null)
      {
        listener.notifyResult();
      }
    }

  //          String key = attributes.getValue("id");
//          if (key != null)
//          {
//            ResultListener listener = (ResultListener) resultlisteners.get(key);
//            if (listener != null)
//            {
//              listener.notifyResult();
//
//              setStanza(null);
//              parent.release();
//            }
//          }
  }

  /**
   * receivedOOB
   *
   * @param stanza IQSetOOBStanza
   */
  private void receivedOOB(IQSetOOBStanza stanza)
  {
    if (ooblistener != null) ooblistener.receivedOOB(this, stanza);
  }

  private void receivedDiscoInfo(IQGetDiscoInfoStanza stanza)
  {
    String to = stanza.getFrom();
    String id = stanza.getId();

    try
    {
      sender.write("<iq type='result' id='");
      sender.write(id);
      sender.write("' to='");
      sender.write(to);
      sender.write("'><query xmlns='http://jabber.org/protocol/disco#items'>");
      sender.write("<identity ");
      sender.write("category = 'user' ");
      sender.write("type = 'client' ");
      if (clientname == null) clientname = "LH Java Jabber Client";
      sender.write("name = '" + clientname + "' ");
      sender.write("/>");
      sender.write("<feature var='jabber:iq:time' />");
      sender.write("<feature var='jabber:iq:version' />");
      sender.write("<feature var='jabber:iq:last' />");
      sender.write("<feature var='lh:jabber:buzz' />");
//      sender.write("<feature var='lh:jabber:whiteboard' />");
      sender.write("<feature var='jabber:iq:oob' />");
      sender.write("</query>");
      sender.write("</iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  private void receivedDiscoItems(IQGetDiscoItemsStanza stanza)
  {
    String to = stanza.getFrom();
    String id = stanza.getId();

    try
    {
      sender.write("<iq type='result' id='");
      sender.write(id);
      sender.write("' to='");
      sender.write(to);
      sender.write("'><query xmlns='http://jabber.org/protocol/disco#items' /></iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void requestLastActivity(JabberId jabberId)
  {
    String to;
    if (jabberId == null)
    {
      to = server;
    }
    else
    {
      to = jabberId.getFullJID();
    }
    String id = "last_" + counter++;

    try
    {
      sender.write("<iq type='get' id='");
      sender.write(id);
      sender.write("' to='");
      sender.write(to);
      sender.write("'><query xmlns='jabber:iq:last' /></iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void receivedLastActivity(LastActivityStanza lastactivity)
  {
    Message msg = new Message(lastactivity);
    msg.setType(Message.NORMAL);
    msg.setSubject("Last activity of: " + msg.getFrom());
//    Date lad = new Date(lastactivity.getSeconds())* 1000);
    int temp = lastactivity.getSeconds();
    int seconds = temp;
    seconds -= (temp/60*60);
    temp -= seconds;
    temp /= 60;
    int minutes = temp - (temp/60*60);
    temp -= minutes;
    temp /= 60;
    int hours = temp - (temp/60*60);
    int days = (temp - hours) / 24;
    String time = "" + (days > 0 ? days +" days " : "") + (hours > 0 ? hours +" hours " : "") + (minutes > 0 ? minutes +" minutes " : "") + seconds + " seconds'";
    msg.setBody("Away for " + time);
    receivedMessage(msg);
  }

  void sendLastActivity(GetLastActivityStanza stanza)
  {
    try
    {
      log.info("Last Activity sent to: " + stanza.getFrom());
      sender.write("<iq type='result' id='");
      sender.write(stanza.getId());
      sender.write("' to='");
      sender.write(stanza.getFrom());
      sender.write("'><query seconds='");
      sender.write(sender.getLastActivity());
      sender.write("' xmlns='jabber:iq:last' /></iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void addOneTimeErrorListener(String key, ErrorListener listener)
  {
    onetimeerrorlisteners.put(key, listener);
  }

  /**
   * receivedRegister
   *
   * @param stanza Stanza
   */
  private void receivedRegister(Stanza stanza)
  {
    registerlistener.receivedStanza(stanza);
    docontinue();
  }

  public void setRegisterListener(RegisterListener registerlistener)
  {
    this.registerlistener = registerlistener;
  }

  public synchronized void sendRegister(String host)
  {
    try
    {
      String id = "register_" + counter++;
      addOneTimeErrorListener(id, registerlistener);
      sender.write("<iq type='get' id='");
      sender.write(id);
      sender.write("' ");
      sender.write("><query ");
      sender.write(" xmlns='jabber:iq:register' /></iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
    try
    {
      wait();
    }
    catch (InterruptedException ex1)
    {
      ex1.printStackTrace();
    }
  }

  /**
   * sendRegistrationInfo
   *
   * @param fields Vector
   */
  public synchronized void sendRegistrationInfo(Vector fields)
  {
    try
    {
      String id = "register_" + counter++;

      addOneTimeErrorListener(id, registerlistener);
      ResultListener rl = new ResultListener()
      {
        public void notifyResult()
        {
          docontinue();
        }
      };
      /*resulthandler.*/addResultListener(id, rl);

//      String id = "register_" + counter++;
      sender.write("<iq type='set' id='");
      sender.write(id);
      sender.write("' ");
      sender.write("><query ");
      sender.write(" xmlns='jabber:iq:register'>");
      Iterator iter = fields.iterator();
      while (iter.hasNext()) {
        Field field = (Field)iter.next();
        sender.write("<" + field.name + ">" + field.value + "</" + field.name + ">");
      }
      sender.write("</query>");
      sender.write("</iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
    try
    {
      wait();
    }
    catch (InterruptedException ex1)
    {
      ex1.printStackTrace();
    }
  }

  /**
   * unregister
   */
  public void unregister()
  {
    try
    {
      String id = "unregister_" + counter++;
//      addOneTimeErrorListener(id, registerlistener);
      sender.write("<iq type='set' id='");
      sender.write(id);
      sender.write("' ");
      sender.write("><query ");
      sender.write(" xmlns='jabber:iq:register' ><remove/></query></iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
//    try
//    {
//      wait();
//    }
//    catch (InterruptedException ex1)
//    {
//      ex1.printStackTrace();
//    }
  }

  public void sendBuzz(int type, String to)
  {
    try
    {
      String id = "msg_" + counter++;
      sender.write("<message ");
      switch (type)
      {
        case Message.CHAT:
          sender.write("type='chat' ");
          break;
        case Message.GROUPCHAT:
          sender.write("type='groupchat' ");
          break;
        case Message.HEADLINE:
          sender.write("type='headline' ");
          break;
        case Message.ERROR: // no handled, treat as single
          sender.write("type='error' ");
          break;
        case Message.NORMAL:
        default:
          //add nothing
      }
      if (to != null)
      {
        sender.write("to='");
        sender.write(to);
        sender.write("' ");
      }
      sender.write(" id='");
      sender.write(id);
      sender.write("'>");

      sender.write("<thread>");
      sender.write("thread_");
      sender.write(jid.toString());
      sender.write("_");
      sender.write(to);
      sender.write("</thread>");

      sender.write("<x xmlns='lh:jabber:buzz' />");

      sender.write("</message>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void sendExtendedMessage(int type, String to, String subject, String message, String ext)
  {
    try
    {
      String id = "msg_" + counter++;
      sender.write("<message ");
      switch (type)
      {
        case Message.CHAT:
          sender.write("type='chat' ");
          break;
        case Message.GROUPCHAT:
          sender.write("type='groupchat' ");
          break;
        case Message.HEADLINE:
          sender.write("type='headline' ");
          break;
        case Message.ERROR: // no handled, treat as single
          sender.write("type='error' ");
          break;
        case Message.NORMAL:
        default:
          //add nothing
      }
      if (to != null)
      {
        sender.write("to='");
        sender.write(to);
        sender.write("' ");
      }
      sender.write(" id='");
      sender.write(id);
      sender.write("'>");

      sender.write("<thread>");
      sender.write("thread_");
      sender.write(jid.toString());
      sender.write("_");
      sender.write(to);
      sender.write("</thread>");

      if (subject != null)
      {
        sender.write("<subject>");
        sender.write(subject);
        sender.write("</subject>");
      }
      if (message != null)
      {
        sender.write("<body>");
        message = lh.util.XmlUtil.escapeXml(message);
        sender.write(message);
        sender.write("</body>");
      }

      sender.write(ext);

      sender.write("</message>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void setOOBListener(OOBListener ooblistener)
  {
    this.ooblistener = ooblistener;
  }

  public void sendExtendedIQ(int type, String to, String xmlns, String ext, String error)
  {
    try
    {
      String id = "iq_" + counter++;
      sender.write("<iq ");
      sender.write("type='");
      sender.write(IQ.toString(type));
      sender.write("' ");
      sender.write("from='");
      sender.write(jid.toString());
      sender.write("' ");

      if (to != null)
      {
        sender.write("to='");
        sender.write(to);
        sender.write("' ");
      }
      sender.write(" id='");
      sender.write(id);
      sender.write("'>");

      sender.write("<query xmlns=\"");
      sender.write(xmlns);
      sender.write("\">");
      sender.write(ext);

      sender.write("</query>");
      if (error != null)
      {
        sender.write(error);
      }
      sender.write("</iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void sendIQResult(String to, String id)
  {
    try
    {
      sender.write("<iq ");
      sender.write("type='result' ");
      sender.write("from='");
      sender.write(jid.toString());
      sender.write("' ");

      if (to != null)
      {
        sender.write("to='");
        sender.write(to);
        sender.write("' ");
      }
      sender.write(" id='");
      sender.write(id);
      sender.write("' />");

      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * sendStanza
   *
   * @param stanza String
   */
  public void sendStanza(String stanza)
  {
    try
    {
      sender.write(stanza);
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  private void sendBye()
  {
    sender.write("/<stream:stream>");
  }

  private void addResultListener(String key, ResultListener listener)
  {
    resultlisteners.put(key, listener);
  }

  public void addErrorListener(ErrorListener l)
  {
    errorlisteners.add(l);
  }

  private void fireError(JabberError error)
  {
    Iterator iter = ((ArrayList)errorlisteners).iterator();
    while (iter.hasNext())
    {
      ((ErrorListener)iter.next()).receivedError(error);
    }
  }

  public void addContact(final String sjid, String nick, String group)
  {
    if (nick.equals("")) nick = sjid;
    if ((group != null) && (group.equals(""))) group = null;


    try
    {
      // set roster
      String id = "iq_" + counter++;

      ResultListener l = new ResultListener()
      {
        public void notifyResult()
        {
          sendPresence(Presence.SUBSCRIBE, sjid, null, null, -1);
        }

      };
      addResultListener(id, l);
      sender.write("<iq ");
      sender.write("type='set' id='");
      sender.write(id);
      sender.write("'>");

      sender.write("<query xmlns='jabber:iq:roster'>");
      sender.write("<item ");
      sender.write(" jid='");
      sender.write(sjid);
      sender.write("'");
      sender.write(" name='");
      sender.write(nick);
      sender.write("'>");
      if (group != null)
      {
        sender.write("<group>");
        sender.write(group);
        sender.write("</group>");
      }
      sender.write("</item>");

      sender.write("</query>");
      sender.write("</iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  public void removeRosterItem(RosterItem item)
  {

    try
    {
      // set roster
      String id = "remove_" + counter++;

//      ResultListener l = new ResultListener()
//      {
//        public void notifyResult()
//        {
//        }
//
//      };
//      addResultListener(id, l);
      sender.write("<iq ");
      sender.write("type='set' id='");
      sender.write(id);
      sender.write("'>");

      sender.write("<query xmlns='jabber:iq:roster'>");
      sender.write("<item ");
      sender.write(" jid='");
      sender.write(item.getJid().getJID());
      sender.write("'");
      sender.write(" subscription='remove'");
      sender.write(" />");

      sender.write("</query>");
      sender.write("</iq>");
      sender.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
}

