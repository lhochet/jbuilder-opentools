package lh.jabber.whiteboard;

import java.awt.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:18 $
 * @created 30/10/03
 */

public abstract class Shape
{
  public Shape()
  {
  }

  public abstract void start(Point p);
  public abstract void intermediate(Point p);
  public abstract void end(Point p);
  public abstract void paintComponent(Graphics g);
  public abstract void send(StringBuffer buf);

}
