package lh.jabber.whiteboard;

import java.awt.*;


/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:17 $
 * @created 30/10/03
 */

public class Line extends Shape
{
  private Point start = null;
  private Point end = null;
  public Line()
  {
  }
  public void end(Point p)
  {
    end = p;
  }
  public void intermediate(Point p)
  {
    end = p;
  }
  public void start(Point p)
  {
    start = p;
  }
  public void paintComponent(Graphics g)
  {
    if (start != null && end != null)
    {
      g.drawLine(start.x, start.y, end.x, end.y);
    }
  }
  public void send(StringBuffer buf)
  {
    buf.append("<line start_x=\"");
    buf.append(start.x);
    buf.append("\" start_y=\"");
    buf.append(start.y);
    buf.append("\" end_x=\"");
    buf.append(end.x);
    buf.append("\" end_y=\"");
    buf.append(end.y);
    buf.append("\" />");
  }

}
