package lh.jabber.whiteboard;

import java.awt.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:16 $
 * @created 7/10/03
 */

public class Clear extends Shape
{
  public Clear()
  {
  }
  public void end(Point p)
  {
    // do nothing, just ignore
  }
  public void send(StringBuffer buf)
  {
    // do nothing, just ignore
  }
  public void paintComponent(Graphics g)
  {
    // do nothing, just ignore
  }
  public void start(Point p)
  {
    // do nothing, just ignore
  }
  public void intermediate(Point p)
  {
    // do nothing, just ignore
  }

}
