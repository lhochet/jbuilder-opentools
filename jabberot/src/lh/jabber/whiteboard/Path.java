package lh.jabber.whiteboard;

import java.awt.*;
import java.util.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:17 $
 * @created 30/10/03
 */

public class Path extends Shape
{
  private java.util.List path = new java.util.ArrayList();

  public Path()
  {
  }
  public void end(Point p)
  {
    path.add(p);
  }
  public void intermediate(Point p)
  {
    path.add(p);
  }
  public void start(Point p)
  {
    path.add(p);
  }
  public void paintComponent(Graphics g)
  {
    Point s = null;
    Point e = null;
    Iterator iter = path.iterator();
    while (iter.hasNext())
    {
      Point p = (Point) iter.next();
      if (s == null)
      {
        s = p;
      }
      else
      {
        e = p;
        g.drawLine(s.x, s.y, e.x, e.y);
        s = p;
      }
    }
  }
  public void send(StringBuffer buf)
  {
    Point s = null;
    Point e = null;
    buf.append("<path p=\"");
    Iterator iter = path.iterator();
    while (iter.hasNext())
    {
      Point p = (Point) iter.next();
      buf.append(p.x);
      buf.append(' ');
      buf.append(p.y);
      buf.append(' ');
    }
    buf.append("\" />");
  }

}
