package lh.jabber;

import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:27 $
 * @created 15/11/03
 */

public interface OOBListener
{
  public void receivedOOB(JabberService service, IQSetOOBStanza stanza);
}
