package lh.jabber;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 13/01/02
 */

public interface ResultListener
{
  public void notifyResult();
}