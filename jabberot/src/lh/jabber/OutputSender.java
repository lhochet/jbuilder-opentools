package lh.jabber;

import java.io.*;
import java.util.logging.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:28 $
 * @created 13/07/03
 */

public class OutputSender
{
  private static Logger log = Logger.getLogger(OutputSender.class.getName());
  static
  {
    log.setLevel(Level.ALL);
    log.setUseParentHandlers(false);
  }
  public static Logger getLogger()
  {
    return log;
  }

  private Writer writer = null;
  private StringBuffer buf = new StringBuffer();

  private int lastactivity = (int)(System.currentTimeMillis() / 1000);

  public OutputSender(Writer writer)
  {
    this.writer = writer;
  }

  public void write(String str)
  {
     buf.append(str);
  }

  public void write(int i)
  {
     buf.append(i);
  }

  public void flush() throws IOException
  {
    String str = buf.toString();
    writer.write(str);
    writer.flush();
    if (!str.endsWith("\n")) str += "\n";
    log.finer("SENT: " + str);
    buf.delete(0, buf.length());
    lastactivity = (int)(System.currentTimeMillis() / 1000);
  }

  public int getLastActivity()
  {
    return (int)(System.currentTimeMillis() / 1000) - lastactivity;
  }

}