package lh.jabber;

import java.util.logging.*;
import org.xml.sax.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:21 $
 * @created 13/07/03
 */

public class InputLogger // extends DefaultHandler
{
  private static Logger log = Logger.getLogger(InputLogger.class.getName());
  static
  {
    log.setLevel(Level.ALL);
    log.setUseParentHandlers(false);
  }
  public static Logger getLogger()
  {
    return log;
  }


  StringBuffer buf = new StringBuffer();


  String lineEnd = System.getProperty("line.separator");
  int indentMult = 2;
  char indentChar = ' ';

  int indent = 0;
  boolean prevIsText = false;
  boolean prevIsStart = false;
  boolean prevIsEnd = false;
  boolean prevIsStartDoc = false;

  public InputLogger()
  {
  }


  public void characters(char[] ch, int start, int length) throws SAXException
  {
    if (prevIsStart)
    {
      write(">");
    }
    else if (prevIsEnd)
    {
      nl();
    }
    writeQuote(ch, start, length);
    prevIsText = true;
    prevIsStart = false;
    prevIsEnd = false;
  }
  public void endDocument() throws SAXException
  {
    if (prevIsStart)
    {
      write(" />");
      prevIsStart = false;
    }
    nl();
    String p = buf.toString();
    if (!p.endsWith(lineEnd)) p += lineEnd;
    log.fine(p);
    System.out.println(p);
    buf.delete(0, buf.length());
  }
  public void endElement(String uri, String localName, String qName) throws SAXException
  {
    if (prevIsStart)
    {
      write(" />");
      indent--;
    }
    else
    {
      if (prevIsEnd)
      {
        nl();
      }
      indent--;
      if (!prevIsText) writeIndent();
      write("</" + qName + ">");
//      if (isFlush)
//      {
//        nl();
//        String p = buf.toString();
//        if (!p.endsWith(lineEnd))
//          p += lineEnd;
//        System.out.println("RECV: " + p);
//        buf.delete(0, buf.length());
//        isFlush = false;
//      }
    }
    prevIsText = false;
    prevIsStart = false;
    prevIsEnd = true;
  }
  public void processingInstruction(String target, String data) throws SAXException
  {
    if (prevIsStart)
    {
      write(" />");
      indent--;
    }
    nl();
    write("<?" + target + " " + data + "?>");
  }
  public void startDocument() throws SAXException
  {
    write("<?xml version='1.0' encoding='UTF-8'?>");
    if (true)
    {
      String p = buf.toString();
      if (!p.endsWith(lineEnd)) p += lineEnd;
      System.out.println("RECV: " + p);
      buf.delete(0, buf.length());
    }


    prevIsStartDoc = true;
  }
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
  {
    if (prevIsStart)
    {
      write(">");
      nl();
//      if (isFlush)
//      {
//        String p = buf.toString();
//        if (!p.endsWith(lineEnd))
//          p += lineEnd;
//        System.out.println("RECV: " + p);
//        buf.delete(0, buf.length());
//        isFlush = false;
//      }
    }
    else
    {
//      if (prevIsOutput)
//      {
//        prevIsOutput = false;
//      }
//      else
//      {
//        nl();
//      }
    }

    writeIndent();
    indent++;
    write("<");
    write(qName);
    if (attributes != null)
    {
      for (int i = 0; i < attributes.getLength(); i++)
      {
        write(" ");
        writeQuote(attributes.getQName(i));
        write("=\"");
        writeQuote(attributes.getValue(i));
        write("\"");
      }
    }
//    if (localName.equals("stream")) prevIsStartStream = true;
    prevIsStart = true;
    prevIsText = false;
    prevIsEnd = false;
  }

  public void ignorableWhitespace(char[] ch, int start, int length) throws org.xml.sax.SAXException
  {
    if (prevIsStart)
    {
      write(">");
      nl();
      writeIndent();
    }
    else if (prevIsEnd)
    {
      nl();
      writeIndent();
    }
    else if (prevIsStartDoc)
    {
      nl();
      prevIsStartDoc = false;
    }

    write((new String(ch, start, length)));
    prevIsText = false;
    prevIsStart = false;
    prevIsEnd = false;
  }

  protected void write(String s) throws SAXException
  {
    buf.append(s);
  }

  protected void nl() throws SAXException
  {
    try
    {
      buf.append(lineEnd);
    }
    catch (Exception ex)
    {
      throw new SAXException("I/O Error", ex);
    }
  }

  protected void writeQuote(String s) throws SAXException
  {
    s = s.trim();
    for (int i = 0; i < s.length(); i++) writeQuote(s.charAt(i));
  }

  protected void writeQuote(char[] buf, int off, int len) throws SAXException
  {
    int last = off + len - 1;
    for (int i = off; i <= last; i++) writeQuote(buf[i]);
  }

  protected void writeQuote(char c) throws SAXException
  {
//    try
//    {
//    System.err.println("c = '" + c + "' (" + (int)c + ")");
      buf.append(c);
//      if (c == '&')
//        buf.append("&amp;");
//      else if (c == '"')
//        buf.append("&quot;");
//      else if (c == '<')
//        buf.append("&lt;");
//      else if (c == '>')
//        buf.append("&gt;");
//      else if (c == '\'')
//        buf.append("&apos;");
//      else if (c == '\n')
//        ;
//      else if (c == '\r')
//        ;
//      else if (c < 127)
//        buf.append(c);
//      else
//        buf.append("&#" + Integer.toString(c) + ";");
//    }
//    catch (IOException ex)
//    {
//      throw new SAXException("I/O Error", ex);
//    }

  }

  protected void writeIndent() throws SAXException
  {
    write((new String(new char[indent * indentMult])).replace((char)0, indentChar));
  }

  public void flush()
  {
//  isFlush = true;
    if (prevIsStart)
    {
      try
      {
        write(">");
        nl();
        prevIsStart = false;
      }
      catch (SAXException ex)
      {
        ex.printStackTrace();
      }
    }
    String p = buf.toString();
    if (p.equals("")) return;
    if (!p.endsWith(lineEnd))
      p += lineEnd;
    log.fine("RECV: " + p);
//    System.out.println("RECV: " + p);
    buf.delete(0, buf.length());
  }
}

