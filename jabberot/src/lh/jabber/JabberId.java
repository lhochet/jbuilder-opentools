package lh.jabber;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:24 $
 * @created 10/01/03
 */

public class JabberId
{
  private String user;
  private String domain;
  private String resource;

  public JabberId(String jid)
  {
    if (jid == null) throw new IllegalArgumentException("Jabber ID is null in JabberId(jid)");
    int serversep = jid.indexOf('@');
    int resourcesep = jid.indexOf('/', serversep);
    //user
    if (serversep < 0)
    {
      user = jid;
    }
    else if (serversep == 0)
    {
      user = null;
    }
    else
    {
      user = jid.substring(0, serversep);
      if (resourcesep < 0)
      {
        domain = jid.substring(serversep + 1);
      }
      else if (resourcesep == 0)
      {
        domain = jid.substring(serversep + 1, resourcesep);
      }
      else
      {
        domain = jid.substring(serversep + 1, resourcesep);
        if (resourcesep < jid.length())
        {
          resource = jid.substring(resourcesep + 1);
        }
      }
    }
  }
  public JabberId(String user, String domain)
  {
    this(user, domain, null);
  }
  public JabberId(String user, String domain, String resource)
  {
    this.user = user;
    this.domain = domain;
    this.resource = resource;
  }

  public void setUser(String user)
  {
    this.user = user;
  }
  public String getUser()
  {
    return user;
  }

  public void setDomain(String domain)
  {
    this.domain = domain;
  }
  public String getDomain()
  {
    return domain;
  }

  public void setResource(String resource)
  {
    this.resource = resource;
  }
  public String getResource()
  {
    return resource;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    boolean hasUser = user != null;
    boolean hasDomain = domain != null;
    boolean hasResource = resource != null;

    if (hasUser) buf.append(user);
    if (hasUser && hasDomain) buf.append('@');
    if (hasDomain) buf.append(domain);
    if (hasDomain && hasResource)
    {
      buf.append('/');
      buf.append(resource);
    }

    return buf.toString();
  }

  public String getJID()
  {
    StringBuffer buf = new StringBuffer();
    boolean hasUser = user != null;
    boolean hasDomain = domain != null;

    if (hasUser) buf.append(user);
    if (hasUser && hasDomain) buf.append('@');
    if (hasDomain) buf.append(domain);

    return buf.toString();
  }

  public String getFullJID()
  {
    return toString();
  }

  public boolean equal(JabberId jid)
  {
    return equal(user, jid.user) && equal(domain, jid.domain);
  }

  public boolean equal(String jid)
  {
    if (jid == null) return false;
    if (jid.equals("")) return false;
    return equal(new JabberId(jid));
  }

  public boolean equalFull(JabberId jid)
  {
    return equal(user, jid.user) && equal(domain, jid.domain) && equal(resource, jid.resource);
  }

  public boolean equalFull(String jid)
  {
    if (jid == null) return false;
    if (jid.equals("")) return false;
    return equalFull(new JabberId(jid));
  }

  private boolean equal(String me, String other)
  {
    if (me == null && other == null) return true;
    if (me == null || other == null) return false; // already handle when both are null so they must be different
    return me.equalsIgnoreCase(other);
  }

}

