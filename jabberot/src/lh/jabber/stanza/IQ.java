package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:11 $
 * @created 18/11/03
 */

public class IQ extends Stanza
{
  // types
  public static final int SET = 0;
  public static final int GET = 1;
  public static final int RESULT = 2;
  public static final int ERROR = 3;

  public IQ()
  {
  }

  public IQ(Stanza p0)
  {
    super(p0);
  }

  static public String toString(int type)
  {
    switch (type)
    {
        case SET:
          return "set";
        case GET:
          return "get";
        case RESULT:
          return "result";
        case ERROR:
          return "error";
    }
    return "error";
  }
}
