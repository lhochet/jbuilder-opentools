package lh.jabber.stanza;

import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:09 $
 * @created 6/08/03
 */

public class ClientVersionStanza extends Stanza
{
  ClientVersion version = null;

  public ClientVersionStanza()
  {
  }

  public ClientVersionStanza(Stanza copy)
  {
    super(copy);
  }

  public void setClientVersion(ClientVersion version)
  {
    this.version = version;
  }
  public ClientVersion getVersion()
  {
    return version;
  }
}