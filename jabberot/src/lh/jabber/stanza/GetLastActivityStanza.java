package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:09 $
 * @created 7/08/03
 */

public class GetLastActivityStanza extends Stanza
{

  public GetLastActivityStanza()
  {
  }

  public GetLastActivityStanza(Stanza copy)
  {
    super(copy);
  }
}