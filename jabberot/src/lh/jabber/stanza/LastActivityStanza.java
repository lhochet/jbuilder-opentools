package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:13 $
 * @created 7/08/03
 */

public class LastActivityStanza extends Stanza
{

  private int seconds;

  public LastActivityStanza()
  {
  }

  public LastActivityStanza(Stanza copy)
  {
    super(copy);
  }
  public int getSeconds()
  {
    return seconds;
  }
  public void setSeconds(int seconds)
  {
    this.seconds = seconds;
  }
}