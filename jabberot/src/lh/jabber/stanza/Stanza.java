package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:16 $
 * @created 6/08/03
 */

public class Stanza
{
  private String id;
  private String from;
  private String to;
  private String type;

  public Stanza()
  {
  }

  public Stanza(Stanza copy)
  {
    this.id = copy.id;
    this.from = copy.from;
    this.to = copy.to;
    this.type = copy.type;
  }

  public void setId(String id)
  {
    this.id = id;
  }
  public void setRawType(String type)
  {
    this.type = type;
  }
  public void setFrom(String from)
  {
    this.from = from;
  }
  public void setTo(String to)
  {
    this.to = to;
  }
  public String getFrom()
  {
    return from;
  }
  public String getId()
  {
    return id;
  }
  public String getTo()
  {
    return to;
  }
  public String getRawType()
  {
    return type;
  }

  public String toString()
  {
    return ""+ getClass() + " from: " + from + ", to: " + to + ", id:" + id + ", type:" + type;
  }

}
