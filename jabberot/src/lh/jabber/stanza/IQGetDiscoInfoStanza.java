package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:11 $
 * @created 24/08/03
 */

public class IQGetDiscoInfoStanza extends Stanza
{

  public IQGetDiscoInfoStanza()
  {
  }

  public IQGetDiscoInfoStanza(Stanza copy)
  {
    super(copy);
  }
  private String node = null;

  public String getNode()
  {
    return node;
  }
  public void setNode(String node)
  {
    this.node = node;
  }
}