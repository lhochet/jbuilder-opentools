package lh.jabber.stanza;

import java.util.*;

import lh.jabber.whiteboard.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:16 $
 * @created 30/10/03
 */

public class WhiteBoardStanza extends Message
{
  private List shapes = new ArrayList();
  public WhiteBoardStanza()
  {
  }
  public WhiteBoardStanza(Stanza copy)
  {
    super(copy);
  }
  public void addShape(Shape shape)
  {
    shapes.add(shape);
  }

  /**
   * getShapes
   *
   * @return List
   */
  public List getShapes()
  {
    return shapes;
  }

}
