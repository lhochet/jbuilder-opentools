package lh.jabber.stanza;

import java.util.*;
import java.text.*;



/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:14 $
 * @created 6/08/03
 */

public class Message extends Stanza
{
  public static final int NORMAL = 0;
  public static final int CHAT = 1;
  public static final int GROUPCHAT = 2;
  public static final int HEADLINE = 3;
  public static final int ERROR = 4;

  private int type = NORMAL;
  private String thread;
  private String subject;
  private String body;
  private String time;
  private Date dtime;

  public Message()
  {
    setTime(new Date());
  }
  public Message(Stanza copy)
  {
    super(copy);
    if (copy instanceof Message)
    {
      Message msg = (Message)copy;
      setThread(msg.getThread());
      setSubject(msg.getSubject());
      setType(msg.getType());
      setBody(msg.getBody());
      setTime(msg.getTime());
    }
  }
  public String getThread()
  {
    return thread;
  }
  public void setThread(String thread)
  {
    this.thread = thread;
  }
  public String getSubject()
  {
    return subject;
  }
  public void setSubject(String subject)
  {
    this.subject = subject;
  }
  public String getBody()
  {
    return body;
  }
  public void setBody(String body)
  {
    this.body = body;
  }
  public Date getTime()
  {
    return dtime;
  }
  public String getRawTime()
  {
    return time;
  }
  public void setTime(String stime)
  {
    this.time = stime;
    if ((stime == null) || (stime.equals("")))
    {
      dtime = null;
    }
    else
    {
      try
      {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        dtime = sdf.parse(stime);
      }
      catch (ParseException ex)
      {
      }
    }
  }
  public void setTime(Date dtime)
  {
    this.dtime = dtime;
    if (dtime == null)
    {
      time = null;
    }
    else
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
      sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
      time = sdf.format(dtime);
    }
  }
  public int getType()
  {
    return type;
  }
  public void setType(int type)
  {
    this.type = type;
  }

  public static int toType(String rawtype)
  {
    int ret = NORMAL;
//    public static final int NORMAL = 0;
//    public static final int CHAT = 1;
//    public static final int GROUPCHAT = 2;
//    public static final int HEADLINE = 3;
//    public static final int ERROR = 4;
    if (rawtype == null)
    {
      // ret = NORMAL;
    }
    else if (rawtype.equals("chat"))
    {
      ret = CHAT;
    }
    else if (rawtype.equals("groupchat"))
    {
      ret = GROUPCHAT;
    }
//    else if (rawtype.equals())
//    {
//      ret = ;
//    }

    return ret;
  }
}

