package lh.jabber.stanza;

import java.util.logging.*;



/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:14 $
 * @created 6/08/03
 */

public class Presence extends Stanza
{
  private static Logger log = Logger.getLogger(Presence.class.getName());

  // types
  public static final int AVAILABLE = 0;
  public static final int UNAVAILABLE = 1;
  public static final int SUBSCRIBE = 2;
  public static final int SUBSCRIBED = 3;
  public static final int UNSUBSCRIBE = 4;
  public static final int UNSUBSCRIBED = 5;
  public static final int PROBE = 6;
  public static final int ERROR = 7;
  public static final int INVISIBLE = 8;
  public static final int VISIBLE = 9;

  // show
//  public static final int AVAILABLE = 0;
//  public static final int UNAVAILABLE = 1;
  public static final int CHAT = 102;
  public static final int AWAY = 103;
  public static final int XA = 104;
  public static final int DND = 105;
//  public static final int INVISIBLE = 8;

  private int type = UNAVAILABLE;
  private String showStr;
  private int show = UNAVAILABLE;
  private String statusStr;
  private int priority = -1;
  private int errorcode = -1;
  private String errormessage;

  public Presence()
  {
  }
  public Presence(Stanza copy)
  {
    super(copy);
  }
  public int getType()
  {
    return type;
  }
  public void setType(int type)
  {
    this.type = type;
  }
  public int getShowInt()
  {
    return show;
  }
  public String getShow()
  {
    return showStr;
  }
  public void setShow(String showStr)
  {
    this.showStr = showStr;
    show = toInt(showStr);
  }

  /**
   * toInt
   *
   * @param showStr String
   * @return int
   */
  private int toInt(String showStr)
  {
//    log.fine("toInt("+showStr+")");
//    log.fine("getType() = "+ getType());
    int ret = AVAILABLE;
    if (showStr == null)
    {
      ret = getType();
    }
    else if (showStr.equals(""))
    {
      ret = getType();
    }
    else if (showStr.equals("chat"))
    {
      ret = CHAT;
    }
    else if (showStr.equals("away"))
    {
      ret = AWAY;
    }
    else if (showStr.equals("xa"))
    {
      ret = XA;
    }
    else if (showStr.equals("dnd"))
    {
      ret = DND;
    }

//    log.fine("ret = "+ret);
    return ret;
  }

  public String getStatus()
  {
    return statusStr;
  }
  public void setStatus(String status)
  {
    this.statusStr = status;
  }
  public int getPriority()
  {
    return priority;
  }
  public void setPriority(int priority)
  {
    this.priority = priority;
  }
  public int getErrorcode()
  {
    return errorcode;
  }
  public void setErrorcode(int errorcode)
  {
    this.errorcode = errorcode;
  }
  public String getErrormessage()
  {
    return errormessage;
  }
  public void setErrormessage(String errormessage)
  {
    this.errormessage = errormessage;
  }

}

