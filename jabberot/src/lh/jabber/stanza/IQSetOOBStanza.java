package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:13 $
 * @created 15/11/03
 */

public class IQSetOOBStanza extends Stanza
{
  private String url = null;
  private String desc = null;

  public IQSetOOBStanza()
  {
  }

  public IQSetOOBStanza(Stanza copy)
  {
    super(copy);
  }
  public String getUrl()
  {
    return url;
  }
  public void setUrl(String url)
  {
    this.url = url;
  }
  public String getDesc()
  {
    return desc;
  }
  public void setDesc(String desc)
  {
    this.desc = desc;
  }
}
