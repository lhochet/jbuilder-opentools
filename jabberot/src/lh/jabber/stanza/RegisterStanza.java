package lh.jabber.stanza;

import java.util.Vector;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:15 $
 * @created 20/08/03
 */

public class RegisterStanza extends Stanza
{
  private String instructions = null;

  private Vector fields = null;
  public RegisterStanza()
  {
  }

  public RegisterStanza(Stanza copy)
  {
    super(copy);
  }

  public void setInstructions(String instructions)
  {
    this.instructions = instructions;
  }
  public String getInstructions()
  {
    return instructions;
  }

  /**
   * setFields
   *
   * @param fields Vector
   */
  public void setFields(Vector fields)
  {
    this.fields = fields;
  }
  public Vector getFields()
  {
    return fields;
  }
}