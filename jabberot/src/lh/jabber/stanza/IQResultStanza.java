package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:12 $
 * @created 29/07/04
 */
public class IQResultStanza extends Stanza
{
  public IQResultStanza()
  {
    super();
  }

  public IQResultStanza(Stanza copy)
  {
    super(copy);
  }
}
