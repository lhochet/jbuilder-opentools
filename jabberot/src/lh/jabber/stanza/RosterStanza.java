package lh.jabber.stanza;

import java.util.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:15 $
 * @created 6/08/03
 */

public class RosterStanza extends Stanza
{

  private Vector items = null;
  private RosterItem item = null;
  private boolean set = true;

  public RosterStanza()
  {
  }

  public RosterStanza(Stanza copy)
  {
    super(copy);
  }

  public void setUpdate(boolean update)
  {
    this.set = !update;
  }

  public boolean isSet()
  {
    return set;
  }

  public void setItems(Vector items)
  {
    item = null;
    this.items = items;
  }

  public Vector getItems()
  {
    return items;
  }

  public void setItem(RosterItem item)
  {
    items = null;
    this.item = item;
  }

  public RosterItem getItem()
  {
    return item;
  }
}
