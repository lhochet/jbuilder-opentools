package lh.jabber.stanza;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:13 $
 * @created 6/08/03
 */

public class JabberError extends Stanza
{
  private String packetid;
  private int code;
  private String msg;
  private String xmppName;
  private String xmppType;

  public JabberError()
  {
  }
  public JabberError(Stanza copy)
  {
    super(copy);
  }
  public int getCode()
  {
    return code;
  }
  public String getMsg()
  {
    return msg;
  }
  public String getPacketid()
  {
    return packetid;
  }
  public void setCode(int code)
  {
    this.code = code;
  }
  public void setMsg(String msg)
  {
    this.msg = msg;
  }
  public void setPacketid(String packetid)
  {
    this.packetid = packetid;
  }
  public String getXmppName()
  {
    return xmppName;
  }
  public void setXmppName(String xmppName)
  {
    this.xmppName = xmppName;
  }
  public String getXmppType()
  {
    return xmppType;
  }
  public void setXmppType(String xmppType)
  {
    this.xmppType = xmppType;
  }
}