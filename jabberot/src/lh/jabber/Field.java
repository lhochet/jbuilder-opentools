package lh.jabber;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:21 $
 * @created 20/08/03
 */

public class Field
{
  public String name = null;
  public String value = null;
  public Field(String name)
  {
    this.name = name;
  }
}