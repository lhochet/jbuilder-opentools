package lh.jabber;

import javax.xml.parsers.*;
import org.xml.sax.*;
import com.sun.corba.se.internal.iiop.*;
import java.io.*;
import lh.jabber.handler.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1,10/01/03
 */

public class InputProcess extends Thread
{
  private JabberInputHandler inputhandler;
  private Reader reader;
  private JabberService service;
  public InputProcess(JabberService service, Reader reader, JabberInputHandler inputhandler)
  {
    super("JabberInputProcess");
    this.inputhandler = inputhandler;
    this.reader = reader;
    this.service = service;
  }
  public void run()
  {
    setupParser();
  }

  private void setupParser()
  {
    System.setProperty("javax.xml.parsers.SAXParserFactory", "org.apache.xerces.jaxp.SAXParserFactoryImpl");
//    String uri = argv[0];
    try
    {
      SAXParserFactory parserFactory = SAXParserFactory.newInstance();
      parserFactory.setValidating(false);
      parserFactory.setNamespaceAware(true);
      parserFactory.setFeature("http://xml.org/sax/features/namespace-prefixes",true);
      parserFactory.setFeature("http://apache.org/xml/features/continue-after-fatal-error",true);
      SAXParser parser = parserFactory.newSAXParser();
      parser.parse(new InputSource(reader), inputhandler);
    }
    catch(IOException ex)
    {
//      ex.printStackTrace();
    }
    catch(SAXException ex)
    {
      ex.printStackTrace();
    }
    catch(ParserConfigurationException ex)
    {
      ex.printStackTrace();
    }
    catch(FactoryConfigurationError ex)
    {
      ex.printStackTrace();
    }
    service.setStatus(JabberService.DISCONNECTED);
  }

}

