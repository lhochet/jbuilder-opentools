package lh.jabber;

import java.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 15/01/03
 */

public interface RosterListener
{
  public void setItems(Vector items);
  public void updateItem(RosterItem item);
}
