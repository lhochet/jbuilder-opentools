package lh.jabber;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 10/01/03
 */

public class JabberException extends Exception
{

  public JabberException()
  {
  }

  public JabberException(String message)
  {
    super(message);
  }

  public JabberException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public JabberException(Throwable cause)
  {
    super(cause);
  }
}