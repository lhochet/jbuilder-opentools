package lh.jabber;

import java.util.*;

import lh.jabber.stanza.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:33 $
 * @created 14/01/03
 */

public class RosterItem extends RosterElement
{

  private String group;

  private String subscription;
  private String ask;

  private RosterResourceElement delegate = null;
  private Vector resources = new Vector();
  private HashMap resourcesMap = new HashMap();

  public RosterItem()
  {
  }

  public String getGroup()
  {
    return group;
  }

  public void setGroup(String group)
  {
    this.group = group;
  }

  public String getSubscription()
  {
    return subscription;
  }

  public void setSubscription(String subscription)
  {
    this.subscription = subscription;
  }

  public String getAsk()
  {
    return ask;
  }

  public void setAsk(String ask)
  {
    this.ask = ask;
  }

//  public void setCurrentDelegate(RosterResourceElement delegate)
//  {
//    this.delegate = delegate;
//  }

//  public String getCurrentJid()
//  {
//    if (presence != null)
//    {
//      return presence.getFrom();
//    }
//    return jid.getFullJID();
//  }

  public void addResource(String res, Presence pres)
  {
    RosterResourceElement item = new RosterResourceElement();
    item.setPresence(pres);
    JabberId jid = new JabberId(res);
    item.setJid(jid);
    item.setName(jid.getResource());
    resourcesMap.put(res, item);
    resources.add(item);
  }

  public void removeResource(String res)
  {
    RosterResourceElement item = (RosterResourceElement)resourcesMap.get(res);
    resourcesMap.remove(res);
    resources.remove(item);
    if (item == delegate)
    {
      if (resources.size() == 0)
      {
        delegate = null;
      }
      else
      {
        delegate = (RosterResourceElement) resources.firstElement();
      }
    }
  }

//  public boolean hasResource(String res)
//  {
//    return resourcesMap.containsKey(res);
//  }
//
//  public RosterResourceElement getResource(String res)
//  {
//    return (RosterResourceElement)resourcesMap.get(res);
//  }

  public int getResourcesCount()
  {
    return resources.size();
  }

  public RosterResourceElement getResource(int i)
  {
    return (RosterResourceElement) resources.elementAt(i);
  }

  /**
   * indexOfResource
   *
   * @param child Object
   * @return int
   */
  public int indexOfResource(Object child)
  {
    return resources.indexOf(child);
  }

  /**
   * updateResource
   *
   * @param res String
   * @param presence Presence
   */
  public void updateResource(String res, Presence presence)
  {
    RosterResourceElement item = (RosterResourceElement)resourcesMap.get(res);
    if (item == null)
    {
      addResource(res, presence);
      item = (RosterResourceElement)resourcesMap.get(res);
    }
    else
    {
      item.setPresence(presence);
    }
    delegate = item;
  }

  /**
   * getClientTime
   *
   * @return ClientTime
   */
  public ClientTime getClientTime()
  {
    if (delegate == null) return super.getClientTime();
    return delegate.getClientTime();
  }

  /**
   * getClientVersion
   *
   * @return ClientVersion
   */
  public ClientVersion getClientVersion()
  {
    if (delegate == null) return super.getClientVersion();
    return delegate.getClientVersion();
  }

  /**
   * getJid
   *
   * @return JabberId
   */
  public JabberId getJid()
  {
    if (delegate == null) return super.getJid();
    return delegate.getJid();
  }

  /**
   * getName
   *
   * @return String
   */
  public String getName()
  {
    if (delegate == null) return super.getName();
    return delegate.getName();
  }

  /**
   * getPresence
   *
   * @return Presence
   */
  public Presence getPresence()
  {
    if (delegate == null) return super.getPresence();
    return delegate.getPresence();
  }

  /**
   * getSentPresence
   *
   * @return Presence
   */
  public Presence getSentPresence()
  {
    if (delegate == null) return super.getSentPresence();
    return delegate.getSentPresence();
  }

  /**
   * setClientTime
   *
   * @param t ClientTime
   */
  public void setClientTime(ClientTime t)
  {
    if (delegate == null)
    {
      super.setClientTime(t);
    }
    else
    {
      delegate.setClientTime(t);
    }
  }

  /**
   * setClientVersion
   *
   * @param v ClientVersion
   */
  public void setClientVersion(ClientVersion v)
  {
    if (delegate == null)
    {
      super.setClientVersion(v);
    }
    else
    {
      delegate.setClientVersion(v);
    }
  }

  /**
   * setJid
   *
   * @param jid JabberId
   */
  public void setJid(JabberId jid)
  {
    if (delegate == null)
    {
      super.setJid(jid);
    }
    else
    {
      delegate.setJid(jid);
    }
  }

  /**
   * setName
   *
   * @param name String
   */
  public void setName(String name)
  {
    if (delegate == null)
    {
      super.setName(name);
    }
    else
    {
      delegate.setName(name);
    }
  }

  /**
   * setPresence
   *
   * @param presence Presence
   * @todo Implement this lh.jabber.RosterElement method
   */
  public void setPresence(Presence presence)
  {
    if (delegate == null)
    {
      super.setPresence(presence);
    }
    else
    {
      delegate.setPresence(presence);
    }
  }

  /**
   * setSentPresence
   *
   * @param sentPresence Presence
   */
  public void setSentPresence(Presence sentPresence)
  {
    if (delegate == null)
    {
      super.setSentPresence(sentPresence);
    }
    else
    {
      delegate.setSentPresence(sentPresence);
    }
  }
}
