package lh.jabber;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 13/05/03
 */

public interface ClientsInfoListener
{
  public void receivedVersion(String jid, ClientVersion cv);
  public void receivedTime(String jid, ClientTime ct);
}