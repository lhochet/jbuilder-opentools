package lh.jabber;

import lh.jabber.stanza.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 16/01/03
 */

public interface PresenceSubscriptionListener
{
  public void receivedSubscriptionEvent(Presence presence);
}