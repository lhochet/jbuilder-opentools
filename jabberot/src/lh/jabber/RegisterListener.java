package lh.jabber;

import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:05:31 $
 * @created 20/08/03
 */

public interface RegisterListener extends ErrorListener
{
  public void receivedStanza(Stanza stanza);
}