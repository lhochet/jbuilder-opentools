package lh.util;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 5/07/03
 */

public class Password
{
  public static String encode(String password)
  {
    try
    {
      DESKeySpec desKeySpec = new DESKeySpec("just a simple stupid test key".getBytes());
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
      SecretKey skeySpec = keyFactory.generateSecret(desKeySpec);
//      SecretKeySpec skeySpec = new SecretKeySpec("just a simple stupid test key".getBytes(), "DES");

      // Instantiate the cipher
      Cipher cipher = Cipher.getInstance("DES");
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

      byte[] encrypted = cipher.doFinal(password.getBytes());
//      System.out.println("encrypted string: " + asHex(encrypted));

      return asHex(encrypted);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      return null;
    }
  }

  public static String decode(String encode)
  {
    try
    {
      DESKeySpec desKeySpec = new DESKeySpec("just a simple stupid test key".getBytes());
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
      SecretKey skeySpec = keyFactory.generateSecret(desKeySpec);
//      SecretKeySpec skeySpec = new SecretKeySpec("just a simple stupid test key".getBytes(), "DES");

      // Instantiate the cipher
      Cipher cipher = Cipher.getInstance("DES");
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

      byte[] encrypted = fromHexString(encode);

      cipher.init(Cipher.DECRYPT_MODE, skeySpec);
      byte[] original = cipher.doFinal(encrypted);
      String originalString = new String(original);
//      System.out.println("Original string: " +
//                         originalString + " " + asHex(original));
      return originalString;
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      return null;
    }
  }

  /**
   * Turns array of bytes into string
   *
   * @param buf	Array of bytes to convert to hex string
   * @return	Generated hex string
   */
  public static String asHex(byte buf[])
  {
    StringBuffer strbuf = new StringBuffer(buf.length * 2);
    int i;

    for (i = 0; i < buf.length; i++)
    {
      if (((int) buf[i] & 0xff) < 0x10)
      {
        strbuf.append("0");

      }
      strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
    }

    return strbuf.toString();
  }

  /**
   * Convert a hex string to a byte array.
   * Permits upper or lower case hex.
   *
   * @param s String must have even number of characters.
   * and be formed only of digits 0-9 A-F or
   * a-f. No spaces, minus or plus signs.
   * @return corresponding byte array.
   */
  public static byte[] fromHexString(String s)
  {
    int stringLength = s.length();
    if ((stringLength & 0x1) != 0)
    {
      throw new IllegalArgumentException("fromHexString requires an even number of hex characters");
    }
    byte[] b = new byte[stringLength / 2];

    for (int i = 0, j = 0; i < stringLength; i += 2, j++)
    {
      int high = charToNibble(s.charAt(i));
      int low = charToNibble(s.charAt(i + 1));
      b[j] = (byte) ((high << 4) | low);
    }
    return b;
  }

  /**
   * convert a single char to corresponding nibble.
   *
   * @param c char to convert. must be 0-9 a-f A-F, no
   * spaces, plus or minus signs.
   *
   * @return corresponding integer
   */
  private static int charToNibble(char c)
  {
    if ('0' <= c && c <= '9')
    {
      return c - '0';
    }
    else if ('a' <= c && c <= 'f')
    {
      return c - 'a' + 0xa;
    }
    else if ('A' <= c && c <= 'F')
    {
      return c - 'A' + 0xa;
    }
    else
    {
      throw new IllegalArgumentException("Invalid hex character: " + c);
    }
  }
}
