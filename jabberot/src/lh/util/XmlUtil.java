package lh.util;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:33 $
 * @created 17/10/03
 * <p>Adapted from Jakarta Commons XmlUtils (in the attic for unclear reasons)
 */

public class XmlUtil
{
  /**
   * Escape restricted XML characters to entities.
   */
  public static String escapeXml(String str)
  {
    str = replace(str, "&", "&amp;");
    str = replace(str, "<", "&lt;");
    str = replace(str, ">", "&gt;");
    str = replace(str, "\"", "&quot;");
    str = replace(str, "'", "&apos;");
    return str;
  }

  /**
   * Unescape entities to a 'normal' string
   */
  public static String unescapeXml(String str)
  {
    str = replace(str, "&amp;", "&");
    str = replace(str, "&lt;", "<");
    str = replace(str, "&gt;", ">");
    str = replace(str, "&quot;", "\"");
    str = replace(str, "&apos;", "'");
    return str;
  }

  public static String replace(String text, String repl, String with)
  {
    if (text == null)
    {
      return null;
    }

    StringBuffer buf = new StringBuffer(text.length());
    int start = 0, end = 0;
    while ((end = text.indexOf(repl, start)) != -1)
    {
      //System.err.println("end=" + end);
      buf.append(text.substring(start, end)).append(with);
      start = end + repl.length();
      //System.err.println("new start=" + start);

    }
    buf.append(text.substring(start));
    return buf.toString();
  }

}
