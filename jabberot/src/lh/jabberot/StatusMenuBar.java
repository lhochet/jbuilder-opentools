package lh.jabberot;

import javax.swing.JMenuBar;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import lh.jabber.stanza.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:28 $
 * @created 18/10/03
 */

public class StatusMenuBar extends JMenuBar
{
  private class MiniPresence
  {
    int type;
    String status;
    int priority;
  }

  private class MiniPresence_actionAdapter implements java.awt.event.ActionListener
  {
    StatusMenuBar adaptee;

    MiniPresence_actionAdapter(StatusMenuBar adaptee)
    {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e)
    {
      adaptee.miniPresence_actionPerformed(e);
    }
  }

  private JabberService service = null;
  private String target = null;
  private MiniPresence_actionAdapter adapter = null;

  JMenu jStatusMenu = new JMenu();
  JMenu jStatusAway = new JMenu("Away");
  JMenu jStatusXA = new JMenu("XA");
  JMenu jStatusDND = new JMenu("DND");
  private HashMap statuses = new HashMap();
  private boolean forceUpdate = false;

  public StatusMenuBar(JabberService service, String target)
  {
    this(service, target, true);
  }
  public StatusMenuBar(JabberService service, String target, boolean allowInvisible)
  {
    this(service, target, allowInvisible, true);
  }
  public StatusMenuBar(JabberService service, String target, boolean allowInvisible, boolean listenForServiceUpdate)
  {
    super();
    this.service = service;
    this.target = target;
    adapter = new MiniPresence_actionAdapter(this);

    jStatusMenu.setText("Available");
    jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.ONLINE));
    add(jStatusMenu);

    jStatusAway.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.AWAY));
    jStatusXA.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.XA));
    jStatusDND.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.DND));

    jStatusMenu.add(createItem(Presence.AVAILABLE, "Available", 2));
    jStatusMenu.add(createItem(Presence.AVAILABLE, "Online", 1));
    jStatusMenu.add(createItem(Presence.CHAT, "Free for chat", 1));
    jStatusMenu.add(createItem(Presence.AWAY, "Away", 0));
    jStatusMenu.add(createItem(Presence.AWAY, "Coding...", 0));
    jStatusMenu.add(createItem(Presence.AWAY, "Lunch", 0));
    jStatusMenu.add(createItem(Presence.AWAY, "Dinner", 0));
    jStatusMenu.add(createItem(Presence.AWAY, "Meeting", 0));
    jStatusMenu.add(createItem(Presence.XA, "xa", 0));
    jStatusMenu.add(createItem(Presence.XA, "Coding...", 0));
    jStatusMenu.add(createItem(Presence.XA, "Dinner", 0));
    jStatusMenu.add(createItem(Presence.XA, "Meeting", 0));
    jStatusMenu.add(createItem(Presence.XA, "Gone Home", 0));
    jStatusMenu.add(createItem(Presence.XA, "Sleeping", 0));
    jStatusMenu.add(createItem(Presence.DND, "dnd", -1));
    jStatusMenu.add(createItem(Presence.DND, "Busy", -1));
    jStatusMenu.add(createItem(Presence.DND, "Working", -1));
    jStatusMenu.add(createItem(Presence.DND, "Mad", -1));
    if (allowInvisible) jStatusMenu.add(createItem(Presence.INVISIBLE, "Invisible", -1));

    if (listenForServiceUpdate)
    {
      PresenceUpdateListener pul = new PresenceUpdateListener()
      {
        public void receivedUpdateEvent(Presence presence)
        {
          processReceivedUpdateEvent(presence);
        }
      };

      service.addServiceSentPresenceListener(pul);
    }


  }

  public JMenu getMenu()
  {
    return jStatusMenu;
  }

  public void setTarget(String target)
  {
    this.target = target;
  }

  void miniPresence_actionPerformed(ActionEvent e)
  {
    int type = Presence.AVAILABLE;
    String status = "Offline";
    int priority = -1;
    int senttype = Presence.AVAILABLE;

    if (e.getSource() instanceof JMenuItem)
    {
      JMenuItem item = (JMenuItem)e.getSource();
      MiniPresence p = (MiniPresence)statuses.get(item);
      if (p == null) return;
      type = p.type;
      status = p.status;
      priority = p.priority;
      jStatusMenu.setText(status);
      String show = null;
      switch(type)
      {
        case Presence.AVAILABLE:
          // type = null when avail
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.ONLINE));
          break;
        case Presence.CHAT:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.CHAT));
          show = "chat";
//          status = null;
          break;
        case Presence.AWAY:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.AWAY));
          show = "away";
          break;
        case Presence.XA:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.XA));
          show = "xa";
          break;
        case Presence.DND:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.DND));
          show = "dnd";
          break;
        case Presence.INVISIBLE:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.INVISIBLE));
          show = null;
//          target = null;
          status = null;
          senttype = Presence.INVISIBLE;
          break;
      }
      service.sendPresence(senttype, target, status, show, priority);
      return;
    }
  }

  private JMenuItem createItem(int type, String status, int priority)
  {
    JMenuItem ret = null;
    MiniPresence p = new MiniPresence();
    p.type = type;
    p.status = status;
    p.priority = priority;

    JMenuItem item = new JMenuItem(status);
    item.addActionListener(adapter);

    statuses.put(item, p);
    switch(type)
    {
      case Presence.AWAY:
        item.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.AWAY));
        jStatusAway.add(item);
        ret = jStatusAway;
        break;
      case Presence.XA:
        item.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.XA));
        jStatusXA.add(item);
        ret = jStatusXA;
        break;
      case Presence.DND:
        item.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.DND));
        jStatusDND.add(item);
        ret = jStatusDND;
        break;
      case Presence.INVISIBLE:
        item.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.INVISIBLE));
        ret = item;
        break;
      default:
        item.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.ONLINE));
        ret = item;
    }

    return ret;
  }

  private void processReceivedUpdateEvent(Presence presence)
  {
    if (presence == null) return;

    boolean process = false;
    String to = presence.getTo();
    if (to == null)
    {
      if (target == null)
      {
        process = true;
      }
      // else ignore
    }
    else
    {
      if (to.equals(target))
      {
        process = true;
      }
    }

    if (process || forceUpdate)
    {
      String show = "Available";
      switch (presence.getShowInt())
      {
        case Presence.AVAILABLE:
          // type = null when avail
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.ONLINE));
          break;
        case Presence.CHAT:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.CHAT));
          show = "Chat";
//          status = null;
          break;
        case Presence.AWAY:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.AWAY));
          show = "Away";
          break;
        case Presence.XA:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.XA));
          show = "XA";
          break;
        case Presence.DND:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.DND));
          show = "DND";
          break;
        case Presence.INVISIBLE:
          jStatusMenu.setIcon(StatusIcons.getInstance().getIcon(StatusIcons.INVISIBLE));
          show = "Invisible";
          break;
      }


      String status = presence.getStatus();
      if (status == null)
      {
        if (show != null) status = show;
      }

      jStatusMenu.setText(status);
    }
  }

  public void updatePresence(Presence presence)
  {
    boolean savForceUpdate = forceUpdate;
    forceUpdate = true;
    processReceivedUpdateEvent(presence);
    forceUpdate = savForceUpdate;
  }

  public void setEnabled(boolean enabled)
  {
    super.setEnabled(enabled);
    jStatusMenu.setEnabled(enabled);
  }

}

