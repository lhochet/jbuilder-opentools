package lh.jabberot;

import com.borland.primetime.ide.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:27 $
 * @created 5/08/03
 */

public class RosterCategory extends MessageCategory
{
  private static RosterCategory instance = null;
  static RosterCategory getInstance()
  {
    return instance;
  }
  static void setInstance(RosterCategory rc)
  {
    instance = rc;
  }

//  private JabberService service = null;
  private RosterPanel panel = null;
//  private boolean isshown = false;

//  public RosterCategory()
//  {
//    super("Roster");
//  }

  public RosterCategory(JabberService service)
  {
    super("Roster");
//    this.service = service;
    panel = new RosterPanel(service);
  }

//  public RosterPanel getPanel()
//  {
//    return panel;
//  }
//
//  public boolean isShown()
//  {
//    return isshown;
//  }
//  public void setShown(boolean isshown)
//  {
//    this.isshown = isshown;
//  }

//  public void categoryClosing() throws com.borland.primetime.util.VetoException
//  {
//    throw new com.borland.primetime.util.VetoException();
//    super.categoryClosing();
//    isshown = false;
//  }

  public void show()
  {
    Browser.getActiveBrowser().getMessageView().addCustomTab(this, panel);
    Browser.getActiveBrowser().getMessageView().setActiveTab(this);
  }
}
