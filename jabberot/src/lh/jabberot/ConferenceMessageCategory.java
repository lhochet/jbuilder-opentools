package lh.jabberot;

import com.borland.primetime.ide.MessageCategory;
import lh.jabber.*;
import javax.swing.*;
import com.borland.primetime.ide.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:20 $
 * @created 18/09/03
 */

public class ConferenceMessageCategory extends MessageCategory
{
  private JabberService service = null;
  private JabberId jid = null;
  private ConferencePanel panel = null;
  private boolean isshown = false;

  public ConferenceMessageCategory(JabberService service, JabberId jid)
  {
    super(jid.getUser() + " - Conference");
    this.service = service;
    this.jid = jid;
    panel = new ConferencePanel(service, jid);
  }

  public ConferencePanel getPanel()
  {
    return panel;
  }

  public boolean isShown()
  {
    return isshown;
  }
  public void setShown(boolean isshown)
  {
    this.isshown = isshown;
  }

  public void categoryClosing() throws com.borland.primetime.util.VetoException
  {
    super.categoryClosing();
    isshown = false;
    switch (JOptionPane.showConfirmDialog(Browser.getActiveBrowser(), "Exit conference in addition to closing tab?"))
    {
      case JOptionPane.YES_OPTION:
        JabberClient.service.sendPresence(Presence.UNAVAILABLE, jid.toString(), null, null, -1);
        break;
      case JOptionPane.NO_OPTION:
        // do nothing
        break;
      case JOptionPane.CANCEL_OPTION:
        throw new com.borland.primetime.util.VetoException("Conference tab closure canceled.");
//        break;
    }

  }

  /**
   * categoryWillPromptOnClose
   *
   * @return boolean
   * @todo Implement this com.borland.primetime.ide.MessageCategory method
   */
  public boolean categoryWillPromptOnClose()
  {
    return true;
  }
}