package lh.jabberot;

import com.borland.primetime.properties.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:24 $
 * @created 5/07/03
 */

public class JabberPropertyGroup implements PropertyGroup
{
//  public static final Object JABBER_TOPIC = new Object();

  public void initializeProperties()
  {
  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == JabberClient.JABBER_TOPIC)
    {
      return new PropertyPageFactory("Options")
      {
        public PropertyPage createPropertyPage()
        {
          return new JabberPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

}