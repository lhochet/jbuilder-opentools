package lh.jabberot;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.*;
import java.awt.*;
import javax.swing.*;
import lh.jabber.*;
import com.borland.jbuilder.ide.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:28 $
 * @created 30/12/02
 */

public class RosterTreeRenderer extends DefaultTreeCellRenderer
{
  public RosterTreeRenderer()
  {
  }

//  Icon train = null;
//
//  public ControlTreeRenderer()
//  {
//    train = new ImageIcon(lh.trainsim.TrainSim.class.getResource("imgs/locohori0_180.gif"));
//  }
//
  public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                boolean sel, boolean expanded,
                                                boolean leaf, int row,
                                                boolean hasFocus)
  {
    super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
                                       hasFocus);
    if (value instanceof RosterElement)
    {
      setIcon(StatusIcons.getInstance().getIcon(StatusIcons.OFFLINE));
      RosterElement item = (RosterElement)value;
      StringBuffer buf = new StringBuffer();
      if (item instanceof RosterResourceElement)
      {
        buf.append(item.getJid().getResource());
      }
      else
      {
        buf.append(item.getJid().getJID());
      }
      buf.append(" [");
      Presence presence = item.getPresence();
      if (presence == null)
      {
        buf.append("Offline");
//        setIcon(StatusIcons.getInstance().getIcon(StatusIcons.OFFLINE));
      }
      else
      {
        switch (presence.getType())
        {
          case Presence.SUBSCRIBE:
            buf.append("Subscribing...");
            break;
          case Presence.SUBSCRIBED:
            buf.append("Subscribed");
            break;
          case Presence.UNSUBSCRIBE:
            buf.append("Unsubscribing...");
            break;
          case Presence.UNSUBSCRIBED:
            buf.append("Removed");
            break;
          case Presence.UNAVAILABLE:
            {
              buf.append("Offline");
            }
            break;
          case Presence.AVAILABLE:
            {
              setIcon(StatusIcons.getInstance().getIcon(StatusIcons.ONLINE));
              String show = presence.getShow();
              String status = presence.getStatus();
              boolean hasStatus = (status != null) && (!status.equals(""));
              if (show == null)
              {
                buf.append(hasStatus ? status : "Available");
              }
              else
              {
                buf.append(show);

                if (show.equals("away")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.AWAY));
                else if (show.equals("xa")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.XA));
                else if (show.equals("dnd")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.DND));
                else if (show.equals("chat")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.CHAT));

                if (hasStatus)
                {
                  buf.append(": ");
                  buf.append(presence.getStatus());
                }
              }
            }
            break;
        }
      }
      buf.append("]");

      Presence sentpresence = item.getSentPresence();
      if (sentpresence != null)
      {
        buf.append(" [I am shown as being: ");
        String show = "Available";
        String status = sentpresence.getStatus();
        switch (sentpresence.getShowInt())
        {
          case Presence.AVAILABLE:
            // type = null when avail
            break;
          case Presence.CHAT:
            show = "Chat";
            break;
          case Presence.AWAY:
            show = "Away";
            break;
          case Presence.XA:
            show = "XA";
            break;
          case Presence.DND:
            show = "DND";
            break;
          case Presence.INVISIBLE:
            show = "Invisible";
            break;
        }
        buf.append(show);
        if ((status != null) && (!status.equals("")))
        {
          buf.append(" - ");
          buf.append(status);
        }
        buf.append("]");
      }

      setText(buf.toString());
    }
    else
    {
      setIcon(null);
    }

//    if (value instanceof Train)
//    {
//      String id = ((Train)value).getId();
//      setText(id);
//      setToolTipText(id);
//      setIcon(train);
//    }
//    if (value instanceof BoardElement)
//    {
//      String id = ((BoardElement)value).getId();
//      setText(id);
//      setToolTipText(id);
//      setIcon(((BoardElement)value).getImage());
//    }
    return this;
  }

}

