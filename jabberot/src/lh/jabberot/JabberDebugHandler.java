package lh.jabberot;

import java.util.logging.*;
import com.borland.primetime.ide.*;
import lh.jabber.*;


/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:23 $
 * @created 13/07/03
 */

public class JabberDebugHandler extends Handler
{
  static JabberLogCategory JABBER_DEBUG = new JabberLogCategory();
  private static JabberDebugHandler instance = null;
  private JabberDebugPane dp = null;
  private JabberDebugTextPane tp = null;

  public static JabberDebugHandler getInstance()
  {
    if (instance == null) instance = new JabberDebugHandler();
    return instance;
  }
  private JabberDebugHandler()
  {
    dp = new JabberDebugPane();
    tp = dp.getDebugTextPane();
  }
  public void flush()
  {
    // do nothing
  }
  public void publish(LogRecord record)
  {
    Level lvl = record.getLevel();
    if (lvl.equals(Level.FINE))
    {
      tp.appendIn(record.getMessage());
    }
    else if (lvl.equals(Level.FINER))
    {
      tp.appendOut(record.getMessage());
    }
    else
    {
      tp.appendErr(record.getMessage());
    }
  }
  public void close() throws java.lang.SecurityException
  {
    // do nothing
  }

  public JabberDebugPane getDebugPane()
  {
    return dp;
  }

  public void show()
  {
    Browser.getActiveBrowser().getMessageView().addCustomTab(JabberDebugHandler.JABBER_DEBUG, dp);
    Browser.getActiveBrowser().getMessageView().setActiveTab(JabberDebugHandler.JABBER_DEBUG);
  }
  public void setService(JabberService service)
  {
    dp.setService(service);
  }

}
