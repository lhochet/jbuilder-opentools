package lh.jabberot;

import java.text.*;
import lh.jabber.*;
import java.util.logging.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import lh.jabber.stanza.*;
import java.util.*;
import javax.swing.*;
import com.borland.primetime.ui.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:21 $
 * @created 18/09/03
 */

public class ConferencePanel extends JPanel implements MessageListener, MouseListener, ServiceStatusListener
{
  Logger log = Logger.getLogger(ConferencePanel.class.getName());
  {
    log.setLevel(Level.OFF);
  }


  private Presence curPres = null;

  private String target;
  private JabberId jid;
  private JabberService service;
  private ConferenceParticipantListModel participantsModel = new ConferenceParticipantListModel();

  private SimpleAttributeSet meTxtAttr = null;
  private SimpleAttributeSet otherTxtAttr = null;
  private SimpleAttributeSet timeTxtAttr = null;
  private SimpleAttributeSet infoTxtAttr = null;
  private SimpleAttributeSet msgTxtAttr = null;
  private SimpleAttributeSet buzzTxtAttr = null;
  private Document doc = null;

  BorderLayout borderLayout1 = new BorderLayout();
  JTextField tfMsg = new JTextField();
  JPanel jPanel4 = new JPanel();
  JLabel jLabel1 = new JLabel();
  BorderLayout borderLayout3 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  private JButton btnBuzz = new JButton();
  private JButton btnSend = new JButton();
  private FlowLayout flowLayout1 = new FlowLayout();

  private SimpleDateFormat sdf = null;
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JTextPane tpMsgs = new JTextPane();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JList lstParticipants = new JList();

  private JMenuItem jMenuItem1 = new JMenuItem();
  private JMenuItem jMenuItem3 = new JMenuItem();
  private JPopupMenu popupContact = new JPopupMenu();
  private JMenuItem jMenuItem2 = new JMenuItem();
  private JMenuItem jMenuItem4 = new JMenuItem();
  private JMenu jMenu2 = new JMenu();
  private JMenuItem jMenuItem5 = new JMenuItem();
  JPanel jPanel2 = new JPanel();
  JLabel lblSubjectLabel = new JLabel();
  FlowLayout flowLayout2 = new FlowLayout();
  JLabel lblSubject = new JLabel();
  private JLabel lblStatus = new JLabel();
  private StatusMenuBar statusbar = null;

  public ConferencePanel()
  {
    this(null, null);
  }
  public ConferencePanel(JabberService service, JabberId jid)
  {
    this.service = service;
    if (service != null)
    {
      service.addStatusListener(this);
    }

    this.jid = jid;
    if (jid != null) target = jid.getJID();
    sdf = new SimpleDateFormat("[HH:mm]");
    try
    {
      preJBInit();
      jbInit();
      postJBInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * postJBInit
   */
  private void postJBInit()
  {
    statusbar = new StatusMenuBar(service, jid.getJID(), false);
    jPanel1.add(statusbar);
    jMenuItem3.setText("Version");
    jMenuItem3.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuItem3_actionPerformed(e);
      }
    });
    jMenuItem1.setText("Send Message...");
    jMenuItem1.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuItem1_actionPerformed(e);
      }
    });
    jMenuItem2.setText("Start chat...");
    jMenuItem2.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuItem2_actionPerformed(e);
      }
    });
    jMenuItem4.setText("Time");
    jMenuItem4.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuItem4_actionPerformed(e);
      }
    });
    jMenuItem5.setText("Last Activity");
    jMenuItem5.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuItem5_actionPerformed(e);
      }
    });
    popupContact.add(jMenuItem1);
    popupContact.add(jMenuItem2);
    popupContact.addSeparator();
    popupContact.add(jMenu2);
    jMenu2.setText("Info");
    jMenu2.add(jMenuItem3);
    jMenu2.add(jMenuItem4);
    jMenu2.add(jMenuItem5);
//    popupContact.addSeparator();
//    jParticipantStatusMenuBar = new StatusMenuBar(service, jid.getJID(), false);
//    popupContact.add(jParticipantStatusMenuBar.getMenu());

    lstParticipants.addMouseListener(this);
  }

  private void jbInit() throws Exception
  {
    jPanel4.setLayout(borderLayout3);
    tfMsg.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfMsg_actionPerformed(e);
      }
    });
    tfMsg.setSelectionStart(11);
    tfMsg.setText("");
    this.setLayout(borderLayout1);
    jLabel1.setMaximumSize(new Dimension(41, 13));
    jLabel1.setText("Message:");
    borderLayout3.setHgap(5);
    btnBuzz.setMaximumSize(new Dimension(41, 19));
    btnBuzz.setMinimumSize(new Dimension(41, 19));
    btnBuzz.setPreferredSize(new Dimension(41, 19));
    btnBuzz.setMargin(new Insets(2, 2, 2, 2));
    btnBuzz.setText("Buzz");
    btnBuzz.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBuzz_actionPerformed(e);
      }
    });
    btnSend.setMaximumSize(new Dimension(41, 19));
    btnSend.setMinimumSize(new Dimension(41, 19));
    btnSend.setPreferredSize(new Dimension(41, 19));
    btnSend.setMargin(new Insets(2, 2, 2, 2));
    btnSend.setText("Send");
    btnSend.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnSend_actionPerformed(e);
      }
    });
    jPanel1.setLayout(flowLayout1);
    flowLayout1.setHgap(3);
    flowLayout1.setVgap(0);
    jPanel2.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    lblSubjectLabel.setText("Subject:");
    lblSubject.setText(" ");
    flowLayout2.setAlignment(0);
    flowLayout2.setHgap(0);
    flowLayout2.setVgap(0);
    lblStatus.setText("Status:");
    tpMsgs.setEditable(false);
    jPanel4.add(jLabel1, BorderLayout.WEST);
    jPanel4.add(tfMsg, BorderLayout.CENTER);
    jPanel4.add(jPanel1, BorderLayout.EAST);
    jPanel1.add(btnSend, null);
    jPanel1.add(btnBuzz, null);
    jPanel1.add(lblStatus);
    this.add(jPanel2, java.awt.BorderLayout.NORTH);
    jPanel2.add(lblSubjectLabel);
    jPanel2.add(lblSubject);
    this.add(jScrollPane1,  BorderLayout.EAST);
    jScrollPane1.getViewport().add(lstParticipants, null);
    this.add(jScrollPane2,  BorderLayout.CENTER);
    jScrollPane2.getViewport().add(tpMsgs, null);
    this.add(jPanel4, BorderLayout.SOUTH);
  }

  void preJBInit()
  {
    meTxtAttr = new SimpleAttributeSet();
    meTxtAttr.addAttribute(StyleConstants.Foreground, Color.BLUE);
    otherTxtAttr = new SimpleAttributeSet();
    otherTxtAttr.addAttribute(StyleConstants.Foreground, Color.RED);
    timeTxtAttr = new SimpleAttributeSet();
    timeTxtAttr.addAttribute(StyleConstants.Foreground, Color.GRAY);
    msgTxtAttr = new SimpleAttributeSet();
    msgTxtAttr.addAttribute(StyleConstants.Foreground, Color.BLACK);
    infoTxtAttr = new SimpleAttributeSet();
    infoTxtAttr.addAttribute(StyleConstants.Foreground, Color.DARK_GRAY);
    buzzTxtAttr = new SimpleAttributeSet();
    buzzTxtAttr.addAttribute(StyleConstants.Foreground, Color.RED);
    StyleConstants.setBold(buzzTxtAttr, true);
    doc = tpMsgs.getDocument();

    lstParticipants.setModel(participantsModel);
    lstParticipants.setCellRenderer(new ConferenceParticipantListCellRenderer());


    MouseAdapter mouseadapter = new MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        if ((e.getClickCount() == 1) && SwingUtilities.isLeftMouseButton(e))
        {
          String subject = JOptionPane.showInputDialog("New subject:", lblSubject.getText());
          if (subject != null)
          {
            service.sendMessage(Message.GROUPCHAT, jid.getJID(), subject, "/me has set the subject to: " + subject);
          }
        }
      }
    };
    lblSubject.addMouseListener(mouseadapter);
    lblSubjectLabel.addMouseListener(mouseadapter);
  }

  public void setTarget(String target)
  {
    this.target = target;
  }

   public void received(Message msg)
  {
    if (msg.getType() != Message.GROUPCHAT) return;
    log.finest("ConferencePanel(" + jid + ").received");
    JabberId from = new JabberId(msg.getFrom());
//    if (!jid.equalFull(from)) return;
    String nick = jid.getResource();
    if ((nick != null) && (nick.equals(from.getResource())))
    {
      if (msg.getSubject() == null) return;
    }
    log.finest("ConferencePanel(" + jid + ").received adding msg: " + msg.getBody());
    if (msg instanceof BuzzStanza)
    {
//      msg.setSubject("<Buzz>");
      msg.setBody("<Buzz>");
      com.borland.primetime.ide.Browser.getActiveBrowser().getStatusView().setText("Buzz from " + from, com.borland.primetime.ide.StatusView.TYPE_WARNING);
      com.borland.primetime.audio.AudioClipPlayer.playClipNamed(com.borland.jbuilder.audio.JBuilderAudio.KEY_BUILD_WARNINGS);
    }
    else
    {
      com.borland.primetime.ide.Browser.getActiveBrowser().getStatusView().setText("Groupchat message from " + from + " : " + msg.getBody());
    }
    if (msg.getSubject() != null) lblSubject.setText(msg.getSubject());

    Date d = msg.getTime();
    if (d == null)
    {
      addMessage(from.getResource(), msg.getBody());
    }
    else
    {
      addMessage(from.getResource(), msg.getBody(), d);
    }

    repaint();
  }

  void tfMsg_actionPerformed(ActionEvent e)
  {
    String msg = tfMsg.getText();
    if (msg.equals("")) return;

    service.sendMessage(Message.GROUPCHAT, target, null, msg);
    addMessage("Me", msg);

    tfMsg.setText("");
    repaint();
  }

  void btnSend_actionPerformed(ActionEvent e)
  {
    tfMsg_actionPerformed(e);
  }

  void btnBuzz_actionPerformed(ActionEvent e)
  {
    service.sendBuzz(Message.CHAT, target);
    addMessage("Me", "<Buzz>");
  }

  void addMessage(String from, String msg)
  {
    addMessage(from, msg, new Date());
  }
  void addMessage(String from, String msg, Date dnow)
  {
    if (msg == null) return;
    if (msg.endsWith("\n")) msg = msg.substring(0, msg.length() - 1);
    String now = sdf.format(dnow);

    try
    {
        int len = doc.getLength();
        int inserted = 0;
        doc.insertString(len, now + " ", timeTxtAttr);
        inserted = now.length() + 1;
        if (from == null)
        {
          // do nothing here
        }
        else if (from.equals("Me"))
        {
          doc.insertString(len + inserted, from + ":", meTxtAttr);
        }
        else
        {
          doc.insertString(len + inserted, from + ":", otherTxtAttr);
        }
        if (from != null) inserted += from.length() + 1;
        if (msg.equals("<Buzz>"))
        {
          doc.insertString(len + inserted, msg + "\n", buzzTxtAttr);
        }
        else
        {
          if (from == null)
          {
            doc.insertString(len + inserted, msg + "\n", infoTxtAttr);
          }
          else
          {
            doc.insertString(len + inserted, msg + "\n", msgTxtAttr);
          }
        }
        inserted += msg.length() + 1;
        tpMsgs.paintImmediately(tpMsgs.getBounds());
        tpMsgs.setCaretPosition(len + inserted);
    }
    catch (BadLocationException blex)
    {
      blex.printStackTrace();
    }
  }
  /**
   * receivedUpdateEvent
   *
   * @param presence Presence
   */
  public void receivedUpdateEvent(Presence presence)
  {
//    String nick = new JabberId(presence.getFrom()).getResource();
    switch (presence.getType())
    {
      case Presence.AVAILABLE:
        participantsModel.addParticipant(presence);
        break;
      case Presence.UNAVAILABLE:
        participantsModel.removeParticipant(presence);
        break;
    }
  }

  public void mouseClicked(MouseEvent e)
  {
    if ((e.getClickCount() == 1) && SwingUtilities.isRightMouseButton(e)) mouseRightClicked(e);
  }

  /** lh method */
  public void mouseRightClicked(MouseEvent e)
  {
    Point p = e.getPoint();
    int ind = lstParticipants.locationToIndex(p);
    if (ind == -1) return;
    curPres = (Presence)participantsModel.getElementAt(ind);
    if (curPres == null) return;

//    jParticipantStatusMenuBar.setTarget(curPres.getFrom());

    popupContact.show(lstParticipants, p.x, p.y);
  }

  /** not used */
  public void mousePressed(MouseEvent e){}
  /** not used */
  public void mouseReleased(MouseEvent e){}
  /** not used */
  public void mouseEntered(MouseEvent e){}
  /** not used */
  public void mouseExited(MouseEvent e){}

  void jMenuItem1_actionPerformed(ActionEvent e)
  {
    if (curPres == null) return;

    SendMessagePanel panel = new SendMessagePanel();
    panel.setTo(curPres.getFrom());
    if (DefaultDialog.showModalDialog(this, "Send message", panel, null))
    {
      service.sendMessage(Message.NORMAL, panel.getTo(), panel.getSubject(), panel.getMessage());
    }
  }

  void jMenuItem2_actionPerformed(ActionEvent e)
  {
    if (curPres == null) return;
    JabberId jid = new JabberId(curPres.getFrom());

    //start chat
    ChatManager.getInstance().addChat(jid);
  }

  void jMenuItem3_actionPerformed(ActionEvent e)
  {
    if (curPres == null) return;
    JabberId jid = new JabberId(curPres.getFrom());

    // ask version
    service.requestVersion(jid);
  }

  void jMenuItem4_actionPerformed(ActionEvent e)
  {
    if (curPres == null) return;
    JabberId jid = new JabberId(curPres.getFrom());

    // ask time
    service.requestTime(jid);
  }

  void jMenuItem5_actionPerformed(ActionEvent e)
  {
    if (curPres == null) return;
    JabberId jid = new JabberId(curPres.getFrom());

    // ask last activity
    service.requestLastActivity(jid);
  }

  public void serviceStatusUpdate(int status)
  {
    switch (status)
    {
      case JabberService.READY:
        tfMsg.setEnabled(true);
        btnSend.setEnabled(true);
        btnBuzz.setEnabled(true);
        statusbar.setEnabled(true);
        break;
      case JabberService.DISCONNECTED:
        addMessage(null, "Disconnected");
        tfMsg.setEnabled(false);
        btnSend.setEnabled(false);
        btnBuzz.setEnabled(false);
        statusbar.setEnabled(false);
        break;
      default:
        tfMsg.setEnabled(false);
        btnSend.setEnabled(false);
        btnBuzz.setEnabled(false);
        statusbar.setEnabled(false);
    }
  }

}

