package lh.jabberot;

import com.borland.primetime.actions.*;
import com.borland.primetime.*;
import com.borland.primetime.ide.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:23 $
 * @created 19/08/03
 */

public class JabberClientMenu
{
  static final ActionGroup GROUP_Jabber = new ActionGroup("Jabber", 'J', "Jabber actions group...");

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH JabberClient Menu 0.1");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    GROUP_Jabber.setPopup(true);
    Browser.addMenuGroup(/*2,*/ GROUP_Jabber);

  }
}
