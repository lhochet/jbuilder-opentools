package lh.jabberot;

import lh.jabber.OOBListener;
import java.net.*;
import java.io.*;
import javax.swing.*;
import com.borland.primetime.ide.*;
import lh.jabber.*;
import lh.jabber.stanza.*;
import java.util.logging.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:22 $
 * @created 15/11/03
 */

public class JOTOOBListenerImpl implements OOBListener
{
  Logger log = Logger.getLogger(JOTOOBListenerImpl.class.getName());


  public JOTOOBListenerImpl()
  {
  }
  public void receivedOOB(JabberService service, IQSetOOBStanza stanza)
  {
    String from = stanza.getFrom();
    String urls = stanza.getUrl();
    String desc = stanza.getDesc();
    String id = stanza.getId();
    log.finest("from = " + from);
    log.finest("url = " + urls);
    log.finest("desc = " + desc);

    JabberId jid = new JabberId(from);

    try
    {
      String dirs = FileTransferPropertyPage.DOWNLOAD_PATH.getValue();
      File dir = new File(dirs);
      dir.mkdirs(); // make sure dir exist

      URL url = new URL(urls);

      String fstr = url.getFile().substring(1);
      log.finest("fstr=" + fstr);

      int ret = JOptionPane.showConfirmDialog(Browser.getActiveBrowser(), jid.getUser() + " is sending you '" + fstr + "'. Accept it ?");
      if (ret != JOptionPane.OK_OPTION)
      {
        String msg = "<url>" + url + "</url><desc>" + desc + "</desc>";
        String error = "<error code=\"406\">Not Acceptable</error>";
        service.sendExtendedIQ(IQ.ERROR, from, "jabber:iq:oob", msg, error);
        return;
      }


      File f = new File(dir, fstr);
      System.out.println("file = " + f);

      URLConnection conn = url.openConnection();
      log.finest("sz = " + conn.getContentLength());
      BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(f));
      int csz = 1024; //conn.getContentLength() < 1 ? 50000 : conn.getContentLength();
      byte[] content = new byte[csz];
      int len = 0;
//      int tot = 0;
      while ((len = bis.read(content/*, len, csz - tot*/)) > 0)
      {
        bos.write(content, 0, len);
//        System.out.println("wrote len = "+len);
//        tot += len;
      }
      bis.close();
      bos.flush();
      bos.close();
      service.sendIQResult(from, id);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      String msg = "<url>" + urls + "</url><desc>" + desc + "</desc>";
      String error = "<error code=\"404\">Not Found</error>";
      service.sendExtendedIQ(IQ.ERROR, from, "jabber:iq:oob", msg, error);
    }
  }

}
