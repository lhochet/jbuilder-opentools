package lh.jabberot;

import javax.swing.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:28 $
 */

public class StatusIcons
{
  public static final int ONLINE = 1;
  public static final int OFFLINE = 2;
  public static final int AWAY = 3;
  public static final int XA = 4;
  public static final int DND = 5;
  public static final int INVISIBLE = 6;
  public static final int CHAT = 7;

  private static StatusIcons instance = null;
  public static StatusIcons getInstance()
  {
    if (instance == null)
    {
      instance = new StatusIcons();
      instance.loadDefaults();
    }
    return instance;
  }

  private Icon online = null;
  private Icon offline = null;
  private Icon away = null;
  private Icon xa = null;
  private Icon dnd = null;
  private Icon invisible = null;
  private Icon chat = null;

  private StatusIcons()
  {
  }

  public Icon getIcon(int status)
  {
    switch (status)
    {
      case ONLINE: return online;
      case OFFLINE: return offline;
      case AWAY: return away;
      case XA: return xa;
      case DND: return dnd;
      case INVISIBLE: return invisible;
      case CHAT: return chat;
      default: return null;
    }
  }

  public void loadDefaults()
  {
    online = new ImageIcon(lh.jabberot.JabberClient.class.getResource("images/online.png"));
    offline = new ImageIcon(lh.jabberot.JabberClient.class.getResource("images/offline.png"));
    away = new ImageIcon(lh.jabberot.JabberClient.class.getResource("images/away.png"));
    xa = new ImageIcon(lh.jabberot.JabberClient.class.getResource("images/xa.png"));
    dnd = new ImageIcon(lh.jabberot.JabberClient.class.getResource("images/dnd.png"));
    invisible = new ImageIcon(lh.jabberot.JabberClient.class.getResource("images/invisible.png"));
    chat = new ImageIcon(lh.jabberot.JabberClient.class.getResource("images/chat.png"));
  }

}
