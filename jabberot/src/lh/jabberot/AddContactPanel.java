package lh.jabberot;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: Jabber OpenTool</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:18 $
 * @created 7/08/04
 */
public class AddContactPanel extends JPanel
{
  private JLabel lblJID = new JLabel();
  private JTextField tfJID = new JTextField();
  private JLabel lblNick = new JLabel();
  private JTextField tfNick = new JTextField();
  private JLabel lblGroup = new JLabel();
  private JComboBox cbGroup = new JComboBox();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JPanel jPanel1 = new JPanel();
  public AddContactPanel()
  {
    try
    {
      jbInit();
      lblNick.setVisible(false);
      tfNick.setVisible(false);
      lblGroup.setVisible(false);
      cbGroup.setVisible(false);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    lblJID.setText("Jabber ID:");
    this.setLayout(gridBagLayout1);
    tfJID.setToolTipText("");
    tfJID.setText("");
    tfJID.setColumns(30);
    lblNick.setText("Nickname:");
    tfNick.setText("");
    tfNick.setColumns(20);
    lblGroup.setText("Group:");
    cbGroup.setEditable(true);
    jPanel1.setOpaque(false);
    this.add(lblJID,        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    this.add(tfJID,      new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    this.add(lblNick,    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    this.add(lblGroup,    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 1, 0));
    this.add(tfNick,    new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    this.add(cbGroup,    new GridBagConstraints(1, 2, 2, 2, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    this.add(jPanel1,  new GridBagConstraints(0, 3, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
  }

  public String getJID()
  {
    return tfJID.getText();
  }

  public String getNick()
  {
    return tfNick.getText();
  }

  public String getGroup()
  {
    return (String)cbGroup.getSelectedItem();
  }
}
