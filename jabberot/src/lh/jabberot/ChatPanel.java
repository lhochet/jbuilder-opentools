package lh.jabberot;

import java.text.*;
import java.util.*;
import java.util.logging.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;

import lh.jabber.*;
import lh.jabber.stanza.*;
import com.borland.primetime.ide.Browser;
import lh.jabberot.whiteboard.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:19 $
 * @created 14/07/03
 */

public class ChatPanel extends JPanel implements MessageListener, ServiceStatusListener
{
  Logger log = Logger.getLogger(ChatPanel.class.getName());
  {
    log.setLevel(Level.OFF);
  }


  private static int msgid = 0;
  private String target;
  private JabberId jid;
  private JabberService service;

  private WhiteBoardNode whiteboard = null;

  private SimpleAttributeSet meTxtAttr = null;
  private SimpleAttributeSet otherTxtAttr = null;
  private SimpleAttributeSet infoTxtAttr = null;
  private SimpleAttributeSet msgTxtAttr = null;
  private SimpleAttributeSet buzzTxtAttr = null;
  private Document doc = null;

  BorderLayout borderLayout1 = new BorderLayout();
  JTextField tfMsg = new JTextField();
  JPanel jPanel4 = new JPanel();
  JLabel jLabel1 = new JLabel();
  BorderLayout borderLayout3 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  private JButton btnBuzz = new JButton();
  private JButton btnSend = new JButton();
  private FlowLayout flowLayout1 = new FlowLayout();

  private SimpleDateFormat sdf = null;
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JTextPane tpMsgs = new JTextPane();
  private JButton btnWhiteboard = new JButton();
  private StatusMenuBar statusbar = null;

  public ChatPanel()
  {
    this(null, null);
  }
  public ChatPanel(JabberService service, JabberId jid)
  {
    this.service = service;
    if (service != null)
    {
      service.addStatusListener(this);
    }
//    if (service != null) service.addMessageListener(this);
    this.jid = jid;
    if (jid != null) target = jid.toString(); //.getJID();
    sdf = new SimpleDateFormat("[HH:mm]");
    try
    {
      preJBInit();
      jbInit();
      postJBInit();

      btnWhiteboard.setVisible(false);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * postJBInit
   */
  private void postJBInit()
  {
    jPanel1.add(new JLabel("Status:"));
    statusbar = new StatusMenuBar(service, jid.getFullJID());
    jPanel1.add(statusbar);
  }

  private void jbInit() throws Exception
  {
    jPanel4.setLayout(borderLayout3);
    tfMsg.setText("");
    tfMsg.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfMsg_actionPerformed(e);
      }
    });
    tfMsg.setSelectionStart(11);
    this.setLayout(borderLayout1);
    jLabel1.setMaximumSize(new Dimension(41, 13));
    jLabel1.setText("Message:");
    borderLayout3.setHgap(5);
    btnBuzz.setMaximumSize(new Dimension(41, 19));
    btnBuzz.setMinimumSize(new Dimension(41, 19));
    btnBuzz.setPreferredSize(new Dimension(41, 19));
    btnBuzz.setMargin(new Insets(2, 2, 2, 2));
    btnBuzz.setText("Buzz");
    btnBuzz.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBuzz_actionPerformed(e);
      }
    });
    btnSend.setMaximumSize(new Dimension(41, 19));
    btnSend.setMinimumSize(new Dimension(41, 19));
    btnSend.setPreferredSize(new Dimension(41, 19));
    btnSend.setMargin(new Insets(2, 2, 2, 2));
    btnSend.setText("Send");
    btnSend.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnSend_actionPerformed(e);
      }
    });
    jPanel1.setLayout(flowLayout1);
    flowLayout1.setHgap(3);
    flowLayout1.setVgap(0);
    btnWhiteboard.setMaximumSize(new Dimension(71, 19));
    btnWhiteboard.setMinimumSize(new Dimension(71, 19));
    btnWhiteboard.setPreferredSize(new Dimension(71, 19));
    btnWhiteboard.setMargin(new Insets(2, 2, 2, 2));
    btnWhiteboard.setMnemonic('0');
    btnWhiteboard.setText("Whiteboard");
    btnWhiteboard.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnWhiteboard_actionPerformed(e);
      }
    });
    tpMsgs.setEditable(false);
    jPanel4.add(jLabel1, BorderLayout.WEST);
    jPanel4.add(tfMsg, BorderLayout.CENTER);
    jPanel4.add(jPanel1, BorderLayout.EAST);
    jPanel1.add(btnSend, null);
    jPanel1.add(btnBuzz, null);
    jPanel1.add(btnWhiteboard, null);
    this.add(jScrollPane2,  BorderLayout.CENTER);
    jScrollPane2.getViewport().add(tpMsgs, null);
    this.add(jPanel4, BorderLayout.SOUTH);
  }

  void preJBInit()
  {
    meTxtAttr = new SimpleAttributeSet();
    meTxtAttr.addAttribute(StyleConstants.Foreground, Color.blue);
    otherTxtAttr = new SimpleAttributeSet();
    otherTxtAttr.addAttribute(StyleConstants.Foreground, Color.red);
    infoTxtAttr = new SimpleAttributeSet();
    infoTxtAttr.addAttribute(StyleConstants.Foreground, Color.gray);
    msgTxtAttr = new SimpleAttributeSet();
    msgTxtAttr.addAttribute(StyleConstants.Foreground, Color.black);
    buzzTxtAttr = new SimpleAttributeSet();
    buzzTxtAttr.addAttribute(StyleConstants.Foreground, Color.red);
    StyleConstants.setBold(buzzTxtAttr, true);
    doc = tpMsgs.getDocument();
  }

//  public void addMessage(String msg)
//  {
//    messages.addElement(msg);
//    repaint();
//  }

/*
  void tfMsg2_actionPerformed(ActionEvent e)
  {
    String msg = tfMsg.getText();
    if (msg.equals("")) return;

//    try
//    {
      service.sendMessage(Message.CHAT, target, null, msg);
      messages.addElement("Me: " + msg);
//    }
//    catch (IOException ex)
//    {
//      ex.printStackTrace();
//    }

    tfMsg.setText("");
    repaint();
  }
*/

  public void setTarget(String target)
  {
    this.target = target;
  }

   public void received(Message msg)
  {
    if (msg.getType() != Message.CHAT) return;
    log.finest("ChatPanel(" + jid + ").received");
    JabberId from = new JabberId(msg.getFrom());
    if (!jid.equalFull(from)) return;
    log.finest("ChatPanel(" + jid + ").received adding msg: " + msg.getBody());
    if (msg instanceof BuzzStanza)
    {
      msg.setSubject("<Buzz>");
      msg.setBody("<Buzz>");
      com.borland.primetime.ide.Browser.getActiveBrowser().getStatusView().setText("Buzz from " + from, com.borland.primetime.ide.StatusView.TYPE_WARNING);
      com.borland.primetime.audio.AudioClipPlayer.playClipNamed(com.borland.jbuilder.audio.JBuilderAudio.KEY_BUILD_WARNINGS);
    }
    else if (msg instanceof WhiteBoardStanza)
    {
      btnWhiteboard_actionPerformed(null);
      whiteboard.received(msg);
    }
    else
    {
      com.borland.primetime.ide.Browser.getActiveBrowser().getStatusView().setText("Chat message from " + from + " : " + msg.getBody());
    }
//    messages.addElement(from.getUser() + ": " + msg.getBody());
    addMessage(from.getUser(), msg.getBody());
    repaint();
  }

  void tfMsg_actionPerformed(ActionEvent e)
  {
    String msg = tfMsg.getText();
    if (msg.equals("")) return;

//    try
//    {
      service.sendMessage(Message.CHAT, target, null, msg);
//      messages.addElement("Me: " + msg);
      addMessage("Me", msg);
//    }
//    catch (IOException ex)
//    {
//      ex.printStackTrace();
//    }

    tfMsg.setText("");
    repaint();
  }

  void btnSend_actionPerformed(ActionEvent e)
  {
    tfMsg_actionPerformed(e);
  }

  void btnBuzz_actionPerformed(ActionEvent e)
  {
    service.sendBuzz(Message.CHAT, target);
    addMessage("Me", "<Buzz>");
  }

  void addMessage(String from, String msg)
  {
    if (msg == null) return;
    if ((msg != null) && (msg.endsWith("\n"))) msg = msg.substring(0, msg.length() - 1);
    String now = sdf.format(new Date());
    String txt = now + " " + from + ": " + msg;
    try
    {
        int len = doc.getLength();
        int inserted = 0;
        doc.insertString(len, now + " ", infoTxtAttr);
        inserted = now.length() + 1;
        if (from.equals("Me"))
        {
          doc.insertString(len + inserted, from + ":", meTxtAttr);
        }
        else
        {
          doc.insertString(len + inserted, from + ":", otherTxtAttr);
        }
        inserted += from.length() + 1;
        if (msg == null)
        {

        }
        else if (msg.equals("<Buzz>"))
        {
          doc.insertString(len + inserted, msg + "\n", buzzTxtAttr);
        }
        else
        {
          doc.insertString(len + inserted, msg+"\n", msgTxtAttr);
        }
        if (msg != null) inserted += msg.length() + 1;
        tpMsgs.paintImmediately(tpMsgs.getBounds());
        tpMsgs.setCaretPosition(len + inserted);
    }
    catch (BadLocationException blex)
    {
      blex.printStackTrace();
    }
  }

  void btnWhiteboard_actionPerformed(ActionEvent e)
  {
    Browser browser = Browser.getActiveBrowser();
    try
    {
      if (whiteboard == null)
      {
        whiteboard = new WhiteBoardNode(browser.getActiveProject(), service, jid);
      }
      browser.setActiveNode(whiteboard, true);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void serviceStatusUpdate(int status)
  {
    switch (status)
    {
      case JabberService.READY:
        tfMsg.setEnabled(true);
        btnSend.setEnabled(true);
        btnBuzz.setEnabled(true);
        statusbar.setEnabled(true);
        break;
      case JabberService.DISCONNECTED:
        addMessage(null, "Disconnected");
        tfMsg.setEnabled(false);
        btnSend.setEnabled(false);
        btnBuzz.setEnabled(false);
        statusbar.setEnabled(false);
        break;
      default:
        tfMsg.setEnabled(false);
        btnSend.setEnabled(false);
        btnBuzz.setEnabled(false);
        statusbar.setEnabled(false);
    }
  }

}

