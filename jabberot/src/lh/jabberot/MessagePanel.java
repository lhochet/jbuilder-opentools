package lh.jabberot;

import java.text.*;
import java.util.*;
import java.util.logging.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.borland.primetime.ui.*;
import lh.jabber.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:25 $
 * @created 23/07/03
 */

public class MessagePanel extends JPanel implements MessageListener, MouseListener
{
  Logger log = Logger.getLogger(MessagePanel.class.getName());
  {
    log.setLevel(Level.OFF);
  }

  class MessagesModel extends AbstractTableModel
  {
    private Vector msgs = new Vector();

    public Message getMessage(int ind)
    {
      return (Message)msgs.elementAt(ind);
    }

    public MessagesModel()
    {
    }

    public int getColumnCount()
    {
      return 4;
    }
    public Object getValueAt(int y, int x)
    {
      Message msg = (Message)msgs.elementAt(y);
      switch (x)
      {
        case 0: return getIcon(msg.getType());
        case 1: return msg.getFrom();
        case 2: return msg.getSubject();
        case 3: return format(msg.getTime());
      }
      return new ArrayIndexOutOfBoundsException("Invalid column.");
    }
    public int getRowCount()
    {
      if (msgs == null) return 0;
      return msgs.size();
    }
    public String getColumnName(int column)
    {
      switch (column)
      {
        case 0: return "";
        case 1: return "From";
        case 2: return "Subject";
        case 3: return "Date";
      }
      return "";
    }

    public Class getColumnClass(int c)
    {
      switch (c)
      {
        case 0:
          return Icon.class;
        default:
          return String.class;
      }
//      if (getValueAt(0, c) == null)
//      {
//        return (new Object()).getClass();
//      }
//      return getValueAt(0, c).getClass();
    }

    public void addMessage(Message msg)
    {
      msgs.add(msg);
      fireTableRowsInserted(msgs.size()-1, msgs.size()-1);
    }

    public void removeRow(int i)
    {
      msgs.remove(i);
      fireTableRowsDeleted(i, i);
    }

    private String format(Date d)
    {
      if (d == null) return "";
      DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
      return df.format(d);
    }

    private Icon getIcon(int type)
    {
      Icon icon = com.borland.primetime.ide.BrowserIcons.ICON_BLANK;
      switch (type)
      {
        case Message.HEADLINE:
          icon = com.borland.primetime.ide.BrowserIcons.ICON_WHERE_AM_I;
          break;
      }
      return icon;
    }

  }



  private static int msgid = 0;
  private JabberService service;
  private MessagesModel messages = new MessagesModel();

  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane jsBody = new JScrollPane();
  private JScrollPane jsMessages = new JScrollPane();
  private JTable tblMessages = new JTable();
  private JTextArea taBody = new JTextArea();
  private JSplitPane jSplitPane1 = new JSplitPane();

  private int currow = -1;
  private JPopupMenu popup = new JPopupMenu();
  private JMenuItem mnuiDelete = new JMenuItem();
  private JMenuItem mnuiReply = new JMenuItem();

  public MessagePanel()
  {
    this(null);
  }
  public MessagePanel(JabberService service)
  {
    this.service = service;
//    if (service != null) service.addMessageListener(this);
    try
    {
      preJBInit();
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    taBody.setEnabled(true);
    taBody.setEditable(false);
    taBody.setText("");
    jSplitPane1.setResizeWeight(0.3);
    mnuiDelete.setText("Delete");
    mnuiDelete.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        mnuiDelete_actionPerformed(e);
      }
    });
    mnuiReply.setText("Reply...");
    mnuiReply.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        mnuiReply_actionPerformed(e);
      }
    });
    this.add(jSplitPane1,  BorderLayout.CENTER);
    jSplitPane1.add(jsMessages, JSplitPane.LEFT);
    jSplitPane1.add(jsBody, JSplitPane.RIGHT);
    jsBody.getViewport().add(taBody, null);
    jsMessages.getViewport().add(tblMessages, null);
    popup.add(mnuiReply);
    popup.add(mnuiDelete);
  }

  void preJBInit()
  {
   tblMessages.setModel(messages);
   tblMessages.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
   tblMessages.getColumnModel().getColumn(0).setPreferredWidth(32);
   tblMessages.getColumnModel().getColumn(0).setResizable(false);
   tblMessages.getColumnModel().getColumn(0).setMaxWidth(32);

   tblMessages.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
   ListSelectionModel msgSM = tblMessages.getSelectionModel();
   msgSM.addListSelectionListener(new ListSelectionListener() {
       public void valueChanged(ListSelectionEvent e) {
           //Ignore extra messages.
           if (e.getValueIsAdjusting()) return;

           ListSelectionModel lsm =
               (ListSelectionModel)e.getSource();
           if (lsm.isSelectionEmpty()) {
              taBody.setText("");
           } else {
               int selectedRow = lsm.getMinSelectionIndex();
               taBody.setText(messages.getMessage(selectedRow).getBody());
           }
       }
   });

   tblMessages.addMouseListener(this);
  }




   public void received(Message msg)
  {
    if ((msg.getType() != lh.jabber.stanza.Message.NORMAL)
        &&
        (msg.getType() != lh.jabber.stanza.Message.HEADLINE)
        &&
        (msg.getType() != lh.jabber.stanza.Message.ERROR)
        )
    {
      return;
    }
    log.finest("MessagePanel().received from: " + msg.getFrom() + ", adding msg: " + msg.getBody());
    if (msg instanceof BuzzStanza)
    {
      msg.setSubject("<Buzz>");
      msg.setBody("<Buzz>");
      com.borland.primetime.ide.Browser.getActiveBrowser().getStatusView().setText("Buzz from " + msg.getFrom(), com.borland.primetime.ide.StatusView.TYPE_WARNING);
    }
    else
    {
      com.borland.primetime.ide.Browser.getActiveBrowser().getStatusView().setText("Message from " + msg.getFrom() + " : " + msg.getSubject());
      messages.addMessage(msg);
    }
//    JabberId from = new JabberId(msg.getFrom());
//    if (!jid.equalFull(from)) return;

    repaint();
  }


  public void mouseClicked(MouseEvent e)
  {
    if ((e.getClickCount() == 1) && SwingUtilities.isRightMouseButton(e)) mouseRightClicked(e);
  }

  /** lh method */
  public void mouseRightClicked(MouseEvent e)
  {
    currow = tblMessages.rowAtPoint(e.getPoint());
    popup.show(this, e.getX(), e.getY());
  }

  /** not used */
  public void mousePressed(MouseEvent e){}
  /** not used */
  public void mouseReleased(MouseEvent e){}
  /** not used */
  public void mouseEntered(MouseEvent e){}
  /** not used */
  public void mouseExited(MouseEvent e){}

  void mnuiDelete_actionPerformed(ActionEvent e)
  {
    if (currow > -1)
    {
      messages.removeRow(currow);
      currow = -1;
    }
  }

  void mnuiReply_actionPerformed(ActionEvent e)
  {
    if (currow > -1)
    {
      Message msg = messages.getMessage(currow);
      SendMessagePanel panel = new SendMessagePanel();
      panel.setTo(msg.getFrom());
      panel.setSubject("Re: " + msg.getSubject());
      panel.setMessage(quote(msg.getBody()));
      if (DefaultDialog.showModalDialog(this, "Send message", panel, null))
      {
        service.sendMessage(Message.NORMAL, panel.getTo(), panel.getSubject(), panel.getMessage());
      }
    }
  }

  private String quote(String msg)
  {
    StringBuffer buf = new StringBuffer();
    StringTokenizer tk = new StringTokenizer(msg, "\n");
    while (tk.hasMoreTokens())
    {
      buf.append('>');
      buf.append(' ');
      buf.append(tk.nextToken());
      buf.append('\n');
    }
    buf.append('\n');
    return buf.toString();
  }

}

