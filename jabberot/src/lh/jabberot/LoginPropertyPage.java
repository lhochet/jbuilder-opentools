package lh.jabberot;

import com.borland.primetime.properties.*;
import com.borland.primetime.help.HelpTopic;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import lh.util.Password;
import com.borland.primetime.help.ZipHelpTopic;


/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:25 $
 * @created 0.1, 8/07/03
 */

public class LoginPropertyPage extends PropertyPage implements PropertyGroup
{

  static final GlobalProperty SERVER = new GlobalProperty(JabberClient.JABBER_CAT, "server", "localhost");
  static final GlobalIntegerProperty PORT = new GlobalIntegerProperty(JabberClient.JABBER_CAT, "port", 5222);
  static final GlobalProperty JID_USER = new GlobalProperty(JabberClient.JABBER_CAT, "jid_user", "username");
  static final GlobalProperty JID_DOMAIN = new GlobalProperty(JabberClient.JABBER_CAT, "jid_domain", "localhost");
  static final GlobalProperty JID_RESOURCE = new GlobalProperty(JabberClient.JABBER_CAT, "jid_resource", "JB");
  static final GlobalProperty JID_PASSWORD = new GlobalProperty(JabberClient.JABBER_CAT, "jid_password");

  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private TitledBorder titledBorder1;
  private TitledBorder titledBorder2;
  private JLabel jLabel1 = new JLabel();
  private JLabel jLabel2 = new JLabel();
  private JTextField tfHost = new JTextField();
  private JTextField tfPort = new JTextField();
  private JLabel jLabel3 = new JLabel();
  private JLabel jLabel4 = new JLabel();
  private JLabel jLabel5 = new JLabel();
  private JLabel jLabel6 = new JLabel();
  private JTextField tfUser = new JTextField();
  private JTextField tfDomain = new JTextField();
  private JTextField tfResource = new JTextField();
  private JPasswordField pfPassword = new JPasswordField();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private GridBagLayout gridBagLayout3 = new GridBagLayout();
  private JPanel jPanel3 = new JPanel();
  private JPanel jPanel4 = new JPanel();

  public void initializeProperties()
  {

  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == JabberClient.JABBER_TOPIC)
    {
      return new PropertyPageFactory("Login")
      {
        public PropertyPage createPropertyPage()
        {
          return new LoginPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

  public void writeProperties()
  {
    SERVER.setValue(tfHost.getText());
    PORT.setValue(tfPort.getText());
    JID_USER.setValue(tfUser.getText());
    JID_DOMAIN.setValue(tfDomain.getText());
    JID_RESOURCE.setValue(tfResource.getText());
    JID_PASSWORD.setValue(lh.util.Password.encode(new String(pfPassword.getPassword())));
  }
  public void readProperties()
  {
    tfHost.setText(SERVER.getValue());
    tfPort.setText(PORT.getValue());
    tfUser.setText(JID_USER.getValue());
    tfDomain.setText(JID_DOMAIN.getValue());
    tfResource.setText(JID_RESOURCE.getValue());
    String pwd = LoginPropertyPage.JID_PASSWORD.getValue();
    if (pwd == null)
    {
      pwd = "";
    }
    else
    {
      pwd = Password.decode(pwd);
    }
    pfPassword.setText(pwd);
  }
  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("/doc/LoginPropertyPage.html").toString());
  }

  public LoginPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(SystemColor.controlShadow,1),"Server:");
    titledBorder2 = new TitledBorder(BorderFactory.createLineBorder(SystemColor.controlShadow,1),"Jabber ID:");
    this.setLayout(gridBagLayout3);
    jPanel1.setBorder(titledBorder1);
    jPanel1.setLayout(gridBagLayout1);
    jPanel2.setBorder(titledBorder2);
    jPanel2.setLayout(gridBagLayout2);
    jLabel1.setText("Host:");
    jLabel2.setToolTipText("");
    jLabel2.setText("Port:");
    tfHost.setText("localhost");
    tfPort.setText("5222");
    jLabel3.setText("User:");
    jLabel4.setText("Domain:");
    jLabel5.setText("Resource:");
    jLabel6.setText("Password:");
    tfUser.setSelectionStart(11);
    tfUser.setText("username");
    tfDomain.setText("localhost");
    tfResource.setText("JB");
    pfPassword.setText("jPasswordField1");
    this.add(jPanel1,  new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 11, 0, 13), 124, 17));
    jPanel1.add(jLabel1,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jLabel2,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(tfHost,       new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 150, 0));
    jPanel1.add(tfPort,   new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 27, 0));
    jPanel1.add(jPanel3,  new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    this.add(jPanel2,  new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(33, 11, 32, 13), -12, 18));
    jPanel2.add(jLabel3,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(tfUser,         new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 100), 150, 0));
    jPanel2.add(tfDomain,     new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 100), 150, 0));
    jPanel2.add(jLabel4,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(jLabel6,    new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 1, 0));
    jPanel2.add(jLabel5,   new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    jPanel2.add(tfResource,    new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 100), 150, 0));
    jPanel2.add(pfPassword,    new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 100), 150, 0));
    jPanel2.add(jPanel4,   new GridBagConstraints(0, 4, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
  }
}
