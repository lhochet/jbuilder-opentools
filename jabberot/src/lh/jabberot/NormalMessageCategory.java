package lh.jabberot;

import com.borland.primetime.ide.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 23/07/03
 */

public class NormalMessageCategory extends MessageCategory
{
  private JabberService service = null;
  private MessagePanel panel = null;
  private boolean isshown = false;

  public NormalMessageCategory(JabberService service)
  {
    super("Jabber Messages");
    this.service = service;
    panel = new MessagePanel(service);
  }

  public MessagePanel getPanel()
  {
    return panel;
  }

  public boolean isShown()
  {
    return isshown;
  }
  public void setShown(boolean isshown)
  {
    this.isshown = isshown;
  }

  public void categoryClosing() throws com.borland.primetime.util.VetoException
  {
    super.categoryClosing();
    isshown = false;
  }
}