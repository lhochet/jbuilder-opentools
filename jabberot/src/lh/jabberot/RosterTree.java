package lh.jabberot;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;

import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;
import com.borland.primetime.ui.*;
import lh.jabber.*;
import lh.jabber.stanza.*;
import com.borland.primetime.ide.Browser;
import java.net.*;
import java.io.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:27 $
 * @created 30/12/02
 */

public class RosterTree extends JTree implements MouseListener
{

  private RosterElement currentitem = null;
  private JabberService service = null;
  private RosterTreeModel rostertreemodel = null;
  private JMenuItem jMenuItem1 = new JMenuItem();
  private JMenuItem jMenuItem3 = new JMenuItem();
  private JPopupMenu popupContact = new JPopupMenu();
  private JMenuItem jMenuItem2 = new JMenuItem();
  private JMenuItem jMenuItem4 = new JMenuItem();
  private JMenu jMenu2 = new JMenu();
  private JMenuItem jMenuItem5 = new JMenuItem();
  private StatusMenuBar jRosterItemStatusMenuBar;
  private JMenuItem jMenuItem6 = new JMenuItem();
  private JMenuItem jMenuItem7 = new JMenuItem();

  public RosterTree()
  {
    this(null);
  }

  public RosterTree(JabberService service)
  {
    super(new RosterTreeModel(service));
    this.service = service;
    rostertreemodel = (RosterTreeModel)getModel();
    rostertreemodel.setTree(this);
    getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    //Enable tool tips.
//   ToolTipManager.sharedInstance().registerComponent(this);

    setCellRenderer(new RosterTreeRenderer());

    addMouseListener(this);

    try
    {
      jbInit();
      postJBInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * postJBInit
   */
  private void postJBInit()
  {
    popupContact.addSeparator();
    jRosterItemStatusMenuBar = new StatusMenuBar(service, null, true, false);
    popupContact.add(jRosterItemStatusMenuBar.getMenu());
    this.setShowsRootHandles(false);
//    this.setShowsRootHandles(true);
    this.setRootVisible(false);
    this.expandRow(0);
    this.expandRow(1);

  }

  public void mouseClicked(MouseEvent e)
  {
    if ((e.getClickCount() == 1) && SwingUtilities.isRightMouseButton(e)) mouseRightClicked(e);
  }

  /** lh method */
  public void mouseRightClicked(MouseEvent e)
  {
    TreePath path = getPathForLocation(e.getX(), e.getY());
    if (path == null) return;
    setSelectionPath(path);

    Object node = path.getLastPathComponent();
    if (node == null) return;
    if (!(node instanceof RosterElement)) return;

    currentitem = (RosterElement)node;

    if (currentitem instanceof RosterItem)
    {
      jMenuItem7.setVisible(true);
      jMenuItem7.setEnabled(true);
    }
    else
    {
      jMenuItem7.setVisible(false);
      jMenuItem7.setEnabled(false);
    }

    jRosterItemStatusMenuBar.setTarget(currentitem.getJid().getFullJID());
    Presence sentPresence = currentitem.getSentPresence();
    if (sentPresence == null) sentPresence = service.getPresence();
    jRosterItemStatusMenuBar.updatePresence(sentPresence);

    popupContact.show(this, e.getX(), e.getY());
  }

  /** not used */
  public void mousePressed(MouseEvent e){}
  /** not used */
  public void mouseReleased(MouseEvent e){}
  /** not used */
  public void mouseEntered(MouseEvent e){}
  /** not used */
  public void mouseExited(MouseEvent e){}

  private void jbInit() throws Exception
  {
    jMenuItem3.setText("Version");
    jMenuItem3.addActionListener(new RosterTree_jMenuItem3_actionAdapter(this));
    jMenuItem1.setText("Send Message...");
    jMenuItem1.addActionListener(new RosterTree_jMenuItem1_actionAdapter(this));
    jMenuItem2.setText("Start chat...");
    jMenuItem2.addActionListener(new RosterTree_jMenuItem2_actionAdapter(this));
    jMenuItem4.setText("Time");
    jMenuItem4.addActionListener(new RosterTree_jMenuItem4_actionAdapter(this));
    jMenu2.setText("Info");
    jMenuItem5.setText("Last Activity");
    jMenuItem5.addActionListener(new RosterTree_jMenuItem5_actionAdapter(this));
    jMenuItem6.setText("Send file...");
    jMenuItem6.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuItem6_actionPerformed(e);
      }
    });
    jMenuItem7.setText("Remove...");
    jMenuItem7.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jMenuItem7_actionPerformed(e);
      }
    });
    popupContact.add(jMenuItem1);
    popupContact.add(jMenuItem2);
    popupContact.add(jMenuItem6);
    popupContact.addSeparator();
    popupContact.addSeparator();
    popupContact.add(jMenu2);
    popupContact.addSeparator();
    popupContact.add(jMenuItem7);
    jMenu2.add(jMenuItem3);
    jMenu2.add(jMenuItem4);
    jMenu2.add(jMenuItem5);
  }

  void jMenuItem1_actionPerformed(ActionEvent e)
  {
    if (currentitem == null) return;
    SendMessagePanel panel = new SendMessagePanel();
    panel.setTo(currentitem.getJid().getFullJID());
    if (DefaultDialog.showModalDialog(this, "Send message", panel, null))
    {
      service.sendMessage(Message.NORMAL, panel.getTo(), panel.getSubject(), panel.getMessage());
    }
  }

  void jMenuItem2_actionPerformed(ActionEvent e)
  {
    if (currentitem == null) return;
    //start chat
    ChatManager.getInstance().addChat(currentitem.getJid());
  }

  void jMenuItem3_actionPerformed(ActionEvent e)
  {
    if (currentitem == null) return;
    // ask version
    service.requestVersion(currentitem.getJid());
  }

  void jMenuItem4_actionPerformed(ActionEvent e)
  {
    if (currentitem == null) return;
    // ask time
    service.requestTime(currentitem.getJid());
  }

  void jMenuItem5_actionPerformed(ActionEvent e)
  {
    if (currentitem == null) return;
    // ask last activity
    service.requestLastActivity(currentitem.getJid());
  }

  void jMenuItem6_actionPerformed(ActionEvent e)
  {
    Thread t = new Thread()
    {
      public void run()
      {
        String urls = null;
        String desc = "";
        String to = null;
        Url startdir = null;
        try
        {
          startdir = new Url(FileTransferPropertyPage.DOWNLOAD_PATH.getValue());
        }
        catch (Exception ex)
        {
          // do nothing
        }

        Url url = UrlChooser.promptForUrl(Browser.getActiveBrowser(),
                                          startdir
                                          , "Select file to transfer...");
        if (url == null)
        {
          return;
        }

        desc = JOptionPane.showInputDialog(Browser.getActiveBrowser(), "Description:");
        to = currentitem.getJid().getFullJID();
        File file = url.getFileObject();

        try
        {
          int port = FileTransferPropertyPage.UPLOAD_PORT.getInteger();
          int delay = FileTransferPropertyPage.UPLOAD_DELAY.getInteger();
          FileSenderThread fst = new FileSenderThread(port, file, delay);
          fst.start();

          urls = "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port + "/" + file.getName();

          String msg = "<url>" + urls + "</url><desc>" + desc + "</desc>";
          service.sendExtendedIQ(IQ.SET, to, "jabber:iq:oob", msg, null);
        }
        catch (IOException ex1)
        {
          ex1.printStackTrace();
        }
      }
    };
    t.start();
  }

  void jMenuItem7_actionPerformed(ActionEvent e)
  {
    String msg = "Remove '" + currentitem.getJid().getJID() + "' from your Roster?";
    if (JOptionPane.showConfirmDialog(this, msg, "Remove Contact", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
    {
      service.removeRosterItem((RosterItem)currentitem);
    }
  }

}

class RosterTree_jMenuItem1_actionAdapter implements java.awt.event.ActionListener
{
  RosterTree adaptee;

  RosterTree_jMenuItem1_actionAdapter(RosterTree adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuItem1_actionPerformed(e);
  }
}

class RosterTree_jMenuItem2_actionAdapter implements java.awt.event.ActionListener
{
  RosterTree adaptee;

  RosterTree_jMenuItem2_actionAdapter(RosterTree adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuItem2_actionPerformed(e);
  }
}

class RosterTree_jMenuItem3_actionAdapter implements java.awt.event.ActionListener
{
  RosterTree adaptee;

  RosterTree_jMenuItem3_actionAdapter(RosterTree adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuItem3_actionPerformed(e);
  }
}

class RosterTree_jMenuItem4_actionAdapter implements java.awt.event.ActionListener
{
  RosterTree adaptee;

  RosterTree_jMenuItem4_actionAdapter(RosterTree adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuItem4_actionPerformed(e);
  }
}

class RosterTree_jMenuItem5_actionAdapter implements java.awt.event.ActionListener
{
  private RosterTree adaptee;

  RosterTree_jMenuItem5_actionAdapter(RosterTree adaptee)
  {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuItem5_actionPerformed(e);
  }
}

class FileSenderThread extends Thread
{
  private ServerSocket serverSocket = null;
  private File file = null;
  private int delay = 30000; //milliseconds
//  private Timer timer = null;

  FileSenderThread(int port, File file, int delay) throws IOException
  {
    super("JabberFileSender");
    this.file = file;
    if (delay > 0) this.delay = delay;

    serverSocket = new ServerSocket(port);

  }

  public void run()
  {
    Socket clientSocket = null;

    ActionListener taskPerformer = new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        try
        {
          serverSocket.close();
        }
        catch (IOException ex)
        {
          // ignore, as we reset to null
        }
        serverSocket = null;
      }
    };
    Timer timer = new Timer(delay, taskPerformer);
    timer.setRepeats(false);
    timer.start();

    try
    {
      clientSocket = serverSocket.accept();
      timer.stop();
      timer = null;
      BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
//      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("C:\\Documents and Settings\\Ludovic\\Desktop\\jbtst.txt"));
      BufferedOutputStream bos = new BufferedOutputStream(clientSocket.getOutputStream());

      bos.write("HTTP/1.1 200 OK\r\n".getBytes());
      bos.write("Server: JB-Jabber-FileTransfer/1.0\r\n".getBytes());
      bos.write(("Content-length: " + file.length() + "\r\n").getBytes());
      bos.write("Content-type: application/octet-stream\r\n".getBytes());
      bos.write("Connection: close\r\n\r\n".getBytes());


      int csz = 1024; //conn.getContentLength() < 1 ? 50000 : conn.getContentLength();
      byte[] content = new byte[csz];
      int len = 0;
//      int tot = 0;
      while ((len = bis.read(content/*, len, csz - tot*/)) > 0)
      {
        bos.write(content, 0, len);
//        System.out.println("wrote len = "+len);
//        tot += len;
      }
      bis.close();
      bos.flush();
      bos.close();
      clientSocket.close();
      Browser.getActiveBrowser().getStatusView().setText("Transfer of '" + file.getName() + "' finished.");
    }
    catch (Exception ex)
    {
      Browser.getActiveBrowser().getStatusView().setErrorText("Failed to transfer '" + file.getName() + "'.");
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        if (serverSocket != null) serverSocket.close();
      }
      catch (IOException ex1)
      {
        // do nothing
      }
      serverSocket = null;
    }
  }
}

