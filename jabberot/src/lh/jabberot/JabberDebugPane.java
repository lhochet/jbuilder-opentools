package lh.jabberot;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import lh.jabber.*;
import java.util.logging.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:23 $
 * @created 13/07/03
 */

public class JabberDebugPane extends JPanel
{
  Logger log = Logger.getLogger(JabberDebugPane.class.getName());

  private JabberService service = null;

  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JabberDebugTextPane tp = new JabberDebugTextPane();
  private JPanel jPanelDebugInput = new JPanel();
  private JTextField tfDebugInput = new JTextField();
  private BorderLayout borderLayout2 = new BorderLayout();
  public JabberDebugPane()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    jPanelDebugInput.setLayout(borderLayout2);
    tfDebugInput.setText("");
    tfDebugInput.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfDebugInput_actionPerformed(e);
      }
    });
    this.add(jScrollPane1, BorderLayout.CENTER);
    this.add(jPanelDebugInput,  BorderLayout.SOUTH);
    jPanelDebugInput.add(tfDebugInput, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(tp, null);
  }

  JabberDebugTextPane getDebugTextPane()
  {
    return tp;
  }

  void tfDebugInput_actionPerformed(ActionEvent e)
  {
    if (service == null)
    {
      tp.appendErr("JabberDebugPane: No service found. unable to send debug stanza");
      log.warning("JabberDebugPane: No service found. unable to send debug stanza");
    }
    else
    {
      service.sendStanza(tfDebugInput.getText());
    }
  }

  /**
   * setService
   *
   * @param service JabberService
   */
  public void setService(JabberService service)
  {
    this.service = service;
  }

}
