package lh.jabberot;

import lh.jabber.*;
import com.borland.primetime.ide.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 17/07/03
 */

public class ChatMessageCategory extends MessageCategory
{
  private JabberService service = null;
  private JabberId jid = null;
  private ChatPanel panel = null;
  private boolean isshown = false;

  public ChatMessageCategory(JabberService service, JabberId jid)
  {
    super(jid.toString() + " - Chat");
    this.service = service;
    this.jid = jid;
    panel = new ChatPanel(service, jid);
  }

  public ChatPanel getPanel()
  {
    return panel;
  }

  public boolean isShown()
  {
    return isshown;
  }
  public void setShown(boolean isshown)
  {
    this.isshown = isshown;
  }

  public void categoryClosing() throws com.borland.primetime.util.VetoException
  {
    super.categoryClosing();
    isshown = false;
  }

}