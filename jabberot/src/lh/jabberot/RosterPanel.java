package lh.jabberot;

import java.awt.*;
import javax.swing.*;

import lh.jabber.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:27 $
 * @created 14/01/03
 */

public class RosterPanel extends JPanel implements ServiceStatusListener
{
  private JabberService service;

  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private RosterTree tRosterTree; // = new RosterTree();
  private StatusMenuBar statusMenuBar = null;

  private JPanel jPanel1 = new JPanel();
  private JLabel jLabel1 = new JLabel();
  public RosterPanel()
  {
    this(null);
  }
  public RosterPanel(JabberService service)
  {
    this.service = service;

    service.addStatusListener(this);

    try
    {
      preJBInit();
      jbInit();
      postJBInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * postJBInit
   */
  private void postJBInit()
  {
    statusMenuBar = new StatusMenuBar(service, null);
    jPanel1.add(statusMenuBar);
    statusMenuBar.setEnabled(false);
    jPanel1.setEnabled(false);
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    jLabel1.setText("Status:");
    this.add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(tRosterTree, null);
    this.add(jPanel1, java.awt.BorderLayout.EAST);
    jPanel1.add(jLabel1);
  }
  private void preJBInit()
  {
    tRosterTree = new RosterTree(service);
  }

  public void serviceStatusUpdate(int status)
  {
    switch (status)
    {
      case JabberService.READY:
        statusMenuBar.setEnabled(true);
        jPanel1.setEnabled(true);
        break;
      default:
        statusMenuBar.setEnabled(false);
        jPanel1.setEnabled(false);
    }
  }
}

