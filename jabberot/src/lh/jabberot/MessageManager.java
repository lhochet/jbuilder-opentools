package lh.jabberot;

import lh.jabber.*;
import java.util.logging.*;
import java.util.*;
import com.borland.primetime.ide.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:25 $
 * @created 23/07/03
 */

public class MessageManager implements MessageListener, ClientsInfoListener
{
  Logger log = Logger.getLogger(MessageManager.class.getName());
  {
    log.setLevel(Level.OFF);
  }

  private static MessageManager instance = new MessageManager();
  public static MessageManager getInstance() { return instance; }

  private NormalMessageCategory cat = null;

  public MessageManager()
  {
  }

  public void received(lh.jabber.stanza.Message msg)
 {
   log.finest("MessageManager(type:" + msg.getType() + ").received");
   if ((msg.getType() != lh.jabber.stanza.Message.NORMAL)
       &&
       (msg.getType() != lh.jabber.stanza.Message.ERROR)
       )
   {
     return;
   }
   String sfrom = msg.getFrom();
   JabberId from = null;
   log.finest("MessageManager(from:" + sfrom +").received");


   if (sfrom != null)
   {
     from = new JabberId(sfrom);
     // no need add ourself
     if (from.equalFull(JabberClient.service.getJabberId()))
     {
       return;
     }
   }

   if (cat == null) cat = new NormalMessageCategory(JabberClient.service);
   MessagePanel p = cat.getPanel();
   Browser.getActiveBrowser().getMessageView().addCustomTab(cat, p);
   p.received(msg);
 }
  public void receivedVersion(String jid, ClientVersion cv)
  {
    lh.jabber.stanza.Message msg = new lh.jabber.stanza.Message();
    msg.setType(lh.jabber.stanza.Message.HEADLINE);
    msg.setFrom(jid);
    msg.setSubject("Version Request Answer");
    String body = jid + " is using:\n";
    body += "Client: " + cv.name + "\n";
    body += "Version: " + cv.version + "\n";
    body += "OS: " + cv.os + "\n";
    msg.setBody(body);

    if (cat == null) cat = new NormalMessageCategory(JabberClient.service);
    MessagePanel p = cat.getPanel();
    Browser.getActiveBrowser().getMessageView().addCustomTab(cat, p);
    p.received(msg);

  }
  public void receivedTime(String jid, ClientTime ct)
  {
    lh.jabber.stanza.Message msg = new lh.jabber.stanza.Message();
    msg.setType(lh.jabber.stanza.Message.HEADLINE);
    msg.setFrom(jid);
    msg.setSubject("Time Request Answer");
    String body = jid + " time is :\n";
    body += "" + ct.display + "\n";
    body += "Time zone: " + ct.tz + "\n";
    body += "UTC: " + ct.utc + "\n";
    msg.setBody(body);

    if (cat == null) cat = new NormalMessageCategory(JabberClient.service);
    MessagePanel p = cat.getPanel();
    Browser.getActiveBrowser().getMessageView().addCustomTab(cat, p);
    p.received(msg);

  }


}

