package lh.jabberot;

import javax.swing.JPanel;
import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 14/07/03
 */

public class SendMessagePanel extends JPanel
{
  FlowLayout flowLayout1 = new FlowLayout();
  JLabel jLabel1 = new JLabel();
  JTextField tfTo = new JTextField();
  JLabel jLabel2 = new JLabel();
  JTextField tfSubject = new JTextField();
  JLabel jLabel3 = new JLabel();
  JTextArea taMessage = new JTextArea();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JScrollPane jsMessage = new JScrollPane();

  public SendMessagePanel()
  {
    try
    {
      jbInit();
      setSize(400, 300);
      validate();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    setLayout(gridBagLayout1);
    jLabel1.setText("To:");
    jLabel2.setText("Subject:");
    jLabel3.setText("Message:");
    taMessage.setMinimumSize(new Dimension(0, 15));
    taMessage.setPreferredSize(new Dimension(300, 200));
    taMessage.setText("");
    taMessage.setLineWrap(true);
    taMessage.setWrapStyleWord(true);
    tfSubject.setText("");
    add(jLabel1,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    add(tfTo,   new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 124), 0, 0));
    add(tfSubject,   new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    add(jLabel2,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    add(jLabel3,   new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    add(jsMessage,   new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    jsMessage.getViewport().add(taMessage);
  }

  public String getTo()
  {
    return tfTo.getText();
  }

  public String getSubject()
  {
    return tfSubject.getText();
  }
  public String getMessage()
  {
    return taMessage.getText();
  }

  public void setTo(String txt)
  {
    tfTo.setText(txt);
  }

  public void setSubject(String subject)
  {
    tfSubject.setText(subject);
  }

  public void setMessage(String msg)
  {
    taMessage.setText(msg);
  }

}
