package lh.jabberot;

import javax.swing.text.*;
import java.awt.*;
import javax.swing.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 13/07/03
 */

public class JabberDebugTextPane extends JTextPane
{
  SimpleAttributeSet inTxtAttr = null;
  SimpleAttributeSet outTxtAttr = null;
  SimpleAttributeSet errTxtAttr = null;
  Document doc = null;

  public JabberDebugTextPane()
  {
    setEditable(false);
    inTxtAttr = new SimpleAttributeSet();
    inTxtAttr.addAttribute(StyleConstants.Foreground, Color.black);
    outTxtAttr = new SimpleAttributeSet();
    outTxtAttr.addAttribute(StyleConstants.Foreground, Color.blue);
    errTxtAttr = new SimpleAttributeSet();
    errTxtAttr.addAttribute(StyleConstants.Foreground, Color.red);
    doc = getDocument();
  }


  synchronized public void appendIn(String txt)
  {
      try
      {
        int len2 = doc.getLength();
        doc.insertString(doc.getLength(), txt, inTxtAttr);
        paintImmediately(getBounds());
        setCaretPosition(len2 + txt.length());
      }
      catch (BadLocationException blex)
      {
        blex.printStackTrace();
      }
  }

  synchronized public void appendOut(String txt)
  {
      try
      {
        int len2 = doc.getLength();
        doc.insertString(doc.getLength(), txt, outTxtAttr);
        paintImmediately(getBounds());
        setCaretPosition(len2 + txt.length());
      }
      catch (BadLocationException blex)
      {
        blex.printStackTrace();
      }
  }
  synchronized public void appendErr(String txt)
  {
      try
      {
        int len2 = doc.getLength();
        doc.insertString(doc.getLength(), txt, errTxtAttr);
        paintImmediately(getBounds());
        setCaretPosition(len2 + txt.length());
      }
      catch (BadLocationException blex)
      {
        blex.printStackTrace();
      }
  }



}
