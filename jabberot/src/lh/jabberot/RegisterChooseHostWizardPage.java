package lh.jabberot;

import com.borland.primetime.wizard.BasicWizardPage;
import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:26 $
 * @created 19/08/03
 */

public class RegisterChooseHostWizardPage extends BasicWizardPage
{
  private JComboBox cbHost = new JComboBox();
  private JLabel jLabel1 = new JLabel();
  private FlowLayout flowLayout1 = new FlowLayout();
  public RegisterChooseHostWizardPage()
  {
    try
    {
      preJBInit();
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * preJBInit
   */
  private void preJBInit()
  {
    cbHost.addItem("localhost");
    cbHost.addItem("jabber.org");
    cbHost.addItem("jabber.com");
  }

  private void jbInit() throws Exception
  {
    this.setInstructions("Select the Jabber server to register with");
    this.setPageTitle("Jabber server");
    this.setLargeIcon(null);
    this.setLayout(flowLayout1);
    jLabel1.setText("Server:");
    flowLayout1.setAlignment(FlowLayout.LEFT);
    cbHost.setEditable(true);
    this.add(jLabel1, null);
    this.add(cbHost, null);
  }

  public String getServer()
  {
    return (String)cbHost.getSelectedItem();
  }

}
