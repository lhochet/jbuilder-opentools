package lh.jabberot;

import com.borland.primetime.properties.*;
import javax.swing.*;
import java.awt.*;
import com.borland.primetime.help.*;
import java.awt.event.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;
import com.borland.primetime.ide.*;
import com.borland.jbcl.layout.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:21 $
 * @created 16/11/03
 */

public class FileTransferPropertyPage extends PropertyPage implements PropertyGroup
{
  static final GlobalProperty DOWNLOAD_PATH = new GlobalProperty(JabberClient.JABBER_CAT,
      "download_path");
  static final GlobalIntegerProperty UPLOAD_PORT = new GlobalIntegerProperty(JabberClient.JABBER_CAT,
      "upload_port", 8079);
  static final GlobalIntegerProperty UPLOAD_DELAY = new GlobalIntegerProperty(JabberClient.JABBER_CAT,
      "upload_delay", 30000);

  private JLabel jLabel1 = new JLabel();
  private JTextField tfPath = new JTextField();
  private JButton btnSetPath = new JButton();
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private VerticalFlowLayout verticalFlowLayout1 = new VerticalFlowLayout();
  private JLabel jLabel2 = new JLabel();
  private FlowLayout flowLayout1 = new FlowLayout();
  private FlowLayout flowLayout2 = new FlowLayout();
  private JTextField tfPort = new JTextField();
  private FlowLayout flowLayout3 = new FlowLayout();
  private JTextField tfDelay = new JTextField();
  private JLabel jLabel3 = new JLabel();
  private JPanel jPanel3 = new JPanel();
  private JLabel jLabel4 = new JLabel();
  public FileTransferPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void readProperties()
  {
    tfPath.setText(DOWNLOAD_PATH.getValue());
    tfPort.setText(UPLOAD_PORT.getValue());
    tfDelay.setText(UPLOAD_DELAY.getValue());
  }

  public void writeProperties()
  {
    DOWNLOAD_PATH.setValue(tfPath.getText());
    UPLOAD_PORT.setValue(tfPort.getText());
    UPLOAD_DELAY.setValue(tfDelay.getText());
  }

  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("/doc/FileTransferPropertyPage.html").toString());
  }

  public void initializeProperties()
  {

  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == JabberClient.JABBER_TOPIC)
    {
      return new PropertyPageFactory("File Transfer")
      {
        public PropertyPage createPropertyPage()
        {
          return new FileTransferPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

  private void jbInit() throws Exception
  {
    jLabel1.setText("Download Directory:");
    this.setLayout(verticalFlowLayout1);
    tfPath.setPreferredSize(new Dimension(255, 19));
    tfPath.setText("jTextField1");
    btnSetPath.setMaximumSize(new Dimension(19, 19));
    btnSetPath.setMinimumSize(new Dimension(19, 19));
    btnSetPath.setPreferredSize(new Dimension(19, 19));
    btnSetPath.setMargin(new Insets(2, 2, 2, 2));
    btnSetPath.setText("...");
    btnSetPath.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnSetPath_actionPerformed(e);
      }
    });
    jLabel2.setText("Sending port:");
    jPanel1.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    jPanel2.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    tfPort.setMinimumSize(new Dimension(48, 19));
    tfPort.setPreferredSize(new Dimension(48, 19));
    tfPort.setText("5280");
    flowLayout3.setAlignment(FlowLayout.LEFT);
    tfDelay.setMinimumSize(new Dimension(48, 19));
    tfDelay.setPreferredSize(new Dimension(48, 19));
    tfDelay.setText("25000");
    jLabel3.setText("Delay before aborting outgoing transfer:");
    jPanel3.setLayout(flowLayout3);
    jLabel4.setText("ms.");
    this.add(jPanel1, null);
    jPanel1.add(jLabel1, null);
    jPanel1.add(tfPath, null);
    jPanel1.add(btnSetPath, null);
    this.add(jPanel2, null);
    jPanel2.add(jLabel2, null);
    jPanel2.add(tfPort, null);
    this.add(jPanel3, null);
    jPanel3.add(jLabel3, null);
    jPanel3.add(tfDelay, null);
    jPanel3.add(jLabel4, null);
  }

  void btnSetPath_actionPerformed(ActionEvent e)
  {
    Url startdir = null;
    try
    {
      startdir = new Url(tfPath.getText());
    }
    catch (Exception ex)
    {
      // do nothing
    }

    Url url = UrlChooser.promptForDir(Browser.getActiveBrowser(),
                                      startdir
                                      /*null*/
                                      /*project.getUrl()*/
                                      , "Choose Default Download directory...");
    if (url != null)
    {
      tfPath.setText(url.getFullName());
    }
  }

}
