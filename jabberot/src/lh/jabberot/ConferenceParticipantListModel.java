package lh.jabberot;

import javax.swing.*;

import lh.jabber.stanza.*;
import java.util.*;
import lh.jabber.*;
import java.text.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:21 $
 * @created 21/09/03
 */

public class ConferenceParticipantListModel extends AbstractListModel
{
  Map map = null; //new TreeMap();
  Object[] values = null;

  public ConferenceParticipantListModel()
  {
    Collator c = Collator.getInstance();
    map = new TreeMap(c);
  }
  public Object getElementAt(int index)
  {
    if (values == null) return null;
    return values[index];
  }
  public int getSize()
  {
    if (values == null) return 0;
    return values.length;
  }

  public void addParticipant(Presence presence)
  {
    String nick = new JabberId(presence.getFrom()).getResource();
    map.put(nick, presence);
    values = map.values().toArray();
    fireContentsChanged(this, 0, values.length);
  }

  public void removeParticipant(Presence presence)
  {
    String nick = new JabberId(presence.getFrom()).getResource();
    map.remove(nick);
    values = map.values().toArray();
    fireContentsChanged(this, 0, values.length);
  }

}