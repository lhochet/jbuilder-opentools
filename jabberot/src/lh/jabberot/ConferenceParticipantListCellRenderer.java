package lh.jabberot;

import javax.swing.ListCellRenderer;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.*;
import lh.jabber.stanza.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:21 $
 * @created 21/09/03
 */

public class ConferenceParticipantListCellRenderer extends DefaultListCellRenderer
{
  public ConferenceParticipantListCellRenderer()
  {
  }
  public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
  {
    if (isSelected)
    {
      setBackground(list.getSelectionBackground());
      setForeground(list.getSelectionForeground());
    }
    else
    {
      setBackground(list.getBackground());
      setForeground(list.getForeground());
    }

    if (value instanceof Presence)
    {
      StringBuffer buf = new StringBuffer();
      Presence presence = (Presence)value;
      String nick = new JabberId(presence.getFrom()).getResource();
      String status = presence.getStatus();
      if (status == null) status = "Available";
      if (status.equals("")) status = "Available";
      String show = presence.getShow();
      if (show == null) show = "Available";
      if (show.equals("")) show = "Available";

      setIcon(StatusIcons.getInstance().getIcon(StatusIcons.ONLINE));
      if (show.equals("away")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.AWAY));
      else if (show.equals("xa")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.XA));
      else if (show.equals("dnd")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.DND));
      else if (show.equals("chat")) setIcon(StatusIcons.getInstance().getIcon(StatusIcons.CHAT));

      buf.append(nick);
      buf.append(" [");
      buf.append(show);
      buf.append("]");
      setText(buf.toString());
      setToolTipText(status);
      revalidate();
    }
    return this; //super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
  }

}

