package lh.jabberot;

import java.awt.*;
import javax.swing.*;

import com.borland.primetime.properties.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:19 $
 * @created 18/09/03
 */

public class ConferenceLoginPanel extends JPanel
{
  static final GlobalProperty ROOM_NAME = new GlobalProperty(JabberClient.JABBER_CAT, "room_name", "Jabber");
  static final GlobalProperty ROOM_SERVER = new GlobalProperty(JabberClient.JABBER_CAT, "room_server", "conference.jabber.org");
  static final GlobalProperty ROOM_NICK = new GlobalProperty(JabberClient.JABBER_CAT, "room_nick", "");
  static final GlobalProperty ROOM_PWD = new GlobalProperty(JabberClient.JABBER_CAT, "room_pwd", "");

  private JLabel jLabel1 = new JLabel();
  private JLabel jLabel2 = new JLabel();
  private JLabel jLabel3 = new JLabel();
  private JLabel jLabel4 = new JLabel();
  private JTextField tfRoomName = new JTextField();
  private JTextField tfRoomServer = new JTextField();
  private JTextField tfNickname = new JTextField();
  private JTextField tfPassword = new JTextField();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  public ConferenceLoginPanel()
  {
    try
    {
      jbInit();
      postJBInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * postJBInit
   */
  private void postJBInit()
  {
    tfRoomName.setText(ROOM_NAME.getValue());
    tfRoomServer.setText(ROOM_SERVER.getValue());
    tfNickname.setText(ROOM_NICK.getValue());
    tfPassword.setText(ROOM_PWD.getValue());
  }

  private void jbInit() throws Exception
  {
    jLabel1.setText("Room name:");
    this.setLayout(gridBagLayout1);
    jLabel2.setText("Room server:");
    jLabel3.setText("Nickname:");
    jLabel4.setText("Password:");
    tfPassword.setEnabled(false);
    this.add(jLabel1,   new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(14, 15, 0, 0), 8, 0));
    this.add(tfRoomName,   new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(8, 7, 0, 193), 74, 0));
    this.add(tfRoomServer,   new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(7, 6, 0, 100), 95, 0));
    this.add(jLabel2,   new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 15, 0, 0), 9, 0));
    this.add(jLabel4,   new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(12, 15, 0, 0), 7, 0));
    this.add(jLabel3,   new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 15, 0, 19), 5, 0));
    this.add(tfNickname,   new GridBagConstraints(2, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(7, 7, 0, 251), 13, 0));
    this.add(tfPassword,   new GridBagConstraints(2, 3, 1, 2, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 6, 195, 252), 51, 0));
  }
  public String getNickname()
  {
    String txt = tfNickname.getText();
    ROOM_NICK.setValue(txt);
    return txt;
  }
  public String getPassword()
  {
    String txt = tfPassword.getText();
    ROOM_PWD.setValue(txt);
    return txt;
  }
  public String getRoomName()
  {
    String txt = tfRoomName.getText();
    ROOM_NAME.setValue(txt);
    return txt;
  }
  public String getRoomServer()
  {
    String txt = tfRoomServer.getText();
    ROOM_SERVER.setValue(txt);
    return txt;
  }

}

