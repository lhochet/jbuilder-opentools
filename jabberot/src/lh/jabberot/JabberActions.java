package lh.jabberot;

import com.borland.primetime.ide.*;
import lh.jabber.*;
import java.io.*;
import com.borland.primetime.properties.*;
import lh.jabber.stanza.*;
import com.borland.primetime.wizard.*;
import javax.swing.*;
import lh.util.*;
import com.borland.primetime.ui.*;
import java.util.logging.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:22 $
 * @created 0.1, 7/07/03
 */

public class JabberActions
{
  public static class ConnectAction extends BrowserAction
  {
    public ConnectAction()
    {
      super("Connect", 'C', "Start a session...");
    }

    public void actionPerformed(Browser browser)
    {
      Thread t = new Thread()
      {
        public void run()
        {
          try
          {
            JabberService service = JabberClient.service;
//            if (service.getStatus() != JabberService.DISCONNECTED)
//            {
              service.disconnect();
//            }
            service.setServer(LoginPropertyPage.SERVER.getValue());
            service.setPort(LoginPropertyPage.PORT.getInteger());
            service.setJabberId(new JabberId(LoginPropertyPage.JID_USER.getValue(),
                                             LoginPropertyPage.JID_DOMAIN.getValue(),
                                             LoginPropertyPage.JID_RESOURCE.getValue()));
            String pwd = LoginPropertyPage.JID_PASSWORD.getValue();
            if (pwd == null)
            {
              pwd = JOptionPane.showInputDialog(Browser.getActiveBrowser(), "Jabber password for " + service.getJabberId() + ":");
              LoginPropertyPage.JID_PASSWORD.setValue(Password.encode(pwd));
            }
            else
            {
//          System.out.println("pwd bd="+pwd);
              pwd = Password.decode(pwd);
//          System.out.println("pwd ad="+pwd);
            }
//System.out.println("pwd aad="+pwd);
            service.setPassword(pwd);

            service.connect();

          }

          catch (JabberException ex)
          {
            ex.printStackTrace();
          }
          catch (IOException ex)
          {
            ex.printStackTrace();
          }
        }
      };
      t.start();
    }
  }

  public static class DisconnectAction extends BrowserAction
  {
    public DisconnectAction()
    {
      super("Disconnect", 'D', "Terminate the session...");
    }

    public void actionPerformed(Browser browser)
    {
      JabberClient.service.disconnect();
    }
  }

  public static class OptionsAction extends BrowserAction
  {
    public OptionsAction()
    {
      super("Options...", 'O', "Show options...");
    }

    public void actionPerformed(Browser browser)
    {
      PropertyManager.showPropertyDialog(browser,
            "Jabber Options",
            JabberClient.JABBER_TOPIC);
    }
  }

  public static class PresenceAction extends BrowserStateAction
  {
    private static Logger log = Logger.getLogger(PresenceAction.class.getName());
    private int type = Presence.AVAILABLE;
    private String status = null;
    private JabberService service = null;
    private static int state = Presence.AVAILABLE;

    public PresenceAction(JabberService service, int type, String status, Icon icon)
    {
      super(status, ' ', "Set my presence to " + status, icon);
      this.type = type;
      this.service = service;
      if (type != Presence.INVISIBLE) this.status = status;
      state = service.getPresence().getShowInt();
    }

    public boolean getState(Browser browser)
    {
      state = service.getPresence().getShowInt();
//      log.fine("state = " + state);
      return type == state;
    }

    public void setState(Browser browser, boolean bstate)
    {
      if (type == state) return;
      if ((state == Presence.INVISIBLE) && (type != state))
      {
        service.sendPresence(Presence.VISIBLE, null, null, null, -1);
      }

//      state = type;

      int senttype = Presence.AVAILABLE;
      String show = null;
      int priority = 0;

      switch(type)
      {
        case Presence.AVAILABLE:
          // type = null when avail
          break;
        case Presence.CHAT:
          show = "chat";
          priority = 1;
//          status = null;
          break;
        case Presence.AWAY:
          show = "away";
          break;
        case Presence.XA:
          show = "xa";
          priority = -1;
          break;
        case Presence.DND:
          show = "dnd";
          priority = -2;
          break;
        case Presence.INVISIBLE:
          senttype = Presence.INVISIBLE;
          break;
      }
      service.sendPresence(senttype, null, status, show, priority);
    }
  }

  public static class RegisterAction extends BrowserAction
  {
    public RegisterAction()
    {
      super("Register...", 'R', "Register with a Jabber server...");
    }

    public void actionPerformed(Browser browser)
    {
      WizardDialog dlg = new WizardDialog(browser, new RegisterWizard());
      dlg.show();
    }
  }

  public static class UnRegisterAction extends BrowserAction
  {
    public UnRegisterAction()
    {
      super("UnRegister...", 'U', "Cancel an existing registration with a Jabber server...");
    }

    public void actionPerformed(Browser browser)
    {
      JabberService service = JabberClient.service;
      if (JOptionPane.showConfirmDialog(browser, "Unregister with " + service.getServer() + " ?") == JOptionPane.OK_OPTION)
      {
        service.unregister();
      }
    }
  }

  public static class JoinConferenceAction extends BrowserAction
  {
    public JoinConferenceAction()
    {
      super("Join Conference...", 'J', "Join a conference...");
    }

    public void actionPerformed(Browser browser)
    {
      ConferenceLoginPanel panel = new ConferenceLoginPanel();
      if (DefaultDialog.showModalDialog(browser, "Join Conference", panel, null))
      {
        ConferenceManager.getInstance().addConference(panel.getRoomName(), panel.getRoomServer(), panel.getNickname(), panel.getPassword());
      }
    }
  }

  public static class ShowDebugAction extends BrowserAction
  {
    public ShowDebugAction()
    {
      super("Show Log...", 'L', "Show jabber logs");
    }

    public void actionPerformed(Browser browser)
    {
      JabberDebugHandler.getInstance().show();
    }
  }

  public static class ShowRosterAction extends BrowserAction
  {
    public ShowRosterAction()
    {
      super("Show Roster...", 'R', "Show jabber roster");
    }

    public void actionPerformed(Browser browser)
    {
      RosterCategory.getInstance().show();
    }
  }

  public static class AddContactAction extends BrowserAction
  {
    public AddContactAction()
    {
      super("Add Contact...", 't', "Adds a contact...");
    }

    public void actionPerformed(Browser browser)
    {
      AddContactPanel panel = new AddContactPanel();
      if (DefaultDialog.showModalDialog(browser, "Add Contact", panel, null))
      {
        JabberClient.service.addContact(panel.getJID(), panel.getNick(), panel.getGroup());
      }
    }
  }

}

