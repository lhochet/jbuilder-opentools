package lh.jabberot.whiteboard;

//import java.awt.*;
import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import lh.jabber.whiteboard.*;
import lh.jabber.whiteboard.Rectangle;
import lh.jabber.whiteboard.Shape;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:31 $
 * @created 30/10/03
 */

public class DrawingBoard extends JPanel implements MouseListener, MouseMotionListener
{
  public static final int NONE = 0;
  public static final int RECTANGLE = 1;
  public static final int LINE = 2;
  public static final int PATH = 3;

  private Shape curShape = null;

  private java.util.List shapes = new ArrayList();
  private java.util.List unsentshapes = new ArrayList();

  private int curShapeType = NONE;

  public DrawingBoard()
  {
    try
    {
      jbInit();

      addMouseListener(this);
      addMouseMotionListener(this);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    this.setBackground(Color.white);
  }

  /**
   * Calls the UI delegate's paint method, if the UI delegate is
   * non-<code>null</code>.
   *
   * @param g the <code>Graphics</code> object to protect
   * @todo Implement this javax.swing.JComponent method
   */
  protected void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    g.setColor(Color.BLACK);
    Iterator iter = shapes.iterator();
    while (iter.hasNext()) {
      Shape shape = (Shape)iter.next();
      shape.paintComponent(g);
    }
  }

  /**
   * Invoked when the mouse button has been clicked (pressed and released) on a
   * component.
   *
   * @param e MouseEvent
   * @todo Implement this java.awt.event.MouseListener method
   */
  public void mouseClicked(MouseEvent e)
  {
  }

  /**
   * Invoked when a mouse button has been pressed on a component.
   *
   * @param e MouseEvent
   * @todo Implement this java.awt.event.MouseListener method
   */
  public void mousePressed(MouseEvent e)
  {
    createShape(curShapeType);
    if (curShape != null) curShape.start(e.getPoint());
  }

  /**
   * Invoked when a mouse button has been released on a component.
   *
   * @param e MouseEvent
   * @todo Implement this java.awt.event.MouseListener method
   */
  public void mouseReleased(MouseEvent e)
  {
    if (curShape != null) curShape.end(e.getPoint());
    repaint();
  }

  /**
   * Invoked when the mouse enters a component.
   *
   * @param e MouseEvent
   * @todo Implement this java.awt.event.MouseListener method
   */
  public void mouseEntered(MouseEvent e)
  {
  }

  /**
   * Invoked when the mouse exits a component.
   *
   * @param e MouseEvent
   * @todo Implement this java.awt.event.MouseListener method
   */
  public void mouseExited(MouseEvent e)
  {
  }

  /**
   * Invoked when a mouse button is pressed on a component and then dragged.
   *
   * @param e MouseEvent
   * @todo Implement this java.awt.event.MouseMotionListener method
   */
  public void mouseDragged(MouseEvent e)
  {
    if (curShape != null) curShape.intermediate(e.getPoint());
    repaint();
  }

  /**
   * Invoked when the mouse cursor has been moved onto a component but no buttons
   * have been pushed.
   *
   * @param e MouseEvent
   * @todo Implement this java.awt.event.MouseMotionListener method
   */
  public void mouseMoved(MouseEvent e)
  {
  }

  public void setShape(int shape)
  {
    curShapeType = shape;
  }

  public void createShape(int shape)
  {
    switch (shape)
    {
      case PATH:
        curShape = new Path();
        break;
      case LINE:
        curShape = new Line();
        break;
      case RECTANGLE:
        curShape = new Rectangle();
        break;
      default:
        curShape = null;
        break;
    }
    if (curShape != null)
    {
      shapes.add(curShape);
      unsentshapes.add(curShape);
    }
  }

  /**
   * send
   */
  public void send(StringBuffer buf)
  {
    Iterator iter = unsentshapes.iterator();
    while (iter.hasNext()) {
      Shape shape = (Shape)iter.next();
      shape.send(buf);
    }
    unsentshapes.clear();
  }

  /**
   * addShape
   *
   * @param shape Shape
   */
  public void addShape(Shape shape)
  {
    shapes.add(shape);
    repaint();
  }

  /**
   * clear
   */
  public void clear()
  {
    shapes.clear();
    repaint();
  }

}
