package lh.jabberot.whiteboard;

import com.borland.primetime.ide.NodeViewer;
import com.borland.primetime.util.VetoException;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.Icon;
import com.borland.primetime.viewer.*;
import com.borland.primetime.ide.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:32 $
 */

public class WhiteBoardNodeViewer extends AbstractNodeViewer
{
  private WhiteBoardNode node = null;
  public WhiteBoardNodeViewer(Context context, WhiteBoardNode node)
  {
    super(context);
    this.node = node;
  }

  /**
   * createStructureComponent
   *
   * @return JComponent
   * @todo Implement this com.borland.primetime.viewer.AbstractNodeViewer method
   */
  public JComponent createStructureComponent()
  {
    return null;
  }

  /**
   * createViewerComponent
   *
   * @return JComponent
   * @todo Implement this com.borland.primetime.viewer.AbstractNodeViewer method
   */
  public JComponent createViewerComponent()
  {
    return new WhiteBoardNodeViewerPanel(node);
  }

  /**
   * getViewerTitle
   *
   * @return String
   * @todo Implement this com.borland.primetime.ide.NodeViewer method
   */
  public String getViewerTitle()
  {
    return "WhiteBoard";
  }

}
