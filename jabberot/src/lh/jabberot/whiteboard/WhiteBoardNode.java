package lh.jabberot.whiteboard;

import javax.swing.Icon;
import com.borland.primetime.node.*;
import com.borland.primetime.ide.*;
import lh.jabber.*;
import java.util.ArrayList;
import java.util.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:32 $
 * @created 30/10/03
 */

public class WhiteBoardNode extends LightweightNode implements MessageListener
{
  private JabberService service = null;
  private JabberId jid = null;
  private ArrayList messageslisteners = new ArrayList();
  private ArrayList msgs = new ArrayList();

  public WhiteBoardNode(Project prj, JabberService service, JabberId jid)
  {
    super(prj, null, jid + "- Chat WhiteBoard");
    this.service = service;
    this.jid = jid;
    service.addMessageListener(this);
  }
  public Icon getDisplayIcon()
  {
    return BrowserIcons.ICON_BLANK;
  }

  public JabberService getService()
  {
    return service;
  }
  public JabberId getJID()
  {
    return jid;
  }

  public void addMessageListener(MessageListener l)
  {
    messageslisteners.add(l);
    if (msgs.size() > 0)
    {
      ArrayList lmsgs = new ArrayList(msgs);
      msgs.clear();
      Iterator iter = (new ArrayList(messageslisteners)).iterator();
      while (iter.hasNext())
      {
        MessageListener item = (MessageListener) iter.next();
        Iterator iter2 = lmsgs.iterator();
        while (iter2.hasNext()) {
          lh.jabber.stanza.Message item2 = (lh.jabber.stanza.Message)iter2.next();
          item.received(item2);
        }
      }
    }
  }

  public void received(lh.jabber.stanza.Message msg)
  {
    if (messageslisteners.size() == 0)
    {
      msgs.add(msg);
    }
    else
    {
      Iterator iter = (new ArrayList(messageslisteners)).iterator();
      while (iter.hasNext())
      {
        MessageListener item = (MessageListener) iter.next();
        item.received(msg);
      }
    }
  }
}
