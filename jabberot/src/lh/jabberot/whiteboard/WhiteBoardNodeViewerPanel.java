package lh.jabberot.whiteboard;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import lh.jabber.*;
import lh.jabber.stanza.*;
import java.util.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:32 $
 * @created 30/10/03
 */

public class WhiteBoardNodeViewerPanel extends JPanel implements MessageListener
{
  private WhiteBoardNode node = null;
  private JabberService service = null;
  private JabberId jid = null;

  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel pToolbar = new JPanel();
  private DrawingBoard pDrawingPanel = new DrawingBoard();
  private JToggleButton tbtnNone = new JToggleButton();
  private FlowLayout flowLayout1 = new FlowLayout();
  private JToggleButton tbtnLine = new JToggleButton();
  private JToggleButton tbtnRectangle = new JToggleButton();
  private JToggleButton tbtnPath = new JToggleButton();
  private JPanel jPanel1 = new JPanel();
  private JButton btnSend = new JButton();
  private FlowLayout flowLayout2 = new FlowLayout();
  private JButton btnClear = new JButton();

  public WhiteBoardNodeViewerPanel()
  {
    this(null);
  }
  public WhiteBoardNodeViewerPanel(WhiteBoardNode node)
  {
    this.node = node;
    this.service = node.getService();
    this.jid = node.getJID();

    node.addMessageListener(this);

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    tbtnNone.setText("None");
    tbtnNone.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tbtnNone_actionPerformed(e);
      }
    });
    pToolbar.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    tbtnLine.setText("Line");
    tbtnLine.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tbtnLine_actionPerformed(e);
      }
    });
    tbtnRectangle.setText("Rectangle");
    tbtnRectangle.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tbtnRectangle_actionPerformed(e);
      }
    });
    tbtnPath.setPreferredSize(new Dimension(54, 23));
    tbtnPath.setText("Path");
    tbtnPath.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tbtnPath_actionPerformed(e);
      }
    });
    btnSend.setText("Send");
    btnSend.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnSend_actionPerformed(e);
      }
    });
    jPanel1.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.RIGHT);
    btnClear.setMaximumSize(new Dimension(74, 23));
    btnClear.setMinimumSize(new Dimension(74, 23));
    btnClear.setPreferredSize(new Dimension(74, 23));
    btnClear.setText("Clear");
    btnClear.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnClear_actionPerformed(e);
      }
    });
    this.add(pToolbar, BorderLayout.NORTH);
    this.add(pDrawingPanel, BorderLayout.CENTER);
    this.add(jPanel1,  BorderLayout.SOUTH);
    jPanel1.add(btnSend, null);
    pToolbar.add(tbtnNone, null);
    pToolbar.add(tbtnLine, null);
    pToolbar.add(tbtnRectangle, null);
    pToolbar.add(tbtnPath, null);
    pToolbar.add(btnClear, null);
  }

  void tbtnNone_actionPerformed(ActionEvent e)
  {
    clearButtons();
    tbtnNone.setSelected(true);
  }

  void tbtnLine_actionPerformed(ActionEvent e)
  {
    clearButtons();
    tbtnLine.setSelected(true);
    pDrawingPanel.setShape(DrawingBoard.LINE);
  }

  void tbtnRectangle_actionPerformed(ActionEvent e)
  {
    clearButtons();
    tbtnRectangle.setSelected(true);
    pDrawingPanel.setShape(DrawingBoard.RECTANGLE);

  }

  void tbtnPath_actionPerformed(ActionEvent e)
  {
    clearButtons();
    tbtnPath.setSelected(true);
    pDrawingPanel.setShape(DrawingBoard.PATH);
  }

  private void clearButtons()
  {
     tbtnNone.setSelected(false);
     tbtnLine.setSelected(false);
     tbtnRectangle.setSelected(false);
     tbtnPath.setSelected(false);
     pDrawingPanel.setShape(DrawingBoard.NONE);
  }

  void btnSend_actionPerformed(ActionEvent e)
  {
    StringBuffer buf = new StringBuffer();
    buf.append("<x xmlns=\"lh:jabber:whiteboard\">");
    pDrawingPanel.send(buf);
    buf.append("</x>");
    service.sendExtendedMessage(Message.CHAT, jid.toString(), null, null, buf.toString());
  }

  /**
   * received
   *
   * @param msg Message
   * @todo Implement this lh.jabber.MessageListener method
   */
  public void received(Message msg)
  {
    if (msg instanceof WhiteBoardStanza)
    {
      WhiteBoardStanza wbs = (WhiteBoardStanza)msg;
      java.util.List shapes = wbs.getShapes();
      Iterator iter = shapes.iterator();
      while (iter.hasNext()) {
        lh.jabber.whiteboard.Shape shape = (lh.jabber.whiteboard.Shape)iter.next();
        if (shape instanceof lh.jabber.whiteboard.Clear)
        {
          pDrawingPanel.clear();
          break;
        }
        pDrawingPanel.addShape(shape);

      }
    }
  }

  void btnClear_actionPerformed(ActionEvent e)
  {
    pDrawingPanel.clear();
    service.sendExtendedMessage(Message.CHAT, jid.toString(), null, null, "<x xmlns=\"lh:jabber:whiteboard\"><clear/></x>");
  }

}
