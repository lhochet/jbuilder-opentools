package lh.jabberot.whiteboard;

import com.borland.primetime.ide.NodeViewerFactory;
import com.borland.primetime.ide.NodeViewer;
import com.borland.primetime.ide.Context;
import com.borland.primetime.node.Node;
import com.borland.primetime.ide.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:32 $
 * @created 30/10/03
 */

public class WhiteBoardNodeViewerFactory implements NodeViewerFactory
{
  public WhiteBoardNodeViewerFactory()
  {
  }
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    Browser.registerNodeViewerFactory(new WhiteBoardNodeViewerFactory(), true);
   }


  public NodeViewer createNodeViewer(Context context)
  {
    Node node = context.getNode();
    if (canDisplayNode(node))
    {
      return new WhiteBoardNodeViewer(context, (WhiteBoardNode)node);
    }
    return null;

  }
  public boolean canDisplayNode(Node node)
  {
    return node instanceof WhiteBoardNode;
  }

}
