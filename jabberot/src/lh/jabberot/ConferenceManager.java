package lh.jabberot;

import lh.jabber.*;
import java.util.logging.*;
import java.util.*;
import com.borland.primetime.ide.*;
import lh.jabber.stanza.Presence;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:20 $
 * @created 18/09/03
 */

public class ConferenceManager implements MessageListener, PresenceUpdateListener
{
  Logger log = Logger.getLogger(ConferenceManager.class.getName());
  {
    log.setLevel(Level.OFF);
  }

  private static ConferenceManager instance = new ConferenceManager();
  public static ConferenceManager getInstance() { return instance; }

  private HashMap map = new HashMap();

  public ConferenceManager()
  {
  }

  public void received(lh.jabber.stanza.Message msg)
 {
   log.finest("ConferenceManager(type:" + msg.getType() + ").received");
   if (msg.getType() != lh.jabber.stanza.Message.GROUPCHAT) return;
   log.finest("ConferenceManager(from:" + msg.getFrom() +").received");
   String sfrom = msg.getFrom();
   JabberId from = new JabberId(sfrom);
   // no need add ourself
   if (from.equalFull(JabberClient.service.getJabberId())) return;

   ConferenceMessageCategory c = (ConferenceMessageCategory)map.get(from.getJID().toLowerCase());
   boolean isnew = false;
   if (c == null)
   {
     c = new ConferenceMessageCategory(JabberClient.service, from);
     map.put(from.getJID().toLowerCase(), c);
     isnew = true;
   }
   ConferencePanel p = c.getPanel();
   Browser.getActiveBrowser().getMessageView().addCustomTab(c, p);
   /*if (isnew)*/ p.received(msg);
 }

  public void addConference(String roomname, String roomServer, String roomNick, String roomPassword)
  {
    JabberId jid = new JabberId(roomname, roomServer, roomNick);
    ConferenceMessageCategory c = (ConferenceMessageCategory)map.get(jid.getJID().toLowerCase());
    if (c == null)
    {
      c = new ConferenceMessageCategory(JabberClient.service, jid);
      map.put(jid.getJID().toLowerCase(), c);
    }
    MessageView mv = Browser.getActiveBrowser().getMessageView();
    mv.addCustomTab(c, c.getPanel());
    mv.showTab(c);
    JabberClient.service.sendPresence(Presence.AVAILABLE, jid.toString(), "available", null, 1);
  }

  /**
   * receivedUpdateEvent
   *
   * @param presence Presence
   */
  public void receivedUpdateEvent(Presence presence)
  {
    log.finest("ConferenceManager.receivedUpdateEvent(presence:" + presence.toString() + ")");
    String sfrom = presence.getFrom();
    JabberId from = new JabberId(sfrom);


    ConferenceMessageCategory c = (ConferenceMessageCategory)map.get(from.getJID().toLowerCase());
//    boolean isnew = false;
    if (c == null)
    {
      return;
//      c = new ConferenceMessageCategory(JabberClient.service, from);
//      map.put(from.getJID(), c);
//      isnew = true;
    }
    ConferencePanel p = c.getPanel();
    p.receivedUpdateEvent(presence);
  }
}
