package lh.jabberot;

import javax.swing.*;

import com.borland.jbcl.layout.*;
import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:24 $
 * @created 19/08/03
 */

public class JabberPropertyPage extends PropertyPage implements PropertyGroup
{
  static final GlobalBooleanProperty AUTO_CONNECT = new GlobalBooleanProperty(JabberClient.JABBER_CAT,
      "connect_on_start", false);
  static final GlobalBooleanProperty SHOW_ROSTER_ONSTARTUP = new GlobalBooleanProperty(JabberClient.JABBER_CAT,
      "show_roster_onstartup", false);
  static final GlobalBooleanProperty SHOW_DEBUG_ONSTARTUP = new GlobalBooleanProperty(JabberClient.JABBER_CAT,
      "show_debug_onstartup", false);

  private JCheckBox cbxAutoConnect = new JCheckBox();
  private JCheckBox cbxShowRoster = new JCheckBox();
  private JCheckBox cbxShowDebug = new JCheckBox();
  private VerticalFlowLayout verticalFlowLayout1 = new VerticalFlowLayout();

  public JabberPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void readProperties()
  {
    cbxAutoConnect.setSelected(AUTO_CONNECT.getBoolean());
    cbxShowRoster.setSelected(SHOW_ROSTER_ONSTARTUP.getBoolean());
    cbxShowDebug.setSelected(SHOW_DEBUG_ONSTARTUP.getBoolean());
  }

  public void writeProperties()
  {
    AUTO_CONNECT.setBoolean(cbxAutoConnect.isSelected());
    SHOW_ROSTER_ONSTARTUP.setBoolean(cbxShowRoster.isSelected());
    SHOW_DEBUG_ONSTARTUP.setBoolean(cbxShowDebug.isSelected());
  }

  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("/doc/JabberPropertyPage.html").toString());
  }

  public void initializeProperties()
  {

  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == JabberClient.JABBER_TOPIC)
    {
      return new PropertyPageFactory("Options")
      {
        public PropertyPage createPropertyPage()
        {
          return new JabberPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

  private void jbInit() throws Exception
  {
    this.setLayout(verticalFlowLayout1);
    cbxAutoConnect.setText("AutoConnect on start up");
    cbxShowRoster.setText("Show Roster on start up");
    cbxShowDebug.setText("Show debug log on start up");
    this.add(cbxAutoConnect);
    this.add(cbxShowRoster);
    this.add(cbxShowDebug);
  }
}
