package lh.jabberot;

import com.borland.primetime.wizard.BasicWizardPage;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:26 $
 * @created 20/08/03
 */

public class RegisterResultPage extends BasicWizardPage
{
  public RegisterResultPage()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception
  {
    this.setInstructions("");
    this.setPageTitle("Result");
    this.setLargeIcon(null);
  }
  public void setResult(String txt)
  {
    setInstructions(txt);
  }

}
