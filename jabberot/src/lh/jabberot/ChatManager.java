package lh.jabberot;

import lh.jabber.*;
import java.util.*;
import com.borland.primetime.ide.*;
import java.util.logging.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:18 $
 * @created 14/07/03
 */

public class ChatManager implements MessageListener
{
  Logger log = Logger.getLogger(ChatManager.class.getName());
  {
    log.setLevel(Level.OFF);
  }

  private static ChatManager instance = new ChatManager();
  public static ChatManager getInstance() { return instance; }

  HashMap map = new HashMap();

  public ChatManager()
  {
  }

  public void received(lh.jabber.stanza.Message msg)
 {
   log.finest("ChatManager(type:" + msg.getType() + ").received");
   if (msg.getType() != lh.jabber.stanza.Message.CHAT) return;
   log.finest("ChatManager(from:" + msg.getFrom() +").received");
   String sfrom = msg.getFrom();
   JabberId from = new JabberId(sfrom);
   // no need add ourself
   if (from.equalFull(JabberClient.service.getJabberId())) return;

   ChatMessageCategory c = (ChatMessageCategory)map.get(sfrom);
   boolean isnew = false;
   if (c == null)
   {
     c = new ChatMessageCategory(JabberClient.service, from);
     map.put(sfrom, c);
     isnew = true;
   }
   ChatPanel p = c.getPanel();
   Browser.getActiveBrowser().getMessageView().addCustomTab(c, p);
   /*if (isnew)*/ p.received(msg);
 }

  public void addChat(JabberId jid)
  {
    ChatMessageCategory c = (ChatMessageCategory)map.get(jid.toString());
    if (c == null)
    {
      c = new ChatMessageCategory(JabberClient.service, jid);
      map.put(jid.toString(), c);
    }
    MessageView mv = Browser.getActiveBrowser().getMessageView();
    mv.addCustomTab(c, c.getPanel());
    mv.showTab(c);
  }

}
