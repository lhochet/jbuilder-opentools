package lh.jabberot;

import com.borland.primetime.wizard.BasicWizardPage;
import lh.jabber.stanza.RegisterStanza;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import javax.swing.table.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:26 $
 * @created 20/08/03
 */

public class RegisterInfoWizardPage extends BasicWizardPage
{
  class FieldsModel extends AbstractTableModel
  {
    private Vector rows = new Vector();

    public FieldsModel(Vector rows)
    {
      Iterator iter = rows.iterator();
      while (iter.hasNext()) {
        String name = (String)iter.next();
        this.rows.add(new Field(name));
      }
//      this.rows = rows;
    }

    public Vector getFields()
    {
      return rows;
    }

    public int getColumnCount()
    {
      return 2;
    }

    public Object getValueAt(int y, int x)
    {
      Field field = (Field) rows.elementAt(y);
      switch (x)
      {
        case 0:
          return field.name;
        case 1:
          return field.value;
      }
      return new ArrayIndexOutOfBoundsException("Invalid column.");
    }

    public int getRowCount()
    {
      if (rows == null)
      {
        return 0;
      }
      return rows.size();
    }

    public String getColumnName(int column)
    {
      switch (column)
      {
        case 0:
          return "Name";
        case 1:
          return "Value";
      }
      return "";
    }

    public Class getColumnClass(int c)
    {
      if (getValueAt(0, c) == null)
      {
        return (new Object()).getClass();
      }
      return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex)
    {
      if (colIndex > 0)
      {
        return true;
      }
      return false;
    }

    public void setValueAt(Object value, int row, int col)
    {
      Field field = (Field) rows.elementAt(row);
      switch (col)
      {
        case 0:
          field.name = (String) value;
          break;
        case 1:
          field.value = (String) value;
          break;
      }
      fireTableCellUpdated(row, col);
    }
  }

  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JTable jTable1 = new JTable();
  public RegisterInfoWizardPage()
  {
    try
    {
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * setStanza
   *
   * @param stanza RegisterStanza
   */
  public void setStanza(RegisterStanza stanza)
  {
    this.setInstructions(stanza.getInstructions());
    jTable1.setModel(new FieldsModel(stanza.getFields()));

  }

  private void jbInit() throws Exception
  {
    this.setInstructions("Fill in the fields:");
    this.setPageTitle("Details");
    this.setLargeIcon(null);
    this.setLayout(borderLayout1);
    jScrollPane1.setPreferredSize(new Dimension(453, 203));
    this.add(jScrollPane1,  BorderLayout.CENTER);
    jScrollPane1.getViewport().add(jTable1, null);
  }

  public Vector getFields()
  {
    TableModel model = jTable1.getModel();
    if (model instanceof FieldsModel)
    {
      return ((FieldsModel)model).getFields();
    }
    return new Vector();
  }

  public void flush()
  {
    if (jTable1.isEditing())
    {
      jTable1.getCellEditor().stopCellEditing();
    }
  }
}
