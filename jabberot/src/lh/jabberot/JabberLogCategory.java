package lh.jabberot;

import com.borland.primetime.ide.*;
import lh.jabber.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:24 $
 * @created 3/08/03
 */

public class JabberLogCategory extends MessageCategory
{
//  private JabberDebugPane panel = null;
//  private boolean isshown = false;

  public JabberLogCategory()
  {
    super("Jabber Log");
  }

//  public JabberDebugPane getPanel()
//  {
//    return panel;
//  }
//
//  public void setPanel(JabberDebugPane panel)
//  {
//    this.panel = panel;
//  }
//
//  public boolean isShown()
//  {
//    return isshown;
//  }
//  public void setShown(boolean isshown)
//  {
//    this.isshown = isshown;
//  }

//  public void categoryClosing() throws com.borland.primetime.util.VetoException
//  {
//    throw new com.borland.primetime.util.VetoException();
//    super.categoryClosing();
//    isshown = false;
//  }
}
