package lh.jabberot;

import java.io.*;
import java.util.*;

import com.borland.primetime.*;
import com.borland.primetime.actions.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.properties.*;
import lh.community.*;
import lh.jabber.*;
import lh.jabber.stanza.*;
import lh.util.*;

/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:22 $
 * @created 5/07/03
 */

public class JabberClient
{
  public static final Object JABBER_TOPIC = new Object();
  static final String JABBER_CAT = "lh.jabber";

  static ActionGroup presences = null;
  static ActionGroup connect = null;
  static ActionGroup register = null;
  static ActionGroup options = null;
  static ActionGroup contact = null;

  public static JabberService service = null;
  private static final MessageCategory ERROR_CAT = new MessageCategory("Jabber Errors");

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH JabberClient " + VersionUtil.getVersionString(JabberClient.class));
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    Thread thread = new Thread()
    {
      public void run()
      {
        //Jabber
        // init service
        service = new JabberService(LoginPropertyPage.SERVER.getValue(),
                                                  LoginPropertyPage.PORT.getInteger());
        service.setClientName("LH JBuilder Jabber Client");
        service.setClientVersion(getVersion());
        service.setClientOs(getOs());
        service.setJabberId(new JabberId(LoginPropertyPage.JID_USER.getValue(),
                                         LoginPropertyPage.JID_DOMAIN.getValue(),
                                         LoginPropertyPage.JID_RESOURCE.getValue()));

        String pwd = LoginPropertyPage.JID_PASSWORD.getValue();
        if (pwd == null)
        {
          // do nothing if password is null
//          pwd = JOptionPane.showInputDialog(Browser.getActiveBrowser(), "Jabber password:");
//          LoginPropertyPage.JID_PASSWORD.setValue(Password.encode(pwd));
        }
        else
        {
          pwd = Password.decode(pwd);
        }
        service.setPassword(pwd);

        // create menu
        connect = new ActionGroup("Connection");
        connect.add(new JabberActions.ConnectAction());
        connect.add(new JabberActions.DisconnectAction());
        JabberClientMenu.GROUP_Jabber.add(connect);

        options = new ActionGroup("Options");
        options.add(new JabberActions.OptionsAction());
        options.add(new JabberActions.ShowRosterAction());
        options.add(new JabberActions.ShowDebugAction());
        JabberClientMenu.GROUP_Jabber.add(options);

        register = new ActionGroup("Registration");
        register.add(new JabberActions.RegisterAction());
//        register.add(new JabberActions.UnRegisterAction());
        JabberClientMenu.GROUP_Jabber.add(register);

        contact = new ActionGroup("Contact");
        contact.add(new JabberActions.AddContactAction());
        JabberClientMenu.GROUP_Jabber.add(contact);

        JabberClientMenu.GROUP_Jabber.add(new JabberActions.JoinConferenceAction());

        presences = new ActionGroup("Presences");
        presences.setPopup(true);
        JabberClientMenu.GROUP_Jabber.add(presences);

        presences.add(new JabberActions.PresenceAction(service, Presence.AVAILABLE, "Available", StatusIcons.getInstance().getIcon(StatusIcons.ONLINE)));
        presences.add(new JabberActions.PresenceAction(service, Presence.CHAT, "Chat", StatusIcons.getInstance().getIcon(StatusIcons.CHAT)));
        presences.add(new JabberActions.PresenceAction(service, Presence.AWAY, "Away", StatusIcons.getInstance().getIcon(StatusIcons.AWAY)));
        presences.add(new JabberActions.PresenceAction(service, Presence.DND, "DND", StatusIcons.getInstance().getIcon(StatusIcons.DND)));
        presences.add(new JabberActions.PresenceAction(service, Presence.INVISIBLE, "Invisible", StatusIcons.getInstance().getIcon(StatusIcons.INVISIBLE)));

        // add prop page
        PropertyManager.registerPropertyGroup(new LoginPropertyPage());
        PropertyManager.registerPropertyGroup(new JabberPropertyPage());
        PropertyManager.registerPropertyGroup(new FileTransferPropertyPage());

        // roster
        RosterCategory roster = new RosterCategory(service);
        RosterCategory.setInstance(roster);
        if (JabberPropertyPage.SHOW_ROSTER_ONSTARTUP.getBoolean())
        {
          roster.show();
        }

        // debug
        JabberDebugHandler handler = JabberDebugHandler.getInstance();
        handler.setService(service);
        InputLogger.getLogger().addHandler(handler);
        OutputSender.getLogger().addHandler(handler);
        if (JabberPropertyPage.SHOW_DEBUG_ONSTARTUP.getBoolean())
        {
          handler.show();
        }

        // close listener
        com.borland.primetime.ide.Browser.addStaticBrowserListener(new com.borland.primetime.ide.BrowserAdapter()
        {
          public void browserClosed(Browser browser)
          {
            if (com.borland.primetime.ide.Browser.getBrowserCount() == 0)
            {
              JabberClient.service.disconnect();
            }
          }
        }); // addStatic

        // messsage listener
        service.addMessageListener(ChatManager.getInstance());
        MessageManager msgman = MessageManager.getInstance();
        service.addMessageListener(msgman);
        service.addClientsInfoListener(msgman);

        // conference listener
        ConferenceManager cfgman = ConferenceManager.getInstance();
        service.addMessageListener(cfgman);
        service.addAvailibityListener(cfgman);

        // oob listener
        service.setOOBListener(new JOTOOBListenerImpl());

        // error listener
        service.addErrorListener(new ErrorListener()
        {
          public void receivedError(JabberError error)
          {
            String msg = "" + error.getCode() + ": " + error.getMsg() + ", Disconnected.";
//            com.borland.primetime.ide.Message msg = new com.borland.primetime.ide.Message();
            Browser.getActiveBrowser().getMessageView().addMessage(ERROR_CAT, msg, BrowserIcons.ICON_ERROR);
          }
        });

        // start service
        try
        {
          boolean override = System.getProperty("lh.jabberot.noautoconnect", null) != null;
          if (JabberPropertyPage.AUTO_CONNECT.getBoolean() && !override) service.connect();
        }
        catch (Exception ex)
        {
          //ignore
        }
//        service.start();

        Browser.addBrowserShutdownListener(new ShutdownListener()
        {
          public void browserExit(int _int)
          {
            JabberClient.service.disconnect();
          }
        });
      }
    };
    thread.start();
  }

  private static String getVersion()
  {
    try
    {
      Properties props = new Properties();
      InputStream in = null;
      in = JabberClient.class.getResourceAsStream("build.num");
      props.load(in);
      in.close();
      return props.getProperty("version");
    }
    catch (Exception ex)
    {
      return null;
    }

  }

  private static String getOs()
  {

    try
    {
      StringBuffer buf = new StringBuffer();
      String tmp = System.getProperty("java.runtime.name", "JVM");
      buf.append(tmp);
      tmp = System.getProperty("java.runtime.version");
//      tmp = "1.4.1";
      if (tmp != null)
      {
        buf.append(' ');
        buf.append(tmp);
      }
      tmp = System.getProperty("os.name");
      if (tmp != null)
      {
        buf.append(" on ");
        buf.append(tmp);
        tmp = System.getProperty("os.version");
        if (tmp != null)
        {
          buf.append(' ');
          buf.append(tmp);
        }
      }
      return buf.toString();
    }
    catch (Exception ex)
    {
      return null;
    }
  }
}

