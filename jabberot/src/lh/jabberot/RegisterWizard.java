package lh.jabberot;

import com.borland.primetime.wizard.*;
import lh.jabber.*;
import java.io.*;
import lh.jabber.stanza.Stanza;
import lh.jabber.stanza.*;
import java.util.*;
import com.borland.primetime.help.*;
import com.borland.primetime.util.*;


/**
 * <p>Title: Jabber OpenTool</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2006-01-15 23:06:26 $
 * @created 19/08/03
 */

public class RegisterWizard extends BasicWizard implements RegisterListener
{
  private RegisterChooseHostWizardPage choosepg = null;
  private RegisterInfoWizardPage infopg = null;
  private RegisterResultPage respg = null;

  private boolean receivedError = false;


  public RegisterWizard()
  {
  }
  public WizardPage invokeWizard(WizardHost wh)
  {
    setWizardTitle("Register");
    choosepg = new RegisterChooseHostWizardPage();
    infopg = new RegisterInfoWizardPage();
    respg = new RegisterResultPage();
    addWizardPage(choosepg);
    addWizardPage(infopg);
    addWizardPage(respg);
    WizardPage wp = super.invokeWizard(wh);
    return wp;
  }
  protected void checkPage(WizardPage wp) throws com.borland.primetime.util.VetoException
  {
    super.checkPage(wp);

    if (wp == choosepg)
    {
//      try
//      {
        JabberService service = JabberClient.service;
//        if (service.getStatus() != JabberService.DISCONNECTED)
        {
          service.disconnect();
        }
        service.setServer(choosepg.getServer());
        service.setPort(LoginPropertyPage.PORT.getInteger());
        try
        {
          service.connect(false);
        }
        catch (Exception ex)
        {
          receivedError = true;
        }
        if ((!receivedError) && (service.getStatus() == JabberService.CONNECTED))
        {
          service.setRegisterListener(this);
          service.sendRegister(choosepg.getServer());
        }
        else
        {
          respg.setInstructions("Could not connect to server.");
          receivedError = true;
        }
        if (receivedError) removeWizardPage(infopg);
//      }
//      catch (JabberException ex)
//      {
//        ex.printStackTrace();
//      }
//      catch (IOException ex)
//      {
//        ex.printStackTrace();
//      }
    }
    else if (wp == infopg)
    {
      infopg.flush();
      JabberService service = JabberClient.service;
      respg.setResult("Registered.");
      service.sendRegistrationInfo(infopg.getFields());
    }
    else if (wp == respg)
    {
      JabberClient.service.disconnect();
      if (!receivedError)
      {
        JabberService service = JabberClient.service;
        String server = choosepg.getServer();
        service.setServer(server);
        LoginPropertyPage.SERVER.setValue(server);
        LoginPropertyPage.JID_DOMAIN.setValue(server);
        Vector v = infopg.getFields();
        Iterator iter = v.iterator();
        while (iter.hasNext())
        {
          Field field = (Field) iter.next();
          if (field.name.equals("username"))
          {
            LoginPropertyPage.JID_USER.setValue(field.value);
          }
          else if (field.name.equals("password"))
          {
            String pwd = lh.util.Password.encode(field.value);
            service.setPassword(pwd);
            LoginPropertyPage.JID_PASSWORD.setValue(pwd);
          }

        }
        service.setJabberId(new JabberId(LoginPropertyPage.JID_USER.getValue(),
                                         LoginPropertyPage.JID_DOMAIN.getValue(),
                                         LoginPropertyPage.JID_RESOURCE.getValue()));
      }
    }
    /** @todo think of unregistering registerlistener */
  }

  public WizardPage next(WizardPage currentPage, WizardHost host) throws VetoException
  {
    WizardPage wp = super.next(currentPage, host);
    if (wp == respg)
    {
      host.setCancelEnabled(false);
    }
    else
    {
      host.setCancelEnabled(true);
    }
    return wp;
  }

  /**
   * received
   *
   * @param stanza Stanza
   */
  public void receivedStanza(Stanza stanza)
  {
    if (stanza instanceof RegisterStanza)
    {
      RegisterStanza rs = (RegisterStanza)stanza;

      infopg.setStanza(rs);
      JabberClient.service.docontinue();
    }
    else if (stanza instanceof JabberError)
    {
      JabberError je = (JabberError)stanza;
      receivedError(je);
    }

  }
  /**
   * received
   *
   * @param stanza Stanza
   */
  public void receivedError(JabberError error)
  {
      respg.setInstructions(error.getCode() + ": " + error.getMsg());
      receivedError = true;
  }

  public void help(WizardPage currentPage, WizardHost host)
  {
    /** @todo ADD THE HELP */
//    return new ZipHelpTopic(null, getClass().getResource("/doc/LoginPropertyPage.html").toString());
    if (currentPage == choosepg)
    {
      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(null, getClass().getResource("/doc/RegisterChooseHostWizardPage.html").toString()), host.getDialogParent());
    }
    else if (currentPage == infopg)
    {
      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(null, getClass().getResource("/doc/RegisterInfoWizardPage.html").toString()), host.getDialogParent());
    }
    else if (currentPage == respg)
    {
      com.borland.primetime.help.HelpManager.showHelp(new ZipHelpTopic(null, getClass().getResource("/doc/RegisterResultPage.html").toString()), host.getDialogParent());
    }
  }

}
