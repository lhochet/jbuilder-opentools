package lh.jabberot;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.event.TreeModelListener;
import java.util.*;
import lh.jabber.*;
import javax.swing.event.*;
import javax.swing.*;
import com.borland.primetime.ide.*;
import lh.jabber.stanza.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 30/12/02
 */

public class RosterTreeModel implements TreeModel, RosterListener, PresenceSubscriptionListener, PresenceUpdateListener, ServiceStatusListener
{
  private RosterTree tree;
  private JabberService service = null;
  private Vector items = new Vector();
  private Vector myres = new Vector();

  private String root = "Roster";
  private String contacts = "Contacts";
  private String resources = "My Resources";

  private Vector listeners = new Vector();

  public RosterTreeModel(JabberService service)
  {
    this.tree = tree;
    this.service = service;
    if (service != null)
    {
      service.addRosterListener(this);
      service.addAvailibityListener(this);
      service.addSubscriptionListener(this);
      service.addServiceSentPresenceListener(this);
      service.addStatusListener(this);
    }
  }

  void setTree(RosterTree tree)
  {
    this.tree = tree;
  }

  public void setItems(Vector items)
  {
    this.items = items;
    fireTreeStructureChanged(contacts);
  }

  public void updateItem(RosterItem item)
  {
    String tmp = item.getSubscription();
    boolean dealt = false;
    Iterator iter = items.iterator();
    while (iter.hasNext())
    {
      RosterItem litem = (RosterItem) iter.next();
      if (litem.getJid().equal(item.getJid()))
      {
        if (tmp.equalsIgnoreCase("remove"))
        {
          items.remove(litem);
          fireTreeStructureChanged(contacts);
        }
        else
        {
          litem.setName(item.getName());
          litem.setSubscription(item.getSubscription());
          litem.setAsk(item.getAsk());
          litem.setGroup(item.getGroup());
          fireTreeContactStructureChanged(litem);
        }
        dealt = true;
        break;
      }
    }
    if (!dealt)
    {
      items.add(item);
      fireTreeStructureChanged(contacts);
    }
  }

  public Object getRoot()
  {
    return root;
  }
  public Object getChild(Object parent, int index)
  {
    if (parent == root)
    {
      if (index == 0) return contacts;
      if (index == 1) return resources;
    }
    if (parent == contacts) return items.elementAt(index);
    if (parent == resources) return myres.elementAt(index);
    if (parent instanceof RosterItem)
    {
      RosterItem item = (RosterItem)parent;
      return item.getResource(index);
    }
    return null;
  }
  public int getChildCount(Object parent)
  {
    if (parent == root) return 2;
    if (parent == contacts) return items.size();
    if (parent == resources) return myres.size();
    if (parent instanceof RosterItem)
    {
      RosterItem item = (RosterItem)parent;
      return item.getResourcesCount();
    }
    return 0;
  }
  public boolean isLeaf(Object node)
  {
    if (node == root) return false;
    if (node == contacts) return false;
    if (node == resources) return false;
    if (node instanceof RosterItem)
    {
      RosterItem item = (RosterItem)node;
      return item.getResourcesCount() < 1;
    }
    return true;
  }
  public void valueForPathChanged(TreePath path, Object newValue)
  {
    // do nothing
//    /**@todo Implement this javax.swing.tree.TreeModel method*/
//    throw new java.lang.UnsupportedOperationException("Method valueForPathChanged() not yet implemented.");
  }
  public int getIndexOfChild(Object parent, Object child)
  {
    if (parent == root)
    {
      if (child == contacts) return 0;
      if (child == resources) return 1;
    }
    if (parent == contacts) return items.indexOf(child);
    if (parent == resources) return myres.indexOf(child);
    if (parent instanceof RosterItem)
    {
      RosterItem item = (RosterItem)parent;
      return item.indexOfResource(child);
    }
    return -1;
  }
  public void addTreeModelListener(TreeModelListener l)
  {
    listeners.add(l);
  }

  public void removeTreeModelListener(TreeModelListener l)
  {
    listeners.remove(l);
  }

  protected void fireTreeStructureChanged(Object o)
  {
    int len = listeners.size();
    TreeModelEvent e = new TreeModelEvent(this, new Object[] {root, o});
    for (int i = 0; i < len; i++)
    {
      ((TreeModelListener)listeners.elementAt(i)).treeStructureChanged(e);
    }
  }
  protected void fireTreeContactStructureChanged(Object o)
  {
    int len = listeners.size();
    TreeModelEvent e = new TreeModelEvent(this, new Object[] {root, contacts, o});
    for (int i = 0; i < len; i++)
    {
      ((TreeModelListener)listeners.elementAt(i)).treeStructureChanged(e);
    }
  }
  protected void fireTreeResourceStructureChanged(Object o)
  {
    int len = listeners.size();
    TreeModelEvent e = new TreeModelEvent(this, new Object[] {root, resources, o});
    for (int i = 0; i < len; i++)
    {
      ((TreeModelListener)listeners.elementAt(i)).treeStructureChanged(e);
    }
  }
  public void receivedSubscriptionEvent(Presence presence)
  {
//    System.out.println("subscription event");
    int type = presence.getType();
    switch (type)
    {
      case Presence.SUBSCRIBE:
        if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(tree, "Accept subscription from: " + presence.getFrom(), "Subscription", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
        {
//          System.out.println("accepted");
          service.sendPresence(Presence.SUBSCRIBED, presence.getFrom(), null, null, -1);
        }
        else
        {
//          System.out.println("rejected");
          service.sendPresence(Presence.UNSUBSCRIBED, presence.getFrom(), null, null, -1);
        }
        break;
      case Presence.SUBSCRIBED:
        break;
      case Presence.UNSUBSCRIBE:
        break;
      case Presence.UNSUBSCRIBED:
        break;
    }
  }
  public void receivedUpdateEvent(Presence presence)
  {
    JabberId jid = service.getJabberId();
    String from = presence.getFrom();
    JabberId fjid = null;
    if (from != null) fjid = new JabberId(from);
//    if (from != null &&  jid.equal(fjid) && (jid.getResource() != fjid.getResource()))
    {
      boolean found = false;
      Iterator iter = myres.iterator();
      while (iter.hasNext())
      {
        RosterResourceElement item = (RosterResourceElement) iter.next();
        String to = presence.getTo();
        if (from != null && item.getJid().equalFull(from))
        {
          found = true;
          String str = presence.getStatus();
          if (str == null) str = presence.getShow();
          if (str == null) str = "Online";
          Browser.getActiveBrowser().getStatusView().setText(from + " : " + str, StatusView.TYPE_WARNING);
//        item.setPresence(presence);
//        item.setJid(new JabberId(from));
//          if (presence.getType() == Presence.AVAILABLE)
//          {
//
            item.setPresence(presence);
//          }
//          else
//          {
////            myres.remove(item);
//          }
          fireTreeResourceStructureChanged(item);
        }
        else if (item.getJid().equalFull(to))
        {
          found = true;
          item.setSentPresence(presence);
          fireTreeResourceStructureChanged(item);
        }
        else if (to == null)
        {
          found = true;
          item.setSentPresence(null);
//       item.setSentPresence(presence);
          fireTreeResourceStructureChanged(item);
        }
      }
      if (!found && from != null &&  jid.equal(fjid) && (jid.getResource() != fjid.getResource())) //if (!found)
      {
        RosterResourceElement item = new RosterResourceElement();
        item.setName(fjid.getResource());
        item.setJid(fjid);
        item.setPresence(presence);
        myres.add(item);
        fireTreeStructureChanged(resources);
      }
    }
//    else if
//    {
//
//    }
    {
      Iterator iter = items.iterator();
      while (iter.hasNext())
      {
        RosterItem item = (RosterItem) iter.next();
//        String from = presence.getFrom();
        String to = presence.getTo();
        if (item.getJid().equal(from))
        {
          String str = presence.getStatus();
          if (str == null) str = presence.getShow();
          if (str == null) str = "Online";
          Browser.getActiveBrowser().getStatusView().setText(from + " : " + str, StatusView.TYPE_WARNING);
//        item.setPresence(presence);
//        item.setJid(new JabberId(from));
          if (presence.getType() == Presence.AVAILABLE)
          {
            item.updateResource(from, presence);
          }
          else
          {
            item.removeResource(from);
          }
          fireTreeContactStructureChanged(item);
        }
        else if (item.getJid().equal(to))
        {
          item.setSentPresence(presence);
          fireTreeContactStructureChanged(item);
        }
        else if (to == null)
        {
          item.setSentPresence(null);
//       item.setSentPresence(presence);
          fireTreeContactStructureChanged(item);
        }
      }
    }
  }

  public void serviceStatusUpdate(int status)
  {
    if (status == JabberService.DISCONNECTED)
    {
      items.clear();
      myres.clear();
      fireTreeStructureChanged(contacts);
      fireTreeStructureChanged(resources);
    }
  }

}

