package net.java.dev.jbuilder.opentool.linemover;

import com.borland.primetime.editor.EditorActions;
import net.java.dev.jbuilder.opentool.linemover.editor.linemover.*;

public class LineMoverOpenTool {
  public static void initOpenTool(byte majorVersion, byte minorVersion) {
    EditorActions.addBindableEditorAction(new MoveLinesDownEditAction(),
                                          EditorActions.GROUP_CARET_MOVEMENT,
                                          "move-lines-down");
    EditorActions.addBindableEditorAction(new MoveLinesUpEditAction(),
                                          EditorActions.GROUP_CARET_MOVEMENT,
                                          "move-lines-up");
    EditorActions.addBindableEditorAction(new DuplicateLinesEditAction(),
                                          EditorActions.GROUP_CARET_MOVEMENT,
                                          "duplicate-lines");
    EditorActions.addBindableEditorAction(new JoinLineAction(),
                                          EditorActions.GROUP_CARET_MOVEMENT,
                                          "join-lines");
  }


}
