package net.java.dev.jbuilder.opentool.linemover.editor.linemover;

import java.awt.event.ActionEvent;

import javax.swing.text.BadLocationException;
import javax.swing.text.Element;

import com.borland.primetime.editor.*;

public class MoveLinesDownEditAction extends MoveLinesEditAction {
  public MoveLinesDownEditAction() {
    super("move-line-down");
  }

  public void actionPerformed(ActionEvent e) {
    EditorPane editor = getEditorTarget(e);
    int caretOffset = editor.getCaretPosition();
    int start = editor.getSelectionStart();
    int selectionEnd = editor.getSelectionEnd();
    int selectionType = editor.getSelectionType();
    int end = selectionEnd - 1;
    if (end < start) {
      end = start;
    }
    EditorDocument document = (EditorDocument) editor.getDocument();
    Element lines = document.getDefaultRootElement();
    Element beginLine = getLine(lines, start);
    int lineIndexToMove = lines.getElementIndex(end) + 1;
    Element lineToMove = lines.getElementCount() == lineIndexToMove ? null :
                         lines.getElement(lineIndexToMove);
    if (lineToMove.getStartOffset() == document.getLength()) {
      return;
    }
    JBUndoManager manager = document.getUndoManager();
    manager.startUndoGroup(caretOffset, caretOffset);
    try {
      int offset = lineToMove.getStartOffset();
      int length = lineToMove.getEndOffset() - offset;
      String content = document.getText(offset, length);
      document.remove(lineToMove.getStartOffset(), length);
      document.insertString(beginLine.getStartOffset(), content, null);
      editor.setCaretPosition(selectionEnd + length - 1);
      editor.setSelectionStart(start + length);
      editor.setSelectionEnd(selectionEnd + length);
    } catch (BadLocationException ex) {
      ex.printStackTrace();
    } finally {
      manager.endUndoGroup(caretOffset, caretOffset);
    }
  }

}
