package net.java.dev.jbuilder.opentool.linemover.editor.linemover;

import java.awt.event.ActionEvent;

import javax.swing.text.BadLocationException;
import javax.swing.text.Element;

import com.borland.primetime.editor.*;

public class JoinLineAction extends MoveLinesEditAction {
  public JoinLineAction() {
    super("join-line");
  }

  public void actionPerformed(ActionEvent e) {
    EditorPane editor = getEditorTarget(e);
    int caretOffset = editor.getCaretPosition();
    EditorDocument document = (EditorDocument) editor.getDocument();
    Element lines = document.getDefaultRootElement();
    int lineIndex = lines.getElementIndex(caretOffset);
    Element currentLine = lines.getElement(lineIndex);
    if (currentLine.getEndOffset() == document.getLength()) {
      return;
    }
    JBUndoManager manager = document.getUndoManager();
    manager.startUndoGroup(caretOffset, caretOffset);
    int finalCaretPosition = caretOffset;
    try {
      String textTilEnd = document.getText(caretOffset,
                                           currentLine.getEndOffset() -
                                           caretOffset);
      StringBuffer buffer = new StringBuffer(textTilEnd.trim());
      finalCaretPosition = caretOffset + buffer.length();
      Element nextLine = lines.getElement(lineIndex + 1);
      int length = nextLine.getEndOffset() - caretOffset;
      String nextLineContent = document.getText(nextLine.getStartOffset(),
                                                nextLine.getEndOffset() -
                                                nextLine.getStartOffset());
      buffer.append(nextLineContent.trim()).append("\n");
      document.remove(caretOffset, length);
      document.insertString(caretOffset, buffer.toString(), null);
      editor.setCaretPosition(finalCaretPosition);

    } catch (BadLocationException ex) {
      ex.printStackTrace();
    } finally {
      manager.endUndoGroup(finalCaretPosition, finalCaretPosition);
    }
  }

}
