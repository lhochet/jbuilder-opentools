package net.java.dev.jbuilder.opentool.linemover.editor.linemover;

import java.awt.event.ActionEvent;

import javax.swing.text.BadLocationException;
import javax.swing.text.Element;

import com.borland.primetime.editor.EditorDocument;
import com.borland.primetime.editor.EditorPane;

public class DuplicateLinesEditAction extends MoveLinesEditAction {
  public DuplicateLinesEditAction() {
    super("duplicate-lines");
  }

  public void actionPerformed(ActionEvent e) {
    EditorPane editor = getEditorTarget(e);
    EditorDocument document = (EditorDocument) editor.getDocument();
    int selectionStart = editor.getSelectionStart();
    int selectionEnd = editor.getSelectionEnd() - 1;
    if (selectionEnd < selectionStart) {
      selectionEnd = selectionStart;
    }
    Element lines = document.getDefaultRootElement();
    int copyStart = getCopyStart(lines, selectionStart);
    int copyEnd = getCopyEnd(lines, selectionEnd);
    try {
      int length = copyEnd - copyStart;
      String content = document.getText(copyStart, length);
      if (copyEnd == document.getLength()) {
        content = "\n" + content;
      }
      document.insertString(copyEnd, content, null);
      editor.select(copyEnd, copyEnd + length);
    } catch (BadLocationException ex) {
      ex.printStackTrace();
    }
  }

  private int getCopyStart(Element lines, int offset) {
    return getLine(lines, offset).getStartOffset();
  }

  private int getCopyEnd(Element lines, int offset) {
    return getLine(lines, offset).getEndOffset();
  }

}
