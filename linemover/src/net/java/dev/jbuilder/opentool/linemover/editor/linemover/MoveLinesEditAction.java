package net.java.dev.jbuilder.opentool.linemover.editor.linemover;

import javax.swing.text.Element;

import com.borland.primetime.editor.EditorAction;

abstract public class MoveLinesEditAction extends EditorAction {
  public MoveLinesEditAction(String name) {
    super(name);
  }

  protected Element getLine(Element lines, int offset) {
    int index = lines.getElementIndex(offset);
    return lines.getElement(index);
  }
}
