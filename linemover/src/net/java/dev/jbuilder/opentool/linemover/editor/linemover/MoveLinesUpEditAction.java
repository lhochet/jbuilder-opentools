package net.java.dev.jbuilder.opentool.linemover.editor.linemover;

import java.awt.event.ActionEvent;

import javax.swing.text.BadLocationException;
import javax.swing.text.Element;

import com.borland.primetime.editor.*;
import com.borland.primetime.editor.EditorPane.*;

public class MoveLinesUpEditAction extends MoveLinesEditAction {
  public MoveLinesUpEditAction() {
    super("move-line-up");
  }

  public void actionPerformed(ActionEvent e) {
    EditorPane editor = getEditorTarget(e);
    SelectionInfo info = editor.getSelectionInfo();
    int start = editor.getSelectionStart();
    int selectionEnd = editor.getSelectionEnd();
    int type = info.selType;
    int end = selectionEnd - 1;
    if (end < start) {
      end = start;
    }
    EditorDocument document = (EditorDocument) editor.getDocument();
    Element lines = document.getDefaultRootElement();
    Element endLine = getLine(lines, end);
    int lineIndexToMove = lines.getElementIndex(start) - 1;
    Element lineToMove = -1 == lineIndexToMove ? null :
                         lines.getElement(lineIndexToMove);
    if (lineToMove == null) {
      return;
    }
    JBUndoManager manager = document.getUndoManager();
    manager.startUndoGroup(info.dot, info.mark, info.selType, info.extra);
    try {
      int offset = lineToMove.getStartOffset();
      int length = lineToMove.getEndOffset() - offset;
      String content = document.getText(offset, length);
      document.insertString(endLine.getEndOffset(), content, null);
      document.remove(lineToMove.getStartOffset(), length);
      editor.getCaret().setDot(selectionEnd - length);
      editor.getCaret().moveDot(start - length);
//            editor.select(start - length, selectionEnd - length, type);
    } catch (BadLocationException ex) {
      ex.printStackTrace();
    } finally {
      manager.endUndoGroup(info.dot, info.mark, info.selType, info.extra);
    }
  }
}
