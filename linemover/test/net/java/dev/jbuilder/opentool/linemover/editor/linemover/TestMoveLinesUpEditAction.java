package net.java.dev.jbuilder.opentool.linemover.editor.linemover;

import java.awt.event.ActionEvent;

import javax.swing.text.Element;

import com.borland.primetime.editor.*;
import junit.framework.TestCase;

public class TestMoveLinesUpEditAction extends TestCase {
  private EditorPane pane;
  private ActionEvent event;
  private MoveLinesUpEditAction moveLinesUpEditAction = null;

  protected void setUp() throws Exception {
    super.setUp();
    pane = new EditorPane();
    pane.setDocument(new EditorDocument());
    pane.setEditorKit(new TextEditorKit());
    event = new ActionEvent(pane, 0, "test");
    moveLinesUpEditAction = new MoveLinesUpEditAction();
  }

  protected void tearDown() throws Exception {
    moveLinesUpEditAction = null;
    event = null;
    pane = null;
    super.tearDown();
  }

  public void testDummy() {}

  public void not_testActionPerformed() {
    String[] lines = {
                     "line one",
                     "line two",
                     "line three",
                     "line four"
    };
    for (int i = 0; i < lines.length; i++) {
      pane.insertAt(i, 0, lines[i] + "\n");
    }
    selectLine(2);
    moveLinesUpEditAction.actionPerformed(event);
    String[] expected = {
                        "line one",
                        "line three",
                        "line two",
                        "line four"
    };
    StringBuffer buffer = new StringBuffer();
    for (int i = 0; i < expected.length; i++) {
      buffer.append(expected[i]).append("\n");
    }
    assertEquals(buffer.toString(), pane.getText());
  }

  private void selectLine(int index) {
    EditorDocument document = (EditorDocument) pane.getDocument();
    Element lines = document.getDefaultRootElement();
    Element line = lines.getElement(index);
    pane.select(line.getStartOffset(), line.getEndOffset());
  }

}
