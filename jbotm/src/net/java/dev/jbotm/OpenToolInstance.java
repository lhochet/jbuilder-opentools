package net.java.dev.jbotm;

import com.borland.primetime.util.*;

/**
 * <p>Title: CodeCentral</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2004</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:06:00 $
 */
public class OpenToolInstance
{
  private String id;
  private Version version;

  public OpenToolInstance(String id, Version version)
  {
    this.id = id;
    this.version = version;
  }

  public String getID()
  {
    return id;
  }

  public Version getVersion()
  {
    return version;
  }
}
