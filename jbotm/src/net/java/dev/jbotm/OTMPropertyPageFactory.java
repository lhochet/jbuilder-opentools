/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm;

import java.util.*;

import com.borland.primetime.properties.*;

import net.java.dev.jbotm.ui.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/03/05 21:57:05 $
 * @created 20/02/05
 */
public class OTMPropertyPageFactory extends PropertyPageFactory
{
  private static OTMPropertyPageFactory instance = new OTMPropertyPageFactory();
  static OTMPropertyPageFactory getInstance()
  {
    return instance;
  }

  private static ArrayList subfactories = new ArrayList();

  private OTMPropertyPageFactory()
  {
    super("OpenTools Manager");
  }

  public PropertyPage createPropertyPage()
  {
    return new OTMPropertyPage();
  }

  public static void registerSubFactory(PropertyPageFactory f)
  {
    subfactories.add(f);
  }

  public PropertyPageFactory[] getNestedFactories()
  {
    return PropertyManager.getPageFactories(OTMPropertyGroup.OTM_CAT);
  }

}
