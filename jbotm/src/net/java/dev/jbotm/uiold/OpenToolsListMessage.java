/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.uiold;

import java.util.*;

import com.borland.primetime.actions.*;
import com.borland.primetime.ide.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.structs.OpenToolEntry;

import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.old.*;

/**
 * @author Ludovic HOCHET
 * @version 0.2, 11/12/01
 * 0.1, 25/07/01
 * @deprecated
 */
public class OpenToolsListMessage extends Message
{
  public static final String NON_EXPANDED_TEXT = "All OpenTools... (double click to expand)";
  public static final String EXPANDED_TEXT = "All OpenTools...";

  private Collection entries = null;
  private MessageCategory cat = null;
  private CodeCentralChecker checker = null;

  private ActionGroup OTMContextActions = new ActionGroup();
  private ActionGroup sortActions = null;
  private ActionGroup filterActions = null;

  public OpenToolsListMessage(CodeCentralChecker checker, MessageCategory cat)
  {
    super(EXPANDED_TEXT);
    this.cat = cat;
    this.checker = checker;

    setLazyFetchChildren(true);

    if (sortActions == null)
    {
      initStaticStdMenuActions();
    }
    OTMContextActions.add(sortActions);
    OTMContextActions.add(filterActions);
    setContextAction(OTMContextActions);
  }

  public void setEntries(Collection entries)
  {
    this.entries = entries;
  }

  public void fetchChildren(Browser browser)
  {
    MessageView mv = browser.getMessageView();

    setText(EXPANDED_TEXT);

    if (entries == null)
    {
      return;
    }

    mv.removeChildren(cat, this);
    Iterator iter = entries.iterator();
    while (iter.hasNext())
    {
      OpenToolEntry entry = (OpenToolEntry)iter.next();
      if (entry.isShown())
      {
        mv.addMessage(cat, this, new OpenToolMessage(checker, entry), false);
      }
    }
  }

  private void initStaticStdMenuActions()
  {
    sortActions = checker.getSortActions();
    ActionGroup firstLevel = new ActionGroup("Search...");
    firstLevel.add(checker.getFilterActions());
    firstLevel.setPopup(true);
    filterActions = firstLevel;
  }


}
