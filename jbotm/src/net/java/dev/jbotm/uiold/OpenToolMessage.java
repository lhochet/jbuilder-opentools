/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.uiold;

import javax.swing.tree.*;

import com.borland.jbuilder.ide.*; // for JBuilderIcons, also exist in CBX 1.0
import com.borland.primetime.actions.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.ccold.CodeCentralOTDownloader;
import net.java.dev.jbotm.repo.ccold.CodeCentralOTEntryPageLoader;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.repo.ccold.CodeCentralEntry;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.old.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.9 $ $Date: 2005/03/05 21:58:04 $
 * 0.6, 14/08/02
 * 0.5, 11/12/01
 * 0.4, 23/11/01
 * 0.3, 23/09/01
 * 0.1, 25/07/01
 * @deprecated 
 */
public class OpenToolMessage extends Message
{
  public static class TrackAction extends StateAction
  {
    private CodeCentralChecker checker;
    private OpenToolEntry entry = null;

    public TrackAction(CodeCentralChecker checker, OpenToolEntry entry)
    {
      super("Tracked", 'T', "Track this OpenTool", null, true);
      this.checker = checker;
      this.entry = entry;
    }

    public void setState(java.lang.Object source, boolean state)
    {
      entry.setTracked(state);
      checker.saveEntries();
    }

    public boolean getState(java.lang.Object source)
    {
      return entry.isTracked();
    }
  }

  public static class ThisAuthorSearchAction extends BrowserAction
  {
    private CodeCentralChecker checker;
    private OpenToolEntry entry;

    public ThisAuthorSearchAction(CodeCentralChecker checker, OpenToolEntry entry)
    {
      super("This Author");
      this.checker = checker;
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {
      checker.searchOn(browser, OTMPropertyGroup.THIS_AUTHOR_FILTER, entry.getAuthorName());
    }
  }

  public static class UpdatePropertiesAction extends BrowserAction
  {
    private OpenToolEntry entry;

    public UpdatePropertiesAction(OpenToolEntry entry)
    {
      super("Update properties", 'U', "Explicitely update this opentool properties.");
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {

      browser.getStatusView().setText("Updating properties for '" + entry.getTitle() + "'...");
      Thread thread = new Thread(new Runnable()
      {
        public void run()
        {
          try
          {
            entry.updateProperties();
            Browser.getActiveBrowser().getStatusView().setText("Properties for '" + entry.getTitle() + "' updated.");
          }
          catch (Exception ex)
          {
            Browser.getActiveBrowser().getStatusView().setText("Failed to update properties for '" + entry.getTitle() + "'. (" +
              ex.getClass().toString() + " : " + ex.getLocalizedMessage() + ")", StatusView.TYPE_ERROR);
            ex.printStackTrace();
          }
        }
      });
      thread.start();
    }
  }

  public static class PropertiesAction extends BrowserAction
  {
    private OpenToolEntry entry;

    public PropertiesAction(OpenToolEntry entry)
    {
      super("Properties...", 'P', "Show the full properties for this opentool.");
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {
      if (!entry.isFullyLoaded())
      {
        try
        {
          entry.updateProperties();
        }
        catch (Exception ex)
        {
          Browser.getActiveBrowser().getStatusView().setText("Failed to get extended properties for '" + entry.getTitle() + "'. (" +
            ex.getClass().toString() + " : " + ex.getLocalizedMessage() + ")", StatusView.TYPE_ERROR);
          ex.printStackTrace();
        }
      }

      PropertiesPanel panel = new PropertiesPanel(entry);
      DefaultDialog.showModalDialog(Browser.getActiveBrowser(), "OpenTool '" + entry.getTitle() + "' Properties", panel, panel.getHelpPage(), panel);

    }
  }

  public static class DownloadAction extends BrowserAction
  {
    private CodeCentralChecker checker;
    private OpenToolEntry entry;

    public DownloadAction(OpenToolEntry entry)
    {
      super("Download", 'D', "Download the selected OpenTool");
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {

      Thread thread = new Thread()
      {
        public void run()
        {
          CodeCentralOTDownloader d = new CodeCentralOTDownloader(entry);
          try
          {
            d.load();
          }
          catch (Exception ex)
          {
          }
        }
      };

      thread.start();
    }
  }

  public static class SortAscAction extends StateAction
  {
    private CodeCentralChecker checker;
    private boolean selected = false;

    public SortAscAction(CodeCentralChecker checker)
    {
      super("Ascending direction", 'A', "Sort in ascending order", null, true);
      this.checker = checker;
    }

    public void setState(java.lang.Object source, boolean state)
    {
      selected = state;
      if (state)
      {
        if (prevDirectionAction != this)
        {
          prevDirectionAction.setState(null, false);
          prevDirectionAction = this;
        }
        OTMPropertyGroup.SORT_DIRECTION.setBoolean(OTMPropertyGroup.ASC);
        if (source instanceof com.borland.primetime.ide.Browser)
        {
          checker.showList();
        }
      }
    }

    public boolean getState(java.lang.Object source)
    {
      return selected;
    }
  }

  public static class SortDescAction extends StateAction
  {
    private CodeCentralChecker checker;
    private boolean selected = false;

    public SortDescAction(CodeCentralChecker checker)
    {
      super("Descending direction", 'D', "Sort in descending order", null, true);
      this.checker = checker;
    }

    public void setState(java.lang.Object source, boolean state)
    {
      selected = state;
      if (state)
      {
        if (prevDirectionAction != this)
        {
          prevDirectionAction.setState(null, false);
          prevDirectionAction = this;
        }
        OTMPropertyGroup.SORT_DIRECTION.setBoolean(OTMPropertyGroup.DESC);
        if (source instanceof com.borland.primetime.ide.Browser)
        {
          checker.showList();
        }
      }
    }

    public boolean getState(java.lang.Object source)
    {
      return selected;
    }
  }

  public static class SortIDAction extends StateAction
  {
    private CodeCentralChecker checker;
    private boolean selected = false;

    public SortIDAction(CodeCentralChecker checker)
    {
      super("ID", 'I', "Sort by ID");
      this.checker = checker;
      setGrouped(true);
    }

    public void setState(java.lang.Object source, boolean state)
    {
      selected = state;
      if (state)
      {
        if (prevByAction != this)
        {
          prevByAction.setState(null, false);
          prevByAction = this;
        }
        OTMPropertyGroup.SORT_BY.setValue(OTMPropertyGroup.ID);
        if (source instanceof com.borland.primetime.ide.Browser)
        {
          checker.showList();
        }
      }
    }

    public boolean getState(java.lang.Object source)
    {
      return selected;
    }
  }

  public static class SortTitleAction extends StateAction
  {
    private CodeCentralChecker checker;
    private boolean selected = false;

    public SortTitleAction(CodeCentralChecker checker)
    {
      super("Title", 'T', "Sort by title", null, true);
      this.checker = checker;
    }

    public boolean isGrouped()
    {
      return true;
    }

    public void setState(java.lang.Object source, boolean state)
    {
      selected = state;
      if (state)
      {
        if (prevByAction != this)
        {
          prevByAction.setState(null, false);
          prevByAction = this;
        }
        OTMPropertyGroup.SORT_BY.setValue(OTMPropertyGroup.TITLE);
        if (source instanceof com.borland.primetime.ide.Browser)
        {
          checker.showList();
        }
      }
    }

    public boolean getState(java.lang.Object source)
    {
      return selected;
    }
  }

  public static class SortAuthorAction extends StateAction
  {
    private CodeCentralChecker checker;
    private boolean selected = false;

    public SortAuthorAction(CodeCentralChecker checker)
    {
      super("Author", 'A', "Sort by author");
      this.checker = checker;
      setGrouped(true);
    }

    public void setState(java.lang.Object source, boolean state)
    {
      selected = state;
      if (state)
      {
        if (prevByAction != this)
        {
          prevByAction.setState(null, false);
          prevByAction = this;
        }
        OTMPropertyGroup.SORT_BY.setValue(OTMPropertyGroup.AUTHOR);
        if (source instanceof com.borland.primetime.ide.Browser)
        {
          checker.showList();
        }
      }
    }

    public boolean getState(java.lang.Object source)
    {
      return selected;
    }
  }

  public static class SortDateAction extends StateAction
  {
    private CodeCentralChecker checker;
    private boolean selected = false;

    public SortDateAction(CodeCentralChecker checker)
    {
      super("Date", 'D', "Sort by date");
      this.checker = checker;
      setGrouped(true);
    }

    public void setState(java.lang.Object source, boolean state)
    {
      selected = state;
      if (state)
      {
        if (prevByAction != this)
        {
          prevByAction.setState(null, false);
          prevByAction = this;
        }
        OTMPropertyGroup.SORT_BY.setValue(OTMPropertyGroup.DATE);
        if (source instanceof com.borland.primetime.ide.Browser)
        {
          checker.showList();
        }
      }
    }

    public boolean getState(java.lang.Object source)
    {
      return selected;
    }
  }

  public static class SortDownloadAction extends StateAction
  {
    private CodeCentralChecker checker;
    private boolean selected = false;

    public SortDownloadAction(CodeCentralChecker checker)
    {
      super("Download", 'o', "Sort by number of downloads");
      this.checker = checker;
      setGrouped(true);
    }

    public void setState(java.lang.Object source, boolean state)
    {
      selected = state;
      if (state)
      {
        if (prevByAction != this)
        {
          prevByAction.setState(null, false);
          prevByAction = this;
        }
        OTMPropertyGroup.SORT_BY.setValue(OTMPropertyGroup.DOWNLOAD);
        if (source instanceof com.borland.primetime.ide.Browser)
        {
          checker.showList();
        }
      }
    }

    public boolean getState(java.lang.Object source)
    {
      return selected;
    }
  }

  public static StateAction prevByAction = null;
  public static StateAction prevDirectionAction = null;

  private ActionGroup OTMContextActions = new ActionGroup();
  private ActionGroup sortActions = null;

  private ActionGroup filterActions = null;
  private ActionGroup thisentryfilterActions = new ActionGroup("Search...");

  private CodeCentralChecker checker = null;
  private OpenToolEntry entry = null;

  public OpenToolMessage(CodeCentralChecker checker, OpenToolEntry entry)
  {
    this.checker = checker;
    this.entry = entry;

    if (sortActions == null)
    {
      initStaticStdMenuActions();
    }

    OTMContextActions.add(new DownloadAction(entry));
    OTMContextActions.add(new PropertiesAction(entry));
    OTMContextActions.add(new UpdatePropertiesAction(entry));
    thisentryfilterActions.add(filterActions);
    thisentryfilterActions.add(new ThisAuthorSearchAction(checker, entry));
    thisentryfilterActions.setPopup(true);
    OTMContextActions.add(new TrackAction(checker, entry));

    OTMContextActions.add(sortActions);
    OTMContextActions.add(thisentryfilterActions);
    setContextAction(OTMContextActions);
  }

  public String toString()
  {
    return entry.getIdInRepository() + ":" + entry.getTitle();
  }

  public TreeCellRenderer  getCellRenderer()
  {
    int downloads = entry.getNbDownloads();
    int prvDownloads = entry.getPrevNbDownloads();

    String txt = entry.getIdInRepository() + ": " + entry.getTitle() + " [" + entry.getAuthorName() + "]";
    if (entry.getNbNewComments() > 0)
    {
      if (entry.getNbNewComments() == 1)
      {
        txt += " (1 new comment)";
      }
      else
      {
        txt += " (" + entry.getNbNewComments() + " new comments)";
      }
    }

    if (OTMPropertyGroup.SHOW_DOWNLOADS.getBoolean())
    {
      txt += " [" + downloads;
      if (OTMPropertyGroup.SHOW_DOWNLOAD_CHANGES.getBoolean())
      {
        txt += " (+" + (downloads - prvDownloads) + ")";
      }
      txt += " downloads]";
    }

    setText(txt);
    /** @todo the last modified date should probably be formatted to the local user coventions */
    String tiptxt = "<html><body><b>ID:</b> " + entry.getIdInRepository() + "<br><b>Title:</b> " + entry.getTitle() + "<br><b>Author:</b> " + entry.getAuthorName() +
      "<br><b>Version:</b> " + entry.getRunMinVersion() + " - " + entry.getRunMaxVersion() +
      "<br><b>Last Modified:</b> " + entry.getLastModifiedDate() + "<br>"
      + "<b>Downloads:</b> " + downloads + " (" + prvDownloads;
    if (OTMPropertyGroup.SHOW_DOWNLOAD_CHANGES.getBoolean())
    {
      tiptxt += " (+" + (downloads - prvDownloads) + ")";
    }
    tiptxt += ")";
    if (entry.getNbComments() > 0)
    {
      tiptxt += "<br><b>Comments:</b> ";
      tiptxt += entry.getNbComments();
    }
    if (entry.getProperty(CodeCentralEntry.RATING) != null)
    {
      tiptxt += "<br><b>Average rating:</b> ";
      tiptxt += entry.getProperty(CodeCentralEntry.RATING) + " with ";
      tiptxt += entry.getProperty(CodeCentralEntry.RATINGRESPONSES) + " response(s), rating system : ";
      tiptxt += entry.getProperty(CodeCentralEntry.RATINGSYSTEM);
    }
    if (entry.getShortDescription() != null)
    {
      tiptxt += "<br><b>Short description:</b> " + entry.getShortDescription();
    }
    tiptxt += "</body></html>";
    setToolTipText(tiptxt);
    if (net.java.dev.jbuilder.opentools.community.Community.jbOTAPI.getMinor() < 6)
    {
//      setIcon(BrowserIcons.ICON_ZIPFOLDER); // in JB8 it is there... comment it so that the same OT can still be used with JB8
    }
    else
    {
      setIcon(JBuilderIcons.ICON_ZIPFOLDER);
    }
    return super.getCellRenderer();
  }

  private void initStaticStdMenuActions()
  {
    sortActions = checker.getSortActions();
    filterActions = checker.getFilterActions();
  }

}
