/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.jar.*;

import com.borland.primetime.util.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005/01/23 22:05:56 $
 * @created 7/12/04
 */
public class OpenToolsManager
{
  private static Map ots = new HashMap();
  private static boolean first = true;

  private OpenToolsManager()
  {
  }

  public static OpenToolInstance getInstance(String id)
  {
    if (first)
    {
      registerOTs();
      first = false;
    }
    return (OpenToolInstance)ots.get(id);
  }

  private static void registerOTs()
  {
    String cp = System.getProperty("java.class.path");
    String ps = System.getProperty("path.separator");
    StringTokenizer tk = new StringTokenizer(cp, ps);
    while (tk.hasMoreTokens())
    {
      String pe = tk.nextToken();
      System.out.println("" + pe);

      File f = new File(pe);
      if (f.isDirectory()) continue;

      try
      {
        JarFile jar = new JarFile(f);
        Manifest manifest = jar.getManifest();
        if (manifest == null) continue;

        Attributes attrs = manifest.getMainAttributes();
        for (Iterator it=attrs.keySet().iterator(); it.hasNext(); ) {
            // Get attribute name
            Attributes.Name attrName = (Attributes.Name)it.next();
            String name = attrName.toString();
            if (name.startsWith("OpenTools-"))
            {
              // Get attribute value
              String attrValue = attrs.getValue(attrName);
              String cat = name.substring("OpenTools-".length());
//              System.out.println(cat + ": " + attrValue);
              StringTokenizer tk2 = new StringTokenizer(attrValue, " ");
              while (tk2.hasMoreTokens())
              {
                String className = tk2.nextToken();

                try
                {
                  Class clazz = Class.forName(className);

                  Method[] methods = clazz.getMethods();
                  for (int i = 0; i < methods.length; i++)
                  {
                    Method m = methods[i];
                    if (m.getName().equals("getOpenToolVersion"))
                    {
                      int mod = m.getModifiers();
                      if (Modifier.isPublic(mod) && Modifier.isStatic(mod))
                      {
                        Object o = m.invoke(null, new Object[]{});
                        if (o instanceof Version)
                        {
                          Version version = (Version) o;
                          OpenToolInstance inst = new OpenToolInstance(className, version);
                          ots.put(className, inst);
                        }
                      }
                    }
                  }
                }
                catch (Exception ex)
                {
                }
              }

            }

        }
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
      }
    }
  }
}
