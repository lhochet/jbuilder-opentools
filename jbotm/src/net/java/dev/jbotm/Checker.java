/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm;

import java.text.*;
import java.util.*;
import java.util.logging.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.repo.*;
import net.java.dev.jbotm.structs.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.4 $ $Date: 2005/01/14 14:49:04 $
 * @created 27/11/04
 */
public class Checker
{
  private Logger log = Logger.getLogger(Checker.class.getName());

  private Product product = null;
  private Map localEntries = null;
  private List repoEntries = null;
  private List opentools = null;
  private List newOpentools = null;
  private List updatedOpentools = null;
  private List newlyCommentedOpentools = null;
  private List trackedOpentools = null;

  public Checker(Product product)
  {
    this.product = product;
  }

  public Product getProduct()
  {
    return product;
  }

  public List getOpenTools()
  {
    return opentools;
  }

  public List getNewOpenTools()
  {
    return newOpentools;
  }

  public List getUpdatedOpenTools()
  {
    return updatedOpentools;
  }

  public List getNewlyCommentedOpenTools()
  {
    return newlyCommentedOpentools;
  }

  public List getTrackedOpenTools()
  {
    return trackedOpentools;
  }

  public void run()
  {
    long start = System.currentTimeMillis();
    log.entering("Checker", "run");

    loadLocal();

    String productId = product.getID();
    List repositories = RepositoryManager.getRepositories();
    Iterator iter = repositories.iterator();
    while (iter.hasNext())
    {
      Repository repository = (Repository) iter.next();
      if (repository.isChecked())
      {
        if (repository.getProduct().equals(productId))
        {
          checkRepository(repository);
        }
      }
    }

    reconcile();
//    //    updateTracked();
//        if (CodeCentralPropertyGroup.AUTO_LOAD_ENTRIES.getBoolean())
//        {
//          autoLoadEntries();
//        }

    writeLocal();

    log.exiting("Checker", "run", "time: " + (System.currentTimeMillis() - start) + "ms");
  }

  public void runLocal()
  {
    long start = System.currentTimeMillis();
    log.entering("Checker", "runLocal");

    loadLocal();

//    String productId = product.getID();
//    List repositories = RepositoryManager.getRepositories();
//    Iterator iter = repositories.iterator();
//    while (iter.hasNext())
//    {
//      Repository repository = (Repository) iter.next();
//      if (repository.isChecked())
//      {
//        if (repository.getProduct().equals(productId))
//        {
//          checkRepository(repository);
//        }
//      }
//    }
//

    reconcile();
//    //    updateTracked();
//        if (CodeCentralPropertyGroup.AUTO_LOAD_ENTRIES.getBoolean())
//        {
//          autoLoadEntries();
//        }

    writeLocal();

    log.exiting("Checker", "runLocal", "time: " + (System.currentTimeMillis() - start) + "ms");
  }

  private void loadLocal()
  {
    XMLOTListFileManager fman = product.getFileManager();
    fman.load();
    localEntries = fman.getEntries();
  }

  private void checkRepository(Repository repository)
  {
    try
    {
      repository.check();
      List lst = repository.getEntries();
      if (lst.size() > 0)
      {
        if (repoEntries == null)
        {
          repoEntries = new ArrayList(lst);
        }
        else
        {
          /** @todo reconcile with or add to exiting list */
          repoEntries.addAll(lst);
        }
      }
    }
    catch (Exception ex)
    {
      Browser.getActiveBrowser().getStatusView().setText("Error checking \"" + repository.getName() + "\"... (" +
                                      ex.getClass().toString() + " : " +
                                      ex.getLocalizedMessage() + ")",
                                      StatusView.TYPE_ERROR);
      ex.printStackTrace();
    }
  }

  private void reconcile()
  {
    log.entering("Checker", "reconcile()");
    newOpentools = new ArrayList();
    updatedOpentools = new ArrayList();
    trackedOpentools = new ArrayList();
    newlyCommentedOpentools = new ArrayList();

    if ((repoEntries == null) || (repoEntries.size() == 0))
    {
      if (localEntries != null)
      {
        opentools = new ArrayList(localEntries.values());
        Iterator iter = opentools.iterator();
        while (iter.hasNext())
        {
          OpenToolEntry item = (OpenToolEntry)iter.next();
          if (item.isTracked())
          {
            item.setPrevNbDownloads(item.getNbDownloads());
            trackedOpentools.add(item);
          }
        }
      }
    }
    else
    {

      Iterator iter = repoEntries.iterator();
      while (iter.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry) iter.next();

        OpenToolEntry entry = null;
        try
        {
          entry = (OpenToolEntry) localEntries.get(item.getID());
        }
        catch (Exception ex)
        {
          log.log(Level.FINE, "Could not get local entry for id = " + item.getID(), ex);
        }

        if (entry == null)
        {
          item.setNew(true);
          localEntries.put(item.getID(), item);
          newOpentools.add(item);
        }
        else
        {
          try
          {
            entry.setPrevNbDownloads(entry.getNbDownloads());
            entry.setTitle(item.getTitle()); // always update this field
            entry.setShortDescription(item.getShortDescription()); // always update this field
            entry.setNbDownloads(item.getNbDownloads()); // always update this field
            entry.setRunMinVersion(item.getRunMinVersion()); // always update this field
            entry.setRunMaxVersion(item.getRunMaxVersion()); // always update this field
            entry.setNbNewComments(item.getNbComments() - entry.getNbComments());
            entry.setNbComments(item.getNbComments());

            if (entry.getNbNewComments() > 0)
            {
              newlyCommentedOpentools.add(entry);
            }

            if (entry.isTracked())
            {
              trackedOpentools.add(entry);
            }

            if (item.getLastModifiedDate().after(entry.getLastModifiedDate()))
            {
              entry.setLastModifiedDate(item.getLastModifiedDate());
              entry.setUpdated(true);
              updatedOpentools.add(entry);
            }
          }
          catch (Exception ex)
          {
            log.log(Level.FINE, "Could not reconcile entry id=" + item.getID(), ex);
          }
        }
      }
      // create all entries list
      opentools = new ArrayList(localEntries.values());
    }

    log.exiting("Checker", "reconcile()");
  }

  public void writeLocal()
  {
    XMLOTListFileManager fman = product.getFileManager();
    fman.setEntries(localEntries);
    fman.write();
  }
}
