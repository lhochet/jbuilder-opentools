/*
   Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.structs;

import java.util.*;

/**
 * @author Ludovic HOCHET
 * @version 0.1, 18/02/02
 */
public class CommentEntry
{
  public String id = null;
  public String title = null;
  public String author = null;
  public String date = null;
  public String comment = null;
  public ArrayList subcomments = null;

  public String toString()
  {
    return
    /*id + ">" +*/ title + " (" + author + " - " + date + ")"; // + comment;
  }

//  public String toXMLString()
//  {
//    StringBuffer ret = new StringBuffer();
//    ret.append("<comment id=\"" + id + "\" date=\"" + date + "\" title=\"" + title + "\" author=\"" + author + "\" >" + comment);
//    if (subcomments != null)
//    {
//      Iterator iter = subcomments.iterator();
//      while (iter.hasNext())
//      {
//        CommentEntry item = (CommentEntry)iter.next();
//        ret.append(item.toXMLString());
//      }
//    }
//
//    ret.append("<comment />\n");
//    return ret.toString();
//  }

}
