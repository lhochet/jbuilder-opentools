/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.structs;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import com.borland.primetime.util.*;

import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.ui.EntryInfosPanel;
import net.java.dev.jbotm.ui.EntryDescriptionPanel;
import net.java.dev.jbotm.ui.EntryDownloadPanel;
import net.java.dev.jbotm.ui.EntryFilesPanel;
import net.java.dev.jbotm.ui.EntryCommentsPanel;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.4 $ $Date: 2005/01/23 22:05:16 $
 * @created 4/07/01
 */

public class OpenToolEntry
{
  private Logger log = Logger.getLogger(OpenToolEntry.class.getName());

  // keys for other props
  // (some are likely to be moved to a codecentral subclass of OTE)
  public static final String TERMS = "terms"; //CC
  public static final String ONCD = "oncd"; //CC
  public static final String COPYRIGHT = "copyright"; //CC
  public static final String OT_SITE_URL = "ot_site_url"; //CC
  public static final String ORIGINAL_UPLOAD = "original_upload"; //CC
  public static final String SIZE = "size";
  public static final String LAST_DOWNLOADED = "last_downloaded";
  public static final String LOCAL_NAME = "local_name";

  /** OpenTool identifier */
  private String id = null;
  /** identifier of the repository from which this OpenTool comes from */
  private Repository repository = null;
  /** identifier in the repository for this OpenTool */
  private String rid;
  /** author of the OpenTool */
  private Author author = null;
  /** title of the OpenTool entry */
  private String title = null;
  /** short description of the OpenTool */
  private String shortDesc = null;
  /** long description of the OpenTool */
  private String description = null;
  /** date this opentool was last modified */
  private Date lastModifiedDate = null;
  /** minimum version of (jbuilder) needed to run the OpenTool */
  private Version min_run_version;
  /** maximum version of (jbuilder) supported to run the OpenTool */
  private Version max_run_version;
  /** version of this OpenTool */
  private Version version = null;

  /** file entries */
  private ArrayList files = null;

  /** flag indicating that the OpenTool is new to the local repository */
  private boolean isNew = false;
  /** flag indicating that the OpenTool has been updated */
  private boolean isUpdated = false;
  /** flag indicating that the OpenTool is tracked, ie that it always shown in the summary view in the tracked 'section' */
  private boolean isTracked = false;
  /** flag indicating that the OpenTool entry is shown in the different views (summary and list) */
  private boolean isShown = true;

  /** should automatically download the OT if it is modified */
  private boolean autoDownload = false;

  // CC properties ?
  private ArrayList comments = null;
  private int nbcomments = 0;
  private int nbnewcomments = 0;

  private int nbdownloads = 0;
  private int prevnbdownload = 0;

  // tmp
  private boolean fullyLoaded = false;

  // checker local instance
  Checker checker = null;


  /** other properties */
  private Properties props = new Properties();


  public OpenToolEntry()
  {
  }

  // id
  public void setID(String id)
  {
    this.id = id;
  }
  public String getID()
  {
    return id; // new repo independent id
  }

  // title
  public void setTitle(String title)
  {
    this.title = title;
  }
  public String getTitle()
  {
    return title;
  }

  // repository
  public void setRepository(Repository repository)
  {
    this.repository = repository;
  }
  public Repository getRepository()
  {
    return repository;
  }

  // repo id
  public void setIdInRepository(String repoId)
  {
    this.rid = repoId;
//    if (id == null) id = repoId;
  }
  public String getIdInRepository()
  {
    return rid;
  }

  // author
  public void setAuthor(Author author)
  {
    this.author = author;
  }
  public Author getAuthor()
  {
    return author;
  }
  // helper to set/get the author name
  public void setAuthorName(String name)
  {
    if (author == null)
    {
      author = new Author();
      author.setName(name);
    }
    else
    {
      author.setName(name);
    }
  }
  public String getAuthorName()
  {
    if (author == null) return null;
    return author.getName();
  }

  // description
  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  // short description
  public void setShortDescription(String shortDescription)
  {
    this.shortDesc = shortDescription;
  }
  public String getShortDescription()
  {
    if (shortDesc == null) return description;
    return shortDesc;
  }

  // file entries
  public void setFiles(ArrayList files_)
  {
    this.files = files_;
  }
  public ArrayList getFiles()
  {
    return files;
  }

  // last updated date
  public void setLastModifiedDate(Date date)
  {
    this.lastModifiedDate = date;
  }
  public Date getLastModifiedDate()
  {
    return lastModifiedDate;
  }

  // autodownload
  /** @todo (should be changed to autoupdate?) */
  public void setAutoDownload(boolean autodownload)
  {
    this.autoDownload = autodownload;
  }
  public boolean isAutoDownload()
  {
    return autoDownload;
  }

  // min jb version to run
  public void setRunMinVersion(Version vrs)
  {
    this.min_run_version = vrs;
  }
  public Version getRunMinVersion()
  {
    return min_run_version;
  }

  // max jb version to run
  public void setRunMaxVersion(Version vrs)
  {
    this.max_run_version = vrs;
  }
  public Version getRunMaxVersion()
  {
    return max_run_version;
  }

  // version
  public void setVersion(Version vrs)
  {
    this.version = vrs;
  }
  public Version getVersion()
  {
    return version;
  }

  // other properties
  public void setProperty(String key, String value)
  {
    if (value == null)
    {
      props.remove(key);
    }
    else
    {
      props.setProperty(key, value);
    }
  }
  public String getProperty(String key)
  {
    return props.getProperty(key);
  }
  public Properties getProperties()
  {
    return props;
  }
  public List getSavedKeys()
  {
    List lst = new ArrayList(11);
    lst.add(LOCAL_NAME);
    lst.add(SIZE);
    lst.add(LAST_DOWNLOADED);
    lst.add(ORIGINAL_UPLOAD);
    lst.add(OT_SITE_URL);
    lst.add(COPYRIGHT);
    lst.add(TERMS);
    lst.add(ONCD);

    return lst;
  }

////
  // isNew
  public void setNew(boolean isNew)
  {
    this.isNew = isNew;
  }
  public boolean isNew()
  {
    return isNew;
  }

  // isUpdated
  public void setUpdated(boolean isUpdated)
  {
    this.isUpdated = isUpdated;
  }
  public boolean isUpdated()
  {
    return isUpdated;
  }

  // isTracked
  public void setTracked(boolean isTracked)
  {
    this.isTracked = isTracked;
  }
  public boolean isTracked()
  {
    return isTracked;
  }

  // isShown
  public void setShown(boolean isShown)
  {
    this.isShown = isShown;
  }
  public boolean isShown()
  {
    return isShown;
  }

////
  public OpenToolInstance getInstance()
  {
    return OpenToolsManager.getInstance(id);
  }

  public boolean isDownloaded()
  {
    String localName = getProperty(LOCAL_NAME);
    if (localName == null) return false;
    File f = new File(localName);
    return f.exists();
  }

  public boolean isInstalled()
  {
    return getInstance() != null;
  }

  public Version getInstalledVersion()
  {
    OpenToolInstance oti = getInstance();
    if (oti != null)
    {
      return oti.getVersion();
    }
    return null;
  }

  public void setFullyLoaded()
  {
    fullyLoaded = true;
  }
  public boolean isFullyLoaded()
  {
    return fullyLoaded;
  }

  public void updateProperties() throws OTMException
  {
    log.fine("Updating properties for " + title + "...");
    if (repository == null) throw new OTMException("Repository is not set");

    repository.updatePropertiesFor(this);
    save();
  }

  public void save()
  {
    if (checker == null)
    {
      Product product = ProductManager.getProduct(repository.getProduct());
      checker = product.getChecker();
    }
    checker.writeLocal();
  }

  public List getPropertyPages()
  {
    ArrayList lst = new ArrayList(5);
    lst.add(new EntryInfosPanel(this));
    lst.add(new EntryDescriptionPanel(this));
    lst.add(new EntryFilesPanel(this));
    lst.add(new EntryDownloadPanel(this));
//    lst.add(new EntryCommentsPanel(this));
    return lst;
  }

////
  public void download() throws OTMException
  {
    log.fine("Downloading " + title + "...");
    if (repository == null) throw new OTMException("Repository is not set");
    if (repository.equals("")) throw new OTMException("Repository is empty");

    repository.download(this);
  }

  public void install() throws OTMException
  {
    log.fine("Installing " + title + "...");
  }

  // CC methods ?
  // comments
  public void setComments(ArrayList comments)
  {
    this.comments = comments;
  }
  public ArrayList getComments()
  {
    return comments;
  }
  // nb comments
  public void setNbComments(int nbcomments)
  {
    this.nbcomments = nbcomments;
  }
  public int getNbComments()
  {
    return nbcomments;
  }
  // nb new comments
  public void setNbNewComments(int nbnewcomments)
  {
    this.nbnewcomments = nbnewcomments;
  }
  public int getNbNewComments()
  {
    return nbnewcomments;
  }

  // downloads
  public void setNbDownloads(int nb)
  {
    this.nbdownloads = nb;
  }
  public int getNbDownloads()
  {
    return nbdownloads;
  }
  // prev downloads
  public void setPrevNbDownloads(int nb)
  {
    this.prevnbdownload = nb;
  }
  public int getPrevNbDownloads()
  {
    return prevnbdownload;
  }

}
