/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.structs;

/**
 * @author Ludovic HOCHET
 * @version 0.2, 14/12/01
 * 0.1, 3/10/01
 */

public class FileEntry
{
  public String name = null;
  public String date = null;
  public String size = null;
  public String extractPath = null;
  public boolean autoExtract = false;

  public FileEntry()
  {
  }

  public String toString()
  {
    return name + ">" + date + ">" + size + ">" + autoExtract + ">" + extractPath;
  }

  public String toXMLString()
  {
    StringBuffer ret = new StringBuffer();
    ret.append("<file name=\"" + name + "\" date=\"" + date + "\" size=\"" + size + "\" autoExtract=\"" + autoExtract + "\" extractPath=\"" +
               extractPath + "\" / >\n");
    return ret.toString();
  }
}
