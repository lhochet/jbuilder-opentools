/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.structs;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:07:34 $
 * @created 9/01/05
 */
public class Author implements Comparable
{
  private String name;
  private String email;

  public Author()
  {
  }

  // name
  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  // email
  public void setEmail(String email)
  {
    this.email = email;
  }
  public String getEmail()
  {
    return email;
  }

  public int compareTo(Object o)
  {
    if (o == null) return 1;
    if (!(o instanceof Author)) return 1;

    if (name == null) return -1;

    Author a = (Author)o;
    return name.compareTo(a.getName());
  }
}
