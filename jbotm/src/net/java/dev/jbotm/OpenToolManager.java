/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm;

import java.text.*;
import java.util.*;
import java.util.logging.*;

import com.borland.primetime.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.properties.*;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbuilder.opentools.community.*;
import net.java.dev.jbotm.products.*;
import com.borland.primetime.util.*;
import net.java.dev.jbotm.view.props.OTMsPropertyPageFactory;
import com.borland.primetime.personality.*;
import net.java.dev.jbotm.repo.cc.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.5 $ $Date: 2005/03/05 21:58:41 $
 * @created 4/07/01
 */
public class OpenToolManager implements Agent
{
  private static Logger log = Logger.getLogger(OpenToolManager.class.getName());

  public static class ShowOTs extends BrowserAction
  {
    private Product product;

    public ShowOTs(Product product)
    {
      super("Show " + product.getName() + " OpenTools", product.getName().charAt(0),
            "Show " + product.getName() + " OpenTools");
      this.product = product;
    }

    public void actionPerformed(Browser browser)
    {
      if (product.isChecked())
      {
        ProductManager.showOTs(product);
      }
    }
  }


  public static class CheckOpenToolsAction extends BrowserAction
  {
    public CheckOpenToolsAction()
    {
      super("Check for new/updated OpenTools...", 'C',
            "Check whether there are new/updated Opentools",
            BrowserIcons.ICON_HELPBORLANDONLINE);
    }

    public void actionPerformed(Browser browser)
    {
      ProductManager.check();
    }
  }

  public static final CheckOpenToolsAction CHECK_OPENTOOLS_ACTION = new CheckOpenToolsAction();

  private OpenToolManager()
  {
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (!PersonalityManager.inCurrentPersonality(OpenToolManagerPersonality.OTM_PERSONALITIES, PersonalityManager.getCurrentPersonalityContext())) return;

    String vrs = VersionUtil.getVersionFromPackage(OpenToolManager.class); //VersionUtil.getVersionString(OpenToolManager.class);
    if (PrimeTime.isVerbose())
    {
      System.out.println("JBuilder Opentool Manager vrs " + vrs);
      System.out.println("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." +
                         PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Excuted with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    net.java.dev.jbuilder.opentools.community.Community.registerAgent(new OpenToolManager());
    PropertyManager.registerPropertyGroup(new OTMPropertyGroup());
    PropertyManager.registerPropertyGroup(new CodeCentralPropertyGroup());
    PropertyManager.registerPropertyGroup(new CCTabOptionPropertyGroup());
    PropertyManager.registerPropertyGroup(new ProductsPropertyGroup());

    // Add to the community group
    net.java.dev.jbuilder.opentools.community.Community.GROUP_Community.add(CHECK_OPENTOOLS_ACTION);
    net.java.dev.jbuilder.opentools.community.Community.GROUP_CommunityToolbar.add(CHECK_OPENTOOLS_ACTION);

    // initialise dynamics
    PrimeTime.initializeOpenTools("OTM_Products");
    PrimeTime.initializeOpenTools("OTM_Repositories");

    PropertyManager.registerPropertyGroup(new OTMsPropertyPageFactory());

  }

  public static Version getOpenToolVersion()
  {
    return new Version(VersionUtil.getVersionFromPackage(OpenToolManager.class));
  }

  public boolean isAutoexec()
  {
    log.setLevel(Level.OFF);
    log.entering("CCAgent", "isAutoexec()");

    if (System.getProperty("lh.codecentral.noautoexec") != null)
    {
      return false;
    }
    if (!PersonalityManager.inCurrentPersonality(OpenToolManagerPersonality.OTM_PERSONALITIES, PersonalityManager.getCurrentPersonalityContext())) return false;

    if (OTMPropertyGroup.AUTO_CHECK.getBoolean())
    {
      if (OTMPropertyGroup.AUTO_CHECK_ONCE_A_DAY.getBoolean())
      {
//        TimeZone utc = TimeZone.getTimeZone("UTC"); // java does not understand utc... :(
        TimeZone utc = TimeZone.getTimeZone("GMT"); // use the old name

        Calendar now = Calendar.getInstance(utc);
        log.finest("now = " + now.getTime());

        Calendar lastcheck = Calendar.getInstance(utc);
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy"); //"dd-MMM-yy H:mm:ss a", Locale.US);
        sdf.setTimeZone(utc);
        try
        {
          log.finest("lacv = " + OTMPropertyGroup.LAST_AUTO_CHECK.getValue());
          lastcheck.setTime(sdf.parse(OTMPropertyGroup.LAST_AUTO_CHECK.getValue()));
          log.finest("lc = " + lastcheck.getTime());
        }
        catch (Exception ex)
        {
          // then we don't know when we last check
          // change to now - 2 days
          lastcheck.add(Calendar.DAY_OF_MONTH, -2);
          log.finest("lcex = " + lastcheck.getTime());
        }

        log.finest("rlc = " + lastcheck.getTime());
//        lastcheck.add(Calendar.DAY_OF_MONTH, 1);
//        log.finest("lc1 = " + lastcheck.getTime());
//        if (now.after(lastcheck))
        log.finest("now dom = " + now.get(Calendar.DAY_OF_MONTH));
        log.finest("lc dom = " + lastcheck.get(Calendar.DAY_OF_MONTH));
        if (now.get(Calendar.DAY_OF_MONTH) != lastcheck.get(Calendar.DAY_OF_MONTH))
        {
//          log.finest("lc aft now ");
          log.finest("lc not now ");
          OTMPropertyGroup.LAST_AUTO_CHECK.setValue(sdf.format(now.getTime()));
          log.finest("wlc = " + sdf.format(now.getTime()));
          log.exiting("CCAgent", "isAutoexec()", Boolean.TRUE);
          return true;
        }
      }
      else
      {
        log.exiting("CCAgent", "isAutoexec()", Boolean.TRUE);
        return true;
      }
    }

    if (true) return true;
    log.exiting("CCAgent", "isAutoexec()", Boolean.FALSE);
    return false;
  }

  public void run()
  {
    ProductManager.check();
  }

}
