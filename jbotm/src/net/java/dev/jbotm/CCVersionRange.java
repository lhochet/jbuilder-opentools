/*
   Copyright: Copyright (c) 2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm;

import com.borland.primetime.util.*;

public class CCVersionRange
{
  private Version min;
  private Version max;

  public CCVersionRange(Version min, Version max)
  {
    this.min = min;
    this.max = max;
  }

  public Version getMinVersion()
  {
    return min;
  }

  public Version getMaxVersion()
  {
    return max;
  }

  public boolean contains(Version vrs)
  {
    if (vrs == null) return false;

    int mj = vrs.getMajor();
    int mn = vrs.getMinor();

    if (mj < min.getMajor()) return false;
    if (mn < min.getMinor()) return false;

    if (mj > max.getMajor()) return false;
    if (mn < max.getMinor()) return false;

    return true;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer(min.toString());
    buf.append(" - ");
    buf.append(max.toString());
    return buf.toString();
  }
}
