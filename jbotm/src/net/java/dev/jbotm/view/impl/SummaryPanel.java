/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.impl;

import java.util.List;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;

import com.borland.primetime.actions.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.view.*;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.4 $ $Date: 2005/01/23 22:05:44 $
 * @created 28/11/04
 */
public class SummaryPanel extends JPanel implements SummaryUI
{
  public static class TrackAction extends StateAction
  {
    private Checker checker;
    private OpenToolEntry entry = null;

    public TrackAction(Checker checker, OpenToolEntry entry)
    {
      super("Tracked", 'T', "Track this OpenTool", null, true);
      this.checker = checker;
      this.entry = entry;
    }

    public void setState(java.lang.Object source, boolean state)
    {
      entry.setTracked(state);
      checker.writeLocal();
    }

    public boolean getState(java.lang.Object source)
    {
      return entry.isTracked();
    }
  }

  public static class UpdatePropertiesAction extends BrowserAction
  {
    private Checker checker;
    private OpenToolEntry entry;

    public UpdatePropertiesAction(Checker checker, OpenToolEntry entry)
    {
      super("Update properties", 'U', "Explicitely update this opentool properties.");
      this.checker = checker;
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {

      browser.getStatusView().setText("Updating properties for '" + entry.getTitle() + "'...");
      Thread thread = new Thread(new Runnable()
      {
        public void run()
        {
          try
          {
            entry.updateProperties();
            checker.writeLocal();
            Browser.getActiveBrowser().getStatusView().setText("Properties for '" + entry.getTitle() + "' updated.");
          }
          catch (Exception ex)
          {
            Browser.getActiveBrowser().getStatusView().setText("Failed to update properties for '" + entry.getTitle() + "'. (" +
              ex.getClass().toString() + " : " + ex.getLocalizedMessage() + ")", StatusView.TYPE_ERROR);
            ex.printStackTrace();
          }
        }
      });
      thread.start();
    }
  }

  public static class PropertiesAction extends BrowserAction
  {
    private Checker checker;
    private OpenToolEntry entry;

    public PropertiesAction(Checker checker, OpenToolEntry entry)
    {
      super("Properties...", 'P', "Show the full properties for this opentool.");
      this.checker = checker;
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {
      if (!entry.isFullyLoaded())
      {
        try
        {
          entry.updateProperties();
        }
        catch (Exception ex)
        {
          Browser.getActiveBrowser().getStatusView().setText("Failed to get extended properties for  '" + entry.getTitle() + "'. (" +
            ex.getClass().toString() + " : " + ex.getLocalizedMessage() + ")", StatusView.TYPE_ERROR);
          ex.printStackTrace();
        }
      }

      PropertiesPanel panel = new PropertiesPanel(entry);
      DefaultDialog.showModalDialog(Browser.getActiveBrowser(), "OpenTool '" + entry.getTitle() + "' Properties", panel, panel.getHelpPage(), panel);

    }
  }

  public static class DownloadAction extends BrowserAction
  {
    private OpenToolEntry entry;

    public DownloadAction(OpenToolEntry entry)
    {
      super("Download", 'D', "Download the selected OpenTool");
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {

      Thread thread = new Thread()
      {
        public void run()
        {
          try
          {
            entry.download();
          }
          catch (OTMException ex)
          {
            ex.printStackTrace();
          }
        }
      };

      thread.start();
    }
  }



  public static class InstallAction extends BrowserAction
  {
    private OpenToolEntry entry;

    public InstallAction(OpenToolEntry entry)
    {
      super("Install", 'I', "Install the selected OpenTool");
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {

      Thread thread = new Thread()
      {
        public void run()
        {
          try
          {
            entry.install();
          }
          catch (OTMException ex)
          {
            ex.printStackTrace();
          }
        }
      };

      thread.start();
    }
  }




  private static class SummaryTreeTableModel extends AbstractTreeTableModel
  {
    private static final String ROOT = "root";
    private static final String NEW = "New OpenTools";
    private static final String UPDATED = "Updated OpenTools";
    private static final String NEWLY_COMMENTED = "Newly commented OpenTools";
    private static final String TRACKED = "Tracked OpenTools";

    private static final int ID = 0;
    private static final int TITLE = 1;
    private static final int AUTHOR = 2;
    private static final int DOWNLOADS = 3;
    private static final int NEW_DOWNLOADS = 4;
    private static final int VERSION = 5;

    private static final int NB_COLS = 6;

    private List newOTs = null;
    private List updatedOTS = null;
    private List newlyCommentedOTs = null;
    private List trackedOTs = null;


    public SummaryTreeTableModel()
    {
      super(ROOT);
    }

    public Object getValueAt(Object object, int x)
    {
      if ((object == ROOT) || (object == NEW) || (object == UPDATED) || (object == NEWLY_COMMENTED) || (object == TRACKED))
      {
        if (x == 0) return object; // never called
        return "";
      }
      if (object instanceof OpenToolEntry)
      {
        OpenToolEntry entry = (OpenToolEntry)object;
        switch (x)
        {
          case ID: return entry.getIdInRepository(); // never called
          case TITLE: return entry.getTitle();
          case AUTHOR: return entry.getAuthorName();
          case DOWNLOADS:// return entry.downloads;
//          case NEW_DOWNLOADS:
          {
            return entry.getNbDownloads() + " (+" + (entry.getNbDownloads() - entry.getPrevNbDownloads()) + ")";
          }
          case NEW_DOWNLOADS:
          {
            return entry.isNew() ? Boolean.TRUE : Boolean.FALSE;
          }
          case VERSION: return entry.getVersion();
        }
        return "";
      }
      return "";
    }

    public void setValueAt(Object object, Object value, int x)
    {
    }

    public int getColumnCount()
    {
      return NB_COLS;
    }

    public String getColumnName(int x)
    {
      String ret = "";
      switch (x)
      {
        case ID:
          ret = "ID";
          break;
        case TITLE:
          ret = "Title";
          break;
        case AUTHOR:
          ret = "Author";
          break;
        case DOWNLOADS:
          ret = "Downloads";
          break;
        case NEW_DOWNLOADS:
          ret = "New";
          break;
        case VERSION:
          ret = "Version";
          break;
      }
      return ret;
    }

    public Object getChild(Object parent, int index)
    {
      if (parent == ROOT)
      {
        switch(index)
        {
          case 0: return NEW;
          case 1: return UPDATED;
          case 2: return NEWLY_COMMENTED;
          case 3: return TRACKED;
        }
      }
      else if (parent == NEW)
      {
        if ((newOTs != null) && (newOTs.size() > 0))
        {
          return newOTs.get(index);
        }
      }
      else if (parent == UPDATED)
      {
        if ((updatedOTS != null) && (updatedOTS.size() > 0))
        {
          return updatedOTS.get(index);
        }
      }
      else if (parent == NEWLY_COMMENTED)
      {
        if ((newlyCommentedOTs != null) && (newlyCommentedOTs.size() > 0))
        {
          return newlyCommentedOTs.get(index);
        }
      }
      else if (parent == TRACKED)
      {
        if ((trackedOTs != null) && (trackedOTs.size() > 0))
        {
          return trackedOTs.get(index);
        }
      }
      return "";
    }

    public int getChildCount(Object parent)
    {
      if (parent == ROOT)
      {
        return 4;
      }
      else if (parent == NEW)
      {
        if (newOTs == null)
        {
          return 0;
        }
        else
        {
          return newOTs.size();
        }
      }
      else if (parent == UPDATED)
      {
        if (updatedOTS == null)
        {
          return 0;
        }
        else
        {
          return updatedOTS.size();
        }
      }
      else if (parent == NEWLY_COMMENTED)
      {
        if (newlyCommentedOTs == null)
        {
          return 0;
        }
        else
        {
          return newlyCommentedOTs.size();
        }
      }
      else if (parent == TRACKED)
      {
        if (trackedOTs == null)
        {
          return 0;
        }
        else
        {
          return trackedOTs.size();
        }
      }
      return 0;
    }

    public void setNewOTsList(List list)
    {
      newOTs = list;
    }

    public void setUpdatedOTsList(List list)
    {
      updatedOTS = list;
    }

    public void setNewlyCommentedOTsList(List list)
    {
      newlyCommentedOTs = list;
    }

    public void setTrackedOTsList(List list)
    {
      trackedOTs = list;
    }
  }

  private static class SummaryTreeTreeCellRenderer extends DefaultTreeCellRenderer
  {
    private static final Icon OT_ICON = new ImageIcon(SummaryPanel.class.getResource("imgs/oticon.png"));
    public SummaryTreeTreeCellRenderer()
    {
      super();
      setLeafIcon(OT_ICON);
    }
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus)
    {
      if (value instanceof OpenToolEntry)
      {
        value = ((OpenToolEntry)value).getIdInRepository() ;
      }
      else
      {
        leaf = false;
      }
      return super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
    }

  }



  private Product product;

  private SummaryTreeTableModel model = new SummaryTreeTableModel();
  private JXTreeTable jXTreeTable1 = new JXTreeTable();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();

  public SummaryPanel()
  {
    try
    {
      jbInit();

      jXTreeTable1.setTreeTableModel(model);
      jXTreeTable1.setTreeCellRenderer(new SummaryTreeTreeCellRenderer());
      jXTreeTable1.addMouseListener(new PopupMouseAdapter(this)
      {
        public ActionGroup constructActionGroup(MouseEvent mouseEvent)
        {
          ActionGroup ag = null;
          TreePath tp = jXTreeTable1.getPathForLocation(mouseEvent.getX(), mouseEvent.getY());
          if (tp != null)
          {
            Object o = tp.getLastPathComponent();
            if (o instanceof OpenToolEntry)
            {
              OpenToolEntry entry = (OpenToolEntry)o;
              Checker checker = product.getChecker();
              ag = new ActionGroup("Actions");
              ag.add(new TrackAction(checker, entry));
              ag.add(new PropertiesAction(checker, entry));
              ag.add(new UpdatePropertiesAction(checker, entry));
              ag.add(new DownloadAction(entry));
              ag.add(new InstallAction(entry));
            }
          }


          return ag;
        }

        public void handleMousePressedEvent(MouseEvent mouseEvent)
        {
//          System.out.println("handleMousePressedEvent");
        }

        public void handleMouseReleasedEvent(MouseEvent mouseEvent)
        {
//          System.out.println("handleMouseReleasedEvent");
        }

      });
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }

  }
  public SummaryPanel(Product product)
  {
    this();
    this.product = product;
  }

  public int getType()
  {
    return ViewManager.SUMMARY_VIEW;
  }

  public Product getProduct()
  {
    return product;
  }

  public void setNewOTsList(List list)
  {
    model.setNewOTsList(list);
  }

  public void setUpdatedOTsList(List list)
  {
    model.setUpdatedOTsList(list);
  }

  public void setNewlyCommentedOTsList(List list)
  {
    model.setNewlyCommentedOTsList(list);
  }

  public void setTrackedOTsList(List list)
  {
    model.setTrackedOTsList(list);
  }

  public boolean hasData()
  {
    return false;
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    jScrollPane1.getViewport().setBackground(Color.white);
    this.add(jScrollPane1, java.awt.BorderLayout.CENTER);
    jScrollPane1.getViewport().add(jXTreeTable1);
  }

}
