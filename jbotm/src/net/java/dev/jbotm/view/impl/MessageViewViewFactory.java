/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.impl;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.view.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:09:01 $
 * @created 28/11/04
 */
public class MessageViewViewFactory
{
  private MessageViewViewFactory()
  {
  }

  public static UI createView(int type, Product product)
  {
    UI ui = null;
    switch (type)
    {
      case ViewManager.SUMMARY_VIEW:
        ui = new SummaryPanelMVAdapter(product);
        break;
      case ViewManager.OTLIST_VIEW:
        ui = new OTListPanelMVAdapter(product);
        break;
    }
    return ui;
  }

  public static void show(UI ui)
  {
    switch (ui.getType())
    {
      case ViewManager.SUMMARY_VIEW:
      {
        if (!(ui instanceof SummaryPanelMVAdapter)) return;

        SummaryPanelMVAdapter spa = (SummaryPanelMVAdapter)ui;
        MessageView mv = Browser.getActiveBrowser().getMessageView();
        mv.addCustomTab(spa.getMessageCategory(), spa);
        mv.showTab(spa.getMessageCategory());
      }
      break;
      case ViewManager.OTLIST_VIEW:
      {
        if (!(ui instanceof OTListPanelMVAdapter)) return;

        OTListPanelMVAdapter otlpa = (OTListPanelMVAdapter)ui;
        MessageView mv = Browser.getActiveBrowser().getMessageView();
        mv.addCustomTab(otlpa.getMessageCategory(), otlpa);
        mv.showTab(otlpa.getMessageCategory());
      }
      break;
    }
  }

}
