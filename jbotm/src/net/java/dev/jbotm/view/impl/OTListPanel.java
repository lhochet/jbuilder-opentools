/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.impl;

import java.util.List;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

import com.borland.primetime.actions.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.view.*;
import org.jdesktop.swingx.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.4 $ $Date: 2005/01/23 22:05:44 $
 * @created 3/12/04
 */
public class OTListPanel extends JPanel implements OTListUI
{
  public static class PlaceHolderAction extends BrowserAction
  {
    public PlaceHolderAction(String str)
    {
      super(str, str.charAt(0), str);
    }

    public void actionPerformed(Browser browser)
    {
    }
  }

  public static class TrackAction extends StateAction
  {
    private Checker checker;
    private OpenToolEntry entry = null;

    public TrackAction(Checker checker, OpenToolEntry entry)
    {
      super("Tracked", 'T', "Track this OpenTool", null, true);
      this.checker = checker;
      this.entry = entry;
    }

    public void setState(java.lang.Object source, boolean state)
    {
      entry.setTracked(state);
      checker.writeLocal();
    }

    public boolean getState(java.lang.Object source)
    {
      return entry.isTracked();
    }
  }

  public static class UpdatePropertiesAction extends BrowserAction
  {
    private Checker checker;
    private OpenToolEntry entry;

    public UpdatePropertiesAction(Checker checker, OpenToolEntry entry)
    {
      super("Update properties", 'U', "Explicitely update this opentool properties.");
      this.checker = checker;
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {

      browser.getStatusView().setText("Updating properties for '" + entry.getTitle() + "'...");
      Thread thread = new Thread(new Runnable()
      {
        public void run()
        {
          try
          {
            entry.updateProperties();
            checker.writeLocal();
            Browser.getActiveBrowser().getStatusView().setText("Properties for '" + entry.getTitle() + "' updated.");
          }
          catch (Exception ex)
          {
            Browser.getActiveBrowser().getStatusView().setText("Failed to update properties for '" + entry.getTitle() + "'. (" +
              ex.getClass().toString() + " : " + ex.getLocalizedMessage() + ")", StatusView.TYPE_ERROR);
            ex.printStackTrace();
          }
        }
      });
      thread.start();
    }
  }

  public static class PropertiesAction extends BrowserAction
  {
    private Checker checker;
    private OpenToolEntry entry;

    public PropertiesAction(Checker checker, OpenToolEntry entry)
    {
      super("Properties...", 'P', "Show the full properties for this opentool.");
      this.checker = checker;
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {
      if (!entry.isFullyLoaded())
      {
        try
        {
          entry.updateProperties();
        }
        catch (Exception ex)
        {
          Browser.getActiveBrowser().getStatusView().setText("Failed to get extended properties for '" + entry.getTitle() + "'. (" +
            ex.getClass().toString() + " : " + ex.getLocalizedMessage() + ")", StatusView.TYPE_ERROR);
          ex.printStackTrace();
        }
      }

      PropertiesPanel panel = new PropertiesPanel(entry);
      DefaultDialog.showModalDialog(Browser.getActiveBrowser(), "OpenTool '" + entry.getTitle() + "' Properties", panel, panel.getHelpPage(), panel);

    }
  }

    public static class DownloadAction extends BrowserAction
    {
      private OpenToolEntry entry;

      public DownloadAction(OpenToolEntry entry)
      {
        super("Download", 'D', "Download the selected OpenTool");
        this.entry = entry;
      }

      public void actionPerformed(Browser browser)
      {

        Thread thread = new Thread()
        {
          public void run()
          {
            try
            {
              entry.download();
            }
            catch (OTMException ex)
            {
              ex.printStackTrace();
            }
          }
        };

        thread.start();
      }
  }


  public static class InstallAction extends BrowserAction
  {
    private OpenToolEntry entry;

    public InstallAction(OpenToolEntry entry)
    {
      super("Install", 'I', "Install the selected OpenTool");
      this.entry = entry;
    }

    public void actionPerformed(Browser browser)
    {

      Thread thread = new Thread()
      {
        public void run()
        {
          try
          {
            entry.install();
          }
          catch (OTMException ex)
          {
            ex.printStackTrace();
          }
        }
      };

      thread.start();
    }
  }

  private static class Wrap
  {
    private String str;
    public Wrap(String str)
    {
      this.str = str;
    }
    public String get()
    {
      return str;
    }
  }


  private static class OTListTableModel extends AbstractTableModel
  {
    private static final int ID = 0;
    private static final int TITLE = 1;
    private static final int AUTHOR = 2;
    private static final int DOWNLOADS = 3;
    private static final int LAST_UPDATED = 4;
    private static final int DOWNLOADED = 5;
    private static final int INSTALLED = 6;
    private static final int VERSION = 7;
    private static final int INSTALLED_VERSION = 8;

    private static final int NB_COLS = 9;

    private List ots = null;

    public int getColumnCount()
    {
      return NB_COLS;
    }

    public String getColumnName(int x)
    {
      String ret = "";
      switch (x)
      {
        case ID:
          ret = "ID";
          break;
        case TITLE:
          ret = "Title";
          break;
        case AUTHOR:
          ret = "Author";
          break;
        case DOWNLOADS:
          ret = "Downloads";
          break;
        case LAST_UPDATED:
          ret = "Last updated";
          break;
        case DOWNLOADED:
          ret = "Downloaded";
          break;
        case INSTALLED:
          ret = "Installed";
          break;
        case VERSION:
          ret = "Version";
          break;
        case INSTALLED_VERSION:
          ret = "Installed Version";
          break;
      }
      return ret;
    }

    public int getRowCount()
    {
      if (ots == null) return 0;
      return ots.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex)
    {
      Object ret = "";
      OpenToolEntry entry = (OpenToolEntry)ots.get(rowIndex);

      switch (columnIndex)
      {
        case ID:
          ret = new Wrap(entry.getIdInRepository());
          break;
        case TITLE:
          ret = entry.getTitle();
          break;
        case AUTHOR:
          ret = entry.getAuthorName();
          break;
        case DOWNLOADS:
          ret = new Integer(entry.getNbDownloads());
          break;
        case LAST_UPDATED:
          ret = entry.getLastModifiedDate();
          break;
        case DOWNLOADED:
          ret = entry.isDownloaded() ? Boolean.TRUE : Boolean.FALSE;
          break;
        case INSTALLED:
          ret = entry.isInstalled() ? Boolean.TRUE : Boolean.FALSE;
          break;
        case VERSION:
          ret = entry.getVersion();
          break;
        case INSTALLED_VERSION:
          ret = entry.getInstalledVersion();
          break;
      }

      return ret;
    }

    public void setOTs(List list)
    {
      ots = list;
      fireTableStructureChanged();
    }

    private OpenToolEntry getRow(int row)
    {
      return (OpenToolEntry)ots.get(row);
    }

    public Class getColumnClass(int columnIndex)
    {
      Class cls = String.class;
      if (columnIndex == ID) cls = Wrap.class;
      return cls;
    }

  }

  private static class OTListTableCellRenderer extends DefaultTableCellRenderer
  {
    private static final Icon OT_ICON = new ImageIcon(SummaryPanel.class.getResource("imgs/oticon.png"));

    public OTListTableCellRenderer()
    {
      super();
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column)
    {
      Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      if (column == 0)
      {
         if (comp instanceof JLabel)
         {
           if (value instanceof Wrap)
           {
             JLabel lbl = (JLabel)comp;
             lbl.setIcon(OT_ICON);
             lbl.setText(((Wrap)value).get());
           }
         }
      }
      return comp;
    }

  }

  private Product product;
  private OTListTableModel model = new OTListTableModel();

  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JXTable jXTable1 = new JXTable();
  private JPanel jPanel1 = new JPanel();
  private JLabel jLabel1 = new JLabel();
  private JTextField jTextField1 = new JTextField();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JRadioButton jRadioButton1 = new JRadioButton();
  private JRadioButton jRadioButton2 = new JRadioButton();
  private JRadioButton jRadioButton3 = new JRadioButton();
  private JRadioButton jRadioButton4 = new JRadioButton();
  private JButton jButton1 = new JButton();
  private JButton jButton2 = new JButton();
  private ButtonGroup buttonGroup1 = new ButtonGroup();

  public OTListPanel()
  {
    try
    {
      jXTable1.setDefaultRenderer(Wrap.class, new OTListTableCellRenderer());//jXTable1.getDefaultRenderer(Wrap.class)));
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public OTListPanel(Product product)
  {
    this();
    this.product = product;
    final Product localProduct = product;
    jXTable1.setModel(model);
    jXTable1.addMouseListener(new PopupMouseAdapter(this)
    {
      public ActionGroup constructActionGroup(MouseEvent mouseEvent)
      {
        ActionGroup ag = null;
        int rowClicked = jXTable1.rowAtPoint(mouseEvent.getPoint());
        jXTable1.setRowSelectionInterval(rowClicked, rowClicked);
        int row = jXTable1.convertRowIndexToModel(rowClicked);
        if (row > -1)
        {
            OpenToolEntry entry = model.getRow(row);
            Checker checker = localProduct.getChecker();
            ag = new ActionGroup("Actions");
            ag.add(new TrackAction(checker, entry));
            ag.add(new PropertiesAction(checker, entry));
            ag.add(new UpdatePropertiesAction(checker, entry));
            ag.add(new DownloadAction(entry));
            ag.add(new InstallAction(entry));
            ag.add(new PlaceHolderAction("Update list..."));
        }


        return ag;
      }

      public void handleMousePressedEvent(MouseEvent mouseEvent)
      {
        // idea of what this could be for?
//        System.out.println("handleMousePressedEvent");
      }

      public void handleMouseReleasedEvent(MouseEvent mouseEvent)
      {
//        System.out.println("handleMouseReleasedEvent");
      }

    });
  }

  public int getType()
  {
    return ViewManager.OTLIST_VIEW;
  }

  public Product getProduct()
  {
    return product;
  }

  public void setOTList(List list)
  {
    model.setOTs(list);
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    jLabel1.setText("Filter:");
    jRadioButton1.setText("This author");
    jRadioButton2.setText("ID");
    jRadioButton3.setText("Author");
    jRadioButton4.setText("Keyword");
    jButton1.setText("Filter");
    jButton2.setText("More...");
    this.add(jScrollPane1, java.awt.BorderLayout.CENTER);
    this.add(jPanel1, java.awt.BorderLayout.NORTH);
    jScrollPane1.getViewport().add(jXTable1);
    jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                                                new Insets(5, 5, 0, 0), 0, 0));
    jPanel1.add(jRadioButton1, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(jRadioButton2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(jRadioButton3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(jRadioButton4, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(jButton1, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.CENTER, GridBagConstraints.NONE,
                                                 new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(jButton2, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.CENTER, GridBagConstraints.NONE,
                                                 new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(jTextField1, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
        , GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    buttonGroup1.add(jRadioButton2);
    buttonGroup1.add(jRadioButton3);
    buttonGroup1.add(jRadioButton1);
    buttonGroup1.add(jRadioButton4);
  }
}
