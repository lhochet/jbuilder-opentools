/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.impl;

import java.util.List;

import java.awt.*;
import javax.swing.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.view.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:09:19 $
 * @created 3/12/04
 */
public class OTListPanelMVAdapter extends JPanel implements OTListUI, MessageViewView
{
  public OTListPanelMVAdapter()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  private MessageCategory messageCategory;
  private BorderLayout borderLayout1 = new BorderLayout();
  private OTListPanel panel = new OTListPanel();

  public OTListPanelMVAdapter(Product product)
  {
    messageCategory = new MessageCategory(
      product.getName() + " OpenTools ", product.getName() + " OpenTools List",
      BrowserIcons.ICON_HELPBORLANDONLINE);

    panel = new OTListPanel(product);

    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public int getType()
  {
    return panel.getType();
  }

  public Product getProduct()
  {
    return panel.getProduct();
  }

//  public void setNewOTsList(List list)
//  {
//    panel.setNewOTsList(list);
//  }
//
//  public void setUpdatedOTsList(List list)
//  {
//    panel.setUpdatedOTsList(list);
//  }
//
//  public void setNewlyCommentedOTsList(List list)
//  {
//    panel.setNewlyCommentedOTsList(list);
//  }
//
//  public void setTrackedOTsList(List list)
//  {
//    panel.setTrackedOTsList(list);
//  }
//
//  public boolean hasData()
//  {
//    return panel.hasData();
//  }

  public MessageCategory getMessageCategory()
  {
    return messageCategory;
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    this.add(panel, java.awt.BorderLayout.CENTER);
  }

  public void setOTList(List list)
  {
    panel.setOTList(list);
  }
}
