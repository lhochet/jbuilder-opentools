/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.viewimpl;

import java.util.*;

import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.view.*;
import net.java.dev.jbotm.view.impl.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:10:48 $
 * @created 4/12/04
 */
public class SummaryPanelVAdapter implements SummaryUI
{
  private List newOTs = null;
  private List updatedOTS = null;
  private List newlyCommentedOTs = null;
  private List trackedOTs = null;
  private Product product = null;
  private SummaryPanel panel = null;

  public SummaryPanelVAdapter(Product product)
  {
    this.product = product;
  }

  /**
   * getProduct
   *
   * @return Product
   * @todo Implement this net.java.dev.jbotm.view.UI method
   */
  public Product getProduct()
  {
    return product;
  }

  public List getNewlyCommentedOTs()
  {
    return newlyCommentedOTs;
  }

  public List getNewOTs()
  {
    return newOTs;
  }

  public List getTrackedOTs()
  {
    return trackedOTs;
  }

  public List getUpdatedOTS()
  {
    return updatedOTS;
  }

  /**
   * getType
   *
   * @return int
   * @todo Implement this net.java.dev.jbotm.view.UI method
   */
  public int getType()
  {
    return ViewManager.SUMMARY_VIEW;
  }

  /**
   * hasData
   *
   * @return boolean
   * @todo Implement this net.java.dev.jbotm.view.SummaryUI method
   */
  public boolean hasData()
  {
    return true;
  }

  /**
   * setNewOTsList
   *
   * @param list List
   * @todo Implement this net.java.dev.jbotm.view.SummaryUI method
   */
  public void setNewOTsList(List list)
  {
    newOTs = list;
    if (panel != null) panel.setNewOTsList(list);
  }

  /**
   * setNewlyCommentedOTsList
   *
   * @param list List
   * @todo Implement this net.java.dev.jbotm.view.SummaryUI method
   */
  public void setNewlyCommentedOTsList(List list)
  {
    newlyCommentedOTs = list;
    if (panel != null) panel.setNewlyCommentedOTsList(list);
  }

  /**
   * setTrackedOTsList
   *
   * @param list List
   * @todo Implement this net.java.dev.jbotm.view.SummaryUI method
   */
  public void setTrackedOTsList(List list)
  {
    trackedOTs = list;
    if (panel != null) panel.setTrackedOTsList(list);
  }

  /**
   * setUpdatedOTsList
   *
   * @param list List
   * @todo Implement this net.java.dev.jbotm.view.SummaryUI method
   */
  public void setUpdatedOTsList(List list)
  {
    updatedOTS = list;
    if (panel != null) panel.setUpdatedOTsList(list);
  }
  public void setPanel(SummaryPanel panel)
  {
    this.panel = panel;
  }
}
