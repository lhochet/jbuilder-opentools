/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.viewimpl;

import javax.swing.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.ide.view.*;
import net.java.dev.jbotm.view.impl.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:10:32 $
 * @created 5/12/04
 */
public class OTListViewFactory extends DefaultViewFactory
{
  private OTListPanelVAdapter adapter;
  public OTListViewFactory(ViewType type, OTListPanelVAdapter adapter)
  {
    super(type);
    this.adapter = adapter;
  }
  protected JComponent createComponent(Browser browser)
  {
    OTListPanel panel = new OTListPanel(adapter.getProduct());
    panel.setOTList(adapter.getOTList());
    adapter.setPanel(panel);
    return panel;
  }
}
