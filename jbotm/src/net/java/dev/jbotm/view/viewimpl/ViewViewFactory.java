/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.viewimpl;

import com.borland.primetime.ide.*;
import com.borland.primetime.ide.view.*;
import com.borland.primetime.ide.workspace.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.view.*;
import net.java.dev.jbotm.view.ViewManager;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:11:23 $
 * @created 4/12/04
 */
public class ViewViewFactory
{
  private ViewViewFactory()
  {
  }
  public static UI createView(int type, Product product)
  {
    UI ui = null;
    switch (type)
    {
      case ViewManager.SUMMARY_VIEW:
        ui = new SummaryPanelVAdapter(product);
        break;
      case ViewManager.OTLIST_VIEW:
        ui = new OTListPanelVAdapter(product);
        break;
    }
    return ui;
  }

  public static void show(UI ui)
  {
    switch (ui.getType())
    {
      case ViewManager.SUMMARY_VIEW:
      {
        if (!(ui instanceof SummaryPanelVAdapter)) return;

        SummaryPanelVAdapter spa = (SummaryPanelVAdapter)ui;
        Product product = spa.getProduct();
        String id = product.getID() + SummaryViewType.ID;
        View view;
        Browser browser = Browser.getActiveBrowser();
        Workspace workspace = WorkspaceManager.getWorkspace(browser);
        View[] views = workspace.getViews(id);
          if (views.length == 1)
          {
            view = views[0];
          }
          else
          {
            ViewType viewtype = new SummaryViewType(product);
            com.borland.primetime.ide.view.ViewManager.registerViewFactory(new SummaryViewFactory(viewtype, spa));
            view = com.borland.primetime.ide.view.ViewManager.createView(browser, id);
          }

          workspace.openView(view);
          workspace.setViewActive(view);
      }
      break;
      case ViewManager.OTLIST_VIEW:
      {
        if (!(ui instanceof OTListPanelVAdapter)) return;

        OTListPanelVAdapter otlpa = (OTListPanelVAdapter)ui;
        Product product = otlpa.getProduct();
        String id = product.getID() + OTListViewType.ID;
        View view;
        Browser browser = Browser.getActiveBrowser();
        Workspace workspace = WorkspaceManager.getWorkspace(browser);
        View[] views = workspace.getViews(id);
          if (views.length == 1)
          {
            view = views[0];
          }
          else
          {
            ViewType viewtype = new OTListViewType(product);
            com.borland.primetime.ide.view.ViewManager.registerViewFactory(new OTListViewFactory(viewtype, otlpa));
            view = com.borland.primetime.ide.view.ViewManager.createView(browser, id);
          }

          workspace.openView(view);
          workspace.setViewActive(view);
      }
      break;
    }
  }
}
