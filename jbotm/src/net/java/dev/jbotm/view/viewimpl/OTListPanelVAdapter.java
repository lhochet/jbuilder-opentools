/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.viewimpl;

import java.util.*;

import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.view.*;
import net.java.dev.jbotm.view.impl.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:10:24 $
 * @created 4/12/04
 */
public class OTListPanelVAdapter implements OTListUI
{
  private Product product = null;
  private List ots = null;
  private OTListPanel panel = null;

  public OTListPanelVAdapter(Product product)
  {
    this.product = product;
  }

  /**
   * getProduct
   *
   * @return Product
   * @todo Implement this net.java.dev.jbotm.view.UI method
   */
  public Product getProduct()
  {
    return product;
  }

  /**
   * getType
   *
   * @return int
   * @todo Implement this net.java.dev.jbotm.view.UI method
   */
  public int getType()
  {
    return ViewManager.OTLIST_VIEW;
  }

  /**
   * setOTList
   *
   * @param list List
   * @todo Implement this net.java.dev.jbotm.view.OTListUI method
   */
  public void setOTList(List list)
  {
    ots = list;
    if (panel != null) panel.setOTList(list);
  }

  public List getOTList()
  {
    return ots;
  }

  public void setPanel(OTListPanel panel)
  {
    this.panel = panel;
  }
}
