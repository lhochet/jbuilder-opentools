/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view;

import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.view.impl.*;
import net.java.dev.jbotm.view.viewimpl.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:08:37 $
 * @created 28/11/04
 */
public class ViewManager
{
  public static final int SUMMARY_VIEW = 0;
  public static final int OTLIST_VIEW = 1;

  private ViewManager()
  {
  }

  public static UI createView(int type, Product product)
  {
    if (ProductsPropertyPage.USE_VIEW.getBoolean())
    {
      return ViewViewFactory.createView(type, product);
    }
    else
    {
      return MessageViewViewFactory.createView(type, product);
    }
  }

  public static void show(UI ui)
  {
    if (ProductsPropertyPage.USE_VIEW.getBoolean())
    {
      ViewViewFactory.show(ui);
    }
    else
    {
      MessageViewViewFactory.show(ui);
    }
  }
}
