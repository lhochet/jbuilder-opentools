/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.props;

import java.awt.*;

import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.view.impl.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:10:03 $
 * @created 7/12/04
 */
public class OTMsPropertyPage extends PropertyPage
{
  public OTMsPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * getHelpTopic
   *
   * @return HelpTopic
   * @todo Implement this com.borland.primetime.properties.PropertyPage method
   */
  public HelpTopic getHelpTopic()
  {
    return null;
  }

  /**
   * readProperties
   *
   * @todo Implement this com.borland.primetime.properties.PropertyPage method
   */
  public void readProperties()
  {
    Product product = JBuilder.getInstance();
    Checker checker = product.getChecker();
    checker.runLocal();
    otlist.setOTList(checker.getOpenTools());
  }

  /**
   * writeProperties
   *
   * @todo Implement this com.borland.primetime.properties.PropertyPage method
   */
  public void writeProperties()
  {
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    this.add(otlist, java.awt.BorderLayout.CENTER);
  }

  private BorderLayout borderLayout1 = new BorderLayout();
  private OTListPanel otlist = new OTListPanel(JBuilder.getInstance());
}
