/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.view.props;

import com.borland.primetime.properties.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:10:11 $
 * @created 7/12/04
 */
public class OTMsPropertyPageFactory extends PropertyPageFactory implements PropertyGroup
{
  public OTMsPropertyPageFactory()
  {
    super("OpenTools");
  }
  /**
   * createPropertyPage
   *
   * @return PropertyPage
   * @todo Implement this com.borland.primetime.properties.PropertyPageFactory
   *   method
   */
  public PropertyPage createPropertyPage()
  {
    return new OTMsPropertyPage();
  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == null)
    {
      return this;
    }
    return null;
  }

  public void initializeProperties()
  {
  }
}
