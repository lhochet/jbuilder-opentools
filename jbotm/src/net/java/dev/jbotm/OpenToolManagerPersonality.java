package net.java.dev.jbotm;

import com.borland.primetime.personality.*;
import com.borland.primetime.*;

/**
 * <p>Title: CodeCentral</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2004</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:06:09 $
 */
public class OpenToolManagerPersonality extends DefaultPersonality
{
  /** Static instance of the OTM Personality **/
  public static final Personality OTM_PERSONALITY = new OpenToolManagerPersonality();
  /** Convenience wrapper of the static instance for the methods returning a Personality[] **/
  public static final Personality[] OTM_PERSONALITIES = new Personality[] { OTM_PERSONALITY };

  /**
   * Register the static instance of the Community personality with the personalty manager
   * @param majorVersion major version number of the PrimeTime version used
   * @param minorVersion minor version number of the PrimeTime version used
   */
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("OpenToolManagerPersonality vrs 0.1");
      System.out.println("  Compiled with JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." +
                         PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    PersonalityManager.registerPersonality(OTM_PERSONALITY);
  }

  /**
   * Overrides the DefaultPersonality constructor to set the name of this personality
   */
  public OpenToolManagerPersonality()
  {
    super("OpenTools Manager");
  }

  public String[] getDepend()
  {
    return new String[] { "net.java.dev.jbuilder.opentools.community.CommunityPersonality" };
  }

}
