/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import com.borland.primetime.properties.*;
import net.java.dev.jbotm.OTMPropertyGroup;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.3 $ $Date: 2005/03/05 21:58:04 $
 * @created 25/07/04
 */
public class CCTabOptionPropertyGroup implements PropertyGroup
{
  public CCTabOptionPropertyGroup()
  {
  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == OTMPropertyGroup.OTM_CAT) //net.java.dev.jbuilder.opentools.community.CommunityPropertyGroup.COMMUNITY_TOPIC)
    {
      return new PropertyPageFactory("Options")
      {
        public PropertyPage createPropertyPage()
        {
          return new CCTabOptionPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

  public void initializeProperties()
  {
  }
}
