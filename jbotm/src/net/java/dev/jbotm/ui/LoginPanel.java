/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;
import javax.swing.*;

/**
 * @author Ludovic HOCHET
 * @version 0.1, 25/08/01
 */
public class LoginPanel extends JPanel
{
  JLabel jLabel1 = new JLabel();
  JTextField tfUserID = new JTextField();
  JLabel jLabel2 = new JLabel();
  JTextField tfEMail = new JTextField();
  JLabel jLabel3 = new JLabel();
  JPasswordField pfPassword = new JPasswordField();
  JCheckBox jCheckBox1 = new JCheckBox();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  public LoginPanel()
  {
    try
    {
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    jLabel1.setText("User name:");
    this.setLayout(gridBagLayout1);
    jLabel2.setText("AND E-mail:");
    jLabel3.setText("Password:");
    jCheckBox1.setText("Save cookie");
    this.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(14, 10, 0, 0), 6, 0));
    this.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(14, 10, 0, 0), 11, 0));
    this.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                             , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(18, 10, 0, 9), 6, 0));
    this.add(tfUserID, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
                                              , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(14, 18, 0, 22), 166, 0));
    this.add(tfEMail, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
                                             , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(12, 18, 0, 22), 166, 0));
    this.add(pfPassword, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0
                                                , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(15, 18, 0, 22), 166, 0));
    this.add(jCheckBox1, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10),
                                                0, 0));
  }

  public String getUID()
  {
    return tfUserID.getText();
  }

  public String getEmail()
  {
    return tfEMail.getText();
  }

  public String getPassword()
  {
    return new String(pfPassword.getPassword());
  }

  public boolean isSaveCookie()
  {
    return jCheckBox1.isSelected();
  }
}
