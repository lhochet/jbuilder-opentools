/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.borland.primetime.help.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.old.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005/03/05 21:58:04 $
 * 0.3, 16/03/02
 * 0.2, 22/11/01
 * 0.1, 15/08/01
 */
public class CodeCentralPropertyPage extends PropertyPage
{
  JPanel jPanel1 = new JPanel();
  JTextField tfDownloadDir = new JTextField();
  JPanel jPanel2 = new JPanel();
  JButton btnSetDownloadDir = new JButton();
  JCheckBox cbAutoLoadEntryExtProps = new JCheckBox();
  JLabel jLabel1 = new JLabel();
  JButton btnStop = new JButton();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  Border border1;
  TitledBorder titledBorder1;
  ButtonGroup btnGrpShowIfNoNewUpdated = new ButtonGroup();
  Border border2;
  JLabel jLabel2 = new JLabel();
  JTextField tfCookie = new JTextField();
  private Border border4;

  public CodeCentralPropertyPage()
  {
    try
    {
      jbInit();

    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public void writeProperties()
  {
    CodeCentralPropertyGroup.AUTO_LOAD_ENTRIES.setBoolean(cbAutoLoadEntryExtProps.isSelected());
    CodeCentralPropertyGroup.DEST_PATH.setValue(tfDownloadDir.getText());
    String cookie = tfCookie.getText();
    if (cookie.equals(""))
    {
      cookie = null;
    }
    CodeCentralPropertyGroup.COOKIE.setValue(cookie);


  }

  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("/doc/codecentralpropertypage.html").toString());
  }

  public void readProperties()
  {
    cbAutoLoadEntryExtProps.setSelected(CodeCentralPropertyGroup.AUTO_LOAD_ENTRIES.getBoolean());

    tfDownloadDir.setText(CodeCentralPropertyGroup.DEST_PATH.getValue());
    tfCookie.setText(CodeCentralPropertyGroup.COOKIE.getValue());
//    if (DownloadManager.getDownloadManager().isDownloading())
//    {
//      btnStop.setEnabled(true);
//    }
//    else
    {
      btnStop.setEnabled(false);
    }
  }

  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(SystemColor.controlText, 1);
    titledBorder1 =
      new TitledBorder(BorderFactory.createLineBorder(SystemColor.controlText, 1), "Check for new or updated OpenTools for:");
    border2 =
      BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1),
      "If there are no new or updated OpenTools:")
                                         , BorderFactory.createEmptyBorder(0, 5, 0, 0));
    border4 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1),
      "Check for new or updated OpenTools for:"), BorderFactory.createEmptyBorder(0, 5, 0, 0));
    this.setLayout(borderLayout2);
    jPanel2.setLayout(gridBagLayout1);
    btnSetDownloadDir.setText("...");
    btnSetDownloadDir.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnSetDownloadDir_actionPerformed(e);
      }
    });
    cbAutoLoadEntryExtProps.setText("Auto load OpenTools extended details when new or updated");
    tfDownloadDir.setToolTipText("Default directory in which the OpenTools zips will be downloaded, " +
                                 "this should not be <Builder>/lib/ext");
    jLabel1.setToolTipText("Default directory in which the OpenTools zips will be downloaded, " +
                           "this should not be <Builder>/lib/ext");
    jLabel1.setHorizontalAlignment(SwingConstants.LEFT);
    jLabel1.setText("CC .Zips Download directory:");
    jLabel1.setVerticalAlignment(SwingConstants.TOP);
    jPanel1.setLayout(borderLayout1);
    this.setMinimumSize(new Dimension(1, 1));
    this.setPreferredSize(new Dimension(450, 350));
    btnStop.setText("Stop AutoLoad");
    btnStop.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnStop_actionPerformed(e);
      }
    });
    jLabel2.setText("Borland Cookie:");
    tfCookie.setSelectionEnd(0);
    tfCookie.setSelectionStart(67);
    tfCookie.setText("USER=UID[|]username[|]firstname[|]name[|]email[|]EN[|]90[|]1[|]ISO8859_1; expires=Sunday, 16-Jul-03 6:52:17 GMT; path=/; domain=.borland.com");
    this.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jPanel2, BorderLayout.NORTH);
    jPanel2.add(tfDownloadDir, new GridBagConstraints(2, 4, 2, 1, 1.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(3, 5, 0, 0), 0, 0));
    jPanel2.add(cbAutoLoadEntryExtProps, new GridBagConstraints(0, 1, 5, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(btnSetDownloadDir, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(2, 5, 0, 5), 0, 0));
    jPanel2.add(jLabel1, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(btnStop, new GridBagConstraints(0, 2, 3, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(jLabel2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(5, 5, 0, 0), 0, 0));
    jPanel2.add(tfCookie, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0
                                                 , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                                                 new Insets(3, 5, 0, 0), 0, 0));
  }

  void btnSetDownloadDir_actionPerformed(ActionEvent e)
  {
    Url startdir = null;
    try
    {
      startdir = new Url(tfDownloadDir.getText());
    }
    catch (Exception ex)
    {
      // do nothing
    }

    Url url = UrlChooser.promptForDir(Browser.getActiveBrowser(),
                                      startdir
                                      /*null*/
                                      /*project.getUrl()*/
                                      , "Choose Default Download directory...");
    if (url != null)
    {
      tfDownloadDir.setText(url.getFullName());
    }
  }

  void btnStop_actionPerformed(ActionEvent e)
  {
    DownloadManager.getDownloadManager().stop();
    btnStop.setEnabled(false);
  }


}
