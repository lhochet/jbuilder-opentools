/*
   Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.structs.OpenToolEntry;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.4 $ $Date: 2005/01/23 22:05:22 $
 * @created 17/02/02
 */
public class EntryCommentsPanel extends EntryPropertyPage
{
  JSplitPane jSplitPane1 = new JSplitPane();
  JScrollPane jScrollPane1 = new JScrollPane();
  SearchTree commentTree = new SearchTree();
  JScrollPane jScrollPane2 = new JScrollPane();
  JEditorPane commentArea = new JEditorPane();
  BorderLayout borderLayout1 = new BorderLayout();

  DefaultMutableTreeNode top = new DefaultMutableTreeNode();
  OpenToolEntry entry = null;

  public EntryCommentsPanel()
  {
    this(null);
  }

  public EntryCommentsPanel(OpenToolEntry entry)
  {
    this.entry = entry;
    try
    {
      preJBInit();
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public String getTitle()
  {
    return "Comments";
  }

  private void jbInit() throws Exception
  {
    commentArea.setEditable(false);
    commentArea.setText("<html><body></body></html>");
    commentArea.setContentType("text/html");
    this.setLayout(borderLayout1);
    jSplitPane1.setOrientation(JSplitPane.VERTICAL_SPLIT);
    jSplitPane1.setLastDividerLocation(90);
    this.add(jSplitPane1, BorderLayout.CENTER);
    jSplitPane1.add(jScrollPane1, JSplitPane.LEFT);
    jSplitPane1.add(jScrollPane2, JSplitPane.RIGHT);
    jScrollPane2.getViewport().add(commentArea, null);
    jScrollPane1.getViewport().add(commentTree, null);
    jSplitPane1.setDividerLocation(100);
  }

  private void preJBInit()
  {

    commentTree.setShowsRootHandles(true);
    DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
    renderer.setLeafIcon(null);
    renderer.setOpenIcon(null);
    renderer.setClosedIcon(null);
    commentTree.setCellRenderer(renderer);
    commentTree.setAutoscrolls(true);
    commentTree.setScrollsOnExpand(true);
    commentTree.setRootVisible(false);
    commentTree.setModel(new DefaultTreeModel(top));
    commentTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    //Listen for when the selection changes.
    commentTree.addTreeSelectionListener(new TreeSelectionListener()
    {
      public void valueChanged(TreeSelectionEvent e)
      {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
          commentTree.getLastSelectedPathComponent();

        if (node == null)
        {
          return;
        }

        Browser browser = Browser.getActiveBrowser();
        CommentEntry nodeInfo = (CommentEntry)node.getUserObject();
        if (nodeInfo.comment == null)
        {
          try
          {
            commentArea.setText("Loading...");
            browser.getStatusView().setText("Loading comment...");
            commentArea.updateUI();
            CodeCentralOTCommentLoader cl = new CodeCentralOTCommentLoader(nodeInfo);
            cl.load();
          }
          catch (Exception ex)
          {
            // do nothing : comment not found will be displayed
          }
          browser.getStatusView().setText("");

        }

        if (nodeInfo.comment == null)
        {
          commentArea.setText("Comment not found.");
        }
        else
        {
          commentArea.setText(nodeInfo.comment);
        }

      }
    });


    appendSubNodes(top, entry.getComments());
    commentTree.updateUI();
  }

  private void appendSubNodes(DefaultMutableTreeNode top, ArrayList subnodes)
  {
    if (subnodes == null)
    {
      return;
    }
    Iterator iter = subnodes.iterator();
    while (iter.hasNext())
    {
      CommentEntry item = (CommentEntry)iter.next();
      DefaultMutableTreeNode node = new DefaultMutableTreeNode(item);
      top.add(node);
      appendSubNodes(node, item.subcomments);
    }
  }

}
