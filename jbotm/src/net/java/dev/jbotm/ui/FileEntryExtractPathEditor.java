/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author Ludovic HOCHET
 * @version 0.1, 18/12/01
 */
public class FileEntryExtractPathEditor extends AbstractCellEditor implements TableCellEditor
{
  private final FileEntryExtractPathEditorPanel panel = new FileEntryExtractPathEditorPanel();

  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
  {
    if (value != null)
    {
      if (!(value instanceof String))
      {
        System.err.println("Err incorrect type passed for cell editing : " + value.getClass().getName());
        throw new java.lang.IllegalArgumentException("Can only edit strings.");
      }

      panel.setExtractPath((String)value);
    }
    panel.setDefaultExtractName((String)table.getValueAt(row, 0));
    return panel;
  }

  public Object getCellEditorValue()
  {
    return panel.getExtractPath();
  }

  public boolean isCellEditable(EventObject anEvent)
  {
    return true;
  }

  public boolean shouldSelectCell(EventObject anEvent)
  {
    return true;

  }

}
