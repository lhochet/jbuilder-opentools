/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;

import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;
import java.util.*;

import net.java.dev.jbotm.products.*;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Rectangle;
import javax.swing.border.TitledBorder;
import javax.swing.BorderFactory;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.*;
import net.java.dev.jbotm.repo.*;
import javax.swing.JCheckBox;
import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005/03/05 21:58:04 $
 * @created 4/11/04
 */
public class ProductsPropertyPage extends PropertyPage
{
  public static final GlobalBooleanProperty USE_VIEW = new GlobalBooleanProperty(OTMPropertyGroup.OTM_CAT, "use_view", true);

  private class ProductsModel extends AbstractTableModel
  {
    private int sz = 0;
    private Product[] products;
    private boolean[] checked;

    public ProductsModel(java.util.List rows)
    {
      if (rows == null) return;

      sz = rows.size();
      products = (Product[])rows.toArray(new Product[sz]);
      checked = new boolean[sz];
      for (int i = 0; i < products.length; i++)
      {
        checked[i] = products[i].isChecked();
      }
    }

    public void updateTo(java.util.List rows)
    {
      sz = rows.size();
      products = (Product[])rows.toArray(new Product[sz]);
      checked = new boolean[sz];
      for (int i = 0; i < checked.length; i++)
      {
        checked[i] = true;
      }
    }


    public int getColumnCount()
    {
      return 2;
    }

    public Object getValueAt(int y, int x)
    {
      switch (x)
      {
        case 0:
          return checked[y] ? Boolean.TRUE : Boolean.FALSE;
        case 1:
          return products[y].getName();
      }
      return new ArrayIndexOutOfBoundsException("Invalid column.");
    }

    public int getRowCount()
    {
      return sz;
    }

    public String getColumnName(int column)
    {
      switch (column)
      {
        case 0:
          return "Checked";
        case 1:
          return "Product";
      }
      return "";
    }

    public Class getColumnClass(int c)
    {
      switch (c)
      {
        case 0:
          return Boolean.class;
        case 1:
          return String.class;
      }
      return Object.class;
    }

    public boolean isCellEditable(int rowIndex, int colIndex)
    {
      if (colIndex == 0)
      {
        return true;
      }
      return false;
    }

    public void setValueAt(Object value, int row, int col)
    {
      switch (col)
      {
        case 0:
          checked[row] = ((Boolean)value).booleanValue();
          break;
      }
      fireTableCellUpdated(row, col);
    }

    public void writeUpdates()
    {
      for (int i = 0; i < products.length; i++)
      {
        products[i].setChecked(checked[i]);
      }
    }
  }

  private class RepositoriesModel extends AbstractTableModel
  {
    private int sz = 0;
    private Repository[] repositories;
    private boolean[] checked;

    public RepositoriesModel(java.util.List rows)
    {
      if (rows == null) return;

      sz = rows.size();
      repositories = (Repository[])rows.toArray(new Repository[sz]);
      checked = new boolean[sz];
      for (int i = 0; i < repositories.length; i++)
      {
        checked[i] = repositories[i].isChecked();
      }
    }

    public void updateTo(java.util.List rows)
    {
      sz = rows.size();
      repositories = (Repository[])rows.toArray(new Repository[sz]);
      checked = new boolean[sz];
      for (int i = 0; i < checked.length; i++)
      {
        checked[i] = true;
      }
    }


    public int getColumnCount()
    {
      return 2;
    }

    public Object getValueAt(int y, int x)
    {
      switch (x)
      {
        case 0:
          return checked[y] ? Boolean.TRUE : Boolean.FALSE;
        case 1:
          return repositories[y].getName();
      }
      return new ArrayIndexOutOfBoundsException("Invalid column.");
    }

    public int getRowCount()
    {
      return sz;
    }

    public String getColumnName(int column)
    {
      switch (column)
      {
        case 0:
          return "Checked";
        case 1:
          return "Repository";
      }
      return "";
    }

    public Class getColumnClass(int c)
    {
      switch (c)
      {
        case 0:
          return Boolean.class;
        case 1:
          return String.class;
      }
      return Object.class;
    }

    public boolean isCellEditable(int rowIndex, int colIndex)
    {
      if (colIndex == 0)
      {
        return true;
      }
      return false;
    }

    public void setValueAt(Object value, int row, int col)
    {
      switch (col)
      {
        case 0:
          checked[row] = ((Boolean)value).booleanValue();
          break;
      }
      fireTableCellUpdated(row, col);
    }

    public void writeUpdates()
    {
      for (int i = 0; i < repositories.length; i++)
      {
        repositories[i].setChecked(checked[i]);
      }
    }
  }

  private ProductsModel model = new ProductsModel(null);
  private RepositoriesModel repoModel = new RepositoriesModel(null);

  public ProductsPropertyPage()
  {
    super();
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public ProductsPropertyPage(LayoutManager layout)
  {
    super(layout);
  }

  public HelpTopic getHelpTopic()
  {
    return null;
  }

  public void readProperties()
  {
    model.updateTo(ProductManager.getProducts());
    repoModel.updateTo(RepositoryManager.getRepositories());
    chUseView.setSelected(USE_VIEW.getBoolean());
  }

  public void writeProperties()
  {
    model.writeUpdates();
    repoModel.writeUpdates();
    USE_VIEW.setBoolean(chUseView.isSelected());
  }

  private void jbInit() throws Exception
  {
    this.setLayout(null);
    jScrollPane1.setBorder(border2);
    jScrollPane1.setBounds(new Rectangle(10, 10, 373, 130));
    tblProducts.setModel(model);
    tblRepositories.setModel(repoModel);
    jScrollPane2.setBorder(border4);
    jScrollPane2.setBounds(new Rectangle(12, 157, 370, 123));
    chUseView.setText("Use View");
    chUseView.setBounds(new Rectangle(16, 286, 88, 23));
    this.add(jScrollPane1);
    this.add(jScrollPane2);
    this.add(chUseView);
    jScrollPane2.getViewport().add(tblRepositories);
    jScrollPane1.getViewport().add(tblProducts);
  }

  private JScrollPane jScrollPane1 = new JScrollPane();
  private JTable tblProducts = new JTable();
  private Border border1 = BorderFactory.createLineBorder(new Color(173, 173, 173), 1);
  private Border border2 = new TitledBorder(border1, "Products:");
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JTable tblRepositories = new JTable();
  private Border border3 = BorderFactory.createLineBorder(Color.lightGray, 1);
  private Border border4 = new TitledBorder(border3, "Repositories:");
  private JCheckBox chUseView = new JCheckBox();
}
