/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.repo.*;
import net.java.dev.jbotm.structs.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.6 $ $Date: 2005/03/05 21:58:04 $
 * 0.5, 16/03/02
 * 0.4, 11/02/02
 * 0.3, 24/01/02
 * 0.2, 23/11/01
 * 0.1, 23/09/01
 */
public class EntryInfosPanel extends EntryPropertyPage
{
  private OpenToolEntry entry;
  private Product product;


  JPanel container = new JPanel();
  JLabel jLabel1 = new JLabel();
  JCheckBox cbNew = new JCheckBox();
  JCheckBox cbUpdated = new JCheckBox();
  JLabel jLabel3 = new JLabel();
  JLabel lTitle = new JLabel();
  Border border1;
  TitledBorder titledBorder1;
  JLabel jLabel11 = new JLabel();
  JLabel lCopyright = new JLabel();
  Border border2;
  TitledBorder titledBorder2;
  JLabel jLabel17 = new JLabel();
  JLabel lFileSize = new JLabel();
  JLabel jLabel2 = new JLabel();
  JLabel lJBVersions = new JLabel();
  JLabel lEmail = new JLabel();
  JLabel jLabel9 = new JLabel();
  JLabel jLabel7 = new JLabel();
  JTextField lURL = new JTextField();
  JLabel jLabel5 = new JLabel();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JPanel jPanel6 = new JPanel();
  JLabel lName = new JLabel();
  Border border3;
  TitledBorder titledBorder3;
  JLabel jLabel13 = new JLabel();
  JLabel lUpdatedDate = new JLabel();
  JLabel lOriginalDate = new JLabel();
  JLabel jLabel15 = new JLabel();
  JPanel jPanel5 = new JPanel();
  Border border4;
  TitledBorder titledBorder4;
  GridBagLayout gridBagLayout2 = new GridBagLayout();
  GridBagLayout gridBagLayout3 = new GridBagLayout();
  JLabel jLabel6 = new JLabel();
  JLabel lDownloads = new JLabel();
  private JPanel jPanel1 = new JPanel();
  private Border border5 = BorderFactory.createLineBorder(Color.gray, 1);
  private Border border6 = new TitledBorder(border5, "Author:");

  public EntryInfosPanel()
  {
    try
    {
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }


  public EntryInfosPanel(OpenToolEntry entry)
  {
    this.product = ProductManager.getProduct(entry.getRepository().getProduct());
    this.entry = entry;
    try
    {
      jbInit();
      postJBInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }


  public String getTitle()
  {
    return "General";
  }

  private void postJBInit() throws Exception
  {
    if (entry == null)
    {
      return;
    }

    String tmp = null;

    cbNew.setSelected(entry.isNew());
    cbUpdated.setSelected(entry.isUpdated());

    lTitle.setText(entry.getTitle());

    jLabel2.setText("For " + product.getName() + " versions:");
    lJBVersions.setText("" + entry.getRunMinVersion() + " - " + entry.getRunMaxVersion());

    Author author = entry.getAuthor();
    if (author != null)
    {
      lName.setText(author.getName());
      String email = author.getEmail();
      if (email != null)
      {
        lEmail.setText(email);
      }
    }

    tmp = entry.getProperty(OpenToolEntry.OT_SITE_URL);
    if (tmp != null)
    {
      lURL.setText(tmp);
    }
    tmp = entry.getProperty(OpenToolEntry.COPYRIGHT);
    if (tmp != null)
    {
      lCopyright.setText(tmp);
    }
    tmp = entry.getProperty(OpenToolEntry.ORIGINAL_UPLOAD);
    if (tmp != null)
    {
      lOriginalDate.setText(tmp);
    }
    /** @todo should format for local user conventions */
    lUpdatedDate.setText("" + entry.getLastModifiedDate());

    tmp = entry.getProperty(OpenToolEntry.SIZE);
    if (tmp != null)
    {
      lFileSize.setText(tmp);
    }
    lDownloads.setText("" + entry.getNbDownloads());


  }

  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(SystemColor.controlText, 1);
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), "Author");
    border2 = BorderFactory.createLineBorder(Color.gray, 1);
    titledBorder2 = new TitledBorder(border2, "Date");
    border3 = BorderFactory.createLineBorder(SystemColor.controlText, 1);
    titledBorder3 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), "Author:");
    border4 = BorderFactory.createLineBorder(Color.lightGray, 1);
    titledBorder4 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), "Date:");
    jLabel1.setText("ID:");
    container.setLayout(gridBagLayout3);
    cbNew.setEnabled(false);
    cbNew.setText("New");
    cbUpdated.setEnabled(false);
    cbUpdated.setText("Updated");
    jLabel3.setText("Title:");
    lTitle.setText(" Unknown");
    jLabel11.setText("Copyright:");
    lCopyright.setText("Unknown");
    jLabel17.setText("Size:");
    lFileSize.setText("Unknown");
    this.setMinimumSize(new Dimension(1, 1));
    this.setPreferredSize(new Dimension(300, 312));
    jLabel2.setText("For JBuilder versions:");
    lJBVersions.setText("Unknown");
    lEmail.setText("Anonymous");
    jLabel9.setText("URL:");
    jLabel7.setText("E-mail:");
    lURL.setEditable(false);
    lURL.setText("Unknown");
    jLabel5.setText("Name:");
    jPanel6.setLayout(gridBagLayout1);
    lName.setText("Unknown");
    jPanel6.setBorder(border6);
    jLabel13.setText("Original:");
    lUpdatedDate.setText("Unknown");
    lOriginalDate.setText("Unknown");
    jLabel15.setText("Updated:");
    jPanel5.setLayout(gridBagLayout2);
    jPanel5.setBorder(titledBorder4);
    jLabel6.setText("Downloads:");
    lDownloads.setText("Unknown");
    jPanel5.add(jLabel15, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 0, 0), 0, 0));
    jPanel5.add(jLabel13, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                 , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( -8, 2, 0, 0), 0, 0));
    jPanel5.add(lUpdatedDate, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
      , GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));
    jPanel5.add(lOriginalDate, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
      , GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets( -8, 5, 0, 0), 0, 0));
    this.setLayout(new BorderLayout());
    container.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 0), 0, 0));
    container.add(jLabel2, new GridBagConstraints(0, 2, 4, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST, GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 0), 0, 0));
    container.add(jLabel3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST, GridBagConstraints.NONE,
                                                  new Insets(9, 5, 0, 0), 0, 0));
    container.add(lTitle, new GridBagConstraints(1, 1, 6, 1, 0.0, 0.0
                                                 , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                                                 new Insets(5, 5, 0, 5), 0, 0));
    container.add(cbUpdated, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    container.add(cbNew, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(5, 5, 0, 0), 0, 0));
    container.add(lFileSize, new GridBagConstraints(1, 7, 2, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 0), 0, 0));
    container.add(jPanel5, new GridBagConstraints(0, 6, 6, 1, 0.0, 0.0
                                                  , GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                                  new Insets(5, 5, 0, 5), 0, 0));
    container.add(lURL, new GridBagConstraints(1, 4, 5, 1, 0.0, 0.0
                                               , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                               new Insets(0, 0, 0, 0), 0, 0));
    jPanel6.add(jPanel1, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0
                                                , GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                                                new Insets(0, 0, 0, 0), 0, 0));
    container.add(jLabel11, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    container.add(lCopyright, new GridBagConstraints(1, 5, 3, 1, 1.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
    container.add(lDownloads, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    container.add(jLabel17, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    container.add(jLabel6, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 0), 0, 0));
    container.add(jPanel6, new GridBagConstraints(0, 3, 6, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                                  new Insets(5, 5, 0, 5), 0, 0));
    jPanel6.add(lName, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
                                              , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                              new Insets(0, 5, 0, 5), 0, 0));
    jPanel6.add(jLabel5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(0, 5, 0, 0), 0, 0));
    jPanel6.add(lEmail, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
                                               , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
                                               new Insets(5, 5, 0, 5), 0, 0));
    jPanel6.add(jLabel7, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(5, 5, 0, 0), 0, 0));
    container.add(jLabel9, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                  new Insets(0, 5, 0, 0), 0, 0));
    container.add(lJBVersions, new GridBagConstraints(3, 2, 2, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
    this.add(container, java.awt.BorderLayout.NORTH);
  }
}
