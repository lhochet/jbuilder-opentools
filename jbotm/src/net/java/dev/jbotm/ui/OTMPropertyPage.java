/*
   Copyright: Copyright (c) 2001-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;

import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/03/05 21:57:39 $
 * @created 20/02/05
 */
public class OTMPropertyPage extends PropertyPage
{
  private JPanel jPanel1 = new JPanel();
  private JPanel jPanel2 = new JPanel();
  private JCheckBox cbAutoCheck = new JCheckBox();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private BorderLayout borderLayout1 = new BorderLayout();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPanel jPanel3 = new JPanel();
  private GridLayout gridLayout1 = new GridLayout();
  private JCheckBox cbShowTabIfNewOrUpdatedAndAutocheking = new JCheckBox();
  private JCheckBox cbShowIfMsgViewDisplayed = new JCheckBox();
  private JCheckBox cbNeverShow = new JCheckBox();
  private JCheckBox cbAlwaysShow = new JCheckBox();
  private ButtonGroup btnGrpShowIfNoNewUpdated = new ButtonGroup();
  private Border border2;
  private JCheckBox cbOnceADay = new JCheckBox();
  private Border border4;

  public OTMPropertyPage()
  {
    try
    {
      jbInit();

    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public void writeProperties()
  {
    OTMPropertyGroup.AUTO_CHECK.setBoolean(cbAutoCheck.isSelected());
    OTMPropertyGroup.AUTO_CHECK_ONCE_A_DAY.setBoolean(cbOnceADay.isSelected());

    if (cbAlwaysShow.isSelected())
    {
      OTMPropertyGroup.IF_NEW_UPDATED_ONLY.setInteger(OTMPropertyGroup.ALWAYS_SHOW);
    }
    else if (cbShowIfMsgViewDisplayed.isSelected())
    {
      OTMPropertyGroup.IF_NEW_UPDATED_ONLY.setInteger(OTMPropertyGroup.ONLY_IF_MESSAGEVIEW_SHOWN);
    }
    else if (cbShowTabIfNewOrUpdatedAndAutocheking.isSelected())
    {
      OTMPropertyGroup.IF_NEW_UPDATED_ONLY.setInteger(OTMPropertyGroup.NOT_IF_AUTOCHECKING);
    }
    else if (cbNeverShow.isSelected())
    {
      OTMPropertyGroup.IF_NEW_UPDATED_ONLY.setInteger(OTMPropertyGroup.NEVER_SHOW);
    }

  }

  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("/doc/codecentralpropertypage.html").toString());
  }

  public void readProperties()
  {
    cbAutoCheck.setSelected(OTMPropertyGroup.AUTO_CHECK.getBoolean());
    if (!cbAutoCheck.isSelected())
    {
      cbShowTabIfNewOrUpdatedAndAutocheking.setEnabled(false);
      cbOnceADay.setEnabled(false);
    }
    cbOnceADay.setSelected(OTMPropertyGroup.AUTO_CHECK_ONCE_A_DAY.getBoolean());
    switch (OTMPropertyGroup.IF_NEW_UPDATED_ONLY.getInteger())
    {
      case OTMPropertyGroup.ALWAYS_SHOW:
        cbAlwaysShow.setSelected(true);
        break;
      case OTMPropertyGroup.ONLY_IF_MESSAGEVIEW_SHOWN:
        cbShowIfMsgViewDisplayed.setSelected(true);
        break;
      case OTMPropertyGroup.NOT_IF_AUTOCHECKING:
        cbShowTabIfNewOrUpdatedAndAutocheking.setSelected(true);
        break;
      case OTMPropertyGroup.NEVER_SHOW:
        cbNeverShow.setSelected(true);
        break;
    }

  }

  private void jbInit() throws Exception
  {
    border2 =
      BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1),
      "If there are no new or updated OpenTools:")
                                         , BorderFactory.createEmptyBorder(0, 5, 0, 0));
    border4 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1),
      "Check for new or updated OpenTools for:"), BorderFactory.createEmptyBorder(0, 5, 0, 0));
    this.setLayout(borderLayout2);
    jPanel2.setLayout(gridBagLayout1);
    jPanel1.setLayout(borderLayout1);
    cbAutoCheck.setToolTipText("Auto check for new/updated OpenTools on CodeCentral when starting");
    cbAutoCheck.setActionCommand("Auto check for new/updated OpenTools on CodeCentral when starting");
    cbAutoCheck.setText("Auto check for new/updated OpenTools when starting");
    cbAutoCheck.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cbAutoCheck_actionPerformed(e);
      }
    });
    this.setMinimumSize(new Dimension(1, 1));
    this.setPreferredSize(new Dimension(450, 350));
    jPanel3.setBorder(border2);
    jPanel3.setLayout(gridLayout1);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(0);
    cbShowTabIfNewOrUpdatedAndAutocheking.setText("and autochecking don\'t show message tab");
    cbShowIfMsgViewDisplayed.setText("Show tab only if message view is already displayed");
    cbNeverShow.setText("Never show");
    cbAlwaysShow.setText("Always show message tab");
    cbOnceADay.setText("Auto check when starting only once a day");
    cbOnceADay.setActionCommand("Auto check for new/updated OpenTools on CodeCentral when starting");
    cbOnceADay.setToolTipText("Auto check once a day only");
    cbOnceADay.setVerifyInputWhenFocusTarget(true);
    jPanel3.add(cbAlwaysShow, null);
    jPanel3.add(cbShowIfMsgViewDisplayed, null);
    jPanel3.add(cbShowTabIfNewOrUpdatedAndAutocheking, null);
    jPanel3.add(cbNeverShow, null);
    btnGrpShowIfNoNewUpdated.add(cbAlwaysShow);
    btnGrpShowIfNoNewUpdated.add(cbShowIfMsgViewDisplayed);
    btnGrpShowIfNoNewUpdated.add(cbShowTabIfNewOrUpdatedAndAutocheking);
    btnGrpShowIfNoNewUpdated.add(cbNeverShow);
    jPanel2.add(cbAutoCheck, new GridBagConstraints(0, 0, 4, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 6, 0));
    jPanel2.add(jPanel3, new GridBagConstraints(0, 2, 5, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                                                new Insets(5, 5, 5, 5), 0, 0));
    jPanel2.add(cbOnceADay, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    this.add(jPanel1, java.awt.BorderLayout.NORTH);
    jPanel1.add(jPanel2, java.awt.BorderLayout.WEST);
  }

  void cbAutoCheck_actionPerformed(ActionEvent e)
  {
    if (cbAutoCheck.isSelected())
    {
      cbShowTabIfNewOrUpdatedAndAutocheking.setEnabled(true);
      cbOnceADay.setEnabled(true);
    }
    else
    {
      cbShowTabIfNewOrUpdatedAndAutocheking.setEnabled(false);
      cbOnceADay.setEnabled(false);
    }
  }

}
