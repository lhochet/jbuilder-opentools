/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;
import javax.swing.*;

import net.java.dev.jbotm.structs.OpenToolEntry;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.3 $ $Date: 2005/01/23 22:05:22 $
 * @created 23/09/01
 */
public class EntryDescriptionPanel extends EntryPropertyPage
{
  private OpenToolEntry entry;
  JScrollPane jScrollPane1 = new JScrollPane();
  BorderLayout borderLayout1 = new BorderLayout();
  JEditorPane taDescription = new JEditorPane();

  public EntryDescriptionPanel()
  {
    this(null);
  }

  public EntryDescriptionPanel(OpenToolEntry entry)
  {
    this.entry = entry;

    try
    {

      if (entry.getDescription() != null)
      {
        taDescription.setContentType("text/html");
        taDescription.setText(entry.getDescription());
        taDescription.select(0, 0);
      }
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public String getTitle()
  {
    return "Description";
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    taDescription.setEditable(false);
    this.add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(taDescription, null);
  }
}
