/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;

/**
 * @author Ludovic HOCHET
 * @version 0.1, 18/12/01
 */
public class FileEntryExtractPathEditorPanel extends JPanel
{
  JTextField tfExtractPath = new JTextField();
  JButton btnBrowseLocation = new JButton();
  BorderLayout borderLayout1 = new BorderLayout();
  String defaultName = null;

  public FileEntryExtractPathEditorPanel()
  {
    try
    {
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    btnBrowseLocation.setMaximumSize(new Dimension(23, 27));
    btnBrowseLocation.setMinimumSize(new Dimension(23, 27));
    btnBrowseLocation.setPreferredSize(new Dimension(23, 27));
    btnBrowseLocation.setText("...");
    btnBrowseLocation.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBrowseLocation_actionPerformed(e);
      }
    });
    this.setLayout(borderLayout1);
    this.add(tfExtractPath, BorderLayout.CENTER);
    this.add(btnBrowseLocation, BorderLayout.EAST);
  }

  public void setDefaultExtractName(String name)
  {
    defaultName = name;
  }

  public void setExtractPath(String path)
  {
    tfExtractPath.setText(path);
  }

  public String getExtractPath()
  {
    return tfExtractPath.getText();
  }

  void btnBrowseLocation_actionPerformed(ActionEvent e)
  {
    try
    {
      File f = new File(tfExtractPath.getText());
      if (tfExtractPath.getText().equals(""))
      {
        f = PropertyManager.getInstallRootUrl().getFileObject();
        f = new File(f, "lib/ext");
        String dftfn = (new File(defaultName)).getName();
        f = new File(f, dftfn);
      }
      Url dest = UrlChooser.promptForUrl(Browser.getActiveBrowser(), new Url(f));
      if (dest != null)
      {
        tfExtractPath.setText(dest.getFile());
      }
    }
    catch (Exception ex)
    {
      // print stack trace and do nothing
      ex.printStackTrace();
    }
  }
}
