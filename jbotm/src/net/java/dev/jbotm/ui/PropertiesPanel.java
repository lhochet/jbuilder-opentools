
/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;
import javax.swing.*;

import com.borland.primetime.help.*;
import com.borland.primetime.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import java.util.*;

/**
 * @author Ludovic HOCHET
 * @version 0.2, 23/11/01
 * @created 23/09/01
 */
public class PropertiesPanel extends JPanel implements DialogValidator
{
  private OpenToolEntry entry = null;
  JTabbedPane jTabbedPane1 = new JTabbedPane();
  BorderLayout borderLayout1 = new BorderLayout();
  EntryInfosPanel infos;
  EntryDescriptionPanel description;
  EntryFilesPanel files;
  EntryDownloadPanel download;
  EntryCommentsPanel comments;

  public PropertiesPanel()
  {
    this(null);
  }

  public PropertiesPanel(OpenToolEntry entry)
  {
    this.entry = entry;

    try
    {
      jbInit();
      postJBInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private void postJBInit() throws Exception
  {
    Iterator iter = entry.getPropertyPages().iterator();
    while (iter.hasNext())
    {
      EntryPropertyPage pg = (EntryPropertyPage) iter.next();
      jTabbedPane1.add(pg, pg.getTitle());
    }
  }


  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    this.setPreferredSize(new Dimension(400, 380));
    this.add(jTabbedPane1, BorderLayout.CENTER);
  }

  public boolean validateDialog()
  {
    return files.validateDialog() && download.validateDialog();
  }

  public void selectDownload()
  {
    jTabbedPane1.setSelectedComponent(download);
  }

  public ZipHelpTopic getHelpPage()
  {
    return new ZipHelpTopic(null, getClass().getResource("/doc/properties.html").toString());
  }

}
