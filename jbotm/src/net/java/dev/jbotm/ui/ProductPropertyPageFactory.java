/*
   Copyright: Copyright (c) 2001-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import com.borland.primetime.properties.PropertyPageFactory;
import com.borland.primetime.properties.PropertyPage;
import net.java.dev.jbotm.OTMPropertyGroup;
import com.borland.primetime.properties.PropertyManager;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/03/05 21:57:47 $
 * @created 20/02/05
 */
public class ProductPropertyPageFactory extends PropertyPageFactory
{
  public static final Object PRODUCTS_TOPIC = new Object();

  public ProductPropertyPageFactory()
  {
    super("Products");
  }
  public PropertyPage createPropertyPage()
  {
    return new ProductsPropertyPage();
  }

  public PropertyPageFactory[] getNestedFactories()
  {
    return PropertyManager.getPageFactories(PRODUCTS_TOPIC);
  }

}
