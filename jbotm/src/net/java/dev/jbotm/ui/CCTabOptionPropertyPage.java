/*
   Copyright: Copyright (c) 2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.awt.*;
import javax.swing.*;

import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;
import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.3 $ $Date: 2005/03/05 21:58:04 $
 * @created 24/07/04
 */
public class CCTabOptionPropertyPage extends PropertyPage
{
  public CCTabOptionPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * readProperties
   */
  public void readProperties()
  {
    cbxShowDownloads.setSelected(OTMPropertyGroup.SHOW_DOWNLOADS.getBoolean());
    cbxDownloadChanges.setSelected(OTMPropertyGroup.SHOW_DOWNLOAD_CHANGES.getBoolean());
    cbxFilterForVersion.setSelected(OTMPropertyGroup.FILTER_JB_VERSIONS.getBoolean());
    rThisVersion.setSelected(OTMPropertyGroup.FILTER_JB_THIS_VERSION.getBoolean());
    if (rThisVersion.isSelected())
    {
      tfVersionAbove.setText(OTMPropertyGroup.JB_VERSION.toString());
    }
    else
    {
      rVersionAbove.setSelected(true);
      tfVersionAbove.setText(OTMPropertyGroup.FILTER_JB_MIN_VERSION.getValue());
    }
  }

  /**
   * writeProperties
   */
  public void writeProperties()
  {
    OTMPropertyGroup.SHOW_DOWNLOADS.setBoolean(cbxShowDownloads.isSelected());
    OTMPropertyGroup.SHOW_DOWNLOAD_CHANGES.setBoolean(cbxDownloadChanges.isSelected());
    OTMPropertyGroup.FILTER_JB_VERSIONS.setBoolean(cbxFilterForVersion.isSelected());
    OTMPropertyGroup.FILTER_JB_THIS_VERSION.setBoolean(rThisVersion.isSelected());
    OTMPropertyGroup.FILTER_JB_MIN_VERSION.setValue(tfVersionAbove.getText());
  }

  /**
   * getHelpTopic
   *
   * @return HelpTopic
   * @todo Implement this com.borland.primetime.properties.PropertyPage method
   */
  public HelpTopic getHelpTopic()
  {
    return null;
  }

  private void jbInit() throws Exception
  {
    this.setLayout(gridBagLayout2);
    cbxShowDownloads.setText("Show Downloads on Opentools list view");
    cbxDownloadChanges.setText("Show Download changes");
    cbxFilterForVersion.setText("Filter OpenTools for:");
    jPanel1.setLayout(gridBagLayout1);
    tfVersionAbove.setText("11.0.236.0");
    jPanel1.setBounds(new Rectangle(63, 167, 42, 13));
    rThisVersion.setText("this version");
    rVersionAbove.setText("versions above");
    buttonGroup1.add(rThisVersion);
    this.add(cbxShowDownloads, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(13, 10, 0, 94), 52, 0));
    this.add(cbxDownloadChanges, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 33, 0, 204), 0, 0));
    this.add(cbxFilterForVersion, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(32, 3, 0, 175), 85, 1));
    this.add(rThisVersion, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 24, 0, 0), 22, 0));
    this.add(jPanel2, new GridBagConstraints(0, 5, 1, 1, 1.0, 1.0
                                             , GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                             new Insets(91, 80, 22, 42), 0, 0));
    this.add(rVersionAbove, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(13, 26, 0, 0), 2, 0));
    buttonGroup1.add(rVersionAbove);
    this.add(tfVersionAbove, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(15, 7, 0, 176), 3, 0));
  }

  private JCheckBox cbxShowDownloads = new JCheckBox();
  private JCheckBox cbxDownloadChanges = new JCheckBox();
  JCheckBox cbxFilterForVersion = new JCheckBox();
  JPanel jPanel1 = new JPanel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JTextField tfVersionAbove = new JTextField();
  private JRadioButton rThisVersion = new JRadioButton();
  private JPanel jPanel2 = new JPanel();
  private JRadioButton rVersionAbove = new JRadioButton();
  private ButtonGroup buttonGroup1 = new ButtonGroup();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
}
