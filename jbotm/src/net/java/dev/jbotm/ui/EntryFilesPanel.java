/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.structs.OpenToolEntry;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.3 $ $Date: 2005/01/23 22:05:22 $
 * 0.2, 14/12/01
 * 0.1, 3/10/01
 */
public class EntryFilesPanel extends EntryPropertyPage
{
  class FilesModel extends AbstractTableModel
  {
    private ArrayList rows = new ArrayList();

    public FilesModel(ArrayList rows)
    {
      this.rows = rows;
    }

    public int getColumnCount()
    {
      return 5;
    }

    public Object getValueAt(int y, int x)
    {
      FileEntry fentry = (FileEntry)rows.get(y);
      switch (x)
      {
        case 0:
          return fentry.name;
        case 1:
          return fentry.date;
        case 2:
          return fentry.size;
        case 3:
          return new Boolean(fentry.autoExtract);
        case 4:
          return fentry.extractPath;
      }
      return new ArrayIndexOutOfBoundsException("Invalid column.");
    }

    public int getRowCount()
    {
      if (rows == null)
      {
        return 0;
      }
      return rows.size();
    }

    public String getColumnName(int column)
    {
      switch (column)
      {
        case 0:
          return "File Name";
        case 1:
          return "Date";
        case 2:
          return "Size";
        case 3:
          return "Auto Extract";
        case 4:
          return "Extract to";
      }
      return "";
    }

    public Class getColumnClass(int c)
    {
      if (getValueAt(0, c) == null)
      {
        return (new Object()).getClass();
      }
      return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int rowIndex, int colIndex)
    {
      if (colIndex > 2)
      {
        return true;
      }
      return false;
    }

    public void setValueAt(Object value, int row, int col)
    {
      FileEntry fentry = (FileEntry)rows.get(row);
      switch (col)
      {
        case 0:
          fentry.name = (String)value;
          break;
        case 1:
          fentry.date = (String)value;
          break;
        case 2:
          fentry.size = (String)value;
          break;
        case 3:
          fentry.autoExtract = ((Boolean)value).booleanValue();
          break;
        case 4:
          fentry.extractPath = (String)value;
          break;
      }
      fireTableCellUpdated(row, col);
    }
  }

  OpenToolEntry entry = null;

  JScrollPane jScrollPane1 = new JScrollPane();
  Border border1;
  TitledBorder titledBorder1;
  JTable filesTable = new JTable();
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel sizePanel = new JPanel();
  JLabel jLabel2 = new JLabel();
  JLabel jLabel1 = new JLabel();
  FlowLayout flowLayout1 = new FlowLayout();

  public EntryFilesPanel()
  {
    this(null);
  }

  public EntryFilesPanel(OpenToolEntry entry)
  {
    this.entry = entry;

    try
    {
      preJBInit();
      jbInit();
      postJBInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }


  public String getTitle()
  {
    return "Files";
  }

  private void preJBInit() throws Exception
  {
    if (entry == null)
    {
      return;
    }
    filesTable.setModel(new FilesModel(entry.getFiles()));
  }

  private void postJBInit() throws Exception
  {
    if (entry == null)
    {
      return;
    }
    String sz = entry.getProperty(OpenToolEntry.SIZE);
    if (sz != null)
    {
      jLabel2.setText(sz);

    }
    filesTable.getColumnModel().getColumn(0).setPreferredWidth(150);
    filesTable.getColumnModel().getColumn(3).setPreferredWidth(50);
    filesTable.getColumnModel().getColumn(4).setPreferredWidth(100);
    filesTable.getColumnModel().getColumn(4).setCellEditor(new FileEntryExtractPathEditor());

  }

  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(Color.gray, 2);
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), "Content");
    this.setLayout(borderLayout1);
    jScrollPane1.setBorder(titledBorder1);
    jLabel2.setText("Unknown");
    jLabel1.setText("Zip size:");
    sizePanel.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    filesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    this.add(jScrollPane1, BorderLayout.CENTER);
    this.add(sizePanel, BorderLayout.NORTH);
    jScrollPane1.getViewport().add(filesTable, null);
    sizePanel.add(jLabel1, null);
    sizePanel.add(jLabel2, null);
  }

  public boolean validateDialog()
  {
    if (filesTable.isEditing())
    {
      filesTable.getCellEditor().stopCellEditing();
    }
    return true;
  }
}
