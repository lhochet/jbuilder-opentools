/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.ui;

import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.vfs.*;
import com.borland.primetime.vfs.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.ccold.CodeCentralOTDownloader;
import net.java.dev.jbotm.structs.OpenToolEntry;
import java.util.*;
import net.java.dev.jbotm.repo.cc.CodeCentralPropertyGroup;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.repo.ccold.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.6 $ $Date: 2005/03/05 21:58:04 $
 * 0.2, 23/11/01
 * 0.1, 23/09/01
 */
public class EntryDownloadPanel extends EntryPropertyPage
{
  private OpenToolEntry entry;

  JPanel jPanel1 = new JPanel();
  JButton btnChoosePath = new JButton();
  JCheckBox cbAutoDownload = new JCheckBox();
  JLabel lLastDownloaded = new JLabel();
  JTextField tfPath = new JTextField();
  JLabel jLabel2 = new JLabel();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel3 = new JLabel();
  JTextField tfDownloadURL = new JTextField();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  BorderLayout borderLayout1 = new BorderLayout();

  public EntryDownloadPanel()
  {
    this(null);
  }

  public EntryDownloadPanel(OpenToolEntry entry)
  {
    this.entry = entry;
    try
    {
      jbInit();
      postJBInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }


  public String getTitle()
  {
    return "Download";
  }

  private void postJBInit() throws Exception
  {
    if (entry == null)
    {
      return;
    }

    if (entry.getProperty(OpenToolEntry.LOCAL_NAME) == null)
    {
      String defaultNamePath = CodeCentralPropertyGroup.DEST_PATH.getValue();
      if ((defaultNamePath != null) && (defaultNamePath.equals("")))
      {
        defaultNamePath = null;
      }
      if (defaultNamePath == null)
      {
        defaultNamePath = System.getProperty("user.dir");
      }
      if (defaultNamePath == null)
      {
        defaultNamePath = System.getProperty("user.home");
      }
      if (defaultNamePath == null)
      {
        defaultNamePath = "";

      }
      String defaultName = entry.getTitle();
      StringBuffer sb = new StringBuffer(defaultName);
      for (int i = 0; i < sb.length(); ++i)
      {
        if (!Character.isJavaIdentifierPart(sb.charAt(i)))
        {
          sb.setCharAt(i, '_');
        }
      }
      defaultName = sb.toString();

      File f = new File(defaultNamePath, defaultName + ".zip");
      defaultName = f.getAbsolutePath();

      tfPath.setText(defaultName);
    }
    else
    {
      tfPath.setText(entry.getProperty(OpenToolEntry.LOCAL_NAME));
    }

    cbAutoDownload.setSelected(entry.isAutoDownload());
    if (entry.getProperty(OpenToolEntry.LAST_DOWNLOADED) != null)
    {
      java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd H:mm:ss");
      sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
      Date d = sdf.parse(entry.getProperty(OpenToolEntry.LAST_DOWNLOADED));
      java.text.DateFormat sdf2 = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
      lLastDownloaded.setText(sdf2.format(d));
    }
    tfDownloadURL.setText(getDownloadURL(entry.getIdInRepository()));
  }
  
  private String getDownloadURL(String id)
  {
    return CodeCentralPropertyGroup.CC_BASE + "Download.aspx?id=" + id;
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    this.setMinimumSize(new Dimension(1, 1));
    this.setPreferredSize(new Dimension(400, 300));
    jPanel1.setLayout(gridBagLayout1);
    btnChoosePath.setText("...");
    btnChoosePath.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnChoosePath_actionPerformed(e);
      }
    });
    cbAutoDownload.setText("Auto download when updated");
    lLastDownloaded.setText("Unknown");
    tfPath.setToolTipText("");
    jLabel2.setText("Last downloaded:");
    jLabel1.setText("Local filename:");
    jLabel3.setText("Download URL:");
    tfDownloadURL.setEditable(false);
    jPanel1.add(lLastDownloaded, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0
      , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    jPanel1.add(btnChoosePath, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0
      , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    jPanel1.add(cbAutoDownload, new GridBagConstraints(0, 1, 4, 1, 0.0, 0.0
      , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    jPanel1.add(jLabel2, new GridBagConstraints(0, 2, 3, 1, 0.0, 0.0
                                                , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    jPanel1.add(tfPath, new GridBagConstraints(1, 0, 3, 1, 1.0, 0.0
                                               , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 261, 0));
    jPanel1.add(jLabel3, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0
                                                , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 5, 0), 7, 0));
    jPanel1.add(tfDownloadURL, new GridBagConstraints(2, 3, 3, 1, 1.0, 0.0
      , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 5), 284, 0));
    this.add(jPanel1, BorderLayout.NORTH);
  }

  void btnChoosePath_actionPerformed(ActionEvent e)
  {
    try
    {
      Url dest = UrlChooser.promptForUrl(Browser.getActiveBrowser(), new Url(new File(CodeCentralPropertyGroup.DEST_PATH.getValue())));
      if (dest != null)
      {
        tfPath.setText(dest.getFile());
      }
    }
    catch (Exception ex)
    {
      // print stack trace and do nothing
      ex.printStackTrace();
    }
  }

  public boolean validateDialog()
  {
    String localName = tfPath.getText();
    if (localName.equals(""))
    {
      localName = null;

    }
    entry.setProperty(OpenToolEntry.LOCAL_NAME, localName);
    entry.setAutoDownload(cbAutoDownload.isSelected());
    entry.save();

    return true;
  }
}
