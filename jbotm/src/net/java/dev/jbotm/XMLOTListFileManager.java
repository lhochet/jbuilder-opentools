
/*
   Copyright: Copyright (c) 2001-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.w3c.dom.Text;
import com.borland.primetime.properties.*;
import com.borland.primetime.util.*;
import lh.util.xmlwriter.*;

import net.java.dev.jbotm.repo.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.repo.ccold.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.10 $ $Date: 2005/06/28 22:18:28 $
 * 0.2, 14/08/02
 * 0.1, 25/09/01
 */
public class XMLOTListFileManager
{
  private static Logger log = Logger.getLogger(XMLOTListFileManager.class.getName());

  private String filename = "community/cc/opentools.xml"; /** @todo no default? */

  private Document document = null;

  private Map entries = null;

  public XMLOTListFileManager(String filename)
  {
    this.filename = filename;
  }

  public void setEntries(Map entries)
  {
    this.entries = entries;
  }

  public Map getEntries()
  {
    return entries;
  }

  private static Element getSubElement(Element elt, String tagname)
  {
    if (elt == null) return null;
    
    NodeList lst = elt.getElementsByTagName(tagname);
    if (lst.getLength() == 0)
    {
      return null;
    }
    if (lst.getLength() > 1)
    {
      return null;
    }
    return (Element)lst.item(0);
  }

  private static String getElementText(Element elt)
  {
    NodeList lst = elt.getChildNodes();
    for (int i = 0; i < lst.getLength(); i++)
    {
      Node n = lst.item(i);
      if (n.getNodeType() == Node.TEXT_NODE)
      {
        return ((Text)n).getData().trim();
      }
    }
    return null;
  }

  private static String getSubElementText(Element elt, String tagname)
  {
    Element selt = getSubElement(elt, tagname);
    if (selt == null)
    {
      return null;
    }
    return getElementText(selt);
  }

//  private String getSubElementCData(Element elt, String tagname)
//  {
//    Element selt = getSubElement(elt, tagname);
//    if (selt == null)
//    {
//      return null;
//    }
//    NodeList lst = selt.getChildNodes();
//    for (int i = 0; i < lst.getLength(); i++)
//    {
//      Node n = lst.item(i);
//      if (n.getNodeType() == Node.CDATA_SECTION_NODE)
//      {
//        return ((CDATASection)n).getData(); //n.getNodeValue();
//      }
//    }
//    return null;
//  }

  public void load()
  {
    log.entering("XMLFileOTListLoader", "load");
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
      sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

      entries = new TreeMap();

      // back up file
      File f = PropertyManager.getSettingsUrl(filename).getFileObject();
      File f2 = new File(f.getAbsolutePath() + ".old");

      // if file does not exist then attempt to open the backup copy
      if (!f.exists())
      {
        File fold = new File(f.getAbsolutePath() + ".old");
        if (!fold.exists())
        {
          return; // abort loading if we can't find any exiting file
        }
        fold.renameTo(f);
      }

      if (!f2.exists())
      {
        f2.getParentFile().mkdirs();
        f2.createNewFile();
      }

      int rlen = f.length() > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int)f.length();
      BufferedReader r = new BufferedReader(new FileReader(f), rlen);
      FileWriter w = new FileWriter(f2);

      char[] buf = new char[rlen];
      while (r.read(buf) > 0)
      {
        w.write(buf);
      }
      r.close();
      w.close();
      // eo backup

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      document = builder.parse(f);

      final boolean filterver = OTMPropertyGroup.FILTER_JB_VERSIONS.getBoolean();
      final boolean filterthisversion = OTMPropertyGroup.FILTER_JB_THIS_VERSION.getBoolean();
      Version filterversion = null;
      if (filterthisversion)
      {
        filterversion = OTMPropertyGroup.JB_VERSION;
      }
      else
      {
        try
        {
          filterversion = new Version(OTMPropertyGroup.FILTER_JB_MIN_VERSION.getValue());
        }
        catch (Exception ex2)
        {
          // keep the version null -> no OT will match the null version
        }
      }

      NodeList entrieslist = document.getDocumentElement().getElementsByTagName("entry");
      for (int i = 0; i < entrieslist.getLength(); i++)
      {
        Element entrynode = (Element)entrieslist.item(i);

        Repository repository = RepositoryManager.getRepository(entrynode.getAttribute("repo"));
        OpenToolEntry entry = repository.createEntry();
        entry.setID(entrynode.getAttribute("id"));
        entries.put(entry.getID(), entry);

        entry.setRepository(repository);
        entry.setIdInRepository(entrynode.getAttribute("repoid"));

        String istracked = getSubElementText(entrynode, "istracked");
        if (istracked != null)
        {
          if (istracked.equals("true"))
          {
            entry.setTracked(true);
          }
        }

        entry.setTitle(getSubElementText(entrynode, "title"));
        entry.setRunMinVersion(new Version(getSubElementText(entrynode, "min_version")));
        entry.setRunMaxVersion(new Version(getSubElementText(entrynode, "max_version")));

        Element authornode = getSubElement(entrynode, "author");
        Author author = new Author();
        entry.setAuthor(author);
        author.setName(getSubElementText(authornode, "name"));
        author.setEmail(getSubElementText(authornode, "email"));


        Element datenode = getSubElement(entrynode, "date");
        String datestr = getSubElementText(datenode, "updated");
        try
        {
          if (datestr != null)
          {
            entry.setLastModifiedDate(sdf.parse(datestr));
          }
        }
        catch (ParseException ex3)
        {
          log.logp(Level.CONFIG, "XMLFileOTListLoader", "load", "pb during parsing of entry date", ex3);
          entry.setLastModifiedDate(new Date(0)); // this should set the date to 1/1/70
        }

        try
        {
          String intstr = getSubElementText(entrynode, "downloads");
          if (intstr != null)
          {
            entry.setNbDownloads(Integer.parseInt(intstr));
          }
        }
        catch (NumberFormatException ex1)
        {
          // ignore
        }

        Element downloadnode = getSubElement(entrynode, "download");
        if (downloadnode != null)
        {
          entry.setAutoDownload(Boolean.getBoolean(getSubElementText(downloadnode, "auto")));
        }

        entry.setShortDescription(getSubElementText(entrynode, "shortdesc"));

        entry.setDescription(getSubElementText(entrynode, "description"));

        Element propertiesnode = getSubElement(entrynode, "properties");
        if (propertiesnode != null)
        {
          NodeList lst = propertiesnode.getElementsByTagName("property");
          for (int z = 0; z < lst.getLength(); z++)
          {
            Element elt = (Element) lst.item(z);
            String key = elt.getAttribute("key");
            if (key != null)
            {
              String value = getElementText(elt);
              if (value != null)
              {
                entry.setProperty(key, value);
              }
            }
          }
        }

        Element filesnode = getSubElement(entrynode, "files");
        if (filesnode != null)
        {
          NodeList lst = filesnode.getElementsByTagName("file");
          ArrayList files = new ArrayList(lst.getLength());
          for (int z = 0; z < lst.getLength(); z++)
          {
            Element elt = (Element)lst.item(z);
            FileEntry fentry = new FileEntry();
            fentry.name = elt.getAttribute("name");
            fentry.date = elt.getAttribute("date");
            fentry.size = elt.getAttribute("size");
            fentry.extractPath = elt.getAttribute("extractpath");
            String autoextract = elt.getAttribute("autoextract");
            if (autoextract != null)
            {
              if (autoextract.equals("true"))
              {
                fentry.autoExtract = true;
              }
            }
            files.add(fentry);
          }
          entry.setFiles(files);
        }


        // comments
        Element commentsnode = getSubElement(entrynode, "comments");
        if (commentsnode != null)
        {
          try
          {
            String intstr = commentsnode.getAttribute("nb");
            if (intstr != null)
            {
              entry.setNbComments(Integer.parseInt(intstr));
            }
          }
          catch (NumberFormatException ex)
          {
            entry.setNbComments(0);
          }

          entry.setComments(null);
          NodeList lst = commentsnode.getChildNodes();
          ArrayList comments = new ArrayList(lst.getLength());

          for (int z = 0; z < lst.getLength(); z++)
          {
            if (lst.item(z) instanceof Element)
            {
              Element elt = (Element)lst.item(z);
              if (elt.getNodeName().equals("comment"))
              {
                CommentEntry comment = new CommentEntry();
                comments.add(comment);
                comment.id = elt.getAttribute("id");
                comment.date = elt.getAttribute("date");
                comment.author = elt.getAttribute("author");
                comment.title = elt.getAttribute("title");
                comment.comment = elt.getNodeValue();
                comment.subcomments = getSubComments(elt);
              }
            }
          }
          if (comments.size() > 0)
          {
            entry.setComments(comments);
          }
        }

        if (filterver)
        {
          CCVersionRange versions = new CCVersionRange(entry.getRunMinVersion(), entry.getRunMaxVersion());
          if (filterthisversion)
          {
            entry.setShown(versions.contains(filterversion));
          }
          else
          {
            entry.setShown(versions.getMaxVersion().compareTo(filterversion) >= 0);
          }
        }
        entries.put(entry.getID(), entry);

      }

    }
    catch (Exception ex)
    {
      log.logp(Level.CONFIG, "XMLFileOTListLoader", "load", "Problem during load of CC local entries", ex);
      ex.printStackTrace();
    }
    finally
    {
      // release doc
      document = null;
    }
    log.exiting("XMLFileOTListLoader", "load");
  }

  private ArrayList getSubComments(Element parent)
  {
    NodeList lst = parent.getChildNodes();
    if (lst.getLength() == 0)
    {
      return null;
    }

    ArrayList subcomments = new ArrayList();
    for (int z = 0; z < lst.getLength(); z++)
    {
      if (lst.item(z) instanceof Element)
      {
        Element elt = (Element)lst.item(z);
        if (elt.getNodeName().equals("comment"))
        {
          CommentEntry comment = new CommentEntry();
          subcomments.add(comment);
          comment.id = elt.getAttribute("id");
          comment.date = elt.getAttribute("date");
          comment.author = elt.getAttribute("author");
          comment.title = elt.getAttribute("title");
          comment.comment = elt.getNodeValue();
          comment.subcomments = getSubComments(elt);
        }
      }
    }
    return subcomments;
  }

  public void write()
  {
    log.entering("XMLFileOTListLoader", "write");
    try
    {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      document = builder.newDocument();

      Node root = document.createElement("opentools");
      document.appendChild(root);

      Element entrynode;
      Element n;
      Iterator iter = entries.values().iterator();
      while (iter.hasNext())
      {
        OpenToolEntry entry = (OpenToolEntry)iter.next();
        // node
        entrynode = document.createElement("entry");
        entrynode.setAttribute("id", entry.getID());
        entrynode.setAttribute("repo", entry.getRepository().getID());
        entrynode.setAttribute("repoid", entry.getIdInRepository());
        root.appendChild(entrynode);

        // is tracked
        n = document.createElement("istracked");
        n.appendChild(document.createTextNode(entry.isTracked() ? "true" : "false"));
        entrynode.appendChild(n);

        // title
        n = document.createElement("title");
        n.appendChild(document.createTextNode(entry.getTitle()));
        entrynode.appendChild(n);

        // versions
        // min version
        n = document.createElement("min_version");
        Version minvrs = entry.getRunMinVersion();
        if (minvrs != null)
        {
          n.appendChild(document.createTextNode(minvrs.toString()));
        }
          entrynode.appendChild(n);
        // max version
        n = document.createElement("max_version");
        Version maxvrs = entry.getRunMaxVersion();
        if (maxvrs != null)
        {
          n.appendChild(document.createTextNode(maxvrs.toString()));
        }
        entrynode.appendChild(n);

        // author
        Author author = entry.getAuthor();
        if (author != null)
        {
          Element authorElt = document.createElement("author");
          entrynode.appendChild(authorElt);
          n = document.createElement("name");
          n.appendChild(document.createTextNode(entry.getAuthorName()));
          authorElt.appendChild(n);
          String email = author.getEmail();
          if (email != null)
          {
            n = document.createElement("email");
            n.appendChild(document.createTextNode(email));
            authorElt.appendChild(n);
          }
        }


        // date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        Element dateElt = document.createElement("date");
        entrynode.appendChild(dateElt);
        n = document.createElement("updated");
        Date lastMod = entry.getLastModifiedDate();
        if (lastMod != null)
        {
          n.appendChild(document.createTextNode(sdf.format(lastMod)));
          dateElt.appendChild(n);
        }
        
        // downloads
        n = document.createElement("downloads");
        n.appendChild(document.createTextNode("" + entry.getNbDownloads()));
        entrynode.appendChild(n);

        // download
        if (entry.getProperty(OpenToolEntry.LOCAL_NAME) != null)
        {
          Element download = document.createElement("download");
          entrynode.appendChild(download);

          n = document.createElement("auto");
          n.appendChild(document.createTextNode(new Boolean(entry.isAutoDownload()).toString()));
          download.appendChild(n);
        }

        if (entry.getShortDescription() != null)
        {
          n = document.createElement("shortdesc");
          n.appendChild(document.createTextNode(entry.getShortDescription()));
          entrynode.appendChild(n);
        }

        if (entry.getDescription() != null)
        {
          n = document.createElement("description");
          n.appendChild(document.createTextNode(entry.getDescription()));
          entrynode.appendChild(n);
        }


        // properties
        Element properties = document.createElement("properties");
        entrynode.appendChild(properties);
        List keys = entry.getSavedKeys();
        Iterator keysIterator = keys.iterator();
        while (keysIterator.hasNext())
        {
          String key = (String) keysIterator.next();
          String prop = entry.getProperty(key);
          if (prop != null)
          {
            Element e = document.createElement("property");
            e.setAttribute("key", key);
            e.appendChild(document.createTextNode(prop));
            properties.appendChild(e);
          }
        }

        ArrayList files = entry.getFiles();
        if (files != null)
        {
          Element filesnode = document.createElement("files");
          Iterator fiter = files.iterator();
          while (fiter.hasNext())
          {
            FileEntry fentry = (FileEntry)fiter.next();
            n = document.createElement("file");
            n.setAttribute("name", fentry.name);
            n.setAttribute("date", fentry.date);
            n.setAttribute("size", fentry.size);
            if (fentry.extractPath != null)
            {
              n.setAttribute("extractpath", fentry.extractPath);
            }
            if (fentry.autoExtract)
            {
              n.setAttribute("autoextract", "true");
            }
            filesnode.appendChild(n);
          }
          entrynode.appendChild(filesnode);
        }

        Element commentsnode = document.createElement("comments");
        commentsnode.setAttribute("nb", "" + entry.getNbComments());
        if (entry.getComments() != null)
        {
          Iterator citer = entry.getComments().iterator();
          while (citer.hasNext())
          {
            writeComments(commentsnode, (CommentEntry)citer.next());
          }
        }
        entrynode.appendChild(commentsnode);

      }

      try
      {
        document.normalize();
      }
      catch (AbstractMethodError ex)
      {
        // ignore, unsupported with this parser
        if (com.borland.primetime.PrimeTime.isVerbose())
        {
          log.warning("LH CC: no normalise for this parser.");
        }
      }

      File f = PropertyManager.getSettingsUrl(filename).getFileObject();
      if (!f.exists())
      {
        f.getParentFile().mkdirs();
        f.createNewFile();
      }
      DOM2Sax.writeDOM2File(document, f);

    }
    catch (Exception ex)
    {
      log.logp(Level.CONFIG, "XMLFileOTListLoader", "write", "Problem when writing codecentral.xml", ex);
//      ex.printStackTrace();
    }
    log.exiting("XMLFileOTListLoader", "write");
  }



  private void writeComments(Element node, CommentEntry comment)
  {
    Element commentsnode = document.createElement("comment");
    commentsnode.setAttribute("id", "" + comment.id);
    commentsnode.setAttribute("date", "" + comment.date);
    commentsnode.setAttribute("title", "" + comment.title);
    commentsnode.setAttribute("author", "" + comment.author);
    commentsnode.appendChild(document.createTextNode(comment.comment == null ? "" : comment.comment));
    if (comment.subcomments != null)
    {
      Iterator citer = comment.subcomments.iterator();
      while (citer.hasNext())
      {
        writeComments(commentsnode, (CommentEntry)citer.next());
      }
    }
    node.appendChild(commentsnode);
  }

}
