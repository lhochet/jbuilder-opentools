/*
   Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.old;

import java.text.*;
import java.util.*;
import java.util.logging.*;

import java.awt.*;
import javax.swing.*;

import com.borland.primetime.actions.*;
import com.borland.primetime.ide.*;
import net.java.dev.jbotm.repo.ccold.CodeCentralOTDownloader;
import net.java.dev.jbotm.repo.ccold.CodeCentralOTEntryPageLoader;
import net.java.dev.jbotm.repo.ccold.CodeCentralOTListLoader;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.repo.cc.CodeCentralPropertyGroup;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.uiold.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.ccold.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.7 $ $Date: 2005/03/05 21:58:40 $
 * @created 23/12/03
 * @deprecated
 */
public class CodeCentralChecker implements Runnable, Comparator
{
  private static Logger log = Logger.getLogger(CodeCentralChecker.class.getName());

  private boolean running = false;

  // CC Profile
  private CodeCentralProfile profile = null;

  //UI
  private Browser browser = null;

  private ActionGroup sortActions = null;
  private ActionGroup sortByActions = null;
  private ActionGroup sortDirectionActions = null;
  private ActionGroup filterActions = null;

  // Messages
  private MessageCategory CODECENTRAL_MSG_CAT = null;

  private Message NEW_OT_PARENT = null;
  private Message UPDATED_OT_PARENT = null;
  private Message SEARCH_RESULT = null;
  private Message TRACKED_OT_PARENT = null;
  private Message OT_NEW_COMMENTS = null;

  private OpenToolsListMessage ALL_OTS = null;


  private Vector codecentralEntries = null;
  private Map localEntries = null;
  private Vector newEntries = null;
  private Vector updatedEntries = null;
  private Vector trackedEntries = null;
  private Vector allEntries = null;
  private Vector otsWithNewComments = null;

  public CodeCentralChecker(CodeCentralProfile profile, Browser browser)
  {
    this.browser = browser;
    this.profile = profile;
  }

  public CodeCentralProfile getProfile()
  {
    return profile;
  }

  private void update()
  {
    log.entering("CodeCentralChecker", "update()");
    loadLocals();
    downloadCodeCentralInfos();
    reconcile();
//    updateTracked();
    if (CodeCentralPropertyGroup.AUTO_LOAD_ENTRIES.getBoolean())
    {
      autoLoadEntries();
    }

    writeLocals();
    log.exiting("CodeCentralChecker", "update()");
  }

  private void downloadCodeCentralInfos()
  {
    try
    {
      long start = System.currentTimeMillis();
      browser.getStatusView().setText("Loading Code Central page...");
      log.info("Loading CodeCentral page...");

      CodeCentralOTListLoader codecentralLoader = new CodeCentralOTListLoader(this);
      log.info("Loaded CodeCentral page in " + (System.currentTimeMillis() - start) + "ms.");
      browser.getStatusView().setText(null);
      codecentralEntries = codecentralLoader.getEntries();
    }
    catch (Exception ex)
    {
      browser.getStatusView().setText("Error loading Code Central page... (" +
                                      ex.getClass().toString() + " : " +
                                      ex.getLocalizedMessage() + ")",
                                      java.awt.Color.red);
    }
  }

  private void loadLocals()
  {
    XMLOTListFileManager fileman = profile.getFileManager();
    fileman.load();
    localEntries = fileman.getEntries();
  }

  private void writeLocals()
  {
    XMLOTListFileManager fileman = profile.getFileManager();
    fileman.setEntries(localEntries);
    fileman.write();
  }

  private void reconcile()
  {
    log.entering("CodeCentralChecker", "reconcile()");
    newEntries = new Vector();
    updatedEntries = new Vector();
    trackedEntries = new Vector();
    otsWithNewComments = new Vector();

    if (codecentralEntries == null)
    {
      if (localEntries != null)
      {
        allEntries = new Vector(localEntries.values());
        Iterator iter = allEntries.iterator();
        while (iter.hasNext())
        {
          OpenToolEntry item = (OpenToolEntry)iter.next();
          if (item.isTracked())
          {
            item.setPrevNbDownloads(item.getNbDownloads());
            trackedEntries.add(item);
          }
        }
      }
      return;
    }

    Iterator iter = codecentralEntries.iterator();
    while (iter.hasNext())
    {
      OpenToolEntry item = (OpenToolEntry)iter.next();

      OpenToolEntry entry = null;
      try
      {
        entry = (OpenToolEntry)localEntries.get(item.getID());
      }
      catch (Exception ex)
      {
        log.log(Level.FINE, "Could not get local entry for id = " + item.getID(), ex);
      }

      if (entry == null)
      {
        item.setNew(true);
        localEntries.put(item.getID(), item);
        newEntries.add(item);
      }
      else
      {
        try
        {
          entry.setPrevNbDownloads(entry.getNbDownloads());
          entry.setTitle(item.getTitle()); // always update this field
          entry.setNbDownloads(item.getNbDownloads());  // always update this field
          entry.setShortDescription(item.getShortDescription()); // always update this field
          entry.setRunMinVersion(item.getRunMinVersion()); // always update this field
          entry.setRunMaxVersion(item.getRunMaxVersion()); // always update this field
          entry.setNbNewComments(item.getNbComments() - entry.getNbComments());
          entry.setNbComments(item.getNbComments());

            if (entry.getNbNewComments() > 0)
          {
            otsWithNewComments.add(entry);
          }

          if (entry.isTracked())
          {
            trackedEntries.add(entry);
          }

          if (item.getLastModifiedDate().after(entry.getLastModifiedDate()))
          {
            entry.setLastModifiedDate(item.getLastModifiedDate());
            entry.setUpdated(true);
            updatedEntries.add(entry);
          }
        }
        catch (Exception ex)
        {
          log.log(Level.FINE, "Could not reconcile entry id=" + item.getID(), ex);
        }
      }
    }

    // create all entries list
    allEntries = new Vector(localEntries.values());

    log.exiting("CodeCentralChecker", "reconcile()");
  }

  void autoLoadEntries()
  {
    DownloadManager downloadManager = null;
    if (newEntries.size() > 0)
    {
      downloadManager = DownloadManager.getDownloadManager();
      Collections.sort(newEntries, this);
      Iterator iter2 = newEntries.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        try
        {
          downloadManager.add(this, new CodeCentralOTEntryPageLoader(item));
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
    }

    if (updatedEntries.size() > 0)
    {
      if (downloadManager == null)
      {
        downloadManager = DownloadManager.getDownloadManager();

      }
      Collections.sort(updatedEntries, this);
      Iterator iter2 = updatedEntries.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        try
        {
          downloadManager.add(this, new CodeCentralOTEntryPageLoader(item));
          if (item.isAutoDownload())
          {
            downloadManager.add(this, new CodeCentralOTDownloader(item));
          }
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
    }

    if (otsWithNewComments.size() > 0)
    {
      if (downloadManager == null)
      {
        downloadManager = DownloadManager.getDownloadManager();

      }
      Collections.sort(otsWithNewComments, this);
      Iterator iter2 = otsWithNewComments.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        try
        {
          downloadManager.add(this, new CodeCentralOTEntryPageLoader(item));
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
    }
  }

  private void updateTracked()
  {
    DownloadManager downloadManager = null;
    if (trackedEntries.size() > 0)
    {
      downloadManager = DownloadManager.getDownloadManager();
      Collections.sort(trackedEntries, this);
      Iterator iter2 = trackedEntries.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        try
        {
          downloadManager.add(this, new CodeCentralOTEntryPageLoader(item));
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
    }
  }

  public int compare(Object o1, Object o2)
  {
    if (!(o1 instanceof OpenToolEntry))
    {
      throw new ClassCastException();
    }
    if (!(o2 instanceof OpenToolEntry))
    {
      throw new ClassCastException();
    }
    OpenToolEntry e1 = (OpenToolEntry)o1;
    OpenToolEntry e2 = (OpenToolEntry)o2;

    if (OTMPropertyGroup.SORT_BY.getValue().equals(
      OTMPropertyGroup.DATE))
    {
      Date d1 = e1.getLastModifiedDate();
      Date d2 = e2.getLastModifiedDate();
      return d1.compareTo(d2) * (OTMPropertyGroup.SORT_DIRECTION.getBoolean() ? 1 : -1);
    }
    else if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.DOWNLOAD))
    {
      try
      {
        int i1 = e1.getNbDownloads();
        int i2 = e2.getNbDownloads();
        int ret = 0;
        if (i1 > i2)
        {
          ret = 1;
        }
        else
        {
          if (i1 < i2)
          {
            ret = -1;
          }
        }
        return ret * (OTMPropertyGroup.SORT_DIRECTION.getBoolean() ? 1 : -1);
      }
      catch (NumberFormatException ex)
      {
        log.log(Level.FINE, "Problem when parsing numbers when comparing entries", ex);
//        ex.printStackTrace();
      }
    }
    else if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.ID))
    {
      try
      {
        int i1 = Integer.parseInt(e1.getIdInRepository());
        int i2 = Integer.parseInt(e2.getIdInRepository());
        int ret = 0;
        if (i1 > i2)
        {
          ret = 1;
        }
        else
        {
          if (i1 < i2)
          {
            ret = -1;
          }
        }
        return ret * (OTMPropertyGroup.SORT_DIRECTION.getBoolean() ? 1 : -1);
      }
      catch (NumberFormatException ex)
      {
        log.log(Level.FINE, "Problem when parsing dates when comparing entries", ex);
//        ex.printStackTrace();
      }
    }
    else if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.AUTHOR))
    {
      if (e1.getAuthor() == null)
      {
        return OTMPropertyGroup.SORT_DIRECTION.getBoolean() ? 1 : -1;
      }
      else
      {
        return e1.getAuthor().compareTo(e2.getAuthor()) *
            (OTMPropertyGroup.SORT_DIRECTION.getBoolean() ? 1 : -1);
      }
    }
    else if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.TITLE))
    {
      return e1.getTitle().compareTo(e2.getTitle()) *
        (OTMPropertyGroup.SORT_DIRECTION.getBoolean() ? 1 : -1);
    }
    return 1;
  }

  public void showList()
  {
    MessageView mv = browser.getMessageView();

    if ((newEntries.size() == 0) && (updatedEntries.size() == 0))
    {
      switch (OTMPropertyGroup.IF_NEW_UPDATED_ONLY.getInteger())
      {
        case OTMPropertyGroup.ALWAYS_SHOW:
          OTMPropertyGroup.AUTO_CHECKING = false;
          break;
        case OTMPropertyGroup.ONLY_IF_MESSAGEVIEW_SHOWN:
        {
          OTMPropertyGroup.AUTO_CHECKING = false;
          if (!mv.isVisible())
          {
            Browser.getActiveBrowser().getStatusView().setText("No new or updated OpenTools found.");
            return;
          }
        }
        case OTMPropertyGroup.NOT_IF_AUTOCHECKING:
        {
          if (OTMPropertyGroup.AUTO_CHECKING)
          {
            Browser.getActiveBrowser().getStatusView().setText(
              "No new or updated OpenTools found.");
            OTMPropertyGroup.AUTO_CHECKING = false;
            return;
          }
        }
        break;
        case OTMPropertyGroup.NEVER_SHOW:
        {
          Browser.getActiveBrowser().getStatusView().setText(
            "No new or updated OpenTools found.");
          OTMPropertyGroup.AUTO_CHECKING = false;
          return;
        }
      }
    }

    try
    {
      mv.removeTab(CODECENTRAL_MSG_CAT);
    }
    catch (com.borland.primetime.util.VetoException vex)
    {
      mv.clearMessages(CODECENTRAL_MSG_CAT);
    }

    initMessageViewCats();

    // add entries to msg list
    boolean added = false;
    if (newEntries.size() > 0)
    {
      mv.addMessage(CODECENTRAL_MSG_CAT, NEW_OT_PARENT);
      Collections.sort(newEntries, this);
      Iterator iter2 = newEntries.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        if (item.isShown())
        {
          mv.addMessage(CODECENTRAL_MSG_CAT, NEW_OT_PARENT, new OpenToolMessage(this, item));
          added = true;
        }
      }
      if (!added)
      {
        mv.addMessage(CODECENTRAL_MSG_CAT, NEW_OT_PARENT, new Message("No new OpenTools."));
      }
    }
    else
    {
      mv.addMessage(CODECENTRAL_MSG_CAT, "No new OpenTools.");
    }

    added = false;
    if (updatedEntries.size() > 0)
    {
      mv.addMessage(CODECENTRAL_MSG_CAT, UPDATED_OT_PARENT);
      Collections.sort(updatedEntries, this);
      Iterator iter2 = updatedEntries.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        if (item.isShown())
        {
          mv.addMessage(CODECENTRAL_MSG_CAT, UPDATED_OT_PARENT, new OpenToolMessage(this, item));
          added = true;
        }
      }
      if (!added)
      {
        mv.addMessage(CODECENTRAL_MSG_CAT, UPDATED_OT_PARENT, new Message("No updated OpenTools."));
      }
    }
    else
    {
      mv.addMessage(CODECENTRAL_MSG_CAT, "No updated OpenTools.");
    }

    if (otsWithNewComments.size() > 0)
    {
      mv.addMessage(CODECENTRAL_MSG_CAT, OT_NEW_COMMENTS);
      Collections.sort(otsWithNewComments, this);
      Iterator iter2 = otsWithNewComments.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        if (item.isShown())
        {
          mv.addMessage(CODECENTRAL_MSG_CAT, OT_NEW_COMMENTS, new OpenToolMessage(this, item));
        }
      }
    }

    added = false;
    if (trackedEntries.size() > 0)
    {
      mv.addMessage(CODECENTRAL_MSG_CAT, TRACKED_OT_PARENT);
      Collections.sort(trackedEntries, this);
      Iterator iter2 = trackedEntries.iterator();
      while (iter2.hasNext())
      {
        OpenToolEntry item = (OpenToolEntry)iter2.next();
        if (item.isShown())
        {
          mv.addMessage(CODECENTRAL_MSG_CAT, TRACKED_OT_PARENT, new OpenToolMessage(this, item));
          added = true;
        }
      }
      if (!added)
      {
        mv.addMessage(CODECENTRAL_MSG_CAT, TRACKED_OT_PARENT, new Message("No tracked OpenTools."));
      }
    }
    else
    {
      mv.addMessage(CODECENTRAL_MSG_CAT, "No tracked OpenTools.");
    }

    Collections.sort(allEntries, this);
    ALL_OTS.setEntries(allEntries);
    if ((newEntries.size() == 0) && (updatedEntries.size() == 0))
    {
      ALL_OTS.setText(OpenToolsListMessage.NON_EXPANDED_TEXT);
    }
    mv.addMessage(CODECENTRAL_MSG_CAT, ALL_OTS);
  }

  private void initMessageViewCats()
  {
    if (CODECENTRAL_MSG_CAT != null)
    {
      return; // already inited
    }

    CODECENTRAL_MSG_CAT = new MessageCategory(
      "CC " + profile.getProductName(), "CodeCentral " + profile.getProductName() + " OpenTools List",
      BrowserIcons.ICON_HELPBORLANDONLINE);

    NEW_OT_PARENT = new Message("New OpenTools...");
    UPDATED_OT_PARENT = new Message("Updated OpenTools...");
    SEARCH_RESULT = new Message("No OpenTools found for the current search.");
    TRACKED_OT_PARENT = new Message("Tracked OpentTools...");
    OT_NEW_COMMENTS = new Message("OpenTools with new comments");

    ALL_OTS = new OpenToolsListMessage(this, CODECENTRAL_MSG_CAT);

    // bind action
    NEW_OT_PARENT.setContextAction(ALL_OTS.getContextAction());
    UPDATED_OT_PARENT.setContextAction(ALL_OTS.getContextAction());
    SEARCH_RESULT.setContextAction(ALL_OTS.getContextAction());
    TRACKED_OT_PARENT.setContextAction(ALL_OTS.getContextAction());
    OT_NEW_COMMENTS.setContextAction(ALL_OTS.getContextAction());
  }

  public ActionGroup getSortActions()
  {
    if (sortActions == null)
    {
      initSortActions();
    }
    return sortActions;
  }

  public ActionGroup getFilterActions()
  {
    if (filterActions == null)
    {
      initFilterActions();
    }
    return filterActions;
  }

  private void initSortActions()
  {
//    sortActions = new ActionGroup("Sort...");
//    sortByActions = new ActionGroup("by...");
//    sortDirectionActions = new ActionGroup("in...");
//    StateAction asc = new OpenToolMessage.SortAscAction(this);
//    StateAction desc = new OpenToolMessage.SortDescAction(this);
//    if (OTMPropertyGroup.SORT_DIRECTION.getBoolean())
//    {
//      OpenToolMessage.prevDirectionAction = asc;
//      asc.setState(null, true);
//    }
//    else
//    {
//      OpenToolMessage.prevDirectionAction = desc;
//      desc.setState(null, true);
//    }
//
//    sortDirectionActions.add(asc);
//    sortDirectionActions.add(desc);
//
//    StateAction id = new OpenToolMessage.SortIDAction(this);
//    if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.ID))
//    {
//      OpenToolMessage.prevByAction = id;
//      id.setState(null, true);
//    }
//    StateAction author = new OpenToolMessage.SortAuthorAction(this);
//    if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.AUTHOR))
//    {
//      OpenToolMessage.prevByAction = author;
//      author.setState(null, true);
//    }
//    StateAction title = new OpenToolMessage.SortTitleAction(this);
//    if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.TITLE))
//    {
//      OpenToolMessage.prevByAction = title;
//      title.setState(null, true);
//    }
//    StateAction date = new OpenToolMessage.SortDateAction(this);
//    if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.DATE))
//    {
//      OpenToolMessage.prevByAction = date;
//      date.setState(null, true);
//    }
//    StateAction download = new OpenToolMessage.SortDownloadAction(this);
//    if (OTMPropertyGroup.SORT_BY.getValue().equals(OTMPropertyGroup.DOWNLOAD))
//    {
//      OpenToolMessage.prevByAction = download;
//      download.setState(null, true);
//    }
//
//    sortByActions.add(id);
//    sortByActions.add(author);
//    sortByActions.add(title);
//    sortByActions.add(date);
//    sortByActions.add(download);
//
//    sortActions.add(sortByActions);
//    sortActions.add(sortDirectionActions);
//    sortActions.setPopup(true);
  }

  private void initFilterActions()
  {
    filterActions = new ActionGroup("Search...");

    filterActions.add(new BrowserAction("Id...")
    {
      public void actionPerformed(Browser b)
      {
        searchOn(b, OTMPropertyGroup.ID_FILTER);
      }
    }
    );

    filterActions.add(new BrowserAction("Author...")
    {
      public void actionPerformed(Browser b)
      {
        searchOn(b, OTMPropertyGroup.AUTHOR_FILTER);
      }
    }
    );

    filterActions.add(new BrowserAction("Keyword...")
    {
      public void actionPerformed(Browser b)
      {
        searchOn(b, OTMPropertyGroup.KEYWORD_FILTER);
      }
    }
    );

  }

  public void run()
  {
    synchronized (this)
    {
      if (running)
      {
        return;
      }
      running = true;
    }
    update();
    showList();
    running = false;
  }

  public void saveEntries()
  {
    writeLocals();
  }

  public void searchOn(Browser browser, int filter)
  {
    searchOn(browser, filter, null);
  }

  public void searchOn(Browser browser, int filter, Object arg)
  {
    MessageView mv = browser.getMessageView();
    Vector searchRes = new Vector();
    switch (filter)
    {
      case OTMPropertyGroup.ID_FILTER:
      {
        String id = JOptionPane.showInputDialog((Component)browser,
                                                (Object)new String("ID : "),
                                                "Search OpenTool for",
                                                JOptionPane.QUESTION_MESSAGE);
        if ((id != null) && (!id.equals("")))
        {
          Iterator iter2 = allEntries.iterator();
          while (iter2.hasNext())
          {
            OpenToolEntry item = (OpenToolEntry)iter2.next();
            if (item.getIdInRepository().equals(id))
            {
              searchRes.add(item);
            }
          }
        }
      }
      break;
      case OTMPropertyGroup.AUTHOR_FILTER:
      {
        String author = JOptionPane.showInputDialog((Component)browser,
          (Object)new String("Author : "), "Search OpenTools for",
          JOptionPane.QUESTION_MESSAGE);
        if (author == null)
        {
          return;
        }
        if (author.equals(""))
        {
          return;
        }

        author = author.toUpperCase();
        Iterator iter2 = allEntries.iterator();
        while (iter2.hasNext())
        {
          OpenToolEntry item = (OpenToolEntry)iter2.next();
          if (item.getAuthorName().toUpperCase().indexOf(author) >= 0)
          {
            searchRes.add(item);
          }
        }
      }
      break;
      case OTMPropertyGroup.THIS_AUTHOR_FILTER:
      {
        String author = (String)arg;
        if (author == null)
        {
          return;
        }
        if (author.equals(""))
        {
          return;
        }

        author = author.toUpperCase();
        Iterator iter2 = allEntries.iterator();
        while (iter2.hasNext())
        {
          OpenToolEntry item = (OpenToolEntry)iter2.next();
          if (item.getAuthorName().toUpperCase().indexOf(author) >= 0)
          {
            searchRes.add(item);
          }
        }
      }
      break;
      case OTMPropertyGroup.KEYWORD_FILTER:
      {
        String keyword = JOptionPane.showInputDialog((Component)browser,
          (Object)new String("Keyword : "), "Search OpenTools for",
          JOptionPane.QUESTION_MESSAGE);
        if (keyword == null)
        {
          return;
        }
        if (keyword.equals(""))
        {
          return;
        }
        keyword = keyword.toUpperCase();

        Iterator iter2 = allEntries.iterator();
        while (iter2.hasNext())
        {
          OpenToolEntry item = (OpenToolEntry)iter2.next();
          if ((item.getTitle().toUpperCase().indexOf(keyword) >= 0) ||
              ((item.getDescription() != null) &&
               (item.getDescription().toUpperCase().indexOf(keyword) >= 0)) ||
              ((item.getShortDescription() != null) &&
               (item.getShortDescription().toUpperCase().indexOf(keyword) >= 0)))
          {
            searchRes.add(item);
          }
        }
      }
      break;
      default:
        return;
    }

    showList();

    int nbres = searchRes.size();
    if (nbres == 0)
    {
      SEARCH_RESULT.setText("No OpenTool was found");
    }
    if (nbres == 1)
    {
      SEARCH_RESULT.setText(nbres + " OpenTool was found");
    }
    if (nbres > 1)
    {
      SEARCH_RESULT.setText(nbres + " OpenTools were found");
    }
    mv.addMessage(CODECENTRAL_MSG_CAT, SEARCH_RESULT);

    Iterator iter = searchRes.iterator();
    while (iter.hasNext())
    {
      OpenToolEntry item = (OpenToolEntry)iter.next();
      mv.addMessage(CODECENTRAL_MSG_CAT, SEARCH_RESULT,
                    new OpenToolMessage(this, item));
    }
  }

}
