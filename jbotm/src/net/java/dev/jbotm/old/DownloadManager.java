/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.old;

import java.util.*;
import java.util.logging.*;

import net.java.dev.jbotm.repo.http.InvalidCookieException;

import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version 0.1, 24/09/01
 */
public class DownloadManager implements Runnable
{
  Logger log = Logger.getLogger(DownloadManager.class.getName());

  // singleton
  private static DownloadManager manager = null;

  public static DownloadManager getDownloadManager()
  {
    if (manager == null)
    {
      manager = new DownloadManager();
    }
    return manager;
  }

  // class
  private Vector queue;
  private CCCDPair currentDownload = null;
  private boolean running = false;
  private Thread thread = null;

  private Hashtable map = new Hashtable();

  private class CCCCounter
  {
    private int nb = 0;
    synchronized void inc()
    {nb++;
    }

    synchronized void dec()
    {nb--;
    }

    synchronized int getNb()
    {return nb;
    }
  }

  private class CCCDPair
  {
    Downloadable d = null;
    CodeCentralChecker checker = null;
    CCCDPair(CodeCentralChecker checker, Downloadable d)
    {
      this.checker = checker;
      this.d = d;
    }

  }

  private DownloadManager()
  {
    queue = new Vector();
  }

  public void add(CodeCentralChecker checker, Downloadable d)
  {
    log.finest("Entering DownloadManager.add(" + checker + ", " + d);
    // counter
    CCCCounter counter = (CCCCounter)map.get(checker);
    if (counter == null)
    {
      map.put(checker, new CCCCounter());
    }
    else
    {
      counter.inc();
    }
    // main queue
    queue.add(new CCCDPair(checker, d));

    // start thread if not running
    if (!running)
    {
      if (thread == null)
      {
        thread = new Thread(this);
        thread.start();
      }
    }
    log.finest("Exiting DownloadManager.add()");
  }

  public void run()
  {
    log.finest("Entering DownloadManager.run()");
    synchronized (this)
    {
      if (running)
      {
        return;
      }
      running = true;
    }

    while (queue.size() > 0)
    {
      synchronized (queue)
      {
        currentDownload = (CCCDPair)queue.firstElement();
        queue.remove(currentDownload);
      }
      try
      {
        CCCCounter counter = (CCCCounter)map.get(currentDownload.checker);
        if (counter != null)
        {
          counter.dec();
          currentDownload.d.load();
          if (counter.getNb() < 1)
          {
            map.remove(currentDownload.checker);
            log.finest("saving entries for checker : " + currentDownload.checker);
            currentDownload.checker.saveEntries();
          }
        }
      }
      catch (InvalidCookieException ex)
      {
        break;
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }

    thread = null;
    running = false;
  }

  public void stop()
  {
    Thread t = thread;
    thread = null;
    running = false;
    synchronized (queue)
    {
      queue.removeAllElements();
    }
  }

  public boolean isDownloading()
  {
    return queue.size() > 0;
  }

}
