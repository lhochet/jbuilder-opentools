/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo;

import java.util.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005/01/23 22:05:33 $
 * @created 27/11/04
 */
public abstract class Repository
{
  private boolean checked = true;

  public abstract String getName();
  public abstract String getID();
  public abstract String getProduct();
  public abstract void check() throws Exception;
  public abstract List getEntries();
  public abstract void download(OpenToolEntry entry) throws OTMException;
  public abstract void updatePropertiesFor(OpenToolEntry entry) throws OTMException;
  public abstract OpenToolEntry createEntry();

  public void setChecked(boolean checked)
  {
    this.checked = checked;
  }
  public boolean isChecked()
  {
    return checked;
  }
}
