/*
   Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.cc;

import java.io.*;
import java.util.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.repo.http.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.structs.OpenToolEntry;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.5 $ $Date: 2005/01/14 14:48:51 $
 * @created 0.1, 18/02/02
 */
public class CodeCentralOTCommentsListLoader extends HTMLDownloader
{
  private final String CC_ENTRY_COMMENT_PG_BASE =
    "http://threads.borland.com/threads/threads.exe/thread?sysid=2&contentid=";

  private OpenToolEntry entry = null;

  public CodeCentralOTCommentsListLoader(OpenToolEntry entry)
  {
    this.entry = entry;
  }

  /**
   * getDownloadErrorText
   *
   * @return String
   */
  protected String getDownloadErrorText()
  {
    return "Error during loading of comments list for OpenTool '" + entry.getIdInRepository() + "'. Loading aborted.";
  }

  /**
   * getDownloadURL
   *
   * @return String
   */
  protected String getDownloadURL()
  {
    return CC_ENTRY_COMMENT_PG_BASE + entry.getIdInRepository();
  }

  /**
   * getNocookieText
   *
   * @return String
   */
  protected String getNocookieText()
  {
    return "Error downloading comment list for OT " + entry.getIdInRepository() + ": could not get cookie";
  }

  /**
   * preLoadChecks
   *
   * @return boolean
   */
  protected boolean preLoadChecks()
  {
    log.info("Downloading comments list for OT " + entry.getIdInRepository() + "...");
    return true;
  }

  /**
   * handleHTMLContent
   *
   * @param reader BufferedReader
   */
  protected void handleHTMLContent(BufferedReader reader)
  {
    try
    {
      // reinit comments
      entry.setNbComments(0);
      entry.setComments(null);

      String line;

      // skip start of page
      while (((line = reader.readLine()) != null) && (line.indexOf("title=\"New Comment\">New Comment</a>") < 0))
      {
        ; // System.out.println("@ " + line);
      }

      // skip
      line = reader.readLine(); //<hr>
      line = reader.readLine(); // <!-- end thrdsubj.htm -->

      CommentEntry curCEntry = null;
      CommentEntry prevCEntry = null;
      ArrayList curSubVect = null;
      Stack subvectstack = new Stack();
      
//      <ul><!-- start commsubj.htm -->
//      <b><a name="#29720"></a>
//      <a href="/threads/threads.exe/view?commentid=29720&view=short"
//      title="View Comment"><b>Proxy support?</b></a>
//      (<a href="/threads/threads.exe/userall?commentid=29720&view=short"
//      title="All comments in all threads by Jarek Sacha">
//
//      Jarek Sacha</a>, 24-Aug-01 09:49)
//      <a href="/threads/threads.exe/add?refid=10578&view=short"
//      title="New Comment">New</a>
//      <a href="/threads/threads.exe/reply?commentid=29720&view=short"
//      title="Reply to this Comment">Reply</a>
//      </b><br>
//      <!-- end commsubj.htm -->


      while (((line = reader.readLine()) != null) && (line.indexOf("<hr>") < 0))
      {
        //<ul><b><a name="#29720"></a>
//        line = reader.readLine(); //new level + comment id
//        System.out.println(" line = " + line);
        prevCEntry = curCEntry;
        curCEntry = new CommentEntry();
        if (line.startsWith("<ul>"))
        {
//          System.out.println(">> new comment level");
//          System.out.println("stack sz av pus = "+subvectstack.size());
          if (curSubVect != null)
          {
            subvectstack.push(curSubVect);
          }
          curSubVect = new ArrayList();
//          System.out.println("stack sz ap pus = "+subvectstack.size());
          if (entry.getComments() == null)
          {
            entry.setComments(curSubVect);
          }
          else
          {
            if (prevCEntry != null)
            {
              prevCEntry.subcomments = curSubVect;
            }
          }
        }
        else if (line.startsWith("</ul>"))
        {
          String tmp = line;
          while (tmp.startsWith("</ul>"))
          {
//            System.out.println(">> closing level");
//            System.out.println("tmp = " + tmp);
//            System.out.println("stack sz = "+subvectstack.size());
            if (subvectstack.size() > 0)
            {
              curSubVect = (ArrayList)subvectstack.pop();
            }
//            else if (subvectstack.size() == 1)
//            {
//              curSubVect = (Vector)subvectstack.peek();
//            }
//            System.out.println("stack sz ap po = "+subvectstack.size());
            if (tmp.indexOf("</ul>") >= 0)
            {
              tmp = tmp.substring(5);
//            System.out.println("tmp2 = " + tmp);
            }
          }
        }
        
         // <b><a name="#29720"></a>
        line = reader.readLine();
        curSubVect.add(curCEntry);
        entry.setNbComments(entry.getNbComments() + 1);
        int pos = line.indexOf("#");
        pos++;
        if (pos > 0)
        {
//          System.out.println("id = *" + line.substring(pos, line.indexOf("\"", pos)) + "*");
          curCEntry.id = line.substring(pos, line.indexOf("\"", pos));
        }

        //<a href="/threads/threads.exe/view?commentid=29720&view=short"
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //title="View Comment"><b>Proxy support?</b></a>
        line = reader.readLine(); //tr
//        System.out.println("line = " + line);
//        System.out.println("title = *" + stripEnd(stripStart(stripStart(line))) + "*");
        pos = line.indexOf("<b>");
        int len = (new String("<b>")).length();
        if (pos > 0)
        {
//          System.out.println("title = *" + line.substring(pos + len, line.indexOf("</b>")) + "*");
          curCEntry.title = line.substring(pos + len, line.indexOf("</b>"));
        }

        //(<a href="/threads/threads.exe/userall?commentid=29720&view=short"
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //title="All comments in all threads by Jarek Sacha">
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //Jarek Sacha</a>, 24-Aug-01 09:49)
        line = reader.readLine(); //tr
//        System.out.println("line = " + line);
        pos = line.indexOf("</a>, ");
        len = (new String("</a>, ")).length();
        if (pos > 0)
        {
//          System.out.println("author = *" + line.substring(0, pos) + "*");
          curCEntry.author = line.substring(0, pos);
          String date = line.substring(pos + len);
          date = date.substring(0, date.length() - 1);
//          System.out.println("date = *" + date + "*");
          curCEntry.date = date;
        }

        //<a href="/threads/threads.exe/add?refid=10578&view=short"
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //title="New Comment">New</a>
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //<a href="/threads/threads.exe/reply?commentid=29720&view=short"
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //title="Reply to this Comment">Reply</a>
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //</b><br>
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

        //      <!-- end commsubj.htm -->
        line = reader.readLine(); //ignore
//        System.out.println("line = " + line);

//        System.out.println("cur cmt v sz = " + curSubVect.size());
      } // while

      log.info("Downloaded comments list for OT " + entry.getIdInRepository() + ".");

    }
    catch (IOException ex)
    {
      String loadingaborted = "Error during loading of comments list for OpenTool '" + entry.getIdInRepository() + "'. Loading aborted.";
      log.info(loadingaborted);
      Browser.getActiveBrowser().getStatusView().setText(loadingaborted, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTCommentsListLoader", "handleHTMLContent()", ex);
    }
  }

  protected boolean needCookie()
  {
    return false;
  }
}
