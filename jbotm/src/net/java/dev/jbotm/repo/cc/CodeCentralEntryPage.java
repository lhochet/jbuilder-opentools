/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.cc;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.ui.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005/03/05 21:58:17 $
 * @created 23/01/05
 */
public class CodeCentralEntryPage extends EntryPropertyPage
{
  private OpenToolEntry entry;


  JPanel container = new JPanel();
  JLabel jLabel1 = new JLabel();
  JLabel lID = new JLabel();
  Border border1;
  TitledBorder titledBorder1;
  JLabel jLabel11 = new JLabel();
  JLabel lCopyright = new JLabel();
  Border border2;
  TitledBorder titledBorder2;
  JLabel jLabel17 = new JLabel();
  JCheckBox cbOnCompanionCD = new JCheckBox();
  JLabel lFileSize = new JLabel();
  Border border3;
  TitledBorder titledBorder3;
  JLabel jLabel13 = new JLabel();
  JLabel lOriginalDate = new JLabel();
  Border border4;
  TitledBorder titledBorder4;
  JLabel jLabel4 = new JLabel();
  JLabel lTerms = new JLabel();
  GridBagLayout gridBagLayout3 = new GridBagLayout();
  JLabel jLabel6 = new JLabel();
  JLabel lDownloads = new JLabel();
  private BorderLayout borderLayout1 = new BorderLayout();
  public CodeCentralEntryPage()
  {
    this(null);
  }

  public CodeCentralEntryPage(OpenToolEntry entry)
  {
    this.entry = entry;
    try
    {
      jbInit();
      postJBInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public String getTitle()
  {
    return "CodeCentral";
  }

  private void postJBInit() throws Exception
  {
    if (entry == null)
    {
      return;
    }

    String tmp = null;

    lID.setText(entry.getIdInRepository());
    Author author = entry.getAuthor();
    if (author != null)
    {
      String email = author.getEmail();
      if (email != null)
      {
      }
    }

    tmp = entry.getProperty(OpenToolEntry.OT_SITE_URL);
    if (tmp != null)
    {
    }
    tmp = entry.getProperty(OpenToolEntry.COPYRIGHT);
    if (tmp != null)
    {
      lCopyright.setText(tmp);
    }
    tmp = entry.getProperty(OpenToolEntry.TERMS);
    if (tmp != null)
    {
      lTerms.setText(tmp);
    }
    tmp = entry.getProperty(OpenToolEntry.ORIGINAL_UPLOAD);
    if (tmp != null)
    {
      lOriginalDate.setText(tmp);
    }
    tmp = entry.getProperty(OpenToolEntry.ONCD);
    if ((tmp != null) && (tmp.startsWith("Yes")))
    {
      cbOnCompanionCD.setSelected(true);

    }
    tmp = entry.getProperty(OpenToolEntry.SIZE);
    if (tmp != null)
    {
      lFileSize.setText(tmp);
    }
    lDownloads.setText("" + entry.getNbDownloads());

  }

  private void jbInit() throws Exception
  {
    border1 = BorderFactory.createLineBorder(SystemColor.controlText, 1);
    titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), "Author");
    border2 = BorderFactory.createLineBorder(Color.gray, 1);
    titledBorder2 = new TitledBorder(border2, "Date");
    border3 = BorderFactory.createLineBorder(SystemColor.controlText, 1);
    titledBorder3 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), "Author:");
    border4 = BorderFactory.createLineBorder(Color.lightGray, 1);
    titledBorder4 = new TitledBorder(BorderFactory.createLineBorder(Color.gray, 1), "Date:");
    jLabel1.setText("ID:");
    container.setLayout(gridBagLayout3);
    lID.setText("Unknown");
    jLabel11.setText("Copyright:");
    lCopyright.setText("Unknown");
    jLabel17.setText("Size:");
    cbOnCompanionCD.setEnabled(false);
    cbOnCompanionCD.setText("On companion CD");
    lFileSize.setText("Unknown");
    this.setMinimumSize(new Dimension(1, 1));
    this.setPreferredSize(new Dimension(300, 312));
    jLabel13.setText("First published:");
    lOriginalDate.setText("Unknown");
    jLabel4.setText("Terms:");
    lTerms.setText("Unknown");
    jLabel6.setText("Downloads:");
    lDownloads.setText("Unknown");
    this.setLayout(borderLayout1);
    container.add(lID, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0
                                              , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                                              new Insets(5, 5, 0, 0), 0, 0));
    container.add(lTerms, new GridBagConstraints(2, 2, 3, 1, 1.0, 0.0
                                                 , GridBagConstraints.WEST, GridBagConstraints.BOTH,
                                                 new Insets(5, 5, 0, 5), 0, 0));
    container.add(jLabel4, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST, GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 0), 0, 0));
    container.add(lCopyright, new GridBagConstraints(2, 1, 3, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
    container.add(jLabel11, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    container.add(jLabel17, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    container.add(cbOnCompanionCD, new GridBagConstraints(0, 4, 4, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    container.add(lFileSize, new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 0), 0, 0));
    container.add(jLabel6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 0), 0, 0));
    container.add(lDownloads, new GridBagConstraints(2, 6, 3, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
    container.add(jLabel13, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    container.add(lOriginalDate, new GridBagConstraints(2, 3, 4, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    this.add(container, java.awt.BorderLayout.NORTH);
    container.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                  new Insets(5, 5, 0, 0), 0, 0));
  }
}
