/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.cc;

import com.borland.primetime.properties.*;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.OTMPropertyGroup;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/03/05 21:57:13 $
 * @created 20/02/05
 */
public class CodeCentralPropertyGroup implements PropertyGroup
{
  public static final String CC_BASE = "http://codecentral.borland.com/";

  // load entry pages
  public static final GlobalBooleanProperty AUTO_LOAD_ENTRIES = new GlobalBooleanProperty(OTMPropertyGroup.OTM_CAT, "autoloadentries", false);

  // download
  /** @todo move this to a CC property group? */
  public static final GlobalProperty COOKIE = new GlobalProperty(OTMPropertyGroup.OTM_CAT, "cookie");
  public static final GlobalProperty DEST_PATH = new GlobalProperty(OTMPropertyGroup.OTM_CAT, "destpath", "");


  public CodeCentralPropertyGroup()
  {
  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == ProductPropertyPageFactory.PRODUCTS_TOPIC) //net.java.dev.jbuilder.opentools.community.CommunityPropertyGroup.COMMUNITY_TOPIC)
    {
      return new PropertyPageFactory("CodeCentral")
      {
        public PropertyPage createPropertyPage()
        {
          return new CodeCentralPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

  /**
   * not used.
   */
  public void initializeProperties()
  {
  }
}
