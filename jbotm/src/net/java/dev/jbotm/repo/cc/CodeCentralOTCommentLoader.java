/*
   Copyright: Copyright (c) 2002-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.cc;

import java.io.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.repo.http.*;
import net.java.dev.jbotm.structs.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2004/11/23 21:19:24 $
 * @created 0.1, 25/02/02
 */
public class CodeCentralOTCommentLoader extends HTMLDownloader
{

  private final String CC_ENTRY_COMMENT_PG_BASE = "http://threads.borland.com/threads/threads.exe/view?commentid=";
  private final String CC_ENTRY_COMMENT_PG_TAIL = "&view=short";

  private CommentEntry entry = null;

  public CodeCentralOTCommentLoader(CommentEntry entry)
  {
    this.entry = entry;
  }

  /**
   * getDownloadErrorText
   *
   * @return String
   */
  protected String getDownloadErrorText()
  {
    return "Error during loading of comment for OpenTool '" + entry.id + "'. Loading aborted.";
  }

  /**
   * getDownloadURL
   *
   * @return String
   */
  protected String getDownloadURL()
  {
    return CC_ENTRY_COMMENT_PG_BASE + entry.id + CC_ENTRY_COMMENT_PG_TAIL;
  }

  /**
   * getNocookieText
   *
   * @return String
   */
  protected String getNocookieText()
  {
    return "Error downloading comment " + entry.id + ": could not get cookie";
  }

  /**
   * preLoadChecks
   *
   * @return boolean
   */
  protected boolean preLoadChecks()
  {
    log.info("Downloading comment " + entry.id + "...");
    return true;
  }

  /**
   * handleHTMLContent
   *
   * @param reader BufferedReader
   */
  protected void handleHTMLContent(BufferedReader reader)
  {
    try
    {
      String line;

      // skip start of page
      while (((line = reader.readLine()) != null) && (!line.startsWith("title=\"All comments in all threads by")))
      {
        ; // System.out.println("@ " + line);
      }

      // skip
      line = reader.readLine(); //author
      line = reader.readLine(); //date

      String comment = "";

      while (((line = reader.readLine()) != null) && (line.indexOf("</td></tr>") < 0))
      {
        comment += line;
      }
      if ((line != null) && (line.indexOf("</td></tr>") > 0))
      {
        System.out.println("line = " + comment);
        comment += line.substring(0, line.indexOf("</td></tr>"));
      }
      System.out.println("comment = " + comment);
      
      comment = comment.replaceAll("<br />", "<br>");

      entry.comment = "<html><body>" + comment + "</body></html>";

      log.info("Downloaded comment " + entry.id + ".");

    }
    catch (IOException ex)
    {
      String loadingaborted = "Error during loading of comment for OpenTool '" + entry.id + "'. Loading aborted.";
      log.info(loadingaborted);
      Browser.getActiveBrowser().getStatusView().setText(loadingaborted, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTCommentsListLoader", "handleHTMLContent()", ex);
    }
  }

}
