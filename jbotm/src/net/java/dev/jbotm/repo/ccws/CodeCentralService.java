package net.java.dev.jbotm.repo.ccws;

import com.borland.webservices.*;
import java.rmi.*;
import javax.xml.rpc.*;
import lh.borland.ccservice.loginresult.NewDataSet.*;
import javax.xml.bind.*;
import java.io.*;
import lh.borland.ccservice.loginresult.*;
import org.xml.sax.*;
import java.util.zip.*;
import com.borland.primetime.properties.*;
import java.util.*;
import lh.borland.ccservice.ccsearch.*;
import lh.borland.ccservice.ccsearch.Ccsearch.*;

/**
 * <p>Title: CodeCentral</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2006</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: $ $Date: $
 * @created 17/06/06
 */
public class CodeCentralService
{
  public static final String TEST_USERNAME = "test@borland.com";
  public static final String TEST_PASSWORD = "test";
  public static final String USER_AGENT = "JBOTM/0.9.5";
  
//  private GlobalProperty LAST_CHECK = new GlobalProperty("net.java.dev.jbotm.repo.ccws.CodeCentralService", "last_check");
  
  public static class NotLoggedInException extends Exception
  {
    
  }
  
  public static class CodeCentralServiceException extends Exception
  {
    CodeCentralServiceException(String msg)
    {
      super(msg);
    }
    CodeCentralServiceException(String msg, Throwable ex)
    {
      super(msg, ex);
    }
    CodeCentralServiceException(Throwable ex)
    {
      super(ex);
    }
  }

  private ICodeCentralWSSoap service = null;
  private String sessionId = null;
  
  
  public CodeCentralService()
  {
  }
  
  public boolean isLoggedIn()
  {
    return !(service == null || sessionId == null);
  }
  
  public void login() throws CodeCentralServiceException
  {
    try
    {
      ICodeCentralWS locator = new ICodeCentralWSLocator();
      service = locator.getICodeCentralWSSoap();
  
      byte[] resp = service.loginZLib(TEST_USERNAME, TEST_PASSWORD, USER_AGENT);
  
      LoginResult login = getLoginResult(resp);
      sessionId = login.getSessionID();
    }
    catch (Exception ex)
    {
      throw new CodeCentralServiceException(ex);
    }
  }
  
  public List<Results> check()
  {
    List<Results> ret = null;
    byte[] bytes = getListBytes();
    if (bytes != null)
    {
      try
      {
        ret = getSearchResult(bytes);
      }
      catch (IOException ex)
      {
        // something went wrong, just return no results
      }
      catch (JAXBException ex)
      {
        // something went wrong, just return no results
      }
    }
    return ret;
  }
  
  
  public Results updateEntry(int entryId)
  {
    Results ret = null;
    try
    {
      byte[] bytes = service.getSnippetZLib(sessionId, entryId);
      if (bytes != null)
      {
        try
        {
          List<Results> lst = getSearchResult(bytes);
          if (lst.size() > 0)
          {
            ret = lst.get(0);
          }
        }
        catch (IOException ex)
        {
          // something went wrong, just return no results
        }
        catch (JAXBException ex)
        {
          // something went wrong, just return no results
        }
      }
    }
    catch (RemoteException ex1)
    {
      // something went wrong, just return no results
    }
    return ret;
  }
  
  public byte[] download(int entryId) throws net.java.dev.jbotm.repo.ccws.CodeCentralService.
      CodeCentralServiceException
  {
    try
    {
      return service.downloadAttachment(sessionId, entryId);
    }
    catch (RemoteException ex)
    {
      throw new CodeCentralServiceException(ex);
    }
  }
  
  // -- private helpers
  
  private byte[] getListBytes()
  {
    byte[] bytes = null;
    
    long lastCheck = 0;
//    String val = LAST_CHECK.getValue();
//    if (val != null)
//    {
//      lastCheck = Long.parseLong(val);
//    }


    try
    {
      if (lastCheck == 0)
      {
        Calendar cal = Calendar.getInstance();
        bytes = service.searchZLib(sessionId, null, "3", null, "11", 0.0, 0.0, cal, cal, false, null, 0);
      }
      else
      {
        Calendar calFrom = Calendar.getInstance();
        calFrom.setTimeInMillis(lastCheck);
        Calendar calTo = Calendar.getInstance();
        bytes = service.searchZLib(sessionId, null, "3", null, "11", 0.0, 0.0, calFrom, calTo, true, null, 0);
      }
    }
    catch (RemoteException ex)
    {
      // ignore: no new or updated OTs
      ex.printStackTrace();
    }
    return bytes;
  }
  
  private static List<Results> getSearchResult(byte[] bytes) throws JAXBException, IOException
  {
    JAXBContext jc = JAXBContext.newInstance("lh.borland.ccservice.ccsearch");

    // create an Unmarshaller
    Unmarshaller u = jc.createUnmarshaller();

    // unmarshal
    Ccsearch ccs = (Ccsearch) u.unmarshal(getInputSource(bytes));
    return ccs.getResults();
  }


  
  private static LoginResult getLoginResult(byte[] bytes) throws JAXBException, IOException
  {
    JAXBContext jc = JAXBContext.newInstance("lh.borland.ccservice.loginresult");

    // create an Unmarshaller
    Unmarshaller u = jc.createUnmarshaller();

    // unmarshal
    NewDataSet ds = (NewDataSet) u.unmarshal(getInputSource(bytes));
    return ds.getLoginResult().get(0);
  }


  private static InputSource getInputSource(byte[] bytes) throws IOException
  {
    InputStream in = new InflaterInputStream(
        new ByteArrayInputStream(bytes));
    BufferedReader din = new BufferedReader(new InputStreamReader(in));
    StringBuilder buf = new StringBuilder(bytes.length);
    // clean up the retrieved xml (it has 0s at the end)
    char c;
    while ((c = (char) din.read()) != 0)
    {
      buf.append(c);
    }

    String xml = buf.toString(); //din.readLine();
    System.out.println(xml);
    return new InputSource(new StringReader(xml));
  }


}
