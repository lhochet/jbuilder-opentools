package net.java.dev.jbotm.repo.ccws;

import java.util.*;

import com.borland.primetime.util.*;
import lh.borland.ccservice.ccsearch.Ccsearch.*;

import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.*;
import net.java.dev.jbotm.repo.ccws.CodeCentralService.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.repo.cc.CodeCentralOTCommentsListLoader;

/**
 * <p>Title: CodeCentral</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2006</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: $ $Date: $
 * @created 17/06/06
 */
public class CodeCentralJBuilderWS extends Repository
{
  private CodeCentralService service = new CodeCentralService();
  private List<Results> results = null;
  
  private CodeCentralJBuilderWS()
  {
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    RepositoryManager.registerRepository(new CodeCentralJBuilderWS());
  }

  public String getID()
  {
    return "net.java.dev.jbotm.repo.ccws.CodeCentralJBuilderWS";
  }

  public String getName()
  {
    return "CodeCentral (JBuilder)";
  }

  public String getProduct()
  {
    return "net.java.dev.jbotm.products.JBuilder";
  }

  public void check() throws Exception
  {
    if (!service.isLoggedIn())
    {
      service.login();
    }
    results = service.check();
  }

  public OpenToolEntry createEntry()
  {
    return new CodeCentralEntry();
  }

  public void download(OpenToolEntry entry) throws OTMException
  {
    try
    {
      if (!service.isLoggedIn())
      {
        service.login();
      }
      service.download(Integer.parseInt(entry.getIdInRepository()));
    }
    catch (CodeCentralServiceException ex)
    {
      throw new OTMException(ex.getMessage(), ex);
    }
  }

  public List getEntries()
  {
    if (results == null)
    {
      return new ArrayList(0);
    }
    
    List ret = new ArrayList(results.size());
    for (Results result: results)
    {
      ret.add(getEntry(result));
    }
    return ret;
  }
  
  private OpenToolEntry getEntry(Results result)
  {
    CodeCentralEntry entry = new CodeCentralEntry();
    entry.setRepository(this);
    updateEntry(entry, result);
    return entry;
  }
  
  private static void updateEntry(CodeCentralEntry entry, Results result)
  {
    entry.setID(result.getSNIPPETID().toString());
    entry.setIdInRepository(result.getSNIPPETID().toString());
    entry.setTitle(result.getTITLE());
    entry.setShortDescription(result.getSHORTDESC());
    entry.setDescription(fmt(result.getDESCRIPTION()));
    entry.setRunMinVersion(getVersion(result.getLOWVERSION()));
    entry.setRunMaxVersion(getVersion(result.getHIGHVERSION()));
    entry.setLastModifiedDate(result.getUPDATED().toGregorianCalendar().getTime());
    entry.setNbDownloads(result.getHITCOUNT());
    entry.setAuthorName(result.getFIRSTNAME() + " " + result.getLASTNAME());   
    
    //
    entry.setSize(result.getBLOBSIZE());
    entry.setOnCD(result.getCOMPANIONCD());
    entry.setCopyright(result.getCOPYRIGHTID());
    entry.setOriginalUpload(result.getORIGINAL().toGregorianCalendar().getTime());
  }
  
  private static String fmt(String desc)
  {
    return desc.replaceAll("\n", "<br>");
  }
  
  private static Version getVersion(float f)
  {
    int maj = (int)f;
    int min = maj * 10 - (int)f * 10;
    return new Version(maj, min);
  }

  public void updatePropertiesFor(OpenToolEntry entry) throws OTMException
  {
    try
    {
      if (!service.isLoggedIn())
      {
        service.login();
      }
      Results result = service.updateEntry(Integer.parseInt(entry.getIdInRepository()));
      updateEntry((CodeCentralEntry)entry, result);
      
      // download comments list
      CodeCentralOTCommentsListLoader cll = new CodeCentralOTCommentsListLoader(entry);
      cll.load();


    }
    catch (CodeCentralServiceException ex)
    {
      throw new OTMException(ex.getMessage(), ex);
    }
    catch (Exception ex)
    {
      throw new OTMException(ex.getMessage(), ex);
    }

  }
}
