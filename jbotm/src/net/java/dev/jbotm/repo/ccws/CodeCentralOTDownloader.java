package net.java.dev.jbotm.repo.ccws;

import net.java.dev.jbotm.structs.*;
import java.util.logging.*;
import java.util.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.products.*;
import java.io.*;
import com.borland.primetime.ide.*;
import net.java.dev.jbotm.ui.*;
import com.borland.primetime.ui.*;

/**
 * <p>Title: CodeCentral</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2006</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: $ $Date: $
 * @created 18/06/06
 */
public class CodeCentralOTDownloader
{
  private static Logger log = Logger.getLogger(CodeCentralOTDownloader.class.getName());

  public static void download(CodeCentralService service, OpenToolEntry entry)
  {
    if (preLoadChecks(entry))
    {
      doDownload(service, entry);
    }
  }

  private static void autoextract(OpenToolEntry entry)
  {
    try
    {
      ArrayList files = entry.getFiles();
      if (files == null)
      {
        return;
      }
      Iterator iter = files.iterator();
      while (iter.hasNext())
      {
        FileEntry fe = (FileEntry) iter.next();
        if (fe.autoExtract)
        {
          log.fine("extractpath = " + fe.extractPath);
          if ((fe.extractPath != null) && (!fe.extractPath.equals("")))
          {
            String localurlname = entry.getProperty(OpenToolEntry.LOCAL_NAME);
            localurlname = localurlname.replace('\\', '/');
            String urlstr = "zip:///[" + localurlname + "]";
            if (!fe.name.startsWith("/"))
            {
              urlstr += "/";
            }
            urlstr += fe.name;
            log.fine("extract source = " + urlstr);
            com.borland.primetime.vfs.Url url = new com.borland.primetime.vfs.Url(urlstr);
            BufferedInputStream reader = new BufferedInputStream(url.getFilesystem().getInputStream(url));

            File outf = new File(fe.extractPath);
            FileOutputStream w = new FileOutputStream(outf);

            byte[] buf = new byte[reader.available()];
            while (reader.read(buf) > 0)
            {
              w.write(buf);
            }
            reader.close();
            w.close();
          }
        }
      }
    }
    catch (Exception ex)
    {
      String extraction = "Error during extraction of OpenTool '" + entry.getIdInRepository() +
          "'. Extraction aborted.";
      log.info(extraction);
      Browser.getActiveBrowser().getStatusView().setText(extraction, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTDownloader", "autoextract()", ex);
    }
  }

  private static void doDownload(CodeCentralService service, OpenToolEntry entry)
  {
    try
    {
      byte[] content = service.download(Integer.parseInt(entry.getIdInRepository()));
      String filename = entry.getProperty(OpenToolEntry.LOCAL_NAME);
      FileOutputStream fo = new FileOutputStream(filename);
      fo.write(content);
      fo.flush();
      fo.close();
      log.finest("Wrote " + filename + " (" + content.length + " bytes)");

      java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd H:mm:ss");
      sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
      entry.setProperty(OpenToolEntry.LAST_DOWNLOADED, sdf.format(new java.util.Date()));
      log.finest("CodeCentralOTDownloader saving entries");
      entry.save();

      String downloaded = "OT " + entry.getIdInRepository() + " downloaded.";
      log.info(downloaded);
      com.borland.primetime.ide.Browser browser = com.borland.primetime.ide.Browser.getActiveBrowser();
      if (browser != null)
      {
        browser.getStatusView().setText(downloaded);
      }
      autoextract(entry);
    }
    catch (Exception ex)
    {
      String extraction = "Error during download of OpenTool '" + entry.getIdInRepository() + "'. Download aborted.";
      log.info(extraction);
      Browser.getActiveBrowser().getStatusView().setText(extraction, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTDownloader", "handleContent()", ex);
    }
  }

  private static boolean preLoadChecks(OpenToolEntry entry)
  {
    if (entry == null)
    {
      Browser.getActiveBrowser().getStatusView().setText("Error invalid OT entry...", java.awt.Color.red);
      return false;
    }
    String id = entry.getIdInRepository();
    if ((id == null) || id.equals(""))
    {
      Browser.getActiveBrowser().getStatusView().setText("Error invalid OT id...", java.awt.Color.red);
      return false;
    }
    String localname = entry.getProperty(OpenToolEntry.LOCAL_NAME);
    if ((localname == null) || localname.equals(""))
    {
      PropertiesPanel panel = new PropertiesPanel(entry);
      panel.selectDownload();

      DefaultDialog.showModalDialog(Browser.getActiveBrowser(), "OpenTool " + entry.getIdInRepository() + " Properties",
                                    panel,
                                    panel.getHelpPage(), panel);

      localname = entry.getProperty(OpenToolEntry.LOCAL_NAME);
      if ((localname == null) || localname.equals(""))
      {
        Browser.getActiveBrowser().getStatusView().setText("Error OT " + entry.getIdInRepository() +
            " invalid download path... (" +
            localname + ")", java.awt.Color.red);
        return false;
      }
    }
    String downloading = "Downloading OT " + entry.getIdInRepository() + " to " +
        entry.getProperty(OpenToolEntry.LOCAL_NAME) + " ...";
    log.info(downloading);
    Browser.getActiveBrowser().getStatusView().setText(downloading);

    return true;
  }

}
