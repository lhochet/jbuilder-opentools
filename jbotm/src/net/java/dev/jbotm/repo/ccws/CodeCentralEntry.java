package net.java.dev.jbotm.repo.ccws;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import com.borland.primetime.ide.*;

import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.ui.*;

/**
 * <p>Title: CodeCentral</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2006</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: $ $Date: $
 * @created 18/06/06
 */
public class CodeCentralEntry extends OpenToolEntry
{
  private Logger log = Logger.getLogger(CodeCentralEntry.class.getName());

  public CodeCentralEntry()
  {
  }

  public void install() throws OTMException
  {
    String dlpath = getProperty(LOCAL_NAME);
    if (dlpath == null)
    {
      throw new OTMException("File not downloaded");
    }
    File dlfile = new File(dlpath);
    if (!dlfile.exists())
    {
      throw new OTMException("File not downloaded");
    }

    // local entries only have 1 jar as files
    try
    {
      ArrayList files = getFiles();
      if ( files == null)
      {
        return;
      }
      Iterator iter = files.iterator();
      while (iter.hasNext())
      {
        FileEntry fe = (FileEntry)iter.next();
//        if (fe.autoExtract)
        {
          log.fine("extractpath = " + fe.extractPath);
          if ((fe.extractPath != null) && (!fe.extractPath.equals("")))
          {
            String localurlname = dlpath;
            localurlname = localurlname.replace('\\', '/');
            String urlstr = "zip:///[" + localurlname + "]";
            if (!fe.name.startsWith("/"))
            {
              urlstr += "/";
            }
            urlstr += fe.name;
            log.fine("extract source = " + urlstr);
            com.borland.primetime.vfs.Url url = new com.borland.primetime.vfs.Url(urlstr);
            BufferedInputStream reader = new BufferedInputStream(url.getFilesystem().getInputStream(url));

            File outf = new File(fe.extractPath);
            FileOutputStream w = new FileOutputStream(outf);

            byte[] buf = new byte[reader.available()];
            while (reader.read(buf) > 0)
            {
              w.write(buf);
            }
            reader.close();
            w.close();
          }
        }
      }
    }
    catch (Exception ex)
    {
      String extraction = "Error during extraction of OpenTool '" + getIdInRepository() + "'. Extraction aborted.";
      log.info(extraction);
      Browser.getActiveBrowser().getStatusView().setText(extraction, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTDownloader", "autoextract()", ex);
    }
  }

  public List getPropertyPages()
  {
    List lst = super.getPropertyPages();
    lst.add(new CodeCentralEntryPage(this));
    lst.add(new EntryCommentsPanel(this));
    return lst;
  }
  
  public void setSize(int sz)
  {
    setProperty(OpenToolEntry.SIZE, "" + sz);
  }
  
  public void setTerms(int termsid)
  {
    setProperty(OpenToolEntry.TERMS, "" + termsid);
  }
  
  public void setOnCD(String oncd)
  {
    setProperty(OpenToolEntry.ONCD, oncd);
  }
  
  public void setCopyright(int copyrightid)
  {
    setProperty(OpenToolEntry.COPYRIGHT, "" + copyrightid);
  }
  
  public void setOriginalUpload(Date date)
  {
    setProperty(OpenToolEntry.ORIGINAL_UPLOAD, "" + date);
  }


}
