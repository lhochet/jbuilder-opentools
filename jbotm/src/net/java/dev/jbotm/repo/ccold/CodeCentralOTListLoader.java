
/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import java.io.*;
import java.util.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.util.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.http.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import java.text.*;
import java.util.logging.*;
import net.java.dev.jbotm.repo.cc.*;
import net.java.dev.jbotm.old.*;


/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.10 $ $Date: 2005/06/23 21:21:47 $
 * 0.3, 4/07/03
 * 0.2, 16/01/02
 * 0.1, 4/07/01
 * @deprecated
 */
public class CodeCentralOTListLoader extends HTMLDownloader
{
  Logger logger = Logger.getLogger(CodeCentralOTListLoader.class.getName());

  private long start = 0;
  private Vector entries = new Vector();
  private CodeCentralChecker checker = null;
  private String url = null;
  private CodeCentralJBuilder repository = null;
  
  private SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy h:mm:ss a", Locale.US);
  
  private CodeCentralOTListLoader()
  {
    sdf.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
  }

  public CodeCentralOTListLoader(CodeCentralChecker checker) throws Exception
  {
    this();
    this.checker = checker;
    load();
  }

  public CodeCentralOTListLoader(CodeCentralJBuilder repository)
  {
    this();
    this.repository = repository;
    this.url = repository.getOTListURL();
  }

  public Vector getEntries()
  {
    return entries;
  }

  /**
   * getDownloadErrorText
   *
   * @return String
   */
  protected String getDownloadErrorText()
  {
    return "Error during loading of OpenTools list. Loading aborted.";
  }

  /**
   * getDownloadURL
   *
   * @return String
   */
  protected String getDownloadURL()
  {
    if (checker != null)
    {
      return CodeCentralPropertyGroup.CC_BASE + "prodcat?prodid=3&catid=11";
//      return checker.getProfile().getOTListURL();
    }
    else if (url != null)
    {
      return url;
    }
    else
    {
      return null;
    }
  }

  /**
   * getNocookieText
   *
   * @return String
   */
  protected String getNocookieText()
  {
    return "Could not get the Borland cookie necessary to load the list. Loading aborted.";
  }

  /**
   * handleHTMLContent
   *
   * @param reader BufferedReader
   */
  protected void handleHTMLContent(BufferedReader reader)
  {
    try
    {
      log.fine("Time taken to connect (in ms): " + (System.currentTimeMillis() - start));


      String line;

      // strip start
      while ((line = reader.readLine()) != null)
      {
//        System.out.println("@ "+line);
//        if (line.startsWith("<h2>JBuilder OpenTools Submissions</h2>"))
//        if (line.endsWith(" OpenTools Submissions</h2>"))
        if (line.endsWith("&nbsp;Matching Records<br>")) // :) new "Examples"
        {
          break;
        }
      }

      // skip start of table line
      line = reader.readLine();
      // header row start
      line = reader.readLine();
      // header row
      line = reader.readLine();

      StringBuffer buf = new StringBuffer();

      while ((line = reader.readLine()) != null)
      {
        logger.finest("line: " + line);

        if (line.indexOf("</table>") > -1)
        {
          break;
        }

        buf.append(line.trim());
      }

      logger.finest(buf.toString());

      ArrayList lines = new ArrayList();
      int ind = 0;
      final int mx = buf.length();
      final String startTag = "<tr";
      final int szStartTag = startTag.length();
      final String endTag = "</tr>";
      final int szEndTag = endTag.length();
      while (ind < mx)
      {
        int si = buf.indexOf(startTag, ind);
        if (si > -1)
        {
          int ei = buf.indexOf(endTag, si + szStartTag);
          if (ei > -1)
          {
            String ln = buf.substring(si, ei);
            logger.finest(ln);
            lines.add(ln);
            ind = ei + szEndTag;
          }
        }

      }

      Iterator iter = lines.iterator();
      while (iter.hasNext())
      {
        String ln = (String) iter.next();
        processLine(ln);
      }
      
      log.fine("Time taken to load OT list (in ms): " + (System.currentTimeMillis() - start));
    }
    catch (IOException ex)
    {
      String loadingaborted = getDownloadErrorText();
      log.info(loadingaborted);
      Browser.getActiveBrowser().getStatusView().setText(loadingaborted, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTListLoader", "handleHTMLContent()", ex);
    }
  }
  
  private void processLine(String lline)
  {
    final String itemLink = "<a href=\"/Item.aspx?id=";
    final String endId = "\">";
    final String endTitle = "</a><br/><span>";
    final String endDesc = "</span></td><td>";
    final String endDate = "</td><td>";
    final String endDl = "</td><td><span>";
    final String endSz = "</span></td>";
    
    int si = 0;
    int ei = 0;
    
    String idS = null;
    String title = null;
    String desc = null;
    String dateS = null;
    String dlS = null;
    String szS = null;
    
    int ind = lline.indexOf(itemLink);
    if (ind > -1) // OT lline
    {
  
      si = ind + itemLink.length();
      ei = lline.indexOf(endId, si);
      if (ei > -1) // if == -1, unexpected lline type, skip
      {
        idS = lline.substring(si, ei);
  
        si = ei + endId.length();
        ei = lline.indexOf(endTitle, si);
        if (ei == -1)
        {
          idS = null;
        }
        else
        {
          title = lline.substring(si, ei);
  
          si = ei + endTitle.length();
          ei = lline.indexOf(endDesc, si);
          if (ei == -1)
          {
            idS = null;
          }
          else
          {
            desc = lline.substring(si, ei);
  
            si = ei + endDesc.length();
            ei = lline.indexOf(endDate, si);
            if (ei == -1)
            {
              idS = null;
            }
            else
            {
              dateS = lline.substring(si, ei);
  
              si = ei + endDate.length();
              ei = lline.indexOf(endDl, si);
              if (ei == -1)
              {
                idS = null;
              }
              else
              {
                dlS = lline.substring(si, ei);
  
                si = ei + endDl.length();
                ei = lline.indexOf(endSz, si);
                if (ei == -1)
                {
                  idS = null;
                }
                else
                {
                  szS = lline.substring(si, ei);
                }
              } // dl
            } // date
          } // desc
        } // title
      } // id
    } // ot lline
    
    logger.finest("id = " + idS + "\n" +
                  "title = " + title + "\n" +
                  "desc = " + desc + "\n" +
                  "date = " + dateS + "\n" +
                  "dl = " + dlS + "\n" +
                  "sz = " + szS + "\n"
        );
    
    if (idS != null)
    {
      OpenToolEntry entry = new OpenToolEntry();
      entry.setRepository(repository);
      entry.setID(idS);
      entry.setTitle(title);
      entry.setShortDescription(desc);
      try
      {
        entry.setLastModifiedDate(sdf.parse(dateS));
      }
      catch (ParseException ex)
      {
        // do nothing, keeps date null
      }
      entry.setNbDownloads(Integer.parseInt(dlS));
      entry.setProperty(OpenToolEntry.SIZE, szS);
      entries.add(entry);
    }
  }
      
    
    void deadCode()
    {
      try
      {
        String line = null;
        BufferedReader reader = null;
        boolean filterver = OTMPropertyGroup.FILTER_JB_VERSIONS.getBoolean();
        final boolean filterthisversion = OTMPropertyGroup.FILTER_JB_THIS_VERSION.getBoolean();
        Version filterversion = null;
        if (filterthisversion)
        {
          filterversion = OTMPropertyGroup.JB_VERSION;
        }
        else
        {
          try
          {
            filterversion = new Version(OTMPropertyGroup.FILTER_JB_MIN_VERSION.getValue());
          }
          catch (Exception ex2)
          {
            // keep the version null -> no OT will match the null version
          }
        }


      OpenToolEntry entry = null;
      while ((line = reader.readLine()) != null)
      {
        
//        System.out.println("@ " + line);
        if (line.startsWith("</table>"))
        {
          break;
        }
        if (line.startsWith("<tr>"))
        {
          line = reader.readLine();
        }

        entry = new OpenToolEntry();
        entry.setRepository(repository);

//        System.out.println("@startid " + line);

        // id
        line = reader.readLine();
//        System.out.println("@id " + line);
        entry.setID(stripEnd(line));
        entry.setIdInRepository(stripEnd(line));
//        System.out.println("@id " + entry.id);

        // version
        line = reader.readLine();
//        System.out.println("@version " + line);
        String version = stripEnd(stripStart(line));
        entry.setRunMinVersion(extractVersionMin(version));
        entry.setRunMaxVersion(extractVersionMax(version));
//        System.out.println("@version " + entry.version);

        // author
        line = reader.readLine();
//        System.out.println("@author " + line);
        entry.setAuthorName(stripEnd(stripStart(stripStart(line))));
//        System.out.println("@author " + entry.author);

        // title
        line = reader.readLine();
//        System.out.println("@title " + line);
        line = stripStart(stripStart(stripStart(line)));
        entry.setTitle(stripEnd(line));
//        System.out.println("@title " + entry.title);
        // short desc
        line = /*stripEnd(*/ stripStart(stripStart(stripStart(line))) /*)*/;
        StringBuffer desc = new StringBuffer(line);
        while ((!line.endsWith("</td>")) && ((line = reader.readLine()) != null))
        {
          desc.append(line);
        }
        String shortDesc = desc.toString();
        if (shortDesc.endsWith("</td>"))
        {
          shortDesc = shortDesc.substring(0, shortDesc.length() - 5);
        }
        if (shortDesc.length() > 100)
        {
          String tmp = "";
          int len = shortDesc.length();
          for (int i = 0; i < shortDesc.length(); i += 100)
          {
            int ln = i + 100 > len ? len : i + 100;
//            System.out.println("len = " + len + ", ln = " + ln + ", i = " + i + ", i + 100 = " + (i + 100));
            tmp += shortDesc.substring(i, ln) + "<br>";
          }
          shortDesc = tmp;
        }
        entry.setShortDescription(shortDesc);
//        System.out.println("@shortDesc " + entry.shortDesc);

        // date
        line = reader.readLine();
//        System.out.println("@date " + line);
        try
        {
          String datestr = stripEnd(stripStart(line));
          entry.setLastModifiedDate(sdf.parse(datestr));
//        System.out.println("@date " + datestr);
        }
        catch (ParseException ex3)
        {
          log.throwing("CodeCentralOTListLoader", "handleHTMLContent()", ex3);
          entry.setLastModifiedDate(new Date(0)); // this should set the date to 1/1/70
        }

        // download
        line = reader.readLine();
//        System.out.println("@download " + line);
        try
        {
          entry.setNbDownloads(Integer.parseInt(stripEnd(stripStart(line))));
//        System.out.println("@download " + entry.downloads);
        }
        catch (NumberFormatException ex4)
        {
          // ignore
        }

        // threads
        line = reader.readLine();
//        System.out.println("@threads *" + line + "*");
        try
        {
          entry.setNbComments(Integer.parseInt(stripEnd(stripStart(stripStart(line)))));
        }
        catch (NumberFormatException ex1)
        {
          // do nothing
        }

        // /td
        line = reader.readLine();
//        System.out.println("@/td " + /*stripEnd(stripStart(*/line/*))*/);

//        entry.isShown = true;
        if (filterver)
        {
          CCVersionRange versions = new CCVersionRange(entry.getRunMinVersion(), entry.getRunMaxVersion());
          if (filterthisversion)
          {
            entry.setShown(versions.contains(filterversion));
          }
          else
          {
            entry.setShown(versions.getMaxVersion().compareTo(filterversion) >= 0);
          }
        }
        entries.add(entry);

      }

      log.fine("Time taken to load OT list (in ms): " + (System.currentTimeMillis() - start));
      // clean up
      reader.close();
    }
    catch (IOException ex)
    {
      String loadingaborted = getDownloadErrorText();
      log.info(loadingaborted);
      Browser.getActiveBrowser().getStatusView().setText(loadingaborted, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTListLoader", "handleHTMLContent()", ex);
    }
  }

  private Version extractVersionMax(String string)
  {
//    log.finest("version line max = " + string);
    int i = string.indexOf("to");
    String str = string.substring(i + 3);
//    log.finest("extracted = *" + str + "*");
    Version vrs = new Version(str);
//    log.fine("version = *" + vrs.toString() + "*");

    return vrs;
  }

  private Version extractVersionMin(String string)
  {
//    log.finest("version line min = " + string);
    int i = string.indexOf("to");
    String str = string.substring(0, i-1);
//    log.finest("extracted = *" + str + "*");
    Version vrs = new Version(str);
//    log.fine("version = *" + vrs.toString() + "*");

    return vrs;
  }

  /**
   * preLoadChecks
   *
   * @return boolean
   */
  protected boolean preLoadChecks()
  {
    start = System.currentTimeMillis(); // check how long it takes to load the page
    return true;
  }

  /**
   * Return false as the Borland cookie is NOT needed to load the OT list
   *
   * @return boolean False as the Borland cookie is NOT needed to load the OT list
   */
  protected boolean needCookie()
  {
    return false;
  }
}
