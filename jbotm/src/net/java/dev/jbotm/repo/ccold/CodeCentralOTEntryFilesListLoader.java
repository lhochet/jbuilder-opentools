/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import java.io.*;
import java.util.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.http.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import com.borland.primetime.properties.PropertyManager;
import com.borland.primetime.vfs.Url;

import net.java.dev.jbotm.repo.cc.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.6 $ $Date: 2005/01/23 22:05:28 $
 * 0.3, 16/03/02
 * 0.2, 20/11/01
 * 0.1, 3/10/01
 * @deprecated no longer supported by CodeCentral it seems
 */

public class CodeCentralOTEntryFilesListLoader extends HTMLDownloader
{
  private final String CC_ENTRY_FL_BASE = CodeCentralPropertyGroup.CC_BASE + "files?id=";

//  private static final String CC_ENTRY_FG_BASE = "http://codecentral.borland.com/codecentral/ccweb.exe/getfile?id=";
//  private static final String CC_ENTRY_FG_END = "&filename=deploy.xml";

  private OpenToolEntry entry = null;

  public CodeCentralOTEntryFilesListLoader(OpenToolEntry entry) throws Exception
  {
    this.entry = entry;
  }

  private void populateExtractPaths(ArrayList files) throws Exception
  {
    CodeCentralOTDeployFileLoader ccotdfl = new CodeCentralOTDeployFileLoader(entry, files);
    ccotdfl.load();
  }

  /**
   * getDownloadErrorText
   *
   * @return String
   */
  protected String getDownloadErrorText()
  {
    return "Error during loading of file list for OpenTool '" + entry.getIdInRepository() + "'. Loading aborted.";
  }

  /**
   * getDownloadURL
   *
   * @return String
   */
  protected String getDownloadURL()
  {
    return CC_ENTRY_FL_BASE + entry.getIdInRepository();
  }

  /**
   * getNocookieText
   *
   * @return String
   */
  protected String getNocookieText()
  {
    return "Error downloading OT " + entry.getIdInRepository() + " files list : could not get cookie";
  }

  /**
   * preLoadChecks
   *
   * @return boolean
   */
  protected boolean preLoadChecks()
  {
    log.info("Downloading OT " + entry.getIdInRepository() + " files list...");
    return true;
  }

  /**
   * handleHTMLContent
   *
   * @param reader BufferedReader
   */
  protected void handleHTMLContent(BufferedReader reader)
  {
    try
    {
      String line;
      ArrayList newFiles = new ArrayList();
      boolean hasDeploy = false;

      while ((line = reader.readLine()) != null)
      {
        if (line.indexOf("/ccweb.exe/getfile?id=") > 0)
        {
          FileEntry fentry = new FileEntry();


          int start = line.indexOf('>', 55);
          int end = line.indexOf('<', start);
          fentry.name = line.substring(start + 1, end);
          if (fentry.name.endsWith("deploy.xml"))
          {
            hasDeploy = true;

          }
          start = end + 13;
          end = line.indexOf('<', start);
          fentry.date = line.substring(start, end);

          start = end + 23;
          end = line.indexOf('<', start);
          fentry.size = line.substring(start, end);

          newFiles.add(fentry);
        }
      }
      ;

      Url install = PropertyManager.getInstallRootUrl();
      Url libext = install.getRelativeUrl("lib/ext");

      ArrayList oldFiles = entry.getFiles();
      Iterator iter = newFiles.iterator();
      while (iter.hasNext())
      {
        FileEntry item = (FileEntry)iter.next();
        if (item.name.endsWith(".jar"))
        {
          item.extractPath = libext.getRelativeUrl(item.name).getFullName();
        }
        if (oldFiles != null)
        {
          Iterator iterOld = oldFiles.iterator();
          while (iterOld.hasNext())
          {
            FileEntry olditem = (FileEntry)iterOld.next();
            if (item.name.equals(olditem.name))
            {
              item.autoExtract = olditem.autoExtract;
              item.extractPath = olditem.extractPath;
            }
          }
        }
      }

      if (hasDeploy)
      {
        populateExtractPaths(newFiles);

      }

      entry.setFiles(newFiles);

      log.info("Downloaded OT " + entry.getIdInRepository() + " files list.");
    }
    catch (Exception ex)
    {
      String loadingaborted = "Error during loading of file list for OpenTool '" + entry.getIdInRepository() + "'. Loading aborted.";
      log.info(loadingaborted);
      Browser.getActiveBrowser().getStatusView().setText(loadingaborted, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTEntryFileListLoader", "handleHTMLContent()", ex);
    }
  }
}
