/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.http.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.repo.cc.*;

/**
 * @author Ludovic HOCHET
 * @version  $Revision: 1.9 $ $Date: 2005/03/05 21:58:17 $
 * 0.4, 22/11/01
 * 0.3, 24/09/01
 * 0.2, 25/08/01
 * 0.1, 19/07/01
 * @deprecated
 */
public class CodeCentralOTDownloader extends Downloader
{
  Logger log = Logger.getLogger(CodeCentralOTDownloader.class.getName());

  private static final String downloadURLBase = CodeCentralPropertyGroup.CC_BASE + "download?id=";

  public static String getDownloadURL(String id)
  {
    return downloadURLBase + id;
  }

  private OpenToolEntry entry = null;
  private String realURL = null;
  private Checker checker = null;

  /*
    String cookieName = "USER";
    String cookieValue = null;
    String cookiePath = ";Path=/";
    String cookieBase1 = "[|]";
    String cookieBase2 = "[|][|][|]";
    String cookieBase3 = "[|]EN[|]90[|]1[|]ISO8859_1";
//  String cookieBase = "[|]username[|]firstname[|]name[|]email[|]EN[|]90[|]1[|]ISO8859_1";
    String communityLogin = "https://community.borland.com/cgi-bin/login/login.cgi?";
   */

  public CodeCentralOTDownloader(OpenToolEntry entry)
  {
    this.entry = entry;
    checker = ProductManager.getProduct(entry.getRepository().getProduct()).getChecker();
  }

  private void autoextract()
  {
    try
    {
      ArrayList files = entry.getFiles();
      if ( files == null)
      {
        return;
      }
      Iterator iter = files.iterator();
      while (iter.hasNext())
      {
        FileEntry fe = (FileEntry)iter.next();
        if (fe.autoExtract)
        {
          log.fine("extractpath = " + fe.extractPath);
          if ((fe.extractPath != null) && (!fe.extractPath.equals("")))
          {
            String localurlname = entry.getProperty(OpenToolEntry.LOCAL_NAME);
            localurlname = localurlname.replace('\\', '/');
            String urlstr = "zip:///[" + localurlname + "]";
            if (!fe.name.startsWith("/"))
            {
              urlstr += "/";
            }
            urlstr += fe.name;
            log.fine("extract source = " + urlstr);
            com.borland.primetime.vfs.Url url = new com.borland.primetime.vfs.Url(urlstr);
            BufferedInputStream reader = new BufferedInputStream(url.getFilesystem().getInputStream(url));

            File outf = new File(fe.extractPath);
            FileOutputStream w = new FileOutputStream(outf);

            byte[] buf = new byte[reader.available()];
            while (reader.read(buf) > 0)
            {
              w.write(buf);
            }
            reader.close();
            w.close();
          }
        }
      }
    }
    catch (Exception ex)
    {
      String extraction = "Error during extraction of OpenTool '" + entry.getIdInRepository() + "'. Extraction aborted.";
      log.info(extraction);
      Browser.getActiveBrowser().getStatusView().setText(extraction, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTDownloader", "autoextract()", ex);
    }
  }

  /**
   * getDownloadErrorText
   *
   * @return String
   */
  protected String getDownloadErrorText()
  {
    return "Error during OpenTool '" + entry.getIdInRepository() + "' download. Download aborted.";
  }

  /**
   * getDownloadURL
   *
   * @return String
   */
  protected String getDownloadURL()
  {
    // hack until a better solution is found
    if (realURL == null)
    {
      extractRealURL();
    }
    return realURL;
//    return downloadURLBase + entry.id;
  }

  private void extractRealURL()
  {
    final CodeCentralOTDownloader me = this;
    HTMLDownloader redirectDownloader = new HTMLDownloader()
    {
      protected String getDownloadErrorText()
      {
        return me.getDownloadErrorText();
      }

      protected String getNocookieText()
      {
        return me.getNocookieText();
      }

      protected String getDownloadURL()
      {
        return downloadURLBase + entry.getIdInRepository();
      }

      protected boolean preLoadChecks()
      {
        return true;
      }

      protected void handleHTMLContent(BufferedReader reader)
      {
        try
        {
          String line;
          String anchor = "<meta http-equiv=\"refresh\" content=\"0;url=";
          int asz = anchor.length();

          while ((line = reader.readLine()) != null)
          {
            if (line.startsWith(anchor))
            {
              realURL = line.substring(asz, line.length() - 2);
              return;
            }
          };

          log.fine("Retrieved " + entry.getIdInRepository() + " zip location.");
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
          log.throwing("CodeCentralOTDownloader.redirectDownloader", "handleHTMLContent()", ex);
        }
      }

    };
    try
    {
      redirectDownloader.load();
    }
    catch (Exception ex)
    {
    }
  }

  /**
   * getNocookieText
   *
   * @return String
   */
  protected String getNocookieText()
  {
    return "OT " + entry.getIdInRepository() + ": Could not get the cookie necessary to download. Download aborted.";
  }

  /**
   * handleContent
   *
   * @param bis BufferedInputStream
   * @param csz int
   */
  protected void handleContent(BufferedInputStream bis, int csz)
  {
    try
    {
      byte[] content = new byte[csz];
      FileOutputStream fo = new FileOutputStream(entry.getProperty(OpenToolEntry.LOCAL_NAME));
      int len = 0;
      int tot = 0;
//      System.out.println("before writing");
      while ((len = bis.read(content /*, len, csz - tot*/)) > 0)
      {
        fo.write(content, 0, len);
//        System.out.println("wrote len = "+len);
        tot += len;
      }
      bis.close();
      fo.flush();
      fo.close();
//      System.out.println("after writing, last len = "+len);
//      System.out.println("diff = " + (csz - tot));

      java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd H:mm:ss");
      sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
      entry.setProperty(OpenToolEntry.LAST_DOWNLOADED, sdf.format(new java.util.Date()));
      log.finest("CodeCentralOTDownloader saving entries");
      entry.save();

      String downloaded = "OT " + entry.getIdInRepository() + " downloaded.";
      log.info(downloaded);
      com.borland.primetime.ide.Browser browser = com.borland.primetime.ide.Browser.getActiveBrowser();
      if (browser != null)
      {
        browser.getStatusView().setText(downloaded);
      }
      autoextract();
    }
    catch (Exception ex)
    {
      String extraction = "Error during download of OpenTool '" + entry.getIdInRepository() + "'. Download aborted.";
      log.info(extraction);
      Browser.getActiveBrowser().getStatusView().setText(extraction, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTDownloader", "handleContent()", ex);
    }
  }

  /**
   * preLoadChecks
   *
   * @return boolean
   */
  protected boolean preLoadChecks()
  {
    if (entry == null)
    {
      Browser.getActiveBrowser().getStatusView().setText("Error invalid OT entry...", java.awt.Color.red);
      return false;
    }
    String id = entry.getIdInRepository();
    if ((id == null) || id.equals(""))
    {
      Browser.getActiveBrowser().getStatusView().setText("Error invalid OT id...", java.awt.Color.red);
      return false;
    }
    String localname = entry.getProperty(OpenToolEntry.LOCAL_NAME);
    if ((localname == null) || localname.equals(""))
    {
      PropertiesPanel panel= new PropertiesPanel(entry);
      panel.selectDownload();

      DefaultDialog.showModalDialog(Browser.getActiveBrowser(), "OpenTool " + entry.getIdInRepository() + " Properties", panel,
                                    panel.getHelpPage(), panel);

      localname = entry.getProperty(OpenToolEntry.LOCAL_NAME);
      if ((localname == null) || localname.equals(""))
      {
        Browser.getActiveBrowser().getStatusView().setText("Error OT " + entry.getIdInRepository() + " invalid download path... (" +
          localname + ")", java.awt.Color.red);
        return false;
      }
    }
    String downloading = "Downloading OT " + entry.getIdInRepository() + " to " + entry.getProperty(OpenToolEntry.LOCAL_NAME) + " ...";
    log.info(downloading);
    Browser.getActiveBrowser().getStatusView().setText(downloading);

    return true;
  }

}
