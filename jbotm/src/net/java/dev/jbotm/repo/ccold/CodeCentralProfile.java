/*
   Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import java.lang.ref.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.old.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2004/10/27 20:41:43 $
 * @created 26/12/03
 */
abstract public class CodeCentralProfile
{
//  static class CCJBuilderProfile extends CodeCentralProfile
//  {
//    protected String filename = "community/cc/jbuilder.xml";
//    protected String productname = "JBuilder";
//    public String getFilename()    {return filename;    }
//    public String getProductName()    {return productname;    }
//  }

//  public static final CodeCentralProfile JBUILDER = new CCJBuilderProfile();

//  private SoftReference checkerRef = null;
  private SoftReference filemanRef = null;



//  public CodeCentralChecker getChecker(Browser browser)
//  {
//    CodeCentralChecker checker = null;
//    if (checkerRef == null)
//    {
//      checker = new CodeCentralChecker(this, browser);
//      checkerRef = new SoftReference(checker);
//    }
//    else
//    {
//      checker = (CodeCentralChecker)checkerRef.get();
//      if (checker == null)
//      {
//        checker = new CodeCentralChecker(this, browser);
//        checkerRef = new SoftReference(checker);
//      }
//    }
//    return checker;
//  }

  public XMLOTListFileManager getFileManager()
  {
    XMLOTListFileManager fileman = null;
    if (filemanRef == null)
    {
      fileman = new XMLOTListFileManager(getFilename());
      filemanRef = new SoftReference(fileman);
    }
    else
    {
      fileman = (XMLOTListFileManager)filemanRef.get();
      if (fileman == null)
      {
        fileman = new XMLOTListFileManager(getFilename());
        filemanRef = new SoftReference(fileman);
      }
    }
    return fileman;
  }

  public abstract String getFilename();

  public abstract String getProductName();

}
