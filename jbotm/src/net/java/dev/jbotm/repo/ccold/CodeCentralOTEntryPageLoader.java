/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import java.io.*;
import java.util.logging.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.http.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.repo.cc.*;

/**
 * @author Ludovic HOCHET
 * @version  $Revision: 1.4 $ $Date: 2005/01/14 14:48:51 $
 * 0.2.2, 7/12/01
 * 0.2, 20/11/01
 * 0.1, 23/09/01
 * @deprecated
 */
public class CodeCentralOTEntryPageLoader extends HTMLDownloader
{
  private static Logger log = Logger.getLogger(CodeCentralOTEntryPageLoader.class.getName());

  private final String CC_ENTRY_PG_BASE = CodeCentralPropertyGroup.CC_BASE + "listing?id=";

  private OpenToolEntry entry = null;

  private long start = 0;

  public CodeCentralOTEntryPageLoader(OpenToolEntry entry) throws Exception
  {
    this.entry = entry;
  }

  /**
   * getDownloadErrorText
   *
   * @return String
   */
  protected String getDownloadErrorText()
  {
    return "Error during loading of OpenTool '" + entry.getIdInRepository() + "' listing. Loading aborted.";
  }

  /**
   * getDownloadURL
   *
   * @return String
   */
  protected String getDownloadURL()
  {
    return CC_ENTRY_PG_BASE + entry.getIdInRepository();
  }

  /**
   * getNocookieText
   *
   * @return String
   */
  protected String getNocookieText()
  {
    return "Error downloading OT " + entry.getIdInRepository() + " listing : could not get cookie";
  }

  /**
   * preLoadChecks
   *
   * @return boolean
   */
  protected boolean preLoadChecks()
  {
    log.info("Downloading OT " + entry.getIdInRepository() + " listing...");
    start = System.currentTimeMillis();
    return true;
  }

  /**
   * handleHTMLContent
   *
   * @param reader BufferedReader
   */
  protected void handleHTMLContent(BufferedReader reader)
  {
    try
    {
      String line;

      // skip start of page
      while (((line = reader.readLine()) != null) && (!line.startsWith("<b>ID: ")))
      {
//        System.out.println("@ " + line);
        ;
      }

      // skip
      line = reader.readLine(); //table
//			System.out.println("table = " + line);
      reader.readLine(); //tr
      reader.readLine(); //title
      reader.readLine(); //title
      reader.readLine(); //tr
      reader.readLine(); //tr
      line = reader.readLine(); //vendor
      if (line.indexOf("Vendor") > 0) // if not then it should be the terms section
      {
        reader.readLine(); //vendor
        reader.readLine(); //tr
        reader.readLine(); //tr
        reader.readLine(); //terms
      }

      // terms
      line = reader.readLine();
//			System.out.println("term :@ " + line);
      entry.setProperty(OpenToolEntry.TERMS, stripEnd(stripStart(line)));
//			System.out.println("term ? :@ " + entry.terms);

      reader.readLine(); //tr
      reader.readLine(); //tr
      reader.readLine(); //name
      reader.readLine(); //name url
      line = reader.readLine(); //name + email
//			System.out.println("email:@ " + line);
//			System.out.println("email:@ " + line.substring(line.indexOf("email: ") + 1));
      Author author = entry.getAuthor();
      if (author == null) // shouldn't be
      {
        author = new Author();
        entry.setAuthor(author);
      }
      author.setEmail(stripEnd(line.substring(line.indexOf("email: ") + 1 + "email: ".length())));
      reader.readLine(); //tr

//      reader.readLine(); //tr old
//      reader.readLine(); //email
//      // email
//      entry.email = stripEnd(stripStart(reader.readLine()));
//      reader.readLine(); //tr

      reader.readLine(); //tr
      reader.readLine(); // url

      // url
      line = reader.readLine();
//      System.out.println("line = @" + line + "@");
      line = stripStart(line);
//      System.out.println("line = @" + line + "@");
      if (line.startsWith("<"))
      {
        entry.setProperty(OpenToolEntry.OT_SITE_URL, stripEnd(line));
      }
      else
      {
        entry.setProperty(OpenToolEntry.OT_SITE_URL, stripEnd(line));
//      System.out.println("line = @" + entry.url + "@");
//      entry.url = stripEnd(stripStart(line));
//      System.out.println("line = @" + entry.url + "@");

      }
      reader.readLine(); //tr

      reader.readLine(); //tr
      reader.readLine(); //summary
      reader.readLine(); //summary
      reader.readLine(); //tr

      reader.readLine(); //tr
      reader.readLine(); //description

      // description
//      String descstart; // = stripStart(reader.readLine());
//      if (descstart == null) descstart = "";
      StringBuffer desc = new StringBuffer("");
      boolean endofdescription = false;
      while ((line = reader.readLine()) != null) // && (!line.endsWith("</td>")))
      {
//        log.finest("@desc 1 : " + line + "@eol");
        if (line.indexOf("<td>") >= 0)
        {
          line = line.substring(line.indexOf("<td>") + 4);
//          log.finest("@desc 2 : " + line + "@eol");
        }
        if (line.indexOf("</td>") >= 0)
        {
          line = line.substring(0, line.indexOf("</td>"));
//          log.finest("@desc 3 : " + line + "@eol");
          endofdescription = true;
        }
        if (line.endsWith("<br />"))
        {
          line = line.substring(0, line.length() - 6) + "<br>";
//          log.finest("@desc 3.1 : " + line + "@eol");
        }
//        log.finest("@desc 4 : " + line + "@eol");
        desc.append(line);
        if (endofdescription)
        {
          break;
        }
      }
      entry.setDescription("<html><body>" + desc.toString() + "</body></html>");
      //      int ind;
//      while ((ind = entry.description.indexOf("<br>"))> -1)
//      {
//        desc = new StringBuffer(entry.description);
//        desc.replace(ind, ind + 4, "\n");
//        entry.description = desc.toString();
//      }

      reader.readLine(); //tr
      reader.readLine(); //tr
      reader.readLine(); //product and version
      reader.readLine(); //product
      reader.readLine(); //tr
      reader.readLine(); //tr
      reader.readLine(); //contest / cat
      reader.readLine(); //contest / cat
      reader.readLine(); //tr
//      reader.readLine(); //tr
//      reader.readLine(); //cat
//      reader.readLine(); //cat
//      reader.readLine(); //tr
      reader.readLine(); //tr
      reader.readLine(); //original

      line = reader.readLine(); //update
//			System.out.println("originalupload:@ " + line);
//			System.out.println("originalupload:@ " + line.substring(0, line.indexOf("<b>last updated</b> ")));
//			System.out.println("originalupload:@ " + stripStart(line.substring(0, line.indexOf("<b>last updated</b> "))));
      if (line.indexOf("<b>last updated</b> ") >= 0)
      {
        // original upload
        entry.setProperty(OpenToolEntry.ORIGINAL_UPLOAD, stripStart(line.substring(0, line.indexOf("<b>last updated</b> "))));
      }
      reader.readLine(); //tr
//      reader.readLine(); //tr
//      reader.readLine(); //updated old
//      reader.readLine(); //updated
//      reader.readLine(); //tr
      reader.readLine(); //tr
      reader.readLine(); //tool cd

      // on cd
      entry.setProperty(OpenToolEntry.ONCD, stripEnd(stripStart(line)));

      reader.readLine(); //tr
      reader.readLine(); //tr
      reader.readLine(); //copyright

      // copyright
      entry.setProperty(OpenToolEntry.COPYRIGHT, stripEnd(stripStart(line)));

      reader.readLine(); //tr
//      reader.readLine(); //tr
//      reader.readLine(); //downloads / old
//      reader.readLine(); //downloads
//      reader.readLine(); //tr
      reader.readLine(); //tr
      reader.readLine(); //sz

      // size
      line = reader.readLine();
//			System.out.println("size:@ " + line);
//			System.out.println("size:@ " + stripStart(line));
//			System.out.println("size:@ " + stripEnd(stripStart(line)));
      entry.setProperty(OpenToolEntry.SIZE, stripEnd(stripStart(line)));

      // ignore end of page
      // skip start of page
      while (((line = reader.readLine()) != null) && (!line.startsWith("Rating System: ")))
      {
        ;
      }
//      System.out.println("@ " + line);
      if (line.startsWith("Rating System: "))
      {
        entry.setProperty(CodeCentralEntry.RATINGSYSTEM, stripEnd(stripStart(line)));
//				System.out.println("rating system : " + entry.ratingsystem);
        line = reader.readLine();
//				System.out.println("@ " + line);
        if (line.startsWith("<b>Average</b>:"))
        {
//					entry.ratingsystem = null;
//					entry.rating = null;
//					entry.ratingresponses = null;
//				  System.out.println("average rating : Not rated.");
          String rating = stripEnd(stripStart(line));
          entry.setProperty(CodeCentralEntry.RATING, rating);
          if (rating.indexOf("No ratings") >= 0)
          {
//            entry.rating = null;
//            entry.ratingresponses = null;
//            entry.ratingsystem = null;
          }
          else
          {
//            System.out.println("average rating : " + entry.rating);
            line = reader.readLine();
//            System.out.CodeCentralEntry("@ " + line);
            entry.setProperty(CodeCentralEntry.RATINGRESPONSES, stripEnd(stripStart(line)));
//            System.out.println("rating responses : " + entry.ratingresponses);
          }
        }
        else
        {
          entry.setProperty(CodeCentralEntry.RATING, stripEnd(stripStart(line)));
//					System.out.println("average rating : " + entry.rating);
          line = reader.readLine();
//					System.out.println("@ " + line);
          entry.setProperty(CodeCentralEntry.RATINGRESPONSES, stripEnd(stripStart(line)));
//					System.out.println("rating responses : " + entry.ratingresponses);
        }

      }

      log.fine("Time taken to parse OT " + entry.getIdInRepository() + " listing (in ms): " + (System.currentTimeMillis() - start));

      // download comments list
      CodeCentralOTCommentsListLoader cll = new CodeCentralOTCommentsListLoader(entry);
      cll.load();

      // download files list
      CodeCentralOTEntryFilesListLoader fll = new CodeCentralOTEntryFilesListLoader(entry);
      fll.load();

      log.fine("Downloaded OT " + entry.getIdInRepository() + " listing. Time taken to load (in ms): " + (System.currentTimeMillis() - start));

    }
    catch (Exception ex)
    {
      String loadingaborted = "Error during loading of OpenTool '" + entry.getIdInRepository() + "' listing. Loading aborted.";
      log.info(loadingaborted);
      Browser.getActiveBrowser().getStatusView().setText(loadingaborted, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTEntryPageLoader", "handleHTMLContent()", ex);
    }
  }

}
