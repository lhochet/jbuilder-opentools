/*
   Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.properties.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.repo.http.Downloader;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.repo.cc.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.4 $ $Date: 2005/01/14 14:48:51 $
 * @created 27/12/03
 * @deprecated
 */
public class CodeCentralOTDeployFileLoader extends Downloader
{
  private static final String CC_ENTRY_FG_BASE = CodeCentralPropertyGroup.CC_BASE + "getfile?id=";
  private static final String CC_ENTRY_FG_END = "&filename=deploy.xml";

  private OpenToolEntry entry = null;
  private ArrayList files = null;

  public CodeCentralOTDeployFileLoader(OpenToolEntry entry, ArrayList files)
  {
    this.entry = entry;
    this.files = files;
  }

  /**
   * getDownloadErrorText
   *
   * @return String
   */
  protected String getDownloadErrorText()
  {
    return "Error during loading of the deploy file for OpenTool '" + entry.getIdInRepository() + "'. Loading aborted.";
  }

  /**
   * getDownloadURL
   *
   * @return String
   */
  protected String getDownloadURL()
  {
    return CC_ENTRY_FG_BASE + entry.getIdInRepository() + CC_ENTRY_FG_END;
  }

  /**
   * getNocookieText
   *
   * @return String
   */
  protected String getNocookieText()
  {
    return "Error downloading OT " + entry.getIdInRepository() + " deploy file : could not get cookie";
  }

  /**
   * handleContent
   *
   * @param bis BufferedInputStream
   * @param csz int
   */
  protected void handleContent(BufferedInputStream bis, int csz)
  {
    try
    {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(bis);

      NodeList entrieslist = document.getDocumentElement().getElementsByTagName("file");
      for (int i = 0; i < entrieslist.getLength(); i++)
      {
        Element entrynode = (Element)entrieslist.item(i);

        Iterator iter = files.iterator();
        while (iter.hasNext())
        {
          FileEntry file = (FileEntry)iter.next();
          if (file.name.equals(entrynode.getAttribute("name")))
          {
            String extractpath = entrynode.getAttribute("to");
            if (extractpath != null)
            {
              if (extractpath.startsWith("jbuilder"))
              {
                String subpath = extractpath.substring((new String("jbuilder")).length() + 1);
                if (!subpath.endsWith("/"))
                {
                  subpath += "/";

                }
                if ((file.extractPath == null) || file.extractPath.equals(""))
                {
                  String localname = entrynode.getAttribute("localname");
                  if ((localname == null) || localname.equals(""))
                  {
                    localname = file.name.substring(file.name.lastIndexOf("/") + 1);
                  }
                  file.extractPath = (new File(PropertyManager.getInstallRootUrl().getFileObject(), subpath + localname)).
                    getAbsolutePath();
                }
              }
              else if (extractpath.startsWith(".jbuilder"))
              {
                String subpath = extractpath.substring((new String(".jbuilder")).length() + 1);
                if (!subpath.endsWith("/"))
                {
                  subpath += "/";

                }
                if ((file.extractPath == null) || file.extractPath.equals(""))
                {
                  String localname = entrynode.getAttribute("localname");
                  if ((localname == null) || localname.equals(""))
                  {
                    localname = file.name.substring(file.name.lastIndexOf("/") + 1);
                  }
                  file.extractPath = (new File(PropertyManager.getSettingsRootUrl().getFileObject(),
                                               subpath + localname)).getAbsolutePath();
                }
              }
              else if (extractpath.startsWith("home"))
              {
                String subpath = extractpath.substring((new String("home")).length() + 1);
                if (!subpath.endsWith("/"))
                {
                  subpath += "/";

                }
                if ((file.extractPath == null) || file.extractPath.equals(""))
                {
                  String localname = entrynode.getAttribute("localname");
                  if ((localname == null) || localname.equals(""))
                  {
                    localname = file.name.substring(file.name.lastIndexOf("/") + 1);
                  }
                  String home = System.getProperty("user.home");
                  if ((home == null) || home.equals(""))
                  {
                    file.extractPath = (new File((new File(home)), subpath + localname)).getAbsolutePath();
                  }
                }
              }
            }
            break;
          }
        }
      }
    }
    catch (Exception ex)
    {
      String loadingaborted = "Error during loading of the deploy file for OpenTool '" + entry.getIdInRepository() + "'. Loading aborted.";
      log.info(loadingaborted);
      Browser.getActiveBrowser().getStatusView().setText(loadingaborted, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTDeployFileLoader", "handleContent()", ex);
    }
  }

  /**
   * preLoadChecks
   *
   * @return boolean
   */
  protected boolean preLoadChecks()
  {
    log.info("Downloading OT " + entry.getIdInRepository() + " deploy file...");
    return true;
  }

}
