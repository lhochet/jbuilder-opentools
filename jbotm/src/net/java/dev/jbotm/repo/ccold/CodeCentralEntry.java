/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import com.borland.primetime.ide.*;

import net.java.dev.jbotm.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.repo.cc.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/23 22:04:47 $
 * @created 18/01/05
 * @deprecated
 */
public class CodeCentralEntry extends OpenToolEntry
{
  Logger log = Logger.getLogger(CodeCentralEntry.class.getName());

  public static final String RATINGSYSTEM = "ratingsystem"; //CC
  public static final String RATING = "rating"; //CC
  public static final String RATINGRESPONSES = "ratingresponses"; //CC

  public CodeCentralEntry()
  {
  }

  public void install() throws OTMException
  {
    String dlpath = getProperty(LOCAL_NAME);
    if (dlpath == null)
    {
      throw new OTMException("File not downloaded");
    }
    File dlfile = new File(dlpath);
    if (!dlfile.exists())
    {
      throw new OTMException("File not downloaded");
    }

    // local entries only have 1 jar as files
    try
    {
      ArrayList files = getFiles();
      if ( files == null)
      {
        return;
      }
      Iterator iter = files.iterator();
      while (iter.hasNext())
      {
        FileEntry fe = (FileEntry)iter.next();
//        if (fe.autoExtract)
        {
          log.fine("extractpath = " + fe.extractPath);
          if ((fe.extractPath != null) && (!fe.extractPath.equals("")))
          {
            String localurlname = dlpath;
            localurlname = localurlname.replace('\\', '/');
            String urlstr = "zip:///[" + localurlname + "]";
            if (!fe.name.startsWith("/"))
            {
              urlstr += "/";
            }
            urlstr += fe.name;
            log.fine("extract source = " + urlstr);
            com.borland.primetime.vfs.Url url = new com.borland.primetime.vfs.Url(urlstr);
            BufferedInputStream reader = new BufferedInputStream(url.getFilesystem().getInputStream(url));

            File outf = new File(fe.extractPath);
            FileOutputStream w = new FileOutputStream(outf);

            byte[] buf = new byte[reader.available()];
            while (reader.read(buf) > 0)
            {
              w.write(buf);
            }
            reader.close();
            w.close();
          }
        }
      }
    }
    catch (Exception ex)
    {
      String extraction = "Error during extraction of OpenTool '" + getIdInRepository() + "'. Extraction aborted.";
      log.info(extraction);
      Browser.getActiveBrowser().getStatusView().setText(extraction, StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("CodeCentralOTDownloader", "autoextract()", ex);
    }
  }

  public List getPropertyPages()
  {
    List lst = super.getPropertyPages();
    lst.add(new CodeCentralEntryPage(this));
    lst.add(new EntryCommentsPanel(this));
    return lst;
  }

}
