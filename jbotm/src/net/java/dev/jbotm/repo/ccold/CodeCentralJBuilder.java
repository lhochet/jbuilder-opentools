/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.ccold;

import net.java.dev.jbotm.repo.*;
import java.util.*;
import net.java.dev.jbotm.OTMException;
import net.java.dev.jbotm.structs.OpenToolEntry;
import net.java.dev.jbotm.products.*;
import net.java.dev.jbotm.*;

import net.java.dev.jbotm.repo.cc.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005/01/23 22:05:28 $
 * @created 27/11/04
 * @deprecated
 */
public class CodeCentralJBuilder extends Repository
{
//  private static final String CC_BASE = "http://cc.borland.com/ccweb.exe/";
//  private static final String CC_OT_LIST_URL = CC_BASE + "prodcat?prodid=3&catid=11";
  private static final String CC_BASE = "http://codecentral.borland.com/";
  private static final String CC_OT_LIST_URL = CC_BASE + "ProdCat.aspx?prodid=3&catid=11";

  private Product product = null;
  private Checker checker = null;

  private CodeCentralOTListLoader lstLoader = null;

  private CodeCentralJBuilder()
  {
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    RepositoryManager.registerRepository(new CodeCentralJBuilder());
  }


  public String getID()
  {
    return "net.java.dev.jbotm.repo.cc.CodeCentralJBuilder";
  }

  public String getName()
  {
    return "CodeCentral (JBuilder) (Web -- Deprecated)";
  }

  public String getProduct()
  {
    return "net.java.dev.jbotm.products.JBuilder";
  }

  public void check() throws Exception
  {
    if (true) return;
    lstLoader = new CodeCentralOTListLoader(this);
    lstLoader.load();
  }

  public List getEntries()
  {
    ArrayList lst = new ArrayList(0);
    if (lstLoader != null)
    {
      lst = new ArrayList(lstLoader.getEntries());
    }
    return lst;
  }

  public void download(OpenToolEntry entry) throws OTMException
  {
    CodeCentralOTDownloader d = new CodeCentralOTDownloader(entry);
    try
    {
      d.load();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      throw new OTMException(ex.getMessage(), ex);
    }
  }

  public void updatePropertiesFor(OpenToolEntry entry) throws OTMException
  {
    if (product == null) product = ProductManager.getProduct(getProduct());
    if (checker == null) checker = product.getChecker();
    try
    {
      CodeCentralOTEntryPageLoader d = new CodeCentralOTEntryPageLoader(entry);
      d.load();
      entry.setFullyLoaded();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      throw new OTMException(ex.getMessage(), ex);
    }
  }

  String getOTListURL()
  {
    return CC_OT_LIST_URL;
  }

  public OpenToolEntry createEntry()
  {
    return new CodeCentralEntry();
  }

}
