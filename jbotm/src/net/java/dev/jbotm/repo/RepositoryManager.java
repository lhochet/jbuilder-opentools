/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo;

import java.util.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:07:03 $
 * @created 27/11/04
 */
public class RepositoryManager
{
  private static HashMap repositories = new HashMap();

  private RepositoryManager()
  {
  }

  public static void registerRepository(Repository repository)
  {
    repositories.put(repository.getID(), repository);
  }

  public static List getRepositories()
  {
    return new ArrayList(repositories.values());
  }

  public static Repository getRepository(String id)
  {
    return (Repository)repositories.get(id);
  }
}
