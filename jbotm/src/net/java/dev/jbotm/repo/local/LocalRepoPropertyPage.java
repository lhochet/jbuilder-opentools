/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.local;

import com.borland.primetime.help.*;
import com.borland.primetime.properties.*;
import javax.swing.JLabel;
import java.awt.Rectangle;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.ide.Browser;
import com.borland.primetime.vfs.ui.UrlChooser;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/03/05 21:57:29 $
 * @created 20/02/05
 */
public class LocalRepoPropertyPage extends PropertyPage
{
  public LocalRepoPropertyPage()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public HelpTopic getHelpTopic()
  {
    return null;
  }

  public void readProperties()
  {
    tfPath.setText(LocalRepoPropertyGroup.REPO_PATH.getValue());
  }

  public void writeProperties()
  {
    LocalRepoPropertyGroup.REPO_PATH.setValue(tfPath.getText());
  }

  private void jbInit() throws Exception
  {
    jLabel1.setText("Path:");
    tfPath.setColumns(20);
    btnBrowse.setText("...");
    btnBrowse.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnBtnBrowse_actionPerformed(e);
      }
    });
    this.setLayout(borderLayout1);
    jPanel1.setLayout(gridBagLayout1);
    this.add(jPanel1, java.awt.BorderLayout.NORTH);
    jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                new Insets(0, 0, 0, 0), 0, 10));
    jPanel1.add(tfPath, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
                                               , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
                                               new Insets(1, 2, 1, 2), 0, 0));
    jPanel1.add(btnBrowse, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                  new Insets(2, 0, 2, 0), 0, 0));
  }

  void btnBtnBrowse_actionPerformed(ActionEvent e)
  {
    Url startdir = null;
    try
    {
      startdir = new Url(tfPath.getText());
    }
    catch (Exception ex)
    {
      // do nothing
    }

    Url url = UrlChooser.promptForDir(Browser.getActiveBrowser(),
                                      startdir
                                      /*null*/
                                      /*project.getUrl()*/
                                      , "Choose Local repository directory...");
    if (url != null)
    {
      tfPath.setText(url.getFullName());
    }
  }

  private JLabel jLabel1 = new JLabel();
  private JTextField tfPath = new JTextField();
  private JButton btnBrowse = new JButton();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel jPanel1 = new JPanel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
}
