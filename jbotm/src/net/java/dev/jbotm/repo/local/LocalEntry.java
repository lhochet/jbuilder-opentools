
/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.local;

import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.OTMException;
import java.io.*;
import java.util.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/23 22:05:02 $
 * @created 18/01/05
 */
public class LocalEntry extends OpenToolEntry
{
  public LocalEntry()
  {
    setFullyLoaded();
  }

  public void install() throws OTMException
  {
    String dlpath = getProperty(LOCAL_NAME);
    if (dlpath == null)
    {
      throw new OTMException("File not downloaded");
    }
    File dlfile = new File(dlpath);
    if (!dlfile.exists())
    {
      throw new OTMException("File not downloaded");
    }

    // local entries only have 1 jar as files
    ArrayList files = getFiles();
    if (files.size() != 1)
    {
      throw new OTMException("No File to install");
    }

    FileEntry fe = (FileEntry)files.get(0);
    File destfile = new File(fe.extractPath);

    try
    {
      BufferedInputStream bis = new BufferedInputStream(new FileInputStream(dlfile));
      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destfile));
      int b;
      while ((b = bis.read()) != -1)
      {
        bos.write(b);
      }
      bis.close();
      bos.flush();
      bos.close();

    }
    catch (Exception ex)
    {
      // fail silently?
      ex.printStackTrace();
    }

  }

}
