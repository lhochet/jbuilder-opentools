package net.java.dev.jbotm.repo.local;

import java.util.*;

import net.java.dev.jbotm.repo.*;
import java.io.*;
import net.java.dev.jbotm.structs.*;
import net.java.dev.jbotm.OTMException;
import com.borland.primetime.util.*;
import java.text.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.vfs.*;

/**
 * <p>Title: CodeCentral</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2001-2004</p>
 *
 * <p>Company: </p>
 *
 * @author Ludovic HOCHET
 * @version $Revision: 1.6 $ $Date: 2005/06/15 23:16:41 $
 * @created 21/12/04
 */
public class LocalRepository extends Repository
{
  private java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd H:mm:ss");

  private LocalRepository()
  {
    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    RepositoryManager.registerRepository(new LocalRepository());
    PropertyManager.registerPropertyGroup(new LocalRepoPropertyGroup());
  }


  /**
   * check
   *
   * @throws Exception
   * @todo Implement this net.java.dev.jbotm.repo.Repository method
   */
  public void check() throws Exception
  {
  }

  /**
   * getEntries
   *
   * @return List
   * @todo Implement this net.java.dev.jbotm.repo.Repository method
   */
  public List getEntries()
  {
    ArrayList lst = new ArrayList();

//    if (true) return lst; // disable this class

    try
    {
      Url install = PropertyManager.getInstallRootUrl();
      Url libext = install.getRelativeUrl("lib/ext");

      String path = System.getProperty("lh.temp.repo_path");
      if (path == null)
      {
        path = LocalRepoPropertyGroup.REPO_PATH.getValue();
      }

      if (path != null && path.length() > 0 && new File(path).exists())
      {
        File repo = new File(path);
        File[] ots = repo.listFiles(new FilenameFilter()
        {
          public boolean accept(File dir, String name)
          {
            return name.endsWith(".jar");
          }
        });

        for (int i = 0; i < ots.length; i++)
        {
          System.out.println("ots[" + i + "]=" + ots[i]);

          String fname = ots[i].getName();
          String fpname = fname.substring(0, fname.lastIndexOf(".") + 1);
          File propf = new File(repo, fpname + "properties");

          if (propf.exists())
          {
            Properties props = new Properties();
            props.load(new FileInputStream(propf));

            OpenToolEntry entry = new OpenToolEntry();

            entry.setID(props.getProperty("id"));
            entry.setIdInRepository("");// + i);
            entry.setRepository(this);

            entry.setProperty(OpenToolEntry.LOCAL_NAME, ots[i].getAbsolutePath());
            entry.setTitle(props.getProperty("title"));
            entry.setShortDescription(props.getProperty("shortdesc"));
            entry.setDescription(props.getProperty("desc"));
            entry.setAuthorName(props.getProperty("author"));
            long date = ots[i].lastModified();
            date /= 1000;
            date *= 1000;
            entry.setLastModifiedDate(new Date(date));
            entry.setRunMinVersion(new Version(props.getProperty("min_version")));
            entry.setRunMaxVersion(new Version(props.getProperty("max_version")));

            entry.setProperty(OpenToolEntry.SIZE, "" + ots[i].length() + " bytes");

            FileEntry fe = new FileEntry();
            fe.name = ots[i].getName();
            fe.size = "" + ots[i].length() + " bytes";
            fe.date = sdf.format(new Date(ots[i].lastModified()));
            fe.extractPath = libext.getRelativeUrl(fe.name).getFullName();
//            fe.autoExtract = true;
            ArrayList files = new ArrayList(1);
            files.add(fe);
            entry.setFiles(files);

//            entry.setNew(true);

            String vrs = props.getProperty("version");
            if (vrs != null)
            {
              entry.setVersion(new Version(vrs));
            }

            lst.add(entry);
          }
        }

      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
    return lst;
  }

  /**
   * getID
   *
   * @return String
   * @todo Implement this net.java.dev.jbotm.repo.Repository method
   */
  public String getID()
  {
    return "net.java.dev.jbotm.repo.local.LocalRepository";
  }

  /**
   * getName
   *
   * @return String
   * @todo Implement this net.java.dev.jbotm.repo.Repository method
   */
  public String getName()
  {
    return "Local";
  }

  /**
   * getProduct
   *
   * @return String
   * @todo Implement this net.java.dev.jbotm.repo.Repository method
   */
  public String getProduct()
  {
    return "net.java.dev.jbotm.products.JBuilder";
  }

  public void download(OpenToolEntry entry) throws OTMException
  {
    // do nothing as the local name is the location of the downloaded entry
    // just update the last downloaded date
    entry.setProperty(OpenToolEntry.LAST_DOWNLOADED, sdf.format(new java.util.Date()));
  }

  public void updatePropertiesFor(OpenToolEntry entry) throws OTMException
  {
    // nothing to update, all props are set when retrieving the list
  }

  public OpenToolEntry createEntry()
  {
    return new LocalEntry();
  }
}
