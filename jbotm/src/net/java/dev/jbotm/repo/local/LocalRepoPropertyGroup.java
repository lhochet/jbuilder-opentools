/*
   Copyright: Copyright (c) 2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.local;

import com.borland.primetime.properties.*;
import net.java.dev.jbotm.ui.ProductPropertyPageFactory;
import net.java.dev.jbotm.ui.CodeCentralPropertyPage;
import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/03/05 21:57:21 $
 * @created 20/02/05
 */
public class LocalRepoPropertyGroup implements PropertyGroup
{
  public static final GlobalProperty REPO_PATH = new GlobalProperty(OTMPropertyGroup.OTM_CAT, "local_repo_path");

  public LocalRepoPropertyGroup()
  {
  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (topic == ProductPropertyPageFactory.PRODUCTS_TOPIC)
    {
      return new PropertyPageFactory("Local Repository")
      {
        public PropertyPage createPropertyPage()
        {
          return new LocalRepoPropertyPage();
        }
      };
    }
    return null; // else not our business
  }

  /**
   * not used
   */
  public void initializeProperties()
  {
  }
}
