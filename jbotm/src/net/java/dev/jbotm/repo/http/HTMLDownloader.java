/*
   Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.http;

import java.io.*;


/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2004/10/27 20:41:46 $
 * @created 26/12/03
 */
public abstract class HTMLDownloader extends Downloader
{
  protected String stripStart(String line)
  {
    if (line == null)
    {
      return null;
    }
    return line.substring(line.indexOf('>') + 1);
  }

  protected String stripEnd(String line)
  {
    if (line == null)
    {
      return null;
    }
    if (line.equals(""))
    {
      return null;
    }
    return line.substring(0, line.indexOf('<')).trim();
  }

  protected void handleContent(BufferedInputStream bis, int csz)
  {
    handleHTMLContent(new BufferedReader(new InputStreamReader(bis)));
  }

  protected abstract void handleHTMLContent(BufferedReader reader);

}
