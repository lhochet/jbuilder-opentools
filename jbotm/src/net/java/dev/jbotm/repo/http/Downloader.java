/*
   Copyright: Copyright (c) 2003-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.http;

import java.io.*;
import java.net.*;
import java.util.logging.*;

import com.borland.primetime.ide.*;
import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.3 $ $Date: 2004/11/24 15:56:37 $
 * @created 26/12/03
 */
public abstract class Downloader implements Downloadable
{
  protected Logger log = Logger.getLogger(Downloader.class.getName());

  public void load() throws Exception
  {
    if (!preLoadChecks())
    {
      return;
    }

    try
    {
      boolean needcookie = needCookie();

      String cookieText = null;
      if (needcookie)
      {
        cookieText = CookieManager.getCookieManager().getCookie(getDownloadURL());
        if ((cookieText == null) || (cookieText.equals("")))
        {
          String nocookie = getNocookieText();
          log.warning(nocookie);
          Browser.getActiveBrowser().getStatusView().setText(nocookie, StatusView.TYPE_ERROR);
          return; // abort
        }
      }

      URL downloadURL = new URL(getDownloadURL());
      log.finest("downloadURL = " + downloadURL);

      URLConnection connection = null; // connection when no need for special proxy
      Socket socket = null; // socket for handmade proxy
      BufferedInputStream bis = null;
      int csz = 0;

      String proxyHost = System.getProperty("http.proxyHost");
      String forceProxy = System.getProperty("lh.codecentral.forceproxy");
      if ((proxyHost != null) && (forceProxy != null))
      {
        int port = 80;
        try
        {
          port = Integer.parseInt(System.getProperty("http.proxyPort", "80"));
        }
        catch (NumberFormatException ex)
        {
        }

        String authentication = System.getProperty("http.proxyUser") + ":" +
          System.getProperty("http.proxyPassword");

        String ctntlen = null;

        boolean redirect = true;
        while (redirect)
        {

          redirect = false;

          socket = new Socket(proxyHost, port);

          Writer writer = new OutputStreamWriter(socket.getOutputStream(), "US-ASCII");
          writer.write("GET " + downloadURL.toExternalForm() + " HTTP/1.0\r\n");
          writer.write("Host: " + downloadURL.getHost() + "\r\n");
          writer.write("Proxy-Authorization: Basic "
                       + new sun.misc.BASE64Encoder().encode(
            authentication.getBytes())
                       + "\r\n");
          if (needcookie)
          {
            writer.write("Cookie: " + cookieText + "\r\n");
          }
          writer.write("\r\n");
          writer.flush();

          BufferedReader dis = new BufferedReader(new InputStreamReader(socket.getInputStream()));

          String line = dis.readLine();
//          System.out.println("proxy first line: " + line);
          if (line != null && line.startsWith("HTTP/"))
          {
            int sp = line.indexOf(' ');
            String status = line.substring(sp + 1, sp + 4);
            if (status.equals("200"))
            {
//              System.out.println("status OK");
              while (((line = dis.readLine()) != null) && (!line.equals("")))
              {
//                System.out.println("ln@ " + line);
                if (line.startsWith("Content-Type: text/html"))
                {
                  throw new IOException("Incorrect mime type. HTML returned... not file");
                }
                if (line.startsWith("Content-Length: "))
                {
                  ctntlen = line.substring("Content-Length: ".length());
                }
              }
            }
            else if ((status.equals("301")) || (status.equals("302")))
            {
              String location = null;
              while (((line = dis.readLine()) != null) && (!line.equals("")))
              {
//                System.out.println("ln@ " + line);
                if (line.startsWith("Location: "))
                {
                  location = line.substring("Location: ".length());
//                  System.out.println("location=" + location);
                  break;
                }
              }
              if (location == null)
              {
                throw new FileNotFoundException("Redirection url not found for status " + status);
              }
              redirect = true;
              writer.close();
              socket.close();
              socket = null;
              downloadURL = new URL(location);
              continue;

            }
            else
            {
              throw new FileNotFoundException("Host reports error " + status);
            }
          }
          else
          {
            throw new IOException("Bad protocol");
          }

        } //while

        csz = 2048;
        try
        {
          csz = Integer.parseInt(ctntlen);
        }
        catch (NumberFormatException ex1)
        {
          //ignore
        }
        ++csz;
        /*BufferedInputStream*/
        bis = new BufferedInputStream(socket.getInputStream(), csz);
      }
      else // original
      {
        /*URLConnection*/
        connection = downloadURL.openConnection();
        if (needcookie)
        {
          connection.setRequestProperty("Cookie", cookieText);
        }
        connection.connect();

        csz = connection.getContentLength();
        log.finest("csz = " + csz);
        csz = csz < 1 ? 2048 : connection.getContentLength();
        ++csz;
        /*BufferedInputStream */
        bis = new BufferedInputStream(connection.getInputStream(), csz);
      }

      handleContent(bis, csz);

    }
    catch (Exception ex)
    {
      Browser.getActiveBrowser().getStatusView().setText(getDownloadErrorText(), StatusView.TYPE_ERROR);
      ex.printStackTrace();
      log.throwing("Dowloader", "load()", ex);
      throw ex;
    }
  }

  protected abstract String getDownloadErrorText();

  protected abstract void handleContent(BufferedInputStream bis, int csz);

  protected abstract String getNocookieText();

  protected abstract String getDownloadURL();

  protected abstract boolean preLoadChecks();

  /**
   * Return true if the Borland cookie is needed to download. Currently always the case except to get the list of OTs
   * @return boolean True if the Borland cookie is needed
   */
  protected boolean needCookie()
  {
    return true;
  }

}
