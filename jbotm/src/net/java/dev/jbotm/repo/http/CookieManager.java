/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.repo.http;

import java.io.*;
import java.net.*;
import java.util.logging.*;
import javax.net.ssl.*;

import com.borland.primetime.help.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.ui.*;
import net.java.dev.jbotm.*;
import net.java.dev.jbotm.ui.*;
import net.java.dev.jbotm.repo.cc.CodeCentralPropertyGroup;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005/03/05 21:58:10 $
 * @created 24/09/01
 */
public class CookieManager implements DialogValidator
{
  Logger log = Logger.getLogger(CookieManager.class.getName());

  // singleton
  private static CookieManager manager = null;

  // keep cookie in memory
  String cookieValue = null;

  public static CookieManager getCookieManager()
  {
    if (manager == null)
    {
      manager = new CookieManager();
    }
    return manager;
  }

  // class

  private final String cookieName = "USER";
  private final String cookiePath = ";Path=/";
  private final String cookieBase1 = "[|]";
  private final String cookieBase2 = "[|][|][|]";
  private final String cookieBase3 = "[|]EN[|]90[|]1[|]ISO8859_1";

//  private final String cookieBase = "[|]username[|]firstname[|]name[|]email[|]EN[|]90[|]1[|]ISO8859_1";
//  private final String communityLogin = "https://community.borland.com/cgi-bin/login/login.cgi?";
  private final String communityLogin = "https://bdn.borland.com/cgi-bin/login/login.cgi"; //?";

  private String uid = "";
  private String email = "";
  private String password = "";
  private boolean saveCookie = false;
  private LoginPanel panel = null;

  private CookieManager()
  {
  }

  public String getCookie(String redirect) throws InvalidCookieException
  {
    log.finest("CookieManager.getCookie(" + redirect + ")");
    log.finest("cookie value 1 = " + cookieValue);
    if ((cookieValue != null) && (cookieValue.equals("")))
    {
      cookieValue = null;
    }
    if (cookieValue == null)
    {
      cookieValue = CodeCentralPropertyGroup.COOKIE.getValue();
      log.finest("cookie value = " + cookieValue);
//    System.out.println("cookie val = "  + cookieValue);
    }
    if (cookieValue == null)
    {
      ZipHelpTopic topic = new ZipHelpTopic(null, getClass().getResource("/doc/logindialog.html").toString());
      panel = new LoginPanel();
      if (DefaultDialog.showModalDialog(Browser.getActiveBrowser(), "CodeCentral Login", panel,
                                        topic, this))
      {
        uid = panel.getUID();
        email = panel.getEmail();
        password = panel.getPassword();
        saveCookie = panel.isSaveCookie();

        try
        {
          cookieValue = doGetCookie(redirect);
        }
        catch (InvalidCookieException ex)
        {
          if (com.borland.primetime.PrimeTime.isVerbose())
          {
            log.warning("Error could not get cookie value...");
          }
          Browser.getActiveBrowser().getStatusView().setText("Error could not get cookie value...", java.awt.Color.red);
          throw ex;
        }

      }
      else
      {
        // cookie retrival canceled
        if (com.borland.primetime.PrimeTime.isVerbose())
        {
          log.warning("Cookie retrieval canceled.");
        }
        throw new InvalidCookieException();
      }

    }
    if (saveCookie)
    {
      CodeCentralPropertyGroup.COOKIE.setValue(cookieValue);
    }
    return cookieValue;
  }

  private String doGetCookie(String redirect) throws InvalidCookieException
  {
    try
    {
      log.entering("CookieManager", "doGetCookie");

      URL url = new URL(communityLogin);
      String postString = "edLoginName=" + uid + "&edEmail=" + email + "&edPassword=" + password +
        "&encode=ISO8859_1&edSaveCookie=90&btnSubmit=Login&btnReset=Reset&redirect=" + redirect;
      String cookie = null;

      URLConnection connection = null; // connection when no need for special proxy
//      Socket socket = null; // socket for handmade proxy
      BufferedReader reader = null;

      String proxyHost = System.getProperty("http.proxyHost");
      String forceProxy = System.getProperty("lh.codecentral.forceproxy");
      if ((proxyHost != null) && (forceProxy != null))
      {
        int port = 80;
        try
        {
          port = Integer.parseInt(System.getProperty("http.proxyPort", "80"));
        }
        catch (NumberFormatException ex)
        {
        }

        String authentication = System.getProperty("http.proxyUser") + ":" +
          System.getProperty("http.proxyPassword");

        /*******************************/
        SSLSocketFactory factory =
          (SSLSocketFactory)SSLSocketFactory.getDefault();

        /*
         * Set up a socket to do tunneling through the proxy.
         * Start it off as a regular socket, then layer SSL
         * over the top of it.
         */
//                 proxyHost = System.getProperty("http.proxyHost"); //should be https.proxyHost
//                 port = Integer.getInteger("http.proxyPort").intValue();

        Socket tunnel = new Socket(proxyHost, port);
        Writer writer = new OutputStreamWriter(tunnel.getOutputStream(), "US-ASCII");
        writer.write("CONNECT " + url.getHost() + ":" + url.getPort() + " HTTP/1.0\r\n");
        writer.write("Host: " + url.getHost() + "\r\n");
//        writer.write("Port: 563\r\n");
        writer.write("Proxy-Authorization: Basic "
                     + new sun.misc.BASE64Encoder().encode(
          authentication.getBytes())
                     + "\r\n"
                     + "User-Agent: "
                     + sun.net.www.protocol.http.HttpURLConnection.userAgent
                     + "\r\n\r\n");

        /*
         * Ok, let's overlay the tunnel socket with SSL.
         */
        SSLSocket socket =
          (SSLSocket)factory.createSocket(tunnel, proxyHost, port, true);

        /*
         * register a callback for handshaking completion event
         */
        socket.addHandshakeCompletedListener(
          new HandshakeCompletedListener()
        {
          public void handshakeCompleted(
            HandshakeCompletedEvent event)
          {
//            System.out.println("Handshake finished!");
//            System.out.println(
//              "\t CipherSuite:" + event.getCipherSuite());
//            System.out.println(
//              "\t SessionId " + event.getSession());
//            System.out.println(
//              "\t PeerHost " + event.getSession().getPeerHost());
          }
        }
        );

        /*
         * send http request
         *
         * See SSLSocketClient.java for more information about why
         * there is a forced handshake here when using PrintWriters.
         */
        socket.startHandshake();

        /*******************************/
//        socket = new Socket(proxyHost, port);

        /*Writer*/
        writer = new OutputStreamWriter(socket.getOutputStream(), "US-ASCII");
        writer.write("POST " + url.toExternalForm() + " HTTP/1.0\r\n");
        writer.write("Host: " + url.getHost() + "\r\n");
        writer.write("Proxy-Authorization: Basic "
                     + new sun.misc.BASE64Encoder().encode(
          authentication.getBytes())
                     + "\r\n");
//                     + "\r\n\r\n");
        writer.write("Content-Type: application/x-www-form-urlencoded\r\n");
        writer.write("Content-Length: " + Integer.toString(postString.length()) + "\r\n\r\n");
//        writer.write("Content-Length: " + Integer.toString(postString.length()) + "\r\n");

        byte[] bytes = postString.getBytes();
        for (int i = 0; i < bytes.length; i++)
        {
          writer.write(bytes[i]);
        }

//        writer.write("\r\n\r\n");
        writer.flush();
//        writer.close();

        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()), 128000);

        String line = reader.readLine();
//        System.out.println("cookie man proxy first line: " + line);
        if (line != null && line.startsWith("HTTP/"))
        {
          int sp = line.indexOf(' ');
          String status = line.substring(sp + 1, sp + 4);
//          if (status.equals("200"))
//          {
//              System.out.println("status OK");
//          }
//          else
//          {
//            throw new FileNotFoundException("Host reports error " + status);
//          }

//          System.out.println("line: " + line);
          while (((line = reader.readLine()) != null) && (!line.equals("")))
          {
//            System.out.println("line: " + line);
            if (line.startsWith("Set-Cookie:"))
            {
//              System.out.println("cookie found");

              String startstr = "Set-Cookie: ";
              cookie = line.substring(startstr.length(), line.length() - 2);
              break;
            }
          }
          ;

        }
        else
        {
          throw new IOException("Bad protocol");
        }
//          }
//          else // original
//          {
//            /*BufferedReader*/reader = new BufferedReader(new InputStreamReader(url.openStream()));
//          }

//        }
      }
      else // original
      {
        /*URLConnection*/
        connection = url.openConnection();
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        ((HttpURLConnection)connection).setRequestMethod("POST");
        ((HttpURLConnection)connection).setFollowRedirects(false);
        ((HttpURLConnection)connection).setInstanceFollowRedirects(false);

        String contentLength = Integer.toString(postString.length());
        connection.setRequestProperty("Content-Length", contentLength);

        connection.connect();

        OutputStream out = new BufferedOutputStream(connection.getOutputStream());

        byte[] bytes = postString.getBytes();
        for (int i = 0; i < bytes.length; i++)
        {
          out.write(bytes[i]);
        }

        out.flush();
        out.close();

        cookie = connection.getHeaderField("Set-Cookie");
        log.finest("Set-Cookie field = " + cookie);

      } //EO original


      if (cookie != null)
      {
        int i = cookie.indexOf(';');
        String tmp = cookie.substring(4, i);
        cookie = "USER=" + tmp + cookieBase1 + uid + cookieBase2 + email + cookieBase3 +
          cookie.substring(i, cookie.length());
        log.finest("Cookie = " + cookie);
      }

      if (cookie == null)
      {
        throw new InvalidCookieException();
      }
      return cookie;
    }
    catch (InvalidCookieException ex1)
    {
      ex1.printStackTrace();
      throw ex1;
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      throw new InvalidCookieException(ex);
    }
  }

  public boolean validateDialog()
  {
    if (panel == null)
    {
      return false;
    }
    String uid = panel.getUID();
    String email = panel.getEmail();
    return (!((uid == null) || (uid.equals("")) || (email == null) || (email.equals(""))));
  }
}
