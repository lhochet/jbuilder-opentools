/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm;

import com.borland.jbuilder.info.*;
import com.borland.primetime.properties.*;
import com.borland.primetime.util.*;
import net.java.dev.jbotm.ui.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/03/05 21:56:56 $
 * 0.4, 7/03/02
 * 0.3, 7/12/01
 * 0.2, 24/09/01
 * 0.1, 13/08/01
 */
public class OTMPropertyGroup implements PropertyGroup
{
  public static final String OTM_CAT = "net.java.dev.jbotm";

  // sort order
  /** @todo sort is done by JXTable, so mb this can be removed? */
  public static final boolean ASC = true;
  public static final boolean DESC = false;

  // sort on
  /** @todo sort is done by JXTable, so mb this can be removed? */
  public static final String DATE = "date";
  public static final String AUTHOR = "author";
  public static final String TITLE = "title";
  public static final String ID = "id";
  public static final String DOWNLOAD = "download";

  // filter option
  public static final int NO_FILTER = 0;
  public static final int ID_FILTER = 1;
  public static final int AUTHOR_FILTER = 2;
  public static final int THIS_AUTHOR_FILTER = 3;
  public static final int KEYWORD_FILTER = 4;

  // consts for showing message tab
  public static final int ALWAYS_SHOW = 0;
  public static final int ONLY_IF_MESSAGEVIEW_SHOWN = 1;
  public static final int NOT_IF_AUTOCHECKING = 2;
  public static final int NEVER_SHOW = 3;

  // versions
//  public static final Version MIN_VERSION = new Version(0, 0);
//  public static final Version MAX_VERSION = new Version(99, 99);

  // global properties
  //

  // if no new or updated OT
  public static final GlobalIntegerProperty IF_NEW_UPDATED_ONLY = new GlobalIntegerProperty(OTM_CAT, "ifnewupdatedonly",
    ALWAYS_SHOW);

  // auto
  // check
  public static final GlobalBooleanProperty AUTO_CHECK = new GlobalBooleanProperty(OTM_CAT, "autocheck", false);
  public static final GlobalBooleanProperty AUTO_CHECK_ONCE_A_DAY = new GlobalBooleanProperty(OTM_CAT, "autocheck_once_a_day", false);
  public static final GlobalProperty LAST_AUTO_CHECK = new GlobalProperty(OTM_CAT, "last_autocheck");
  public static boolean AUTO_CHECKING = false;

  // sort
  /** @todo remove? as sorting is done by JXTable? */
  public static final GlobalProperty SORT_BY = new GlobalProperty(OTM_CAT, "sort_by", DATE);
  public static final GlobalBooleanProperty SORT_DIRECTION = new GlobalBooleanProperty(OTM_CAT, "sort_direction", DESC);

  // show extra info
  public static final GlobalBooleanProperty SHOW_DOWNLOADS = new GlobalBooleanProperty(OTM_CAT, "show_downloads", false);
  public static final GlobalBooleanProperty SHOW_DOWNLOAD_CHANGES = new GlobalBooleanProperty(OTM_CAT, "show_download_changes", false);


  // filter OT JB versions
  public static final GlobalBooleanProperty FILTER_JB_VERSIONS = new GlobalBooleanProperty(OTM_CAT, "filter_jb_versions", false);
  public static final Version JB_VERSION = new Version(JBuilderInfo.getBuildNumber());
  public static final GlobalBooleanProperty FILTER_JB_THIS_VERSION = new GlobalBooleanProperty(OTM_CAT, "filter_jb_this_version", true);
  public static final GlobalProperty FILTER_JB_MIN_VERSION = new GlobalProperty(OTM_CAT, "filter_jb_min_version", JBuilderInfo.getBuildNumber());

  /**
   * not used.
   */
  public void initializeProperties()
  {
  }

  public PropertyPageFactory getPageFactory(Object topic)
  {
//    if (topic == null)
    if (topic == net.java.dev.jbuilder.opentools.community.CommunityPropertyGroup.COMMUNITY_TOPIC)
    {
      return OTMPropertyPageFactory.getInstance();
    }
    return null; // else not our business
  }
}
