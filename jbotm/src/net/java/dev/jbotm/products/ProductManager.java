/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.products;

import java.util.*;
import java.util.logging.*;

import net.java.dev.jbotm.*;
import net.java.dev.jbotm.view.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:06:47 $
 * @created 4/11/04
 */
public class ProductManager
{
  private static Logger log = Logger.getLogger(ProductManager.class.getName());

  private static ArrayList products = new ArrayList();

  private ProductManager()
  {
  }

  public static void registerProduct(Product product)
  {
    products.add(product);
  }

  public static List getProducts()
  {
    return products;
  }

  public static void check()
  {
    Iterator iter = products.iterator();
    while (iter.hasNext())
    {
      Product product = (Product) iter.next();
      if (product.isChecked())
      {
        log.fine(product.getName() + " will be checked");
        Checker checker = product.getChecker();
        checker.run();
        SummaryUI summary = (SummaryUI)ViewManager.createView(ViewManager.SUMMARY_VIEW, product);
        summary.setNewOTsList(checker.getNewOpenTools());
        summary.setUpdatedOTsList(checker.getUpdatedOpenTools());
        summary.setNewlyCommentedOTsList(checker.getNewlyCommentedOpenTools());
        summary.setTrackedOTsList(checker.getTrackedOpenTools());
        ViewManager.show(summary);
      }
    }
  }

  public static void showOTs(Product product)
  {
    Checker checker = product.getChecker();
    OTListUI otlist = (OTListUI)ViewManager.createView(ViewManager.OTLIST_VIEW, product);
    ViewManager.show(otlist);
    checker.runLocal();
    otlist.setOTList(checker.getOpenTools());
  }

  public static Product getProduct(String id)
  {
    if (id == null) return null;
    if (id.equals("")) return null;

    Iterator iter = products.iterator();
    while (iter.hasNext())
    {
      Product product = (Product) iter.next();
      if (id.equals(product.getID())) return product;
    }
    return null;
  }
}
