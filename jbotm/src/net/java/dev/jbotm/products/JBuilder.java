/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.products;

import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.3 $ $Date: 2005/03/05 21:58:28 $
 * @created 4/11/04
 */
public class JBuilder extends Product
{
  private static final JBuilder instance = new JBuilder();
  public static final JBuilder getInstance()
  {
    return instance;
  }

  private JBuilder() {
  }

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    JBuilder jb = JBuilder.getInstance();
    ProductManager.registerProduct(jb);
    net.java.dev.jbuilder.opentools.community.Community.GROUP_Community.add(new OpenToolManager.ShowOTs(jb));
  }

  public String getName()
  {
    return "JBuilder";
  }

  public String getID()
  {
    return "net.java.dev.jbotm.products.JBuilder";
  }

  protected String getLocalFileName()
  {
    return "community/cc/jbuilder.xml";
  }

  protected Checker createChecker()
  {
    return new Checker(this);
  }

}
