/*
   Copyright: Copyright (c) 2001-2004 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package net.java.dev.jbotm.products;

import net.java.dev.jbotm.*;

/**
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005/01/09 22:06:39 $
 * @created 4/11/04
 */
public abstract class Product
{
  private boolean checked = true;
  private XMLOTListFileManager filemanager = null;
  private Checker checker = null;

  public abstract String getName();
  public abstract String getID();
  protected abstract String getLocalFileName();
  protected abstract Checker createChecker();

  public void setChecked(boolean checked)
  {
    this.checked = checked;
  }
  public boolean isChecked()
  {
    return checked;
  }

  public XMLOTListFileManager getFileManager()
  {
    if (filemanager == null) filemanager = new XMLOTListFileManager(getLocalFileName());
    return filemanager;
  }

  public Checker getChecker()
  {
    if (checker == null) checker = createChecker();
    return checker;
  }
}
