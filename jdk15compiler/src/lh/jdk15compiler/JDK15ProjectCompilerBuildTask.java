package lh.jdk15compiler;

import com.borland.primetime.build.*;
import com.borland.jbuilder.build.*;

/**
 * <p>Title: JB JDK 1.5 Compiler Support</p>
 * <p>Description: Override the JB ProjectJavacBuildTask to change the -target argument
 * to target JDK 1.5 code and add the -source argument to mark the source code as JDK 1.5 code
 * and -Xlint:unchecked to get warnings about potential problems of code compatibility</p>
 * <p>Copyright: Copyright (c) 2004 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.2, 3/03/04
 * @created 15/02/04
 */

public class JDK15ProjectCompilerBuildTask extends ProjectJavacBuildTask
{
  /**
   * Returns the aguments given to the javac "command line", overriden to mark
   * the code to compile as being JDK 1.5 code
   * @param buildProcess BuildProcess can be used to extract additional info about the build
   * @return String[] Arguments for the javac "command line" augmented of -source 1.5 and
   * -Xlint:unchecked to get warnings about potential problems of code compatibility and
   * with overriden -target 1.5
   */
  protected String[] getArgs(BuildProcess buildProcess)
  {
    final int OFFSET = 3;
    int targetindex = -1;
    String[] args = super.getArgs(buildProcess);
    String[] ret = new String[args.length + OFFSET];
    ret[0] = "-source";
    ret[1] = "1.5";
    ret[2] = "-Xlint:unchecked";

    for (int i = OFFSET; i < args.length + OFFSET; i++)
    {
      ret[i] = args[i - OFFSET];
      if (ret[i].equals("-target"))
      {
        targetindex = i + 1;
      }
    }
    if (targetindex > 0)
    {
      ret[targetindex] = "1.5";
    }

    return ret;
  }
}
