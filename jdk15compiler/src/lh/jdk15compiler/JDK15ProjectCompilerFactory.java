package lh.jdk15compiler;

import com.borland.primetime.*;
import com.borland.jbuilder.build.*;

/**
 * <p>Title: JB JDK 1.5 Compiler Support</p>
 * <p>Description: Factory used by JBuilder to create the JDK 1.5 javac build task and provde the appropriate UI to the user.<br>
 * This class is "very inspired" by Keith Wood JikesCompilerFactory.</p>
 * <p>Copyright: Copyright (c) 2004 Ludovic HOCHET</p>
 * <p>Company: </p>
 * @author Ludovic HOCHET
 * @version 0.1, 15/02/04
 */

public class JDK15ProjectCompilerFactory extends JavaCompilerFactory
{
  public static final String COMPILER_NAME = "JDK1.5 project Javac";
  public static final String COMPILER_KEY = "jdk15javac";

  /**
   * Initialise the OpenTool, in this case register the compiler factory with the compilers manager
   *
   * @param  majorVersion  the major version of the current OpenTools API
   * @param  minorVersion  the minor version of the current OpenTools API
   */
  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH JDK15ProjectCompilerFactory 0.1");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION +
                         "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." +
                         minorVersion);
    }
    JavaCompilerManager.registerJavaCompilerFactory(new JDK15ProjectCompilerFactory());
  }

  /**
   * This tells JB Java Compiler manager that javac does not support excluding class, obfuscation,
   * or directory synchronisation
   *
   * @return  false
   */
  public boolean supportsBmjExtensions()
  {
    return false;
  }

  /**
   * No idea where this is used, mb in the manager to quickly find the instance of the factory?
   * @return the key for this factory
   */
  public String getKey()
  {
    return COMPILER_KEY;
  }

  /**
   * Supply the name of the compiler for the Build property page.
   *
   * @return  the name of this compiler
   */
  public String getDisplayName()
  {
    return COMPILER_NAME;
  }

  /**
   * Return the class to be used to instanciate the Build Task that will compile the .java of the project
   *
   * @return  JDK15ProjectCompilerBuildTask.class
   */
  public Class getJavaCompilerClass()
  {
    return JDK15ProjectCompilerBuildTask.class;
  }

  /**
   * Tells that we want to be displayed only in standard JDKs
   *
   * @return true to indicate that this factory should only be shown for standard JDKs
   * (ie not J2ME JDKs), there is it seems no way to distinguish the version of
   * the standard JDK
   */
  public boolean requiresStandardJdk()
  {
    return true;
  }

}
