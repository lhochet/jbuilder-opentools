package net.java.dev.jbuilder.opentool.findfiles.opentools;

import junit.framework.*;
import java.util.Enumeration;

public class TestCategoryNode extends TestCase {

  public void testAddNode() {
    RootNode root = new RootNode();
    CategoryNode category = new CategoryNode("category", root);
    OpenToolNode node = new OpenToolNode("a.b.ClassName1", true, category);
    category.addNode(node);
    assertEquals(1, category.getChildCount());
    Enumeration enumeration = category.children();
    assertEquals(true, enumeration.hasMoreElements());
    assertSame(node, enumeration.nextElement());
    assertEquals(false, enumeration.hasMoreElements());
    assertEquals(false, category.getAllowsChildren());
    assertSame(node, category.getChildAt(0));
  }
}
