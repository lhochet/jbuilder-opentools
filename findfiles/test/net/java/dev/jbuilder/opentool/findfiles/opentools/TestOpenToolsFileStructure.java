package net.java.dev.jbuilder.opentool.findfiles.opentools;

import junit.framework.*;
import javax.swing.text.*;
import javax.swing.tree.TreeModel;
import javax.swing.JTree;
import javax.swing.tree.TreeNode;

public class TestOpenToolsFileStructure extends TestCase {
  private OpenToolsFileStructure structure = null;

  protected void setUp() throws Exception {
    super.setUp();
    structure = new OpenToolsFileStructure();
    structure.setTree(new JTree());
  }

  protected void tearDown() throws Exception {
    structure = null;
    super.tearDown();
  }

  public void testUpdateStructure() throws BadLocationException {
    Document document = new DefaultStyledDocument();
    document.insertString(0, "OpenTools-Category: p1.p2.ClassName -p1.p2.ClassName2\n", null);
    structure.updateStructure(document);
    TreeModel model = structure.getTree().getModel();
    assertNotNull(model);
    RootNode root = (RootNode) model.getRoot();
    assertEquals(1, root.getChildCount());
    CategoryNode node = (CategoryNode) root.getChildAt(0);
    assertEquals("Category", node.getCategory());
    assertEquals(2, node.getChildCount());
    OpenToolNode first = (OpenToolNode) node.getChildAt(0);
    assertEquals("p1.p2.ClassName", first.getClassName());
    assertEquals(true, first.isEnabled());
    OpenToolNode second = (OpenToolNode) node.getChildAt(1);
    assertEquals("p1.p2.ClassName2", second.getClassName());
    assertEquals(false, second.isEnabled());
  }

  //TODO Enhancement
  public void not_testUpdateStructureWithMoreThanOneSpaceBetween() throws Exception {
    Document document = new DefaultStyledDocument();
    document.insertString(0, "OpenTools-Category: p1.ClassOne   p2.ClassTwo", null);
    structure.updateStructure(document);
    TreeModel model = structure.getTree().getModel();
    RootNode root = (RootNode) model.getRoot();
    CategoryNode node = (CategoryNode) root.getChildAt(0);
    assertEquals(2, node.getChildCount());
  }

}
