package net.java.dev.jbuilder.opentool.findfiles;

import com.borland.primetime.vfs.ui.UrlChooser;
import net.java.dev.jbuilder.opentool.findfiles.urlchooser.SearchUrlChooserPageFactory;

public class SearchUrlChooser {
  public static void initOpenTool(byte majorVersion, byte minorVersion) {
    UrlChooser.registerUrlChooserPageFactory(new SearchUrlChooserPageFactory());
  }

}
