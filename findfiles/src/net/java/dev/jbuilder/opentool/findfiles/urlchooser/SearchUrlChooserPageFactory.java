package net.java.dev.jbuilder.opentool.findfiles.urlchooser;

import com.borland.primetime.vfs.ui.UrlChooserPageFactory;
import com.borland.primetime.vfs.ui.UrlChooserPage;
import com.borland.primetime.ui.ListSupplierPane;

public class SearchUrlChooserPageFactory extends UrlChooserPageFactory {
  public SearchUrlChooserPageFactory() {
    super("Search");
  }

  public UrlChooserPage createUrlChooserPage(int mode) {
    return new SearchUrlChooserPage();
  }
}
