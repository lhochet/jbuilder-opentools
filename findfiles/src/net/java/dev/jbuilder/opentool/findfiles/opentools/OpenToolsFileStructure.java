package net.java.dev.jbuilder.opentool.findfiles.opentools;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.tree.*;

import com.borland.primetime.node.*;
import java.io.InputStream;
import java.io.*;
import java.util.Properties;
import java.util.*;
import java.util.jar.Manifest;
import java.util.jar.Attributes;
import com.borland.jbuilder.ide.JBuilderIcons;
import com.borland.jbuilder.node.java.JavaStructureIcons;
import com.borland.primetime.ui.SearchTree;
import com.borland.primetime.insight.error.ErrorObject;
import com.borland.jbuilder.node.JBProject;
import com.borland.jbuilder.repository.Repository;
import com.borland.jbuilder.jam.JamFactory;
import com.borland.jbuilder.java.JavaNames;

public class OpenToolsFileStructure extends TextStructure {

  public void updateStructure(Document document) {
    SearchTree searchTree = ((SearchTree)tree);
    ArrayList expansionState = searchTree.getExpansionState();
    ArrayList selectionState = searchTree.getSelectionState();
    InputStream is = null;
    try {
      String content = document.getText(0, document.getLength());
      is = new ByteArrayInputStream(content.getBytes());
      RootNode root = new RootNode();
      CategoryNode[] model = createRootNodeForModel(is, root);
      treeModel.setRoot(root);
      searchTree.setSelectionState(selectionState);
      searchTree.setExpansionState(expansionState);
      ((TextFileNode)fileNode).fireNodeInfoChanged(model, findErrors(root));
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
    catch (BadLocationException ex) {
      ex.printStackTrace();
    }

    finally {
      if (is != null) {
        try {is.close();} catch (Exception e) {}
      }
    }
  }

  private ErrorObject[] findErrors(RootNode model) {
    JBProject project = (JBProject) fileNode.getProject();
    JamFactory factory = project.getJamFactory();

    ArrayList list = new ArrayList();
    for (Enumeration enumeration = model.children(); enumeration.hasMoreElements();) {
      CategoryNode categoryNode = (CategoryNode) enumeration.nextElement();
      for (Enumeration openToolEnumeration = categoryNode.children(); openToolEnumeration.hasMoreElements();) {
        OpenToolNode node = (OpenToolNode) openToolEnumeration.nextElement();
        String className = node.getClassName();
        if (!JavaNames.isValidClassName(className)) {
          list.add(new ErrorObject(1, 1, "NotValid"));
        }
        else if (factory.getClass(className) == null) {
          list.add(new ErrorObject(1, 1, "NotFound"));
        }
      }
    }
    return (ErrorObject[]) list.toArray(new ErrorObject[list.size()]);
  }

  private CategoryNode[] createRootNodeForModel(InputStream is, RootNode root) throws IOException {
    ArrayList list = new ArrayList();
    Manifest manifest = new Manifest(is);
    Attributes attributes = manifest.getMainAttributes();
    RootNode rootNode = new RootNode();
    for (Iterator iterator = attributes.entrySet().iterator(); iterator.hasNext(); ) {
      Map.Entry item = (Map.Entry) iterator.next();
      Attributes.Name name = (Attributes.Name) item.getKey();
      String key = name.toString();
      String header = "OpenTools-";
      if (key.startsWith(header)) {
        String category = key.substring(header.length());
        list.add(createCategoryNode(category, (String) item.getValue(), rootNode));
      }
    }
    return (CategoryNode[]) list.toArray(new CategoryNode[list.size()]);
  }

  private CategoryNode createCategoryNode(String category, String openToolList, RootNode parent) {
    CategoryNode categoryNode = new CategoryNode(category, parent);
    for(StringTokenizer tokenizer = new StringTokenizer(openToolList, " ");tokenizer.hasMoreTokens();) {
      String token = tokenizer.nextToken();
      boolean enabled = true;
      if (token.charAt(0) == '-') {
        enabled = false;
        token = token.substring(1);
      }
      OpenToolNode node = new OpenToolNode(token, enabled, categoryNode);
      categoryNode.addNode(node);
    }
    return categoryNode;
  }

  public Icon getStructureIcon(Object object) {
    if (object instanceof CategoryNode) {
      return JBuilderIcons.ICON_GEAR;
    }
    else if (object instanceof OpenToolNode) {
      return JavaStructureIcons.getClassIcon();
    }
    return null;
  }

  public void setTree(JTree tree) {
    super.setTree(tree);
    ((SearchTree)tree).setPreserveMode(SearchTree.PRESERVE_MODE_VALUE);
  }
}
