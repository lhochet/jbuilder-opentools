package net.java.dev.jbuilder.opentool.findfiles.search;
import java.util.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.ui.*;
import com.borland.primetime.ui.ListSupplierPane.*;
import com.borland.primetime.util.*;
import com.borland.primetime.vfs.*;

public class UrlListSupplier implements ListSupplierPane.ListSupplier {
    private ArrayList repository = new ArrayList();
    private Url prototype;
    private Scanner scanner;

    public UrlListSupplier(Browser browser) {
        createScanner(browser, findNavigationNodes(browser), false);
    }

    public UrlListSupplier(Browser browser, Node[] nodes) {
      createScanner(browser, nodes, true);
    }

  private void createScanner(Browser browser, Node[] nodes, boolean extensiveMode) {
    if (nodes == null || nodes.length == 0) {
      scanner = new ProjectDirectoryScanner(browser.getActiveProject());
    }
    else {
      scanner = new NavigationDirectoryScanner(nodes, extensiveMode);
    }
  }

  private Node[] findNavigationNodes(Browser browser) {
        Node[] candidates = browser.getActiveUserProject().getChildren();
        ArrayList result = new ArrayList(candidates.length);
        for (int i = 0; i < candidates.length; i++) {
            if (candidates[i] instanceof NavigationDirectoryNode) {
                result.add(candidates[i]);
            }
        }
        return (Node[]) result.toArray(new Node[result.size()]);
    }

    public List getList(String string) {
        if (!string.endsWith("*")) {
            string += "*";
        }
        RegularExpression expression = new RegularExpression(string, false);
        ArrayList result = new ArrayList(repository.size());
        for(int i = repository.size() - 1; i >= 0; i--) {
            Url url = (Url) repository.get(i);
            if (expression.exactMatch(url.getName())) {
                result.add(url);
                if (prototype == null ||
                        prototype.getFullName().length() < url.getFullName().length()) {
                    prototype = url;
                }
            }
        }
        return result;
    }

    public Object getPrototypeItem() {
        return prototype;
    }

    public void loadListRepository(ListLoadingListener listener) {
        listener.loadStarted();
        scan(listener);
    }

    private void scan(ListLoadingListener listener) {
      scanner.scann(listener);
    }

    private void add(ListLoadingListener listLoadingListener, Url url) {
      repository.add(url);
      listLoadingListener.listChanged(repository.size() + " loaded");
    }

    private static interface Scanner {
      public void scann(ListLoadingListener listener);
    }

    private class NavigationDirectoryScanner implements Scanner{
      private Node[] nodes;
      private boolean extensiveMode;

      public NavigationDirectoryScanner(Node[] nodes, boolean extensiveMode) {
        this.nodes = nodes;
        this.extensiveMode = extensiveMode;
      }

      public void scann(ListLoadingListener listener) {
        for (int i = 0; i < nodes.length; i++) {
            scan(listener, nodes[i]);
        }
      }

      private void scan(ListLoadingListener listLoadingListener, Node directory) {
          Node[] nodes = directory.getDisplayChildren();
          for(int i = 0; i < nodes.length; i++) {
              Node node = nodes[i];
              if (node instanceof NavigationDirectoryNode) {
                  scan(listLoadingListener, node);
              }
              else {
                if (node instanceof FileNode) {
                  FileNode fileNode = (FileNode) node;
                  add(listLoadingListener, fileNode.getUrl());
                }
                if (extensiveMode) {
                  scan(listLoadingListener, node);
                }
              }
          }
      }
  }

  private class ProjectDirectoryScanner implements Scanner {
    private Url directoryUrl;

    public ProjectDirectoryScanner(Project project) {
      this.directoryUrl = project.getUrl().getParent();
    }

    public void scann(ListLoadingListener listener) {
      scan(listener, Collections.singletonList(directoryUrl));
    }

    private void scan(ListLoadingListener listLoadingListener, List list) {
      while (!list.isEmpty()) {
        ArrayList nextLevel = new ArrayList();
        for (Iterator iterator = list.iterator(); iterator.hasNext();) {
          Url url = (Url) iterator.next();
          Url[] urls = VFS.getChildren(url, Filesystem.TYPE_BOTH);
          for (int i = 0; i < urls.length; i++) {
            Url oneUrl = urls[i];
            if (VFS.isDirectory(oneUrl)) {
              nextLevel.add(oneUrl);
            }
            else {
              add(listLoadingListener, oneUrl);
            }
          }
        }
        list = nextLevel;
      }
    }

  }
}
