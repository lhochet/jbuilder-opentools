package net.java.dev.jbuilder.opentool.findfiles.opentools;

import java.util.*;

import javax.swing.tree.*;

public class RootNode extends DefaultMutableTreeNode {
  private List categories = new ArrayList();
  public int getChildCount() {
    return categories.size();
  }

  public boolean getAllowsChildren() {
    return false;
  }

  public boolean isLeaf() {
    return false;
  }

  public Enumeration children() {
    return Collections.enumeration(categories);
  }

  public TreeNode getParent() {
    return null;
  }

  public TreeNode getChildAt(int childIndex) {
    return (TreeNode) categories.get(childIndex);
  }

  public int getIndex(TreeNode node) {
    return categories.indexOf(node);
  }

  public void addNode(CategoryNode category) {
    categories.add(category);
  }

  public String toString() {
    return "RootNode";
  }
}
