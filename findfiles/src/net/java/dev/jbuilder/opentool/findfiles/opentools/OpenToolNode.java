package net.java.dev.jbuilder.opentool.findfiles.opentools;

import java.util.*;

import javax.swing.tree.*;

public class OpenToolNode extends DefaultMutableTreeNode {
  private boolean enabled;
  private String className;
  private CategoryNode parent;
  private String shortClassName;

  public OpenToolNode(String className, boolean enabled, CategoryNode parent) {
    this.className = className;
    this.enabled = enabled;
    this.parent = parent;
    int index = className.lastIndexOf('.');
    shortClassName = index == -1 ? className : className.substring(index + 1);
  }

  public int getChildCount() {
    return 0;
  }

  public boolean getAllowsChildren() {
    return false;
  }

  public boolean isLeaf() {
    return true;
  }

  public Enumeration children() {
    return Collections.enumeration(Collections.EMPTY_LIST);
  }

  public TreeNode getParent() {
    return parent;
  }

  public TreeNode getChildAt(int childIndex) {
    return null;
  }

  public int getIndex(TreeNode node) {
    return -1;
  }

  public String getClassName() {
    return className;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public String toString() {
    return (isEnabled() ? "" : "-") + shortClassName ;
  }

}
