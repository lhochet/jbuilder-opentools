package net.java.dev.jbuilder.opentool.findfiles.urlchooser;

import com.borland.primetime.vfs.ui.*;
import com.borland.primetime.help.HelpTopic;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.ui.ListSupplierPane;
import java.awt.BorderLayout;
import com.borland.primetime.ui.ListPanel;
import net.java.dev.jbuilder.opentool.findfiles.search.UrlListSupplier;
import com.borland.primetime.ide.Browser;
import net.java.dev.jbuilder.opentool.findfiles.search.OpenFileFastAction;

public class SearchUrlChooserPage extends UrlChooserPage {
  private ListSupplierPane listPanel1 = new ListSupplierPane();
  private UrlListSupplier supplier = new UrlListSupplier(Browser.getActiveBrowser());

  public SearchUrlChooserPage() {
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    listPanel1.setListSupplier(supplier);
    listPanel1.setListCellRenderer(new OpenFileFastAction.Renderer());
  }

  private void jbInit() throws Exception {
    this.setLayout(new BorderLayout());
    this.add(listPanel1, java.awt.BorderLayout.CENTER);
  }

  public HelpTopic getHelpTopic() {
    return null;
  }

  public void okPressed() {
  }

  public void setUrl(Url url) {
    listPanel1.setText(url.getName());
  }

  public void setUrlFilters(UrlFilter[] urlFilterArray) {
  }

  public void setMultiSelectionEnabled(boolean multipleSelection) {
  }

}
