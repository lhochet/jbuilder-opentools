package net.java.dev.jbuilder.opentool.findfiles.opentools;

import com.borland.jbuilder.node.ManifestFileNode;
import com.borland.primetime.node.Project;
import com.borland.primetime.node.Node;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.node.DuplicateNodeException;
import com.borland.jbuilder.ide.JBuilderIcons;
import com.borland.primetime.node.FileType;
import com.borland.jbuilder.enterprise.module.ModuleManager;
import com.borland.jbuilder.enterprise.module.DefaultFileTypeSettings;

public class OpenToolsFileNode extends ManifestFileNode {
  public static void initOpenTool(byte majorVersion, byte minorVersion) {
    registerFileNodeClass("mf", "Manifest file", OpenToolsFileNode.class, JBuilderIcons.ICON_GEAR);
    FileType.registerFileType("opentools", FileType.getFileType("mf"));
  }

  public OpenToolsFileNode(Project project, Node node, Url url) throws DuplicateNodeException {
    super(project, node, url);
  }

  public Class getTextStructureClass() {
    return OpenToolsFileStructure.class;
  }
}
