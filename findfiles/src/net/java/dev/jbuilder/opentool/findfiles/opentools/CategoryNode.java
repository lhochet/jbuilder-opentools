package net.java.dev.jbuilder.opentool.findfiles.opentools;

import java.util.*;

import javax.swing.tree.*;

public class CategoryNode extends DefaultMutableTreeNode {
  private List openTools = new ArrayList();
  private RootNode parent;
  private String category;

  public CategoryNode(String category, RootNode parent) {
    this.parent = parent;
    this.category = category;
  }

  public Enumeration children() {
    return Collections.enumeration(openTools);
  }

  public boolean getAllowsChildren() {
    return false;
  }

  public TreeNode getChildAt(int childIndex) {
    return (TreeNode) openTools.get(childIndex);
  }

  public int getChildCount() {
    return openTools.size();
  }

  public int getIndex(TreeNode node) {
    return openTools.indexOf(node);
  }

  public TreeNode getParent() {
    return parent;
  }

  public boolean isLeaf() {
    return false;
  }

  public void addNode(OpenToolNode openToolNode) {
    openTools.add(openToolNode);
  }

  public String getCategory() {
    return category;
  }

  public String toString() {
    return getCategory();
  }

}
