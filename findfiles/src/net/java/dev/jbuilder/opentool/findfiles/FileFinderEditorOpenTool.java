package net.java.dev.jbuilder.opentool.findfiles;

import javax.swing.Action;

import com.borland.jbuilder.JBuilderMenu;
import com.borland.primetime.editor.EditorActions;
import com.borland.primetime.ide.ContextActionProvider;
import com.borland.primetime.ide.ProjectView;
import com.borland.primetime.ide.Browser;
import com.borland.primetime.node.Node;
import com.borland.primetime.ide.BrowserAction;
import com.borland.primetime.ide.BrowserIcons;
import net.java.dev.jbuilder.opentool.findfiles.search.*;

public class FileFinderEditorOpenTool {
    public static void initOpenTool(byte majorVersion, byte minorVersion) {
        OpenFileFastAction action = new OpenFileFastAction();
        action.putValue(Action.NAME, "open-file-fast");
        EditorActions.addBindableIdeAction(
                action,
                EditorActions.GROUP_FILE,
                "open-file-fast");
        JBuilderMenu.GROUP_SearchGoto.add(action);
        ProjectView.registerContextActionProvider(new ContextActionProvider() {
            public Action getContextAction(Browser browser, Node[] nodes) {
                Action action = null;
                if (nodes.length > 0) {
                    boolean hasChildren = false;
                    for (int i = 0; i < nodes.length; i++) {
                        if (nodes[i].getDisplayChildren().length > 0) {
                            hasChildren = true;
                            break;
                        }
                    }
                    if (hasChildren) {
                        action = new ContextAction(nodes);
                    }
                }
                return action;
            }
        });
    }

    private static class ContextAction extends BrowserAction {
        private Node[] nodes;
        public ContextAction(Node[] nodes) {
            super("Open File Fast", 's', "search on all the files in the directory view",
                  BrowserIcons.ICON_SEARCHSOURCEPATH);
            this.nodes = nodes;
        }

        public void update(Browser browser) {
            if (browser.getActiveUserProject() == null) {
                setEnabled(false);
                setDisabledReason("No active project");
            }
        }

        public void actionPerformed(Browser browser) {
            UrlListSupplier supplier = new UrlListSupplier(browser, nodes);
            OpenFileFastAction.showFileListPane(browser, supplier);
        }


    }

}
