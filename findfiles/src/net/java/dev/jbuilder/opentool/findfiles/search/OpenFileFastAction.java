package net.java.dev.jbuilder.opentool.findfiles.search;

import java.awt.Component;
import java.awt.Insets;

import javax.swing.*;

import com.borland.primetime.ide.*;
import com.borland.primetime.node.FileNode;
import com.borland.primetime.ui.*;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.node.FileType;
import com.borland.primetime.node.Node;
import com.borland.primetime.help.HelpTopic;
import com.borland.jbuilder.JBuilderHelp;
import java.net.*;

public class OpenFileFastAction extends BrowserAction {
  private static final HelpTopic HELP;

  static {
    URL url = OpenFileFastAction.class.getResource("./OpenFileDialogHelp.html");
    HELP = url == null ? null : JBuilderHelp.createTopic(null, url.toExternalForm());
  }

      public OpenFileFastAction() {
        super("Open File Fast", 's', "search on all the files in the directory view",
              BrowserIcons.ICON_SEARCHSOURCEPATH);
    }

    public void update(Browser browser) {
        if (browser.getActiveUserProject() == null) {
            setEnabled(false);
            setDisabledReason("No active project");
        }
        else {
            setEnabled(true);
        }
    }

    public void actionPerformed(Browser browser) {
        UrlListSupplier supplier = new UrlListSupplier(browser);
        showFileListPane(browser, supplier);
    }

    public static void showFileListPane(Browser browser, UrlListSupplier supplier) {
        Object[] urls = ListSupplierPane.showListSupplierDialog(browser, supplier,
                true, "Open File Fast", "Search:", 'S', "Matches:", 'M', HELP, "OpenFileFast",
                "", new String[] {"Yo", "Sup", "Like this opentool?", "There are more!!"},
                new Renderer());
        for (int i = 0; i < urls.length; i++) {
            Url url = (Url) urls[i];
            Node node = browser.getActiveProject().getNode(url);
            try {
                browser.navigateTo(node);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    public static class Renderer extends DefaultListCellRenderer {
        private StringBuffer buffer = new StringBuffer();
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            Url url = (Url) value;
            buffer.append(url.getName());
            buffer.append("(").append(url.getFullName()).append(")");
            setText(buffer.toString());
            buffer.delete(0, buffer.length());
            FileType fileType = FileType.getFileType(url);
            if (fileType == null) {
              setIcon(BrowserIcons.ICON_FILETEXT);
            }
            else {
              setIcon(fileType.getIcon());
            }
            return this;
        }

    }
}
