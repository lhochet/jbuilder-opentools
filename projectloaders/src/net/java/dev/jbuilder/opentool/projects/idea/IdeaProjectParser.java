package net.java.dev.jbuilder.opentool.projects.idea;

import java.io.*;

import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;
import net.java.dev.jbuilder.opentool.projects.xml.XmlUtil;
import org.w3c.dom.Document;
import net.java.dev.jbuilder.opentool.projects.xml.DocumentFilter;
import net.java.dev.jbuilder.opentool.projects.xml.NodeRule;
import java.util.List;
import org.w3c.dom.Node;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;

public class IdeaProjectParser {
  private InputStream input;
  private Url url;
  private IdeaProjectInfo project;
  private Url projectDir;

  public IdeaProjectParser(InputStream input, Url url) {
    this.input = input;
    this.url = url;
    projectDir = url.getParent();
  }

  public IdeaProjectInfo load() throws IOException {
    project = new IdeaProjectInfo(url);
    Document document = XmlUtil.getDocument(input);
    DocumentFilter filter = new DocumentFilter(document);
    loadLibraries(filter);
    loadModules(filter);
    return project;
  }

  private void loadModules(DocumentFilter filter) {
    ComponentByName moduleFilter = new ComponentByName("ProjectModuleManager");
    List list = filter.filter(moduleFilter.path(), moduleFilter);
    Node node = (Node) list.get(0);
    DocumentFilter modules = new DocumentFilter(node);
    List moduleUrls = modules.filter("/component/modules/module", new NodeRule() {
      public Object process(Node node) {
        return convert(XmlUtil.getAttributeValue(node, "fileurl"));
      }
    });
    project.setModuleUrls(moduleUrls);
  }

  private void loadLibraries(DocumentFilter filter) {
    List libraryNodes = getLibraryNodes(filter);
    parseLibrary(project, libraryNodes);
  }

  private void parseLibrary(IdeaProjectInfo project, List libraryNodes) {
    for (Iterator iterator = libraryNodes.iterator(); iterator.hasNext();) {
      Node node = (Node)iterator.next();
      DocumentFilter libraryFilter = new DocumentFilter(node);
      String name = XmlUtil.getAttributeValue(node, "name");
      List classPath = libraryFilter.filter("/library/CLASSES/root", new NodeRule() {
        public Object process(Node node) {
          return convert(XmlUtil.getAttributeValue(node, "url"));
        }
      });
      List sourcePath = libraryFilter.filter("/library/SOURCES/root", new NodeRule() {
        public Object process(Node node) {
          return convert(XmlUtil.getAttributeValue(node, "url"));
        }
      });
      project.addLibrary(new BasicLibrary(name, classPath, sourcePath));
    }
  }

  private List getLibraryNodes(DocumentFilter filter) {
    ComponentByName rule = new ComponentByName("libraryTable");
    List list = filter.filter(rule.path(), rule);
    Node libraryNode = (Node) list.get(0);
    DocumentFilter libraryFilter = new DocumentFilter(libraryNode);
    return libraryFilter.filter("/component/library", NodeRule.ALL);
  }

  private Url convert(String path) {
    int index = path.indexOf("://");
    int end = path.indexOf("!");
    if (end == -1) {
      end = path.length();
    }
    String filePath = path.substring(index + 3, end);
    final String variable = "$PROJECT_DIR$";
    if (filePath.startsWith(variable)) {
      filePath = filePath.substring(variable.length() + 1);
      return projectDir.getRelativeUrl(filePath);
    }
    return new Url(new File(filePath));
  }
}
