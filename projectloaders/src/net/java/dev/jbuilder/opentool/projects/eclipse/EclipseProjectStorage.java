package net.java.dev.jbuilder.opentool.projects.eclipse;

import java.io.*;

import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;

public class EclipseProjectStorage extends ProjectLoaderFactory {
  public EclipseProjectStorage(Project project) {
    super(project);
  }

  protected JavaProjectLoader createLoader(InputStream inputStream, Url url) {
    return new EclipseProjectParser(inputStream, url);
  }

}
