package net.java.dev.jbuilder.opentool.projects.xml;

import org.w3c.dom.Document;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

public class XmlUtil {
  private XmlUtil(){}

  public static Document getDocument(InputStream inputStream) throws IOException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setValidating(false);
    DocumentBuilder builder = null;
    try {
      builder = factory.newDocumentBuilder();
      return builder.parse(inputStream);
    }
    catch (Exception ex) {
      throw new RuntimeException(ex.getMessage(), ex);
    }
  }

  public static String getTextContent(Node node) {
    NodeList list = node.getChildNodes();
    for(int i = 0; i < list.getLength(); i++) {
      Node item = list.item(i);
      if (item instanceof Text) {
        return item.getNodeValue();
      }
    }
    return "";
  }

  public static String getAttributeValue(Node node, String name) {
    NamedNodeMap attributes = node.getAttributes();
    Node attribute = attributes.getNamedItem(name);
    return attribute == null ? null : attribute.getNodeValue();
  }

}
