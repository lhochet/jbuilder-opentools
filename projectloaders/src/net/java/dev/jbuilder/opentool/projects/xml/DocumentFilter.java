package net.java.dev.jbuilder.opentool.projects.xml;

import java.util.*;

import org.w3c.dom.*;

public class DocumentFilter {
  private Node node;

  public DocumentFilter(Document document) {
    node = document.getDocumentElement();
  }

  public DocumentFilter(Node node) {
    this.node = node;
  }

  public List filter(String path, NodeRule rule) {
    List list = new ArrayList ();
    list.add(node);
    list = filterByPath(path, list);
    return filterByRule(list, rule);
  }

  private List filterByPath(String path, List list) {
    List children = list;
    for (StringTokenizer tokenizer = new StringTokenizer(path, "/");
         tokenizer.hasMoreTokens() && list.size() > 0; children = getChildren(list)) {
      list = children;
      filter(list, tokenizer.nextToken());
    }
    return list;
  }

  private List filterByRule(List list, NodeRule rule) {
    ArrayList result = new ArrayList(list.size());
    for(Iterator iterator = list.iterator(); iterator.hasNext();) {
      Object object = rule.process((Node)iterator.next());
      if (object != null) {
        result.add(object);
      }
    }
    return result;
  }

  private void filter(List list, String name) {
    for(Iterator iterator = list.iterator(); iterator.hasNext();) {
      Node node = (Node)iterator.next();
      if (!name.equals(node.getNodeName())) {
        iterator.remove();
      }
    }
  }

  private ArrayList getChildren(List list) {
    ArrayList result = new ArrayList();
    for (Iterator iterator = list.iterator(); iterator.hasNext();) {
      Node element = (Node)iterator.next();
      NodeList children = element.getChildNodes();
      for(int i = 0; i < children.getLength(); i++) {
        result.add(children.item(i));
      }
    }
    return result;
  }

}
