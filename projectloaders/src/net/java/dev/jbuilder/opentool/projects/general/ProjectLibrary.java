package net.java.dev.jbuilder.opentool.projects.general;

import java.util.List;
import com.borland.primetime.vfs.Url;
import java.util.Collections;

public class ProjectLibrary implements Library {
  private Url projectUrl;

  public ProjectLibrary(Url url) {
    this.projectUrl = url;
  }

  public String getName() {
    return projectUrl.getName();
  }

  public List getClassPath() {
    return Collections.singletonList(projectUrl);
  }

  public List getSourcePath() {
    throw new UnsupportedOperationException();
  }
}
