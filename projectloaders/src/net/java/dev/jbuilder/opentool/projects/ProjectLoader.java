package net.java.dev.jbuilder.opentool.projects;

import com.borland.jbuilder.ide.*;
import com.borland.jbuilder.node.*;
import com.borland.primetime.build.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.util.*;
import net.java.dev.jbuilder.opentool.projects.eclipse.*;
import net.java.dev.jbuilder.opentool.projects.idea.IdeaProjectStorage;
import net.java.dev.jbuilder.opentool.projects.idea.IdeaModuleStorage;

public class ProjectLoader {
  public static void initOpenTool(byte majorVersion, byte minorVersion) {
    loadEclipseWorkspaceStorage();
    loadEclipseProjectStorage();
//    EclipseProjectRenderer.init();
    loadIdeaProjectAsProjectGroup();
    loadIdeaProjectModuleAsProject();
  }

  private static void loadEclipseWorkspaceStorage() {
    final String extension = "lock";
    ProjectGroup.registerProjectGroupClass(extension, "Eclipse workspace", ProjectGroup.class, BrowserIcons.ICON_PROJECT_GROUP);
    ProjectGroupStorage.registerStorageClass(extension, "Eclipse workspace", EclipseWorkspaceStorage.class);
    BuildPropertyGroup.registerUnViewableErrorFileExtension(extension);
    FileAssociation.create("application/x-eclipse-workspace", extension, "Eclipse Workspace");
  }

  private static void loadEclipseProjectStorage() {
    final String extension = "project";
    Project.registerProjectClass(extension, "Eclipse project", JBProject.class, JBuilderIcons.ICON_JBPROJECT);
    ProjectStorage.registerStorageClass(extension, "Eclipse project", EclipseProjectStorage.class);
    BuildPropertyGroup.registerUnViewableErrorFileExtension(extension);
    FileAssociation.create("application/x-eclipse-project", extension, "Eclipse project");
  }

  private static void loadIdeaProjectAsProjectGroup() {
    String extension = "ipr";
    ProjectGroup.registerProjectGroupClass(extension, "IDEA project", ProjectGroup.class, BrowserIcons.ICON_PROJECT_GROUP);
    ProjectGroupStorage.registerStorageClass(extension, "IDEA project", IdeaProjectStorage.class);
    BuildPropertyGroup.registerUnViewableErrorFileExtension(extension);
    FileAssociation.create("application/x-idea-project", extension, "IDEA project");
  }

  private static void loadIdeaProjectModuleAsProject() {
    final String extension = "iml";
    Project.registerProjectClass(extension, "IDEA module", JBProject.class, JBuilderIcons.ICON_JBPROJECT);
    ProjectStorage.registerStorageClass(extension, "IDEA module", IdeaModuleStorage.class);
    BuildPropertyGroup.registerUnViewableErrorFileExtension(extension);
    FileAssociation.create("application/x-idea-module", extension, "IDEA project module");
  }

}
