package net.java.dev.jbuilder.opentool.projects.general;

import java.util.*;

public class JUnitRunInfo extends RunInfo {
  private String testedClass;
  public JUnitRunInfo(String testedClass) {
    this.testedClass = testedClass;
  }

  public String getTestedClass() {
    return testedClass;
  }

}
