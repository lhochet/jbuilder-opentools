package net.java.dev.jbuilder.opentool.projects.general;

import java.util.List;


public class BasicLibrary implements Library {
  private String name;
  private List classPath;
  private List sourcePath;

  public BasicLibrary(String name, List classPath, List sourcePath) {
    this.name = name;
    this.classPath = classPath;
    this.sourcePath = sourcePath;
  }

  public String getName() {
    return name;
  }

  public List getClassPath() {
    return classPath;
  }

  public List getSourcePath() {
    return sourcePath;
  }
}
