package net.java.dev.jbuilder.opentool.projects.eclipse;

import java.io.*;
import java.util.*;

import org.w3c.dom.*;
import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.xml.*;
import net.java.dev.jbuilder.opentool.projects.general.RunInfo;

public class LauncherParser {
  private Workspace workspace;
  private String launcherPath;
  private boolean isShared;

  public LauncherParser(Workspace workspace, String launcherPath, boolean isShared) {
    this.workspace = workspace;
    this.launcherPath = launcherPath;
    this.isShared = isShared;
  }

  public void parse() throws IOException {
    Url launchersDirectory = workspace.getRoot().getRelativeUrl(".metadata/.plugins/org.eclipse.debug.core/.launches");
    Url launcherUrl = workspace.getUrl(launchersDirectory, launcherPath);
    if (VFS.exists(launcherUrl)) {
      parseLauncher(launcherUrl);
    }
  }

  private void parseLauncher(Url launcherUrl) throws IOException {
    Document document = XmlUtil.getDocument(VFS.getInputStream(launcherUrl));
    DocumentFilter filter = new DocumentFilter(document);
    String type = getType(filter);
    Map attributes = getAttributes(document.getDocumentElement());
    String project = (String) attributes.get("org.eclipse.jdt.launching.PROJECT_ATTR");
    RunInfoFactory factory = RunInfoFactory.get(type);
    if ((isShared || attributes.containsKey("org.eclipse.debug.ui.favoriteGroups"))
        && factory != null) {
      RunInfo runInfo = factory.getConfiguration(workspace, attributes);
      String extension = launcherUrl.getFileExtension();
      String name = launcherUrl.getName();
      runInfo.setName(name.substring(0, name.length() - extension.length() -1));
      workspace.add(project, runInfo);
    }
  }

  private String getType(DocumentFilter filter) {
    List list = filter.filter("/launchConfiguration", new NodeRule() {
      public Object process(Node node) {
        return XmlUtil.getAttributeValue(node, "type");
      }
    });
    return (String) list.get(0);
  }

  private Map getAttributes(Element element) {
    Map map = new HashMap();
    NodeList nodelist = element.getChildNodes();
    for(int i = 0; i < nodelist.getLength(); i++) {
      Node node = nodelist.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        map.put(XmlUtil.getAttributeValue(node, "key"), XmlUtil.getAttributeValue(node, "value"));
      }
    }
    return map;
  }
}
