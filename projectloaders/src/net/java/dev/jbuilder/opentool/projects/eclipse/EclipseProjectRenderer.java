package net.java.dev.jbuilder.opentool.projects.eclipse;

import javax.swing.*;
import com.borland.primetime.ide.BrowserAdapter;
import com.borland.primetime.node.Project;
import com.borland.primetime.ide.ProjectView;
import com.borland.primetime.ide.Browser;
import javax.swing.tree.TreeCellRenderer;
import com.borland.primetime.ide.ProjectTree;
import java.awt.Component;

public class EclipseProjectRenderer implements ListCellRenderer {
  public static void init() {
    Browser.addStaticBrowserListener(new BrowserAdapter() {
      public void browserProjectActivated(Browser browser, Project project) {
        JComboBox projectTree = findJComboBox((browser.getViewContainer().getView("Project").getContent()));
        if (projectTree != null) {
          ListCellRenderer existingCellRenderer = projectTree.getRenderer();
          if (! (existingCellRenderer instanceof Project)) {
            projectTree.setRenderer(new EclipseProjectRenderer(existingCellRenderer));
          }
        }
      }

    });
  }

  private static JComboBox findJComboBox(JComponent component) {
    System.out.println("Examining: " + component);
    for (int i = 0; i < component.getComponentCount(); i++) {
      Component child = component.getComponent(i);
      if (child instanceof JComboBox) {
        JComboBox comboBox = (JComboBox) child;
        return comboBox.isEditable() ? null : comboBox;
      }
      else if (child instanceof JComponent) {
        JComboBox comboBox = findJComboBox((JComponent) child);
        if (comboBox != null) {
          return comboBox;
        }
      }
    }
    return null;
  }

  private ListCellRenderer delegate;

  public EclipseProjectRenderer(ListCellRenderer delegate) {
    this.delegate = delegate;
  }

  public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
    Component component = delegate.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    return component;
  }

}
