package net.java.dev.jbuilder.opentool.projects.idea;

import java.util.*;

import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;

public class IdeaProjectInfo {
  private Url projectDir;
  private Url projectUrl;
  private Map map = new HashMap();
  private List moduleUrls;

  public IdeaProjectInfo(Url projectUrl) {
    this.projectUrl = projectUrl;
    this.projectDir = projectUrl.getParent();
  }

  public Url getWorkingDirectory() {
    return projectDir;
  }

  public Library getLibrary(String name) {
    return (Library) map.get(name);
  }

  public void addLibrary(Library library) {
    map.put(library.getName(), library);
  }

  public void setModuleUrls(List modules) {
    this.moduleUrls = modules;
  }

  public List getModuleUrls() {
    return moduleUrls;
  }

  public Library getModuleAsLibrary(String moduleName) {
    Url moduleUrl = getModule(moduleName);
    return new ProjectLibrary(moduleUrl);
  }

  private Url getModule(String name) {
    for (Iterator iterator = moduleUrls.iterator(); iterator.hasNext();) {
      Url url = (Url) iterator.next();
      if (module(url).equals(name)) {
        return url;
      }
    }
    throw new IllegalArgumentException("No module found for : " + name);
  }

  private String module(Url url) {
    String name = url.getName();
    int index = name.indexOf(".");
    return index == -1 ? name : name.substring(0, index);
  }
}
