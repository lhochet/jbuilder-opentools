package net.java.dev.jbuilder.opentool.projects.eclipse;

import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.xml.XmlUtil;
import org.w3c.dom.Document;
import java.io.IOException;
import net.java.dev.jbuilder.opentool.projects.xml.DocumentFilter;
import java.util.List;
import net.java.dev.jbuilder.opentool.projects.xml.NodeRule;
import org.w3c.dom.Node;
import java.util.Iterator;
import java.io.ByteArrayInputStream;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.util.Set;
import java.util.HashSet;

public class WorkspaceParser {
  private Url workspaceRoot;
  private Workspace workspace;

  public WorkspaceParser(Url workspaceRoot) {
    this.workspaceRoot = workspaceRoot;
  }

  public Workspace parse() throws IOException {
    workspace = new Workspace(workspaceRoot);
    loadProjects();
    loadRunConfigurations();
    return workspace;
  }

  private void loadProjects() {
    Url[] subDirectories = VFS.getChildren(workspaceRoot, Filesystem.TYPE_DIRECTORY);
    for(int i = 0; i < subDirectories.length; i++) {
      Url projectFile = subDirectories[i].getRelativeUrl(".project");
      Url classPathFile = subDirectories[i].getRelativeUrl(".classpath");
      if (VFS.isFile(projectFile) && VFS.isFile(classPathFile)) {
        workspace.addProjectUrl(projectFile);
      }
    }
  }

  private void loadRunConfigurations() throws IOException {
    Url xml = workspaceRoot.getRelativeUrl(".metadata/.plugins/org.eclipse.debug.ui/launchConfigurationHistory.xml");
    Document document = XmlUtil.getDocument(VFS.getInputStream(xml));
    List mementos = new DocumentFilter(document).filter("/launchHistory/launch", new NodeRule() {
      public Object process(Node node) {
        return XmlUtil.getAttributeValue(node, "memento");
      }
    });
    Set launchFiles = new HashSet();
    for (Iterator iterator = mementos.iterator(); iterator.hasNext();) {
      String content = (String) iterator.next();
      Document mementoXml = XmlUtil.getDocument(new ByteArrayInputStream(content.getBytes()));
      Element element = mementoXml.getDocumentElement();
      boolean isShared = "false".equals(XmlUtil.getAttributeValue(element, "local"));
      String launchFile = XmlUtil.getAttributeValue(element, "path");
      if (!launchFiles.contains(launchFile)) {
        new LauncherParser(workspace, launchFile, isShared).parse();
        launchFiles.add(launchFile);
      }
    }
  }

}
