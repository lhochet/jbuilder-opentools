package net.java.dev.jbuilder.opentool.projects.general;

public class ParsingException extends RuntimeException {
  public ParsingException(String message, Throwable cause) {
    super(message, cause);
  }
}
