package net.java.dev.jbuilder.opentool.projects.eclipse;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.*;
import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;
import net.java.dev.jbuilder.opentool.projects.xml.*;

public class EclipseProjectParser implements JavaProjectLoader {
  public static final String PATH_CLASSPATHENTRY = "/classpath/classpathentry";
  private Url projectDirectory;
  private Url fileUrl;
  private JavaProjectInfo project;
  private InputStream inputStream;

  public EclipseProjectParser(InputStream inputStream, Url fileUrl) {
    this.fileUrl = fileUrl;
    this.projectDirectory = fileUrl.getParent();
    this.inputStream = inputStream;
  }

  public JavaProjectInfo load() {
    try {
      project = new JavaProjectInfo(projectDirectory);
      verifyProjectFile();
      loadClassPath();
      loadRunConfiguration();
      return project;
    }
    catch (IOException ex) {
      throw new ParsingException("Error parsing project file " + fileUrl + ".  Caused by:" + ex.getMessage(), ex);
    }
  }

  private void verifyProjectFile() throws IOException {
    Document document = XmlUtil.getDocument(inputStream);
    DocumentFilter filter = new DocumentFilter(document);
    final String path = "/projectDescription/natures/nature";
    NodeRule pcDataLoader = new NodeRule() {
      public Object process(Node node) {
        return XmlUtil.getTextContent(node);
      }
    };
    List nodes = filter.filter(path, pcDataLoader);
    if (!nodes.contains("org.eclipse.jdt.core.javanature")) {
      throw new NoJavaNatureException(path, fileUrl);
    }
    List names = filter.filter("/projectDescription/name", pcDataLoader);
    String name = names.isEmpty() ? projectDirectory.getName() : (String) names.get(0);
    project.setName(name);
  }

  private void loadClassPath() throws IOException {
    Url url = projectDirectory.getRelativeUrl(".classpath");
    Document document = XmlUtil.getDocument(VFS.getInputStream(url));
    DocumentFilter filter = new DocumentFilter(document);
    project.setSources(parseSources(filter));
    project.setLibrary(parseLibrary(filter));
    List output = filter.filter(PATH_CLASSPATHENTRY, new PathFilter("output", projectDirectory));
    project.setOutputUrl((Url) output.get(0));
  }

  private List parseSources(DocumentFilter filter) {
    List sources = filter.filter(PATH_CLASSPATHENTRY, new PathFilter("src", projectDirectory) {
      public Object process(Node node) {
        Url url = (Url) super.process(node);
        if (url != null) {
          Url projectFile = url.getRelativeUrl(".project");
          if (VFS.isFile(projectFile) && !projectFile.equals(fileUrl)) {
            url = null;
          }
        }
        return url;
      }

    });
    if (sources.size() == 0) {
      sources.add(projectDirectory.getRelativeUrl("./src"));
    }
    return sources;
  }

  private List parseLibrary(DocumentFilter filter) {
    List list = filter.filter(PATH_CLASSPATHENTRY, new NodeRule() {
      public Object process(Node node) {
        NamedNodeMap attributes = node.getAttributes();
        String kind = attributes.getNamedItem("kind").getNodeValue();
        String path = attributes.getNamedItem("path").getNodeValue();
        Url result = null;
        if ("src".equals(kind)) {
          Url pathUrl = convert(projectDirectory, path);
          Url projectCandidate = pathUrl.getRelativeUrl(".project");
          if (VFS.exists(projectCandidate)) {
            result = projectCandidate;
          }
        } else if ("lib".equals(kind)) {
          Url pathUrl = convert(projectDirectory, path);
          result = pathUrl;
        }
        return result;
      }

    });
    return list;
  }

  private void loadRunConfiguration() {
    Workspace workspace = EclipseWorkspaceStorage.getCurrentWorkspace();
    if (workspace == null) {
      workspace = new Workspace(projectDirectory.getParent());
    }
    project.setRunConfigurations(workspace.getRunConfig(project.getName()));
  }

  private static class PathFilter implements NodeRule {
    private Url workingDirectory;
    private String kindAttribute;

    public PathFilter(String kindAttribute, Url workingDirectory) {
      this.kindAttribute = kindAttribute;
      this.workingDirectory = workingDirectory;
    }

    public Object process(Node node) {
      NamedNodeMap attributes = node.getAttributes();
      String kind = attributes.getNamedItem("kind").getNodeValue();
      if (kindAttribute.equals(kind)) {
        String path = attributes.getNamedItem("path").getNodeValue();
        return convert(workingDirectory, path);
      }
      return null;
    }
  }

  private static Url convert(Url directory, String path) {
    String relativePath = null;
    if (path.startsWith("/")) {
      directory = directory.getParent();
      relativePath = "." + path;
    } else {
      relativePath = "./" + path;
    }
    return directory.getRelativeUrl(relativePath);
  }

}
