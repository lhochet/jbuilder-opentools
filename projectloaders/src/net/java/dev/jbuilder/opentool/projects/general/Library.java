package net.java.dev.jbuilder.opentool.projects.general;

import java.util.List;

public interface Library {
  public String getName();
  public List getClassPath();
  public List getSourcePath();
}
