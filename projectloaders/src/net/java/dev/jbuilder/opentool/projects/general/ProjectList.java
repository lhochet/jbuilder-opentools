package net.java.dev.jbuilder.opentool.projects.general;

import java.util.*;

import com.borland.jbuilder.node.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;

public class ProjectList {
  private ArrayList projects = new ArrayList();

  public void addProject(Project project) {
    projects.add(new ProjectEntry((JBProject) project));
  }

  public void addToProjectGroup(ProjectGroup projectGroup) {
    for (Iterator iterator = sortedList().iterator(); iterator.hasNext();) {
      ProjectEntry project = (ProjectEntry) iterator.next();
      projectGroup.addProject(project.project);
    }
  }

  private List sortedList() {
    LinkedList list = new LinkedList();
    for (Iterator iterator = projects.iterator(); iterator.hasNext();) {
      ProjectEntry entry = (ProjectEntry) iterator.next();
      addEntry(list, entry);
    }
    return list;
  }

  private void addEntry(LinkedList list, ProjectEntry entry) {
    ListIterator iterator = list.listIterator();
    while (iterator.hasNext()) {
      ProjectEntry current = (ProjectEntry) iterator.next();
      if (current.dependOnProject(entry.project)) {
        iterator.previous();
        iterator.add(entry);
        return;
      }
    }
    iterator.add(entry);
  }

  private static class ProjectEntry {
    private JBProject project;
    private Set dependedProjectUrls;

    public ProjectEntry(JBProject project) {
      this.project = project;
      setDependedProjectUrls(project.getProperty("sys", "Libraries", ""));
    }

    private void setDependedProjectUrls(String string) {
      Url directoryUrl = project.getUrl().getParent();
      dependedProjectUrls = new HashSet();
      for(StringTokenizer tokenizer = new StringTokenizer(string, ";"); tokenizer.hasMoreTokens();) {
        dependedProjectUrls.add(directoryUrl.getRelativeUrl(tokenizer.nextToken()));
      }

    }

    public void addDependedProject(JBProject project) {
      dependedProjectUrls.add(project.getUrl());
    }

    public boolean dependOnProject(JBProject project) {
      return dependedProjectUrls.contains(project.getUrl());
    }

    public String toString() {
      return project.getUrl().getFullName();
    }
  }
}
