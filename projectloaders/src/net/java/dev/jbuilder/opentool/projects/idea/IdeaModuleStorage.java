package net.java.dev.jbuilder.opentool.projects.idea;

import java.io.*;

import com.borland.jbuilder.node.*;
import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;
import com.borland.primetime.node.Project;

public class IdeaModuleStorage extends ProjectLoaderFactory {
  public IdeaModuleStorage(Project project) {
    super(project);
  }

  protected JavaProjectLoader createLoader(InputStream inputStream, Url url) {
    return new IdeaModuleParser(inputStream, url);
  }

}
