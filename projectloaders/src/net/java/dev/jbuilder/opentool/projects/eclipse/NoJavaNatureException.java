package net.java.dev.jbuilder.opentool.projects.eclipse;

import com.borland.primetime.vfs.Url;

public class NoJavaNatureException extends RuntimeException {
  public NoJavaNatureException(String xpath, Url projectFile) {
    super("File does not have org.eclipse.jdt.core.javanature at " + xpath + ": " + projectFile);
  }
}
