package net.java.dev.jbuilder.opentool.projects.eclipse;

import java.util.*;
import java.util.regex.*;

import com.borland.primetime.vfs.*;

public class Workspace {
  private Url root;
  private List projectUrls = new ArrayList();
  private Map projectRunConfigurations = new HashMap();

  public Workspace(Url root) {
    this.root = root;
  }

  public void addProjectUrl(Url url) {
    projectUrls.add(url);
  }

  public Url getRoot() {
    return root;
  }

  public List getProjectUrls() {
    return Collections.unmodifiableList(projectUrls);
  }

  public List getRunConfig(String projectName) {
    return Collections.unmodifiableList(retrieveList(projectName, false));
  }

  private List retrieveList(String projectName, boolean createIfNotFound) {
    List list = (List) projectRunConfigurations.get(projectName);
    if (list == null) {
      if (createIfNotFound) {
        list = new ArrayList();
        projectRunConfigurations.put(projectName, list);
      } else {
        list = Collections.EMPTY_LIST;
      }
    }
    return list;
  }

  public Url getUrl(Url current, String path) {
    String relativePath = null;
    if (path.startsWith("/")) {
      return root.getRelativeUrl(path.substring(1));
    } else {
      return current.getRelativeUrl(path);
    }
  }

  public void add(String project, Object runInfo) {
    retrieveList(project, true).add(runInfo);
  }

  public String expandMacro(String macroString) {
    String value = macroString.replaceAll("\\&quot;", "\"");
    Pattern pattern = Pattern.compile("\\$\\{([^:]*):([^}]*)\\}");
    Matcher matcher = pattern.matcher(value);
    int index = 0;
    StringBuffer buffer = new StringBuffer();
    while (matcher.find(index)) {
      int start = matcher.start();
      int end = matcher.end();
      buffer.append(value.substring(index, start));
      String root = matcher.group(1);
      String relativePath = matcher.group(2);
      buffer.append(construct(root, relativePath));
      index = end;
    }
    buffer.append(value.substring(index));
    return buffer.toString();
  }

  private String construct(String root, String relativePath) {
    Url url = null;
    if ("workspace_loc".equals(root)) {
      url = this.getRoot().getRelativeUrl(relativePath);
    } else if ("resource_loc".equals(root)) {
      url = getUrl(this.root, relativePath);
    } else {
      return "${" + root + ":" + relativePath + "}";
    }
    return url.getFullName();
  }
}
