package net.java.dev.jbuilder.opentool.projects.idea;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;

import org.xml.sax.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;
import com.borland.primetime.util.Streams;

public class IdeaProjectStorage extends ProjectGroupStorage {
  public static final String CATEGORY = "java-net-projects";
  public static final String PROPERTY = "project.group.content";

  public IdeaProjectStorage(ProjectGroup group) {
    super(group);
  }

  protected void readProjectGroup(InputStream inputStream, Url url) throws SAXException, IOException, ParserConfigurationException {
    IdeaProjectParser parser = new IdeaProjectParser(inputStream, projectGroup.getUrl());
    defaultJavaProjectInfo = parser.load();
    ProjectList projectList = new ProjectList();
    for(Iterator iterator = defaultJavaProjectInfo.getModuleUrls().iterator(); iterator.hasNext();) {
      Url moduleUrl = (Url) iterator.next();
      projectList.addProject(Project.getProject( moduleUrl));
    }
    projectList.addToProjectGroup(projectGroup);
  }

  private String loadContent(Url url) throws IOException {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    InputStream input = VFS.getInputStream(url);
    try {
      Streams.copy(input, output);
    }
    finally {
      input.close();
    }
    return output.toString();
  }

  public byte[] getBufferContent() {
    try {
      String content = loadContent(projectGroup.getUrl());
      return content.getBytes();
    }
    catch (IOException ex) {
      throw new RuntimeException(ex.getMessage(), ex);
    }
  }

  static IdeaProjectInfo defaultJavaProjectInfo;

  public static IdeaProjectInfo getDefaultJavaProjectInfo() {
    return defaultJavaProjectInfo;
  }

}
