package net.java.dev.jbuilder.opentool.projects.general;

public class ApplicationRunInfo extends RunInfo {
  private String mainClass;
  private String argument;
  private String vmParameter;

  public void setMainClass(String mainClass) {
    this.mainClass = mainClass;
  }
  public String getMainClass() {
    return mainClass;
  }

  public String getArgument() {
    return argument;
  }

  public void setArgument(String argument) {
    this.argument = argument;
  }

  public String getVmParameter() {
    return vmParameter;
  }

  public void setVmParameter(String vmParameter) {
    this.vmParameter = vmParameter;
  }

}
