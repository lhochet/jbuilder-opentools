package net.java.dev.jbuilder.opentool.projects.jbuilder;

import net.java.dev.jbuilder.opentool.projects.general.JavaProjectInfo;
import com.borland.jbuilder.node.JBProject;
import com.borland.primetime.vfs.*;
import java.util.List;
import java.util.Iterator;
import net.java.dev.jbuilder.opentool.projects.general.RunInfo;
import net.java.dev.jbuilder.opentool.projects.general.JUnitRunInfo;
import net.java.dev.jbuilder.opentool.projects.general.ApplicationRunInfo;
import com.borland.jbuilder.runtime.TestRunner;
import com.borland.jbuilder.runtime.ApplicationRunner;

//TODO Once we know how to load JBProject in test, we'll be able to test them
public class JBProjectSetup {
  public static final String RUNNABLE_TYPE = "RunnableType";

  private JBProject jbProject;
  private JavaProjectInfo javaProject;

  private Url workingDirectory;

  public JBProjectSetup(JBProject jbProject, JavaProjectInfo javaProject) {
    this.jbProject = jbProject;
    this.javaProject = javaProject;
    this.workingDirectory = javaProject.getWorkingDirectory();
    assert jbProject.getUrl().getParent().equals(workingDirectory);
  }

  public void update() {
    String[] extensions = {
      "txt", "xml", "properties", "gif", "png", "jpg", "dtd"
    };
    for(int i = 0; i < extensions.length; i++) {
      jbProject.setProperty("build", "CopyExtensions." + (i + 1), extensions[i]);
    }
    List runConfiguration = javaProject.getRunConfigurations();
    if (runConfiguration.size() == 0) {
      jbProject.setProperty("runtime", "DefaultConfiguration", "-1");
      jbProject.setProperty("runtime", "ExcludeDefaultForZero", "1");
    } else {
      jbProject.setProperty("runtime", "DefaultConfiguration", "-1");
      int i = 0;
      for (Iterator iterator = runConfiguration.iterator(); iterator.hasNext(); i++) {
        RunInfo runInfo = (RunInfo) iterator.next();
        jbProject.setProperty("runtime." + i, "ConfigurationName", runInfo.getName());
        if (runInfo instanceof JUnitRunInfo) {
          setProperties(i, jbProject, (JUnitRunInfo)runInfo);
        } else if (runInfo instanceof ApplicationRunInfo) {
          setProperties(i, jbProject, (ApplicationRunInfo)runInfo);
        } else {
          i--;
        }
      }
      jbProject.setProperty("runtime", "ConfigurationCount", String.valueOf(i - 1));
    }
    jbProject.setProperty("sys", "DefaultPath", toPathValue(javaProject.getDefaultSource()));
    jbProject.setProperty("sys", "Libraries", "JUnit;" + toPathValue(javaProject.getLibraryUrls()));
    jbProject.setProperty("sys", "OutPath", toPathValue(javaProject.getOutputUrl()));
    jbProject.setProperty("sys", "SourcePath", toPathValue(javaProject.getSources()));
    jbProject.setProperty("sys", "TestPath",   toPathValue((javaProject.getTestSource())));
    jbProject.setProperty("sys", "WorkingDirectory", ".");
  }

  private void setProperties(int number, JBProject project, JUnitRunInfo runInfo) {
    String category = "runtime." + number;
    project.setProperty(category, "RunnableType", TestRunner.class.getName());
    project.setProperty(category, "test.class", runInfo.getTestedClass());
    project.setProperty(category, "test.type", "class");
    project.setProperty(category, "test.harness", "com.borland.jbuilder.unittest.JBTestRunner");
  }

  private void setProperties(int number, JBProject project, ApplicationRunInfo runInfo) {
    String category = "runtime." + number;
    project.setProperty(category, "RunnableType", ApplicationRunner.class.getName());
    project.setProperty(category, "application.class", runInfo.getMainClass());
    project.setProperty(category, "application.parameters", runInfo.getArgument());
    project.setProperty(category, "application.vmparameters", runInfo.getVmParameter());
  }


  private String toPathValue(List urls) {
    StringBuffer buffer = new StringBuffer();
    for(Iterator iterator = urls.iterator(); iterator.hasNext();) {
      Url url = (Url)iterator.next();
      if (buffer.length() > 0) {
        buffer.append(";");
      }
      buffer.append(toPathValue(url));
    }
    return buffer.toString();
  }

  private String toPathValue(Url url) {
    return workingDirectory.getRelativePath(url);
  }

}
