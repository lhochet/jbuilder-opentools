package net.java.dev.jbuilder.opentool.projects.general;

import java.util.*;

import com.borland.primetime.vfs.*;

public class JavaProjectInfo {
  private String name;
  private List sources;
  private Url outputUrl;
  private Url defaultSource;
  private Url testUrl;
  private List library;
  private Url workingDirectory;
  private List runConfigurations = Collections.EMPTY_LIST;


  public JavaProjectInfo(Url workingDirectory) {
    this.workingDirectory = workingDirectory;
  }

  private Url guessDefaultPath() {
    Collections.sort(sources, new Comparator() {
      public int compare(Object o1, Object o2) {
        return rank((Url)o2) - rank((Url)o1);
      }

      public boolean equals(Object obj) {
        return obj == this;
      }

      private int rank(Url url) {
        if (!workingDirectory.isParentOf(url)) {
          return 0;
        } else if (!url.getParent().equals(workingDirectory)) {
          return 1;
        }
        String name = url.getName();
        if ("test".equals(name)) {
          return 2;
        }
        if (!"source".equals(name) && !"src".equals(name)) {
          return 3;
        }
        return 4;
      }

    });
    return sources.isEmpty() ? workingDirectory.getRelativeUrl("src") : (Url)sources.get(0);
  }

  private Url guessTestPath() {
    Collections.sort(sources, new Comparator() {
      public int compare(Object o1, Object o2) {
        return rank((Url)o2) - rank((Url)o1);
      }

      public boolean equals(Object obj) {
        return obj == this;
      }

      private int rank(Url url) {
        if (!workingDirectory.isParentOf(url)) {
          return 0;
        } else if (!url.getParent().equals(workingDirectory)) {
          return 1;
        }
        String name = url.getName();
        if (!"test".equals(name)) {
          return 2;
        }
        return 3;
      }

    });
    return sources.isEmpty() ? workingDirectory.getRelativeUrl("test") : (Url)sources.get(0);
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setSources(List sources) {
    this.sources = sources;
  }

  public List getSources() {
    return Collections.unmodifiableList(sources);
  }

  public void setOutputUrl(Url url) {
    this.outputUrl = url;
  }

  public Url getOutputUrl() {
    return outputUrl;
  }

  public Url getDefaultSource() {
    return defaultSource == null ? guessDefaultPath() : defaultSource;
  }

  public Url getTestSource() {
    return testUrl == null ? guessTestPath() : testUrl;
  }

  public void setLibrary(List urls) {
    this.library = urls;
  }

  public List getLibraryUrls() {
    return Collections.unmodifiableList(library);
  }

  public Url getWorkingDirectory() {
    return workingDirectory;
  }

  public void setTestSource(Url url) {
    this.testUrl = url;
  }

  public void setRunConfigurations(List configurations) {
    this.runConfigurations = configurations;
  }

  public List getRunConfigurations() {
    return Collections.unmodifiableList(runConfigurations);
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("[").append(getName()).append("]");
    buffer.append(getWorkingDirectory()).append("\n");
    buffer.append("sources:").append("\n");
    Url defaultSource = getDefaultSource();
    Url testSource = getTestSource();
    for (Iterator iterator = getSources().iterator(); iterator.hasNext();) {
      Url url = (Url) iterator.next();
      buffer.append("  ");
      if (url.equals(defaultSource)) {
        buffer.append("[defaultsource]");
      }
      if (url.equals(testSource)) {
        buffer.append("[test]");
      }
      buffer.append(url).append("\n");
    }
    buffer.append("libraries");
    for (Iterator iterator = getLibraryUrls().iterator(); iterator.hasNext();) {
      buffer.append("  ");
      buffer.append(iterator.next()).append("\n");
    }
    return buffer.toString();
  }



}
