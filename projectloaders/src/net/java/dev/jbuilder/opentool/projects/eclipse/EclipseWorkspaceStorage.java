package net.java.dev.jbuilder.opentool.projects.eclipse;

import java.io.*;
import javax.xml.parsers.*;

import org.xml.sax.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;

public class EclipseWorkspaceStorage extends ProjectGroupStorage {
  public EclipseWorkspaceStorage(ProjectGroup group) {
    super(group);
  }

  public boolean canWriteProject() {
    return true;
  }

  protected void readProjectGroup(InputStream inputStream, Url url) throws SAXException, IOException, ParserConfigurationException {
    workspace = new WorkspaceParser(url.getParent()).parse();
    Url parentDirectory = url.getParent();
    ProjectList projectList = new ProjectList();
    Url[] subDirectories = VFS.getChildren(parentDirectory, Filesystem.TYPE_DIRECTORY);
    for(int i = 0; i < subDirectories.length; i++) {
      Url projectFile = subDirectories[i].getRelativeUrl(".project");
      Url classPathFile = subDirectories[i].getRelativeUrl(".classpath");
      if (VFS.isFile(projectFile)) {
        if (VFS.isFile(classPathFile)) {
          try {
            Project project = Project.getProject(projectFile);
            projectList.addProject(project);
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        } else {
          //TODO load basic projects
        }
      }
    }
    projectList.addToProjectGroup(projectGroup);
    workspace = null;
  }

  private static Workspace workspace = null;

  public static Workspace getCurrentWorkspace() {
    return workspace;
  }

  public static void setCurrentWorkspace(Workspace workspace) {
    EclipseWorkspaceStorage.workspace = workspace;
  }

}
