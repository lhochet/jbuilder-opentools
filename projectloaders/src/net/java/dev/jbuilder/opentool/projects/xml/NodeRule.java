package net.java.dev.jbuilder.opentool.projects.xml;

import org.w3c.dom.Node;

public interface NodeRule {
  public Object process(Node node);

  public static final NodeRule ALL = new NodeRule() {
    public Object process(Node node) {
      return node;
    }

  };
}
