package net.java.dev.jbuilder.opentool.projects.general;

public interface JavaProjectLoader {
  public JavaProjectInfo load();
}
