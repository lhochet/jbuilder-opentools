package net.java.dev.jbuilder.opentool.projects.general;

import com.borland.primetime.node.ProjectStorage;
import com.borland.primetime.node.Project;
import com.borland.primetime.vfs.Url;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import net.java.dev.jbuilder.opentool.projects.eclipse.EclipseProjectParser;
import com.borland.jbuilder.node.JBProject;
import com.borland.primetime.vfs.VFS;
import com.borland.primetime.util.Streams;
import net.java.dev.jbuilder.opentool.projects.jbuilder.JBProjectSetup;

abstract public class ProjectLoaderFactory extends ProjectStorage {
  public ProjectLoaderFactory(Project project) {
    super(project);
  }

  protected abstract JavaProjectLoader createLoader(InputStream inputStream, Url url);

  private String loadContent(Url url) throws IOException {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    InputStream input = VFS.getInputStream(url);
    try {
      Streams.copy(input, output);
    }
    finally {
      input.close();
    }
    return output.toString();
  }


  public static final String PROPERTY = "project.content";
  public static final String CATEGORY = "java-net-projects";

  public byte[] getBufferContent() {
    String content = project.getProperty(CATEGORY, PROPERTY, "");
    if (content.length() == 0) {
      throw new RuntimeException("Couldn't find property " + CATEGORY + ":" + PROPERTY);
    }
    return content.getBytes();
  }

  protected void readProject(InputStream inputStream, Url url) throws Exception {
    JBProject jbProject = (JBProject) project;
    JavaProjectLoader parser = createLoader(inputStream, jbProject.getUrl());
    JavaProjectInfo info = parser.load();
    new JBProjectSetup(jbProject, info).update();
    project.setProperty(CATEGORY, PROPERTY, loadContent(jbProject.getUrl()));
  }

  public boolean canWriteProject() {
    return true;
  }
}
