package net.java.dev.jbuilder.opentool.projects.idea;

import net.java.dev.jbuilder.opentool.projects.xml.NodeRule;
import org.w3c.dom.Node;
import net.java.dev.jbuilder.opentool.projects.xml.XmlUtil;

public class ComponentByName implements NodeRule {
  private String name;

  public ComponentByName(String name) {
    this.name = name;
  }

  public String path() {
    return "/project/component";
  }

  public Object process(Node node) {
    return name.equals(XmlUtil.getAttributeValue(node, "name")) ? node : null;
  }
}
