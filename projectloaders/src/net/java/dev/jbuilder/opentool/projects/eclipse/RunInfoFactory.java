package net.java.dev.jbuilder.opentool.projects.eclipse;

import java.util.*;

import net.java.dev.jbuilder.opentool.projects.general.*;

public abstract class RunInfoFactory {
  private static Map factoryMap = new HashMap();

  public static RunInfoFactory get(String type) {
    return (RunInfoFactory) factoryMap.get(type);
  }

  abstract public RunInfo getConfiguration(Workspace workspace, Map attributes);

  static {
    registerFactory("org.eclipse.jdt.junit.launchconfig", new RunInfoFactory() {
      public RunInfo getConfiguration(Workspace workspace, Map attributes) {
        JUnitRunInfo info = new JUnitRunInfo(
            (String)attributes.get("org.eclipse.jdt.launching.MAIN_TYPE"));
        return info;
      }

    });
    registerFactory("org.eclipse.jdt.launching.localJavaApplication", new RunInfoFactory() {
      public RunInfo getConfiguration(Workspace workspace, Map attributes) {
        ApplicationRunInfo info = new ApplicationRunInfo();;
        info.setMainClass((String)attributes.get("org.eclipse.jdt.launching.MAIN_TYPE"));
        String argumentMacro = (String)attributes.get("org.eclipse.jdt.launching.PROGRAM_ARGUMENTS");
        info.setArgument(argumentMacro == null ? "" : workspace.expandMacro(argumentMacro));
        String vmParameterMacro = (String) attributes.get("org.eclipse.jdt.launching.VM_ARGUMENTS");
        info.setVmParameter(vmParameterMacro == null ? "" : workspace.expandMacro(vmParameterMacro));
        return info;
      }

    });
  }

  private static void registerFactory(String string, RunInfoFactory factory) {
    factoryMap.put(string, factory);
  }
}
