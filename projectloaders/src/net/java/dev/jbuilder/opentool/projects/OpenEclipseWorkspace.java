package net.java.dev.jbuilder.opentool.projects;

import com.borland.jbuilder.JBuilderMenu;
import com.borland.primetime.ide.BrowserAction;
import com.borland.primetime.ide.Browser;
import com.borland.primetime.vfs.ui.UrlChooser;
import com.borland.primetime.vfs.Url;

public class OpenEclipseWorkspace {
  public static void initOpenTool(byte major, byte minor) {
    JBuilderMenu.GROUP_FileOpen.add(new BrowserAction() {
      public void actionPerformed(Browser browser) {
        Url url = UrlChooser.promptForDir(browser, null, "Choose the Directory to open as Eclipse Workspace");

      }
    });
  }
}
