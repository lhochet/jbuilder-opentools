package net.java.dev.jbuilder.opentool.projects.general;

public class RunInfo {
  private String name;

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
