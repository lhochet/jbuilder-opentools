package net.java.dev.jbuilder.opentool.projects.idea;

import java.io.*;
import java.util.*;

import org.w3c.dom.*;
import com.borland.primetime.vfs.*;
import net.java.dev.jbuilder.opentool.projects.general.*;
import net.java.dev.jbuilder.opentool.projects.xml.*;

public class IdeaModuleParser implements JavaProjectLoader {
  private InputStream input;
  private Url url;
  private Url projectDirectory;
  private JavaProjectInfo project;

  public IdeaModuleParser(InputStream inputStream, Url url) {
    this.input = inputStream;
    this.url = url;
    this.projectDirectory = url.getParent();
  }

  public JavaProjectInfo load() {
    project = new JavaProjectInfo(projectDirectory);
    project.setName(url.getName());
    try {
      DocumentFilter filter = new DocumentFilter(XmlUtil.getDocument(input));
      final Url[] testUrl = new Url[1];
      List sources = filter.filter("/module/component/content/sourceFolder", new NodeRule() {
        public Object process(Node node) {
          Url url = convert(XmlUtil.getAttributeValue(node, "url"));
          if ("true".equals(XmlUtil.getAttributeValue(node, "isTestSource"))) {
            testUrl[0] = url;
          }
          return url;
        }
      });
      project.setSources(sources);
      project.setTestSource(testUrl[0]);
      List output = filter.filter("/module/component/output", new NodeRule() {
        public Object process(Node node) {
          return convert(XmlUtil.getAttributeValue(node, "url"));
        }
      });
      project.setOutputUrl(output.size() == 0 ?
          projectDirectory.getRelativeUrl("classes") : (Url) output.get(0));
      List libraries = filter.filter("/module/component/orderEntry", new NodeRule() {
        public Object process(Node node) {
          Library library = null;
          String type = XmlUtil.getAttributeValue(node, "type");
          if ("library".equals(type)) {
            String libraryName = XmlUtil.getAttributeValue(node, "name");
            library = IdeaProjectStorage.getDefaultJavaProjectInfo().getLibrary(libraryName);
          } else if ("module".equals(type)) {
            String moduleName = XmlUtil.getAttributeValue(node, "module-name");
            library = IdeaProjectStorage.getDefaultJavaProjectInfo().getModuleAsLibrary(moduleName);
          }
          return library;
        }
      });
      project.setLibrary(convertToUrls(libraries));
    }
    catch (IOException ex) {
      throw new ParsingException("Error parsing file: " + url, ex);
    }
    return project;
  }

  private List convertToUrls(List libraries) {
    ArrayList list = new ArrayList();
    for (Iterator iterator = libraries.iterator(); iterator.hasNext(); ) {
      Library library = (Library) iterator.next();
      list.addAll(library.getClassPath());
    }
    return list;
  }

  private Url convert(String path) {
    int index = path.indexOf("://");
    int end = path.indexOf("!");
    if (end == -1) {
      end = path.length();
    }
    String filePath = path.substring(index + 3, end);
    final String variable = "$MODULE_DIR$";
    if (filePath.startsWith(variable)) {
      filePath = filePath.substring(variable.length() + 1);
      return projectDirectory.getRelativeUrl(filePath);
    }
    throw new UnsupportedOperationException(filePath + " not supported");
  }
}
