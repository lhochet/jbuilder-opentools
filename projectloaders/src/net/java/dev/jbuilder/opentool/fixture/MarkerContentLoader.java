package net.java.dev.jbuilder.opentool.fixture;

import java.io.*;
import java.util.*;

public class MarkerContentLoader {
  private BufferedReader reader;
  private String marker;

  public MarkerContentLoader(InputStream input, String marker) {
    this.marker = marker;
    this.reader = new BufferedReader(new InputStreamReader(input));
  }

  public String[] loadLines() throws IOException {
    ArrayList lines = new ArrayList();
    seekMark("/*@@" + marker, null);
    seekMark("*/", lines);
    return (String[]) lines.toArray(new String[lines.size()]);
  }

  private void seekMark(String mark, List list) throws IOException {
    String line = reader.readLine();
    line = rtrim(line);
    while (!line.equals(mark)) {
      if (list != null) {
        list.add(line);
      }
      line = reader.readLine();
      if (line == null) {
        throw new IOException("Cannot find marker: " + marker);
      }
    }
  }

  private String rtrim(String line) {
    int lastIndex = line.length();
    for (int index = lastIndex - 1; index > 0 && Character.isWhitespace(line.charAt(index)); index--) {
      lastIndex = index;
    }
    return line.substring(0, lastIndex);
  }

}
