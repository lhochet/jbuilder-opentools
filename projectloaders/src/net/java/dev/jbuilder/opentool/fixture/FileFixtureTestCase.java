package net.java.dev.jbuilder.opentool.fixture;

import java.io.*;
import java.util.*;

import com.borland.primetime.vfs.*;
import junit.framework.*;

public abstract class FileFixtureTestCase extends TestCase {
  protected FileFixture fileFixture;
  private List inputs = new ArrayList();

  protected void setUp() throws Exception {
    super.setUp();
    fileFixture = FileFixture.setUp(new Url(new File("tmp")), testSource());
  }

  private Url testSource() {
    return new Url(new File("test"));
  }

  protected void tearDown() throws Exception {
    fileFixture = null;
    closeAllStreams();
    super.tearDown();
  }

  public InputStream inputStream(Url url) {
    InputStream input = null;
    try {
      input = VFS.getInputStream(url);
    }
    catch (IOException ex) {
      throw new RuntimeException(ex.getMessage(), ex);
    }
    inputs.add(input);
    return input;
  }

  private void closeAllStreams() {
    for (Iterator iterator = inputs.iterator(); iterator.hasNext();) {
      InputStream stream = (InputStream) iterator.next();
      try {stream.close();}catch (IOException e) {}
    }
    inputs = null;
  }

}
