package net.java.dev.jbuilder.opentool.fixture;

import java.io.*;
import java.util.*;

import com.borland.primetime.util.*;
import com.borland.primetime.vfs.*;

public class FileFixture {

  public static FileFixture setUp(Url tempDirectoryUrl, Url resourceDirectory) {
    FileFixture fixture = new FileFixture(tempDirectoryUrl, resourceDirectory);
    fixture.cleanUp();
    return fixture;
  }

  private Url tempDirectory;
  private Url resourceDirectory;

  public FileFixture(Url tempDirectory, Url sourceDirectory) {
    this.tempDirectory = tempDirectory;
    this.resourceDirectory = sourceDirectory;
  }

  private void cleanUp() {
    if (VFS.exists(tempDirectory)) {
      clean(tempDirectory);
    }
  }

  private void clean(Url directory) {
    try {
      Url[] urls = VFS.getChildren(directory, Filesystem.TYPE_BOTH);
      for (int i = 0; i < urls.length; i++) {
        Url oneUrl = urls[i];
        if (VFS.isDirectory(oneUrl)) {
          clean(oneUrl);
        }
        VFS.delete(oneUrl);
      }
      VFS.delete(directory, false);
    }
    catch (IOException ex) {
      throw new RuntimeException("Error deleting directory: " + directory + ".  Caused by " + ex.getMessage(), ex);
    }
  }

  public String loadContent(Object object, String mark) {
    String[] lines = loadLines(object, mark);
    return concat(lines, 0, lines.length);
  }

  private String concat(String[] lines, int start, int end) {
    StringBuffer buffer = new StringBuffer();
    for (int i = start; i < end; i++) {
      buffer.append(lines[i]).append("\n");
    }
    return buffer.toString();
  }

  public void configDirectory(Object object, String mark) {
    try {
      String[] lines = loadLines(object, mark);
      for (int index = 0; index < lines.length; index++) {
        String path = path(lines[index]);
        Url url = tempDirectory.getRelativeUrl(path);
        index++;
        int end = findEnd(index, lines);
        String content = concat(lines, index, end);
        setContent(url, content);
        index = end;
      }
    }
    catch (IOException ex) {
      throw new RuntimeException(ex.getMessage(), ex);
    }
  }

  public void setContent(Url url, String content) throws IOException {
    ByteArrayInputStream input = new ByteArrayInputStream(content.getBytes());
    OutputStream output = VFS.getOutputStream(url);
    Streams.copy(input, output);
    input.close();
    output.close();
  }

  private int findEnd(int start, String[] lines) {
    for (int i = start; i < lines.length; i++) {
      if (lines[i].equals("\"\"\"")) {
        return i;
      }
    }
    throw new IllegalArgumentException("Couldn't find \"\"\" in string: " + concat(lines, 0, lines.length));
  }

  private String path(String line) {
    int index = line.indexOf(' ');
    String action = line.substring(0, index).trim();
    if (!"file".equals(action)) {
      throw new IllegalArgumentException("Ooops..guess now is the time to make a controller class for action: " + action);
    }
    return line.substring(index + 1).trim();
  }

  private String[] loadLines(Object object, String mark) {
    String className = object.getClass().getName();
    String path = className.replace('.', '/') + ".java";
    Url url = resourceDirectory.getRelativeUrl(path);
    InputStream input = null;
    try {
      input = VFS.getInputStream(url);
      return new MarkerContentLoader(input, mark).loadLines();
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    } finally {
      if (input != null) {
        try {input.close();} catch (Exception e) {}
      }
    }
  }

  public Url getUrl() {
    return tempDirectory;
  }
}
