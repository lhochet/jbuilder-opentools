package net.java.dev.jbuilder.opentool.projects.eclipse;

import junit.framework.*;
import net.java.dev.jbuilder.opentool.fixture.FileFixtureTestCase;
import com.borland.primetime.vfs.Url;
import java.util.List;
import java.io.IOException;
import net.java.dev.jbuilder.opentool.projects.general.JUnitRunInfo;
import net.java.dev.jbuilder.opentool.projects.general.ApplicationRunInfo;
import com.borland.primetime.PrimeTime;
import com.borland.primetime.Command;
import java.io.PrintStream;

public class TestWorkspaceParser extends FileFixtureTestCase {

  public void testParseProjects() throws IOException {
    fileFixture.configDirectory(this, "marker");
    Url url = fileFixture.getUrl();
    WorkspaceParser parser = new WorkspaceParser(url);
    Workspace workspace = parser.parse();
    List projectUrls = workspace.getProjectUrls();
    assertEquals(1, projectUrls.size());
    assertEquals(fileFixture.getUrl().getRelativeUrl("project2/.project"), projectUrls.get(0));
  }

  public void testParseJunitRunConfiguration() throws IOException {
    fileFixture.configDirectory(this, "marker");
    Url url = fileFixture.getUrl();
    WorkspaceParser parser = new WorkspaceParser(url);
    Workspace workspace = parser.parse();
    List runConfigurations = workspace.getRunConfig("project1");
    assertEquals(1, runConfigurations.size());
    JUnitRunInfo runinfo = (JUnitRunInfo) runConfigurations.get(0);
    assertEquals("net.java.dev.ApiTestSuite", runinfo.getTestedClass());
  }

  public void testParseApplicationRunConfiguration() throws IOException {
    fileFixture.configDirectory(this, "marker");
    Url url = fileFixture.getUrl();
    WorkspaceParser parser = new WorkspaceParser(url);
    Workspace workspace = parser.parse();
    List runConfigurations = workspace.getRunConfig("project2");
    assertEquals(1, runConfigurations.size());
    ApplicationRunInfo runinfo = (ApplicationRunInfo) runConfigurations.get(0);
    assertEquals("fitnesse.FitNesse", runinfo.getMainClass());
    assertEquals("-Xmx512M", runinfo.getVmParameter());
  }
/*@@marker
file project1/.project
dummy
"""
file project2/.project
dummy
"""
file project2/.classpath
dummy
"""
file .metadata/.plugins/org.eclipse.debug.ui/launchConfigurationHistory.xml
<?xml version="1.0" encoding="UTF-8"?>
 <launchHistory>
 <launch memento="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#10;&lt;launchConfiguration local=&quot;true&quot; path=&quot;unified build.xml.launch&quot;/&gt;&#10;" mode="run"/>
 <lastLaunch memento="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#10;&lt;launchConfiguration local=&quot;true&quot; path=&quot;unified build.xml.launch&quot;/&gt;&#10;" mode="run"/>
 <launch memento="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#10;&lt;launchConfiguration local=&quot;false&quot; path=&quot;ApiTestSuite.launch&quot;/&gt;&#10;" mode="debug"/>
 <launch memento="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#10;&lt;launchConfiguration local=&quot;false&quot; path=&quot;/vendor/test/fitnesse/Fitnesse Server.launch&quot;/&gt;&#10;" mode="debug"/>
 <launch memento="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#10;&lt;launchConfiguration local=&quot;false&quot; path=&quot;/vendor/test/fitnesse/NoSuchFile&quot;/&gt;&#10;" mode="debug"/>
 </launchHistory>
"""
file .metadata/.plugins/org.eclipse.debug.core/.launches/unified build.xml.launch
<?xml version="1.0" encoding="UTF-8"?>
 <launchConfiguration type="org.eclipse.ant.AntLaunchConfigurationType">
 <stringAttribute key="process_factory_id" value="org.eclipse.ant.ui.remoteAntProcessFactory"/>
 <booleanAttribute key="org.eclipse.debug.ui.ATTR_LAUNCH_IN_BACKGROUND" value="true"/>
 <stringAttribute key="org.eclipse.jdt.launching.MAIN_TYPE" value="org.eclipse.ant.internal.ui.antsupport.InternalAntRunner"/>
 <stringAttribute key="org.eclipse.jdt.launching.VM_INSTALL_TYPE_ID" value="org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType"/>
 <stringAttribute key="org.eclipse.jdt.launching.VM_INSTALL_NAME" value="j2sdk1.4.2_06"/>
 <stringAttribute key="org.eclipse.jdt.launching.PROJECT_ATTR" value="project1"/>
 <stringAttribute key="org.eclipse.ui.externaltools.ATTR_LOCATION" value="${workspace_loc:/unified/build.xml}"/>
 <stringAttribute key="org.eclipse.jdt.launching.CLASSPATH_PROVIDER" value="org.eclipse.ant.ui.AntClasspathProvider"/>
 </launchConfiguration>
"""
file .metadata/.plugins/org.eclipse.debug.core/.launches/ApiTestSuite.launch
<?xml version="1.0" encoding="UTF-8"?>
 <launchConfiguration type="org.eclipse.jdt.junit.launchconfig">
 <booleanAttribute key="org.eclipse.jdt.junit.KEEPRUNNING_ATTR" value="false"/>
 <stringAttribute key="org.eclipse.jdt.launching.MAIN_TYPE" value="net.java.dev.ApiTestSuite"/>
 <stringAttribute key="org.eclipse.jdt.launching.PROJECT_ATTR" value="project1"/>
 <stringAttribute key="org.eclipse.jdt.junit.CONTAINER" value=""/>
 </launchConfiguration>
"""
file vendor/test/fitnesse/Fitnesse Server.launch
<?xml version="1.0" encoding="UTF-8"?>
 <launchConfiguration type="org.eclipse.jdt.launching.localJavaApplication">
 <stringAttribute key="org.eclipse.jdt.launching.MAIN_TYPE" value="fitnesse.FitNesse"/>
 <stringAttribute key="org.eclipse.jdt.launching.PROGRAM_ARGUMENTS" value="-o -e 0 -d &quot;${workspace_loc:unified}&quot;"/>
 <stringAttribute key="org.eclipse.jdt.launching.PROJECT_ATTR" value="project2"/>
 <stringAttribute key="org.eclipse.jdt.launching.VM_ARGUMENTS" value="-Xmx512M"/>
 <listAttribute key="org.eclipse.debug.ui.favoriteGroups">
 <listEntry value="org.eclipse.debug.ui.launchGroup.run"/>
 </listAttribute>
 <booleanAttribute key="org.eclipse.debug.core.appendEnvironmentVariables" value="true"/>
</launchConfiguration>
"""
*/
}
