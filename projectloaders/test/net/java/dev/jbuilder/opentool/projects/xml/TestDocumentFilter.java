package net.java.dev.jbuilder.opentool.projects.xml;

import org.w3c.dom.*;
import junit.framework.*;
import net.java.dev.jbuilder.opentool.fixture.FileFixture;
import net.java.dev.jbuilder.opentool.fixture.FileFixtureTestCase;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.*;
import java.io.*;
import org.xml.sax.*;
import java.util.List;

public class TestDocumentFilter extends FileFixtureTestCase {

  public void testFilter() {
    DocumentFilter filter = getDocument("document");
    List list = filter.filter("/root/element", new NodeRule() {
      public Object process(Node node) {
        return node.getAttributes().getNamedItem("attr").getNodeValue();
      }
    });
    assertEquals(2, list.size());
    assertEquals("value1", list.get(0));
    assertEquals("value2", list.get(1));
  }

  public void testIgnoringNullValue() {
    DocumentFilter filter = getDocument("document");
    List list = filter.filter("/root/element", new NodeRule() {
      public Object process(Node node) {
        return null;
      }
    });
    assertEquals(0, list.size());
  }

  public void testNonExistingPath() {
    DocumentFilter filter = getDocument("document");
    List list = filter.filter("/root/element/another", NodeRule.ALL);
    assertEquals(0, list.size());
  }

/*@@document
<root>
   <element attr="value1" />
   <element attr="value2" />
</root>
*/

  private DocumentFilter getDocument(String marker) {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setValidating(false);
    String content = fileFixture.loadContent(this, marker);
    try {
      Document document = factory.newDocumentBuilder().parse(new ByteArrayInputStream(content.getBytes()));
      return new DocumentFilter(document);
    }
    catch (Exception ex) {
      throw new RuntimeException(ex.getMessage(), ex);
    }
  }
}
