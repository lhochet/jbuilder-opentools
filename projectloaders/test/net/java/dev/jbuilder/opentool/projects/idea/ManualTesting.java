package net.java.dev.jbuilder.opentool.projects.idea;

import javax.swing.JFileChooser;
import java.io.File;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.vfs.VFS;
import java.io.InputStream;
import java.io.IOException;
import java.util.Iterator;
import javax.swing.filechooser.FileFilter;
import net.java.dev.jbuilder.opentool.projects.general.JavaProjectInfo;

public class ManualTesting {
  private File file;
  public ManualTesting(File file) {
    this.file = file;
  }

  public void run() throws IOException {
    Url url = new Url(file);
    InputStream inputStream = VFS.getInputStream(url);
    IdeaProjectParser projectParser = new IdeaProjectParser(inputStream, url);
    IdeaProjectStorage.defaultJavaProjectInfo = projectParser.load();
    inputStream.close();
    for (Iterator iterator = IdeaProjectStorage.defaultJavaProjectInfo.getModuleUrls().iterator();
        iterator.hasNext();) {
      Url moduleUrl = (Url) iterator.next();
      InputStream moduleFileInput = VFS.getInputStream(moduleUrl);
      IdeaModuleParser moduleParser = new IdeaModuleParser(moduleFileInput, moduleUrl);
      JavaProjectInfo project = moduleParser.load();
      moduleFileInput.close();
      System.out.println("Loaded project");
      System.out.println(project);
    }
  }

  public static void main(String[] args) throws IOException {
    File file = null;
    if (args.length > 0) {
      file = new File(args[0]);
    } else {
      JFileChooser chooser = new JFileChooser(".");
      chooser.setFileFilter(new FileFilter() {
        public boolean accept(File f) {
          return f.isDirectory() || f.getName().endsWith(".ipr");
        }

        public String getDescription() {
          return "IDEA project file";
        }
      }
);
      int value = chooser.showDialog(null, "choose");
      if (value != JFileChooser.APPROVE_OPTION) {
        return;
      }
      file = chooser.getSelectedFile();
    }
    new ManualTesting(file).run();
  }
}
