package net.java.dev.jbuilder.opentool.projects.idea;

import net.java.dev.jbuilder.opentool.fixture.*;
import com.borland.primetime.vfs.Url;
import java.io.InputStream;
import com.borland.primetime.vfs.VFS;
import java.io.IOException;
import net.java.dev.jbuilder.opentool.projects.general.JavaProjectInfo;
import java.util.List;
import net.java.dev.jbuilder.opentool.projects.general.*;
import java.io.File;

public class TestIdeaProjectParser extends FileFixtureTestCase {
  public void testLoadWithProjectDirVariable() throws IOException {
    fileFixture.configDirectory(this, "mark");
    Url url = fileFixture.getUrl().getRelativeUrl("project/project.ipr");
    InputStream is = inputStream(url);
    IdeaProjectParser parser = new IdeaProjectParser(is, url);
    IdeaProjectInfo project = parser.load();
    Library library = project.getLibrary("fixture");
    List classPath = library.getClassPath();
    assertEquals(1, classPath.size());
    assertEquals(project.getWorkingDirectory().getRelativeUrl("lib/one.jar"), classPath.get(0));
    List modules = project.getModuleUrls();
    assertEquals(1, modules.size());
    assertEquals(project.getWorkingDirectory().getRelativeUrl("dbfixture.iml"), modules.get(0));
  }

  public void testLoadWithNoProjectDirVariable() throws IOException {
    fileFixture.configDirectory(this, "mark");
    Url url = fileFixture.getUrl().getRelativeUrl("project/project.ipr");
    InputStream is = inputStream(url);
    IdeaProjectParser parser = new IdeaProjectParser(is, url);
    IdeaProjectInfo project = parser.load();
    Library library = project.getLibrary("global");
    List classPath = library.getClassPath();
    assertEquals(1, classPath.size());
    assertEquals(new Url(new File("D:/global/lib/global.jar")), classPath.get(0));
    List sourcePath = library.getSourcePath();
    assertEquals(1, sourcePath.size());
    assertEquals(new Url(new File("D:/global/lib/global-src.zip")), sourcePath.get(0));
    List modules = project.getModuleUrls();
    assertEquals(1, modules.size());
    assertEquals(project.getWorkingDirectory().getRelativeUrl("dbfixture.iml"), modules.get(0));
  }
/*@@mark
file project/project.ipr
<?xml version="1.0" encoding="UTF-8"?>
<project version="4" relativePaths="true">
  <component name="CompilerConfiguration">
    <option name="DEFAULT_COMPILER" value="Javac" />
    <option name="CLEAR_OUTPUT_DIRECTORY" value="false" />
    <option name="DEPLOY_AFTER_MAKE" value="0" />
    <resourceExtensions>
      <entry name=".+\.(properties|xml|html|dtd|tld)" />
      <entry name=".+\.(gif|png|jpeg|jpg)" />
    </resourceExtensions>
    <wildcardResourcePatterns>
      <entry name="?*.properties" />
      <entry name="?*.xml" />
      <entry name="?*.gif" />
      <entry name="?*.png" />
      <entry name="?*.jpeg" />
      <entry name="?*.jpg" />
      <entry name="?*.html" />
      <entry name="?*.dtd" />
      <entry name="?*.tld" />
    </wildcardResourcePatterns>
  </component>
  <component name="ProjectModuleManager">
    <modules>
      <module fileurl="file://$PROJECT_DIR$/dbfixture.iml" filepath="$PROJECT_DIR$/dbfixture.iml" />
    </modules>
  </component>
  <component name="ProjectRootManager" version="2" assert-keyword="true" jdk-15="false" project-jdk-name="java version &quot;1.5.0&quot;" />
  <component name="RmicSettings">
    <option name="IS_EANABLED" value="false" />
    <option name="DEBUGGING_INFO" value="true" />
    <option name="GENERATE_NO_WARNINGS" value="false" />
    <option name="GENERATE_IIOP_STUBS" value="false" />
    <option name="ADDITIONAL_OPTIONS_STRING" value="" />
  </component>
  <component name="libraryTable">
    <library name="fixture">
      <CLASSES>
        <root url="jar://$PROJECT_DIR$/lib/one.jar!/" />
      </CLASSES>
      <JAVADOC />
      <SOURCES>
        <root url="jar://$PROJECT_DIR$/lib/one-src.zip!/" />
      </SOURCES>
    </library>
    <library name="global">
      <CLASSES>
        <root url="jar://D:/global/lib/global.jar!/" />
      </CLASSES>
      <JAVADOC />
      <SOURCES>
        <root url="jar://D:/global/lib/global-src.zip!/" />
      </SOURCES>
    </library>
  </component>
</project>
"""
*/
}
