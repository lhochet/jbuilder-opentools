package net.java.dev.jbuilder.opentool.projects.eclipse;

import junit.framework.TestCase;
import net.java.dev.jbuilder.opentool.fixture.FileFixtureTestCase;
import com.borland.primetime.vfs.Url;

public class TestWorkspace extends FileFixtureTestCase {
  public void testGetUrlRelative() {
    Workspace workspace = new Workspace(fileFixture.getUrl());
    Url url = fileFixture.getUrl();
    Url directory = url.getRelativeUrl("project/lib/classes");
    assertEquals(directory.getRelativeUrl("path/path2"), workspace.getUrl(directory, "path/path2"));
  }

  public void testGetUrlAbsolute() {
    Workspace workspace = new Workspace(fileFixture.getUrl());
    Url url = fileFixture.getUrl();
    Url directory = url.getRelativeUrl("project/lib");
    assertEquals(url.getRelativeUrl("project2/lib/classes"), workspace.getUrl(directory, "/project2/lib/classes"));
  }

  public void testExpandMacroWorkspace() {
    Workspace workspace = new Workspace(fileFixture.getUrl());
    Url expected = workspace.getRoot().getRelativeUrl("directory/project");
    Url second = workspace.getRoot().getRelativeUrl("project");
    assertEquals("aaa \"" + expected.getFullName() + "\"" + second.getFullName() + " ",
        workspace.expandMacro("aaa &quot;${workspace_loc:directory/project}&quot;${workspace_loc:project} "));
  }

  public void testExpandMacroResource() {
    Workspace workspace = new Workspace(fileFixture.getUrl());
    Url expected = workspace.getRoot().getRelativeUrl("api/bin/jdbc.properties");
    assertEquals(expected.getFullName(), workspace.expandMacro("${resource_loc:/api/bin/jdbc.properties}"));
  }

}
