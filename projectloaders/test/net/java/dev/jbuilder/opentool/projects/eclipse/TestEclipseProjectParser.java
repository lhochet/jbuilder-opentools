package net.java.dev.jbuilder.opentool.projects.eclipse;

import junit.framework.*;
import net.java.dev.jbuilder.opentool.fixture.FileFixture;
import com.borland.primetime.vfs.Url;
import java.io.File;
import net.java.dev.jbuilder.opentool.projects.general.JavaProjectInfo;
import java.io.InputStream;
import com.borland.primetime.vfs.VFS;
import java.io.IOException;
import java.util.List;
import net.java.dev.jbuilder.opentool.fixture.FileFixtureTestCase;
import java.util.Collections;

public class TestEclipseProjectParser extends FileFixtureTestCase {
  private InputStream input;
  protected void setUp() throws Exception {
    super.setUp();
  }

  protected void tearDown() throws Exception {
    if (input != null) {
      input.close();
      input = null;
    }
    super.tearDown();
  }

  public void testParseProject() throws IOException {
    fileFixture.configDirectory(this, "mark");
    Url projectFile = fileFixture.getUrl().getRelativeUrl("./workspace/project/.project");
    input = VFS.getInputStream(projectFile);
    EclipseProjectParser parser = new EclipseProjectParser(input, projectFile);
    JavaProjectInfo project = parser.load();
    assertEquals("project-name", project.getName());
    List src = project.getSources();
    assertEquals(2, src.size());
    Url workingDirectory = projectFile.getParent();
    assertEquals(workingDirectory.getRelativeUrl("test"), src.get(0));
    assertEquals(workingDirectory.getRelativeUrl("src"), src.get(1));
    assertEquals(workingDirectory.getRelativeUrl("bin"), project.getOutputUrl());
    assertEquals(workingDirectory.getRelativeUrl("src"), project.getDefaultSource());
    assertEquals(workingDirectory.getRelativeUrl("test"), project.getTestSource());
    Url libraryProject = workingDirectory.getRelativeUrl("../library");
    List libraryJars = project.getLibraryUrls();
    assertEquals(4, libraryJars.size());
    assertEquals(workingDirectory.getRelativeUrl("lib/one.jar"), libraryJars.get(0));
    assertEquals(libraryProject.getRelativeUrl(".project"), libraryJars.get(1));
    assertEquals(libraryProject.getRelativeUrl("lib/two.jar"), libraryJars.get(2));
    assertEquals(workingDirectory.getRelativeUrl("lib/classes"), libraryJars.get(3));
  }

/*@@mark
file workspace/project/.project
<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
  <name>project-name</name>
  <comment></comment>
  <projects>
  </projects>
  <buildSpec>
    <buildCommand>
      <name>org.eclipse.jdt.core.javabuilder</name>
      <arguments>
      </arguments>
    </buildCommand>
  </buildSpec>
  <natures>
    <nature>org.eclipse.jdt.core.javanature</nature>
  </natures>
</projectDescription>
"""
file workspace/project/.classpath
<?xml version="1.0" encoding="UTF-8"?>
 <classpath>
   <classpathentry kind="con" path="org.eclipse.jdt.launching.JRE_CONTAINER"/>
   <classpathentry output="test-bin" kind="src" path="test"/>
   <classpathentry kind="src" path="src"/>
   <classpathentry kind="lib" path="lib/one.jar"/>
   <classpathentry kind="src" path="/library"/>
   <classpathentry kind="lib" path="/library/lib/two.jar"/>
   <classpathentry kind="lib" path="lib/classes"/>
   <classpathentry kind="output" path="bin"/>
</classpath>
"""
file workspace/library/.project
<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
  <name>library</name>
  <comment></comment>
  <projects>
  </projects>
  <buildSpec>
    <buildCommand>
      <name>org.eclipse.jdt.core.javabuilder</name>
      <arguments>
      </arguments>
    </buildCommand>
  </buildSpec>
  <natures>
    <nature>org.eclipse.jdt.core.javanature</nature>
  </natures>
</projectDescription>
"""
file workspace/library/.classpath
<?xml version="1.0" encoding="UTF-8"?>
 <classpath>
   <classpathentry kind="con" path="org.eclipse.jdt.launching.JRE_CONTAINER"/>
   <classpathentry output="test-bin" kind="src" path="test"/>
   <classpathentry kind="src" path="src"/>
   <classpathentry kind="lib" path="lib/one.jar"/>
   <classpathentry kind="lib" path="lib/two.jar"/>
   <classpathentry kind="output" path="bin"/>
</classpath>
"""
*/

  public void testSourceIsTheRoot() throws IOException {
    fileFixture.configDirectory(this, "SourceAtRoot");
    Url projectUrl = fileFixture.getUrl().getRelativeUrl("project/.project");
    input = VFS.getInputStream(projectUrl);
    EclipseProjectParser parser = new EclipseProjectParser(input, projectUrl);
    JavaProjectInfo info = parser.load();
    assertEquals("project", info.getName());
    List sources = info.getSources();
    Url projectDirectory = projectUrl.getParent();
    assertEquals(1, sources.size());
    assertEquals(projectDirectory, sources.get(0));
    assertEquals(projectDirectory, info.getDefaultSource());
    assertEquals(projectDirectory, info.getTestSource());
    assertEquals(projectDirectory, info.getOutputUrl());
  }
/*@@SourceAtRoot
file project/.project
<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
  <name>project</name>
  <comment></comment>
  <projects>
  </projects>
  <buildSpec>
    <buildCommand>
      <name>org.eclipse.jdt.core.javabuilder</name>
      <arguments>
      </arguments>
    </buildCommand>
  </buildSpec>
  <natures>
    <nature>org.eclipse.jdt.core.javanature</nature>
  </natures>
</projectDescription>
"""
file project/.classpath
<?xml version="1.0" encoding="UTF-8"?>
<classpath>
  <classpathentry kind="src" path=""/>
  <classpathentry kind="con" path="org.eclipse.jdt.launching.JRE_CONTAINER"/>
  <classpathentry kind="output" path=""/>
</classpath>
"""
*/

}
