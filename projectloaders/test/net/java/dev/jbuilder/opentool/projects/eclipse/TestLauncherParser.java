package net.java.dev.jbuilder.opentool.projects.eclipse;

import java.io.*;
import java.util.*;

import net.java.dev.jbuilder.opentool.fixture.*;
import net.java.dev.jbuilder.opentool.projects.general.*;
import com.borland.primetime.vfs.*;

public class TestLauncherParser extends FileFixtureTestCase {
  public void testAntLauncher() throws IOException {
    fileFixture.configDirectory(this, "ant");
    Workspace workspace = new Workspace(fileFixture.getUrl());
    LauncherParser parser = new LauncherParser(workspace, "unified build.xml.launch", false);
    parser.parse();

    assertEquals(0, workspace.getRunConfig("unified").size());
  }
/*@@ant
file .metadata/.plugins/org.eclipse.debug.core/.launches/unified build.xml.launch
<?xml version="1.0"?>
<launchConfiguration type="org.eclipse.ant.AntLaunchConfigurationType">
    <stringAttribute key="process_factory_id" value="org.eclipse.ant.ui.remoteAntProcessFactory"/>
    <booleanAttribute key="org.eclipse.debug.ui.ATTR_LAUNCH_IN_BACKGROUND" value="true"/>
    <stringAttribute key="org.eclipse.jdt.launching.MAIN_TYPE" value="org.eclipse.ant.internal.ui.antsupport.InternalAntRunner"/>
    <stringAttribute key="org.eclipse.jdt.launching.VM_INSTALL_TYPE_ID" value="org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType"/>
    <stringAttribute key="org.eclipse.jdt.launching.VM_INSTALL_NAME" value="j2sdk1.4.2_06"/>
    <stringAttribute key="org.eclipse.jdt.launching.PROJECT_ATTR" value="unified"/>
    <stringAttribute key="org.eclipse.ui.externaltools.ATTR_LOCATION" value="${workspace_loc:/unified/build.xml}"/>
    <stringAttribute key="org.eclipse.jdt.launching.CLASSPATH_PROVIDER" value="org.eclipse.ant.ui.AntClasspathProvider"/>
</launchConfiguration>
"""
*/

  public void testJUnitLauncher() throws IOException {
    fileFixture.configDirectory(this, "junit");
    Workspace workspace = new Workspace(fileFixture.getUrl());
    LauncherParser parser = new LauncherParser(workspace, "ApiTestSuite.launch", true);
    parser.parse();
    List list = workspace.getRunConfig("unified");
    assertEquals(1, list.size());
    JUnitRunInfo runinfo = (JUnitRunInfo) list.get(0);
    assertEquals("net.java.dev.ApiTestSuite", runinfo.getTestedClass());
  }
/*@@junit
file .metadata\.plugins\org.eclipse.debug.core\.launches\ApiTestSuite.launch
<?xml version="1.0" encoding="UTF-8"?>
<launchConfiguration type="org.eclipse.jdt.junit.launchconfig">
    <booleanAttribute key="org.eclipse.jdt.junit.KEEPRUNNING_ATTR" value="false"/>
    <stringAttribute key="org.eclipse.jdt.launching.MAIN_TYPE" value="net.java.dev.ApiTestSuite"/>
    <stringAttribute key="org.eclipse.jdt.launching.PROJECT_ATTR" value="unified"/>
    <stringAttribute key="org.eclipse.jdt.junit.CONTAINER" value=""/>
</launchConfiguration>
"""
*/

  public void testApplicationLauncher() throws IOException {
    fileFixture.configDirectory(this, "application");
    Workspace workspace = new Workspace(fileFixture.getUrl());
    Url unifiedUrl = workspace.getRoot().getRelativeUrl("unified");
    workspace.addProjectUrl(unifiedUrl);
    LauncherParser parser = new LauncherParser(workspace, "Fitnesse.launch", true);
    parser.parse();
    List list = workspace.getRunConfig("unified");
    assertEquals(1, list.size());
    ApplicationRunInfo runinfo = (ApplicationRunInfo) list.get(0);
    assertEquals("fitnesse.FitNesse", runinfo.getMainClass());
    assertEquals("-o -e 0 -d \"" + unifiedUrl.getFullName() + "\"", runinfo.getArgument());
    assertEquals("-Xmx512M", runinfo.getVmParameter());
  }

/*@@application
file .metadata\.plugins\org.eclipse.debug.core\.launches\Fitnesse.launch
<?xml version="1.0" encoding="UTF-8"?>
<launchConfiguration type="org.eclipse.jdt.launching.localJavaApplication">
  <stringAttribute key="org.eclipse.jdt.launching.MAIN_TYPE" value="fitnesse.FitNesse"/>
  <stringAttribute key="org.eclipse.jdt.launching.PROGRAM_ARGUMENTS" value="-o -e 0 -d &quot;${workspace_loc:unified}&quot;"/>
  <stringAttribute key="org.eclipse.jdt.launching.PROJECT_ATTR" value="unified"/>
  <stringAttribute key="org.eclipse.jdt.launching.VM_ARGUMENTS" value="-Xmx512M"/>
  <listAttribute key="org.eclipse.debug.ui.favoriteGroups">
  <listEntry value="org.eclipse.debug.ui.launchGroup.run"/>
  </listAttribute>
  <booleanAttribute key="org.eclipse.debug.core.appendEnvironmentVariables" value="true"/>
</launchConfiguration>
"""
*/
}
