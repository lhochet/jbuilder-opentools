package net.java.dev.jbuilder.opentool.projects.general;

import java.io.*;
import java.util.*;

import com.borland.primetime.vfs.*;
import junit.framework.*;

public class TestJavaProjectInfo extends TestCase {
  public void testGetDefaultSourceAndTestForSingleSource() {
    Url url = new Url(new File("tmp"));
    JavaProjectInfo project = new JavaProjectInfo(url);
    project.setSources(asList(url, new String[] {"src"}));
    assertEquals(url.getRelativeUrl("src"), project.getDefaultSource());
    assertEquals(url.getRelativeUrl("src"), project.getTestSource());
  }

  public void testGetDefaultSourceAndTest() {
    Url url = new Url(new File("tmp"));
    JavaProjectInfo project = new JavaProjectInfo(url);
    project.setSources(asList(url, new String[] {"src", "test"}));
    assertEquals(url.getRelativeUrl("src"), project.getDefaultSource());
    assertEquals(url.getRelativeUrl("test"), project.getTestSource());
  }

  public void testTestAndTools() {
    Url url = new Url(new File("tmp"));
    JavaProjectInfo project = new JavaProjectInfo(url);
    project.setSources(asList(url, new String[] {"test", "tools"}));
    assertEquals(url.getRelativeUrl("tools"), project.getDefaultSource());
    assertEquals(url.getRelativeUrl("test"), project.getTestSource());
  }

  private List asList(Url url, String[] paths) {
    ArrayList list = new ArrayList(paths.length);
    for(int i = 0; i < paths.length; i++) {
      list.add(url.getRelativeUrl(paths[i]));
    }
    return list;
  }
}
