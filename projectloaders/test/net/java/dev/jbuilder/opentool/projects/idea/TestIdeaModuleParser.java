package net.java.dev.jbuilder.opentool.projects.idea;

import net.java.dev.jbuilder.opentool.fixture.*;
import com.borland.primetime.vfs.Url;
import net.java.dev.jbuilder.opentool.projects.general.JavaProjectInfo;
import net.java.dev.jbuilder.opentool.projects.general.BasicLibrary;
import java.util.Arrays;
import java.util.List;

public class TestIdeaModuleParser extends FileFixtureTestCase {
  public void testLoadSingleModule() {
    fileFixture.configDirectory(this, "mark");
    Url projectDirectory = fileFixture.getUrl().getRelativeUrl("project");
    IdeaProjectInfo ideaProject = new IdeaProjectInfo(projectDirectory.getRelativeUrl("project.ipr"));
    Url libraryClass = projectDirectory.getRelativeUrl("lib/fixture.jar");
    Url librarySource = projectDirectory.getRelativeUrl("lib/source.zip");
    ideaProject.addLibrary(new BasicLibrary("fixture",
        Arrays.asList(new Url[] {libraryClass}),
        Arrays.asList(new Url[] {librarySource})
        ));
    ideaProject.setModuleUrls(Arrays.asList(new Url[] {
        projectDirectory.getRelativeUrl("modules/depended.iml"),
        projectDirectory.getRelativeUrl("project/module.iml")
    }));
    IdeaProjectStorage.defaultJavaProjectInfo = ideaProject;
    Url url = fileFixture.getUrl().getRelativeUrl("project/module.iml");
    IdeaModuleParser parser = new IdeaModuleParser(inputStream(url), url);
    JavaProjectInfo project = parser.load();
    assertEquals("module.iml", project.getName());
    List sources = project.getSources();
    assertEquals(2, sources.size());
    assertEquals(projectDirectory.getRelativeUrl("src"), sources.get(0));
    assertEquals(projectDirectory.getRelativeUrl("test"), sources.get(1));
    assertEquals(projectDirectory.getRelativeUrl("classes"), project.getOutputUrl());
    assertEquals(projectDirectory.getRelativeUrl("src"), project.getDefaultSource());
    assertEquals(projectDirectory.getRelativeUrl("test"), project.getTestSource());
    List libraryUrls = project.getLibraryUrls();
    assertEquals(2, libraryUrls.size());
    assertEquals(projectDirectory.getRelativeUrl("modules/depended.iml"), libraryUrls.get(0));
    assertEquals(libraryClass, libraryUrls.get(1));
  }
/*@@mark
file project/module.iml
<?xml version="1.0" encoding="UTF-8"?>
<module version="4" relativePaths="true" type="JAVA_MODULE">
 <component name="ModuleRootManager" />
 <component name="NewModuleRootManager">
   <output url="file://$MODULE_DIR$/classes" />
   <exclude-output />
   <content url="file://$MODULE_DIR$">
     <sourceFolder url="file://$MODULE_DIR$/src" isTestSource="false" />
     <sourceFolder url="file://$MODULE_DIR$/test" isTestSource="true" />
   </content>
   <orderEntry type="inheritedJdk" />
   <orderEntry type="sourceFolder" forTests="false" />
    <orderEntry type="module" module-name="depended" />
   <orderEntry type="library" name="fixture" level="project" />
   <orderEntryProperties />
 </component>
</module>
"""
*/
}
