/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.jvnet.jbuilder.opentools.pkgversion;

import com.borland.jbuilder.wizard.archive.*;
import com.borland.primetime.properties.*;

/**
 * Jar Version property group, creates the property page and stores the 'increment version'
 * property for the given archive node
 * @author Ludovic HOCHET
 * @version $Revision: 1.1 $ $Date: 2005-04-17 21:01:09 $
 */
public class VersionPropertyGroup implements PropertyGroup
{
  private static final String CAT = "lh.manifestext";
  public static final NodeBooleanProperty INCREMENT_VERSIONS = new NodeBooleanProperty(CAT, "inc_vrs", false);

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    PropertyManager.registerPropertyGroup(new VersionPropertyGroup());
  }


  public PropertyPageFactory getPageFactory(Object topic)
  {
    if (!(topic instanceof ArchiveNode)) return null;
    final ArchiveNode node = (ArchiveNode)topic;

    return new PropertyPageFactory("Version")
    {
      public PropertyPage createPropertyPage()
      {
        return new VersionPropertyPage(node);
      }

      public boolean isWizardPageFactory()
      {
        return true;
      }

    };
  }

  public void initializeProperties()
  {
    // do nothing
  }
}
