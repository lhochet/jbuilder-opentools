
/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.jvnet.jbuilder.opentools.pkgversion;

import java.io.*;
import java.util.logging.*;

import com.borland.jbuilder.wizard.archive.*;
import com.borland.primetime.build.*;
import com.borland.primetime.ide.*;
import com.borland.primetime.node.*;
import com.borland.primetime.vfs.*;
import java.util.*;

/**
 * The build task incrementing the Implementation-Version lines of the manifest
 * associated with the given archive node.
 * 
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005-07-20 21:03:15 $
 */
public class PkgVersionBuildTask extends BuildTask
{
  private Logger logger = Logger.getLogger(PkgVersionBuildTask.class.getName());
  
  private class Pair
  {
    final String name;
    final String value;

    Pair(String name, String value)
    {
      this.name = name;
      this.value = value;
    }
  }

  private ArrayList nodes = new ArrayList(1);
  
  private ArrayList manifests = new ArrayList(1);

  public PkgVersionBuildTask()
  {
  }
  
  public void addNode(ArchiveNode node)
  {
    nodes.add(node);
  }
  
  public void setManifestList(ArrayList manifests)
  {
    this.manifests = manifests;
  }

  public boolean build(BuildProcess buildProcess)
  {
    Iterator iter = nodes.iterator();
    while (iter.hasNext())
    {
      ArchiveNode node = (ArchiveNode)iter.next();
      buildNode(node); 
    }
    return true;
  }
  
  private void buildNode(ArchiveNode nodeToBuild)
  {
    String manifestpath = ManifestPropertyGroup.MANIFEST_OVERRIDE_PATH.getValue(nodeToBuild);
    logger.finest("manifestpath = " + manifestpath);
    try
    {
      Url url = null;
      if (manifestpath.indexOf("%") > -1)
      {
        url = new Url("file://" + manifestpath);
      }
      else
      {
        Url prjpath = Browser.getActiveBrowser().getActiveProject().getProjectPath();
        logger.finest("prjpath = " + prjpath);
        url = new Url(new File(prjpath.getFileObject(), manifestpath));
      }

      logger.finest("url="+url.toString());
      logger.finest("path="+url.getFileObject().getAbsolutePath());
      Url prjpath = Browser.getActiveBrowser().getActiveProject().getProjectPath();
      logger.finest("prjpath = " + prjpath);
      FileNode fileNode = Browser.getActiveBrowser().getActiveProject().getNode(url);
      
      if (manifests.contains(fileNode))
      {
        return; // abort incrementing, this manifest already has 
      }
      
      Buffer buffer = fileNode.getBuffer();
      byte[] bytes = buffer.getContent();
      BufferedReader in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bytes)));
      ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length);
      PrintWriter pw = new PrintWriter(new OutputStreamWriter(baos));
      String line = null;
      final String prefix = "Implementation-Version: ";
      while ((line = in.readLine()) != null)
      {
        if (line.startsWith(prefix))
        {
          String subline = line.substring(prefix.length());
          boolean hasQuotes = subline.charAt(0) == '"';
          if (hasQuotes)
          {
            line = prefix + "\"" + increment(subline.substring(1, subline.length() - 1)) + "\"";
          }
          else
          {
            line = prefix + increment(subline);
          }
        }
        pw.println(line);
      }
      pw.flush();
      bytes = baos.toByteArray();
      buffer.setContent(bytes);
      buffer.save();
      
      manifests.add(fileNode);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      logger.log(Level.FINE, "error when trying to increment a version in a manifest file", ex);
    }

  }

  private String increment(String subline)
  {
    String ret = subline;
    try
    {
      int lio = ret.lastIndexOf(".") + 1;
      String p = ret.substring(0, lio);
      String e = ret.substring(lio);
      int i = Integer.parseInt(e);
      ret = p + (i+1);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      logger.log(Level.FINE, "error when parsing a version string", ex);
    }
    logger.finest("ret = " + ret);
    return ret;
  }

}
