
/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.jvnet.jbuilder.opentools.pkgversion;

import java.util.*;
import java.util.logging.*;

import com.borland.jbuilder.build.*;
import com.borland.jbuilder.enterprise.module.web.*;
import com.borland.jbuilder.wizard.archive.*;
import com.borland.primetime.*;
import com.borland.primetime.build.*;
import com.borland.primetime.node.*;
import com.borland.primetime.properties.*;

/**
 * Package version Builder, creates a task for every archive node that
 * has the increment version to true
 * @author Ludovic HOCHET
 * @author  grik24
 * @version $Revision: 1.4 $ $Date: 2005-07-26 21:47:14 $
 */
public class PkgVersionBuilder extends Builder
{
  private Logger logger = Logger.getLogger(PkgVersionBuilder.class.getName());
  
  // are those publically defined somewhere?
  
  // these is the node property used to determine wether an ArchiveNode should be built
  public static final NodeBooleanProperty ARCHIVE_BUILD_WITH_PROJECT = new NodeBooleanProperty("archiving", "buildWithProject", true); 
  // these are the node properties used to determine wether an WebModuleNode should be built
  public static final NodeBooleanProperty WEBMODULE_BUILD_WITH_MODULE = new NodeBooleanProperty("module", "buildModuleArchiveWithModuleNode", true);  
  public static final NodeBooleanProperty WEBMODULE_BUILD_WITH_PROJECT = new NodeBooleanProperty("module", "buildModuleArchiveWithProject", true);  
  
  private class TaskDef
  {
    String target;
    String taskName;
    PkgVersionBuildTask task;
    
    public TaskDef(String target, String taskName)
    {
      this.target = target;
      this.taskName = taskName;
    }
  }
    
  
  private boolean initialised = false;
  private TaskDef archive = null;
  private TaskDef webmodule = null;
  
  private boolean isProjectBuild = false;
  
  private ArrayList manifests = null;

  

  public static void initOpenTool(byte majorVersion, byte minorVersion)
  {
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising JVNet PkgVersionBuilder " + Package.getPackage("org.jvnet.jbuilder.opentools.pkgversion").getImplementationVersion());
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + majorVersion + "." + minorVersion);
    }

    BuilderManager.registerBuilder(new PkgVersionBuilder());

  }
  
  
  public void beginUpdateBuildProcess(BuildProcess buildProcess)
  {
    if (!initialised)
    {
      manifests = new ArrayList(1);
      
      archive = new TaskDef(BuildTargets.ARCHIVE, "PkgVersionBuildTarget");
      webmodule = new TaskDef(BuildTargets.WEB_MODULE, "PkgVersionBuildTargetWebModule");
      
      initialised = true;
    }
    // reset the tasks for this build
    archive.task = null;
    webmodule.task = null;
    manifests.clear();
    isProjectBuild = false;
  }

  public void updateBuildProcess(BuildProcess buildProcess, Node node)
  {
    checkIsProjectBuild(node);
    
    if (isMakeable(node))
    {
      TaskDef tdef;
      
      if (isWebModuleNode(node))
      {
        tdef = webmodule;
      }
      else // isArchiveNode(node)
      {
        tdef = archive;
      }
      if (tdef != null)
      {
        PkgVersionBuildTask myBuildTask = tdef.task;
        if (myBuildTask == null)
        {
          myBuildTask = (PkgVersionBuildTask)buildProcess.createTask(PkgVersionBuildTask.class, tdef.taskName);
          myBuildTask.setManifestList(manifests);
          
          tdef.task = myBuildTask;
        }
        myBuildTask.addNode((ArchiveNode)node);
      }
    }
  }

  private void checkIsProjectBuild(Node node)
  {
    if (node instanceof Project)
    {
      isProjectBuild = true;
    }
  }

  public boolean isMakeable(Node node)
  {
    // 1. check if the node is a archive description node
    // 2. then check if the archive is going to be made
    // 3. then check if the build number is to be incremented
    
    boolean ret = false;
    if (isWebModuleNode(node))
    {
      if (isProjectBuild) // only check if the node is to be built with the project when we are doing a project build
      {
        ret = WEBMODULE_BUILD_WITH_PROJECT.getBoolean(node);
      }
      else // it is expected that it is the module that is built
      {
        ret = WEBMODULE_BUILD_WITH_MODULE.getBoolean(node);
      }      
    }
    else if (isArchiveNode(node))
    {
      if (isProjectBuild) // only check if the node is to be built with the project when we are doing a project build
      {
        ret = ARCHIVE_BUILD_WITH_PROJECT.getBoolean(node);
      }
    }
    if (ret)
    {
      ret = VersionPropertyGroup.INCREMENT_VERSIONS.getBoolean(node);
    }
    return ret;
//    return (isWebModuleNode(node) || isArchiveNode(node)) && (VersionPropertyGroup.INCREMENT_VERSIONS.getBoolean(node);
  }

  public boolean isArchiveNode(Node node)
  {
    return node instanceof ArchiveNode;
  }

  public boolean isWebModuleNode(Node node)
  {
    return node instanceof WebModuleNode;
  }

  public void endUpdateBuildProcess(BuildProcess buildProcess)
  {
    if (archive.task != null) addDependency(buildProcess, archive);
    if (webmodule.task != null) addDependency(buildProcess, webmodule);
  }
  
  private void addDependency(BuildProcess buildProcess, TaskDef tdef)
  {
    ArchiveBuilder.ArchiveBuildTask archiver = (ArchiveBuilder.ArchiveBuildTask)buildProcess.getFirstBuildTask(tdef.target, ArchiveBuilder.ArchiveBuildTask.class);
    logger.fine("target = " + tdef.target + ", archiver = " + archiver);
    if (archiver != null)
    {
      buildProcess.addDependency(tdef.taskName, tdef.target);
    }
  }

}
