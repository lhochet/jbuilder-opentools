/*
   Copyright: Copyright (c) 2004-2005 Ludovic HOCHET</p>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.jvnet.jbuilder.opentools.pkgversion;

import java.awt.*;
import javax.swing.*;

import com.borland.jbuilder.wizard.archive.*;
import com.borland.primetime.help.*;
import com.borland.primetime.wizard.*;

/**
 * Jar properties version page.
 * 
 * @author Ludovic HOCHET
 * @version $Revision: 1.2 $ $Date: 2005-05-07 13:12:05 $
 */
public class VersionPropertyPage extends WizardPropertyPage
{
  private ArchiveNode node;

  public VersionPropertyPage(ArchiveNode node)
  {
    this.node = node;
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void readProperties()
  {
    cbxIncrementVersion.setSelected(VersionPropertyGroup.INCREMENT_VERSIONS.getBoolean(node));
  }

  public void writeProperties()
  {
    VersionPropertyGroup.INCREMENT_VERSIONS.setBoolean(node, cbxIncrementVersion.isSelected());
  }

  public HelpTopic getHelpTopic()
  {
    return new ZipHelpTopic(null, getClass().getResource("propertypage.html").toString());
  }

  public String getTitle()
  {
    return "Version";
  }
  public String getInstructions()
  {
    return "Set whether the packages versions should be incremented or not";
  }
  public Icon getLargeIcon()
  {
    return null;
  }
  public Icon getSmallIcon()
  {
    return null;
  }

  private void jbInit() throws Exception
  {
    this.setLayout(borderLayout1);
    cbxIncrementVersion.setText("Increment packages versions");
    this.add(cbxIncrementVersion, java.awt.BorderLayout.NORTH);
  }

  private BorderLayout borderLayout1 = new BorderLayout();
  private JCheckBox cbxIncrementVersion = new JCheckBox();
}
