package lh.newtoolbar;

// jdk 1.4
import java.util.logging.*;
import java.util.prefs.*;

// general wizards
import com.borland.jbuilder.wizard.jbx.*;
// the Primetime ! class
import com.borland.primetime.*;
// the ActionGroup class
import com.borland.primetime.actions.*;
// the all important Browser class
import com.borland.primetime.ide.*;
// the usefull JBuilderIcons class
import com.borland.jbuilder.ide.*;

/**
 * Title:        LH JBuilder UI Extension<p>
 * Description:  Add the 'New Class' and 'New Interface' actions to either the 'File' toolbar
 *               or a separate one on the left or the right of the toolbar area depanding on
 *               a system property or a preference<p>
 * Copyright:    Copyright (c) Ludovic HOCHET<p>
 * @author       Ludovic HOCHET
 * @version      1.2, 7/06/03<br>
 * 1.1.1, 16/12/02<br>
 * 1.1, 23/09/02<br>
 * 1.0, 22/04/02<br>
 * 0.4, 6/12/01<br>
 * 0.3, 5/05/01<br>
 */
public class NewToolBar
{
  /** Action group used to hold the 'new' actions*/
  static ActionGroup lhGroup = null;

  /**
   * Initialise the opentool, add the actions to the File toolbar or to a 'New' toolbar
   * @param major Major version number of the 'instance' of JBuilder running this OT (eg. 4)
   * @param minor Minor version number of the 'instance' of JBuilder running this OT (eg. 6 for JB9)
   */
  public static void initOpenTool(byte major, byte minor)
  {
    // could be usefull for diagnostic
    if (PrimeTime.isVerbose())
    {
      System.out.println("Initialising LH 'New' Toolbar OpenTool vrs 1.2");
      System.out.println("  Compiled for JB OTAPI " + PrimeTime.CURRENT_MAJOR_VERSION + "." + PrimeTime.CURRENT_MINOR_VERSION);
      System.out.println("  Executed with JB OTAPI " + major + "." + minor);
    }

    try
    {
      lhGroup = new ActionGroup("New", 'n', "New ToolBar");
      // for some reasons these actions don't have small icons
      // JB9 Change: Default icons are now shared between
      // com.borland.primetime.ide.BrowserIcons
      // and
      // com.borland.jbuilder.ide.JBuilderIcons
      JbxWizards.WIZARD_NewClass.setSmallIcon(JBuilderIcons.ICON_CLASS);
      lhGroup.add(JbxWizards.WIZARD_NewClass);
      JbxWizards.WIZARD_NewInterface.setSmallIcon(JBuilderIcons.ICON_INTERFACE);
      lhGroup.add(JbxWizards.WIZARD_NewInterface);

      String where = System.getProperty("lh.newtoolbar");
      if (where == null)
      {
        where = Preferences.userNodeForPackage(NewToolBar.class).get("where", null);
      }
      if (where == null)
      {
        com.borland.jbuilder.JBuilderToolBar.GROUP_FileBar.add(0, lhGroup);
      }
      else if (where.equals("start"))
      {
        Browser.addToolBarGroup(0, lhGroup);
      }
      else if (where.equals("end"))
      {
        Browser.addToolBarGroup(lhGroup);
      }
      else
      {
        com.borland.jbuilder.JBuilderToolBar.GROUP_FileBar.add(0, lhGroup);
      }
    }
    catch (Exception ex)
    {
      // if something wrong happens try to log it
      // (yet to see it fail though)
      Logger logger = Logger.getLogger(NewToolBar.class.getName());
      ex.fillInStackTrace();
      logger.throwing("LH NewToolBar", "initOpenTool", ex);
//      // if no logging is available...
//      System.err.println("LH NewToolBar.initOpenTool: exception occured");
//      ex.printStackTrace();
    }
  }

}